//
//  ProtoParseTest.swift
//  Rentautomation
//
//  Created by kanybek on 5/11/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import XCTest
@testable import TazaPos

class ProtoParseTest: XCTestCase {
    
    let bigValue = Database.shared.bigValue
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCategory() {
        let category = Category(categoryId: bigValue,
                                categoryName: "categoryNamecategoryNamecategoryName")
        let catReq = ModelToProto.categoryRequestFrom(category)
        let savedCategory = ProtoToModel.categoryFrom(catReq)
        
        XCTAssertNotNil(savedCategory)
        XCTAssertEqual(savedCategory.categoryName, "categoryNamecategoryNamecategoryName", "customerImagePath")
    }
    
    func testCustomer() {
        let customer = Customer(customerId: bigValue,
                                customerImagePath: "customerImagePath",
                                firstName: "Marat",
                                secondName: "Isabaev",
                                phoneNumber: "+996(556) 34-34-34",
                                address: "Bishkek",
                                userId: 321)
        
        let custReq = ModelToProto.customerRequestFrom(customer)
        let savedCustomer_ = ProtoToModel.customerFrom(custReq)
        
        XCTAssertNotNil(savedCustomer_)
        XCTAssertEqual(savedCustomer_.customerId, bigValue, " customerId ")
        XCTAssertEqual(savedCustomer_.customerImagePath, "customerImagePath", "customerImagePath")
        XCTAssertEqual(savedCustomer_.firstName, "Marat", "firstName")
        XCTAssertEqual(savedCustomer_.secondName, "Isabaev", "secondName")
        XCTAssertEqual(savedCustomer_.phoneNumber, "+996(556) 34-34-34", "phoneNumber")
        XCTAssertEqual(savedCustomer_.address, "Bishkek", "address")
        XCTAssertEqual(savedCustomer_.userId, 321, "staffId")
    }
    
    func testSupplier() {
        let supplier = Supplier(supplierId: bigValue,
                                supplierImagePath: "supplierImagePathsupplierImagePathsupplierImagePath",
                                companyName: "companyNamecompanyNamecompanyNamecompanyName",
                                contactFname: "contactFnamecontactFnamecontactFnamecontactFname",
                                phoneNumber: "phoneNumberphoneNumberphoneNumberphoneNumber",
                                address: "addressaddressaddressaddress")
        
        let suppReq = ModelToProto.supplierRequestFrom(supplier)
        let savedSupplier = ProtoToModel.supplierFrom(suppReq)
        
        XCTAssertEqual(savedSupplier.supplierImagePath, "supplierImagePathsupplierImagePathsupplierImagePath", "customerImagePath")
        XCTAssertEqual(savedSupplier.companyName, "companyNamecompanyNamecompanyNamecompanyName", "firstName")
        XCTAssertEqual(savedSupplier.contactFname, "contactFnamecontactFnamecontactFnamecontactFname", "secondName")
        XCTAssertEqual(savedSupplier.phoneNumber, "phoneNumberphoneNumberphoneNumberphoneNumber", "phoneNumber")
        XCTAssertEqual(savedSupplier.address, "addressaddressaddressaddress", "address")
    }
    
    func testStaff() {
        
        let employeeNil_ = Database.shared.userBy(userId: bigValue)
        XCTAssertNil(employeeNil_)
        let uuid = UUID().uuidString.lowercased()
        
        let employee = User(userId: bigValue,
                            userUUID: uuid,
                            isCurrentUser: true,
                            roleId: 1000,
                            userImagePath: "staffImagePath",
                            firstName: "Alisher",
                            secondName: "Imankulov",
                            email: "alisher@gmail.com",
                            password: "password",
                            phoneNumber: "phoneNumber",
                            address: "Bishkek")
        
        let userReq = ModelToProto.userRequestFrom(employee)
        let employee_ = ProtoToModel.userFrom(userReq)
        
        XCTAssertEqual(employee_.userId, bigValue, " staffId")
        XCTAssertEqual(employee_.userUUID, uuid, "customerImagePath")
        XCTAssertEqual(employee_.userImagePath, "staffImagePath", "customerImagePath")
        XCTAssertEqual(employee_.roleId, 1000, "roleId")
        XCTAssertEqual(employee_.firstName, "Alisher", "firstName")
        XCTAssertEqual(employee_.secondName, "Imankulov", "secondName")
        XCTAssertEqual(employee_.phoneNumber, "phoneNumber", "phoneNumber")
        XCTAssertEqual(employee_.address, "Bishkek", "address")
        XCTAssertEqual(employee_.email, "alisher@gmail.com", "email")
        XCTAssertEqual(employee_.password, "password", "password")
        
        //updateStaffva also
    }
    
    func testOrderDetail() {
        
        let uuid = UUID().uuidString.lowercased()
        let date = Date()
        
        let orderDetail = OrderDetail(orderDetailId: bigValue,
                                      orderId: 1000,
                                      orderDetailDate: date,
                                      isLast: true,
                                      billingNo: "billingNobillingNo",
                                      orderDetailComment: "orderDetailCommentorderDetailComment",
                                      orderDetailUuid: uuid,
                                      productId: 3333,
                                      price: 1234,
                                      orderQuantity: 4321,
                                      productQuantity: 5432)
        
        let orderDetailReq = ModelToProto.orderDetailRequestFrom(orderDetail)
        let savedOrderDetail_ = ProtoToModel.orderDetailFrom(orderDetailReq)
        
        XCTAssertNotNil(savedOrderDetail_)
        XCTAssertEqual(savedOrderDetail_.orderDetailId, bigValue, "")
        XCTAssertEqual(savedOrderDetail_.orderId, 1000, "")
        XCTAssertEqual(savedOrderDetail_.orderDetailDate.seconds(), date.seconds(), "")
        XCTAssertEqual(savedOrderDetail_.isLast, true, "")
        XCTAssertEqual(savedOrderDetail_.billingNo, "billingNobillingNo", "")
        XCTAssertEqual(savedOrderDetail_.orderDetailComment, "orderDetailCommentorderDetailComment", "")
        XCTAssertEqual(savedOrderDetail_.orderDetailUuid, uuid, "")
        XCTAssertEqual(savedOrderDetail_.productId, 3333, "")
        XCTAssertEqual(savedOrderDetail_.price, 1234, "")
        XCTAssertEqual(savedOrderDetail_.orderQuantity, 4321, "")
        XCTAssertEqual(savedOrderDetail_.productQuantity, 5432, "")
    }
    
    
    func testTransaction() {
        let date = Date()
        let uudid = UUID().uuidString.lowercased()
        
        let transaction = Transaction(transactionId: bigValue,
                                      transactionDate: date,
                                      isLastTransaction: true,
                                      transactionType: .customerBalanceModified,
                                      moneyAmount: 100.0,
                                      orderId: 10000,
                                      customerId: 10000,
                                      supplierId: 10000,
                                      userId: 10000,
                                      ballanceAmount: 123456,
                                      comment: "123-123-123-sdfsdfsddsf",
                                      transactionUuid: uudid)
        
        let transactionReq = ModelToProto.transactionRequestFrom(transaction)
        let saveTransaction_ = ProtoToModel.transactionFrom(transactionReq)
        
        XCTAssertNotNil(saveTransaction_)
        XCTAssertEqual(saveTransaction_.transactionId, bigValue, "")
        XCTAssertEqual(saveTransaction_.transactionDate.seconds(), date.seconds(), "")
        XCTAssertEqual(saveTransaction_.isLastTransaction, true, "")
        XCTAssertEqual(saveTransaction_.transactionType, .customerBalanceModified, "")
        XCTAssertEqual(saveTransaction_.moneyAmount, 100.0, "")
        XCTAssertEqual(saveTransaction_.ballanceAmount, 123456, "")
        XCTAssertEqual(saveTransaction_.orderId, 10000, "")
        XCTAssertEqual(saveTransaction_.customerId, 10000, "")
        XCTAssertEqual(saveTransaction_.supplierId, 10000, "")
        XCTAssertEqual(saveTransaction_.userId, 10000, "")
        XCTAssertEqual(saveTransaction_.comment, "123-123-123-sdfsdfsddsf", "")
        XCTAssertEqual(saveTransaction_.transactionUuid, uudid, "")
    }
    
    func testOrder() {
        
        let uuid = UUID().uuidString.lowercased()
        
        let order = Order(orderId: bigValue,
                          orderDocument: .moneyGone,
                          orderMovement: .orderDevelopeRelated,
                          moneyMovementType: .moneyGoneToCarFixxess,
                          billingNo: "billingNobillingNo",
                          userId: 100,
                          customerId: 100,
                          supplierId: 100,
                          orderDate: Date().dateAtStartOfWeek(),
                          paymentId: 100,
                          errorMsg: "errorMsgerrorMsg",
                          comment: "commentcommentcomment",
                          orderUUID: uuid,
                          deleted: true,
                          isMoneyForDebt: true,
                          editted: true)
        
        
        let orderReq = ModelToProto.orderRequestFrom(order)
        let finalOrder = ProtoToModel.orderFrom(orderReq)
        
        XCTAssertNotNil(finalOrder)
        XCTAssertEqual(finalOrder.orderId, bigValue, "")
        XCTAssertEqual(finalOrder.orderDocument, .moneyGone, "")
        XCTAssertEqual(finalOrder.orderMovement, .orderDevelopeRelated, "")
        XCTAssertEqual(finalOrder.moneyMovementType, .moneyGoneToCarFixxess, "")
        XCTAssertEqual(finalOrder.billingNo, "billingNobillingNo", "")
        XCTAssertEqual(finalOrder.userId, 100, "")
        XCTAssertEqual(finalOrder.customerId, 100, "")
        XCTAssertEqual(finalOrder.supplierId, 100, "")
        XCTAssertEqual(finalOrder.orderDate, Date().dateAtStartOfWeek(), "")
        XCTAssertEqual(finalOrder.paymentId, 100, "")
        XCTAssertEqual(finalOrder.errorMsg, "errorMsgerrorMsg", "")
        XCTAssertEqual(finalOrder.comment, "commentcommentcomment", "")
        XCTAssertEqual(finalOrder.orderUUID, uuid, "")
        XCTAssertEqual(finalOrder.isDeleted, true, "")
        XCTAssertEqual(finalOrder.isMoneyForDebt, true, "")
        XCTAssertEqual(finalOrder.isEdited, true, "")
    }
    
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
