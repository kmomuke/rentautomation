//
//  ModelTests.swift
//  Rentautomation
//
//  Created by kanybek on 4/30/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import XCTest
@testable import TazaPos

class ModelTests: XCTestCase {
    
    let bigValue = Database.shared.bigValue
    
    //Staff
    //Product
    //Category
    //Customer
    //Supplier
    //Account
    //Order
    //OrderDetail
    //Payment
    //Transaction
    //SyncOperation
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        Database.shared.deleteUser(bigValue)
        Database.shared.deleteProduct(bigValue)
        Database.shared.deleteCategory(bigValue)
        Database.shared.deleteCustomer(bigValue)
        Database.shared.deleteSupplier(bigValue)
        Database.shared.deleteAccount(bigValue)
        Database.shared.deleteAccount((bigValue + 1))
        Database.shared.deleteOrder(bigValue)
        Database.shared.deleteOrderDetail(bigValue)
        Database.shared.deletePayment(bigValue)
        Database.shared.deleteTransaction(bigValue)
        Database.shared.deleteOperation(bigValue)
        super.tearDown()
    }
    
    func testStaff() {
        
        let employeeNil_ = Database.shared.userBy(userId: bigValue)
        XCTAssertNil(employeeNil_)
        let uuid = UUID().uuidString.lowercased()
        
        let employee = User(userId: bigValue,
                            userUUID: uuid,
                            isCurrentUser: true,
                            roleId: 1000,
                            userImagePath: "staffImagePath",
                            firstName: "Alisher",
                            secondName: "Imankulov",
                            email: "alisher@gmail.com",
                            password: "password",
                            phoneNumber: "phoneNumber",
                            address: "Bishkek")
        
        Database.shared.createUser([employee])
        
        let employee_ = Database.shared.userBy(userId: bigValue)!
        
        XCTAssertNotNil(employee_)
        
        XCTAssertEqual(employee_.userId, bigValue, " staffId")
        XCTAssertEqual(employee_.userUUID, uuid, " staffId")
        XCTAssertEqual(employee_.isCurrentUser, true, " staffId")
        XCTAssertEqual(employee_.userImagePath, "staffImagePath", "customerImagePath")
        XCTAssertEqual(employee_.roleId, 1000, "roleId")
        XCTAssertEqual(employee_.firstName, "Alisher", "firstName")
        XCTAssertEqual(employee_.secondName, "Imankulov", "secondName")
        XCTAssertEqual(employee_.phoneNumber, "phoneNumber", "phoneNumber")
        XCTAssertEqual(employee_.address, "Bishkek", "address")
        XCTAssertEqual(employee_.email, "alisher@gmail.com", "email")
        XCTAssertEqual(employee_.password, "password", "password")
        
        
        employee_.isCurrentUser = false
        employee_.userImagePath = "userImagePathuserImagePathuserImagePath"
        employee_.address = "adress4dsffdssdfdsfds2344234adress"
        employee_.password = "2434234dsffdssdfdsfds2344234"
        employee_.email = "kano.momuke"
        
        Database.shared.updateUser(employee_)
        
        let employee__ = Database.shared.userBy(email: "kano.momuke")!
        XCTAssertEqual(employee__.userId, bigValue, " staffId")
        XCTAssertEqual(employee__.isCurrentUser, false, " staffId")
        XCTAssertEqual(employee__.userImagePath, "userImagePathuserImagePathuserImagePath", "customerImagePath")
        XCTAssertEqual(employee__.roleId, 1000, "roleId")
        XCTAssertEqual(employee__.firstName, "Alisher", "firstName")
        XCTAssertEqual(employee__.secondName, "Imankulov", "secondName")
        XCTAssertEqual(employee__.phoneNumber, "phoneNumber", "phoneNumber")
        XCTAssertEqual(employee__.address, "adress4dsffdssdfdsfds2344234adress", "address")
        XCTAssertEqual(employee__.email, "kano.momuke", "email")
        XCTAssertEqual(employee__.password, "2434234dsffdssdfdsfds2344234", "password")
        
        
        //updateStaffva also
    }
    
    func testProduct() {
        let productNil_ = Database.shared.productFor(productId: bigValue)
        XCTAssertNil(productNil_)
        
        let product = Product(productId: bigValue,
                              productImagePath: "productImagePath",
                              productName: "productName",
                              supplierId: 10000,
                              categoryId: 10000,
                              barcode: "barcode",
                              quantityPerUnit: "quantityPerUnit",
                              saleUnitPrice: 1000.0,
                              incomeUnitPrice: 1000.0,
                              unitsInStock: 1000.0)
        
        Database.shared.createProducts(products: [product])
        
        let product_ = Database.shared.productFor(productId: bigValue)!
        XCTAssertNotNil(product_)
        
        XCTAssertEqual(product_.productId, bigValue, "")
        XCTAssertEqual(product_.productImagePath, "productImagePath", "")
        XCTAssertEqual(product_.productName, "productName", "")
        XCTAssertEqual(product_.supplierId, 10000, "")
        XCTAssertEqual(product_.categoryId, 10000, "")
        XCTAssertEqual(product_.barcode, "barcode", "")
        XCTAssertEqual(product_.quantityPerUnit, "quantityPerUnit", "")
        XCTAssertEqual(product_.saleUnitPrice, 1000.0, "")
        XCTAssertEqual(product_.incomeUnitPrice, 1000.0, "")
        XCTAssertEqual(product_.unitsInStock, 1000.0, "productId")
        
        
        product_.productImagePath = "productImagePath1"
        product_.productName = "productName1"
        product_.supplierId = 10001
        product_.categoryId = 10001
        product_.barcode = "barcode1"
        product_.quantityPerUnit = "quantityPerUnit1"
        product_.saleUnitPrice = 1001.0
        product_.incomeUnitPrice = 1001.0
        product_.unitsInStock = 1001.0
        
        Database.shared.updateProduct(product_)
        
        let product2_ = Database.shared.productFor(productId: bigValue)!
        XCTAssertNotNil(product2_)
        
        XCTAssertEqual(product2_.productId, bigValue, "")
        XCTAssertEqual(product2_.productImagePath, "productImagePath1", "")
        XCTAssertEqual(product2_.productName, "productName1", "")
        XCTAssertEqual(product2_.supplierId, 10001, "")
        XCTAssertEqual(product2_.categoryId, 10001, "")
        XCTAssertEqual(product2_.barcode, "barcode1", "")
        XCTAssertEqual(product2_.quantityPerUnit, "quantityPerUnit1", "")
        XCTAssertEqual(product2_.saleUnitPrice, 1001.0, "")
        XCTAssertEqual(product2_.incomeUnitPrice, 1001.0, "")
        XCTAssertEqual(product2_.unitsInStock, 1001.0, "productId")
        
        Database.shared.updateProducsPrimaryKey(oldProductId: bigValue, newProduct: (bigValue + 1))
        
        
        let product3_ = Database.shared.productFor(productId: (bigValue + 1))!
        XCTAssertNotNil(product3_)
        
        let k = (bigValue + 1)
        XCTAssertEqual(product3_.productId, k, "")
        XCTAssertEqual(product3_.productImagePath, "productImagePath1", "")
        XCTAssertEqual(product3_.productName, "productName1", "")
        XCTAssertEqual(product3_.supplierId, 10001, "")
        XCTAssertEqual(product3_.categoryId, 10001, "")
        XCTAssertEqual(product3_.barcode, "barcode1", "")
        XCTAssertEqual(product3_.quantityPerUnit, "quantityPerUnit1", "")
        XCTAssertEqual(product3_.saleUnitPrice, 1001.0, "")
        XCTAssertEqual(product3_.incomeUnitPrice, 1001.0, "")
        XCTAssertEqual(product3_.unitsInStock, 1001.0, "productId")
        
        
        Database.shared.updateProducsPrimaryKey(oldProductId: (bigValue + 1), newProduct: bigValue)
        let product4_ = Database.shared.productFor(productId: bigValue)
        XCTAssertNotNil(product4_)
    }
    
    func testCustomer() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let savedCustomerNil_ = Database.shared.customerBy(customerId: bigValue)
        XCTAssertNil(savedCustomerNil_)
        
        let customer = Customer(customerId: bigValue,
                                customerImagePath: "customerImagePath",
                                firstName: "Marat",
                                secondName: "Isabaev",
                                phoneNumber: "+996(556) 34-34-34",
                                address: "Bishkek",
                                userId: 321)
        
        Database.shared.createCustomers([customer])
        
        
        let account = Account(accountId: (bigValue + 1), customerId: bigValue, supplierId: 0, balance: 50000)
        Database.shared.createAccounts([account])
        
        let savedAccount = Database.shared.accountBy(customerId: bigValue)!
        XCTAssertNotNil(savedAccount)
        XCTAssertEqual(savedAccount.balance, 50000, "staffId")
        
        Database.shared.updateCustomerBalanceFor(customerId: bigValue, balance: 500001)
        let savedAccount2 = Database.shared.accountBy(customerId: bigValue)!
        XCTAssertNotNil(savedAccount2)
        XCTAssertEqual(savedAccount2.supplierId, 0, "staffId")
        XCTAssertEqual(savedAccount2.customerId, bigValue, "staffId")
        XCTAssertEqual(savedAccount2.balance, 500001, "staffId")
        
        
        let savedCustomer_ = Database.shared.customerBy(customerId: bigValue)!
        
        XCTAssertNotNil(savedCustomer_)
        XCTAssertEqual(savedCustomer_.customerId, bigValue, " customerId ")
        XCTAssertEqual(savedCustomer_.customerImagePath, "customerImagePath", "customerImagePath")
        XCTAssertEqual(savedCustomer_.firstName, "Marat", "firstName")
        XCTAssertEqual(savedCustomer_.secondName, "Isabaev", "secondName")
        XCTAssertEqual(savedCustomer_.phoneNumber, "+996(556) 34-34-34", "phoneNumber")
        XCTAssertEqual(savedCustomer_.address, "Bishkek", "address")
        XCTAssertEqual(savedCustomer_.userId, 321, "staffId")
        
        
        savedCustomer_.customerImagePath = "customerImagePath2"
        savedCustomer_.firstName = "Kanybek"
        savedCustomer_.secondName = "Momukeev"
        savedCustomer_.phoneNumber = "0552-44-23-99"
        savedCustomer_.address = "Tunguch"
        savedCustomer_.userId = 400
        
        Database.shared.updateCustomer(savedCustomer_) // test update
        
        let savedCustomer2_ = Database.shared.customerBy(customerId: bigValue)!
        XCTAssertEqual(savedCustomer2_.customerImagePath, "customerImagePath2", "customerImagePath")
        XCTAssertEqual(savedCustomer2_.firstName, "Kanybek", "firstName")
        XCTAssertEqual(savedCustomer2_.secondName, "Momukeev", "secondName")
        XCTAssertEqual(savedCustomer2_.phoneNumber, "0552-44-23-99", "phoneNumber")
        XCTAssertEqual(savedCustomer2_.address, "Tunguch", "address")
        XCTAssertEqual(savedCustomer2_.userId, 400, "staffId")
        
        Database.shared.updateCustomersPrimaryKey(oldCustomerId: bigValue, newCustomerId: (bigValue+1)) // test update primary key
        let savedCustomer3_ = Database.shared.customerBy(customerId: (bigValue + 1))!
        XCTAssertEqual(savedCustomer3_.firstName, "Kanybek", "firstName")
        
        Database.shared.updateCustomersPrimaryKey(oldCustomerId: (bigValue + 1), newCustomerId: bigValue) // test update primary key
        let savedCustomer4_ = Database.shared.customerBy(customerId: bigValue)!
        XCTAssertEqual(savedCustomer4_.firstName, "Kanybek", "firstName")
    }
    
    func testCategory() {
        
        let category = Category(categoryId: bigValue,
                                categoryName: "categoryNamecategoryNamecategoryName")
        Database.shared.createCategories([category])
        
        let savedCategory = Database.shared.categoryBy(categoryId: bigValue)!
        XCTAssertNotNil(savedCategory)
        XCTAssertEqual(savedCategory.categoryName, "categoryNamecategoryNamecategoryName", "customerImagePath")
    }
    
    //sync operation
    func testSupplier() {
        
        let supplier = Supplier(supplierId: bigValue,
                                supplierImagePath: "supplierImagePathsupplierImagePathsupplierImagePath",
                                companyName: "companyNamecompanyNamecompanyNamecompanyName",
                                contactFname: "contactFnamecontactFnamecontactFnamecontactFname",
                                phoneNumber: "phoneNumberphoneNumberphoneNumberphoneNumber",
                                address: "addressaddressaddressaddress")
        Database.shared.createSuppliers([supplier])
        
        let account = Account(accountId: bigValue, customerId: 0, supplierId: bigValue, balance: 30000)
        Database.shared.createAccounts([account])
        
        let savedAccount = Database.shared.accountBy(supplierId: bigValue)!
        XCTAssertNotNil(savedAccount)
        XCTAssertEqual(savedAccount.balance, 30000, "staffId")
        
        
        let savedSupplier = Database.shared.supplierBy(supplierId: bigValue)!
        XCTAssertEqual(savedSupplier.supplierImagePath, "supplierImagePathsupplierImagePathsupplierImagePath", "customerImagePath")
        XCTAssertEqual(savedSupplier.companyName, "companyNamecompanyNamecompanyNamecompanyName", "firstName")
        XCTAssertEqual(savedSupplier.contactFname, "contactFnamecontactFnamecontactFnamecontactFname", "secondName")
        XCTAssertEqual(savedSupplier.phoneNumber, "phoneNumberphoneNumberphoneNumberphoneNumber", "phoneNumber")
        XCTAssertEqual(savedSupplier.address, "addressaddressaddressaddress", "address")
        
        savedSupplier.phoneNumber = "12123123131"
        
        Database.shared.updateSupplier(savedSupplier)
        let savedSupplier2 = Database.shared.supplierBy(supplierId: bigValue)!
        XCTAssertEqual(savedSupplier2.phoneNumber, "12123123131", "phoneNumber")
        
        
        Database.shared.updateSupplierBalanceFor(supplierId: bigValue, balance: 300001)
        let savedAccount2 = Database.shared.accountBy(supplierId: bigValue)!
        XCTAssertNotNil(savedAccount2)
        XCTAssertEqual(savedAccount2.customerId, 0, "staffId")
        XCTAssertEqual(savedAccount2.accountId, bigValue, "staffId")
        XCTAssertEqual(savedAccount2.supplierId, bigValue, "staffId")
        XCTAssertEqual(savedAccount2.balance, 300001, "staffId")
        
    }
    
    func testSyncOperation() {
        
        let date = Date()
        let operation = Event(syncOperationId: bigValue,
                              isSynchronized: false,
                              syncOperationDate: date,
                              operationType: .orderCreated,
                              productId: 1234,
                              orderDetailId: 12345,
                              customerId: 123456,
                              transactionId: 1234567,
                              accountId: 123,
                              supplierId: 321,
                              staffId: 4321,
                              categoryId: 54321,
                              orderId: 12345678)
        Database.shared.createOperations([operation])
        
        let savedOperation = Database.shared.operationBy(syncOperationId: bigValue)!
        XCTAssertNotNil(savedOperation)
        
        XCTAssertEqual(savedOperation.isSynchronized, false, "")
        XCTAssertEqual(savedOperation.syncOperationDate.seconds(), date.seconds(), "")
        XCTAssertEqual(savedOperation.operationType, .orderCreated, "")
        XCTAssertEqual(savedOperation.productId, 1234, "")
        XCTAssertEqual(savedOperation.orderDetailId, 12345, "")
        XCTAssertEqual(savedOperation.customerId, 123456, "")
        XCTAssertEqual(savedOperation.transactionId, 1234567, "")
        XCTAssertEqual(savedOperation.accountId, 123, "")
        XCTAssertEqual(savedOperation.supplierId, 321, "")
        XCTAssertEqual(savedOperation.staffId, 4321, "")
        XCTAssertEqual(savedOperation.categoryId, 54321, "")
        XCTAssertEqual(savedOperation.orderId, 12345678, "")
    }
    
    func testPayment() {
        let payment = Payment(paymentId: bigValue,
                              totalOrderPrice: 450000,
                              discount: 43,
                              totalPriceWithDiscount: 40000000,
                              minusPrice: 1000,
                              plusPrice: 1234,
                              comment: "HELLO WORLD COMMENT Here")
        
        Database.shared.createPayments([payment])
        
        let savedPayment = Database.shared.paymentBy(paymentId: bigValue)!
        XCTAssertEqual(savedPayment.paymentId, bigValue, "")
        XCTAssertEqual(savedPayment.totalOrderPrice, 450000, "")
        XCTAssertEqual(savedPayment.discount, 43, "")
        XCTAssertEqual(savedPayment.totalPriceWithDiscount, 40000000, "")
        XCTAssertEqual(savedPayment.minusPrice, 1000, "")
        XCTAssertEqual(savedPayment.plusPrice, 1234, "")
        XCTAssertEqual(savedPayment.comment, "HELLO WORLD COMMENT Here", "")
    }
    
    func testOrder() {
        
        let uuid = UUID().uuidString.lowercased()
        let order = Order(orderId: bigValue,
                          orderDocument: .moneyGone,
                          orderMovement: .orderDevelopeRelated,
                          moneyMovementType: .moneyGoneToCarFixxess,
                          billingNo: "billingNobillingNo",
                          userId: 110011,
                          customerId: 100,
                          supplierId: 100,
                          orderDate: Date().dateAtStartOfWeek(),
                          paymentId: 100,
                          errorMsg: "errorMsgerrorMsg",
                          comment: "commentcommentcomment",
                          orderUUID: uuid,
                          deleted: true,
                          isMoneyForDebt: true,
                          editted: true)
        
        Database.shared.createOrders([order])
        
        let savedOrder_ = Database.shared.orderBy(orderId: bigValue)!
        
        XCTAssertNotNil(savedOrder_)
        XCTAssertEqual(savedOrder_.orderId, bigValue, "")
        XCTAssertEqual(savedOrder_.orderDocument, .moneyGone, "")
        XCTAssertEqual(savedOrder_.orderMovement, .orderDevelopeRelated, "")
        XCTAssertEqual(savedOrder_.moneyMovementType, .moneyGoneToCarFixxess, "")
        XCTAssertEqual(savedOrder_.billingNo, "billingNobillingNo", "")
        XCTAssertEqual(savedOrder_.userId, 110011, "")
        XCTAssertEqual(savedOrder_.customerId, 100, "")
        XCTAssertEqual(savedOrder_.supplierId, 100, "")
        XCTAssertEqual(savedOrder_.orderDate, Date().dateAtStartOfWeek(), "")
        XCTAssertEqual(savedOrder_.paymentId, 100, "")
        XCTAssertEqual(savedOrder_.errorMsg, "errorMsgerrorMsg", "")
        XCTAssertEqual(savedOrder_.comment, "commentcommentcomment", "")
        XCTAssertEqual(savedOrder_.orderUUID, uuid, "")
        XCTAssertEqual(savedOrder_.isDeleted, true, "")
        XCTAssertEqual(savedOrder_.isMoneyForDebt, true, "")
        XCTAssertEqual(savedOrder_.isEdited, true, "")
        
        Database.shared.updateOrderEdittedStatus(orderId: bigValue,
                                                 isEditted: false)
        
        let updatedOrder_ = Database.shared.orderBy(orderId: bigValue)!
        XCTAssertEqual(updatedOrder_.isEdited, false, "")
        
        Database.shared.updateOrdersPaymentId(oldId: 100, newId: 200)
        let updatedOrderPay = Database.shared.orderBy(orderId: bigValue)!
        XCTAssertEqual(updatedOrderPay.paymentId, 200, "")
        
        Database.shared.updateOrdersCustomerId(oldCustomerId: 100, newCustomerId: 300)
        let updatedOrderCust = Database.shared.orderBy(orderId: bigValue)!
        XCTAssertEqual(updatedOrderCust.customerId, 300, "")
        
        Database.shared.updateOrdersCommentAndBillingNo(comment: "kokjambo", billingNo: "likoanf", orderId: bigValue)
        let updatedOrderComment = Database.shared.orderBy(orderId: bigValue)!
        XCTAssertEqual(updatedOrderComment.billingNo, "likoanf", "")
        XCTAssertEqual(updatedOrderComment.comment, "kokjambo", "")
    }
    
    func testOrderDetail() {
        let uuid = UUID().uuidString.lowercased()
        let date = Date()
        
        let orderDetail = OrderDetail(orderDetailId: bigValue,
                                      orderId: 1000,
                                      orderDetailDate: date,
                                      isLast: true,
                                      billingNo: "billingNobillingNo",
                                      orderDetailComment: "orderDetailCommentorderDetailComment",
                                      orderDetailUuid: uuid,
                                      productId: 3333,
                                      price: 1234,
                                      orderQuantity: 4321,
                                      productQuantity: 5432)
        
        Database.shared.createorderDetails([orderDetail])
        let savedOrderDetail_ =  Database.shared.orderDetailsBy(orderId: 1000).last!
        
        XCTAssertNotNil(savedOrderDetail_)
        XCTAssertEqual(savedOrderDetail_.orderDetailId, bigValue, "")
        XCTAssertEqual(savedOrderDetail_.orderId, 1000, "")
        XCTAssertEqual(savedOrderDetail_.orderDetailDate.seconds(), date.seconds(), "")
        XCTAssertEqual(savedOrderDetail_.isLast, true, "")
        XCTAssertEqual(savedOrderDetail_.billingNo, "billingNobillingNo", "")
        XCTAssertEqual(savedOrderDetail_.orderDetailComment, "orderDetailCommentorderDetailComment", "")
        XCTAssertEqual(savedOrderDetail_.orderDetailUuid, uuid, "")
        XCTAssertEqual(savedOrderDetail_.productId, 3333, "")
        XCTAssertEqual(savedOrderDetail_.price, 1234, "")
        XCTAssertEqual(savedOrderDetail_.orderQuantity, 4321, "")
        XCTAssertEqual(savedOrderDetail_.productQuantity, 5432, "")
        
        
        Database.shared.updateOrderDetailsProductId(orderDetailId: bigValue, productId: 500000)
        let savedOrderDetailPr_ =  Database.shared.orderDetailsBy(orderId: 1000).last!
        XCTAssertEqual(savedOrderDetailPr_.productId, 500000, "")
        
        
        Database.shared.updateOrderDetailsOrderId(oldOrderId: 1000, newOrderId: 9000000)
        let savedOrderDetailOrd_ =  Database.shared.orderDetailsBy(orderId: 9000000).last!
        XCTAssertEqual(savedOrderDetailOrd_.orderId, 9000000, "")
    }
    
    func testTransaction() {
        
        let savedTransactionNil_ = Database.shared.transactionBy(transactionId: bigValue)
        XCTAssertNil(savedTransactionNil_)
        
        let date = Date()
        let uudid = UUID().uuidString.lowercased()
        let transaction = Transaction(transactionId: bigValue,
                                      transactionDate: date,
                                      isLastTransaction: true,
                                      transactionType: .customerBalanceModified,
                                      moneyAmount: 100.0,
                                      orderId: 10000,
                                      customerId: 10000,
                                      supplierId: 10000,
                                      userId: 10000,
                                      ballanceAmount: 123456,
                                      comment: "123-123-123-sdfsdfsddsf",
                                      transactionUuid: uudid)
        
        Database.shared.createTransactions([transaction])
        
        let saveTransaction_ = Database.shared.transactionBy(transactionId: bigValue)!
        
        XCTAssertNotNil(saveTransaction_)
        XCTAssertEqual(saveTransaction_.transactionId, bigValue, "")
        XCTAssertEqual(saveTransaction_.transactionDate.seconds(), date.seconds(), "")
        XCTAssertEqual(saveTransaction_.isLastTransaction, true, "")
        XCTAssertEqual(saveTransaction_.transactionType, .customerBalanceModified, "")
        XCTAssertEqual(saveTransaction_.moneyAmount, 100.0, "")
        XCTAssertEqual(saveTransaction_.ballanceAmount, 123456, "")
        XCTAssertEqual(saveTransaction_.orderId, 10000, "")
        XCTAssertEqual(saveTransaction_.customerId, 10000, "")
        XCTAssertEqual(saveTransaction_.supplierId, 10000, "")
        XCTAssertEqual(saveTransaction_.userId, 10000, "")
        XCTAssertEqual(saveTransaction_.comment, "123-123-123-sdfsdfsddsf", "")
        XCTAssertEqual(saveTransaction_.transactionUuid, uudid, "")
        
        
        Database.shared.updateTransactionsCustomerId(oldCustomerId: 10000, newCustomerId: 10000111)
        let saveTransaction__ = Database.shared.transactionBy(transactionId: bigValue)!
        XCTAssertEqual(saveTransaction__.customerId, 10000111, "")
        
        Database.shared.updateTransactionsCustomerId(transactionId: bigValue, customerId: 1234554321)
        Database.shared.updateTransactionsSupplierId(transactionId: bigValue, supplierId: 1234664321)
        Database.shared.updateTransactionsOrderId(oldOrderId: 10000, newOrderId: 100004321)
        Database.shared.updateTransactionsComment(transactionId: bigValue, comment: "comm e n t c o mmentcommentcommentcomment")
        
        let saveTransaction___ = Database.shared.transactionBy(transactionId: bigValue)!
        XCTAssertEqual(saveTransaction___.customerId, 1234554321, "")
        XCTAssertEqual(saveTransaction___.supplierId, 1234664321, "")
        XCTAssertEqual(saveTransaction___.orderId, 100004321, "")
        XCTAssertEqual(saveTransaction___.comment, "comm e n t c o mmentcommentcommentcomment", "")

    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
