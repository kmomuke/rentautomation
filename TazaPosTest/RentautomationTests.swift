//
//  RentautomationTests.swift
//  RentautomationTests
//
//  Created by kanybek on 4/19/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import XCTest
@testable import TazaPos

class RentautomationTests: XCTestCase {
    
    let bigValue = Database.shared.bigValue
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        Database.shared.deleteCustomer(bigValue)
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let customer = Customer(customerId: bigValue,
                                customerImagePath: "customerImagePath",
                                firstName: "Marat",
                                secondName: "Isabaev",
                                phoneNumber: "+996(556) 34-34-34",
                                address: "Bishkek",
                                userId: 321)
        
        Database.shared.createCustomers([customer])
        
        
        let savedCustomer_ = Database.shared.customerBy(customerId: bigValue)!
        
        XCTAssertNotNil(savedCustomer_)
        
        XCTAssertEqual(1 + 1, 2, "one plus one should equal two")
        XCTAssertEqual(savedCustomer_.customerId, bigValue, " customerId ")
        XCTAssertEqual(savedCustomer_.customerImagePath, "customerImagePath", "customerImagePath")
        XCTAssertEqual(savedCustomer_.firstName, "Marat", "firstName")
        XCTAssertEqual(savedCustomer_.secondName, "Isabaev", "secondName")
        XCTAssertEqual(savedCustomer_.phoneNumber, "+996(556) 34-34-34", "phoneNumber")
        XCTAssertEqual(savedCustomer_.address, "Bishkek", "address")
        XCTAssertEqual(savedCustomer_.userId, 321, "staffId")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
