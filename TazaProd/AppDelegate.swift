//
//  AppDelegate.swift
//  TazaProd
//
//  Created by kanybek on 9/2/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

import UIKit
import SVProgressHUD
import Kingfisher
import Whisper

let kScanditBarcodeScannerAppKey = "DVTqsEFFEeSDMNlzEgHjjkxXeU3NHtBMlm1tV0TMd3o";

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    
    var window: UIWindow?
    var passcodeWindow: UIWindow?
    var splitViewController: UISplitViewController?
    var navController: UINavigationController?
    var rootNavController: UINavigationController?
    var currentPasscodePresenter: PasscodeLockPresenter?
    
    private func createPasscodeLockPresenter() -> PasscodeLockPresenter {
        
        let configuration = PasscodeLockConfiguration()
        let passcodeLockVC = PasscodeLockViewController(state: .enter, configuration: configuration)
        
        passcodeLockVC.dismissCompletionCallback = { [weak self] in
            print("dismissCompletionCallback")
            self?.currentPasscodePresenter?.dismiss()
            self?.currentPasscodePresenter = nil
            self?.passcodeWindow = nil
        }
        
        //passcodeLockVC.logOutCallback = { [weak self] in
        //    print("logOutCallback")
        //    self?.gotoAuthorization()
        //}
        
        passcodeLockVC.successCallback = { (_ lock: PasscodeLockType) in
            print("successCallback")
        }
        
        self.passcodeWindow = UIWindow(frame:UIScreen.main.bounds)
        let presenter = PasscodeLockPresenter(mainWindow: self.passcodeWindow, configuration: configuration, viewController: passcodeLockVC)
        
        return presenter
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        self.window = UIWindow(frame:UIScreen.main.bounds)
        
        SBSLicense.setAppKey(kScanditBarcodeScannerAppKey)
        
        self.setUpHud()
        
        if let _ = AppSession.shared.currentEmployee {
            
            self.setUpHud()
            self.setUpKingfisherCache()
            
            self.splitViewController =  UISplitViewController()
            
            let rootUsersListVC = MainProddVC(initialString: "mainScreen")
            let emptyVC = EmptyVC(nibName: "EmptyVC", bundle: nil)
            
            self.rootNavController = UINavigationController(rootViewController:rootUsersListVC)
            let detailNavigationController = RNNavigationController(rootViewController:emptyVC)
            self.splitViewController?.viewControllers = [self.rootNavController!,detailNavigationController]
            
            //let settingsVC = SettingsListVC(initialString: "settings")
            //let settingsNC = RNNavigationController(rootViewController:settingsVC)
            //let tabBarController = UITabBarController()
            //tabBarController.viewControllers = [self.rootNavController!, settingsNC]
            //self.splitViewController?.viewControllers = [tabBarController, detailNavigationController]
            
            self.window!.rootViewController = self.splitViewController
        } else {
            
            let loginViewController = loginVC(initialItem: nil)
            self.navController = UINavigationController(rootViewController: loginViewController)
            self.window!.rootViewController = self.navController
        }
        
        self.window?.makeKeyAndVisible()
        
        return true
    }
    
    func enterMainApplication() -> Void {
        //self.setUpHud()
        self.setUpKingfisherCache()
        self.splitViewController =  UISplitViewController()
        
        let rootUsersListVC = MainProddVC(initialString: "mainScreen")
        let emptyVC = EmptyVC(nibName: "EmptyVC", bundle: nil)
        
        self.rootNavController = UINavigationController(rootViewController:rootUsersListVC)
        //let activeUsersListVC = ActiveUsersListVC()
        //let activeUsersListNC = UINavigationController(rootViewController:activeUsersListVC)
        
        
        ReachabilityManager.shared.startMonitoringNetworkReachability()
        RPCNetwork.shared.openStreamingConnection()
        
        
        let detailNavigationController = RNNavigationController(rootViewController:emptyVC)
        self.splitViewController?.viewControllers = [self.rootNavController!,detailNavigationController]
        
        
        self.window!.rootViewController = self.splitViewController!
    }
    
    func enterLoginApplication() -> Void {
        let loginViewController = loginVC(initialItem: nil)
        self.navController = UINavigationController(rootViewController: loginViewController)
        self.window!.rootViewController = self.navController
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        if let _ = AppSession.shared.currentEmployee {
            ReachabilityManager.shared.stopMonitoringNetworkReachability()
            RPCNetwork.shared.stopStreamingConnection()
        }
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        if let _ = AppSession.shared.currentEmployee {
            ReachabilityManager.shared.stopMonitoringNetworkReachability()
            RPCNetwork.shared.stopStreamingConnection()
        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        if let _ = AppSession.shared.currentEmployee {
            ReachabilityManager.shared.startMonitoringNetworkReachability()
            RPCNetwork.shared.openStreamingConnection()
        }
        
        //AppSession.shared.restartSocketSession()
        
        UIApplication.shared.keyWindow?.endEditing(true)
        let passcodePresenter = self.createPasscodeLockPresenter()
        self.currentPasscodePresenter = passcodePresenter
        passcodePresenter.present()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
    }
    
    private func setUpKingfisherCache() -> Void {
        let maxDigits = AppSettings.shared.maximumFractionDigits
        BKCurrencyTextField.setIdGen(Int32(maxDigits))
        
        ImageCache.default.maxCachePeriodInSecond = TimeInterval(60 * 60 * 24 * 365 * 2) //Cache exists for 2 year
    }
    
    private func setUpHud() -> Void {
        Whisper.Config.modifyInset = false
        SVProgressHUD.setMinimumDismissTimeInterval(1.6)
        SVProgressHUD.setMaximumDismissTimeInterval(1.6)
        SVProgressHUD.setDefaultStyle(.dark)
        SVProgressHUD.setDefaultMaskType(.none)
        SVProgressHUD.setDefaultAnimationType(.native)
        //SVProgressHUD.setDefaultMaskType(.clear)
    }
}

