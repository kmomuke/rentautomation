//
//  DevStatisticTypesVC.swift
//  Rentautomation
//
//  Created by kanybek on 9/10/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class DevStatisticTypesVC: FormViewController {
    
    var initialString: String?
    
    convenience init() {
        self.init(initialString: nil)
    }
    
    init(initialString: String?) {
        self.initialString = initialString
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Отчеты"
        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
    }
    
    // MARK: Private
    private func configure() {
        
        let backBarButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backBarButton
        
        // Create RowFormers
        let createMenu: ((String, UIColor, (() -> Void)?) -> RowFormer) = { text, color, onSelected in
            return LabelRowFormer<FormLabelCell>() {
                $0.titleLabel.textColor = color
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                }.configure {
                    $0.text = text
                    $0.rowHeight = 70
                }.onSelected { _ in
                    onSelected?()
            }
        }
        
        let goneToProdColor = Order.orderDocumentTypeColor(orderDocType: .rawProductGoneToProd)
        let customerPreOrderTypesRow = createMenu("Расход сырья", goneToProdColor) { [weak self] in
            let statisticsVC = MostSaledProductsVC(initialOrderDocumentType: .rawProductGoneToProd)
            let statisticsNC = RNNavigationController(rootViewController:statisticsVC)
            self?.splitViewController?.showDetailViewController(statisticsNC, sender: self)
        }
        
        let receiveColor = Order.orderDocumentTypeColor(orderDocType: .readyProductReceivedFromProd)
        let supplierIncomeRow = createMenu("Выход готовой продукции", receiveColor) { [weak self] in
            let statisticsVC = MostSaledProductsVC(initialOrderDocumentType: .readyProductReceivedFromProd)
            let statisticsNC = RNNavigationController(rootViewController:statisticsVC)
            self?.splitViewController?.showDetailViewController(statisticsNC, sender: self)
        }
        
        // Create Headers and Footers
        let createHeader: ((String) -> ViewFormer) = { text in
            return LabelViewFormer<FormLabelHeaderView>()
                .configure {
                    $0.text = text
                    $0.viewHeight = 40
            }
        }
        
        let realExampleSection1 = SectionFormer(rowFormer: customerPreOrderTypesRow, supplierIncomeRow)
            .set(headerViewFormer: createHeader(" "))
        
        former.append(sectionFormer: realExampleSection1)
    }
}
