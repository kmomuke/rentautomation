//
//  OrderHistoryTypesVC.swift
//  Rentautomation
//
//  Created by kanybek on 9/10/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class OrderHistoryTypesVC: FormViewController {
    
    var initialString: String?
    
    convenience init() {
        self.init(initialString: nil)
    }
    
    init(initialString: String?) {
        self.initialString = initialString
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    deinit {
    }
    
    // MARK: Public
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "История"
        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
    }
    
    // MARK: Private
    private func configure() {
        
        let backBarButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backBarButton
        
        // Create RowFormers        
        let createMenu: ((String, UIColor, (() -> Void)?) -> RowFormer) = { text, color, onSelected in
            return LabelRowFormer<FormLabelCell>() {
                $0.titleLabel.textColor = color
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                }.configure {
                    $0.text = text
                    $0.rowHeight = 70
                }.onSelected { _ in
                    onSelected?()
            }
        }
        
        let allDocumentTypesRow = createMenu("Все операции", .formerColor()) { [weak self] in
            let orderListVC = OrdersListVC(initialOrderDocumentType: .none)
            let productListNC = RNNavigationController(rootViewController:orderListVC)
            self?.splitViewController?.showDetailViewController(productListNC, sender: self)
        }
        
        let goneToProdColor = Order.orderDocumentTypeColor(orderDocType: .rawProductGoneToProd)
        let saleDocumentTypesTRow = createMenu("Расход сырья", goneToProdColor) { [weak self] in
            let orderListVC = OrdersListVC(initialOrderDocumentType: .rawProductGoneToProd)
            let productListNC = RNNavigationController(rootViewController:orderListVC)
            self?.splitViewController?.showDetailViewController(productListNC, sender: self)
        }
        
        let receiveFromProdColor = Order.orderDocumentTypeColor(orderDocType: .readyProductReceivedFromProd)
        let customerPreOrderTypesRow = createMenu("Выход готовой продукции", receiveFromProdColor) { [weak self] in
            let orderListVC = OrdersListVC(initialOrderDocumentType: .readyProductReceivedFromProd)
            let productListNC = RNNavigationController(rootViewController:orderListVC)
            self?.splitViewController?.showDetailViewController(productListNC, sender: self)
        }
        
        // Create Headers and Footers
        let createHeader: ((String) -> ViewFormer) = { text in
            return LabelViewFormer<FormLabelHeaderView>()
                .configure {
                    $0.text = text
                    $0.viewHeight = 40
            }
        }
        
        let createFooter: ((String) -> ViewFormer) = { text in
            return LabelViewFormer<FormLabelFooterView>()
                .configure {
                    $0.text = text
                    $0.viewHeight = 100
            }
        }
        
        let realExampleSection1 = SectionFormer(rowFormer: allDocumentTypesRow, saleDocumentTypesTRow, customerPreOrderTypesRow)
            .set(headerViewFormer: createHeader("История"))
        former.append(sectionFormer: realExampleSection1)
    }
}
