//
//  UITableViewCell+Reuse.swift
//  Rentautomation
//
//  Created by kanybek on 6/18/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
protocol ReusableView: class {}

extension ReusableView where Self: UIView {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

extension UITableViewCell: ReusableView {
}


protocol NibLoadableView: class { }

extension NibLoadableView where Self: UIView {
    static var NibName: String {
        return String(describing: self)
    }
}

extension UITableViewCell: NibLoadableView {
}


extension UITableView {
    func register<T: UITableViewCell>(_: T.Type) where T: ReusableView, T: NibLoadableView {
        let nib = UINib(nibName: T.NibName, bundle: nil)
        register(nib, forCellReuseIdentifier: T.reuseIdentifier)
    }
}
