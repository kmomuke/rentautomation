//
//  String+ToDoubleValue.swift
//  Rentautomation
//
//  Created by kanybek on 1/17/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit


extension String {
    
    func getDoubleFromString() -> Double?
    {
        let inputString = self
        //print("inputString ===> \(inputString)")
        if let number = Global.shared.amountFormatter.number(from: inputString) {
            return number.doubleValue
        }
        return nil
    }
    
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
}
