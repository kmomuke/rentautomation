//
//  Version+Bundle.swift
//  Rentautomation
//
//  Created by kanybek on 2/16/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

extension Bundle {
    var releaseVersionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    var buildVersionNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
}
