//
//  RASegmentedControl.swift
//  Rentautomation
//
//  Created by kanybek on 4/9/17.
//  Copyright © 2017 kanybek. All rights reserved.
//
import UIKit

@IBDesignable class RASegmentedControl: UISegmentedControl {
    
    @IBInspectable var height: CGFloat = 40 {
        didSet {
            let centerSave = center
            frame = CGRect(x: frame.minX, y: frame.minY, width: frame.width, height: height)
            center = centerSave
        }
    }
}
