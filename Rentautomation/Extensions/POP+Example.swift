//
//  POP+Example.swift
//  Rentautomation
//
//  Created by kanybek on 6/18/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

protocol Buzzable {
}

extension Buzzable where Self: UIView {
    func buzz() {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.05
        animation.repeatCount = 5
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 5.0, y: self.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 5.0, y: self.center.y))
        layer.add(animation, forKey: "position")
    }
}

protocol Poppable {
}

extension Poppable where Self: UIView {
    func pop() {
        UIView.animate(withDuration: 0.3,
                       delay: 0,
                       options: .curveEaseIn,
                       animations: { self.alpha = 1.0 }) { (animationCompleted) in
                        if animationCompleted == true {
                            UIView.animate(withDuration: 0.3,
                                           delay: 2.0,
                                           options: .curveEaseOut,
                                           animations: { self.alpha = 0.0 },
                                           completion: nil)
                        }
        }
    }
}


class BuzzableTextField: UITextField, Buzzable {}
class BuzzableButton: UIButton, Buzzable {}
class BuzzableImageView: UIImageView, Buzzable {}
class BuzzablePoppableLabel: UILabel, Buzzable, Poppable {}

class LoginViewController: UIViewController {
    
    @IBOutlet weak var passcodTextField: BuzzableTextField!
    @IBOutlet weak var loginButton: BuzzableButton!
    @IBOutlet weak var errorMessageLabel: BuzzablePoppableLabel!
    @IBOutlet weak var profileImageView: BuzzableImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpCornerRadius()
    }
    
    @IBAction func didTabLoginButton(_ sender: UIButton) {
        passcodTextField.buzz()
        loginButton.buzz()
        errorMessageLabel.buzz()
        errorMessageLabel.pop()
        profileImageView.buzz()
    }
    
    func setUpCornerRadius() {
        let imageHeight = profileImageView.frame.height/2
        profileImageView.layer.cornerRadius = imageHeight
        profileImageView.layer.masksToBounds = true
    }
}


protocol MyProtocol {
    func methodOnProtocol()
}
extension MyProtocol {
    func methodOnProtocol() {
        print("Default Implementation of \(#function)")
    }
    func newMethodOnExtension() {
        print("Default Implementation of \(#function)")
    }
}

protocol Runnable {
    var groundSpeed: Double { get }
    func run() -> String
}
extension Runnable {
    func run() -> String {
        return "Running at \(groundSpeed) mph"
    }
}

protocol Swimmable {
    var waterSpeed: Double { get }
    func swim() -> String
}
extension Swimmable {
    func swim() -> String {
        return "Swimming at \(waterSpeed) mph"
    }
}

protocol Flyable {
    var airSpeed: Double { get }
    func fly() -> String
}

extension Flyable {
    func fly() -> String {
        return "Flying at \(airSpeed) mph"
    }
}

class Penguin: Runnable, Swimmable {
    var groundSpeed = 2.0
    var waterSpeed = 25.0
}

func someMethod2222() -> Void {
    let p = Penguin()
    p.run()
    p.swim()
}
