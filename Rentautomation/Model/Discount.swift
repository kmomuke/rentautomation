//
//  Discount.swift
//  Rentautomation
//
//  Created by kanybek on 10/3/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

final class Discount {
    
    var totalPrice: Double
    var discount: Double
    var minusPrice: Double
    var plusPrice: Double
    var comment: String
    
    convenience init() {
        self.init(totalPrice: 0.0,
                  discount: 0.0,
                  minusPrice: 0.0,
                  plusPrice: 0.0,
                  comment: "")
    }
    
    init(totalPrice: Double,
         discount: Double,
         minusPrice: Double,
         plusPrice: Double,
         comment: String)
    {
        self.totalPrice = totalPrice
        self.discount = discount
        self.minusPrice = minusPrice
        self.plusPrice = plusPrice
        self.comment = comment
    }
    
    init(payment: Payment)
    {
        self.totalPrice = payment.totalOrderPrice
        self.discount = payment.discount
        self.minusPrice = payment.minusPrice
        self.plusPrice = payment.plusPrice
        self.comment = payment.comment
    }
    
    deinit {
    }
    
    func totalPriceWithDiscount() -> Double {
        let totalPriceWithDiscount: Double = self.totalPrice * (1 - self.discount/100) - minusPrice + plusPrice
        return totalPriceWithDiscount
    }
    
    func finlaDifferenceValue() -> Double {
        return (self.totalPrice - self.totalPriceWithDiscount())
    }
    
}
