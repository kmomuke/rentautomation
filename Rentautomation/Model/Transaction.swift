//
//  Transaction.swift
//  Rentautomation
//
//  Created by kanybek on 12/14/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import FMDB

// Transaction related only to Customer and Supplier
// Registers all money relation ships

// Transaction types: {

    // Employee modified initial balance of customer
    // Employee sell product to customer
    // Employee received money from customer
    // Employee modified saled order, correction affected to customer


    // Employee modified initial balance of supplier
    // Employee gave money to supplier
    // Employee received money from supplier
    // Employee modified received order from supplier, correction affected to supplier
//}

public enum TransactionType : UInt {
    
    case none = 0
    
    case customerBalanceModified = 11000
    case customerReceivedProduct = 22000
    case customerGaveMoneyForProduct = 33000 //owner received money from customer
    case customerReturnedProduct = 35000 //customer returned product
    case ownerReturnedMoneyForProductToCustomer = 40000 //owner retruned received money to customer // ougone money
    case customerBalanceAfterOrderCorrection = 44000
    
    case supplierBalanceModified = 55000
    case supplierGaveProduct = 66000
    case supplierReceivedMoneyForProduct = 77000 // money outgone
    case ownerReturnedProductToSupplier = 80000 // return product
    case supplierGaveMoneyForRetrunProduct = 85000 // money income, onwer returned product to supplier
    case supplierBalanceAfterOrderCorrection = 88000
}

//open class TransactionRequest : GPBMessage {
//    open var transactionId: UInt64
//    open var transactionDate: UInt64
//    open var isLastTransaction: UInt32
//    open var transactionType: UInt32
//    open var moneyAmount: Double
//    open var orderId: UInt64
//    open var customerId: UInt64
//    open var supplierId: UInt64
//    open var userId: UInt64
//    open var comment: String!
//    open var transactionUuid: String!
//    open var ballanceAmount: Double
//}

class Transaction {

    var transactionId: UInt64
    var transactionDate: Date
    var isLastTransaction: Bool
    var transactionType: TransactionType
    var moneyAmount: Double
    var orderId: UInt64
    var customerId: UInt64
    var supplierId: UInt64
    var userId: UInt64
    var comment: String
    var transactionUuid: String
    var ballanceAmount: Double
    
    convenience init() {
        self.init(transactionId: 0,
                  transactionDate: Date(),
                  isLastTransaction: false,
                  transactionType: .none,
                  moneyAmount: 0.0,
                  orderId: 0,
                  customerId: 0,
                  supplierId: 0,
                  userId: 0,
                  ballanceAmount: 0.0,
                  comment: "",
                  transactionUuid: UUID().uuidString.lowercased())
    }
    
    init(transactionId: UInt64,
         transactionDate: Date,
         isLastTransaction: Bool,
         transactionType: TransactionType,
         moneyAmount: Double,
         orderId: UInt64,
         customerId: UInt64,
         supplierId: UInt64,
         userId: UInt64,
         ballanceAmount: Double,
         comment: String,
         transactionUuid: String) {
        
        self.transactionId = transactionId
        self.transactionDate = transactionDate
        self.isLastTransaction = isLastTransaction
        self.transactionType = transactionType
        self.moneyAmount = moneyAmount
        self.orderId = orderId
        self.customerId = customerId
        self.supplierId = supplierId
        self.userId = userId
        self.ballanceAmount = ballanceAmount
        self.comment = comment
        self.transactionUuid = transactionUuid
    }
    
    deinit {
    }
    
    var description: String {
        return "___"
    }
    
    //try self.database.executeUpdate("CREATE TABLE IF NOT EXISTS \(self.transactionsTableName) (transaction_id INTEGER PRIMARY KEY, transaction_date INTEGER, is_last_transaction INTEGER, transaction_type INTEGER, money_amount REAL, order_id INTEGER, customer_id INTEGER, supplier_id INTEGER, user_id INTEGER, ballance_amount REAL, comment TEXT, uuid TEXT)", values: nil)
    
    func databaseInsertValues() -> [Any] {
        
        let transaction_date = UInt64(self.transactionDate.timeIntervalSince1970 * 1000)
        var isLastTransactionValue: Int8 = 0
        if self.isLastTransaction {
            isLastTransactionValue = 1
        }
        
        let transactionTypeUInt = Transaction.integerTransactionTypeFrom(transactionType: self.transactionType)
        
        return [self.transactionId,
                transaction_date,
                isLastTransactionValue,
                transactionTypeUInt,
                self.moneyAmount,
                self.orderId,
                self.customerId,
                self.supplierId,
                self.userId,
                self.ballanceAmount,
                self.comment,
                self.transactionUuid]
    }
    
    static func transactionFrom(queryResult: FMResultSet) -> Transaction {
        
        let transactionId = queryResult.unsignedLongLongInt(forColumn: "transaction_id")
        
        let transactionDateInt   = queryResult.unsignedLongLongInt(forColumn: "transaction_date")
        let transactionDate = Date(timeIntervalSince1970: Double(transactionDateInt/1000))
        
        var isLastTransaction_: Bool = false
        let isLastTransactionValueInt = queryResult.int(forColumn: "is_last_transaction")
        if isLastTransactionValueInt == 1 {
            isLastTransaction_ = true
        }
        
        let moneyAmount = queryResult.double(forColumn: "money_amount")
        let ballanceAmount = queryResult.double(forColumn: "ballance_amount")
        
        let transactionTypeUInt   = queryResult.unsignedLongLongInt(forColumn: "transaction_type")
        let transactionType = Transaction.transactionTypeFromInteger(transactionTypeUInt: transactionTypeUInt)
        
        let orderId = queryResult.unsignedLongLongInt(forColumn: "order_id")
        let customerId = queryResult.unsignedLongLongInt(forColumn: "customer_id")
        let supplierId = queryResult.unsignedLongLongInt(forColumn: "supplier_id")
        let userId = queryResult.unsignedLongLongInt(forColumn: "user_id")
        let comment = queryResult.string(forColumn: "comment")
        let transactionUuid = queryResult.string(forColumn: "uuid")
        
        
        let transaction = Transaction(transactionId: transactionId,
                                      transactionDate: transactionDate,
                                      isLastTransaction: isLastTransaction_,
                                      transactionType: transactionType,
                                      moneyAmount: moneyAmount,
                                      orderId: orderId,
                                      customerId: customerId,
                                      supplierId: supplierId,
                                      userId: userId,
                                      ballanceAmount: ballanceAmount,
                                      comment: comment!,
                                      transactionUuid: transactionUuid!)
        
        return transaction
    }
    
    
    static func transactionTypeStringName(transactionType: TransactionType) -> String {
        
        var transactionStringName: String = ""
        
        switch transactionType {
            
        case .none:
            transactionStringName = "Пусто"
            
        case .customerBalanceModified:
            transactionStringName = "Баланс клиента изменен"
        case .customerReceivedProduct:
            transactionStringName = "Клиент получил продукт"
        case .customerGaveMoneyForProduct:
            transactionStringName = "Клиент дал денег за продукт"
        case .customerReturnedProduct:
            transactionStringName = "Клиент сделал возврат продукта"
        case .ownerReturnedMoneyForProductToCustomer:
            transactionStringName = "Агент вернул деньги клиенту за возрат"
        case .customerBalanceAfterOrderCorrection:
            transactionStringName = "Баланс клиента изменен из за изменении документа"
            
        case .supplierBalanceModified:
            transactionStringName = "Баланс поставщика изменен"
        case .supplierGaveProduct:
            transactionStringName = "Агент получил продукт от поставщика"
        case .supplierReceivedMoneyForProduct:
            transactionStringName = "Поставщик получил деньги за поставленный продукт"
        case .ownerReturnedProductToSupplier:
            transactionStringName = "Агент вернул продукт поставщику"
        case .supplierGaveMoneyForRetrunProduct:
            transactionStringName = "Поставщик дал денег за возврат продукта"
        case .supplierBalanceAfterOrderCorrection:
            transactionStringName = "Баланс поставщика изменен из за изменении документа"
            
        default:
            transactionStringName = "Пусто"
        }
        return transactionStringName
    }
    
    
    static func integerTransactionTypeFrom(transactionType: TransactionType) -> UInt32 {
        
        var transactionTypeUInt: UInt32 = 0
        
        switch transactionType {
            
        case .none:
            transactionTypeUInt = 0
            
        case .customerBalanceModified:
            transactionTypeUInt = 11000
        case .customerReceivedProduct:
            transactionTypeUInt = 22000
        case .customerGaveMoneyForProduct:
            transactionTypeUInt = 33000
        case .customerReturnedProduct:
            transactionTypeUInt = 35000
        case .ownerReturnedMoneyForProductToCustomer:
            transactionTypeUInt = 40000
        case .customerBalanceAfterOrderCorrection:
            transactionTypeUInt = 44000
            
        case .supplierBalanceModified:
            transactionTypeUInt = 55000
        case .supplierGaveProduct:
            transactionTypeUInt = 66000
        case .supplierReceivedMoneyForProduct:
            transactionTypeUInt = 77000
        case .ownerReturnedProductToSupplier:
            transactionTypeUInt = 80000
        case .supplierGaveMoneyForRetrunProduct:
            transactionTypeUInt = 85000
        case .supplierBalanceAfterOrderCorrection:
            transactionTypeUInt = 88000
            
        default:
            transactionTypeUInt = 0
        }
        
        return transactionTypeUInt
    }
    
    static func transactionTypeFromInteger(transactionTypeUInt: UInt64) -> TransactionType {
        
        var transactionType: TransactionType = .none
        
        if transactionTypeUInt == 0 {
            transactionType = .none
        } else if transactionTypeUInt == 11000 {
            transactionType = .customerBalanceModified
        } else if transactionTypeUInt == 22000 {
            transactionType = .customerReceivedProduct
        } else if transactionTypeUInt == 33000 {
            transactionType = .customerGaveMoneyForProduct
        } else if transactionTypeUInt == 35000 {
            transactionType = .customerReturnedProduct
        } else if transactionTypeUInt == 40000 {
            transactionType = .ownerReturnedMoneyForProductToCustomer
        } else if transactionTypeUInt == 44000 {
            transactionType = .customerBalanceAfterOrderCorrection
        } else if transactionTypeUInt == 55000 {
            transactionType = .supplierBalanceModified
        } else if transactionTypeUInt == 66000 {
            transactionType = .supplierGaveProduct
        } else if transactionTypeUInt == 77000 {
            transactionType = .supplierReceivedMoneyForProduct
        } else if transactionTypeUInt == 80000 {
            transactionType = .ownerReturnedProductToSupplier
        } else if transactionTypeUInt == 85000 {
            transactionType = .supplierGaveMoneyForRetrunProduct
        } else if transactionTypeUInt == 88000 {
            transactionType = .supplierBalanceAfterOrderCorrection
        }
        
        return transactionType
    }
    
    static func transactionTypeColor(transactionType: TransactionType) -> UIColor {
        
        var transactionColor: UIColor = UIColor.midnightBlueColor()
        
        switch transactionType {
            
        case .none:
            transactionColor = UIColor.midnightBlueColor()
            
        case .customerBalanceModified:
            //"Баланс клиента изменен"
            transactionColor = UIColor.pomergranateColor()
        case .customerReceivedProduct:
            //transactionColor = "Клиент получил продукт"
            transactionColor = UIColor.peterRiverColor()
        case .customerGaveMoneyForProduct:
            //transactionColor = "Клиент дал денег за продукт"
            transactionColor = UIColor.emeraldColor()
        case .customerReturnedProduct:
            //transactionColor = "Клиент сделал возврат за продукт"
            transactionColor = UIColor.pomergranateColor()
        case .ownerReturnedMoneyForProductToCustomer:
            //transactionColor = "Агент вернул деньги клиенту за возрат"
            transactionColor = UIColor.carrotColor()
        case .customerBalanceAfterOrderCorrection:
            //transactionColor = "Балан клиента изменен из за изменении документа"
            transactionColor = UIColor.orange
        case .supplierBalanceModified:
            //transactionColor = "Балан поставщика изменен"
            transactionColor = UIColor.pomergranateColor()
        case .supplierGaveProduct:
            //transactionColor = "Агент получил продукт от поставщика"
            transactionColor = UIColor.belizeHoleColor()
        case .supplierReceivedMoneyForProduct:
            //transactionColor = "Поставщик получил деньги за поставленный продукт"
            transactionColor = UIColor.greenSeaColor()
        case .ownerReturnedProductToSupplier:
            //transactionColor = "Агент вернул продукт поставщику"
            transactionColor = UIColor.pumpkinColor()
        case .supplierGaveMoneyForRetrunProduct:
            //transactionColor = "Поставщик дал денег за возврат продукта"
            transactionColor = UIColor.alizarinColor()
        case .supplierBalanceAfterOrderCorrection:
            //transactionColor = "Балан поставщика изменен из за изменении документа"
            transactionColor = UIColor.orange
            
        default:
            transactionColor = UIColor.midnightBlueColor()
        }
        return transactionColor
    }
}
