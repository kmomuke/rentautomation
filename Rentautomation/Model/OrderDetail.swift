//
//  OrderDetail.swift
//  Rentautomation
//
//  Created by kanybek on 11/28/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import FMDB

//open class OrderDetailRequest : GPBMessage {
//    open var orderDetailId: UInt64
//    open var orderId: UInt64
//    open var orderDetailDate: UInt64
//    open var isLast: UInt32
//    open var productId: UInt64
//    open var billingNo: String!
//    open var orderDetailComment: String!
//    open var price: Double
//    open var orderQuantity: Double
//    open var orderDetailUuid: String!
//    open var productQuantity: Double
//}

class OrderDetail: CustomStringConvertible {
 
    var orderDetailId: UInt64
    var orderId: UInt64
    var orderDetailDate: Date
    var isLast: Bool
    var productId: UInt64
    var billingNo: String
    var orderDetailComment: String
    var price: Double
    var orderQuantity: Double
    var orderDetailUuid: String
    var productQuantity: Double
    
    convenience init() {
        self.init(orderDetailId: 0,
                  orderId: 0,
                  orderDetailDate: Date(),
                  isLast: false,
                  billingNo: "",
                  orderDetailComment: "",
                  orderDetailUuid: UUID().uuidString.lowercased(),
                  productId: 0,
                  price: 0.0,
                  orderQuantity: 0.0,
                  productQuantity: 0.0)
    }
    
    init(orderDetailId: UInt64,
         orderId: UInt64,
         orderDetailDate: Date,
         isLast:Bool,
         billingNo: String,
         orderDetailComment: String,
         orderDetailUuid: String,
         productId: UInt64,
         price: Double,
         orderQuantity: Double,
         productQuantity: Double) {
        
        self.orderDetailId = orderDetailId
        self.orderId = orderId
        self.orderDetailDate = orderDetailDate
        self.isLast = isLast
        self.billingNo = billingNo
        self.orderDetailComment = orderDetailComment
        self.orderDetailUuid = orderDetailUuid
        self.productId = productId
        self.price = price
        self.orderQuantity = orderQuantity
        self.productQuantity = productQuantity
    }
    
    deinit {
    }
    
    var description: String {
        return "___"
    }
    
    //try self.database.executeUpdate("CREATE TABLE IF NOT EXISTS \(self.orderDetailsTableName) (order_detail_id INTEGER PRIMARY KEY, order_id INTEGER, order_detail_date INTEGER, is_last INTEGER, product_id INTEGER, billing_no TEXT, comment TEXT, uuid TEXT, price REAL, order_quantity REAL, product_quantity REAL)", values: nil)
    
    func databaseInsertValues() -> [Any] {
        let orderDetailDateUInt = UInt64(self.orderDetailDate.timeIntervalSince1970 * 1000)
        
        var isLastValue: Int8 = 0
        if self.isLast {
            isLastValue = 1
        }
        
        return [self.orderDetailId,
                self.orderId,
                orderDetailDateUInt,
                isLastValue,
                self.billingNo,
                self.orderDetailComment,
                self.orderDetailUuid,
                self.productId,
                self.price,
                self.orderQuantity,
                self.productQuantity]
    }
    
    static func orderDetailFrom(queryResult: FMResultSet) -> OrderDetail {
        
        let orderDetailId = queryResult.unsignedLongLongInt(forColumn: "order_detail_id")
        let orderId = queryResult.unsignedLongLongInt(forColumn: "order_id")
        
        let orderDetailDateUInt   = queryResult.unsignedLongLongInt(forColumn: "order_detail_date")
        let orderDetailDate = Date(timeIntervalSince1970: Double(orderDetailDateUInt/1000))
        
        var isLast_: Bool = false
        let isLastValueInt = queryResult.int(forColumn: "is_last")
        if isLastValueInt == 1 {
            isLast_ = true
        }
        
        let billingNo = queryResult.string(forColumn: "billing_no")
        let orderDetailComment = queryResult.string(forColumn: "comment")
        let orderDetailUuid = queryResult.string(forColumn: "uuid")
        
        let productId = queryResult.unsignedLongLongInt(forColumn: "product_id")
        let price = queryResult.double(forColumn: "price")
        let orderQuantity = queryResult.double(forColumn: "order_quantity")
        let productQuantity = queryResult.double(forColumn: "product_quantity")
        
        let orderDetail = OrderDetail(orderDetailId: orderDetailId,
                                      orderId: orderId,
                                      orderDetailDate: orderDetailDate,
                                      isLast: isLast_,
                                      billingNo: billingNo!,
                                      orderDetailComment: orderDetailComment!,
                                      orderDetailUuid: orderDetailUuid!,
                                      productId: productId,
                                      price: price,
                                      orderQuantity: orderQuantity,
                                      productQuantity: productQuantity)
        return orderDetail
    }
}
