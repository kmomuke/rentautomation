//
//  Role.swift
//  Rentautomation
//
//  Created by kanybek on 11/28/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import FMDB

class Role: CustomStringConvertible {
    
    var roleId: UInt64
    var roleName: String
    
    convenience init() {
        self.init(roleId: 0, roleName: "")
    }
    
    init(roleId: UInt64, roleName: String) {
        self.roleId = roleId
        self.roleName = roleName
    }
    
    deinit {
    }
    
    var description: String {
        return "___"
    }
    
    //try self.database.executeUpdate("CREATE TABLE IF NOT EXISTS \(self.rolesTableName) (role_id INTEGER PRIMARY KEY, role_name TEXT)", values: nil)
    
    static func roleFrom(queryResult: FMResultSet) -> Role {
        
        let roleId = queryResult.unsignedLongLongInt(forColumn: "role_id")
        let roleName   = queryResult.string(forColumn: "role_name")
        let role = Role(roleId: roleId, roleName: roleName!)
        return role
    }
}
