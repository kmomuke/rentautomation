//
//  Account.swift
//  Rentautomation
//
//  Created by kanybek on 12/14/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import FMDB


//open class AccountRequest : GPBMessage {
//    open var accountId: UInt64
//    open var customerId: UInt64
//    open var supplierId: UInt64
//    open var balance: Double
//}

class Account {

    var accountId: UInt64
    var customerId: UInt64
    var supplierId: UInt64
    
    var balance: Double
    
    convenience init() {
        self.init(accountId: 0, customerId: 0, supplierId: 0, balance: 0.0)
    }
    
    init(accountId: UInt64, customerId: UInt64, supplierId: UInt64, balance: Double) {
        self.accountId = accountId
        self.customerId = customerId
        self.supplierId = supplierId
        self.balance = balance
    }
    
    deinit {
    }
    
    var description: String {
        return "___"
    }
    
    //try self.database.executeUpdate("CREATE TABLE IF NOT EXISTS \(self.accountsTableName) (account_id INTEGER PRIMARY KEY, customer_id INTEGER, supplier_id INTEGER, balance REAL)", values: nil)
    
    func databaseInsertValues() -> [Any] {
        return [self.accountId, self.customerId, self.supplierId, self.balance]
    }
    
    static func accountFrom(queryResult: FMResultSet) -> Account {
        
        let accountId = queryResult.unsignedLongLongInt(forColumn: "account_id")
        let customerId = queryResult.unsignedLongLongInt(forColumn: "customer_id")
        let supplierId = queryResult.unsignedLongLongInt(forColumn: "supplier_id")
        let balance = queryResult.double(forColumn: "balance")
        
        let account = Account(accountId: accountId,
                              customerId: customerId,
                              supplierId: supplierId,
                              balance: balance)
        
        return account
    }
}
