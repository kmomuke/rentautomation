//
//  Payment.swift
//  Rentautomation
//
//  Created by kanybek on 11/28/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import FMDB

//open class PaymentRequest : GPBMessage {
//    open var paymentId: UInt64
//    open var totalOrderPrice: Double
//    open var discount: Double
//    open var totalPriceWithDiscount: Double
//}

class Payment: CustomStringConvertible {
 
    var paymentId: UInt64
    var totalOrderPrice: Double
    var discount: Double
    var totalPriceWithDiscount: Double
    var minusPrice: Double
    var plusPrice: Double
    var comment: String
    
    convenience init() {
        self.init(paymentId: 0,
                  totalOrderPrice: 0.0,
                  discount: 0.0,
                  totalPriceWithDiscount: 0.0,
                  minusPrice: 0.0,
                  plusPrice: 0.0,
                  comment: "")
    }
    
    init(paymentId: UInt64,
         totalOrderPrice: Double,
         discount: Double,
         totalPriceWithDiscount: Double,
         minusPrice: Double,
         plusPrice: Double,
         comment: String)
    {
        self.paymentId = paymentId
        self.totalOrderPrice = totalOrderPrice
        self.discount = discount
        self.totalPriceWithDiscount = totalPriceWithDiscount
        self.minusPrice = minusPrice
        self.plusPrice = plusPrice
        self.comment = comment
    }
    
    deinit {
    }
    
    public func setUpWith(discount: Discount) {
        self.totalOrderPrice = discount.totalPrice
        self.discount = discount.discount
        self.minusPrice = discount.minusPrice
        self.plusPrice = discount.plusPrice
        self.comment = discount.comment
        self.totalPriceWithDiscount = discount.totalPriceWithDiscount()
    }
    
    public func totalPriceWithDiscountF() -> Double {
        let totalPriceWithDiscount: Double = self.totalOrderPrice * (1 - self.discount/100) - minusPrice + plusPrice
        return totalPriceWithDiscount
    }
    
    var description: String {
        return "_\(self.paymentId)_\(self.totalOrderPrice)"
    }
    
    //try self.database.executeUpdate("CREATE TABLE IF NOT EXISTS \(self.paymentsTableName) (payment_id INTEGER PRIMARY KEY, total_order_price REAL, discount REAL, total_price_with_discount REAL, minus_price REAL, plus_price REAL, comment TEXT)", values: nil)
    
    func databaseInsertValues() -> [Any] {
        return [self.paymentId,
                self.totalOrderPrice,
                self.discount,
                self.totalPriceWithDiscount,
                self.minusPrice,
                self.plusPrice,
                self.comment]
    }
    
    static func paymentFrom(queryResult: FMResultSet) -> Payment {
        
        let paymentId = queryResult.unsignedLongLongInt(forColumn: "payment_id")
        let total_order_price = queryResult.double(forColumn: "total_order_price")
        let discount = queryResult.double(forColumn: "discount")
        let total_price_with_discount = queryResult.double(forColumn: "total_price_with_discount")
        
        let minus_price = queryResult.double(forColumn: "minus_price")
        let plus_price = queryResult.double(forColumn: "plus_price")
        let comment = queryResult.string(forColumn: "comment")
        var comment_: String = ""
        if let com__ = comment {
            comment_ = com__
        }
        
        let payment = Payment(paymentId: paymentId,
                              totalOrderPrice: total_order_price,
                              discount: discount,
                              totalPriceWithDiscount: total_price_with_discount,
                              minusPrice: minus_price,
                              plusPrice: plus_price,
                              comment: comment_)
        
        return payment
    }
}
