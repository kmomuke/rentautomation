//
//  Customer.swift
//  Rentautomation
//
//  Created by kanybek on 11/28/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import FMDB

//open class CustomerRequest : GPBMessage {
//    open var customerId: UInt64
//    open var customerImagePath: String!
//    open var firstName: String!
//    open var secondName: String!
//    open var phoneNumber: String!
//    open var address: String!
//    open var userId: UInt64
//}

class Customer: CustomStringConvertible {
    
    var customerId: UInt64
    var customerImagePath: String?
    
    var firstName: String
    var secondName: String
    
    var phoneNumber: String
    var address: String
    
    var userId: UInt64
    
    convenience init() {
        self.init(customerId: 0,
                  customerImagePath: "",
                  firstName: "",
                  secondName: "",
                  phoneNumber: "",
                  address: "",
                  userId: 0)
    }
    
    init(customerId: UInt64,
         customerImagePath: String?,
         firstName: String,
         secondName: String,
         phoneNumber: String,
         address: String,
         userId: UInt64) {
        self.customerId = customerId
        self.customerImagePath = customerImagePath
        self.firstName = firstName
        self.secondName = secondName
        self.phoneNumber = phoneNumber
        self.address = address
        self.userId = userId
    }
    
    deinit {
    }
    
    var description: String {
        return "_\(self.customerId)__\(self.firstName)"
    }
    
    //try self.database.executeUpdate("CREATE TABLE IF NOT EXISTS \(self.customersTableName) (customer_id INTEGER PRIMARY KEY, customer_image_path TEXT, first_name TEXT, second_name TEXT, phone_number TEXT, address TEXT, user_id INTEGER)", values: nil)
    
    func databaseInsertValues() -> [Any] {
        return [self.customerId,
                self.customerImagePath ?? "",
                self.firstName,
                self.secondName,
                self.phoneNumber,
                self.address,
                self.userId]
    }
    
    func databaseUpdateValues() -> [Any] {
        return [self.customerImagePath ?? "",
                self.firstName,
                self.secondName,
                self.phoneNumber,
                self.address,
                self.userId,
                self.customerId]
    }
    
    static func customerFrom(queryResult: FMResultSet) -> Customer {
        
        let customerId = queryResult.unsignedLongLongInt(forColumn: "customer_id")
        let customerImagePath = queryResult.string(forColumn: "customer_image_path")
        let firstName = queryResult.string(forColumn: "first_name")
        let secondName = queryResult.string(forColumn: "second_name")
        let phoneNumber = queryResult.string(forColumn: "phone_number")
        let address = queryResult.string(forColumn: "address")
        let userId = queryResult.unsignedLongLongInt(forColumn: "user_id")
        
        let customer = Customer(customerId: customerId,
                                customerImagePath: customerImagePath!,
                                firstName: firstName!,
                                secondName: secondName!,
                                phoneNumber: phoneNumber!,
                                address: address!,
                                userId: userId)
        return customer
    }
}
