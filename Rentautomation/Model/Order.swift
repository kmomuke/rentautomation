//
//  Order.swift
//  Rentautomation
//
//  Created by kanybek on 11/28/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import FMDB

// Write this for order
// Order related only to Money

// Order Registers all money operations like,
// it more like Activity of every emplyee action

// Order types: {

// Продажа
// Employee selled product to Customer, that made order
// Employee edited the order, that made before

// Закупка
// Employee make order from Supplier
// Employee edited the order from Supplier, that made before

// Возврат от продажи
// Employee make order from receive from customer (Возврат от продажи)
// Employee edited order receivec from customer (Возврат от продажи)

// Возврат от покупки
// Employee make order from receive from supplier (Возврат от закупки)
// Employee edited order receivec from supplier (Возврат от закупки)

// Приход
// Employee received money from customer
// Edit Employee received money from customer

// Расход
// Same here
//}
// MARK: Private

//open class OrderRequest : GPBMessage {
//    open var orderId: UInt64
//    open var orderDocument: UInt32
//    open var moneyMovementType: UInt32
//    open var billingNo: String!
//    open var userId: UInt64
//    open var customerId: UInt64
//    open var supplierId: UInt64
//    open var orderDate: UInt64
//    open var paymentId: UInt64
//    open var errorMsg: String!
//    open var comment: String!
//    open var orderUuid: String!
//    open var isDeleted: UInt32
//    open var isMoneyForDebt: UInt32
//    open var isEdited: UInt32
//}

private enum Repeat {
    case Never, Daily, Weekly, Monthly, Yearly
    func title() -> String {
        switch self {
        case .Never: return "Never"
        case .Daily: return "Daily"
        case .Weekly: return "Weekly"
        case .Monthly: return "Monthly"
        case .Yearly: return "Yearly"
        }
    }
    static func values() -> [Repeat] {
        return [Daily, Weekly, Monthly, Yearly]
    }
}

private enum Alert {
    case None, AtTime, Five, Thirty, Hour, Day, Week
    func title() -> String {
        switch self {
        case .None: return "None"
        case .AtTime: return "At time of event"
        case .Five: return "5 minutes before"
        case .Thirty: return "30 minutes before"
        case .Hour: return "1 hour before"
        case .Day: return "1 day before"
        case .Week: return "1 week before"
        }
    }
    static func values() -> [Alert] {
        return [AtTime, Five, Thirty, Hour, Day, Week]
    }
}

public enum MoneyMovementType : UInt {
    case none = 0
    
    case moneyFromCustomer = 1000        //"Оплата от клиента"
    case moneyFromSupplierReturn = 2000  //"Возврат денежных средств поставщиком"
    case moneyCorrection = 3000          //"Корректировка баланса счета"
    case moneyReceive = 4000             //"Прием денежных средств"
    case otherMoneyReceive = 5000        //"Прочий приход денежных средств"
    
    case moneyGoneToSupplier = 11000   //"Оплата поставщику"
    case moneyReturnToCustomer = 22000 //"Возврат денежных средств клиенту"
    case moneyGoneToCarFixxess = 33000 //"Ремонт"
    case moneyGoneToOil = 44000        //"Бензин"
    case moneyGoneToSomeone = 55000    //"Выдача денежных средств"
    case moneyGoneToAnother = 66000    //"Прочее"
}

public enum OrderMovementType : UInt {
    case none = 0
    
    case orderSaleRelated = 11000
    case orderDevelopeRelated = 22000
}

public enum OrderDocumentType : UInt {
    case none = 0
    
    case saleToCustomer = 1000 // customer sale
    case saleToCustomerEdited = 2000 // "edit" customer sale
    
    case receiveFromSupplier = 3000 // supplier income
    case receiveFromSupplierEdited = 4000 // "edit" supplier income
    
    case returnFromCustomer = 5000 // return to customer
    case returnFromCustomerEdited = 6000 // "edit" return to customer
    
    case returnToSupplier = 5500 // return to supplier
    case returnToSupplierEdited = 6600 // "edit" return to supplier
    
    case moneyReceive = 7000 //  money receive
    case moneyReceiveEdited = 8000 // "edit" money receive
    
    case moneyGone = 10000 // money outgone
    case moneyGoneEdited = 11000 // "edit" money outgone
    
    case preOrderToCustomer = 12000    //"Заказ для клиента"
    case preOrderToCustomerEdited = 12500    //"edit" заказ для клиента
    
    case stockTaking = 13000    //"Инвентаризация"
    
    case rawProductGoneToProd = 21000 // Расход сырья
    case rawProductGoneToProdEdited = 22000
    
    case readyProductReceivedFromProd = 23000 // Выход готовой продукции
    case readyProductReceivedFromProdEdited = 24000
    
    case incomeProductFromStock = 25000 // Приеммка продукта от склада
    case incomeProductFromStockEdited = 26000
    
    case removeProducts = 27000 // Списание товара
    case removeProductsEdited = 28000
    
    case productMovememntToStock = 29000 // Перемещение товара
    case productMovememntToStockEdited = 30000
}

class Order: CustomStringConvertible {
    
    var orderId: UInt64
    var orderDocument: OrderDocumentType
    var orderMovement: OrderMovementType
    var moneyMovementType: MoneyMovementType
    var billingNo: String
    var userId: UInt64
    var customerId: UInt64
    var supplierId: UInt64
    var orderDate: Date
    var paymentId: UInt64
    
    var errorMsg: String
    var comment: String
    var orderUUID: String
    
    var isDeleted: Bool
    var isMoneyForDebt: Bool // money income for customer debt
    var isEdited: Bool
    
    convenience init() {
        self.init(orderId: 0,
                  orderDocument: .none,
                  orderMovement: .orderSaleRelated,
                  moneyMovementType: .none,
                  billingNo: "",
                  userId: 0,
                  customerId: 0,
                  supplierId: 0,
                  orderDate: Date(),
                  paymentId: 0,
                  errorMsg: "",
                  comment: "",
                  orderUUID: UUID().uuidString.lowercased(),
                  deleted: false,
                  isMoneyForDebt: false,
                  editted: false)
    }
    
    init(orderId: UInt64,
         orderDocument: OrderDocumentType,
         orderMovement: OrderMovementType,
         moneyMovementType: MoneyMovementType,
         billingNo: String,
         userId: UInt64,
         customerId: UInt64,
         supplierId: UInt64,
         orderDate: Date,
         paymentId: UInt64,
         errorMsg: String,
         comment: String,
         orderUUID: String,
         deleted: Bool,
         isMoneyForDebt: Bool,
         editted: Bool) {
        
        self.orderId = orderId
        self.orderDocument = orderDocument
        self.orderMovement = orderMovement
        self.moneyMovementType = moneyMovementType
        self.billingNo = billingNo
        self.userId = userId
        self.customerId = customerId
        self.supplierId = supplierId
        self.orderDate = orderDate
        self.paymentId = paymentId
        self.errorMsg = errorMsg
        self.comment = comment
        self.orderUUID = orderUUID
        self.isDeleted = deleted
        self.isMoneyForDebt = isMoneyForDebt
        self.isEdited = editted
    }
    
    deinit {
    }
    
    var description: String {
        return "_\(self.orderId)__\(self.orderDocument)"
    }
    
    func databaseInsertValues() -> [Any] {
        
        var isDeletedValue: Int8 = 0
        if self.isDeleted {
            isDeletedValue = 1
        }
        
        var isMoneyForDebtValue: Int8 = 0
        if self.isMoneyForDebt {
            isMoneyForDebtValue = 1
        }
        
        var isEdittedValue: Int8 = 0
        if self.isEdited {
            isEdittedValue = 1
        }
        
        let orderDateInt = UInt64(self.orderDate.timeIntervalSince1970 * 1000)
        
        let orderDocumentUInt = Order.integerOrderDocumentTypeFrom(orderDocType: self.orderDocument)
        let orderMovementUInt = Order.integerOrderMovement(orderMovement: self.orderMovement)
        let moneyMovementUInt = Order.integerMoneyMovement(moneyMovement: self.moneyMovementType)
        
        return [self.orderId,
                orderDocumentUInt,
                orderMovementUInt,
                moneyMovementUInt,
                self.billingNo,
                self.userId,
                self.customerId,
                self.supplierId,
                orderDateInt,
                self.paymentId,
                self.errorMsg,
                self.comment,
                self.orderUUID,
                isDeletedValue,
                isMoneyForDebtValue,
                isEdittedValue]
    }
    
    //try self.database.executeUpdate("CREATE TABLE IF NOT EXISTS \(self.ordersTableName) (order_id INTEGER PRIMARY KEY, order_document INTEGER, order_movement INTEGER, money_movement INTEGER, billing_no TEXT, user_id INTEGER, customer_id INTEGER, supplier_id INTEGER, order_date INTEGER, payment_id INTEGER, error_msg TEXT, comment TEXT, uuid TEXT, is_deleted INTEGER, is_money_for_debt INTEGER, is_editted INTEGER)", values: nil)
    
    static func orderFrom(queryResult: FMResultSet) -> Order {
        
        let orderId = queryResult.unsignedLongLongInt(forColumn: "order_id")
        
        let orderDocumentUInt = queryResult.unsignedLongLongInt(forColumn: "order_document")
        let orderDocument: OrderDocumentType = Order.orderDocumentTypeFromInteger(orderDocumentUInt)
        
        let orderMovementUInt = queryResult.unsignedLongLongInt(forColumn: "order_movement")
        let orderMovement: OrderMovementType = Order.orderMovementTypeFromInteger(orderMovementUInt: orderMovementUInt)
        
        let moneyMovementUInt = queryResult.unsignedLongLongInt(forColumn: "money_movement")
        let moneyMovement: MoneyMovementType = Order.moneMovementTypeFromInteger(moneyMovementUInt: moneyMovementUInt)
        
        let billingNo = queryResult.string(forColumn: "billing_no")
        let userId = queryResult.unsignedLongLongInt(forColumn: "user_id")
        let customerId = queryResult.unsignedLongLongInt(forColumn: "customer_id")
        let supplierId = queryResult.unsignedLongLongInt(forColumn: "supplier_id")
        
        let orderDateInt   = queryResult.unsignedLongLongInt(forColumn: "order_date")
        let orderDate = Date(timeIntervalSince1970: Double(orderDateInt/1000))
        
        let paymentId = queryResult.unsignedLongLongInt(forColumn: "payment_id")
        let errorMsg = queryResult.string(forColumn: "error_msg")
        let comment = queryResult.string(forColumn: "comment")
        let orderUUID = queryResult.string(forColumn: "uuid")
        
        var isDeleted: Bool = false
        let isDeletedInt = queryResult.int(forColumn: "is_deleted")
        if isDeletedInt == 1 {
            isDeleted = true
        }
        
        var isMoneyForDebt: Bool = false
        let isMoneyForDebtValue = queryResult.int(forColumn: "is_money_for_debt")
        if isMoneyForDebtValue == 1 {
            isMoneyForDebt = true
        }
        
        var isEditted: Bool = false
        let isEdittedInt = queryResult.int(forColumn: "is_editted")
        if isEdittedInt == 1 {
            isEditted = true
        }
        
        let order = Order(orderId: orderId,
                          orderDocument: orderDocument,
                          orderMovement: orderMovement,
                          moneyMovementType: moneyMovement,
                          billingNo: billingNo!,
                          userId: userId,
                          customerId: customerId,
                          supplierId: supplierId,
                          orderDate: orderDate,
                          paymentId: paymentId,
                          errorMsg: errorMsg!,
                          comment: comment!,
                          orderUUID: orderUUID!,
                          deleted: isDeleted,
                          isMoneyForDebt: isMoneyForDebt,
                          editted: isEditted)
        
        return order
    }
    
    //Mark: Movement
    static func orderMovementTypeFromInteger(orderMovementUInt: UInt64) -> OrderMovementType {
        var orderMovement: OrderMovementType = .none
        
        if orderMovementUInt == 0 {
            orderMovement = .none
        } else if orderMovementUInt == 11000 {
            orderMovement = .orderSaleRelated
        } else if orderMovementUInt == 22000 {
            orderMovement = .orderDevelopeRelated
        }
        return orderMovement
    }
    
    static func integerOrderMovement(orderMovement: OrderMovementType) -> UInt32 {
        var orderMovementUInt: UInt32 = 0
        
        switch orderMovement {
        case .none:
            orderMovementUInt = 0
        case .orderSaleRelated:
            orderMovementUInt = 11000
        case .orderDevelopeRelated:
            orderMovementUInt = 22000
        default:
            orderMovementUInt = 0
        }
        return orderMovementUInt
    }
    
    //Mark: Type
    static func orderDocumentTypeFromInteger(_ orderDocumentUInt: UInt64) -> OrderDocumentType {
        
        var orderDocument: OrderDocumentType = .none
        
        if orderDocumentUInt == 0 {
            orderDocument = .none
            
        } else if orderDocumentUInt == 1000 {
            orderDocument = .saleToCustomer
        } else if orderDocumentUInt == 2000 {
            orderDocument = .saleToCustomerEdited
            
        } else if orderDocumentUInt == 3000 {
            orderDocument = .receiveFromSupplier
        } else if orderDocumentUInt == 4000 {
            orderDocument = .receiveFromSupplierEdited
            
        } else if orderDocumentUInt == 5000 {
            orderDocument = .returnFromCustomer
        } else if orderDocumentUInt == 6000 {
            orderDocument = .returnFromCustomerEdited
            
        } else if orderDocumentUInt == 5500 {
            orderDocument = .returnToSupplier
        } else if orderDocumentUInt == 6600 {
            orderDocument = .returnToSupplierEdited
            
        } else if orderDocumentUInt == 7000 {
            orderDocument = .moneyReceive
        } else if orderDocumentUInt == 8000 {
            orderDocument = .moneyReceiveEdited
            
        } else if orderDocumentUInt == 10000 {
            orderDocument = .moneyGone
        } else if orderDocumentUInt == 11000 {
            orderDocument = .moneyGoneEdited
            
        } else if orderDocumentUInt == 12000 {
            orderDocument = .preOrderToCustomer
        } else if orderDocumentUInt == 12500 {
            orderDocument = .preOrderToCustomerEdited
            
        } else if orderDocumentUInt == 13000 {
            orderDocument = .stockTaking
        } else if orderDocumentUInt == 21000 {
            
            orderDocument = .rawProductGoneToProd
        } else if orderDocumentUInt == 22000 {
            orderDocument = .rawProductGoneToProdEdited
            
        } else if orderDocumentUInt == 23000 {
            orderDocument = .readyProductReceivedFromProd
        } else if orderDocumentUInt == 24000 {
            orderDocument = .readyProductReceivedFromProdEdited
            
        } else if orderDocumentUInt == 25000 {
            orderDocument = .incomeProductFromStock
        } else if orderDocumentUInt == 26000 {
            orderDocument = .incomeProductFromStockEdited
            
        } else if orderDocumentUInt == 27000 {
            orderDocument = .removeProducts
        } else if orderDocumentUInt == 28000 {
            orderDocument = .removeProductsEdited
            
        } else if orderDocumentUInt == 29000 {
            orderDocument = .productMovememntToStock
        } else if orderDocumentUInt == 30000 {
            orderDocument = .productMovememntToStockEdited
        }
        
        return orderDocument
    }
    
    static func integerOrderDocumentTypeFrom(orderDocType: OrderDocumentType) -> UInt32 {
        
        var orderDocumentUInt: UInt32 = 0
        
        switch orderDocType {
            
        case .none:
            orderDocumentUInt = 0
            
        case .saleToCustomer:
            orderDocumentUInt = 1000
        case .saleToCustomerEdited:
            orderDocumentUInt = 2000
            
        case .receiveFromSupplier:
            orderDocumentUInt = 3000
        case .receiveFromSupplierEdited:
            orderDocumentUInt = 4000
            
        case .returnFromCustomer:
            orderDocumentUInt = 5000
        case .returnFromCustomerEdited:
            orderDocumentUInt = 6000
            
        case .returnToSupplier:
            orderDocumentUInt = 5500
        case .returnToSupplierEdited:
            orderDocumentUInt = 6600
            
        case .moneyReceive:
            orderDocumentUInt = 7000
        case .moneyReceiveEdited:
            orderDocumentUInt = 8000
            
        case .moneyGone:
            orderDocumentUInt = 10000
        case .moneyGoneEdited:
            orderDocumentUInt = 11000
            
        case .preOrderToCustomer:
            orderDocumentUInt = 12000
        case .preOrderToCustomerEdited:
            orderDocumentUInt = 12500
            
        case .stockTaking:
            orderDocumentUInt = 13000
        
        case .rawProductGoneToProd:
            orderDocumentUInt = 21000
        case .rawProductGoneToProdEdited:
            orderDocumentUInt = 22000
            
        case .readyProductReceivedFromProd:
            orderDocumentUInt = 23000
        case .readyProductReceivedFromProdEdited:
            orderDocumentUInt = 24000
            
        case .incomeProductFromStock:
            orderDocumentUInt = 25000
        case .incomeProductFromStockEdited:
            orderDocumentUInt = 26000
            
        case .removeProducts:
            orderDocumentUInt = 27000
        case .removeProductsEdited:
            orderDocumentUInt = 28000
        
        case .productMovememntToStock:
            orderDocumentUInt = 29000
        case .productMovememntToStockEdited:
            orderDocumentUInt = 30000
            
        default:
            orderDocumentUInt = 0
        }
        
        return orderDocumentUInt
    }
    
    static func moneMovementTypeFromInteger(moneyMovementUInt: UInt64) -> MoneyMovementType {
        var moneyMovement: MoneyMovementType = .none
        
        if moneyMovementUInt == 0 {
            moneyMovement = .none
            
        } else if moneyMovementUInt == 1000 {
            moneyMovement = .moneyFromCustomer
        } else if moneyMovementUInt == 2000 {
            moneyMovement = .moneyFromSupplierReturn
        } else if moneyMovementUInt == 3000 {
            moneyMovement = .moneyCorrection
        } else if moneyMovementUInt == 4000 {
            moneyMovement = .moneyReceive
        } else if moneyMovementUInt == 5000 {
            moneyMovement = .otherMoneyReceive
            
            
        } else if moneyMovementUInt == 11000 {
            moneyMovement = .moneyGoneToSupplier
        } else if moneyMovementUInt == 22000 {
            moneyMovement = .moneyReturnToCustomer
        } else if moneyMovementUInt == 33000 {
            moneyMovement = .moneyGoneToCarFixxess
        } else if moneyMovementUInt == 44000 {
            moneyMovement = .moneyGoneToOil
        } else if moneyMovementUInt == 55000 {
            moneyMovement = .moneyGoneToSomeone
        } else if moneyMovementUInt == 66000 {
            moneyMovement = .moneyGoneToAnother
        }
        
        return moneyMovement
    }
    
    static func integerMoneyMovement(moneyMovement: MoneyMovementType) -> UInt32 {
        var moneyMovementUInt: UInt32 = 0
        
        switch moneyMovement {
            
        case .none:
            moneyMovementUInt = 0
        case .moneyFromCustomer:
            moneyMovementUInt = 1000
        case .moneyFromSupplierReturn:
            moneyMovementUInt = 2000
        case .moneyCorrection:
            moneyMovementUInt = 3000
        case .moneyReceive:
            moneyMovementUInt = 4000
        case .otherMoneyReceive:
            moneyMovementUInt = 5000
            
        case .moneyGoneToSupplier:
            moneyMovementUInt = 11000
        case .moneyReturnToCustomer:
            moneyMovementUInt = 22000
        case .moneyGoneToCarFixxess:
            moneyMovementUInt = 33000
        case .moneyGoneToOil:
            moneyMovementUInt = 44000
        case .moneyGoneToSomeone:
            moneyMovementUInt = 55000
        case .moneyGoneToAnother:
            moneyMovementUInt = 66000
            
        default:
            moneyMovementUInt = 0
        }
        
        return moneyMovementUInt
    }
    
    static func orderDocumentTypeStringName(orderDocType: OrderDocumentType) -> String {
    
        var orderDocumentString: String = "Пусто"
        
        switch orderDocType {
            
        case .none:
            orderDocumentString = "Пусто"
        case .saleToCustomer:
            orderDocumentString = "Продажа продукта к Клиенту"
        case .saleToCustomerEdited:
            orderDocumentString = "Продажа документ был откатан назад"
            
        case .receiveFromSupplier:
            orderDocumentString = "Закупка продукта от Поставщика"
        case .receiveFromSupplierEdited:
            orderDocumentString = "Закупка документ был изменен"
            
        case .returnFromCustomer:
            orderDocumentString = "Возврат продукта от Клиента"
        case .returnFromCustomerEdited:
            orderDocumentString = "Возврат от Клиента документ был изменен"
            
        case .returnToSupplier:
            orderDocumentString = "Возврат продукта к Поставщику"
        case .returnToSupplierEdited:
            orderDocumentString = "Возврат к Поставщику документ изменен"
            
        case .moneyReceive:
            orderDocumentString = "Приход денег"
        case .moneyReceiveEdited:
            orderDocumentString = "Приход денег документ изменен"
            
        case .moneyGone:
            orderDocumentString = "Расход денег"
        case .moneyGoneEdited:
            orderDocumentString = "Расход денег документ изменен"
        
        case .preOrderToCustomer:
            orderDocumentString = "Клиент сделал заказ"
        case .preOrderToCustomerEdited:
            orderDocumentString = "Редактирование Клиент сделал заказ"
            
        case .stockTaking:
            orderDocumentString = "Инвентаризация / Черновик"
            
        case .rawProductGoneToProd:
            orderDocumentString = "Расход сырья"
        case .rawProductGoneToProdEdited:
            orderDocumentString = "Расход сырья изменен"
            
        case .readyProductReceivedFromProd:
            orderDocumentString = "Выход готовой продукции"
        case .readyProductReceivedFromProdEdited:
            orderDocumentString = "Выход готовой продукции изменен"
            
        case .incomeProductFromStock:
            orderDocumentString = "Прием товаров"
        case .incomeProductFromStockEdited:
            orderDocumentString = "Прием товаров изменен"
            
        case .removeProducts:
            orderDocumentString = "Списание товара"
        case .removeProductsEdited:
            orderDocumentString = "Списание товара изменен"
            
        case .productMovememntToStock:
            orderDocumentString = "Перемещение товара"
        case .productMovememntToStockEdited:
            orderDocumentString = "Перемещение товара изменен"
            
        default:
            orderDocumentString = "Пусто"
        }
        
        return orderDocumentString
    }
    
    static func moneyMovementTypeStringName(moneyType: MoneyMovementType) -> String {
        
        var orderDocumentString: String = "Пусто"
        
        switch moneyType {
            
        case .none:
            orderDocumentString = "Пусто"
        case .moneyFromCustomer:
            orderDocumentString = "Оплата от клиента"
        case .moneyFromSupplierReturn:
            orderDocumentString = "Возврат денежных средств поставщиком"
        case .moneyCorrection:
            orderDocumentString = "Корректировка баланса счета"
        case .moneyReceive:
            orderDocumentString = "Прием денежных средств"
        case .otherMoneyReceive:
            orderDocumentString = "Прочий приход денежных средств"
        
        
        case .moneyGoneToSupplier:
            orderDocumentString = "Оплата поставщику"
        case .moneyReturnToCustomer:
            orderDocumentString = "Возврат денежных средств клиенту"
        case .moneyGoneToCarFixxess:
            orderDocumentString = "Ремонт"
        case .moneyGoneToOil:
            orderDocumentString = "Бензин"
        case .moneyGoneToSomeone:
            orderDocumentString = "Выдача денежных средств"
        case .moneyGoneToAnother:
            orderDocumentString = "Прочее"
        
        default:
            orderDocumentString = "Пусто"
        }
        
        return orderDocumentString
    }
    
    static func orderDocumentTypeColor(orderDocType: OrderDocumentType) -> UIColor {
        
        var orderDocumentColor: UIColor = UIColor.black
        
        switch orderDocType {
            
        case .none:
            orderDocumentColor = UIColor.black
            
        case .saleToCustomer:
            orderDocumentColor = UIColor.emeraldColor()
            
        case .saleToCustomerEdited:
            orderDocumentColor = UIColor.red
            
        case .receiveFromSupplier:
            orderDocumentColor = UIColor.belizeHoleColor()
            
        case .receiveFromSupplierEdited:
            orderDocumentColor = UIColor.red
            
        case .returnFromCustomer:
            orderDocumentColor = UIColor.alizarinColor()
            
        case .returnFromCustomerEdited:
            orderDocumentColor = UIColor.red
            
        case .returnToSupplier:
            orderDocumentColor = UIColor.pomergranateColor()
            
        case .returnToSupplierEdited:
            orderDocumentColor = UIColor.red
            
        case .moneyReceive:
            orderDocumentColor = UIColor.greenSeaColor()
            
        case .moneyReceiveEdited:
            orderDocumentColor = UIColor.black
            
        case .moneyGone:
            orderDocumentColor = UIColor.pumpkinColor()
            
        case .moneyGoneEdited:
            orderDocumentColor = UIColor.red
        
        case .preOrderToCustomer:
            orderDocumentColor = UIColor.wisteriaColor()
            
        case .preOrderToCustomerEdited:
            orderDocumentColor = UIColor.wisteriaColor()
            
        case .stockTaking:
            orderDocumentColor = UIColor.midnightBlueColor()
            
        case .rawProductGoneToProd:
            orderDocumentColor = UIColor.peterRiverColor()
        case .rawProductGoneToProdEdited:
            orderDocumentColor = UIColor.red
            
        case .readyProductReceivedFromProd:
            orderDocumentColor = UIColor.wisteriaColor()
        case .readyProductReceivedFromProdEdited:
            orderDocumentColor = UIColor.red
            
        case .incomeProductFromStock:
            orderDocumentColor = UIColor.midnightBlueColor()
        case .incomeProductFromStockEdited:
            orderDocumentColor = UIColor.red
            
        case .removeProducts:
            orderDocumentColor = UIColor.midnightBlueColor()
        case .removeProductsEdited:
            orderDocumentColor = UIColor.red
            
        case .productMovememntToStock:
            orderDocumentColor = UIColor.midnightBlueColor()
        case .productMovememntToStockEdited:
            orderDocumentColor = UIColor.red
            
        default:
            orderDocumentColor = UIColor.black
        }
        
        return orderDocumentColor
    }
    
    static func orderMovementType(fromOrderDocType: OrderDocumentType) -> OrderMovementType {
        
        var orderMovementType: OrderMovementType = .none
        
        switch fromOrderDocType {
            
        case .none:
            orderMovementType = .orderSaleRelated
            
        case .saleToCustomer:
            orderMovementType = .orderSaleRelated
        case .saleToCustomerEdited:
            orderMovementType = .orderSaleRelated
            
        case .receiveFromSupplier:
            orderMovementType = .orderSaleRelated
        case .receiveFromSupplierEdited:
            orderMovementType = .orderSaleRelated
            
        case .returnFromCustomer:
            orderMovementType = .orderSaleRelated
        case .returnFromCustomerEdited:
            orderMovementType = .orderSaleRelated
            
        case .returnToSupplier:
            orderMovementType = .orderSaleRelated
        case .returnToSupplierEdited:
            orderMovementType = .orderSaleRelated
            
        case .moneyReceive:
            orderMovementType = .orderSaleRelated
        case .moneyReceiveEdited:
            orderMovementType = .orderSaleRelated
            
        case .moneyGone:
            orderMovementType = .orderSaleRelated
        case .moneyGoneEdited:
            orderMovementType = .orderSaleRelated
            
        case .preOrderToCustomer:
            orderMovementType = .orderSaleRelated
        case .preOrderToCustomerEdited:
            orderMovementType = .orderSaleRelated
            
        case .stockTaking:
            orderMovementType = .orderSaleRelated
            
        case .rawProductGoneToProd:
            orderMovementType = .orderDevelopeRelated
        case .rawProductGoneToProdEdited:
            orderMovementType = .orderDevelopeRelated
            
        case .readyProductReceivedFromProd:
            orderMovementType = .orderDevelopeRelated
        case .readyProductReceivedFromProdEdited:
            orderMovementType = .orderDevelopeRelated
            
        case .incomeProductFromStock:
            orderMovementType = .orderDevelopeRelated
        case .incomeProductFromStockEdited:
            orderMovementType = .orderDevelopeRelated
            
        case .removeProducts:
            orderMovementType = .orderDevelopeRelated
        case .removeProductsEdited:
            orderMovementType = .orderDevelopeRelated
            
        case .productMovememntToStock:
            orderMovementType = .orderDevelopeRelated
        case .productMovememntToStockEdited:
            orderMovementType = .orderDevelopeRelated
            
        default:
            orderMovementType = .none
        }
        
        return orderMovementType
    }
    
}
