//
//  User.swift
//  Rentautomation
//
//  Created by kanybek on 7/26/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import FMDB

//open class UserRequest : GPBMessage {
//    open var userId: UInt64
//    open var roleId: UInt64
//    open var userImagePath: String!
//    open var firstName: String!
//    open var secondName: String!
//    open var email: String!
//    open var password: String!
//    open var phoneNumber: String!
//    open var address: String!
//}

class User: CustomStringConvertible {
    
    var userId: UInt64
    var userUUID: String
    var isCurrentUser: Bool
    var roleId: UInt64
    var userImagePath: String?
    var firstName: String
    var secondName: String
    var email: String
    var password: String
    var phoneNumber: String
    var address: String
    
    convenience init() {
        self.init(userId: 0,
                  userUUID: UUID().uuidString.lowercased(),
                  isCurrentUser: false,
                  roleId: 0,
                  userImagePath: "",
                  firstName: "",
                  secondName: "",
                  email: "",
                  password: "",
                  phoneNumber: "",
                  address: "")
    }
    
    init(userId: UInt64,
         userUUID: String,
         isCurrentUser: Bool,
         roleId: UInt64,
         userImagePath: String?,
         firstName: String,
         secondName: String,
         email: String,
         password: String,
         phoneNumber: String,
         address: String) {
        
        self.userId = userId
        self.userUUID = userUUID
        self.isCurrentUser = isCurrentUser
        self.roleId = roleId
        self.userImagePath = userImagePath
        self.firstName = firstName
        self.secondName = secondName
        self.email = email
        self.password = password
        self.phoneNumber = phoneNumber
        self.address = address
    }
    
    deinit {
    }
    
    var description: String {
        return "_\(self.userId)__\(self.firstName)"
    }
    
    //try self.database.executeUpdate("CREATE TABLE IF NOT EXISTS \(self.usersTableName) (user_id INTEGER PRIMARY KEY, uuid TEXT, is_current_user INTEGER, role_id INTEGER, user_image_path TEXT, first_name TEXT, second_name TEXT, email TEXT, password TEXT, phone_number TEXT, address TEXT)", values: nil)
    
    func databaseInsertValues() -> [Any] {
        
        var isCurrentUserValue: Int8 = 0
        if self.isCurrentUser {
            isCurrentUserValue = 1
        }
        
        return [self.userId,
                self.userUUID,
                isCurrentUserValue,
                self.roleId,
                self.userImagePath ?? "",
                self.firstName,
                self.secondName,
                self.email,
                self.password,
                self.phoneNumber,
                self.address]
    }
    
    func databaseUpdateValues() -> [Any] {
        
        var isCurrentUserValue: Int8 = 0
        if self.isCurrentUser {
            isCurrentUserValue = 1
        }
        
        return [isCurrentUserValue,
                self.roleId,
                self.userImagePath ?? "",
                self.firstName,
                self.secondName,
                self.email,
                self.password,
                self.phoneNumber,
                self.address,
                self.userId]
    }
    
    static func userFrom(queryResult: FMResultSet) -> User {
        
        let userId = queryResult.unsignedLongLongInt(forColumn: "user_id")
        let userUUID = queryResult.string(forColumn: "uuid")
        
        var isCurrentUser: Bool = false
        let isCurrentUserInt = queryResult.int(forColumn: "is_current_user")
        if isCurrentUserInt == 1 {
            isCurrentUser = true
        }
        
        let roleId = queryResult.unsignedLongLongInt(forColumn: "role_id")
        let userImagePath = queryResult.string(forColumn: "user_image_path")
        let firstName   = queryResult.string(forColumn: "first_name")
        let secondName   = queryResult.string(forColumn: "second_name")
        let email   = queryResult.string(forColumn: "email")
        let password   = queryResult.string(forColumn: "password")
        let phoneNumber   = queryResult.string(forColumn: "phone_number")
        let address   = queryResult.string(forColumn: "address")
        
        let user = User(userId: userId,
                        userUUID: userUUID!,
                        isCurrentUser: isCurrentUser,
                        roleId: roleId,
                        userImagePath: userImagePath!,
                        firstName: firstName!,
                        secondName: secondName!,
                        email: email!,
                        password: password!,
                        phoneNumber: phoneNumber!,
                        address: address!)
        
        return user
    }
}
