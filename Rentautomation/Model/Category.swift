//
//  Category.swift
//  Rentautomation
//
//  Created by kanybek on 11/28/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import FMDB

//open class CategoryRequest : GPBMessage {
//    open var categoryId: UInt64
//    open var categoryName: String!
//}

class Category: CustomStringConvertible {
    
    var categoryId: UInt64
    var categoryName: String
    
    convenience init() {
        self.init(categoryId: 0, categoryName: "")
    }
    
    init(categoryId: UInt64, categoryName: String) {
        self.categoryId = categoryId
        self.categoryName = categoryName
    }
    
    deinit {
    }
    
    var description: String {
        return "_\(self.categoryId)__\(self.categoryName)"
    }
    
    //try self.database.executeUpdate("CREATE TABLE IF NOT EXISTS \(self.categoriesTableName) (category_id INTEGER PRIMARY KEY, category_name TEXT)", values: nil)

    static func categoryFrom(queryResult: FMResultSet) -> Category {
        
        let categoryId = queryResult.unsignedLongLongInt(forColumn: "category_id")
        let categoryName   = queryResult.string(forColumn: "category_name")
        let category = Category(categoryId: categoryId, categoryName: categoryName!)
        return category
    }
}
