//
//  ModelToProto.swift
//  Rentautomation
//
//  Created by kanybek on 5/10/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import grpcPod

final class ModelToProto {
    
    static func transactionRequestFrom(_ transaction: Transaction) -> TransactionRequest {
        let transReq = TransactionRequest()
        transReq.transactionId = transaction.transactionId
        transReq.transactionDate = UInt64(transaction.transactionDate.timeIntervalSince1970 * 1000)
        
        var isLastTransactionValue: UInt32 = 0
        if transaction.isLastTransaction {
            isLastTransactionValue = 1
        }
        transReq.isLastTransaction = isLastTransactionValue
        transReq.transactionType = Transaction.integerTransactionTypeFrom(transactionType: transaction.transactionType)
        transReq.moneyAmount = transaction.moneyAmount
        transReq.orderId = transaction.orderId
        transReq.customerId = transaction.customerId
        transReq.supplierId = transaction.supplierId
        transReq.userId = transaction.userId
        transReq.comment = transaction.comment
        transReq.transactionUuid = transaction.transactionUuid
        transReq.ballanceAmount = transaction.ballanceAmount
        
        return transReq
    }
    
    static func orderDetailRequestFrom(_ orderDetail: OrderDetail) -> OrderDetailRequest {
        let orderDetailReq = OrderDetailRequest()
        orderDetailReq.orderDetailId = orderDetail.orderDetailId
        orderDetailReq.orderId = orderDetail.orderId
        orderDetailReq.orderDetailDate = UInt64(orderDetail.orderDetailDate.timeIntervalSince1970 * 1000)
        
        var isLastValue: UInt32 = 0
        if orderDetail.isLast {
            isLastValue = 1
        }
        orderDetailReq.isLast = isLastValue
        
        orderDetailReq.productId = orderDetail.productId
        orderDetailReq.billingNo = orderDetail.billingNo
        orderDetailReq.price = orderDetail.price
        orderDetailReq.orderQuantity = orderDetail.orderQuantity
        orderDetailReq.productQuantity = orderDetail.productQuantity
        orderDetailReq.orderDetailComment = orderDetail.orderDetailComment
        orderDetailReq.orderDetailUuid = orderDetail.orderDetailUuid
        return orderDetailReq
    }
    
    static func paymentRequestFrom(_ payment: Payment) -> PaymentRequest {
        let paymentReq = PaymentRequest()
        paymentReq.paymentId = payment.paymentId
        paymentReq.totalOrderPrice = payment.totalOrderPrice
        paymentReq.discount = payment.discount
        paymentReq.totalPriceWithDiscount = payment.totalPriceWithDiscount
        paymentReq.minusPrice = payment.minusPrice
        paymentReq.plusPrice = payment.plusPrice
        paymentReq.comment = payment.comment
        return paymentReq
    }
    
    static func orderRequestFrom(_ order: Order) -> OrderRequest {
        let orderReq = OrderRequest()
        orderReq.orderId = order.orderId
        orderReq.orderDocument = Order.integerOrderDocumentTypeFrom(orderDocType: order.orderDocument)
        orderReq.orderMovement = Order.integerOrderMovement(orderMovement: order.orderMovement)
        orderReq.moneyMovementType = Order.integerMoneyMovement(moneyMovement: order.moneyMovementType)
        orderReq.billingNo = order.billingNo
        orderReq.userId = order.userId
        orderReq.customerId = order.customerId
        orderReq.supplierId = order.supplierId
        orderReq.orderDate = UInt64(order.orderDate.timeIntervalSince1970 * 1000)
        orderReq.paymentId = order.paymentId
        orderReq.errorMsg = order.errorMsg
        orderReq.comment = order.comment
        orderReq.orderUuid = order.orderUUID
        
        var isDeletedValue: UInt32 = 0
        if order.isDeleted {
            isDeletedValue = 1
        }
        var isMoneyForDebtValue: UInt32 = 0
        if order.isMoneyForDebt {
            isMoneyForDebtValue = 1
        }
        var isEdittedValue: UInt32 = 0
        if order.isEdited {
            isEdittedValue = 1
        }
        
        orderReq.isDeleted = isDeletedValue
        orderReq.isMoneyForDebt = isMoneyForDebtValue
        orderReq.isEdited = isEdittedValue
        return orderReq
    }
    
    static func accountRequestFrom(_ account: Account) -> AccountRequest {
        let accReq = AccountRequest()
        accReq.accountId = account.accountId
        accReq.customerId = account.customerId
        accReq.supplierId = account.supplierId
        accReq.balance = account.balance
        return accReq
    }
    
    static func supplierRequestFrom(_ supplier: Supplier) -> SupplierRequest {
        let suppReq = SupplierRequest()
        suppReq.supplierId = supplier.supplierId
        suppReq.supplierImagePath = supplier.supplierImagePath
        suppReq.companyName = supplier.companyName
        suppReq.contactFname = supplier.contactFname
        suppReq.phoneNumber = supplier.phoneNumber
        suppReq.address = supplier.address
        return suppReq
    }
    
    static func customerRequestFrom(_ customer: Customer) -> CustomerRequest {
        let custReq = CustomerRequest()
        custReq.customerId = customer.customerId
        custReq.customerImagePath = customer.customerImagePath
        custReq.firstName = customer.firstName
        custReq.secondName = customer.secondName
        custReq.phoneNumber = customer.phoneNumber
        custReq.address = customer.address
        custReq.userId = customer.userId
        return custReq
    }
    
    static func categoryRequestFrom(_ category: Category) -> CategoryRequest {
        let ctReq = CategoryRequest()
        ctReq.categoryId = category.categoryId
        ctReq.categoryName = category.categoryName
        return ctReq
    }
    
    static func productRequestFrom(_ product: Product) -> ProductRequest {
        let prodReq = ProductRequest()
        prodReq.productId = product.productId
        prodReq.productImagePath = product.productImagePath
        prodReq.productName = product.productName
        prodReq.supplierId = product.supplierId
        prodReq.categoryId = product.categoryId
        prodReq.barcode = product.barcode
        prodReq.quantityPerUnit = product.quantityPerUnit
        prodReq.saleUnitPrice = product.saleUnitPrice
        prodReq.incomeUnitPrice = product.incomeUnitPrice
        prodReq.unitsInStock = product.unitsInStock
        return prodReq
    }
    
    static func userRequestFrom(_ user: User) -> UserRequest {
        let userReq = UserRequest()
        userReq.userId = user.userId
        userReq.userUuid = user.userUUID
        userReq.roleId = user.roleId
        userReq.userImagePath = user.userImagePath
        userReq.firstName = user.firstName
        userReq.secondName = user.secondName
        userReq.email = user.email
        userReq.password = user.password
        userReq.phoneNumber = user.phoneNumber
        userReq.address = user.address
        return userReq
    }
}
