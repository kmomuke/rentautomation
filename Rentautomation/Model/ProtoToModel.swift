//
//  ProtoToModel.swift
//  Rentautomation
//
//  Created by kanybek on 5/10/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import grpcPod

final class ProtoToModel {
    
    static func transactionFrom(_ transactionRequest: TransactionRequest) -> Transaction {
        let transaction = Transaction()
        transaction.transactionId = transactionRequest.transactionId
        transaction.transactionDate = Date(timeIntervalSince1970: Double(transactionRequest.transactionDate/1000))
        
        var isLast_: Bool = false
        let isLastValueInt = transactionRequest.isLastTransaction
        if isLastValueInt == 1 {
            isLast_ = true
        }
        
        transaction.isLastTransaction = isLast_
        transaction.transactionType = Transaction.transactionTypeFromInteger(transactionTypeUInt: UInt64(transactionRequest.transactionType))
        transaction.moneyAmount = transactionRequest.moneyAmount
        transaction.orderId = transactionRequest.orderId
        transaction.customerId = transactionRequest.customerId
        transaction.supplierId = transactionRequest.supplierId
        transaction.userId = transactionRequest.userId
        transaction.comment = transactionRequest.comment
        transaction.transactionUuid = transactionRequest.transactionUuid
        transaction.ballanceAmount = transactionRequest.ballanceAmount
        
        return transaction
    }
    
    static func orderDetailFrom(_ orderDetailRequest: OrderDetailRequest) -> OrderDetail {
        let orderDetail = OrderDetail()
        orderDetail.orderDetailId = orderDetailRequest.orderDetailId
        orderDetail.orderId = orderDetailRequest.orderId
        orderDetail.orderDetailDate = Date(timeIntervalSince1970: Double(orderDetailRequest.orderDetailDate/1000))
        
        var isLast_: Bool = false
        let isLastValueInt = orderDetailRequest.isLast
        if isLastValueInt == 1 {
            isLast_ = true
        }
        
        orderDetail.isLast = isLast_
        orderDetail.productId = orderDetailRequest.productId
        orderDetail.billingNo = orderDetailRequest.billingNo!
        orderDetail.price = orderDetailRequest.price
        orderDetail.orderQuantity = orderDetailRequest.orderQuantity
        orderDetail.productQuantity = orderDetailRequest.productQuantity
        orderDetail.orderDetailComment = orderDetailRequest.orderDetailComment
        orderDetail.orderDetailUuid = orderDetailRequest.orderDetailUuid
        return orderDetail
    }
    
    static func paymentFrom(_ paymentRequest: PaymentRequest) -> Payment {
        let payment = Payment()
        payment.paymentId = paymentRequest.paymentId
        payment.totalOrderPrice = paymentRequest.totalOrderPrice
        payment.discount = paymentRequest.discount
        payment.totalPriceWithDiscount = paymentRequest.totalPriceWithDiscount
        payment.minusPrice = paymentRequest.minusPrice
        payment.plusPrice = paymentRequest.plusPrice
        payment.comment = paymentRequest.comment
        return payment
    }
    
    static func orderFrom(_ orderRequest: OrderRequest) -> Order {
        let order = Order()
        order.orderId = orderRequest.orderId
        order.orderDocument = Order.orderDocumentTypeFromInteger(UInt64(orderRequest.orderDocument))
        order.orderMovement = Order.orderMovementTypeFromInteger(orderMovementUInt: UInt64(orderRequest.orderMovement))
        order.moneyMovementType = Order.moneMovementTypeFromInteger(moneyMovementUInt: UInt64(orderRequest.moneyMovementType))
        
        order.billingNo = orderRequest.billingNo
        order.userId = orderRequest.userId
        order.customerId = orderRequest.customerId
        order.supplierId = orderRequest.supplierId
        order.orderDate = Date(timeIntervalSince1970: Double(orderRequest.orderDate/1000))
        order.paymentId = orderRequest.paymentId
        order.errorMsg = orderRequest.errorMsg
        order.comment = orderRequest.comment
        order.orderUUID = orderRequest.orderUuid
        
        var isDeleted: Bool = false
        let isDeletedInt = orderRequest.isDeleted
        if isDeletedInt == 1 {
            isDeleted = true
        }
        order.isDeleted = isDeleted
        
        var isMoneyForDebt: Bool = false
        let isMoneyForDebtInt = orderRequest.isMoneyForDebt
        if isMoneyForDebtInt == 1 {
            isMoneyForDebt = true
        }
        order.isMoneyForDebt = isMoneyForDebt
        
        var isEditted: Bool = false
        let isEdittedInt = orderRequest.isEdited
        if isEdittedInt == 1 {
            isEditted = true
        }
        order.isEdited = isEditted
        return order
    }
    
    static func accountFrom(_ accountRequest: AccountRequest) -> Account {
        let account = Account()
        account.accountId = accountRequest.accountId
        account.customerId = accountRequest.customerId
        account.supplierId = accountRequest.supplierId
        account.balance = accountRequest.balance
        return account
    }
    
    static func supplierFrom(_ supplierRequest: SupplierRequest) -> Supplier {
        let supplier = Supplier()
        supplier.supplierId = supplierRequest.supplierId
        supplier.supplierImagePath = supplierRequest.supplierImagePath
        supplier.companyName = supplierRequest.companyName
        supplier.contactFname = supplierRequest.contactFname
        supplier.phoneNumber = supplierRequest.phoneNumber
        supplier.address = supplierRequest.address
        return supplier
    }
    
    static func customerFrom(_ customerRequest: CustomerRequest) -> Customer {
        let customer = Customer()
        customer.customerId = customerRequest.customerId
        customer.customerImagePath = customerRequest.customerImagePath
        customer.firstName = customerRequest.firstName
        customer.secondName = customerRequest.secondName
        customer.phoneNumber = customerRequest.phoneNumber
        customer.address = customerRequest.address
        customer.userId = customerRequest.userId
        return customer
    }
    
    static func categoryFrom(_ categoryRequest: CategoryRequest) -> Category {
        let category = Category()
        category.categoryId = categoryRequest.categoryId
        category.categoryName = categoryRequest.categoryName
        return category
    }
    
    static func productFrom(_ productRequest: ProductRequest) -> Product {
        let product = Product()
        product.productId = productRequest.productId
        product.productImagePath = productRequest.productImagePath
        product.productName = productRequest.productName
        product.supplierId = productRequest.supplierId
        product.categoryId = productRequest.categoryId
        product.barcode = productRequest.barcode
        product.quantityPerUnit = productRequest.quantityPerUnit
        product.saleUnitPrice = productRequest.saleUnitPrice
        product.incomeUnitPrice = productRequest.incomeUnitPrice
        product.unitsInStock = productRequest.unitsInStock
        return product
    }
    
    static func userFrom(_ userRequest: UserRequest) -> User {
        let user = User()
        user.userId = userRequest.userId
        
        if let currentEmployee = AppSession.shared.currentEmployee {
            if currentEmployee.userId == userRequest.userId {
                user.isCurrentUser = true
            }
        }
        
        user.userUUID = userRequest.userUuid
        user.roleId = userRequest.roleId
        user.userImagePath = userRequest.userImagePath
        user.firstName = userRequest.firstName
        user.secondName = userRequest.secondName
        user.email = userRequest.email
        user.password = userRequest.password
        user.phoneNumber = userRequest.phoneNumber
        user.address = userRequest.address
        return user
    }
}
