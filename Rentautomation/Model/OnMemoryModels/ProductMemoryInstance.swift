//
//  ProductMemoryInstance.swift
//  Rentautomation
//
//  Created by kanybek on 11/30/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

final class ProductMemoryInstance {
    
    static let sharedInstance = ProductMemoryInstance()

    var productImage: UIImage?
    var isNewImageCaputed: Bool
    
    
    var productName: String?
    var saleUnitPrice: Double = 0.0
    var incomeUnitPrice: Double = 0.0
    var unitsInStock: Double = 0.0
    
    var productImagePath: String?
    
    var barcode: String?
    var quantityPerUnit: String = "шт"
    
    var categoryId: UInt64 = 0
    var supplierId: UInt64 = 0
    
    var colors: [String] = []
    var productSizes: [String] = []
    
    init() {
        isNewImageCaputed = false
    }
    
    func clearAllVariables() -> Void {

        self.productImage = nil
        self.productName = nil
        self.saleUnitPrice = 0.0
        self.incomeUnitPrice = 0.0
        self.unitsInStock = 0.0

        self.productImagePath = nil
        
        self.barcode = nil
        self.quantityPerUnit = "шт"
        
        self.categoryId = 0
        self.supplierId = 0
        
        self.colors = []
        self.productSizes = []
        
        isNewImageCaputed = false
    }
}
