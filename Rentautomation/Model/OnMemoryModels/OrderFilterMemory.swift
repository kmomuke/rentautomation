//
//  OrderFilterMemoryInstance.swift
//  Rentautomation
//
//  Created by kanybek on 2/10/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

final class OrderFilterMemory {
    static let shared = OrderFilterMemory()
    
    private var _customerId: UInt64 = 0
    private var _supplierId: UInt64 = 0
    
    var staffId: UInt64 = 0
    
    var customerId: UInt64 {
        get {
            return self._customerId
        }
        set(newCustomerId) {
            self._customerId = newCustomerId
            self._supplierId = 0
        }
    }
    
    var supplierId: UInt64 {
        get {
            return self._supplierId
        }
        set(newSupplierId) {
            self._supplierId = newSupplierId
            self._customerId = 0
        }
    }
    
    var orderDocument: OrderDocumentType = .none
    var moneyMovement: MoneyMovementType = .none
    
    var startDate: Date?
    var endDate: Date?
    
    init() {
    }
    
    func clearAllVariables() -> Void {
        
        self.customerId = 0
        self.supplierId = 0
        self.staffId = 0
        
        self.orderDocument = .none
        self.moneyMovement = .none
        
        self.startDate = nil
        self.endDate = nil
    }
}
