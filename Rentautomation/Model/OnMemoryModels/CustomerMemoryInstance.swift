//
//  CustomerMemoryInstance.swift
//  Rentautomation
//
//  Created by kanybek on 12/5/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

final class CustomerMemoryInstance {
    
    static let sharedInstance = CustomerMemoryInstance()
    
    var firstName: String?
    var secondName: String?
    var phoneNumber: String?
    var address: String?
    var staffId: UInt64
    
    init() {
        self.staffId = 0
    }
    
    func clearAllVariables() -> Void {
        
        self.firstName = nil
        self.secondName = nil
        self.phoneNumber = nil
        self.address = nil
        self.staffId = 0
    }
}
