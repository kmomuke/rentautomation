//
//  SupplierMemoryInstance.swift
//  Rentautomation
//
//  Created by kanybek on 12/3/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

final class SupplierMemoryInstance {
    
    static let sharedInstance = SupplierMemoryInstance()
    
    var companyName: String?
    var phoneNumber: String?
    var address: String?

    init() {
    }
    
    func clearAllVariables() -> Void {
        
        self.companyName = nil
        self.phoneNumber = nil
        self.address = nil
    }
}
