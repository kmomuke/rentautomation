//
//  StaffMemoryInstance.swift
//  Rentautomation
//
//  Created by kanybek on 12/5/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

final class StaffMemoryInstance {
    
    static let sharedInstance = StaffMemoryInstance()
    
    
    var roleId: UInt64 = 0
    
    var ipAdress: String?
    var firstName: String?
    var secondName: String?
    var phoneNumber: String?
    var address: String?
    
    var email: String?
    var password: String?
    var confirmPassword: String?
    
    init() {
    }
    
    func clearAllVariables() -> Void {
        
        self.ipAdress = nil
        self.firstName = nil
        self.secondName = nil
        self.phoneNumber = nil
        self.address = nil
        self.roleId = 0
        
        self.email = nil
        self.password = nil
        self.confirmPassword = nil
    }
}
