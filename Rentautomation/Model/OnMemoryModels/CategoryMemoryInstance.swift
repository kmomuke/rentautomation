//
//  CategoryMemoryInstance.swift
//  Rentautomation
//
//  Created by kanybek on 12/3/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

final class CategoryMemoryInstance {
    
    static let sharedInstance = CategoryMemoryInstance()
    var categoryName: String?

    init() {
    }
    
    func clearAllVariables() -> Void {
        self.categoryName = nil
    }
}
