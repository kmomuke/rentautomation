//
//  OrderMemomryInstance.swift
//  Rentautomation
//
//  Created by kanybek on 12/8/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

//if $0 == "Оплата от клиента" {
//    print("Owner")
//    StaffMemoryInstance.sharedInstance.roleId = 1000
//} else if $0 == "Возврат денежных средств поставщиком" {
//    print("Admin")
//    StaffMemoryInstance.sharedInstance.roleId = 2000
//} else if $0 == "Корректировка баланса счета" {
//    print("Agent")
//    StaffMemoryInstance.sharedInstance.roleId = 3000
//} else if $0 == "Прием денежных средств" {
//    print("Agent")
//    StaffMemoryInstance.sharedInstance.roleId = 3000
//} else if $0 == "Прочий приход денежных средств" {
//    print("Agent")
//    StaffMemoryInstance.sharedInstance.roleId = 3000
//}

//public enum IncomeMoneyType : UInt {
//    case none = 0
//    case moneyReceiveFromCustomer = 1000
//}

final class OrderMemomry {
    
    static let shared = OrderMemomry()
    
    var customerId: UInt64 = 0
    var supplierId: UInt64 = 0
    var staffId: UInt64 = 0
    
    var orderDocument: OrderDocumentType = .none
    var moneyMovement: MoneyMovementType = .none
    
    var moneyIncomeOutgoneSum: Double = 0.0
    var comments: String = ""
    var billingNo: String = ""
    
    init() {
    }
    
    func clearAllVariables() -> Void {
        
        OrderFilterMemory.shared.clearAllVariables()
        
        self.customerId = 0
        self.supplierId = 0
        self.staffId = 0
        
        self.orderDocument = .none
        self.moneyMovement = .none
        
        self.moneyIncomeOutgoneSum = 0.0
        self.comments = ""
        self.billingNo = ""
    }
}
