//
//  Supplier.swift
//  Rentautomation
//
//  Created by kanybek on 11/28/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import FMDB

//open class SupplierRequest : GPBMessage {
//    open var supplierId: UInt64
//    open var supplierImagePath: String!
//    open var companyName: String!
//    open var contactFname: String!
//    open var phoneNumber: String!
//    open var address: String!
//}

class Supplier {
    
    var supplierId: UInt64
    
    var supplierImagePath: String?
    
    var companyName: String
    var contactFname: String
    var phoneNumber: String
    var address: String
    
    convenience init() {
        self.init(supplierId: 0,
                  supplierImagePath: "",
                  companyName: "",
                  contactFname: "",
                  phoneNumber: "",
                  address: "")
    }
    
    init(supplierId: UInt64,
         supplierImagePath: String?,
         companyName: String,
         contactFname: String,
         phoneNumber: String,
         address: String) {
        
        self.supplierId = supplierId
        self.supplierImagePath = supplierImagePath
        self.companyName = companyName
        self.contactFname = contactFname
        self.phoneNumber = phoneNumber
        self.address = address
    }
    
    deinit {
    }
    
    var description: String {
        return "_\(self.supplierId)__\(self.companyName)"
    }
    
    //try self.database.executeUpdate("CREATE TABLE IF NOT EXISTS \(self.suppliersTableName) (supplier_id INTEGER PRIMARY KEY, supplier_image_path TEXT, company_name TEXT, contact_fname TEXT, phone_number TEXT, address TEXT)", values: nil)
    func databaseInsertValues() -> [Any] {
        return [self.supplierId, self.supplierImagePath ?? "", self.companyName, self.contactFname, self.phoneNumber, self.address]
    }
    
    func databaseUpdateValues() -> [Any] {
        return [self.supplierImagePath ?? "", self.companyName, self.contactFname, self.phoneNumber, self.address, self.supplierId]
    }
    
    static func supplierFrom(queryResult: FMResultSet) -> Supplier {
        
        let supplierId = queryResult.unsignedLongLongInt(forColumn: "supplier_id")
        let supplierImagePath = queryResult.string(forColumn: "supplier_image_path")
        let companyName   = queryResult.string(forColumn: "company_name")
        let contactFname   = queryResult.string(forColumn: "contact_fname")
        let phoneNumber   = queryResult.string(forColumn: "phone_number")
        let address   = queryResult.string(forColumn: "address")
        
        let supplier = Supplier(supplierId: supplierId,
                                supplierImagePath: supplierImagePath!,
                                companyName: companyName!,
                                contactFname: contactFname!,
                                phoneNumber: phoneNumber!,
                                address: address!)
        
        return supplier
    }
}
