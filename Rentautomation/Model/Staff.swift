//
//  Staff.swift
//  Rentautomation
//
//  Created by kanybek on 11/28/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import FMDB

public enum StaffRoleType : UInt {
    case owner = 1000
    case admin = 2000
    case agent = 3000
}

class Staff: CustomStringConvertible {
    
    var staffId: UInt64
    var isCurrentStaff: Bool
    var roleId: UInt64
    var staffImagePath: String?
    var firstName: String
    var secondName: String
    var email: String
    var password: String
    var phoneNumber: String
    var address: String
    
    convenience init() {
        self.init(staffId: 0, isCurrentStaff: false, roleId: 0, staffImagePath: "", firstName: "", secondName: "", email: "", password: "", phoneNumber: "", address: "")
    }
    
    init(staffId: UInt64, isCurrentStaff: Bool, roleId: UInt64, staffImagePath: String?, firstName: String, secondName: String, email: String, password: String, phoneNumber: String, address: String) {
        self.staffId = staffId
        self.isCurrentStaff = isCurrentStaff
        self.roleId = roleId
        self.staffImagePath = staffImagePath
        self.firstName = firstName
        self.secondName = secondName
        self.email = email
        self.password = password
        self.phoneNumber = phoneNumber
        self.address = address
    }
    
    deinit {
    }
    
    var description: String {
        return "_\(self.staffId)__\(self.firstName)"
    }
    
    //try self.database.executeUpdate("CREATE TABLE IF NOT EXISTS \(self.staffTableName) (staff_id INTEGER PRIMARY KEY, is_current_staff INTEGER, role_id INTEGER, staff_image_path TEXT, first_name TEXT, second_name TEXT, email TEXT, password TEXT, phone_number TEXT, address TEXT)", values: nil)
    
    func databaseInsertValues() -> [Any] {
        
        var isCurrentStaffValue: Int8 = 0
        if self.isCurrentStaff {
            isCurrentStaffValue = 1
        }
        
        return [self.staffId, isCurrentStaffValue, self.roleId, self.staffImagePath ?? "", self.firstName, self.secondName, self.email, self.password, self.phoneNumber, self.address]
    }
    
    func databaseUpdateValues() -> [Any] {
        
        var isCurrentStaffValue: Int8 = 0
        if self.isCurrentStaff {
            isCurrentStaffValue = 1
        }
        
        return [isCurrentStaffValue, self.roleId, self.staffImagePath ?? "", self.firstName, self.secondName, self.email, self.password, self.phoneNumber, self.address, self.staffId]
    }
    
    static func staffFrom(queryResult: FMResultSet) -> Staff {
        
        let staffId = queryResult.unsignedLongLongInt(forColumn: "staff_id")
        var isCurrentStaff: Bool = false
        let isCurrentStaffInt = queryResult.int(forColumn: "is_current_staff")
        if isCurrentStaffInt == 1 {
            isCurrentStaff = true
        }
        
        let roleId = queryResult.unsignedLongLongInt(forColumn: "role_id")
        let staffImagePath = queryResult.string(forColumn: "staff_image_path")
        let firstName   = queryResult.string(forColumn: "first_name")
        let secondName   = queryResult.string(forColumn: "second_name")
        let email   = queryResult.string(forColumn: "email")
        let password   = queryResult.string(forColumn: "password")
        let phoneNumber   = queryResult.string(forColumn: "phone_number")
        let address   = queryResult.string(forColumn: "address")
        
        let staff = Staff(staffId: staffId,
                          isCurrentStaff: isCurrentStaff,
                          roleId: roleId,
                          staffImagePath: staffImagePath!,
                          firstName: firstName!,
                          secondName: secondName!,
                          email: email!,
                          password: password!,
                          phoneNumber: phoneNumber!,
                          address: address!)
        
        return staff
    }
}
