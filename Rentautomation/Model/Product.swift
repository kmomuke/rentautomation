//
//  Product.swift
//  Rentautomation
//
//  Created by kanybek on 11/28/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import FMDB

//open class ProductRequest : GPBMessage {
//    open var productId: UInt64
//    open var productImagePath: String!
//    open var productName: String!
//    open var supplierId: UInt64
//    open var categoryId: UInt64
//    open var barcode: String!
//    open var quantityPerUnit: String!
//    open var saleUnitPrice: Double
//    open var incomeUnitPrice: Double
//    open var unitsInStock: Double
//}

class Product: CustomStringConvertible {

    var productId: UInt64
    var productImagePath: String?
    var productName: String
    var supplierId: UInt64
    var categoryId: UInt64
    
    var barcode: String
    var quantityPerUnit: String
    var saleUnitPrice: Double
    var incomeUnitPrice: Double
    var unitsInStock: Double
    
    convenience init() {
        self.init(productId: 0, productImagePath: "", productName: "", supplierId: 0, categoryId: 0, barcode: "",  quantityPerUnit:
            "", saleUnitPrice: 0.0, incomeUnitPrice: 0.0, unitsInStock: 0.0)
    }
    
    init(productId: UInt64, productImagePath: String?, productName: String, supplierId: UInt64, categoryId: UInt64, barcode: String, quantityPerUnit: String, saleUnitPrice: Double, incomeUnitPrice: Double, unitsInStock: Double) {
        
        self.productId = productId
        self.productImagePath = productImagePath
        self.productName = productName
        self.supplierId = supplierId
        self.categoryId = categoryId
        self.barcode = barcode
        self.quantityPerUnit = quantityPerUnit
        self.saleUnitPrice = saleUnitPrice
        self.incomeUnitPrice = incomeUnitPrice
        self.unitsInStock = unitsInStock
    }
    
    deinit {
    }
    
    var description: String {
        return "_\(self.productId)__\(self.productName)"
    }
    
    //try self.database.executeUpdate("CREATE TABLE IF NOT EXISTS \(self.productsTableName) (product_id INTEGER PRIMARY KEY, product_image_path TEXT, product_name TEXT, supplier_id INTEGER, category_id INTEGER, barcode TEXT, quantity_per_unit TEXT, sale_unit_price REAL, income_unit_price REAL, units_in_stock REAL)", values: nil)

    func databaseInsertValues() -> [Any] {
        return [self.productId, self.productImagePath ?? "", self.productName, self.supplierId, self.categoryId, self.barcode, self.quantityPerUnit, self.saleUnitPrice, self.incomeUnitPrice, self.unitsInStock]
    }
    
    func databaseUpdateValues() -> [Any] {
        return [self.productImagePath ?? "", self.productName, self.supplierId, self.categoryId, self.barcode, self.quantityPerUnit, self.saleUnitPrice, self.incomeUnitPrice, self.unitsInStock, self.productId]
    }
    
    static func productFrom(queryResult: FMResultSet) -> Product {
        
        let productId = queryResult.unsignedLongLongInt(forColumn: "product_id")
        
        let productImagePath = queryResult.string(forColumn: "product_image_path")
        let productName = queryResult.string(forColumn: "product_name")
        
        let supplierId = queryResult.unsignedLongLongInt(forColumn: "supplier_id")
        let categoryId = queryResult.unsignedLongLongInt(forColumn: "category_id")
        
        let barcode = queryResult.string(forColumn: "barcode")
        let quantityPerUnit = queryResult.string(forColumn: "quantity_per_unit")
        
        let saleUnitPrice = queryResult.double(forColumn: "sale_unit_price")
        let incomeUnitPrice = queryResult.double(forColumn: "income_unit_price")
        let unitsInStock = queryResult.double(forColumn: "units_in_stock")
        
        let product = Product(productId: productId,
                              productImagePath: productImagePath!,
                              productName: productName!,
                              supplierId: supplierId,
                              categoryId: categoryId,
                              barcode: barcode!,
                              quantityPerUnit: quantityPerUnit!,
                              saleUnitPrice: saleUnitPrice,
                              incomeUnitPrice: incomeUnitPrice,
                              unitsInStock: unitsInStock)
        
        return product
    }
    
}
