//
//  SyncOperation.swift
//  Rentautomation
//
//  Created by kanybek on 1/18/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import FMDB


/*

 (1)   ==>   operation should be marked as operated or not operated
 
*/

/*

 // -------------------------------------- PRODUCT -------------------------------------- //
 
 (1) create product operation ===> Operation model should include ==>  productId, orderDetailId
 RPC method create Product(includes OrderDetail), on response we should update productId, and replace on index search,
 also should update OrderDetail orderDetailId
 
 Product{
    productId
    OrderDetail{
 
    }
 }
 
 
 (2) update Product operation ===> Operation model should include ==> productId, orderDetailId
 RPC method update Product(includes OrderDetail), on response we should
 update OrderDetail orderDetailId

 Product{
    productId
    OrderDetail{
 
    }
 }
 
 // -------------------------------------- CUSTOMER -------------------------------------- //
 (3) create Customer operation ===> Operation model should include ==> customerId, transactionId, accountId
 RPC method create Customer(includes Transaction, Account), 
 on response we should update customerId, transactionId, accountId
 
 (4) update Customer operation ===> Operation model should include ==> customerId
 
 (5) owner can correct Customer balance in Account ==>
 Operation model should include ==> customerId, transactionId, accountId
 for this operation first everything should be updated with backend server.
 
 
 // -------------------------------------- Supplier -------------------------------------- //
 (6) create Supplier operation ===> Operation model should include ==> supplierId, transactionId, accountId
 RPC method create Supplier(includes Transaction, Account),
 on response we should update supplierId, transactionId, accountId
 
 (7) update Supplier operation ===> Operation model should include ==> supplierId
 
 (8) owner can correct Supplier balance in Account ==>
 Operation model should include ==> customerId, transactionId, accountId
 for this operation first everything should be updated with backend server.
 
 
 // -------------------------------------- Staff -------------------------------------- //
 (9) create Staff operation ===> Operation model should include ==> staffId
 RPC method create Staff(),
 on response we should update staffId
 
 (10) update Staff operation ===> Operation model should include ==> supplierId
*/


public enum EventType : UInt {
    
    case none = 0
    
    case productCreated = 1000
    case productUpdated = 2000
    
    case staffCreated = 3000
    case staffUpdated = 4000
    
    case customerCreated = 5000
    case customerUpdated = 6000
    case customerBalanceUpdated = 7000
    
    case supplierCreated = 8000
    case supplierUpdated = 9000
    case supplierBalanceUpdated = 10000
    
    case categoryCreated = 11000
    case categoryUpdated = 12000
    
    case orderCreated = 13000
    case orderUpdated = 14000
    
    case transactionUpdated = 15000
}

class Event: CustomStringConvertible {
    
    var syncOperationId: UInt64
    var isSynchronized: Bool
    
    var syncOperationDate: Date
    var operationType: EventType
    
    // ------- Product CREATE/UPDATE ------- //
    var productId: UInt64
    var orderDetailId: UInt64
    
    // ------- CUSTOMER CREATE/UPDATE ------- //
    var customerId: UInt64
    var transactionId: UInt64
    var accountId: UInt64
    
    // ------- SUPPLIER CREATE/UPDATE ------- //
    var supplierId: UInt64
    
    // ------- STAFF CREATE/UPDATE ------- //
    var staffId: UInt64
    
    // ------- CATEGORY CREATE/UPDATE ------- //
    var categoryId: UInt64
    
    // ------- ORDER CREATE ------- //
    var orderId: UInt64
    
    convenience init() {
        self.init(syncOperationId: 0,
                  isSynchronized: true,
                  syncOperationDate: Date(),
                  operationType: .none,
                  productId: 0,
                  orderDetailId: 0,
                  customerId: 0,
                  transactionId: 0,
                  accountId: 0,
                  supplierId: 0,
                  staffId: 0,
                  categoryId: 0,
                  orderId: 0)
    }
    
    init(syncOperationId: UInt64,
         isSynchronized: Bool,
         syncOperationDate: Date,
         operationType: EventType,
         productId: UInt64,
         orderDetailId: UInt64,
         customerId: UInt64,
         transactionId: UInt64,
         accountId: UInt64,
         supplierId: UInt64,
         staffId: UInt64,
         categoryId: UInt64,
         orderId: UInt64)
    {
        self.syncOperationId = syncOperationId
        self.isSynchronized = isSynchronized
        self.syncOperationDate = syncOperationDate
        self.operationType = operationType
        
        self.productId = productId
        self.orderDetailId = orderDetailId
        
        self.customerId = customerId
        self.transactionId = transactionId
        self.accountId = accountId
        
        self.supplierId = supplierId
        self.staffId = staffId
        
        self.categoryId = categoryId
        self.orderId = orderId
    }
    
    deinit {
    }
    
    var description: String {
        return "prdct=\(self.productId), orderDtl=\(self.orderDetailId), cust=\(self.customerId), trans=\(self.transactionId), accnt=\(self.accountId), suppl=\(self.supplierId), staff=\(self.staffId), ctg=\(self.categoryId), ordId=\(self.orderId)"
    }
    
    func databaseInsertValues() -> [Any] {
        
        let operation_date = UInt64(self.syncOperationDate.timeIntervalSince1970 * 1000)
        var isSynchronizedValue: Int8 = 0
        if self.isSynchronized {
            isSynchronizedValue = 1
        }
        
        let syncOperationTypeUInt = Event.operationTypeFromInteger(operationType: self.operationType)
        return [self.syncOperationId, isSynchronizedValue, operation_date, syncOperationTypeUInt, self.productId, self.orderDetailId, self.customerId, self.transactionId, self.accountId, self.supplierId, self.staffId, self.categoryId, self.orderId]
    }
    
    static func syncOpertaionFrom(queryResult: FMResultSet) -> Event {
        
        let syncoperationId = queryResult.unsignedLongLongInt(forColumn: "syncoperation_id")
        
        var isSynchronizedValue: Bool = false
        let isSynchronizedInt = queryResult.int(forColumn: "is_synchronized")
        if isSynchronizedInt == 1 {
            isSynchronizedValue = true
        }
        
        let syncDateInt   = queryResult.unsignedLongLongInt(forColumn: "syncoperation_date")
        let syncOpertaionDate = Date(timeIntervalSince1970: Double(syncDateInt/1000))

        let syncOperationTypeUInt   = queryResult.unsignedLongLongInt(forColumn: "sync_type") //UInt64
        let syncType = Event.operationTypeFromInteger(syncOperationTypeUInt)
        
        let productId_   = queryResult.unsignedLongLongInt(forColumn: "product_id")
        let orderDetailId_   = queryResult.unsignedLongLongInt(forColumn: "order_detail_id")
        
        let customerId_   = queryResult.unsignedLongLongInt(forColumn: "customer_id")
        let transactionId_   = queryResult.unsignedLongLongInt(forColumn: "transaction_id")
        let accountId_   = queryResult.unsignedLongLongInt(forColumn: "account_id")
        let supplierId_   = queryResult.unsignedLongLongInt(forColumn: "supplier_id")
        let staffId_   = queryResult.unsignedLongLongInt(forColumn: "staff_id")
        
        let categoryId = queryResult.unsignedLongLongInt(forColumn: "category_id")
        let orderId = queryResult.unsignedLongLongInt(forColumn: "order_id")
        
        let sync = Event(syncOperationId: syncoperationId,
                                 isSynchronized: isSynchronizedValue,
                                 syncOperationDate: syncOpertaionDate,
                                 operationType: syncType,
                                 productId: productId_,
                                 orderDetailId: orderDetailId_,
                                 customerId: customerId_,
                                 transactionId: transactionId_,
                                 accountId: accountId_,
                                 supplierId: supplierId_,
                                 staffId: staffId_,
                                 categoryId: categoryId,
                                 orderId: orderId)
        return sync
    }
    
    
    static func operationTypeFromInteger(_ operationTypeUInt: UInt64) -> EventType {
        
        var syncType: EventType = .none
        if operationTypeUInt == 0 {
            syncType = .none
            
        } else if operationTypeUInt == 1000 {
            syncType = .productCreated
        } else if operationTypeUInt == 2000 {
            syncType = .productUpdated
            
            
        } else if operationTypeUInt == 3000 {
            syncType = .staffCreated
        } else if operationTypeUInt == 4000 {
            syncType = .staffUpdated
            
            
        } else if operationTypeUInt == 5000 {
            syncType = .customerCreated
        } else if operationTypeUInt == 6000 {
            syncType = .customerUpdated
        } else if operationTypeUInt == 7000 {
            syncType = .customerBalanceUpdated
            
            
        } else if operationTypeUInt == 8000 {
            syncType = .supplierCreated
        } else if operationTypeUInt == 9000 {
            syncType = .supplierUpdated
        } else if operationTypeUInt == 10000 {
            syncType = .supplierBalanceUpdated
            
            
        } else if operationTypeUInt == 11000 {
            syncType = .categoryCreated
        } else if operationTypeUInt == 12000 {
            syncType = .categoryUpdated
            

        } else if operationTypeUInt == 13000 {
            syncType = .orderCreated
        } else if operationTypeUInt == 14000 {
            syncType = .orderUpdated
            
        } else if operationTypeUInt == 15000 {
            syncType = .transactionUpdated

        } else {
            syncType = .none
        }
        
        return syncType
    }
    
    static func operationTypeFromInteger(operationType: EventType) -> UInt64 {
        
        var syncOperationTypeUInt: UInt64 = 0
        switch operationType {
            
        case .none:
            syncOperationTypeUInt = 0
            
        case .productCreated:
            syncOperationTypeUInt = 1000
        case .productUpdated:
            syncOperationTypeUInt = 2000
            
        case .staffCreated:
            syncOperationTypeUInt = 3000
        case .staffUpdated:
            syncOperationTypeUInt = 4000
            
        case .customerCreated:
            syncOperationTypeUInt = 5000
        case .customerUpdated:
            syncOperationTypeUInt = 6000
        case .customerBalanceUpdated:
            syncOperationTypeUInt = 7000
            
        case .supplierCreated:
            syncOperationTypeUInt = 8000
        case .supplierUpdated:
            syncOperationTypeUInt = 9000
        case .supplierBalanceUpdated:
            syncOperationTypeUInt = 10000
        
        case .categoryCreated:
            syncOperationTypeUInt = 11000
        case .categoryUpdated:
            syncOperationTypeUInt = 12000
        
        case .orderCreated:
            syncOperationTypeUInt = 13000
        case .orderUpdated:
            syncOperationTypeUInt = 14000
        
        case .transactionUpdated:
            syncOperationTypeUInt = 15000
        
        default:
            syncOperationTypeUInt = 0
        }
        
        return syncOperationTypeUInt
    }
    

    static func syncOperationTypeStringName(syncType: EventType) -> String {
        
        var operationStringName: String = ""
        
        switch syncType {
            
        case .none:
            operationStringName = "Пусто"
            
        case .productCreated:
            operationStringName = "Продукт создан"
        case .productUpdated:
            operationStringName = "Продукт редактирован"
            
        case .staffCreated:
            operationStringName = "Сотрудник создан"
        case .staffUpdated:
            operationStringName = "Сотрудник редактирован"
            
        case .customerCreated:
            operationStringName = "Клиент создан"
        case .customerUpdated:
            operationStringName = "Клиент редактирован"
        case .customerBalanceUpdated:
            operationStringName = "Баланс клиента изменен"
            
        case .supplierCreated:
            operationStringName = "Поставщик создан"
        case .supplierUpdated:
            operationStringName = "Поставщик редактирован"
        case .supplierBalanceUpdated:
            operationStringName = "Баланс поставщика изменен"
            
        case .categoryCreated:
            operationStringName = "Категория создан"
        case .categoryUpdated:
            operationStringName = "Категория редактирован"
            
        case .orderCreated:
            operationStringName = "Документ создан"
        case .orderUpdated:
            operationStringName = "Документ редактирован"
            
        case .transactionUpdated:
            operationStringName = "Комментарии к балансу обновлен"
            
        default:
            operationStringName = "Пусто"
        }
        
        return operationStringName
    }
    
}
