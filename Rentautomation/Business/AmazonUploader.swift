//
//  AmazonUploader.swift
//  Rentautomation
//
//  Created by kanybek on 2/27/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import Alamofire
import SwiftSignalKit
import Kingfisher
import SVProgressHUD

final class AmazonUploader {
    
    static let shared: AmazonUploader = AmazonUploader()
    
    var amazonS3Manager: AmazonS3RequestManager
    var uploadDispose: DisposableSet
    
    var isUploading: Bool
    
    init () {
        isUploading = false
        uploadDispose = DisposableSet()
        amazonS3Manager = AmazonS3RequestManager(bucket: "rentautomation",
                                                 region: .USStandard,
                                                 accessKey: "AKIAIPSI327VOXCUZGFA",
                                                 secret: "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")
    }
    
    func uploadAndChangeAllProductImages(completion: @escaping (Void) -> ()) {
        
        if self.isUploading {
            return
        }
        
        if !self.isUploading {
            self.isUploading = true
            
            var uploadImageSignals: [Signal<String, Error>] = []
            
            generateQue.async {
                
                let products = Database.shared.loadAllProducts(10000)
                let totalCount_ = products.count
                for (index_, product_) in products.enumerated() {
                    if let uploadSignal = self.uploadImageFor(product: product_,
                                                              index: index_,
                                                              totalCount: totalCount_) {
                        uploadImageSignals.append(uploadSignal)
                    }
                }
                
                DispatchQueue.main.async {
                    
                    var startingSignal = Signal<String, Error> { subscriber in
                        print("starting upload images")
                        SVProgressHUD.show(withStatus: "Начало выгрузки картинки")
                        subscriber.putNext("Hello world")
                        subscriber.putCompletion()
                        return ActionDisposable {
                            print("disposed upload images")
                        }
                    }
                    
                    for signal in uploadImageSignals.prefix(500) {
                        startingSignal = startingSignal |> then(signal)
                    }
                    
                    let finalSignal = startingSignal |> runOn(generateQue)
                    
                    let disposable = finalSignal.start(next: { (s: String) in
                    }, error: { (e: Error) in
                        self.isUploading = false
                        SVProgressHUD.showError(withStatus: e.localizedDescription)
                    }, completed: {
                        self.isUploading = false
                        SVProgressHUD.dismiss()
                        completion()
                    })
                    
                    self.uploadDispose.add(disposable)
                }
            }
        }
    }
    
    private func uploadImageFor(product: Product, index: Int, totalCount: Int) -> Signal<String, Error>? {
        
        if let productImagePath_ = product.productImagePath {
            if (productImagePath_.characters.count > 4) {
                if productImagePath_.lowercased().range(of:"https") != nil {
                    //already uploaded.
                } else {
                    // not uploaded
                    if let productImage_ = ImageCache.default.retrieveImageInDiskCache(forKey: productImagePath_) {
                        return self.upload(product: product,
                                           productImage: productImage_,
                                           index: index,
                                           totalCount: totalCount)
                    } else {
                        // image does not exsists, nnullify it
                        product.productImagePath = nil
                        self.update(product: product)
                    }
                }
            }
        }
        
        return nil
    }
    
    func upload(product: Product, productImage: UIImage, index: Int, totalCount: Int) -> Signal<String, Error> {
        
        let updateOrderRpcSignal = Signal<String, Error> { subscriber in
            
            let dataImage = UIImageJPEGRepresentation(productImage, 0.5)!
            
            var path: String = ""
            if let imagePath_ = product.productImagePath {
                path = imagePath_ + ".jpeg"
            }
            
            print("upload image path is: \(path)")
            let uploadReq: UploadRequest = self.amazonS3Manager.upload(dataImage, to: path)
            
            uploadReq.uploadProgress(queue: DispatchQueue.main) { (progres: Progress) in
                print("progres.fractionCompleted = \(progres.fractionCompleted)")
                DispatchQueue.main.async {
                    let title_ = "Из \(totalCount) идет \(index)"
                    SVProgressHUD.showProgress(Float(progres.fractionCompleted),
                                               status: title_)
                }
            }
            
            uploadReq.responseS3MetaData { (response: DataResponse<S3ObjectMetaData>) in
                
                if let error_ = response.error {
                    subscriber.putError(error_)
                    subscriber.putCompletion()
                }
                
                if let url = response.response?.url {
                    
                    print("url.absoluteString = \(url.absoluteString)")
                    
                    ImageCache.default.store(productImage, forKey: url.absoluteString)
                    ImageCache.default.removeImage(forKey: product.productImagePath!)
                    
                    product.productImagePath = url.absoluteString
                    self.update(product: product)
                    
                    subscriber.putNext(url.absoluteString)
                    subscriber.putCompletion()
                }
            }
            return ActionDisposable {
                uploadReq.cancel()
            }
        }
        return updateOrderRpcSignal
    }
    
    func update(product: Product) -> Void {
        if Database.shared.isModelSynchronized(primaryKey: product.productId) {
            
            Database.shared.updateProduct(product)
            
            let orderDetail = OrderDetail()
            orderDetail.orderDetailId = Database.shared.uniqueIncreasingPrimaryKey()
            orderDetail.orderDetailDate = Date()
            orderDetail.isLast = false
            orderDetail.productId = product.productId
            orderDetail.orderQuantity = product.unitsInStock
            orderDetail.price = product.saleUnitPrice
            orderDetail.productQuantity = product.unitsInStock
            orderDetail.orderId = 0
            orderDetail.billingNo = "\(Date().toString(.custom("EEE, dd MMM HH:mm"))) "
            
            Database.shared.createorderDetails([orderDetail])
            
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                          isSynchronized: false,
                                          syncOperationDate: Date(),
                                          operationType: .productUpdated,
                                          productId: product.productId,
                                          orderDetailId: orderDetail.orderDetailId,
                                          customerId: 0,
                                          transactionId: 0,
                                          accountId: 0,
                                          supplierId: 0,
                                          staffId: 0,
                                          categoryId: 0,
                                          orderId: 0)
            
            Database.shared.createOperations([operation])
        }
    }
    
    
    func testAmazonS3Upload() -> Void {
        
        let path = "\(UInt64(Date().timeIntervalSince1970 * 1000)).jpeg"

        let image = UIImage(named: "sampleImage.png")!
        let dataImage = UIImageJPEGRepresentation(image, 0.5)!
        
        
        print("path = \(path)")
        let uploadReq: UploadRequest = self.amazonS3Manager.upload(dataImage, to: path)
        uploadReq.uploadProgress(queue: DispatchQueue.main) { (progres: Progress) in
            print("progres.fractionCompleted = \(progres.fractionCompleted)")
        }
        
//        uploadReq.responseS3Object { (DataResponse<ResponseObjectSerializable>) in
//
//        }
//        uploadReq.response(queue: DispatchQueue?, responseSerializer: DataResponseSerializerProtocol) { (DataResponse<T.SerializedObject>) in
//            
//        }
        
        
        uploadReq.responseS3MetaData { (response: DataResponse<S3ObjectMetaData>) in
            
            print("----")
            print("response = \(response.response)")
            print("----")
            
            if let url = response.response?.url {
                print("url.absoluteString = \(url.absoluteString)")
            }
            
            if let data_ = response.data {
                let dataString = String(data: data_, encoding: String.Encoding.utf8)
                print("dataString = \(dataString)")
            }
        }
    }
}
