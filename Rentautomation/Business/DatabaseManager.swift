//
//  DatabaseManager.swift
//  mobilnik
//
//  Created by kanybek on 8/8/16.
//  Copyright © 2016 Mobilnik. All rights reserved.
//

import UIKit
import FMDB
import SwiftSignalKit

extension String {
    func stringByAppendingPathComponent(path: String) -> String {
        let nsSt = self as NSString
        return nsSt.appendingPathComponent(path)
    }
}

let databaseQueue: Queue = Queue(name: "database.queueue")
let databaseIndexQueue: Queue = Queue(name: "database.index.queueue")

final class Database {
    
    let conversationUpdatedPipe = ValuePipe<Bool>()
    let bigValue: UInt64 = 1485144919289
    var database: FMDatabase!
    var indexDatabase: FMDatabase!
    
    var databasePath: String
    var databaseName: String = "rentautomation"
    var baseIndexDatabasePath: String
    
    var schemaVersion: Int = 29
    
    var usersTableName: String
    var rolesTableName: String
    var customersTableName: String
    var ordersTableName: String
    var orderDetailsTableName: String
    var paymentsTableName: String
    var productsTableName: String
    var productsIndexTableName: String
    var categoriesTableName: String
    var suppliersTableName: String
    
    var accountsTableName: String
    var transactionsTableName: String
    var syncOperationsTableName: String
    
    static let shared: Database = Database()
    
    init () {
        let name: String = "\(databaseName)" + ".db"
        let indexName: String = "\(databaseName)" + "_index.db"
        let baseDatabasePath: String = Database.documentsPath().stringByAppendingPathComponent(path: name)
        self.databasePath = baseDatabasePath
        self.baseIndexDatabasePath = Database.documentsPath().stringByAppendingPathComponent(path: indexName)
        
        self.usersTableName = "users_v\(self.schemaVersion)"
        self.rolesTableName = "roles_v\(self.schemaVersion)"
        self.customersTableName = "customers_v\(self.schemaVersion)"
        self.ordersTableName = "orders_v\(self.schemaVersion)"
        self.orderDetailsTableName = "orderDetails_v\(self.schemaVersion)"
        self.paymentsTableName = "payments_v\(self.schemaVersion)"
        self.productsTableName = "products_v\(self.schemaVersion)"
        self.categoriesTableName = "categories_v\(self.schemaVersion)"
        self.suppliersTableName = "suppliers_v\(self.schemaVersion)"
        self.accountsTableName = "accounts_v\(self.schemaVersion)"
        self.transactionsTableName = "transactions_v\(self.schemaVersion)"
        self.syncOperationsTableName = "syncoperations_v\(self.schemaVersion)"
        
        self.productsIndexTableName = "product_index_v\(self.schemaVersion)"
        
        databaseQueue.async { (Void) in
            self.initDatabase()
        }
    }
   
    static func documentsPath() -> String {
        return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    }
    
    private func initDatabase() -> Void {
        self.reopenDatabase()
        databaseIndexQueue.async {
            self.reopenIndexDatabase()
        }
        self.upgradeTables()
    }
    
    private func reopenDatabase() -> Void {
        
        self.database = FMDatabase(path: self.databasePath)
        
        if (!self.database.open())
        {
            print("***** Error: couldn't open database! *****")
            do {
                try FileManager().removeItem(atPath: self.databasePath)
            }
            catch let error as NSError {
                print("error ==>:", error)
            }
            self.initDatabase()
            return;
        }
        
        do {
           try NSURL(fileURLWithPath: self.databasePath).setResourceValue(NSNumber(value: true), forKey: URLResourceKey.isExcludedFromBackupKey)
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
        
        self.database.setShouldCacheStatements(true)
        self.database.logsErrors = true
    }
    
    
    private func reopenIndexDatabase() -> Void {
        
        self.indexDatabase = FMDatabase(path: self.baseIndexDatabasePath)
        
        if (!self.indexDatabase.open())
        {
            print("***** Error: couldn't open index database! *****")
            do {
                try FileManager().removeItem(atPath: self.baseIndexDatabasePath)
            }
            catch let error as NSError {
                print("error ==>:", error)
            }
        }
        
        self.indexDatabase.setShouldCacheStatements(true)
        self.indexDatabase.logsErrors = true

        //self.populateDatabase()
    }
    
    func populateDatabase() -> Void {
        do {
            try self.indexDatabase.executeUpdate("CREATE VIRTUAL TABLE IF NOT EXISTS kano USING fts4(text TEXT, matchinfo=fts3)", values: nil)
            try self.indexDatabase.executeUpdate("INSERT INTO kano (docid, text) VALUES (?, ?)", values: [NSNumber(value: 3), "hello world"])
            try self.indexDatabase.executeUpdate("INSERT INTO kano (docid, text) VALUES (?, ?)", values: [NSNumber(value: 32), "This message is a hello world message"])
            try self.indexDatabase.executeUpdate("INSERT INTO kano (docid, text) VALUES (?, ?)", values: [NSNumber(value: 34), "Коть бля ты что намудрил чудак а ааааа"])
            try self.indexDatabase.executeUpdate("INSERT INTO kano (docid, text) VALUES (?, ?)", values: [NSNumber(value: 36), "Антоша давай колбась картошку, мудила"])
        }
        catch let error as NSError {
            print("error ==>:", error)
        }
        
        let querryStr2 = "SELECT docid FROM kano WHERE text MATCH '*hel*' ORDER BY docid DESC"
        let result2 = self.indexDatabase.executeQuery(querryStr2, withArgumentsIn:nil)!
        let docidIndex = result2.columnIndex(forName: "docid")
        
        while result2.next() {
            let productId = result2.unsignedLongLongInt(forColumnIndex: docidIndex)
        }
        
        print("FINISHED")
    }
    
    
    private func upgradeTables() -> Void {
        
        //for (int i = _schemaVersion - 2; i < _schemaVersion + 2; i++)
        //{
        //    if (i != _schemaVersion)
        //    {
        //        [_database executeUpdate:[NSString stringWithFormat:@"DROP TABLE IF EXISTS %@", [NSString stringWithFormat:@"users_v%d", i]]];
        //    }
        //}

        do {
            try self.database.executeUpdate("CREATE TABLE IF NOT EXISTS \(self.usersTableName) (user_id INTEGER PRIMARY KEY, uuid TEXT, is_current_user INTEGER, role_id INTEGER, user_image_path TEXT, first_name TEXT, second_name TEXT, email TEXT, password TEXT, phone_number TEXT, address TEXT)", values: nil)
            
            try self.database.executeUpdate("CREATE INDEX IF NOT EXISTS email_user_idx ON \(self.usersTableName) (email)", values: nil)
            
            try self.database.executeUpdate("CREATE TABLE IF NOT EXISTS \(self.rolesTableName) (role_id INTEGER PRIMARY KEY, role_name TEXT)", values: nil)
            
            try self.database.executeUpdate("CREATE TABLE IF NOT EXISTS \(self.customersTableName) (customer_id INTEGER PRIMARY KEY, customer_image_path TEXT, first_name TEXT, second_name TEXT, phone_number TEXT, address TEXT, user_id INTEGER)", values: nil)
            
            try self.database.executeUpdate("CREATE TABLE IF NOT EXISTS \(self.ordersTableName) (order_id INTEGER PRIMARY KEY, order_document INTEGER, order_movement INTEGER, money_movement INTEGER, billing_no TEXT, user_id INTEGER, customer_id INTEGER, supplier_id INTEGER, order_date INTEGER, payment_id INTEGER, error_msg TEXT, comment TEXT, uuid TEXT, is_deleted INTEGER, is_money_for_debt INTEGER, is_editted INTEGER)", values: nil)
            
            try self.database.executeUpdate("CREATE INDEX IF NOT EXISTS user_id_orders_idx ON \(self.ordersTableName) (user_id)", values: nil)
            
            try self.database.executeUpdate("CREATE INDEX IF NOT EXISTS customer_id_orders_idx ON \(self.ordersTableName) (customer_id)", values: nil)
            
            try self.database.executeUpdate("CREATE INDEX IF NOT EXISTS supplier_id_orders_idx ON \(self.ordersTableName) (supplier_id)", values: nil)
            
            try self.database.executeUpdate("CREATE INDEX IF NOT EXISTS payment_id_orders_idx ON \(self.ordersTableName) (payment_id)", values: nil)
            
            
            try self.database.executeUpdate("CREATE TABLE IF NOT EXISTS \(self.orderDetailsTableName) (order_detail_id INTEGER PRIMARY KEY, order_id INTEGER, order_detail_date INTEGER, is_last INTEGER, product_id INTEGER, billing_no TEXT, comment TEXT, uuid TEXT, price REAL, order_quantity REAL, product_quantity REAL)", values: nil)
            
            try self.database.executeUpdate("CREATE INDEX IF NOT EXISTS order_id_order_details_idx ON \(self.orderDetailsTableName) (order_id)", values: nil)
            
            try self.database.executeUpdate("CREATE INDEX IF NOT EXISTS product_id_order_details_idx ON \(self.orderDetailsTableName) (product_id)", values: nil)
            
            
            try self.database.executeUpdate("CREATE TABLE IF NOT EXISTS \(self.paymentsTableName) (payment_id INTEGER PRIMARY KEY, total_order_price REAL, discount REAL, total_price_with_discount REAL, minus_price REAL, plus_price REAL, comment TEXT)", values: nil)
            
            try self.database.executeUpdate("CREATE TABLE IF NOT EXISTS \(self.productsTableName) (product_id INTEGER PRIMARY KEY, product_image_path TEXT, product_name TEXT, supplier_id INTEGER, category_id INTEGER, barcode TEXT, quantity_per_unit TEXT, sale_unit_price REAL, income_unit_price REAL, units_in_stock REAL)", values: nil)
            
            try self.database.executeUpdate("CREATE INDEX IF NOT EXISTS supplier_id_products_idx ON \(self.productsTableName) (supplier_id)", values: nil)
            
            try self.database.executeUpdate("CREATE INDEX IF NOT EXISTS category_id_products_idx ON \(self.productsTableName) (category_id)", values: nil)
            
            try self.database.executeUpdate("CREATE INDEX IF NOT EXISTS barcode_products_idx ON \(self.productsTableName) (barcode)", values: nil)
            //try self.database.executeUpdate("CREATE UNIQUE INDEX IF NOT EXISTS barcode ON \(self.productsTableName) (barcode)", values: nil)
            
            try self.database.executeUpdate("CREATE TABLE IF NOT EXISTS \(self.categoriesTableName) (category_id INTEGER PRIMARY KEY, category_name TEXT)", values: nil)
            
            try self.database.executeUpdate("CREATE TABLE IF NOT EXISTS \(self.suppliersTableName) (supplier_id INTEGER PRIMARY KEY, supplier_image_path TEXT, company_name TEXT, contact_fname TEXT, phone_number TEXT, address TEXT)", values: nil)
            
            try self.database.executeUpdate("CREATE TABLE IF NOT EXISTS \(self.accountsTableName) (account_id INTEGER PRIMARY KEY, customer_id INTEGER, supplier_id INTEGER, balance REAL)", values: nil)
            
            try self.database.executeUpdate("CREATE INDEX IF NOT EXISTS customer_id_accounts_idx ON \(self.accountsTableName) (customer_id)", values: nil)
            
            try self.database.executeUpdate("CREATE INDEX IF NOT EXISTS supplier_id_accounts_idx ON \(self.accountsTableName) (supplier_id)", values: nil)
            
            try self.database.executeUpdate("CREATE TABLE IF NOT EXISTS \(self.transactionsTableName) (transaction_id INTEGER PRIMARY KEY, transaction_date INTEGER, is_last_transaction INTEGER, transaction_type INTEGER, money_amount REAL, order_id INTEGER, customer_id INTEGER, supplier_id INTEGER, user_id INTEGER, ballance_amount REAL, comment TEXT, uuid TEXT)", values: nil)
            
            try self.database.executeUpdate("CREATE INDEX IF NOT EXISTS order_id_transactions_idx ON \(self.transactionsTableName) (order_id)", values: nil)
            
            try self.database.executeUpdate("CREATE INDEX IF NOT EXISTS customer_id_transactions_idx ON \(self.transactionsTableName) (customer_id)", values: nil)
            
            try self.database.executeUpdate("CREATE INDEX IF NOT EXISTS supplier_id_transactions_idx ON \(self.transactionsTableName) (supplier_id)", values: nil)
            
            try self.database.executeUpdate("CREATE INDEX IF NOT EXISTS user_id_transactions_idx ON \(self.transactionsTableName) (user_id)", values: nil)
            
            try self.database.executeUpdate("CREATE TABLE IF NOT EXISTS \(self.syncOperationsTableName) (syncoperation_id INTEGER PRIMARY KEY, is_synchronized INTEGER, syncoperation_date INTEGER, sync_type INTEGER, product_id INTEGER, order_detail_id INTEGER, customer_id INTEGER, transaction_id INTEGER, account_id INTEGER, supplier_id INTEGER, staff_id INTEGER, category_id INTEGER, order_id INTEGER)", values: nil)
            
            try self.database.executeUpdate("CREATE INDEX IF NOT EXISTS syncoperation_date_syncoperations_idx ON \(self.syncOperationsTableName) (syncoperation_date)", values: nil)
            
            try self.database.executeUpdate("CREATE INDEX IF NOT EXISTS is_synchronized_syncoperations_idx ON \(self.syncOperationsTableName) (is_synchronized)", values: nil)
            
            try self.database.executeUpdate("CREATE INDEX IF NOT EXISTS customer_id_syncoperations_idx ON \(self.syncOperationsTableName) (customer_id)", values: nil)
            
            // ----- MIGRATIONS ---------
            if !self.table(tableName: self.ordersTableName, containsField: "order_movement") {
                print("\(self.ordersTableName) has not field order_movement")
                try self.database.executeUpdate("ALTER TABLE \(self.ordersTableName) ADD COLUMN order_movement INTEGER default \(11000)", values: nil)
            } else {
                print("\(self.ordersTableName) has field order_movement")
            }
            
            if !self.table(tableName: self.paymentsTableName, containsField: "minus_price") {
                print("\(self.paymentsTableName) has not field minus_price")
                try self.database.executeUpdate("ALTER TABLE \(self.paymentsTableName) ADD COLUMN minus_price REAL default \(0)", values: nil)
                try self.database.executeUpdate("ALTER TABLE \(self.paymentsTableName) ADD COLUMN plus_price REAL default \(0)", values: nil)
                try self.database.executeUpdate("ALTER TABLE \(self.paymentsTableName) ADD COLUMN comment TEXT", values: nil)
            } else {
                print("\(self.paymentsTableName) has field minus_price")
            }
            
            if !self.table(tableName: self.customersTableName, containsField: "staff_id") {
                print("\(self.customersTableName) has not field staff_id")
                try self.database.executeUpdate("ALTER TABLE \(self.customersTableName) ADD COLUMN staff_id INTEGER", values: nil)
            } else {
                print("\(self.customersTableName) has field staff_id")
            }
            
            if !self.table(tableName: self.customersTableName, containsField: "updated_at") {
                print("\(self.customersTableName) has not field updated_at")
                try self.database.executeUpdate("ALTER TABLE \(self.customersTableName) ADD COLUMN updated_at INTEGER default \(1000 * Date().timeIntervalSince1970)", values: nil)
            } else {
                print("\(self.customersTableName) has field updated_at")
            }
            
            databaseIndexQueue.async {
                do {
                    try self.indexDatabase.executeUpdate("CREATE VIRTUAL TABLE IF NOT EXISTS \(self.productsIndexTableName) USING fts4(text TEXT, matchinfo=fts3)", values: nil)
                } catch let error as NSError {
                    print("failed: \(error.localizedDescription)")
                }
            }
            
        } catch let error as NSError {
            print("failed: \(error.localizedDescription)")
        }
    }

    func closeDatabase() -> Void {
        
        databaseQueue.async { (Void) in
            self.database.close()
        }
        
        databaseIndexQueue.async { (Void) in
            self.indexDatabase.close()
        }
    }
    
    func dropDatabase(fullDrop: Bool) -> Void {
        databaseQueue.sync {(Void) in
            
            if fullDrop {
                do {
                    try FileManager.default.removeItem(atPath: self.databasePath)
                    try FileManager.default.removeItem(atPath: self.databasePath + "-shm")
                    try FileManager.default.removeItem(atPath: self.databasePath + "-wal")
                } catch let error as NSError {
                    print("failed: \(error.localizedDescription)")
                }
            }
            
            do {
                try self.database.executeUpdate("DROP TABLE IF EXISTS \(self.usersTableName)", values:nil)
                try self.database.executeUpdate("DROP TABLE IF EXISTS \(self.rolesTableName)", values:nil)
                try self.database.executeUpdate("DROP TABLE IF EXISTS \(self.customersTableName)", values:nil)
                try self.database.executeUpdate("DROP TABLE IF EXISTS \(self.ordersTableName)", values:nil)
                try self.database.executeUpdate("DROP TABLE IF EXISTS \(self.orderDetailsTableName)", values:nil)
                try self.database.executeUpdate("DROP TABLE IF EXISTS \(self.paymentsTableName)", values:nil)
                try self.database.executeUpdate("DROP TABLE IF EXISTS \(self.productsTableName)", values:nil)
                try self.database.executeUpdate("DROP TABLE IF EXISTS \(self.categoriesTableName)", values:nil)
                try self.database.executeUpdate("DROP TABLE IF EXISTS \(self.suppliersTableName)", values:nil)
                try self.database.executeUpdate("DROP TABLE IF EXISTS \(self.accountsTableName)", values:nil)
                try self.database.executeUpdate("DROP TABLE IF EXISTS \(self.transactionsTableName)", values:nil)
                try self.database.executeUpdate("DROP TABLE IF EXISTS \(self.syncOperationsTableName)", values:nil)
                
                databaseIndexQueue.sync {
                    do {
                        try self.indexDatabase.executeUpdate("DROP TABLE IF EXISTS \(self.productsIndexTableName)", values:nil)
                    } catch let error as NSError {
                        print("failed: \(error.localizedDescription)")
                    }
                }
                
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
        }
        
        databaseQueue.async { (Void) in
            self.upgradeTables()
        }
        
    }

    private func table(tableName: String, containsField:String) -> Bool {
        var contains: Bool = false
        if let result = self.database.executeQuery("PRAGMA table_info(\(tableName))", withArgumentsIn:nil) {
            while result.next() {
                let k_field = result.string(forColumnIndex: 1)
                if k_field == containsField {
                    contains = true
                }
            }
        }
        return contains
    }
    
    func uniqueIncreasingPrimaryKey() -> UInt64 {
        let date = Date()
        let primaryKeyDateUInt = UInt64(date.timeIntervalSince1970 * 1000)
        return primaryKeyDateUInt
        //date.timeIntervalSince(Date())
        //primaryKeyDateUInt ===> bigValue // date from timeIntervalSince1970
        //   UInt64 maxValue ===> 18446744073709551615
        //   postgres BIGINT ===> 9223372036854775807
    }
    
    func isModelSynchronized(primaryKey: UInt64) -> Bool {
        if primaryKey > bigValue {
            return false
        }
        return true
    }
    
    func isStaticModelsSynced(completion:@escaping (Bool) -> ()) {
        
        var primaryKey: UInt64 = 0
        var shouldContinue: Bool = true
        
        databaseQueue.async { (Void) in
            
            if let result = self.database.executeQuery("SELECT MAX(product_id) as maxID FROM \(self.productsTableName)", withArgumentsIn:nil) {
                while result.next() {
                    primaryKey =  result.unsignedLongLongInt(forColumn: "maxID")
                }
                
                shouldContinue = self.isModelSynchronized(primaryKey: primaryKey)
            }
            
            if shouldContinue {
                if let result = self.database.executeQuery("SELECT MAX(user_id) as maxID FROM \(self.usersTableName)", withArgumentsIn:nil) {
                    while result.next() {
                        primaryKey =  result.unsignedLongLongInt(forColumn: "maxID")
                    }
                }
                
                shouldContinue = self.isModelSynchronized(primaryKey: primaryKey)
            }
            
            if shouldContinue {
                if let result = self.database.executeQuery("SELECT MAX(customer_id) as maxID FROM \(self.customersTableName)", withArgumentsIn:nil) {
                    while result.next() {
                        primaryKey =  result.unsignedLongLongInt(forColumn: "maxID")
                    }
                }
                
                shouldContinue = self.isModelSynchronized(primaryKey: primaryKey)
            }

            if shouldContinue {
                if let result = self.database.executeQuery("SELECT MAX(supplier_id) as maxID FROM \(self.suppliersTableName)", withArgumentsIn:nil) {
                    while result.next() {
                        primaryKey =  result.unsignedLongLongInt(forColumn: "maxID")
                    }
                }
                
                shouldContinue = self.isModelSynchronized(primaryKey: primaryKey)
            }

            if shouldContinue {
                if let result = self.database.executeQuery("SELECT MAX(category_id) as maxID FROM \(self.categoriesTableName)", withArgumentsIn:nil) {
                    while result.next() {
                        primaryKey =  result.unsignedLongLongInt(forColumn: "maxID")
                    }
                }
                
                shouldContinue = self.isModelSynchronized(primaryKey: primaryKey)
            }
            
            if shouldContinue {
                if let result = self.database.executeQuery("SELECT MAX(account_id) as maxID FROM \(self.accountsTableName)", withArgumentsIn:nil) {
                    while result.next() {
                        primaryKey =  result.unsignedLongLongInt(forColumn: "maxID")
                    }
                }
                
                shouldContinue = self.isModelSynchronized(primaryKey: primaryKey)
            }
            
            DispatchQueue.main.async {
                completion(shouldContinue)
            }
        }
    }

    /*if let result = self.database.executeQuery("SELECT MAX(order_detail_id) as maxID FROM \(self.orderDetailsTableName)", withArgumentsIn:nil) {
        while result.next() {
            staffPrimaryKey =  result.unsignedLongLongInt(forColumn: "maxID")
        }
    }
    
    if let result = self.database.executeQuery("SELECT MAX(transaction_id) as maxID FROM \(self.transactionsTableName)", withArgumentsIn:nil) {
        while result.next() {
            supplierPrimaryKey =  result.unsignedLongLongInt(forColumn: "maxID")
        }
    }
    
    if let result = self.database.executeQuery("SELECT MAX(order_id) as maxID FROM \(self.ordersTableName)", withArgumentsIn:nil) {
        while result.next() {
            supplierPrimaryKey =  result.unsignedLongLongInt(forColumn: "maxID")
        }
    }
     
    Payment
    */
    
    //--------------------------------------------------------------------------------
    // MARK: Orders
    func totalOrdersCount() -> Int32 {
        var totalCount: Int32 = 0
        databaseQueue.sync { (Void) in
            let result = self.database.executeQuery("SELECT COUNT(*) FROM \(self.ordersTableName)", withArgumentsIn:nil)
            if let result_ = result {
                if result_.next() {
                    totalCount = result_.int(forColumn: "COUNT(*)")
                }
            }
        }
        return totalCount
    }
    
    func updateOrderEdittedStatus(orderId: UInt64, isEditted: Bool) -> Void {
        
        databaseQueue.async { (Void) in
            self.database.beginTransaction()
            self.database.executeUpdate("UPDATE \(self.ordersTableName) SET is_editted=? WHERE order_id=?",   withArgumentsIn: [NSNumber(value: isEditted), NSNumber(value: orderId)])
            self.database.commit()
        }
    }
    
    func updateOrdersPrimaryKey(oldId: UInt64, newId: UInt64) -> Void {
        
        databaseQueue.sync { (Void) in
            self.database.beginTransaction()
            let queryFormat = "UPDATE \(self.ordersTableName) SET order_id=? WHERE order_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: newId), NSNumber(value: oldId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    func updateOrdersCustomerId(oldCustomerId: UInt64, newCustomerId: UInt64) -> Void {
        
        databaseQueue.sync { (Void) in
            self.database.beginTransaction()
            let queryFormat = "UPDATE \(self.ordersTableName) SET customer_id=? WHERE customer_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: newCustomerId), NSNumber(value: oldCustomerId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    func updateOrdersCommentAndBillingNo(comment: String, billingNo: String, orderId: UInt64) -> Void {
        
        databaseQueue.async { (Void) in
            self.database.beginTransaction()
            let queryFormat = "UPDATE \(self.ordersTableName) SET billing_no=?, comment=? WHERE order_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [billingNo, comment, NSNumber(value: orderId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    func updateOrdersPaymentId(oldId: UInt64, newId: UInt64) -> Void {
        
        databaseQueue.sync { (Void) in
            self.database.beginTransaction()
            let queryFormat = "UPDATE \(self.ordersTableName) SET payment_id=? WHERE payment_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: newId), NSNumber(value: oldId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }

    func lastOrder() -> Order? {
        
        var orders: [Order] = []
        databaseQueue.sync { (Void) in
            
            let result = self.database.executeQuery("SELECT * FROM \(self.ordersTableName) WHERE order_id <? ORDER BY order_date ASC LIMIT 1", withArgumentsIn:[NSNumber(value: bigValue)])
            
            if let result_ = result {
                while result_.next() {
                    let order = Order.orderFrom(queryResult: result_)
                    orders.append(order)
                }
            }
        }
        
        if orders.count > 0 {
            return orders[0]
        }
        
        return nil
    }
    
    func lastSyncedOrder() -> UInt64 {
        
        var primaryKey: UInt64 = 0
        databaseQueue.sync { (Void) in
            
            if let result_ = self.database.executeQuery("SELECT MAX(order_id) as maxID FROM \(self.ordersTableName) WHERE order_id <?",
                withArgumentsIn:[NSNumber(value: bigValue)]) {
                
                while result_.next() {
                    primaryKey = result_.unsignedLongLongInt(forColumn: "maxID")
                }
            }
        }
        return primaryKey
    }
    
    func loadOrdersForFilterM(documentType: OrderDocumentType,
                              startDate: Date,
                              endDate: Date,
                              customerId: UInt64,
                              supplierId: UInt64,
                              userId: UInt64,
                              limit: UInt16,
                              filterOptions: FilterOptions,
                              completion: @escaping (_ result: [Order]) -> ())
    {
        if filterOptions.contains(.isAgentOption) {
        }
    }
    
    func loadStatisticOrdersForDates(startDate: Date,
                                     endDate: Date) -> [Order] {
        var orders: [Order] = []
        //let intOrderValue = Order.integerOrderDocumentTypeFrom(orderDocType: .productOrderSaledToCustomer)
        
        databaseQueue.sync { (Void) in
            
            let result: FMResultSet = self.database.executeQuery("SELECT * FROM \(self.ordersTableName) WHERE order_date <=? AND order_date >? ORDER BY order_date DESC LIMIT ?", withArgumentsIn:[NSNumber(value: endDate.timeIntervalSince1970 * 1000), NSNumber(value: startDate.timeIntervalSince1970 * 1000), NSNumber(value: 50000)])!
            
            while result.next() {
                let order = Order.orderFrom(queryResult: result)
                orders.append(order)
            }
        }
        return orders
    }
    
    func loadOrdersForFilters(documentType: OrderDocumentType,
                              startDate: Date,
                              endDate: Date,
                              customerId: UInt64,
                              supplierId: UInt64,
                              userId: UInt64,
                              limit: UInt16,
                              filterOptions: FilterOptions,
                              completion: @escaping (_ result: [Order]) -> ())
    {
        var orders: [Order] = []
        
        let intOrderValue = Order.integerOrderDocumentTypeFrom(orderDocType: documentType)
        
        let orderMovementType = Order.orderMovementType(fromOrderDocType: documentType)
        let intOrderMovementType = Order.integerOrderMovement(orderMovement: orderMovementType)
        
        databaseQueue.async { (Void) in
            
            let result: FMResultSet
            
            if intOrderValue == 0 {
                if customerId > 0 {
                    if userId > 0 {
                        result = self.database.executeQuery("SELECT * FROM \(self.ordersTableName) WHERE order_date <=? AND order_date >? AND customer_id=? AND user_id=? ORDER BY order_date DESC LIMIT ?", withArgumentsIn:[NSNumber(value: endDate.timeIntervalSince1970 * 1000), NSNumber(value: startDate.timeIntervalSince1970 * 1000), NSNumber(value: customerId), NSNumber(value: userId), NSNumber(value: limit)])!
                    } else {
                        result = self.database.executeQuery("SELECT * FROM \(self.ordersTableName) WHERE order_date <=? AND order_date >? AND customer_id=? ORDER BY order_date DESC LIMIT ?", withArgumentsIn:[NSNumber(value: endDate.timeIntervalSince1970 * 1000), NSNumber(value: startDate.timeIntervalSince1970 * 1000), NSNumber(value: customerId), NSNumber(value: limit)])!
                    }
                } else if supplierId > 0 {
                    if userId > 0 {
                        result = self.database.executeQuery("SELECT * FROM \(self.ordersTableName) WHERE order_date <=? AND order_date >? AND supplier_id=? AND user_id=? ORDER BY order_date DESC LIMIT ?", withArgumentsIn:[NSNumber(value: endDate.timeIntervalSince1970 * 1000), NSNumber(value: startDate.timeIntervalSince1970 * 1000), NSNumber(value: supplierId), NSNumber(value: userId), NSNumber(value: limit)])!
                        
                    } else {
                        result = self.database.executeQuery("SELECT * FROM \(self.ordersTableName) WHERE order_date <=? AND order_date >? AND supplier_id=? ORDER BY order_date DESC LIMIT ?", withArgumentsIn:[NSNumber(value: endDate.timeIntervalSince1970 * 1000), NSNumber(value: startDate.timeIntervalSince1970 * 1000), NSNumber(value: supplierId), NSNumber(value: limit)])!
                        
                    }
                } else {
                    if userId > 0 {
                        result = self.database.executeQuery("SELECT * FROM \(self.ordersTableName) WHERE order_date <=? AND order_date >? AND user_id=? ORDER BY order_date DESC LIMIT ?", withArgumentsIn:[NSNumber(value: endDate.timeIntervalSince1970 * 1000), NSNumber(value: startDate.timeIntervalSince1970 * 1000), NSNumber(value: userId), NSNumber(value: limit)])!
                    } else {
                        result = self.database.executeQuery("SELECT * FROM \(self.ordersTableName) WHERE order_date <=? AND order_date >? ORDER BY order_date DESC LIMIT ?", withArgumentsIn:[NSNumber(value: endDate.timeIntervalSince1970 * 1000), NSNumber(value: startDate.timeIntervalSince1970 * 1000), NSNumber(value: limit)])!
                    }
                }
            } else {
                if customerId > 0 {
                    if userId > 0 {
                        result = self.database.executeQuery("SELECT * FROM \(self.ordersTableName) WHERE order_date <=? AND order_date >? AND order_document=? AND customer_id=? AND user_id=? ORDER BY order_date DESC LIMIT ?", withArgumentsIn:[NSNumber(value: endDate.timeIntervalSince1970 * 1000), NSNumber(value: startDate.timeIntervalSince1970 * 1000), NSNumber(value: intOrderValue), NSNumber(value: customerId), NSNumber(value: userId), NSNumber(value: limit)])!
                    } else {
                        result = self.database.executeQuery("SELECT * FROM \(self.ordersTableName) WHERE order_date <=? AND order_date >? AND order_document=? AND customer_id=? ORDER BY order_date DESC LIMIT ?", withArgumentsIn:[NSNumber(value: endDate.timeIntervalSince1970 * 1000), NSNumber(value: startDate.timeIntervalSince1970 * 1000), NSNumber(value: intOrderValue), NSNumber(value: customerId), NSNumber(value: limit)])!
                    }
                } else if supplierId > 0 {
                    if userId > 0 {
                        result = self.database.executeQuery("SELECT * FROM \(self.ordersTableName) WHERE order_date <=? AND order_date >? AND order_document=? AND supplier_id=? AND user_id=? ORDER BY order_date DESC LIMIT ?", withArgumentsIn:[NSNumber(value: endDate.timeIntervalSince1970 * 1000), NSNumber(value: startDate.timeIntervalSince1970 * 1000), NSNumber(value: intOrderValue), NSNumber(value: supplierId), NSNumber(value: userId), NSNumber(value: limit)])!
                    } else {
                        result = self.database.executeQuery("SELECT * FROM \(self.ordersTableName) WHERE order_date <=? AND order_date >? AND order_document=? AND supplier_id=? ORDER BY order_date DESC LIMIT ?", withArgumentsIn:[NSNumber(value: endDate.timeIntervalSince1970 * 1000), NSNumber(value: startDate.timeIntervalSince1970 * 1000), NSNumber(value: intOrderValue), NSNumber(value: supplierId), NSNumber(value: limit)])!
                    }
                } else {
                    if userId > 0 {
                        result = self.database.executeQuery("SELECT * FROM \(self.ordersTableName) WHERE order_date <=? AND order_date >? AND order_document=? AND user_id=? ORDER BY order_date DESC LIMIT ?", withArgumentsIn:[NSNumber(value: endDate.timeIntervalSince1970 * 1000), NSNumber(value: startDate.timeIntervalSince1970 * 1000), NSNumber(value: intOrderValue), NSNumber(value: userId), NSNumber(value: limit)])!
                    } else {
                        result = self.database.executeQuery("SELECT * FROM \(self.ordersTableName) WHERE order_date <=? AND order_date >? AND order_document=? ORDER BY order_date DESC LIMIT ?", withArgumentsIn:[NSNumber(value: endDate.timeIntervalSince1970 * 1000), NSNumber(value: startDate.timeIntervalSince1970 * 1000), NSNumber(value: intOrderValue), NSNumber(value: limit)])!
                    }
                }
            }
            
            while result.next() {
                let order = Order.orderFrom(queryResult: result)
                orders.append(order)
            }
            
            DispatchQueue.main.async {
                completion(orders)
            }
        }
    }
    
    func loadOrders(orderDocumentType: OrderDocumentType,
                    date: Date,
                    limit: UInt16,
                    completion: @escaping (_ result: [Order]) -> ()) {
        
        var orders: [Order] = []
        let intOrderValue = Order.integerOrderDocumentTypeFrom(orderDocType: orderDocumentType)
        
        let orderMovementType = Order.orderMovementType(fromOrderDocType: orderDocumentType)
        let intOrderMovementType = Order.integerOrderMovement(orderMovement: orderMovementType)
        
        databaseQueue.async { (Void) in
            
            let result: FMResultSet
            if intOrderValue == 0 {
                result = self.database.executeQuery("SELECT * FROM \(self.ordersTableName) WHERE order_date <? ORDER BY order_date DESC LIMIT ?", withArgumentsIn:[NSNumber(value: date.timeIntervalSince1970 * 1000), NSNumber(value: limit)])!
            } else {
                result = self.database.executeQuery("SELECT * FROM \(self.ordersTableName) WHERE order_date <? AND order_document=? ORDER BY order_date DESC LIMIT ?", withArgumentsIn:[NSNumber(value: date.timeIntervalSince1970 * 1000), NSNumber(value: intOrderValue), NSNumber(value: limit)])!
            }
            
            while result.next() {
                let order = Order.orderFrom(queryResult: result)
                orders.append(order)
            }
            
            DispatchQueue.main.async {
                completion(orders)
            }
        }
    }
        
    func orderBy(orderId: UInt64) -> Order? {
        
        var orders: [Order] = []
        databaseQueue.sync { (Void) in
            let result = self.database.executeQuery("SELECT * FROM \(self.ordersTableName) WHERE order_id=? LIMIT 1", withArgumentsIn:[NSNumber(value: orderId)])
            
            if let result_ = result {
                
                while result_.next() {
                    let order = Order.orderFrom(queryResult: result_)
                    orders.append(order)
                }
            }
        }
        
        if orders.count > 0 {
            return orders[0]
        }
        
        return nil
    }
    
    func createOrders(_ orders: [Order]) -> Void {
        
        databaseQueue.async { (Void) in
            
            self.database.beginTransaction()
            for order in orders {
                self.storeOrder(order)
            }
            self.database.commit()
        }
    }
    
    //try self.database.executeUpdate("CREATE TABLE IF NOT EXISTS \(self.ordersTableName) (order_id INTEGER PRIMARY KEY, order_document INTEGER, order_movement INTEGER, money_movement INTEGER, billing_no TEXT, user_id INTEGER, customer_id INTEGER, supplier_id INTEGER, order_date INTEGER, payment_id INTEGER, error_msg TEXT, comment TEXT, uuid TEXT, is_deleted INTEGER, is_money_for_debt INTEGER, is_editted INTEGER)", values: nil)
    
    private func storeOrder(_ order: Order) -> Void {
        
        databaseQueue.async { (Void) in
            
            let queryFormat = "INSERT OR REPLACE INTO \(self.ordersTableName) (order_id, order_document, order_movement, money_movement, billing_no, user_id, customer_id, supplier_id, order_date, payment_id, error_msg, comment, uuid, is_deleted, is_money_for_debt, is_editted) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
            
            do {
                try self.database.executeUpdate(queryFormat, values: order.databaseInsertValues())
                
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
        }
    }
    
    func deleteOrder(_ orderId: UInt64) -> Void {
        databaseQueue.async { (Void) in
            self.database.beginTransaction()
            let queryFormat = "DELETE FROM \(self.ordersTableName) WHERE order_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: orderId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    //--------------------------------------------------------------------------------
    // MARK: Payments
    func paymentBy(paymentId: UInt64) -> Payment? {
        
        var payments: [Payment] = []
        databaseQueue.sync { (Void) in
            let result = self.database.executeQuery("SELECT * FROM \(self.paymentsTableName) WHERE payment_id=? LIMIT 1", withArgumentsIn:[NSNumber(value: paymentId)])
            
            if let result_ = result {
                
                while result_.next() {
                    let payment = Payment.paymentFrom(queryResult: result_)
                    payments.append(payment)
                }
            }
        }
        
        if payments.count > 0 {
            return payments[0]
        }
        
        return nil
    }
    
    func updatePaymentsPrimaryKey(oldId: UInt64, newId: UInt64) -> Void {
        
        databaseQueue.sync { (Void) in
            self.database.beginTransaction()
            let queryFormat = "UPDATE \(self.paymentsTableName) SET payment_id=? WHERE payment_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: newId), NSNumber(value: oldId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    func createPayments(_ payments: [Payment]) -> Void {
        
        databaseQueue.async { (Void) in
            
            self.database.beginTransaction()
            for payment in payments {
                self.storePayment(payment)
            }
            self.database.commit()
        }
    }
    
    private func storePayment(_ payment: Payment) -> Void {
        
        databaseQueue.async { (Void) in
            
            let queryFormat = "INSERT OR REPLACE INTO \(self.paymentsTableName) (payment_id, total_order_price, discount, total_price_with_discount, minus_price, plus_price, comment) VALUES (?, ?, ?, ?, ?, ?, ?)"
            
            do {
                try self.database.executeUpdate(queryFormat, values: payment.databaseInsertValues())
                
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
        }
    }
    
    func deletePayment(_ paymentId: UInt64) -> Void {
        databaseQueue.async { (Void) in
            self.database.beginTransaction()
            let queryFormat = "DELETE FROM \(self.paymentsTableName) WHERE payment_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: paymentId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    //------------------------------------------------------------------------------------
    // MARK: SyncOperations
    func operationCid() -> UInt64 {
        
        var supplierPrimaryKey: UInt64 = 0
        databaseQueue.sync { (Void) in
            if let result = self.database.executeQuery("SELECT MAX(syncoperation_id) as maxID FROM \(self.syncOperationsTableName)", withArgumentsIn:nil) {
                while result.next() {
                    supplierPrimaryKey =  result.unsignedLongLongInt(forColumn: "maxID")
                }
            }
        }
        return (supplierPrimaryKey + 1)
    }
    
    func updateSyncOperationsCustomerId(oldCustomerId: UInt64, newCustomerId: UInt64) -> Void {
        databaseQueue.sync { (Void) in
            self.database.beginTransaction()
            let queryFormat = "UPDATE \(self.syncOperationsTableName) SET customer_id=? WHERE customer_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: newCustomerId), NSNumber(value: oldCustomerId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    func loadAllSyncOperations(date: Date, limit: UInt16, completion: @escaping (_ result: [Event]) -> ()) {
        var operations: [Event] = []
        databaseQueue.async { (Void) in
            
            let result = self.database.executeQuery("SELECT * FROM \(self.syncOperationsTableName) WHERE syncoperation_date <? ORDER BY syncoperation_date DESC LIMIT ?", withArgumentsIn:[NSNumber(value: date.timeIntervalSince1970 * 1000), NSNumber(value: limit)])!
            
            while result.next() {
                let operation = Event.syncOpertaionFrom(queryResult: result)
                operations.append(operation)
            }
            
            DispatchQueue.main.async {
                completion(operations)
            }
        }
    }
    
    func countUnSyncedOperations(completion: @escaping (_ totalCount: Int32) -> ()) -> Void {
        
        var totalCount: Int32 = 0
        databaseQueue.sync { (Void) in
            
            let result = self.database.executeQuery("SELECT COUNT(*) FROM \(self.syncOperationsTableName) WHERE is_synchronized=?", withArgumentsIn:[NSNumber(value: false)])
            
            if let result_ = result {
                if result_.next() {
                    totalCount = result_.int(forColumn: "COUNT(*)")
                }
            }
        }
        
        DispatchQueue.main.async {
            completion(totalCount)
        }
    }
    
    func allUnSyncedOperations() -> [Event] {
        
        var operations: [Event] = []
        
        databaseQueue.sync { (Void) in
            
            let result = self.database.executeQuery("SELECT * FROM \(self.syncOperationsTableName) WHERE is_synchronized=? ORDER BY syncoperation_date ASC", withArgumentsIn:[NSNumber(value: false)])!
            
            while result.next() {
                
                let operation = Event.syncOpertaionFrom(queryResult: result)
                operations.append(operation)
            }
        }
        return operations
    }
    
    func operationBy(syncOperationId: UInt64) -> Event? {
        
        var operations: [Event] = []
        databaseQueue.sync { (Void) in
            let result = self.database.executeQuery("SELECT * FROM \(self.syncOperationsTableName) WHERE syncoperation_id=? LIMIT 1", withArgumentsIn:[NSNumber(value: syncOperationId)])
            
            if let result_ = result {
                
                while result_.next() {
                    
                    let operation = Event.syncOpertaionFrom(queryResult: result_)
                    operations.append(operation)
                }
            }
        }
        
        if operations.count > 0 {
            return operations[0]
        }
        
        return nil
    }
    
    func createOperations(_ operations: [Event]) -> Void {
        databaseQueue.async { (Void) in
            
            self.database.beginTransaction()
            for operation in operations {
                self.storeOperation(operation)
            }
            self.database.commit()
            
            DispatchQueue.main.async {
                PipesStore.shared.syncFinishedPipe.putNext(true)
            }
        }
    }
    
    private func storeOperation(_ operation: Event) -> Void {
    
        databaseQueue.async { (Void) in
            
            let queryFormat = "INSERT OR REPLACE INTO \(self.syncOperationsTableName) (syncoperation_id, is_synchronized, syncoperation_date, sync_type, product_id, order_detail_id, customer_id, transaction_id, account_id, supplier_id, staff_id, category_id, order_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
            
            do {
                try self.database.executeUpdate(queryFormat, values: operation.databaseInsertValues())
                
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
        }
    }
    
    func markOperationAsSynced(operationId: UInt64) -> Void {
        
        databaseQueue.sync { (Void) in
            self.database.beginTransaction()
            let queryFormat = "UPDATE \(self.syncOperationsTableName) SET is_synchronized=? WHERE syncoperation_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: true), NSNumber(value: operationId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    func deleteOperation(_ operationId: UInt64) -> Void {
        databaseQueue.async { (Void) in
            self.database.beginTransaction()
            let queryFormat = "DELETE FROM \(self.syncOperationsTableName) WHERE syncoperation_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: operationId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    //----------------------------------------------------------------------------------------
    // MARK: Transactions
    func lastSyncedTransaction() -> UInt64 {
        
        var primaryKey: UInt64 = 0
        databaseQueue.sync { (Void) in
            
            if let result_ = self.database.executeQuery("SELECT MAX(transaction_id) as maxID FROM \(self.transactionsTableName) WHERE transaction_id <?",
                withArgumentsIn:[NSNumber(value: bigValue)]) {
                
                while result_.next() {
                    primaryKey = result_.unsignedLongLongInt(forColumn: "maxID")
                }
            }
        }
        return primaryKey
    }
    
    func totalTransactionsCount() -> Int32 {
        var totalCount: Int32 = 0
        databaseQueue.sync { (Void) in
            let result = self.database.executeQuery("SELECT COUNT(*) FROM \(self.transactionsTableName)", withArgumentsIn:nil)
            if let result_ = result {
                if result_.next() {
                    totalCount = result_.int(forColumn: "COUNT(*)")
                }
            }
        }
        return totalCount
    }
    
    func loadAllTransactions(customerId: UInt64, supplierId: UInt64, date: Date, limit: UInt16, completion: @escaping (_ result: [Transaction]) -> ()) {
        
        var transactions: [Transaction] = []
        databaseQueue.async { (Void) in
            
            let result: FMResultSet
            if customerId > 0 {
                result = self.database.executeQuery("SELECT * FROM \(self.transactionsTableName) WHERE transaction_date <? AND customer_id=? ORDER BY transaction_date DESC LIMIT ?", withArgumentsIn:[NSNumber(value: date.timeIntervalSince1970 * 1000), NSNumber(value: customerId), NSNumber(value: limit)])!
            } else if supplierId > 0 {
                result = self.database.executeQuery("SELECT * FROM \(self.transactionsTableName) WHERE transaction_date <? AND supplier_id=? ORDER BY transaction_date DESC LIMIT ?", withArgumentsIn:[NSNumber(value: date.timeIntervalSince1970 * 1000), NSNumber(value: supplierId), NSNumber(value: limit)])!
            } else {
                result = self.database.executeQuery("SELECT * FROM \(self.transactionsTableName) WHERE transaction_date <? ORDER BY transaction_date DESC LIMIT ?", withArgumentsIn:[NSNumber(value: date.timeIntervalSince1970 * 1000), NSNumber(value: limit)])!
            }
            
            while result.next() {
                let transaction = Transaction.transactionFrom(queryResult: result)
                transactions.append(transaction)
            }
            
            DispatchQueue.main.async {
                completion(transactions)
            }
        }
    }
    
    func transactionBy(transactionId: UInt64) -> Transaction? {
        
        var transactions: [Transaction] = []
        databaseQueue.sync { (Void) in
            let result = self.database.executeQuery("SELECT * FROM \(self.transactionsTableName) WHERE transaction_id=? LIMIT 1", withArgumentsIn:[NSNumber(value: transactionId)])
            
            if let result_ = result {
                
                while result_.next() {
                    let transaction = Transaction.transactionFrom(queryResult: result_)
                    transactions.append(transaction)
                }
            }
        }
        
        if transactions.count > 0 {
            return transactions[0]
        }
        
        return nil
    }
    
    func updateTransactionsPrimaryKey(oldId: UInt64, newId: UInt64) -> Void {
        
        databaseQueue.sync { (Void) in
            self.database.beginTransaction()
            let queryFormat = "UPDATE \(self.transactionsTableName) SET transaction_id=? WHERE transaction_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: newId), NSNumber(value: oldId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    func updateTransactionsCustomerId(oldCustomerId: UInt64, newCustomerId: UInt64) -> Void {
        
        databaseQueue.sync { (Void) in
            self.database.beginTransaction()
            let queryFormat = "UPDATE \(self.transactionsTableName) SET customer_id=? WHERE customer_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: newCustomerId), NSNumber(value: oldCustomerId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    func updateTransactionsCustomerId(transactionId: UInt64, customerId: UInt64) -> Void {
        
        databaseQueue.sync { (Void) in
            self.database.beginTransaction()
            let queryFormat = "UPDATE \(self.transactionsTableName) SET customer_id=? WHERE transaction_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: customerId), NSNumber(value: transactionId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    func updateTransactionsOrderId(oldOrderId: UInt64, newOrderId: UInt64) -> Void {
        
        databaseQueue.sync { (Void) in
            self.database.beginTransaction()
            let queryFormat = "UPDATE \(self.transactionsTableName) SET order_id=? WHERE order_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: newOrderId), NSNumber(value: oldOrderId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    func updateTransactionsSupplierId(transactionId: UInt64, supplierId: UInt64) -> Void {
        
        databaseQueue.sync { (Void) in
            self.database.beginTransaction()
            let queryFormat = "UPDATE \(self.transactionsTableName) SET supplier_id=? WHERE transaction_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: supplierId), NSNumber(value: transactionId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    func updateTransactionsComment(transactionId: UInt64, comment: String) -> Void {
        
        databaseQueue.sync { (Void) in
            self.database.beginTransaction()
            let queryFormat = "UPDATE \(self.transactionsTableName) SET comment=? WHERE transaction_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [comment, NSNumber(value: transactionId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    func createTransactions(_ transactions: [Transaction]) -> Void {
        
        databaseQueue.async { (Void) in
            
            self.database.beginTransaction()
            for transaction in transactions {
                self.storeTransaction(transaction)
            }
            self.database.commit()
        }
    }
    
    private func storeTransaction(_ transaction: Transaction) -> Void {
        
        databaseQueue.async { (Void) in
            
            let queryFormat = "INSERT OR REPLACE INTO \(self.transactionsTableName) (transaction_id, transaction_date, is_last_transaction, transaction_type, money_amount, order_id, customer_id, supplier_id, user_id, ballance_amount, comment, uuid) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
            
            do {
                try self.database.executeUpdate(queryFormat, values: transaction.databaseInsertValues())
                
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
        }
    }
    
    func deleteTransaction(_ transactionId: UInt64) -> Void {
        databaseQueue.async { (Void) in
            self.database.beginTransaction()
            let queryFormat = "DELETE FROM \(self.transactionsTableName) WHERE transaction_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: transactionId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    //--------------------------------------------------------------------------------
    // MARK: Accounts
    func loadAllAccounts() -> [Account] {
        
        var accounts: [Account] = []
        
        databaseQueue.sync { (Void) in
            let result = self.database.executeQuery("SELECT * FROM \(self.accountsTableName) ORDER BY account_id ASC", withArgumentsIn:nil)!
            
            while result.next() {
                let account = Account.accountFrom(queryResult: result)
                accounts.append(account)
            }
        }
        return accounts
    }
    
    func updateAccountsPrimaryKey(oldId: UInt64, newId: UInt64) -> Void {
        databaseQueue.sync { (Void) in
            self.database.beginTransaction()
            let queryFormat = "UPDATE \(self.accountsTableName) SET account_id=? WHERE account_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: newId), NSNumber(value: oldId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    func updateAccountsCustomerId(accountId: UInt64, customerId: UInt64) -> Void {
        databaseQueue.sync { (Void) in
            self.database.beginTransaction()
            let queryFormat = "UPDATE \(self.accountsTableName) SET customer_id=? WHERE account_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: customerId), NSNumber(value: accountId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    func updateAccountsSupplierId(accountId: UInt64, supplierId: UInt64) -> Void {
        databaseQueue.sync { (Void) in
            self.database.beginTransaction()
            let queryFormat = "UPDATE \(self.accountsTableName) SET supplier_id=? WHERE account_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: supplierId), NSNumber(value: accountId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    func updateSupplierBalanceFor(supplierId: UInt64, balance: Double) -> Void {
        databaseQueue.async { (Void) in
            self.database.beginTransaction()
            self.database.executeUpdate("UPDATE \(self.accountsTableName) SET balance=? WHERE supplier_id=?",   withArgumentsIn: [NSNumber(value: balance), NSNumber(value: supplierId)])
            self.database.commit()
        }
    }
    
    func updateCustomerBalanceFor(customerId: UInt64, balance: Double) -> Void {
        databaseQueue.async { (Void) in
            self.database.beginTransaction()
            self.database.executeUpdate("UPDATE \(self.accountsTableName) SET balance=? WHERE customer_id=?",   withArgumentsIn: [NSNumber(value: balance), NSNumber(value: customerId)])
            self.database.commit()
        }
    }
    
    func accountBy(accountId: UInt64) -> Account? {
        
        var accounts: [Account] = []
        databaseQueue.sync { (Void) in
            let result = self.database.executeQuery("SELECT * FROM \(self.accountsTableName) WHERE account_id=? LIMIT 1", withArgumentsIn:[NSNumber(value: accountId)])
            
            if let result_ = result {
                
                while result_.next() {
                    let account = Account.accountFrom(queryResult: result_)
                    accounts.append(account)
                }
            }
        }
        
        if accounts.count > 0 {
            return accounts[0]
        }
        
        return nil
    }
    
    func accountBy(customerId: UInt64) -> Account? {
        
        var accounts: [Account] = []
        databaseQueue.sync { (Void) in
            let result = self.database.executeQuery("SELECT * FROM \(self.accountsTableName) WHERE customer_id=? LIMIT 1", withArgumentsIn:[NSNumber(value: customerId)])
            
            if let result_ = result {
                while result_.next() {
                    let account = Account.accountFrom(queryResult: result_)
                    accounts.append(account)
                }
            }
        }
        
        if accounts.count > 0 {
            return accounts[0]
        }
        
        return nil
    }
    
    func accountBy(supplierId: UInt64) -> Account? {
        
        var accounts: [Account] = []
        databaseQueue.sync { (Void) in
            let result = self.database.executeQuery("SELECT * FROM \(self.accountsTableName) WHERE supplier_id=? LIMIT 1", withArgumentsIn:[NSNumber(value: supplierId)])
            
            if let result_ = result {
                while result_.next() {
                    let account = Account.accountFrom(queryResult: result_)
                    accounts.append(account)
                }
            }
        }
        
        if accounts.count > 0 {
            return accounts[0]
        }
        
        return nil
    }
    
    func createAccounts(_ accounts: [Account]) -> Void {
        
        databaseQueue.async { (Void) in
            
            self.database.beginTransaction()
            for account in accounts {
                self.storeAccount(account)
            }
            self.database.commit()
        }
    }
    
    private func storeAccount(_ account: Account) -> Void {
        
        databaseQueue.async { (Void) in
            
            let queryFormat = "INSERT OR REPLACE INTO \(self.accountsTableName) (account_id, customer_id, supplier_id, balance) VALUES (?, ?, ?, ?)"
            
            do {
                try self.database.executeUpdate(queryFormat, values: account.databaseInsertValues())
                
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
        }
    }
    
    func deleteAccount(_ accountId: UInt64) -> Void {
        databaseQueue.async { (Void) in
            self.database.beginTransaction()
            let queryFormat = "DELETE FROM \(self.accountsTableName) WHERE account_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: accountId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    //--------------------------------------------------------------------------------
    // MARK: OrderDetails
    func lastSyncedOrderDetail() -> UInt64 {
        
        var primaryKey: UInt64 = 0
        databaseQueue.sync { (Void) in
            
            if let result_ = self.database.executeQuery("SELECT MAX(order_detail_id) as maxID FROM \(self.orderDetailsTableName) WHERE order_detail_id <?",
                withArgumentsIn:[NSNumber(value: bigValue)]) {
                
                while result_.next() {
                    primaryKey = result_.unsignedLongLongInt(forColumn: "maxID")
                }
            }
        }
        return primaryKey
    }
    
    func totalOrderDetails() -> Int32 {
        var totalCount: Int32 = 0
        databaseQueue.sync { (Void) in
            let result = self.database.executeQuery("SELECT COUNT(*) FROM \(self.orderDetailsTableName)", withArgumentsIn:nil)
            if let result_ = result {
                if result_.next() {
                    totalCount = result_.int(forColumn: "COUNT(*)")
                }
            }
        }
        return totalCount
    }
    
    func loadAllOrderDetails() -> [OrderDetail] {
        
        var orderDetails: [OrderDetail] = []
        
        databaseQueue.sync { (Void) in
            let result = self.database.executeQuery("SELECT * FROM \(self.orderDetailsTableName) ORDER BY billing_no ASC", withArgumentsIn:nil)!
            
            while result.next() {
                let orderDetail = OrderDetail.orderDetailFrom(queryResult: result)
                orderDetails.append(orderDetail)
            }
        }
        return orderDetails
    }
    
    func loadAllOrderDetailsFor(productId: UInt64, date: Date, limit: UInt16, completion: @escaping (_ result: [OrderDetail]) -> ()) {
        
        //print("FLT_MIN ==> \(FLT_MIN)")
        //print("DBL_MIN ==> \(DBL_MIN)")
        
        let number = NSNumber(value: date.timeIntervalSince1970 * 1000 - DBL_MIN)
        print("input from database NSNumber(value:) ==> \(number) -> \(NSNumber(value: date.timeIntervalSince1970 * 1000))")
        
        var orderDetails: [OrderDetail] = []
        databaseQueue.async { (Void) in
            
            let result = self.database.executeQuery("SELECT * FROM \(self.orderDetailsTableName) WHERE order_detail_date <? AND product_id=? ORDER BY order_detail_date DESC LIMIT ?", withArgumentsIn:[NSNumber(value: date.timeIntervalSince1970 * 1000), NSNumber(value: productId), NSNumber(value: limit)])!
            
            while result.next() {
                let orderDetail = OrderDetail.orderDetailFrom(queryResult: result)
                
                let orderDetailDateUInt = UInt64(orderDetail.orderDetailDate.timeIntervalSince1970 * 1000)
                print("get from database orderDetailDateUInt ==> \(orderDetailDateUInt)")
                
                orderDetails.append(orderDetail)
            }
            
            DispatchQueue.main.async {
                completion(orderDetails)
            }
        }
    }
    

    func countOrderDetailsBy(orderId: UInt64) -> Int32 {
        
        var totalCount: Int32 = 0
        databaseQueue.sync { (Void) in
            
            let result = self.database.executeQuery("SELECT COUNT(*) FROM \(self.orderDetailsTableName) WHERE order_id=?", withArgumentsIn:[NSNumber(value: orderId)])
            
            if let result_ = result {
                if result_.next() {
                    totalCount = result_.int(forColumn: "COUNT(*)")
                }
            }
        }
        
        return totalCount
    }
    
    func orderDetailsBy(orderId: UInt64) -> [OrderDetail] {
        var orderDetails: [OrderDetail] = []
        databaseQueue.sync { (Void) in
            let result = self.database.executeQuery("SELECT * FROM \(self.orderDetailsTableName) WHERE order_id=?", withArgumentsIn:[NSNumber(value: orderId)])
            
            if let result_ = result {
                while result_.next() {
                    let orderDetail = OrderDetail.orderDetailFrom(queryResult: result_)
                    orderDetails.append(orderDetail)
                }
            }
        }
        return orderDetails
    }
    
    func orderDetailsBy(orderId: UInt64, completion: @escaping (_ result: [OrderDetail]) -> ()) {
        var orderDetails: [OrderDetail] = []
        databaseQueue.async { (Void) in
            let result = self.database.executeQuery("SELECT * FROM \(self.orderDetailsTableName) WHERE order_id=?", withArgumentsIn:[NSNumber(value: orderId)])
            
            if let result_ = result {
                while result_.next() {
                    let orderDetail = OrderDetail.orderDetailFrom(queryResult: result_)
                    orderDetails.append(orderDetail)
                }
            }
            DispatchQueue.main.async {
                completion(orderDetails)
            }
        }
    }
    
    func orderDetailBy(orderDetailId: UInt64) -> OrderDetail? {
        
        var orderDetails: [OrderDetail] = []
        databaseQueue.sync { (Void) in
            let result = self.database.executeQuery("SELECT * FROM \(self.orderDetailsTableName) WHERE order_detail_id=? LIMIT 1", withArgumentsIn:[NSNumber(value: orderDetailId)])
            
            if let result_ = result {
                
                while result_.next() {
                    let orderDetail = OrderDetail.orderDetailFrom(queryResult: result_)
                    orderDetails.append(orderDetail)
                }
            }
        }
        
        if orderDetails.count > 0 {
            return orderDetails[0]
        }
        
        return nil
    }
    
    
    func createorderDetails(_ orderDetails: [OrderDetail]) -> Void {
        
        databaseQueue.async { (Void) in
            
            self.database.beginTransaction()
            for orderDetail in orderDetails {
                self.storeOrderDetail(orderDetail)
            }
            self.database.commit()
        }
    }
    
    private func storeOrderDetail(_ orderDetail: OrderDetail) -> Void {
        
        databaseQueue.async { (Void) in
            
            let queryFormat = "INSERT OR REPLACE INTO \(self.orderDetailsTableName) (order_detail_id, order_id, order_detail_date, is_last, billing_no, comment, uuid, product_id, price, order_quantity, product_quantity) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
            
            do {
                try self.database.executeUpdate(queryFormat, values: orderDetail.databaseInsertValues())
                
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
        }
    }
    
    func deleteOrderDetail(_ orderDetailId: UInt64) -> Void {
        databaseQueue.async { (Void) in
            self.database.beginTransaction()
            let queryFormat = "DELETE FROM \(self.orderDetailsTableName) WHERE order_detail_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: orderDetailId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    func updateOrderDetailsPrimaryKey(oldId: UInt64, newId: UInt64) -> Void {
        
        databaseQueue.sync { (Void) in
            self.database.beginTransaction()
            let queryFormat = "UPDATE \(self.orderDetailsTableName) SET order_detail_id=? WHERE order_detail_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: newId), NSNumber(value: oldId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    func updateOrderDetailsProductId(orderDetailId: UInt64, productId: UInt64) -> Void {
        
        databaseQueue.sync { (Void) in
            self.database.beginTransaction()
            let queryFormat = "UPDATE \(self.orderDetailsTableName) SET product_id=? WHERE order_detail_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: productId), NSNumber(value: orderDetailId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    func updateOrderDetailsOrderId(oldOrderId: UInt64, newOrderId: UInt64) -> Void {
        
        databaseQueue.sync { (Void) in
            self.database.beginTransaction()
            let queryFormat = "UPDATE \(self.orderDetailsTableName) SET order_id=? WHERE order_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: newOrderId), NSNumber(value: oldOrderId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    //--------------------------------------------------------------------------------
    // MARK: Staff
    func loadAllUsers() -> [User] {
        
        var users: [User] = []
        
        databaseQueue.sync { (Void) in
            let result = self.database.executeQuery("SELECT * FROM \(self.usersTableName) ORDER BY first_name ASC", withArgumentsIn:nil)!
            
            while result.next() {
                let user = User.userFrom(queryResult: result)
                users.append(user)
            }
        }
        return users
    }
    
    func searchUserBy(name: String, completion:@escaping ([User]) -> ()) {
        
        var users: [User] = []
        
        databaseQueue.async { (Void) in
            
            let result = self.database.executeQuery("SELECT * FROM \(self.usersTableName) WHERE first_name LIKE '%\(name)%' OR second_name LIKE '%\(name)%' ORDER BY first_name ASC", withArgumentsIn:nil)!
            
            while result.next() {
                let user = User.userFrom(queryResult: result)
                users.append(user)
            }
            
            DispatchQueue.main.async {
                completion(users)
            }
        }
    }
    
    func currentDeviceOwnerUser() -> User? {
        
        var users: [User] = []
        databaseQueue.sync { (Void) in
            let result = self.database.executeQuery("SELECT * FROM \(self.usersTableName) WHERE is_current_user=? LIMIT 1", withArgumentsIn:[NSNumber(value: true)])
            
            if let result_ = result {
                while result_.next() {
                    let user = User.userFrom(queryResult: result_)
                    users.append(user)
                }
            }
        }
        
        if users.count > 0 {
            return users[0]
        }
        
        return nil
    }
    
    func userBy(userId: UInt64) -> User? {
        
        var users: [User] = []
        databaseQueue.sync { (Void) in
            let result = self.database.executeQuery("SELECT * FROM \(self.usersTableName) WHERE user_id=? LIMIT 1", withArgumentsIn:[NSNumber(value: userId)])
            
            if let result_ = result {
                while result_.next() {
                    let user = User.userFrom(queryResult: result_)
                    users.append(user)
                }
            }
        }
        
        if users.count > 0 {
            return users[0]
        }
        
        return nil
    }
    
    func userBy(email: String) -> User? {
        
        var users: [User] = []
        databaseQueue.sync { (Void) in
            let result = self.database.executeQuery("SELECT * FROM \(self.usersTableName) WHERE email='\(email)' LIMIT 1", withArgumentsIn:nil)
            
            if let result_ = result {
                while result_.next() {
                    let user = User.userFrom(queryResult: result_)
                    users.append(user)
                }
            }
        }
        
        if users.count > 0 {
            return users[0]
        }
        
        return nil
    }
    
    func createUser(_ users: [User]) -> Void {
        
        databaseQueue.async { (Void) in
            
            self.database.beginTransaction()
            for user in users {
                self.storeUser(user)
            }
            self.database.commit()
        }
    }
    
    private func storeUser(_ user: User) -> Void {
        
        databaseQueue.async { (Void) in
            
            let queryFormat = "INSERT OR REPLACE INTO \(self.usersTableName) (user_id, uuid, is_current_user, role_id, user_image_path, first_name, second_name, email, password, phone_number, address) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
            
            do {
                try self.database.executeUpdate(queryFormat, values: user.databaseInsertValues())
                
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
        }
    }
    
    func deleteUser(_ userId: UInt64) -> Void {
        databaseQueue.async { (Void) in
            self.database.beginTransaction()
            let queryFormat = "DELETE FROM \(self.usersTableName) WHERE user_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: userId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    func updateUsersPrimaryKey(oldUserId: UInt64, newUserId: UInt64) -> Void {
        
        databaseQueue.sync { (Void) in
            self.database.beginTransaction()
            let queryFormat = "UPDATE \(self.usersTableName) SET user_id=? WHERE user_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: newUserId), NSNumber(value: oldUserId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    func updateUser(_ user: User) -> Void {
        
        databaseQueue.async { (Void) in
            self.database.beginTransaction()
            let queryFormat = "UPDATE \(self.usersTableName) SET is_current_user=?, role_id=?, user_image_path=?, first_name=?, second_name=?, email=?, password=?, phone_number=?, address=? WHERE user_id=?"
            
            do {
                try self.database.executeUpdate(queryFormat, values: user.databaseUpdateValues())
                
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    //--------------------------------------------------------------------------------
    // MARK: Customers
    
    func loadAllCustomers(_ limit: UInt64) -> [Customer] {
        
        var customers: [Customer] = []
        
        databaseQueue.sync { (Void) in
            
            let result = self.database.executeQuery("SELECT * FROM \(self.customersTableName) ORDER BY first_name ASC LIMIT ?", withArgumentsIn:[NSNumber(value: limit)])!
            
            while result.next() {
                let customer = Customer.customerFrom(queryResult: result)
                customers.append(customer)
            }
        }
        return customers
    }
    
    func loadAllCustomers(limit: UInt64, userId: UInt64, completion:@escaping ([Customer]) -> ()) -> Void {
        
        var customers: [Customer] = []
        
        databaseQueue.async { (Void) in
            
            let result: FMResultSet
            if userId > 0 {
                result = self.database.executeQuery("SELECT * FROM \(self.customersTableName) WHERE user_id=? ORDER BY first_name ASC LIMIT ?", withArgumentsIn:[NSNumber(value: userId), NSNumber(value: limit)])!
            } else {
                result = self.database.executeQuery("SELECT * FROM \(self.customersTableName) ORDER BY first_name ASC LIMIT ?", withArgumentsIn:[NSNumber(value: limit)])!
            }
            
            while result.next() {
                let customer = Customer.customerFrom(queryResult: result)
                customers.append(customer)
            }
            
            DispatchQueue.main.async {
                completion(customers)
            }
        }
    }
    
    func searchCustomerBy(name: String, limit: UInt16, completion:@escaping ([Customer]) -> ()) {
        
        var customers: [Customer] = []
        
        databaseQueue.async { (Void) in
            
            //SELECT * FROM mytable
            //WHERE caseSensitiveField like 'test%'
            //AND UPPER(caseInsensitiveField) like 'G2%'
            
            let result = self.database.executeQuery("SELECT * FROM \(self.customersTableName) WHERE first_name LIKE '%\(name)%' OR second_name LIKE '%\(name)%' ORDER BY first_name ASC LIMIT ?", withArgumentsIn:[NSNumber(value: limit)])!
            
            while result.next() {
                let customer = Customer.customerFrom(queryResult: result)
                customers.append(customer)
            }
            
            DispatchQueue.main.async {
                completion(customers)
            }
        }
    }
    
    func customerBy(firstName: String, secondName: String) -> Customer? {
        var customers: [Customer] = []
        databaseQueue.sync { (Void) in
            let result = self.database.executeQuery("SELECT * FROM \(self.customersTableName) WHERE first_name=? AND second_name=? LIMIT 1", withArgumentsIn:[firstName, secondName])
            if let result_ = result {
                while result_.next() {
                    let customer = Customer.customerFrom(queryResult: result_)
                    customers.append(customer)
                }
            }
        }
        if customers.count > 0 {
            return customers[0]
        }
        return nil
    }
    
    func customerBy(customerId: UInt64) -> Customer? {
        
        var customers: [Customer] = []
        databaseQueue.sync { (Void) in
            let result = self.database.executeQuery("SELECT * FROM \(self.customersTableName) WHERE customer_id=? LIMIT 1", withArgumentsIn:[NSNumber(value: customerId)])
            
            if let result_ = result {
                
                while result_.next() {
                    let customer = Customer.customerFrom(queryResult: result_)
                    customers.append(customer)
                }
            }
        }
        
        if customers.count > 0 {
            return customers[0]
        }
        
        return nil
    }
    
    func customerBy(customerName: String) -> Customer? {
        
        var customers: [Customer] = []
        databaseQueue.sync { (Void) in
            let result = self.database.executeQuery("SELECT * FROM \(self.customersTableName) WHERE first_name=? LIMIT 1", withArgumentsIn:[customerName])
            
            if let result_ = result {
                
                while result_.next() {
                    let customer = Customer.customerFrom(queryResult: result_)
                    customers.append(customer)
                }
            }
        }
        
        if customers.count > 0 {
            return customers[0]
        }
        
        return nil
    }
    
    func updateCustomersPrimaryKey(oldCustomerId: UInt64, newCustomerId: UInt64) -> Void {
        
        databaseQueue.sync { (Void) in
            self.database.beginTransaction()
            let queryFormat = "UPDATE \(self.customersTableName) SET customer_id=? WHERE customer_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: newCustomerId), NSNumber(value: oldCustomerId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    func createCustomers(_ customers: [Customer]) -> Void {
        
        databaseQueue.async { (Void) in
            self.database.beginTransaction()
            for customer in customers {
                self.storeCustomer(customer)
            }
            self.database.commit()
        }
    }
    
    private func storeCustomer(_ customer: Customer) -> Void {
        
        databaseQueue.async { (Void) in
            
            let queryFormat = "INSERT OR REPLACE INTO \(self.customersTableName) (customer_id, customer_image_path, first_name, second_name, phone_number, address, user_id) VALUES (?, ?, ?, ?, ?, ?, ?)"
            
            do {
                try self.database.executeUpdate(queryFormat, values: customer.databaseInsertValues())
                
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
        }
    }
    
    func deleteCustomer(_ customerId: UInt64) -> Void {
        databaseQueue.async { (Void) in
            self.database.beginTransaction()
            let queryFormat = "DELETE FROM \(self.customersTableName) WHERE customer_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: customerId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    func updateCustomer(_ customer: Customer) -> Void {
        
        databaseQueue.async { (Void) in
            self.database.beginTransaction()
            let queryFormat = "UPDATE \(self.customersTableName) SET customer_image_path=?, first_name=?, second_name=?, phone_number=?, address=?, user_id=? WHERE customer_id=?"
            
            do {
                try self.database.executeUpdate(queryFormat, values: customer.databaseUpdateValues())
                
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    //--------------------------------------------------------------------------------
    // MARK: Suppliers
    func loadAllSuppliers(_ limit: UInt64) -> [Supplier] {
        
        var suppliers: [Supplier] = []
        
        databaseQueue.sync { (Void) in
            
            let result = self.database.executeQuery("SELECT * FROM \(self.suppliersTableName) ORDER BY company_name ASC LIMIT ?", withArgumentsIn:[NSNumber(value: limit)])!
            
            while result.next() {
                let supplier = Supplier.supplierFrom(queryResult: result)
                suppliers.append(supplier)
            }
        }
        return suppliers
    }
    
    func loadAllSuppliers(limit: UInt64, completion:@escaping ([Supplier]) -> ()) -> Void {
        
        var suppliers: [Supplier] = []
        
        databaseQueue.async { (Void) in
            
            let result = self.database.executeQuery("SELECT * FROM \(self.suppliersTableName) ORDER BY company_name ASC LIMIT ?", withArgumentsIn:[NSNumber(value: limit)])!
            
            while result.next() {
                let supplier = Supplier.supplierFrom(queryResult: result)
                suppliers.append(supplier)
            }
            
            DispatchQueue.main.async {
                completion(suppliers)
            }
        }
    }
    
    func searchSupplierBy(name: String, completion:@escaping ([Supplier]) -> ()) {
        
        var suppliers: [Supplier] = []
        
        databaseQueue.async { (Void) in
            
            let result = self.database.executeQuery("SELECT * FROM \(self.suppliersTableName) WHERE company_name LIKE '%\(name)%' ORDER BY company_name ASC", withArgumentsIn:nil)!
            
            while result.next() {
                let supplier = Supplier.supplierFrom(queryResult: result)
                suppliers.append(supplier)
            }
            
            DispatchQueue.main.async {
                completion(suppliers)
            }
        }
    }
    
    func supplierBy(supplierId: UInt64) -> Supplier? {
        
        var suppliers: [Supplier] = []
        databaseQueue.sync { (Void) in
            let result = self.database.executeQuery("SELECT * FROM \(self.suppliersTableName) WHERE supplier_id=? LIMIT 1", withArgumentsIn:[NSNumber(value: supplierId)])
            
            if let result_ = result {
                
                while result_.next() {
                    let supplier = Supplier.supplierFrom(queryResult: result_)
                    suppliers.append(supplier)
                }
            }
        }
        
        if suppliers.count > 0 {
            return suppliers[0]
        }
        
        return nil
    }
    
    
    func createSuppliers(_ suppliers: [Supplier]) -> Void {
        
        databaseQueue.async { (Void) in
            
            self.database.beginTransaction()
            for supplier in suppliers {
                self.storeSupplier(supplier)
            }
            self.database.commit()
        }
    }
    
    private func storeSupplier(_ supplier: Supplier) -> Void {
        
        databaseQueue.async { (Void) in
            
            let queryFormat = "INSERT OR REPLACE INTO \(self.suppliersTableName) (supplier_id, supplier_image_path, company_name, contact_fname, phone_number, address) VALUES (?, ?, ?, ?, ?, ?)"
            
            do {
                try self.database.executeUpdate(queryFormat, values: supplier.databaseInsertValues())
                
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
        }
    }
    
    func deleteSupplier(_ supplierId: UInt64) -> Void {
        databaseQueue.async { (Void) in
            self.database.beginTransaction()
            let queryFormat = "DELETE FROM \(self.suppliersTableName) WHERE supplier_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: supplierId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    func updateSuppliersPrimaryKey(oldSupplierId: UInt64, newSupplierId: UInt64) -> Void {
        
        databaseQueue.sync { (Void) in
            self.database.beginTransaction()
            let queryFormat = "UPDATE \(self.suppliersTableName) SET supplier_id=? WHERE supplier_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: newSupplierId), NSNumber(value: oldSupplierId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    func updateSupplier(_ supplier: Supplier) -> Void {
        
        databaseQueue.async { (Void) in
            self.database.beginTransaction()
            let queryFormat = "UPDATE \(self.suppliersTableName) SET supplier_image_path=?, company_name=?, contact_fname=?, phone_number=?, address=? WHERE supplier_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: supplier.databaseUpdateValues())
                
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    //--------------------------------------------------------------------------------
    // MARK: Categories
    func loadAllCategories() -> [Category] {
        
        var categories: [Category] = []
        
        databaseQueue.sync { (Void) in
            let result = self.database.executeQuery("SELECT * FROM \(self.categoriesTableName) ORDER BY category_name ASC", withArgumentsIn:nil)!
            
            while result.next() {
                let category = Category.categoryFrom(queryResult: result)
                categories.append(category)
            }
        }
        
        return categories
    }
    
    func searchCategoryBy(name: String, completion:@escaping ([Category]) -> ()) {
        
        var categories: [Category] = []
        
        databaseQueue.async { (Void) in
            
            let result = self.database.executeQuery("SELECT * FROM \(self.categoriesTableName) WHERE category_name LIKE '%\(name)%' ORDER BY category_name ASC", withArgumentsIn:nil)!
            
            while result.next() {
                let category = Category.categoryFrom(queryResult: result)
                categories.append(category)
            }
            
            DispatchQueue.main.async {
                completion(categories)
            }
        }
    }
    
    func categoryBy(categoryId: UInt64) -> Category? {
        
        var categories: [Category] = []
        databaseQueue.sync { (Void) in
            let result = self.database.executeQuery("SELECT * FROM \(self.categoriesTableName) WHERE category_id=? LIMIT 1", withArgumentsIn:[NSNumber(value: categoryId)])
            
            if let result_ = result {
                
                while result_.next() {
                    let category = Category.categoryFrom(queryResult: result_)
                    categories.append(category)
                }
            }
        }
        
        if categories.count > 0 {
            return categories[0]
        }
        
        return nil
    }
    
    func updateCategorysPrimaryKey(oldCategoryId: UInt64, newCategoryId: UInt64) -> Void {
        
        databaseQueue.sync { (Void) in
            self.database.beginTransaction()
            let queryFormat = "UPDATE \(self.categoriesTableName) SET category_id=? WHERE category_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: newCategoryId), NSNumber(value: oldCategoryId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    func updateCategory(_ category: Category) -> Void {
        
        databaseQueue.async { (Void) in
            self.database.beginTransaction()
            let queryFormat = "UPDATE \(self.categoriesTableName) SET category_name=? WHERE category_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [category.categoryName, category.categoryId])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    func createCategories(_ categories: [Category]) -> Void {
        
        databaseQueue.async { (Void) in
            
            self.database.beginTransaction()
            for category in categories {
                self.storeCategory(category)
            }
            self.database.commit()
        }
    }
    
    private func storeCategory(_ category: Category) -> Void {
        
        databaseQueue.async { (Void) in
            
            let queryFormat = "INSERT OR REPLACE INTO \(self.categoriesTableName) (category_id, category_name) VALUES (?, ?)"
            
            do {
                try self.database.executeUpdate(queryFormat, values: [category.categoryId, category.categoryName])
                
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
        }
    }
    
    func deleteCategory(_ categoryId: UInt64) -> Void {
        databaseQueue.async { (Void) in
            self.database.beginTransaction()
            let queryFormat = "DELETE FROM \(self.categoriesTableName) WHERE category_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: categoryId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    //--------------------------------------------------------------------------------
    // MARK: Products
    func createProducts(products: [Product]) -> Void {
        
        databaseQueue.async { (Void) in
            
            self.database.beginTransaction()
            for product in products {
                self.storeProduct(product)
            }
            self.database.commit()
        }
    }
    
    private func storeProduct(_ product: Product) -> Void {
    
        databaseQueue.async { (Void) in
            
            let queryFormat = "INSERT OR REPLACE INTO \(self.productsTableName) (product_id, product_image_path, product_name, supplier_id, category_id, barcode, quantity_per_unit, sale_unit_price, income_unit_price, units_in_stock) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
            
            do {
                try self.database.executeUpdate(queryFormat, values: product.databaseInsertValues())
                
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            
            self.addProductToIndex(product: product)
        }
    }
    
    func deleteProduct(_ productId: UInt64) -> Void {
        databaseQueue.async { (Void) in
            self.database.beginTransaction()
            let queryFormat = "DELETE FROM \(self.productsTableName) WHERE product_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: productId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    func updateProducsPrimaryKey(oldProductId: UInt64, newProduct: UInt64) -> Void {
        
        databaseQueue.sync { (Void) in
            self.database.beginTransaction()
            let queryFormat = "UPDATE \(self.productsTableName) SET product_id=? WHERE product_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: newProduct), NSNumber(value: oldProductId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
            
            self.updateProductIndex(oldProductId: oldProductId, newProductId: newProduct)
        }
    }
    
    func updateProductsCategoryId(oldCategoryId: UInt64, newCategoryId: UInt64) -> Void {
        
        databaseQueue.sync { (Void) in
            self.database.beginTransaction()
            let queryFormat = "UPDATE \(self.productsTableName) SET category_id=? WHERE category_id=?"
            do {
                try self.database.executeUpdate(queryFormat, values: [NSNumber(value: newCategoryId), NSNumber(value: oldCategoryId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
        }
    }
    
    func updateProduct(_ product: Product) -> Void {
        
        databaseQueue.async { (Void) in
            self.database.beginTransaction()
            
            //return [self.productImagePath ?? "", self.productName, self.supplierId, self.categoryId, self.barcode, self.quantityPerUnit, self.saleUnitPrice, self.incomeUnitPrice, self.unitsInStock, self.productId]
            
            let queryFormat = "UPDATE \(self.productsTableName) SET product_image_path=?, product_name=?, supplier_id=?, category_id=?, barcode=?, quantity_per_unit=?, sale_unit_price=?, income_unit_price=?, units_in_stock=? WHERE product_id=?"
            
            do {
                try self.database.executeUpdate(queryFormat, values: product.databaseUpdateValues())
                
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.database.commit()
            
            self.updateProductNameInIndex(product)
        }
    }
    
    func addProductToIndex(product: Product) -> Void {
        databaseIndexQueue.async {
            self.indexDatabase.beginTransaction()
            do {
                try self.indexDatabase.executeUpdate("INSERT OR REPLACE INTO \(self.productsIndexTableName) (docid, text) VALUES (?, ?)", values: [NSNumber(value: product.productId), product.productName.lowercased()])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.indexDatabase.commit()
        }
    }
    
    func updateProductNameInIndex(_ product: Product) -> Void {
        databaseIndexQueue.async {
            self.indexDatabase.beginTransaction()
            do {
                try self.indexDatabase.executeUpdate("UPDATE \(self.productsIndexTableName) SET text=? WHERE docid=?", values: [product.productName.lowercased(), NSNumber(value: product.productId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.indexDatabase.commit()
        }
    }
    
    func updateProductIndex(oldProductId: UInt64, newProductId: UInt64) -> Void {
        databaseIndexQueue.async {
            self.indexDatabase.beginTransaction()
            do {
                try self.indexDatabase.executeUpdate("UPDATE \(self.productsIndexTableName) SET docid=? WHERE docid=?", values: [NSNumber(value: newProductId), NSNumber(value: oldProductId)])
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
            self.indexDatabase.commit()
        }
    }
    
    func deleteProductsFromIndex(productIds: [UInt64]) -> Void {
        var rangeString = ""
        for productId in productIds {
            
            if rangeString.characters.count > 0 {
                rangeString = rangeString + ","
            }
            
            rangeString = rangeString + "\(productId)"
        }
    
        databaseIndexQueue.async { (Void) in
            do {
                try self.indexDatabase.executeUpdate("DELETE FROM \(self.productsIndexTableName) WHERE docid in (\(rangeString)) ", values:nil)
            } catch let error as NSError {
                print("failed: \(error.localizedDescription)")
            }
        }
    }
    
    func loadAllProductsBy(categoryId: UInt64, limit: UInt64, completion: @escaping (_ result: [Product]) -> ()) {
        
        var products: [Product] = []
        
        databaseQueue.async { (Void) in
            let result: FMResultSet
            if categoryId > 0 {
                result = self.database.executeQuery("SELECT * FROM \(self.productsTableName) WHERE category_id=? ORDER BY product_name ASC LIMIT ?", withArgumentsIn:[NSNumber(value: categoryId), NSNumber(value: limit)])!
            } else {
                result = self.database.executeQuery("SELECT * FROM \(self.productsTableName) ORDER BY product_name ASC LIMIT ?", withArgumentsIn:[NSNumber(value: limit)])!
            }
            
            while result.next() {
                let product = Product.productFrom(queryResult: result)
                products.append(product)
            }
            
            DispatchQueue.main.async {
                completion(products)
            }
        }
    }
    
    func loadAllProductsBy(supplierId: UInt64, limit: UInt64, completion: @escaping (_ result: [Product]) -> ()) {
        
        var products: [Product] = []
        
        databaseQueue.async { (Void) in
            let result: FMResultSet
            if supplierId > 0 {
                result = self.database.executeQuery("SELECT * FROM \(self.productsTableName) WHERE supplier_id=? ORDER BY product_name ASC LIMIT ?", withArgumentsIn:[NSNumber(value: supplierId), NSNumber(value: limit)])!
            } else {
                result = self.database.executeQuery("SELECT * FROM \(self.productsTableName) ORDER BY product_name ASC LIMIT ?", withArgumentsIn:[NSNumber(value: limit)])!
            }
            
            while result.next() {
                let product = Product.productFrom(queryResult: result)
                products.append(product)
            }
            
            DispatchQueue.main.async {
                completion(products)
            }
        }
    }
    
    func loadAllProducts(_ limit: UInt64) -> [Product] {
        
        var products: [Product] = []
        
        databaseQueue.sync { (Void) in
            
            let result = self.database.executeQuery("SELECT * FROM \(self.productsTableName) ORDER BY product_name ASC LIMIT ?", withArgumentsIn:[NSNumber(value: limit)])!
            
            while result.next() {
                let product = Product.productFrom(queryResult: result)
                products.append(product)
            }
        }
        
        return products
    }
    
    func productsFor(categoryId: UInt64) -> [Product] {
        
        var products: [Product] = []
        
        databaseQueue.sync { (Void) in
            
            let result = self.database.executeQuery("SELECT * FROM \(self.productsTableName) WHERE category_id=? ORDER BY product_name ASC", withArgumentsIn:[NSNumber(value: categoryId)])!
            
            while result.next() {
                let product = Product.productFrom(queryResult: result)
                products.append(product)
            }
        }
        
        return products
    }
    
    func updateProductUnitsInStock(productsAndQuantityDict: [UInt64: Double]) -> Void {
        
        databaseQueue.async { (Void) in
            self.database.beginTransaction()
            for (productId, unitInStock) in productsAndQuantityDict {
                self.database.executeUpdate("UPDATE \(self.productsTableName) SET units_in_stock=? WHERE product_id=?",   withArgumentsIn: [NSNumber(value: unitInStock), NSNumber(value: productId)])
            }
            self.database.commit()
        }
    }
    
    func productsFor(productIds: [UInt64]) -> [Product] {
        
        var products: [Product] = []
        
        var rangeString = ""
        for productId in productIds {
            
            if rangeString.characters.count > 0 {
                rangeString = rangeString + ","
            }
            
            rangeString = rangeString + "\(productId)"
        }
        
        databaseQueue.sync { (Void) in
            
            let result = self.database.executeQuery("SELECT * FROM \(self.productsTableName) WHERE product_id in (\(rangeString)) ", withArgumentsIn:nil)!
            
            while result.next() {
                let product = Product.productFrom(queryResult: result)
                products.append(product)
            }
        }
        
        return products
    }
    
    
    func productFor(barcode: String) -> Product? {
        
        var products: [Product] = []
        databaseQueue.sync { (Void) in
            let result = self.database.executeQuery("SELECT * FROM \(self.productsTableName) WHERE barcode='\(barcode)' LIMIT 1", withArgumentsIn:nil)
            
            if let result_ = result {
                while result_.next() {
                    let product = Product.productFrom(queryResult: result_)
                    products.append(product)
                }
            }
        }
        
        if products.count > 0 {
            return products[0]
        }
        
        return nil
    }
    
    func productFor(productId: UInt64) -> Product? {
        
        var products: [Product] = []
        databaseQueue.sync { (Void) in
            let result = self.database.executeQuery("SELECT * FROM \(self.productsTableName) WHERE product_id=? LIMIT 1", withArgumentsIn:[NSNumber(value: productId)])
            
            if let result_ = result {
                
                while result_.next() {
                    let product = Product.productFrom(queryResult: result_)
                    products.append(product)
                }
            }
        }
        
        if products.count > 0 {
            return products[0]
        }
        
        return nil
    }
    
    func search(productName: String, completion:@escaping ([Product]) -> ()) -> Void {
        
        var products: [Product] = []
        var productIdx: [UInt64] = []
        
        let nameString = productName.lowercased().replacingOccurrences(of: "*", with: "")
        
        databaseIndexQueue.async {
        
            let querryStr = "SELECT docid FROM \(self.productsIndexTableName) WHERE text MATCH '\(nameString)*' ORDER BY docid DESC"
            print("querryStr ===> \(querryStr)")
            let result = self.indexDatabase.executeQuery(querryStr, withArgumentsIn:nil)!
            let docidIndex = result.columnIndex(forName: "docid")
        
            while result.next() {
                let productId = result.unsignedLongLongInt(forColumnIndex: docidIndex)
                productIdx.append(productId)
            }
            
            var rangeString = ""
            for productId in productIdx {
                
                if rangeString.characters.count > 0 {
                    rangeString = rangeString + ","
                }
                
                rangeString = rangeString + "\(productId)"
            }
            
            databaseQueue.async { (Void) in
                
                let result = self.database.executeQuery("SELECT * FROM \(self.productsTableName) WHERE product_id in (\(rangeString)) ORDER BY product_name ASC", withArgumentsIn:nil)!
                
                while result.next() {
                    let product = Product.productFrom(queryResult: result)
                    products.append(product)
                }
                
                DispatchQueue.main.async {
                    completion(products)
                }
            }
        }
    }
    
    func searchProductBy(name: String) -> [Product] {
        
        var products: [Product] = []
        var productIdx: [UInt64] = []
        
        let nameString = name.lowercased().replacingOccurrences(of: "*", with: "")
        
        databaseIndexQueue.sync {
            
            let querryStr = "SELECT docid FROM \(self.productsIndexTableName) WHERE text MATCH '\(nameString)*' ORDER BY docid DESC"
            print("querryStr ===> \(querryStr)")
            let result = self.indexDatabase.executeQuery(querryStr, withArgumentsIn:nil)!
            let docidIndex = result.columnIndex(forName: "docid")
            
            while result.next() {
                let productId = result.unsignedLongLongInt(forColumnIndex: docidIndex)
                productIdx.append(productId)
            }
        }
        
        var rangeString = ""
        for productId in productIdx {
            
            if rangeString.characters.count > 0 {
                rangeString = rangeString + ","
            }
            
            rangeString = rangeString + "\(productId)"
        }
        
        databaseQueue.sync { (Void) in
            
            let result = self.database.executeQuery("SELECT * FROM \(self.productsTableName) WHERE product_id in (\(rangeString)) ", withArgumentsIn:nil)!
            
            while result.next() {
                let product = Product.productFrom(queryResult: result)
                products.append(product)
            }
        }
        
        return products
    }
    
    func searchProductBy2(name: String) -> [Product] {
        
        var products: [Product] = []
        
        databaseQueue.sync { (Void) in
            
            let result = self.database.executeQuery("SELECT * FROM \(self.productsTableName) WHERE product_name LIKE '%\(name)%'", withArgumentsIn:nil)!
            
            while result.next() {
                let product = Product.productFrom(queryResult: result)
                products.append(product)
            }
        }
        
        return products
    }

}
