//
//  GenerateSamples.swift
//  Rentautomation
//
//  Created by kanybek on 1/31/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit

extension String {
    
    var firstName: String {
        return components(separatedBy: " ").first ?? ""
    }
    
    var lastName: String {
        return components(separatedBy: " ").last ?? ""
    }
    
}

let generateQue: Queue = Queue(name: "generate.queueue")

final class GenerateSamples {
    
    var isEquating: Bool
    var iterations: Int
    
    static let shared: GenerateSamples = GenerateSamples()
    
    init() {
        self.isEquating = false
        self.iterations = 2
    }
    
    private lazy var productNames: [String] = {
        var arrayStrings_: [String] = []
        if let filepath = Bundle.main.path(forResource: "product_samples", ofType: "txt") {
            do {
                let contents = try String(contentsOfFile: filepath)
                let fullNameArr = contents.components(separatedBy: "\n")
                
                for name in fullNameArr {
                    
                    let nameString = name.trimmingCharacters(in: .whitespaces)
                    //let nameString1 = nameString.replacingOccurrences(of: "«", with: "")
                    //let nameString2 = nameString1.replacingOccurrences(of: "»", with: "")
                    //let nameString3 = nameString2.replacingOccurrences(of: "", with: "")
                    
                    if nameString.characters.count > 4 {
                        
                        print("==> \(nameString) <==")
                        print("----------------------")
                        arrayStrings_.append(nameString)
                    }
                }
            } catch {
            }
        }
        return arrayStrings_
    }()
    
    private lazy var customerNames: [(String, String)] = {
        var arrayStrings_: [(String, String)] = []
        if let filepath = Bundle.main.path(forResource: "customer_names", ofType: "txt") {
            do {
                let contents = try String(contentsOfFile: filepath)
                let fullNameArr = contents.components(separatedBy: "\n")
                
                for name in fullNameArr {

                    if name.characters.count > 4 {
                        arrayStrings_.append((name.firstName, name.lastName))
                    }
                }
            } catch {
            }
        }
        return arrayStrings_
    }()
    
    private lazy var supplierNames: [String] = {
        var arrayStrings_: [String] = []
        if let filepath = Bundle.main.path(forResource: "supplier_names", ofType: "txt") {
            do {
                let contents = try String(contentsOfFile: filepath)
                let fullNameArr = contents.components(separatedBy: "\n")
                
                for name in fullNameArr {
                    
                    let nameString = name.trimmingCharacters(in: .whitespaces)
                    if nameString.characters.count > 4 {
                        arrayStrings_.append(nameString)
                    }
                }
            } catch {
            }
        }
        return arrayStrings_
    }()
    
    private lazy var staffNames: [(String, String)] = {
        var arrayStrings_: [(String, String)] = []
        if let filepath = Bundle.main.path(forResource: "staff_names", ofType: "txt") {
            do {
                let contents = try String(contentsOfFile: filepath)
                let fullNameArr = contents.components(separatedBy: "\n")
                
                for name in fullNameArr {
                    
                    if name.characters.count > 4 {
                        arrayStrings_.append((name.firstName, name.lastName))
                    }
                }
            } catch {
            }
        }
        return arrayStrings_
    }()
    
    func generateStaticModelsForCreate(completion:@escaping (Bool) -> ()) {
        
        if self.isEquating {
            return
        }
        
        if !self.isEquating {
            
            self.isEquating = true
            
            generateQue.async {
                
                self.createCategories()
                self.createProducts()
                self.createCustomers()
                self.createSuppliers()
                self.createStaff()
                
                DispatchQueue.main.async {
                    self.isEquating = false
                    completion(true)
                }
            }
        }
    }
    
    func generateStaticModelsForUpdate(completion:@escaping (Bool) -> ()) {
        
        if self.isEquating {
            return
        }
        
        if !self.isEquating {
            
            self.isEquating = true
            
            generateQue.async {
                
                self.updateCategories()
                self.updateProducts()
                self.updateCustomers()
                self.updateCustomersBallance()
                self.updateSuppliers()
                self.updateSuppliersBallance()
                self.updateStaff()
                self.orderUpdated()
                
                DispatchQueue.main.async {
                    self.isEquating = false
                    completion(true)
                }
            }
        }
    }
    
    func generateDynamicModelsForAllOperation(completion:@escaping (Bool) -> ()) {
        
        if self.isEquating {
            return
        }
        
        if !self.isEquating {
            
            self.isEquating = true
            
            generateQue.async {
                
                self.productOrderSaledToCustomer()
                self.productOrderSaleEditedToCustomer()
                
                self.productOrderReceivedFromSupplier()
                self.productOrderReceiveEditedFromSupplier()
                
                self.productReturnedFromCustomer()
                self.productReturnedToSupplier()
                
                self.moneyReceived()
                self.moneyGone()
                
                DispatchQueue.main.async {
                    self.isEquating = false
                    completion(true)
                }
            }
        }
    }
    
    // ------------------------------------------------------------------------------------------------ //
    // MARK: Create static models
    private func createCategories() -> Void {
        
        let key = Database.shared.uniqueIncreasingPrimaryKey()
        for index: UInt64 in 1...30 {

            print("generating category = \(index)")
            
            let creatingCategory: Category = Category()
            creatingCategory.categoryId = key + index
            creatingCategory.categoryName = "category name \(index)"
            Database.shared.createCategories([creatingCategory])
            
            
            let cid = Database.shared.operationCid()
            let operation = Event(syncOperationId: cid,
                                          isSynchronized: false,
                                          syncOperationDate: Date()/*.dateBySubtractingDays(300).dateByAddingMinutes(Int(cid))*/,
                                          operationType: .categoryCreated,
                                          productId: 0,
                                          orderDetailId: 0,
                                          customerId: 0,
                                          transactionId: 0,
                                          accountId: 0,
                                          supplierId: 0,
                                          staffId: 0,
                                          categoryId: creatingCategory.categoryId,
                                          orderId: 0)
            Database.shared.createOperations([operation])
        }
    }
    
    private func createProducts() -> Void {
        
        let key = Database.shared.uniqueIncreasingPrimaryKey()
        for index: UInt64 in 1...600 {

            usleep(useconds_t(500000*2.1))
            print("generating product = \(index)")
            
            let creatingProduct = Product()
            creatingProduct.productId = key + index
            creatingProduct.productName = self.productNames[Int(index)]
            creatingProduct.productImagePath =  "image_path_\(Date().timeIntervalSince1970)" 
            creatingProduct.saleUnitPrice = Double(index)
            creatingProduct.quantityPerUnit = "шт"
            creatingProduct.barcode = "\(Date().timeIntervalSince1970)"
            creatingProduct.unitsInStock = Double(index * 10)
            creatingProduct.incomeUnitPrice = Double(index)
            creatingProduct.supplierId = index % 30
            creatingProduct.categoryId = index % 30
            Database.shared.createProducts(products: [creatingProduct])
            
            let orderDetail = OrderDetail()
            orderDetail.orderDetailId = key + index
            orderDetail.isLast = true
            orderDetail.productId = creatingProduct.productId
            orderDetail.orderQuantity = Double(index * 10)
            orderDetail.price = Double(index)
            orderDetail.productQuantity = Double(index * 3)
            orderDetail.orderId = 0
            orderDetail.billingNo = "\(Date().toString(.custom("EEE, dd MMM HH:mm"))) "
            orderDetail.orderDetailDate = Date()
            
            Database.shared.createorderDetails([orderDetail])
            
            let cid = Database.shared.operationCid()
            let operation = Event(syncOperationId: cid,
                                          isSynchronized: false,
                                          syncOperationDate: Date()/*.dateBySubtractingDays(300).dateByAddingMinutes(Int(cid))*/,
                                          operationType: .productCreated,
                                          productId: creatingProduct.productId,
                                          orderDetailId: orderDetail.orderDetailId,
                                          customerId: 0,
                                          transactionId: 0,
                                          accountId: 0,
                                          supplierId: 0,
                                          staffId: 0,
                                          categoryId: 0,
                                          orderId: 0)
            
            Database.shared.createOperations([operation])
        }
    }
    
    private func createCustomers() -> Void {
        let key = Database.shared.uniqueIncreasingPrimaryKey()
        for index: UInt64 in 1...174 {
            
            usleep(useconds_t(500000*2.1))
            print("generating customer = \(index)")
            
            let creatingCustomer = Customer()
            creatingCustomer.customerId = key + index
            
            let (firstName, secondName) = self.customerNames[Int(index)]
            
            creatingCustomer.firstName = firstName
            creatingCustomer.secondName = secondName
            creatingCustomer.phoneNumber = "555-45-65-\(index)"
            creatingCustomer.address = "Bishkek \(firstName) \(secondName)"
            creatingCustomer.userId = 2
            
            let account = Account()
            account.accountId = key + index
            account.customerId = creatingCustomer.customerId
            account.supplierId = 0
            account.balance = 0.0
            Database.shared.createAccounts([account])
            
            Database.shared.createCustomers([creatingCustomer])
            
            if let employee_ = AppSession.shared.currentEmployee {
                
                let transactionId = key + index
                
                let transaction = Transaction(transactionId: transactionId,
                            transactionDate: Date(),
                            isLastTransaction: true,
                            transactionType: .customerBalanceModified,
                            moneyAmount: 0.0,
                            orderId: 0,
                            customerId: creatingCustomer.customerId,
                            supplierId: 0,
                            userId: employee_.userId,
                            ballanceAmount: account.balance,
                            comment: "",
                            transactionUuid: UUID().uuidString.lowercased())
                
                Database.shared.createTransactions([transaction])
                let cid = Database.shared.operationCid()
                let operation = Event(syncOperationId: cid,
                                              isSynchronized: false,
                                              syncOperationDate: Date()/*.dateBySubtractingDays(300).dateByAddingMinutes(Int(cid))*/,
                                              operationType: .customerCreated,
                                              productId: 0,
                                              orderDetailId: 0,
                                              customerId: creatingCustomer.customerId,
                                              transactionId: transaction.transactionId,
                                              accountId: account.accountId,
                                              supplierId: 0,
                                              staffId: 0,
                                              categoryId: 0,
                                              orderId: 0)
                
                Database.shared.createOperations([operation])
            }
        }
    }
    
    private func createSuppliers() -> Void {
        let key = Database.shared.uniqueIncreasingPrimaryKey()
        for index: UInt64 in 1...200 {
            
            usleep(useconds_t(500000*2.1))
            print("generating supplier = \(index)")
            
            let creatingSupplier =  Supplier()
            creatingSupplier.supplierId = key + index
            creatingSupplier.companyName = self.supplierNames[Int(index)]
            creatingSupplier.phoneNumber = "775-15-05-\(index)"
            creatingSupplier.address = "adress \(index)"
            
            
            let account = Account()
            account.accountId = key + index
            account.supplierId = creatingSupplier.supplierId
            account.customerId = 0
            account.balance = 0
            Database.shared.createAccounts([account])
            
            Database.shared.createSuppliers([creatingSupplier])
            
            // ----------- Transaction test mode create for Supplier ------------------
            if let employee_ = AppSession.shared.currentEmployee {
                
                let transactionId = key + index
                let transaction = Transaction(transactionId: transactionId,
                                              transactionDate: Date(),
                                              isLastTransaction: true,
                                              transactionType: .supplierBalanceModified,
                                              moneyAmount: 0.0,
                                              orderId: 0,
                                              customerId: 0,
                                              supplierId: creatingSupplier.supplierId,
                                              userId: employee_.userId,
                                              ballanceAmount: account.balance,
                                              comment: "",
                                              transactionUuid: UUID().uuidString.lowercased())
                
                Database.shared.createTransactions([transaction])
                
                let cid = Database.shared.operationCid()
                let operation = Event(syncOperationId: cid,
                                              isSynchronized: false,
                                              syncOperationDate: Date()/*.dateBySubtractingDays(300).dateByAddingMinutes(Int(cid))*/,
                                              operationType: .supplierCreated,
                                              productId: 0,
                                              orderDetailId: 0,
                                              customerId: 0,
                                              transactionId: transaction.transactionId,
                                              accountId: account.accountId,
                                              supplierId: creatingSupplier.supplierId,
                                              staffId: 0,
                                              categoryId: 0,
                                              orderId: 0)
                
                Database.shared.createOperations([operation])
            }
        }
    }
    
    private func createStaff() -> Void {
        let key = Database.shared.uniqueIncreasingPrimaryKey()
        
        for index: UInt64 in 1...14 {
            
            usleep(useconds_t(500000*2.1))
            print("generating staff = \(index)")
            
            let creatingStaff: User = User()
            creatingStaff.userId = key + index
            let (firstName, secondName) = self.staffNames[Int(index)]
            creatingStaff.firstName = firstName
            creatingStaff.secondName = secondName
            creatingStaff.phoneNumber = "705-15-05-\(index)"
            creatingStaff.address = "Bishkek, Vostok 5 \(key + index)"
            creatingStaff.roleId = 3000
            creatingStaff.email = "\(key + index)@\(key + index)mail.ru"
            creatingStaff.password = "12345"
            
            Database.shared.createUser([creatingStaff])
            let cid = Database.shared.operationCid()
            let operation = Event(syncOperationId: cid,
                                          isSynchronized: false,
                                          syncOperationDate: Date()/*.dateBySubtractingDays(300).dateByAddingMinutes(Int(cid))*/,
                                          operationType: .staffCreated,
                                          productId: 0,
                                          orderDetailId: 0,
                                          customerId: 0,
                                          transactionId: 0,
                                          accountId: 0,
                                          supplierId: 0,
                                          staffId: creatingStaff.userId,
                                          categoryId: 0,
                                          orderId: 0)
            
            Database.shared.createOperations([operation])
        }
    }
   
    // ------------------------------------------------------------------------------------------------ //
    //MARK: Update static models
    private func updateCategories() -> Void {
        
        let categories = Database.shared.loadAllCategories()
        for category in categories {
            
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                          isSynchronized: false,
                                          syncOperationDate: Date(),
                                          operationType: .categoryUpdated,
                                          productId: 0,
                                          orderDetailId: 0,
                                          customerId: 0,
                                          transactionId: 0,
                                          accountId: 0,
                                          supplierId: 0,
                                          staffId: 0,
                                          categoryId: category.categoryId,
                                          orderId: 0)
            Database.shared.createOperations([operation])
        }
    }
    
    private func updateProducts() -> Void {
        
        let products = Database.shared.loadAllProducts(10)
        
        let key = Database.shared.uniqueIncreasingPrimaryKey()
        for (index, product) in products.enumerated() {
            
            let ms = 500000*2.1
            usleep(useconds_t(ms))
            
            let orderDetail = OrderDetail()
            orderDetail.orderDetailId = key + UInt64(index)
            orderDetail.orderDetailDate = Date()
            orderDetail.isLast = false
            orderDetail.productId = product.productId
            orderDetail.orderQuantity = product.unitsInStock
            orderDetail.price = product.saleUnitPrice
            orderDetail.productQuantity = product.unitsInStock
            orderDetail.orderId = 0
            orderDetail.billingNo = "\(Date().toString(.custom("EEE, dd MMM HH:mm"))) "
            Database.shared.createorderDetails([orderDetail])
            
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                          isSynchronized: false,
                                          syncOperationDate: Date(),
                                          operationType: .productUpdated,
                                          productId: product.productId,
                                          orderDetailId: orderDetail.orderDetailId,
                                          customerId: 0,
                                          transactionId: 0,
                                          accountId: 0,
                                          supplierId: 0,
                                          staffId: 0,
                                          categoryId: 0,
                                          orderId: 0)
            
            Database.shared.createOperations([operation])
        }
    }
    
    private func updateCustomers() -> Void {
        
        let customers = Database.shared.loadAllCustomers(10)
        for customer in customers {
            
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                          isSynchronized: false,
                                          syncOperationDate: Date(),
                                          operationType: .customerUpdated,
                                          productId: 0,
                                          orderDetailId: 0,
                                          customerId: customer.customerId,
                                          transactionId: 0,
                                          accountId: 0,
                                          supplierId: 0,
                                          staffId: 0,
                                          categoryId: 0,
                                          orderId: 0)
            
            Database.shared.createOperations([operation])
        }
    }
    
    private func updateCustomersBallance() -> Void {
        
        let customers = Database.shared.loadAllCustomers(5)
        for customer in customers {
            if let account_ = Database.shared.accountBy(customerId: customer.customerId),
               let employee_ = AppSession.shared.currentEmployee {
                
                let ms = 500000*2.1
                usleep(useconds_t(ms))
                
                let transactionId = Database.shared.uniqueIncreasingPrimaryKey()
                let transaction = Transaction(transactionId: transactionId,
                                              transactionDate: Date(),
                                              isLastTransaction: false,
                                              transactionType: .customerBalanceModified,
                                              moneyAmount: account_.balance,
                                              orderId: 0,
                                              customerId: customer.customerId,
                                              supplierId: 0,
                                              userId: employee_.userId,
                                              ballanceAmount: account_.balance,
                                              comment: "",
                                              transactionUuid: UUID().uuidString.lowercased())
                
                Database.shared.createTransactions([transaction])
            
                let operation = Event(syncOperationId: Database.shared.operationCid(),
                                              isSynchronized: false,
                                              syncOperationDate: Date(),
                                              operationType: .customerBalanceUpdated,
                                              productId: 0,
                                              orderDetailId: 0,
                                              customerId: customer.customerId,
                                              transactionId: transaction.transactionId,
                                              accountId: account_.accountId,
                                              supplierId: 0,
                                              staffId: 0,
                                              categoryId: 0,
                                              orderId: 0)
                
                Database.shared.createOperations([operation])
            }
        }
    }
    
    private func updateSuppliers() -> Void {
        
        let suppliers = Database.shared.loadAllSuppliers(10)
        for supplier in suppliers {
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                          isSynchronized: false,
                                          syncOperationDate: Date(),
                                          operationType: .supplierUpdated,
                                          productId: 0,
                                          orderDetailId: 0,
                                          customerId: 0,
                                          transactionId: 0,
                                          accountId: 0,
                                          supplierId: supplier.supplierId,
                                          staffId: 0,
                                          categoryId: 0,
                                          orderId: 0)
            
            Database.shared.createOperations([operation])
        }
    }
    
    private func updateSuppliersBallance() -> Void {
        let suppliers = Database.shared.loadAllSuppliers(5)
        for supplier in suppliers {
            
            if let account_ = Database.shared.accountBy(supplierId: supplier.supplierId),
                let employee_ = AppSession.shared.currentEmployee {
                
                let ms = 500000*2.1
                usleep(useconds_t(ms))
                
                let transactionId = Database.shared.uniqueIncreasingPrimaryKey()
                let transaction = Transaction(transactionId: transactionId,
                                              transactionDate: Date(),
                                              isLastTransaction: false,
                                              transactionType: .supplierBalanceModified,
                                              moneyAmount: account_.balance,
                                              orderId: 0,
                                              customerId: 0,
                                              supplierId: supplier.supplierId,
                                              userId: employee_.userId,
                                              ballanceAmount: account_.balance,
                                              comment: "",
                                              transactionUuid: UUID().uuidString.lowercased())
                
                Database.shared.createTransactions([transaction])
                
                let operation = Event(syncOperationId: Database.shared.operationCid(),
                                              isSynchronized: false,
                                              syncOperationDate: Date(),
                                              operationType: .supplierBalanceUpdated,
                                              productId: 0,
                                              orderDetailId: 0,
                                              customerId: 0,
                                              transactionId: transaction.transactionId,
                                              accountId: account_.accountId,
                                              supplierId: supplier.supplierId,
                                              staffId: 0,
                                              categoryId: 0,
                                              orderId: 0)
                
                Database.shared.createOperations([operation])
            }   
        }
    }
    
    private func updateStaff() -> Void {
        
        let staff = Database.shared.loadAllUsers()
        for employee in staff {
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                          isSynchronized: false,
                                          syncOperationDate: Date(),
                                          operationType: .staffUpdated,
                                          productId: 0,
                                          orderDetailId: 0,
                                          customerId: 0,
                                          transactionId: 0,
                                          accountId: 0,
                                          supplierId: 0,
                                          staffId: employee.userId,
                                          categoryId: 0,
                                          orderId: 0)
            
            Database.shared.createOperations([operation])
        }
    }
    
    
    //MARK: Create order operations
    private func productOrderSaledToCustomer() -> Void {
        
        //productOrderSaledToCustomer
        
        // need first 10 products
        // need 10 orderDetails
        // total price
        // total discount
        // need first 10 customers
        
        let customers = Database.shared.loadAllCustomers(3)
        let firstTenProducts = Database.shared.loadAllProducts(35)
        if let employee = AppSession.shared.currentEmployee {
            
            for globalIndex: UInt64 in 1...10 {
                for (localIndex, customer) in customers.enumerated() {
                
                    print("Creating sale order \(globalIndex) + \(localIndex)")
                    
                    //let ms = 1000 * 2 // for 2 milliseconds (.002 seconds)
                    //let ms = 1000000
                    let ms = 500000*2.1
                    usleep(useconds_t(ms)) //so usleep(1000000) will sleep for 1 sec
                    
                    let uniqKey = Database.shared.uniqueIncreasingPrimaryKey()
                    
                    var orderDetails: [OrderDetail] = []
                    for (index, product) in firstTenProducts.enumerated() {
                        let orderDetail = OrderDetail()
                        orderDetail.orderDetailId = uniqKey + UInt64(index) + globalIndex + UInt64(localIndex)
                        orderDetail.productId = product.productId
                        orderDetail.orderQuantity = Double(1+index)
                        orderDetail.price = product.saleUnitPrice
                        orderDetail.productQuantity = 0
                        orderDetail.orderDetailDate = Date()
                        orderDetails.append(orderDetail)
                    }
                    
                    var totalPrice_: Double = 0.0
                    for orderDetail in orderDetails {
                        let totalPriceWithDiscount = orderDetail.price * (1) * orderDetail.orderQuantity
                        totalPrice_ = totalPrice_ + totalPriceWithDiscount
                    }
                    
                    let customerId = customer.customerId
                    let _ = OrderManager.shared.ownerSaledProductsToCustomer(orderDetails: orderDetails,
                                                                                           staff: employee,
                                                                                           customerId: customerId,
                                                                                           orderDocType: .saleToCustomer,
                                                                                           totalPrice: totalPrice_,
                                                                                           discount: Discount())
                    
                }
            }
        }
    }
    
    private func productOrderSaleEditedToCustomer() -> Void {
    }
    
    private func productOrderReceivedFromSupplier() -> Void {
        let suppliers = Database.shared.loadAllSuppliers(3)
        let firstTenProducts = Database.shared.loadAllProducts(30)
        if let employee = AppSession.shared.currentEmployee {
            for globalIndex: UInt64 in 1...10 {
                for (localIndex, supplier) in suppliers.enumerated() {
                    
                    print("Creating supplier income order \(globalIndex) + \(localIndex)")
                    
                    //let ms = 1000 * 2 // for 2 milliseconds (.002 seconds)
                    //let ms = 1000000
                    let ms = 500000*2.1
                    usleep(useconds_t(ms)) //so usleep(1000000) will sleep for 1 sec
                    
                    let uniqKey = Database.shared.uniqueIncreasingPrimaryKey()
                    
                    var orderDetails: [OrderDetail] = []
                    for (index, product) in firstTenProducts.enumerated() {
                        let orderDetail = OrderDetail()
                        orderDetail.orderDetailId = uniqKey + UInt64(index) + globalIndex + UInt64(localIndex)
                        orderDetail.productId = product.productId
                        orderDetail.orderQuantity = Double(1+index)
                        orderDetail.price = product.saleUnitPrice
                        orderDetail.productQuantity = product.unitsInStock
                        orderDetail.orderDetailDate = Date()
                        orderDetails.append(orderDetail)
                    }
                    
                    var totalPrice_: Double = 0.0
                    for orderDetail in orderDetails {
                        let totalPriceWithDiscount = orderDetail.price * (1) * orderDetail.orderQuantity
                        totalPrice_ = totalPrice_ + totalPriceWithDiscount
                    }
                    
                    let supplierId = supplier.supplierId
                    OrderManager.shared.ownerReceivedProductsFromSupplier(orderDetails: orderDetails,
                                                                                        staff: employee,
                                                                                        supplierId: supplierId,
                                                                                        orderDocType: .receiveFromSupplier,
                                                                                        totalPrice: totalPrice_,
                                                                                        discount: Discount())
                }
            }
        }
    }
    
    private func productOrderReceiveEditedFromSupplier() -> Void {
    }
    
    private func productReturnedFromCustomer() -> Void {
        let customers = Database.shared.loadAllCustomers(3)
        let firstTenProducts = Database.shared.loadAllProducts(15)
        if let employee = AppSession.shared.currentEmployee {
            
            for globalIndex: UInt64 in 1...5 {
                for (localIndex, customer) in customers.enumerated() {
                    
                    print("Creating customer return order \(globalIndex) + \(localIndex)")
                    
                    //let ms = 1000 * 2 // for 2 milliseconds (.002 seconds)
                    //let ms = 1000000
                    let ms = 500000*2.1
                    usleep(useconds_t(ms)) //so usleep(1000000) will sleep for 1 sec
                    
                    let uniqKey = Database.shared.uniqueIncreasingPrimaryKey()
                    
                    var orderDetails: [OrderDetail] = []
                    for (index, product) in firstTenProducts.enumerated() {
                        let orderDetail = OrderDetail()
                        orderDetail.orderDetailId = uniqKey + UInt64(index) + globalIndex + UInt64(localIndex)
                        orderDetail.productId = product.productId
                        orderDetail.orderQuantity = Double(1+index)
                        orderDetail.price = product.saleUnitPrice
                        orderDetail.productQuantity = product.unitsInStock
                        orderDetail.orderDetailDate = Date()
                        orderDetails.append(orderDetail)
                    }
                    
                    var totalPrice_: Double = 0.0
                    for orderDetail in orderDetails {
                        let totalPriceWithDiscount = orderDetail.price * (1) * orderDetail.orderQuantity
                        totalPrice_ = totalPrice_ + totalPriceWithDiscount
                    }
                    
                    let customerId = customer.customerId
                    OrderManager.shared.ownerReceivedRetrunProductsFromCustomer(orderDetails: orderDetails,
                                                                                              staff: employee,
                                                                                              customerId: customerId,
                                                                                              orderDocType: .returnFromCustomer,
                                                                                              totalPrice: totalPrice_,
                                                                                              discount: Discount())

                    
                }
            }
        }
    }
    
    private func productReturnedToSupplier() -> Void {
        let suppliers = Database.shared.loadAllSuppliers(2)
        let firstTenProducts = Database.shared.loadAllProducts(11)
        if let employee = AppSession.shared.currentEmployee {
            for globalIndex: UInt64 in 1...5 {
                for (localIndex, supplier) in suppliers.enumerated() {
                    
                    print("Creating supplier return order \(globalIndex) + \(localIndex)")
                    
                    //let ms = 1000 * 2 // for 2 milliseconds (.002 seconds)
                    //let ms = 1000000
                    let ms = 500000*2.1
                    usleep(useconds_t(ms)) //so usleep(1000000) will sleep for 1 sec
                    
                    let uniqKey = Database.shared.uniqueIncreasingPrimaryKey()
                    
                    var orderDetails: [OrderDetail] = []
                    for (index, product) in firstTenProducts.enumerated() {
                        let orderDetail = OrderDetail()
                        orderDetail.orderDetailId = uniqKey + UInt64(index) + globalIndex + UInt64(localIndex)
                        orderDetail.productId = product.productId
                        orderDetail.orderQuantity = Double(1+index)
                        orderDetail.price = product.saleUnitPrice
                        orderDetail.productQuantity = product.unitsInStock
                        orderDetail.orderDetailDate = Date()
                        orderDetails.append(orderDetail)
                    }
                    
                    var totalPrice_: Double = 0.0
                    for orderDetail in orderDetails {
                        let totalPriceWithDiscount = orderDetail.price * (1) * orderDetail.orderQuantity
                        totalPrice_ = totalPrice_ + totalPriceWithDiscount
                    }
                    
                    let supplierId = supplier.supplierId
                    OrderManager.shared.ownerReturnedProductsToSupplier(orderDetails: orderDetails,
                                                                                      staff: employee,
                                                                                      supplierId: supplierId,
                                                                                      orderDocType: .returnToSupplier,
                                                                                      totalPrice: totalPrice_,
                                                                                      discount: Discount())

                }
            }
        }
    }
    
    private func orderUpdated() -> Void {
        Database.shared.loadOrders(orderDocumentType: .none,
                                   date: Date(),
                                   limit: 10) { (orders: [Order]) in
                                    
                                    generateQue.async {
                                        for order in orders {
                                            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                                                          isSynchronized: false,
                                                                          syncOperationDate: Date(),
                                                                          operationType: .orderUpdated,
                                                                          productId: 0,
                                                                          orderDetailId: 0,
                                                                          customerId: 0,
                                                                          transactionId: 0,
                                                                          accountId: 0,
                                                                          supplierId: 0,
                                                                          staffId: 0,
                                                                          categoryId: 0,
                                                                          orderId: order.orderId)
                                            Database.shared.createOperations([operation])
                                        }
                                    }
        }
    }
    
    private func moneyReceived() -> Void {
        
        let customers = Database.shared.loadAllCustomers(3)
        let suppliers = Database.shared.loadAllSuppliers(4)
        
        if let employee = AppSession.shared.currentEmployee {
            for globalIndex: UInt64 in 1...4 {
                for (localIndex, supplier) in suppliers.enumerated() {
                    
                    print("Creating supplier money received \(globalIndex) + \(localIndex)")
                    
                    let ms = 500000*2.1
                    usleep(useconds_t(ms))
                    
                    let supplierId = supplier.supplierId
                    OrderManager.shared.ownerReceivedRetrunMoneyFromSupplier(supplierId: supplierId,
                                                                                           staffId: employee.userId,
                                                                                           receivedReturnMoney: 1000,
                                                                                           comment: OrderMemomry.shared.comments)
                    
                    
                    let ms2 = 500000*2.1
                    usleep(useconds_t(ms2))
                    // --------------------------- Order --------------------------- //
                    let order = Order()
                    order.orderId = Database.shared.uniqueIncreasingPrimaryKey()
                    
                    order.orderDocument = .moneyReceive
                    order.moneyMovementType = .otherMoneyReceive
                    
                    order.customerId = 0
                    order.supplierId = 0
                    order.comment = "wefwfsd sd fds fds fsd f sdf dsf dsf"
                    
                    let currentDate = Date()
                    order.orderDate = currentDate
                    order.billingNo = "N==> \(order.orderId) from \(currentDate.toString(.rss))"
                    order.userId = employee.userId
                    
                    // --------------------------- Payment --------------------------- //
                    let payment = Payment()
                    payment.paymentId = Database.shared.uniqueIncreasingPrimaryKey()
                    payment.discount = 0.0
                    payment.totalOrderPrice = 10000
                    payment.totalPriceWithDiscount = 10000
                    
                    order.paymentId = payment.paymentId
                    
                    Database.shared.createOrders([order])
                    Database.shared.createPayments([payment])
                    
                    let operation = Event(syncOperationId: Database.shared.operationCid(),
                                                  isSynchronized: false,
                                                  syncOperationDate: Date(),
                                                  operationType: .orderCreated,
                                                  productId: 0,
                                                  orderDetailId: 0,
                                                  customerId: 0,
                                                  transactionId: 0,
                                                  accountId: 0,
                                                  supplierId: 0,
                                                  staffId: employee.userId,
                                                  categoryId: 0,
                                                  orderId: order.orderId)
                    Database.shared.createOperations([operation])
                    
                    print("default")
                }
            }
        }
        
        if let employee = AppSession.shared.currentEmployee {
            
            for globalIndex: UInt64 in 1...4 {
                for (localIndex, customer) in customers.enumerated() {
                    
                    print("Creating customer money received \(globalIndex) + \(localIndex)")
                    
                    let ms = 500000*2.1
                    usleep(useconds_t(ms))
                    
                    let customerId = customer.customerId
                    OrderManager.shared.ownerReceivedMoneyFromCustomer(customerId: customerId,
                                                                                     staffId: employee.userId,
                                                                                     receivedMoney: 1000,
                                                                                     isMoneyForDebt: true,
                                                                                     comment: OrderMemomry.shared.comments)
                    
                    let ms2 = 500000*2.1
                    usleep(useconds_t(ms2))
                    
                    let order = Order()
                    order.orderId = Database.shared.uniqueIncreasingPrimaryKey()
                    
                    order.orderDocument = .moneyGone
                    order.moneyMovementType = .moneyGoneToAnother
                    
                    order.comment = "sdfsfsdfdsfs"
                    order.customerId = 0
                    order.supplierId = 0
                    
                    let currentDate = Date()
                    order.orderDate = currentDate
                    order.billingNo = "N==> \(order.orderId) from \(currentDate.toString(.rss))"
                    order.userId = employee.userId
                    
                    // --- Payment --- //
                    let payment = Payment()
                    payment.paymentId = Database.shared.uniqueIncreasingPrimaryKey()
                    payment.discount = 0.0
                    payment.totalOrderPrice = 50000
                    payment.totalPriceWithDiscount = 50000
                    
                    order.paymentId = payment.paymentId
                    
                    Database.shared.createOrders([order])
                    Database.shared.createPayments([payment])
                    
                    let operation = Event(syncOperationId: Database.shared.operationCid(),
                                                  isSynchronized: false,
                                                  syncOperationDate: Date(),
                                                  operationType: .orderCreated,
                                                  productId: 0,
                                                  orderDetailId: 0,
                                                  customerId: 0,
                                                  transactionId: 0,
                                                  accountId: 0,
                                                  supplierId: 0,
                                                  staffId: employee.userId,
                                                  categoryId: 0,
                                                  orderId: order.orderId)
                    Database.shared.createOperations([operation])
                }
            }
        }
    }
    
    private func moneyGone() -> Void {
        
        let customers = Database.shared.loadAllCustomers(2)
        let suppliers = Database.shared.loadAllSuppliers(2)
        
        if let employee = AppSession.shared.currentEmployee {
            for globalIndex: UInt64 in 1...4 {
                for (localIndex, supplier) in suppliers.enumerated() {
                    
                    print("Creating supplier money gone \(globalIndex) + \(localIndex)")
                    
                    let ms = 500000*2.1
                    usleep(useconds_t(ms))
                    
                    let supplierId = supplier.supplierId
                    OrderManager.shared.ownerGivedMoneyToSupplierForProduct(supplierId: supplierId,
                                                                                          staffId: employee.userId,
                                                                                          givedMoney: 1000, comment: OrderMemomry.shared.comments)
                }
            }
        }
        
        if let employee = AppSession.shared.currentEmployee {
            
            for globalIndex: UInt64 in 1...4 {
                for (localIndex, customer) in customers.enumerated() {
                    
                    print("Creating supplier money gone \(globalIndex) + \(localIndex)")
                    
                    let ms = 500000*2.1
                    usleep(useconds_t(ms))
                    
                    let customerId = customer.customerId
                    OrderManager.shared.ownerReturnedMoneyToCustomer(customerId: customerId,
                                                                                   staffId: employee.userId,
                                                                                   returnedMoney: 1000, comment: OrderMemomry.shared.comments)
                }
            }
        }
    }
    
}




