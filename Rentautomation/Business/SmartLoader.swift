//
//  SmartLoader.swift
//  Rentautomation
//
//  Created by kanybek on 2/8/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit

final class SmartLoader {
    
    static let shared: SmartLoader = SmartLoader()
    var comonDisposable: Disposable?
    
    init () {
    }
    
    func stopAllGetters() -> Void {
        self.comonDisposable?.dispose()
        self.comonDisposable = nil
    }
    
    // MARK:ORDERS
    // All orders, also available order types
    func loadOrders(orderDocumentType: OrderDocumentType, date: Date, limit: UInt16, completion: @escaping (_ result: [Order]) -> ()) {
        
        self.stopAllGetters()
        
        // this case is correct for OrderDocumentType == .none
        let datebaseGetOrdersS = self.getOrdersFromDatabase(orderDocumentType: orderDocumentType, date: date, limit: limit)
        
        let finalSignal = datebaseGetOrdersS |> mapToSignal({ (orders_: [Order]) -> Signal<[Order], Error> in
            
            if orders_.count > 0 {
                return Signal.single(orders_)
            } else {
                
                let rpcSignal = Synchronizer.shared.getOrdersForFilter(date: date,
                                                                                     limit: limit,
                                                                                     isForInitialState: true)
                
                let databaseSignal = rpcSignal |> mapToSignal({ (boolValue: Bool) -> Signal<[Order], Error> in
                    return self.getOrdersFromDatabase(orderDocumentType: orderDocumentType, date: date, limit: limit)
                })
                return databaseSignal
            }
        })
        
        self.comonDisposable = finalSignal.start(next: { (orders__: [Order]) in
            DispatchQueue.main.async {
                completion(orders__)
            }
        }, error: { (Void) in
            DispatchQueue.main.async {
                completion([])
            }
        }, completed: { (Void) in
            print("loadOrders completed")
        })
    }
    
    private func getOrdersFromDatabase(orderDocumentType: OrderDocumentType, date: Date, limit: UInt16) -> Signal<[Order], Error> {
        return Signal<[Order], Error> { subscriber in
            Database.shared.loadOrders(orderDocumentType: orderDocumentType, date: date, limit: limit) { (result_: [Order]) in
                subscriber.putNext(result_)
                subscriber.putCompletion()
            }
            return ActionDisposable {
            }
        }
    }
    
    // MARK:TRANSACTIONS
    // All transactions, customer, supplier pages
    func loadAllTransactions(customerId: UInt64, supplierId: UInt64, date: Date, limit: UInt16, completion: @escaping (_ result: [Transaction]) -> ()) {
        
        self.stopAllGetters()
        
        let transactionsFromDBSignal = self.getTransactionsFromDatabase(customerId: customerId,
                                                                        supplierId: supplierId,
                                                                        date: date,
                                                                        limit: limit)
        
        let finalSignal = transactionsFromDBSignal |> mapToSignal({ (transactions_: [Transaction]) -> Signal<[Transaction], Error> in
            
            if transactions_.count > 0 {
                return Signal.single(transactions_)
            } else {
                
                let rpcTrsignal = Synchronizer.shared.getTransactionsForFilter(customerId: customerId,
                                                                                             supplierId: supplierId,
                                                                                             date: date,
                                                                                             limit: limit)
                
                let databaseSignal = rpcTrsignal |> mapToSignal({ (boolValue: Bool) -> Signal<[Transaction], Error> in
                    return self.getTransactionsFromDatabase(customerId: customerId,
                                                            supplierId: supplierId,
                                                            date: date,
                                                            limit: limit)
                })
                return databaseSignal
            }
        })
        
        self.comonDisposable = finalSignal.start(next: { (transactions__: [Transaction]) in
            DispatchQueue.main.async {
                completion(transactions__)
            }
        }, error: { (Void) in
            DispatchQueue.main.async {
                completion([])
            }
        }, completed: { (Void) in
            print("loadAllTransactions completed")
        })
    }
    
    private func getTransactionsFromDatabase(customerId: UInt64, supplierId: UInt64, date: Date, limit: UInt16) -> Signal<[Transaction], Error> {
        return Signal<[Transaction], Error> { subscriber in
            Database.shared.loadAllTransactions(customerId: customerId, supplierId: supplierId, date: date, limit: limit) { (results_: [Transaction]) in
                subscriber.putNext(results_)
                subscriber.putCompletion()
            }
            return ActionDisposable {
            }
        }
    }
    
    // MARK:ORDER_DETAILS
    // Only for product page
    func loadAllOrderDetailsFor(productId: UInt64, date: Date, limit: UInt16, completion: @escaping (_ result: [OrderDetail]) -> ()) {
        
        self.stopAllGetters()
        
        let databaseOrderDetSign = self.getOrderDetailsFromDatabase(productId: productId,
                                                                    date: date,
                                                                    limit: limit)
        
        let finalSignal = databaseOrderDetSign |> mapToSignal({ (orderDetails_: [OrderDetail]) -> Signal<[OrderDetail], Error> in
            
            if orderDetails_.count > 0 {
                return Signal.single(orderDetails_)
            } else {
                
                let rpcOrdDetsignal = Synchronizer.shared.getOrderDetailsForFilter(productId: productId,
                                                                                                 date: date,
                                                                                                 limit: limit)
                
                let databaseSignal = rpcOrdDetsignal |> mapToSignal({ (boolValue: Bool) -> Signal<[OrderDetail], Error> in
                    return self.getOrderDetailsFromDatabase(productId: productId,
                                                            date: date,
                                                            limit: limit)
                })
                return databaseSignal
            }
        })
        
        self.comonDisposable = finalSignal.start(next: { (orderDetails__: [OrderDetail]) in
            DispatchQueue.main.async {
                completion(orderDetails__)
            }
        }, error: { (Void) in
            DispatchQueue.main.async {
                completion([])
            }
        }, completed: { (Void) in
            print("loadAllOrderDetailsFor completed")
        })
    }
    
    private func getOrderDetailsFromDatabase(productId: UInt64, date: Date, limit: UInt16) -> Signal<[OrderDetail], Error> {
        return Signal<[OrderDetail], Error> { subscriber in
            Database.shared.loadAllOrderDetailsFor(productId: productId, date: date, limit: limit) { (results_: [OrderDetail]) in
                subscriber.putNext(results_)
                subscriber.putCompletion()
            }
            return ActionDisposable {
            }
        }
    }
}




