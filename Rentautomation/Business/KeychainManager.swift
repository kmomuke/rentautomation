//
//  KeychainManager.swift
//  Rentautomation
//
//  Created by kanybek on 12/30/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import Locksmith

let USER_TOKEN = "com.seyetck.user.token"
let TOKEN_KEY = "user_token"

final class KeychainManager {
    
    static let shared: KeychainManager = KeychainManager()
    
    init () {
    }
    
    func isLoggedIn() -> Bool {
        if let _ = self.getToken() {
            return true
        }
        return false
    }
    
    func saveToken(_ token: String) -> Bool {
        do {
            try Locksmith.saveData(data: [TOKEN_KEY: token], forUserAccount: USER_TOKEN)
        }
        catch let error as NSError {
            print("saveToken error ==>:", error)
            return false
        }
        return true
    }
    
    
    lazy var lazyToken: String? = {
        
        if let token = self.getToken() {
            return token
        }
        
        return nil
    }()
    
    
    func getToken() -> String? {
        if let dictionary = Locksmith.loadDataForUserAccount(userAccount: USER_TOKEN) {
            if let token = dictionary[TOKEN_KEY] as? String {
                return token
            }
        }
        return nil
    }
    
    func removeToken() -> Void {
        do {
            try Locksmith.deleteDataForUserAccount(userAccount: USER_TOKEN)
        }
        catch let error as NSError {
            print("removeToken error ==>:", error)
        }
    }
    
//    //------------------------------------------------------------
//    func encrypt(unSecuredToken: String, key: String) -> String? {
//        print("Encrypt: unSecuredToken token == \(unSecuredToken)")
//        if let data: Data = unSecuredToken.data(using: .utf8) {
//            
//            let ciphertext = RNCryptor.encrypt(data: data, withPassword: key)
//            let cipherString = ciphertext.base64EncodedString()
//            
//            print("Encrypt: SecuredToken token == \(cipherString)")
//            
//            return cipherString
//        }
//        return nil
//    }
//    
//    func decrypt(securedToken: String, key: String) -> String? {
//        print("Decrypt: secured token == \(securedToken)")
//        if let data = Data(base64Encoded: securedToken) {
//            do {
//                let originalData = try RNCryptor.decrypt(data: data, withPassword: key)
//                if let originalString = String(data: originalData, encoding: .utf8) {
//                    print("Decrypt: unsecured token == \(originalString)")
//                    return originalString
//                }
//            } catch {
//                print(error)
//            }
//        }
//        return nil
//    }
}
