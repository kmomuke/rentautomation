//
//  TransactionManager.swift
//  Rentautomation
//
//  Created by kanybek on 12/15/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

//case productOrderSaledToCustomer = 1000 // customer sale
//case productOrderSaleEditedToCustomer = 2000 // "edit" customer sale
//
//case productOrderReceivedFromSupplier = 3000 // supplier income
//case productOrderReceiveEditedFromSupplier = 4000 // "edit" supplier income
//
//case productReturnedFromCustomer = 5000 // return to customer
//case productReturneEditedFromCustomer = 6000 // "edit" return to customer
//
//case productReturnedToSupplier = 5500 // return to supplier
//case productReturneEditedToSupplier = 6600 // "edit" return to supplier
//
//case moneyReceived = 7000 //  money receive
//case moneyReceiveEdited = 8000 // "edit" money receive
//
//case moneyGone = 10000 // money outgone
//case moneyGoneEdited = 11000 // "edit" money outgone

final class OrderManager {
    
    static let shared: OrderManager = OrderManager()
    init() {}
    
    //(1) orderDetails_ = self.orderDetails  //[OrderDetail]?
    //(2) let employee_ = AppSession.shared.currentEmployee //Staff
    //(3) OrderMemomry.shared.customerId // UInt64
    //(4) OrderMemomry.shared.supplierId // UInt64
    //(5) self.orderDocument // OrderDocumentType
    //(6) self.totalPrice // Double
    //(7) discount // Double
    
    // Owner sale products to customer (simple sale)
    func ownerSaledProductsToCustomer(orderDetails: [OrderDetail],
                                      staff: User,
                                      customerId: UInt64,
                                      orderDocType: OrderDocumentType,
                                      totalPrice: Double,
                                      discount: Discount) -> Order {
        // --- Order --- //
        let order = Order()
        order.orderId = Database.shared.uniqueIncreasingPrimaryKey()
        order.customerId = customerId
        order.orderDocument = orderDocType
        order.paymentId = 0
        
        let currentDate = Date()
        order.orderDate = currentDate
        order.billingNo = OrderMemomry.shared.billingNo
        order.comment = OrderMemomry.shared.comments
        order.userId = staff.userId
        order.isDeleted = false
        order.isMoneyForDebt = false
        order.isEdited = false
        
        // --- Payment --- //
        let payment = Payment()
        payment.paymentId = Database.shared.uniqueIncreasingPrimaryKey()
        discount.totalPrice = totalPrice
        payment.setUpWith(discount: discount)
        // -------------
        order.paymentId = payment.paymentId
        // -------------
        
        // --- OrderDetails --- ------- //
        var orderDetailId = Database.shared.uniqueIncreasingPrimaryKey()
        
        for orderDetail in orderDetails {
            
            orderDetail.orderDetailId = orderDetailId
            orderDetail.orderId = order.orderId
            orderDetail.billingNo = order.billingNo
            
            orderDetailId = orderDetailId + 1
        }
        
        Database.shared.createOrders([order])
        Database.shared.createPayments([payment])
        Database.shared.createorderDetails(orderDetails)
        
        // ---------- Transactions for Product ----------
        self.decreaseProductsInStock(orderDetails: orderDetails)
        
        // ---------- Transaction Customer ----------
        let transactionId = Database.shared.uniqueIncreasingPrimaryKey()
        let transaction = Transaction(transactionId: transactionId,
                                      transactionDate: currentDate,
                                      isLastTransaction: false,
                                      transactionType: .customerReceivedProduct,
                                      moneyAmount: discount.totalPriceWithDiscount(),
                                      orderId: order.orderId,
                                      customerId: customerId,
                                      supplierId: 0,
                                      userId: staff.userId,
                                      ballanceAmount: 0,
                                      comment: "",
                                      transactionUuid: UUID().uuidString.lowercased())
        
        Database.shared.createTransactions([transaction])
        
        
        // ---- Change account of Customer
        if let account_ = Database.shared.accountBy(customerId: customerId) {
            account_.balance = account_.balance + discount.totalPriceWithDiscount()
            Database.shared.updateCustomerBalanceFor(customerId: customerId,
                                                     balance: account_.balance)
            
            // ------ Sync OPERATION ----- //
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                          isSynchronized: false,
                                          syncOperationDate: Date(),
                                          operationType: .orderCreated,
                                          productId: 0,
                                          orderDetailId: 0,
                                          customerId: customerId,
                                          transactionId: transaction.transactionId,
                                          accountId: account_.accountId,
                                          supplierId: 0,
                                          staffId: staff.userId,
                                          categoryId: 0,
                                          orderId: order.orderId)
            Database.shared.createOperations([operation])
            // ----------- //
        }
        
        return order
    }
    
    // Owner received (product return) products from customer
    func ownerReceivedRetrunProductsFromCustomer(orderDetails: [OrderDetail],
                                                 staff: User,
                                                 customerId: UInt64,
                                                 orderDocType: OrderDocumentType,
                                                 totalPrice: Double,
                                                 discount: Discount) -> Void {
        
        // --- Order --- //
        let order = Order()
        order.orderId = Database.shared.uniqueIncreasingPrimaryKey()
        order.customerId = customerId
        order.orderDocument = orderDocType
        order.paymentId = 0
        
        let currentDate = Date()
        order.orderDate = currentDate
        order.billingNo = OrderMemomry.shared.billingNo
        order.comment = OrderMemomry.shared.comments
        order.userId = staff.userId
        order.isDeleted = false
        order.isMoneyForDebt = false
        order.isEdited = false
        
        // --- Payment --- //
        let payment = Payment()
        payment.paymentId = Database.shared.uniqueIncreasingPrimaryKey()
        discount.totalPrice = totalPrice
        payment.setUpWith(discount: discount)
        
        // -------------
        order.paymentId = payment.paymentId
        // -------------
        
        // --- OrderDetails --- ------- //
        var orderDetailId = Database.shared.uniqueIncreasingPrimaryKey()
        
        for orderDetail in orderDetails {
            
            orderDetail.orderDetailId = orderDetailId
            orderDetail.orderId = order.orderId
            orderDetail.billingNo = order.billingNo
            
            orderDetailId = orderDetailId + 1
        }
        
        Database.shared.createOrders([order])
        Database.shared.createPayments([payment])
        Database.shared.createorderDetails(orderDetails)
        
        
        // ---------- Transactions for Product ----------
        self.increaseProductsInStock(orderDetails: orderDetails)
        
        // ----- Transaction Customer -----
        let transactionId = Database.shared.uniqueIncreasingPrimaryKey()
        let transaction = Transaction(transactionId: transactionId,
                                      transactionDate: currentDate,
                                      isLastTransaction: false,
                                      transactionType: .customerReturnedProduct,
                                      moneyAmount: discount.totalPriceWithDiscount(),
                                      orderId: order.orderId,
                                      customerId: customerId,
                                      supplierId: 0,
                                      userId: staff.userId,
                                      ballanceAmount: 0,
                                      comment: "",
                                      transactionUuid: UUID().uuidString.lowercased())
        
        Database.shared.createTransactions([transaction])
        
        
        // ---- Change account of Customer
        if let account_ = Database.shared.accountBy(customerId: customerId) {
            account_.balance = account_.balance - discount.totalPriceWithDiscount()
            Database.shared.updateCustomerBalanceFor(customerId: customerId,
                                                     balance: account_.balance)
            
            // ------ Sync OPERATION ----- //
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                          isSynchronized: false,
                                          syncOperationDate: Date(),
                                          operationType: .orderCreated,
                                          productId: 0,
                                          orderDetailId: 0,
                                          customerId: customerId,
                                          transactionId: transaction.transactionId,
                                          accountId: account_.accountId,
                                          supplierId: 0,
                                          staffId: staff.userId,
                                          categoryId: 0,
                                          orderId: order.orderId)
            Database.shared.createOperations([operation])
            // ----------- //
            
        }
    }
    
    // Owner received products from Supplier (simple receive)
    func ownerReceivedProductsFromSupplier(orderDetails: [OrderDetail],
                                           staff: User,
                                           supplierId: UInt64,
                                           orderDocType: OrderDocumentType,
                                           totalPrice: Double,
                                           discount: Discount) -> Void {
        
        
        // --- Order --- //
        let order = Order()
        order.orderId = Database.shared.uniqueIncreasingPrimaryKey()
        order.customerId = 0
        order.supplierId = supplierId
        order.orderDocument = orderDocType
        order.paymentId = 0
        
        let currentDate = Date()
        order.orderDate = currentDate
        order.billingNo = OrderMemomry.shared.billingNo
        order.comment = OrderMemomry.shared.comments
        order.userId = staff.userId
        order.isDeleted = false
        order.isMoneyForDebt = false
        order.isEdited = false
        
        // --- Payment --- //
        let payment = Payment()
        payment.paymentId = Database.shared.uniqueIncreasingPrimaryKey()
        discount.totalPrice = totalPrice
        payment.setUpWith(discount: discount)
        
        // -------------
        order.paymentId = payment.paymentId
        // -------------
        
        // --- OrderDetails --- ------- //
        var orderDetailId = Database.shared.uniqueIncreasingPrimaryKey()
        
        for orderDetail in orderDetails {
            
            orderDetail.orderDetailId = orderDetailId
            orderDetail.orderId = order.orderId
            orderDetail.billingNo = order.billingNo
            
            orderDetailId = orderDetailId + 1
        }
        
        Database.shared.createOrders([order])
        Database.shared.createPayments([payment])
        Database.shared.createorderDetails(orderDetails)
        
        // ---------- Transactions for Product ----------
        self.increaseProductsInStock(orderDetails: orderDetails)
        
        // ----- Transaction Supplier -----
        let transactionId = Database.shared.uniqueIncreasingPrimaryKey()
        let transaction = Transaction(transactionId: transactionId,
                                      transactionDate: currentDate,
                                      isLastTransaction: false,
                                      transactionType: .supplierGaveProduct,
                                      moneyAmount: discount.totalPriceWithDiscount(),
                                      orderId: order.orderId,
                                      customerId: 0,
                                      supplierId: supplierId,
                                      userId: staff.userId,
                                      ballanceAmount: 0,
                                      comment: "",
                                      transactionUuid: UUID().uuidString.lowercased())
        
        Database.shared.createTransactions([transaction])
        
        
        // ---- Change account of Customer
        if let account_ = Database.shared.accountBy(supplierId: supplierId) {
            account_.balance = account_.balance - discount.totalPriceWithDiscount()
            Database.shared.updateSupplierBalanceFor(supplierId: supplierId,
                                                     balance: account_.balance)
            
            // ------ Sync OPERATION ----- //
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                          isSynchronized: false,
                                          syncOperationDate: Date(),
                                          operationType: .orderCreated,
                                          productId: 0,
                                          orderDetailId: 0,
                                          customerId: 0,
                                          transactionId: transaction.transactionId,
                                          accountId: account_.accountId,
                                          supplierId: supplierId,
                                          staffId: staff.userId,
                                          categoryId: 0,
                                          orderId: order.orderId)
            Database.shared.createOperations([operation])
            // ----------- //
        }
    }
    
    // Owner retrun (Product return) products to Customer
    func ownerReturnedProductsToSupplier(orderDetails: [OrderDetail],
                                         staff: User,
                                         supplierId: UInt64,
                                         orderDocType: OrderDocumentType,
                                         totalPrice: Double,
                                         discount: Discount) -> Void {
        
        // --- Order --- //
        let order = Order()
        order.orderId = Database.shared.uniqueIncreasingPrimaryKey()
        order.customerId = 0
        order.supplierId = supplierId
        order.orderDocument = orderDocType
        order.paymentId = 0
        
        let currentDate = Date()
        order.orderDate = currentDate
        let str = "№ \(order.orderId) от \(currentDate.toString(.custom("EEEE, dd MMMM yyyy HH:mm")))"
        order.billingNo = str
        order.comment = OrderMemomry.shared.comments
        order.userId = staff.userId
        order.isDeleted = false
        order.isMoneyForDebt = false
        order.isEdited = false
        
        // --- Payment --- //
        let payment = Payment()
        payment.paymentId = Database.shared.uniqueIncreasingPrimaryKey()
        discount.totalPrice = totalPrice
        payment.setUpWith(discount: discount)
        
        // -------------
        order.paymentId = payment.paymentId
        // -------------
        
        // --- OrderDetails --- ------- //
        var orderDetailId = Database.shared.uniqueIncreasingPrimaryKey()
        
        for orderDetail in orderDetails {
            
            orderDetail.orderDetailId = orderDetailId
            orderDetail.orderId = order.orderId
            orderDetail.billingNo = order.billingNo
            
            orderDetailId = orderDetailId + 1
        }
        
        Database.shared.createOrders([order])
        Database.shared.createPayments([payment])
        Database.shared.createorderDetails(orderDetails)
        
        // ---------- Transactions for Product ----------
        self.decreaseProductsInStock(orderDetails: orderDetails)
        
        // ----- Transaction Customer -----
        let transactionId = Database.shared.uniqueIncreasingPrimaryKey()
        let transaction = Transaction(transactionId: transactionId,
                                      transactionDate: currentDate,
                                      isLastTransaction: false,
                                      transactionType: .supplierGaveProduct,
                                      moneyAmount: discount.totalPriceWithDiscount(),
                                      orderId: order.orderId,
                                      customerId: 0,
                                      supplierId: supplierId,
                                      userId: staff.userId,
                                      ballanceAmount: 0,
                                      comment: "",
                                      transactionUuid: UUID().uuidString.lowercased())
        
        Database.shared.createTransactions([transaction])
        
        
        // ---- Change account of Customer
        if let account_ = Database.shared.accountBy(supplierId: supplierId) {
            account_.balance = account_.balance + discount.totalPriceWithDiscount()
            Database.shared.updateSupplierBalanceFor(supplierId: supplierId,
                                                     balance: account_.balance)
            
            // ------ Sync OPERATION ----- //
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                          isSynchronized: false,
                                          syncOperationDate: Date(),
                                          operationType: .orderCreated,
                                          productId: 0,
                                          orderDetailId: 0,
                                          customerId: 0,
                                          transactionId: transaction.transactionId,
                                          accountId: account_.accountId,
                                          supplierId: supplierId,
                                          staffId: staff.userId,
                                          categoryId: 0,
                                          orderId: order.orderId)
            Database.shared.createOperations([operation])
            // ----------- //
        }
    }
    
    // owner received money from customer
    func ownerReceivedMoneyFromCustomer(customerId: UInt64,
                                        staffId: UInt64,
                                        receivedMoney: Double,
                                        isMoneyForDebt: Bool,
                                        comment: String)
    {
        // --- Order --- //
        let order = Order()
        order.orderId = Database.shared.uniqueIncreasingPrimaryKey()
        order.customerId = customerId
        order.supplierId = 0
        order.orderDocument = .moneyReceive
        order.moneyMovementType = .moneyFromCustomer
        order.paymentId = 0
        order.comment = comment
        
        let currentDate = Date()
        order.orderDate = currentDate
        let str = "№ \(order.orderId) от \(currentDate.toString(.custom("EEEE, dd MMMM yyyy HH:mm")))"
        order.billingNo = str
        order.userId = staffId
        order.isDeleted = false
        order.isMoneyForDebt = isMoneyForDebt // is money come for debt
        order.isEdited = false
        
        // --- Payment --- //
        let payment = Payment()
        payment.paymentId = Database.shared.uniqueIncreasingPrimaryKey()
        payment.discount = 0
        payment.totalOrderPrice = receivedMoney
        payment.totalPriceWithDiscount = receivedMoney
        
        // -------------
        order.paymentId = payment.paymentId
        // -------------
        Database.shared.createOrders([order])
        Database.shared.createPayments([payment])
        // -------------
        
        
        // --- Transaction --- //
        let transactionId = Database.shared.uniqueIncreasingPrimaryKey()
        let transaction = Transaction(transactionId: transactionId,
                                      transactionDate: Date(),
                                      isLastTransaction: false,
                                      transactionType: .customerGaveMoneyForProduct,
                                      moneyAmount: receivedMoney,
                                      orderId: order.orderId,
                                      customerId: customerId,
                                      supplierId: 0,
                                      userId: staffId,
                                      ballanceAmount: 0,
                                      comment: "",
                                      transactionUuid: UUID().uuidString.lowercased())
        
        Database.shared.createTransactions([transaction])
        
        // ---- Change account of Customer
        if let account_ = Database.shared.accountBy(customerId: customerId) {
            account_.balance = account_.balance - receivedMoney
            Database.shared.updateCustomerBalanceFor(customerId: customerId,
                                                     balance: account_.balance)
            
            // ------ Sync OPERATION ----- //
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                          isSynchronized: false,
                                          syncOperationDate: Date(),
                                          operationType: .orderCreated,
                                          productId: 0,
                                          orderDetailId: 0,
                                          customerId: customerId,
                                          transactionId: transaction.transactionId,
                                          accountId: account_.accountId,
                                          supplierId: 0,
                                          staffId: staffId,
                                          categoryId: 0,
                                          orderId: order.orderId)
            Database.shared.createOperations([operation])
            // ----------- //
        }
    }
    
    // owner returned money to customer (For Product return)
    func ownerReturnedMoneyToCustomer(customerId: UInt64, staffId: UInt64, returnedMoney: Double, comment: String) -> Void {
        
        // --- Order --- //
        let order = Order()
        order.orderId = Database.shared.uniqueIncreasingPrimaryKey()
        order.customerId = customerId
        order.supplierId = 0
        order.orderDocument = .moneyGone
        order.moneyMovementType = .moneyReturnToCustomer
        order.paymentId = 0
        order.comment = comment
        
        let currentDate = Date()
        order.orderDate = currentDate
        let str = "№ \(order.orderId) от \(currentDate.toString(.custom("EEEE, dd MMMM yyyy HH:mm")))"
        order.billingNo = str
        order.userId = staffId
        order.isDeleted = false
        order.isMoneyForDebt = false
        order.isEdited = false
        
        // --- Payment --- //
        let payment = Payment()
        payment.paymentId = Database.shared.uniqueIncreasingPrimaryKey()
        payment.discount = 0
        payment.totalOrderPrice = returnedMoney
        payment.totalPriceWithDiscount = returnedMoney
        
        // -------------
        order.paymentId = payment.paymentId
        // -------------
        Database.shared.createOrders([order])
        Database.shared.createPayments([payment])
        // -------------
        
        
        let transactionId = Database.shared.uniqueIncreasingPrimaryKey()
        let transaction = Transaction(transactionId: transactionId,
                                      transactionDate: Date(),
                                      isLastTransaction: false,
                                      transactionType: .ownerReturnedMoneyForProductToCustomer,
                                      moneyAmount: returnedMoney,
                                      orderId: order.orderId,
                                      customerId: customerId,
                                      supplierId: 0,
                                      userId: staffId,
                                      ballanceAmount: 0,
                                      comment: "",
                                      transactionUuid: UUID().uuidString.lowercased())
        
        Database.shared.createTransactions([transaction])
        
        // ---- Change account of Customer
        if let account_ = Database.shared.accountBy(customerId: customerId) {
            account_.balance = account_.balance + returnedMoney
            Database.shared.updateCustomerBalanceFor(customerId: customerId,
                                                     balance: account_.balance)
            
            // ------ Sync OPERATION ----- //
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                          isSynchronized: false,
                                          syncOperationDate: Date(),
                                          operationType: .orderCreated,
                                          productId: 0,
                                          orderDetailId: 0,
                                          customerId: customerId,
                                          transactionId: transaction.transactionId,
                                          accountId: account_.accountId,
                                          supplierId: 0,
                                          staffId: staffId,
                                          categoryId: 0,
                                          orderId: order.orderId)
            Database.shared.createOperations([operation])
            // ----------- //
        }
    }
    
    
    // owner gived money to supplier (For Product receive)
    func ownerGivedMoneyToSupplierForProduct(supplierId: UInt64,
                                             staffId: UInt64,
                                             givedMoney: Double,
                                             comment: String) -> Void
    {
        // --- Order --- //
        let order = Order()
        order.orderId = Database.shared.uniqueIncreasingPrimaryKey()
        order.customerId = 0
        order.supplierId = supplierId
        order.orderDocument = .moneyGone
        order.moneyMovementType = .moneyGoneToSupplier
        order.paymentId = 0
        order.comment = comment
        
        let currentDate = Date()
        order.orderDate = currentDate
        let str = "№ \(order.orderId) от \(currentDate.toString(.custom("EEEE, dd MMMM yyyy HH:mm")))"
        order.billingNo = str
        order.userId = staffId
        order.isDeleted = false
        order.isMoneyForDebt = false
        order.isEdited = false
        
        // --- Payment --- //
        let payment = Payment()
        payment.paymentId = Database.shared.uniqueIncreasingPrimaryKey()
        payment.discount = 0
        payment.totalOrderPrice = givedMoney
        payment.totalPriceWithDiscount = givedMoney
        
        // -------------
        order.paymentId = payment.paymentId
        // -------------
        Database.shared.createOrders([order])
        Database.shared.createPayments([payment])
        
        
        let transactionId = Database.shared.uniqueIncreasingPrimaryKey()
        let transaction = Transaction(transactionId: transactionId,
                                      transactionDate: Date(),
                                      isLastTransaction: false,
                                      transactionType: .supplierReceivedMoneyForProduct,
                                      moneyAmount: givedMoney,
                                      orderId: order.orderId,
                                      customerId: 0,
                                      supplierId: supplierId,
                                      userId: staffId,
                                      ballanceAmount: 0,
                                      comment: "",
                                      transactionUuid: UUID().uuidString.lowercased())
        
        Database.shared.createTransactions([transaction])
        
        // ---- Change account of Customer
        if let account_ = Database.shared.accountBy(supplierId: supplierId) {
            account_.balance = account_.balance + givedMoney
            Database.shared.updateSupplierBalanceFor(supplierId: supplierId,
                                                        balance: account_.balance)
            
            // ------ Sync OPERATION ----- //
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                          isSynchronized: false,
                                          syncOperationDate: Date(),
                                          operationType: .orderCreated,
                                          productId: 0,
                                          orderDetailId: 0,
                                          customerId: 0,
                                          transactionId: transaction.transactionId,
                                          accountId: account_.accountId,
                                          supplierId: supplierId,
                                          staffId: staffId,
                                          categoryId: 0,
                                          orderId: order.orderId)
            Database.shared.createOperations([operation])
            // ----------- //
        }
    }
    
    
    // owner received money from supplier (For Product return to Supplier)
    func ownerReceivedRetrunMoneyFromSupplier(supplierId: UInt64, staffId: UInt64, receivedReturnMoney: Double, comment: String) -> Void {
        
        // --- Order --- //
        let order = Order()
        order.orderId = Database.shared.uniqueIncreasingPrimaryKey()
        order.customerId = 0
        order.supplierId = supplierId
        order.orderDocument = .moneyReceive
        order.moneyMovementType = .moneyFromSupplierReturn
        order.paymentId = 0
        order.comment = comment
        
        let currentDate = Date()
        order.orderDate = currentDate
        let str = "№ \(order.orderId) от \(currentDate.toString(.custom("EEEE, dd MMMM yyyy HH:mm")))"
        order.billingNo = str
        order.userId = staffId
        order.isDeleted = false
        order.isMoneyForDebt = false
        order.isEdited = false
        
        // --- Payment --- //
        let payment = Payment()
        payment.paymentId = Database.shared.uniqueIncreasingPrimaryKey()
        payment.discount = 0
        payment.totalOrderPrice = receivedReturnMoney
        payment.totalPriceWithDiscount = receivedReturnMoney
        
        // -------------
        order.paymentId = payment.paymentId
        // -------------
        Database.shared.createOrders([order])
        Database.shared.createPayments([payment])
        
        let transactionId = Database.shared.uniqueIncreasingPrimaryKey()
        let transaction = Transaction(transactionId: transactionId,
                                      transactionDate: Date(),
                                      isLastTransaction: false,
                                      transactionType: .ownerReturnedProductToSupplier,
                                      moneyAmount: receivedReturnMoney,
                                      orderId: order.orderId,
                                      customerId: 0,
                                      supplierId: supplierId,
                                      userId: staffId,
                                      ballanceAmount: 0,
                                      comment: "",
                                      transactionUuid: UUID().uuidString.lowercased())
        
        Database.shared.createTransactions([transaction])
        
        // ---- Change account of Customer
        if let account_ = Database.shared.accountBy(supplierId: supplierId) {
            account_.balance = account_.balance - receivedReturnMoney
            Database.shared.updateSupplierBalanceFor(supplierId: supplierId,
                                                        balance: account_.balance)
            
            // ------ Sync OPERATION ----- //
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                          isSynchronized: false,
                                          syncOperationDate: Date(),
                                          operationType: .orderCreated,
                                          productId: 0,
                                          orderDetailId: 0,
                                          customerId: 0,
                                          transactionId: transaction.transactionId,
                                          accountId: account_.accountId,
                                          supplierId: supplierId,
                                          staffId: staffId,
                                          categoryId: 0,
                                          orderId: order.orderId)
            Database.shared.createOperations([operation])
            // ----------- //
        }
    }
    
    func ownerMadePreOrderForCustomer(orderDetails: [OrderDetail],
                                      staff: User,
                                      customerId: UInt64,
                                      orderDocType: OrderDocumentType,
                                      totalPrice: Double,
                                      discount: Discount) -> Order {
        
        // --- Order --- //
        let order = Order()
        order.orderId = Database.shared.uniqueIncreasingPrimaryKey()
        order.customerId = customerId
        order.orderDocument = orderDocType
        order.paymentId = 0
        
        let currentDate = Date()
        order.orderDate = currentDate
        order.billingNo = OrderMemomry.shared.billingNo
        order.comment = OrderMemomry.shared.comments
        order.userId = staff.userId
        order.isDeleted = false
        order.isMoneyForDebt = false
        order.isEdited = false
        
        // --- Payment --- //
        let payment = Payment()
        payment.paymentId = Database.shared.uniqueIncreasingPrimaryKey()
        discount.totalPrice = totalPrice
        payment.setUpWith(discount: discount)
        // -------------
        order.paymentId = payment.paymentId
        // -------------
        
        // --- OrderDetails --- ------- //
        var orderDetailId = Database.shared.uniqueIncreasingPrimaryKey()
        
        for orderDetail in orderDetails {
            
            orderDetail.orderDetailId = orderDetailId
            orderDetail.orderId = order.orderId
            orderDetail.billingNo = order.billingNo
            
            orderDetailId = orderDetailId + 1
        }
        
        Database.shared.createOrders([order])
        Database.shared.createPayments([payment])
        Database.shared.createorderDetails(orderDetails)
        
        // ------ Sync OPERATION ----- //
        let operation = Event(syncOperationId: Database.shared.operationCid(),
                                      isSynchronized: false,
                                      syncOperationDate: Date(),
                                      operationType: .orderCreated,
                                      productId: 0,
                                      orderDetailId: 0,
                                      customerId: customerId,
                                      transactionId: 0,
                                      accountId: 0,
                                      supplierId: 0,
                                      staffId: staff.userId,
                                      categoryId: 0,
                                      orderId: order.orderId)
        Database.shared.createOperations([operation])
        return order
    }
    
    func increaseProductsInStock(orderDetails: [OrderDetail]) -> Void {
        var productsId: [UInt64] = []
        var productsAndQuantityDict: [UInt64: Double] = [:]
        
        for orderDetail in orderDetails {
            
            let key = orderDetail.productId
            let value = orderDetail.orderQuantity
            
            if let orderQuantity_ = productsAndQuantityDict[key] {
                productsAndQuantityDict[key] = value + orderQuantity_
            } else {
                productsId.append(key)
                productsAndQuantityDict[key] = value
            }
        }
        
        var productsAndQuantityDictForDatabase: [UInt64: Double] = [:]
        let productsForUpdate = Database.shared.productsFor(productIds: productsId)
        for product in productsForUpdate {
            if let quantity = productsAndQuantityDict[product.productId] {
                product.unitsInStock = product.unitsInStock + quantity
                productsAndQuantityDictForDatabase[product.productId] = product.unitsInStock
            }
        }
        Database.shared.updateProductUnitsInStock(productsAndQuantityDict: productsAndQuantityDictForDatabase)
    }
    
    func decreaseProductsInStock(orderDetails: [OrderDetail]) -> Void {
        var productsId: [UInt64] = []
        var productsAndQuantityDict: [UInt64: Double] = [:]
        
        for orderDetail in orderDetails {
            
            let key = orderDetail.productId
            let value = orderDetail.orderQuantity
            
            if let orderQuantity_ = productsAndQuantityDict[key] {
                productsAndQuantityDict[key] = value + orderQuantity_
            } else {
                productsId.append(key)
                productsAndQuantityDict[key] = value
            }
        }
        
        var productsAndQuantityDictForDatabase: [UInt64: Double] = [:]
        let productsForUpdate = Database.shared.productsFor(productIds: productsId)
        for product in productsForUpdate {
            if let quantity = productsAndQuantityDict[product.productId] {
                product.unitsInStock = product.unitsInStock - quantity
                productsAndQuantityDictForDatabase[product.productId] = product.unitsInStock
            }
        }
        Database.shared.updateProductUnitsInStock(productsAndQuantityDict: productsAndQuantityDictForDatabase)
    }
}
