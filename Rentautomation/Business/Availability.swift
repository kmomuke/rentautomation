//
//  Availability.swift
//  Rentautomation
//
//  Created by kanybek on 9/22/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

final class Availability {
    static let shared: Availability = Availability()
    init() {
    }
    
    func checkForProductPresence(_ orderDetails: [OrderDetail]) -> (Bool, String) {
        var isProductAvailable: Bool = true
        var productName: String = ""
        for orderDetail in orderDetails {
            if let product = Database.shared.productFor(productId: orderDetail.productId) {
                if product.unitsInStock < orderDetail.orderQuantity {
                    isProductAvailable = false
                    productName = product.productName
                    break
                }
            }
        }
        return (isProductAvailable, productName)
    }
}
