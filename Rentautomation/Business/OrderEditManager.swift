//
//  OrderEditManager.swift
//  Rentautomation
//
//  Created by kanybek on 1/7/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

final class OrderEditManager {
    static let shared: OrderEditManager = OrderEditManager()
    
    init() {
    }
    
    func ownerEdittedMoneyIncome(initialMoneyIncomeOrder: Order, staff: User) {
        
        let order = Order()
        order.orderId = Database.shared.uniqueIncreasingPrimaryKey()
        order.customerId = initialMoneyIncomeOrder.customerId
        order.supplierId = initialMoneyIncomeOrder.supplierId
        order.orderDocument = .moneyReceiveEdited
        order.moneyMovementType = .none
        order.paymentId = 0
        order.comment = ""
        
        let currentDate = Date().dateBySubtractingSeconds(10)
        order.orderDate = currentDate
        let str = "№ \(order.orderId) от \(currentDate.toString(.custom("EEEE, dd MMMM yyyy HH:mm")))"
        order.billingNo = str
        order.userId = initialMoneyIncomeOrder.userId
        order.isDeleted = false
        order.isMoneyForDebt = false
        order.isEdited = true
        
        var totalPriceWithDiscount__: Double = 0.0
        if let oldPayment = Database.shared.paymentBy(paymentId: initialMoneyIncomeOrder.paymentId) {
            
            let payment = Payment()
            payment.paymentId = Database.shared.uniqueIncreasingPrimaryKey()
            let oldDiscount = Discount(payment: oldPayment)
            payment.setUpWith(discount: oldDiscount)
            
            totalPriceWithDiscount__ = oldDiscount.totalPriceWithDiscount()
            // -------------
            order.paymentId = payment.paymentId
            // -------------
            Database.shared.createPayments([payment])
        }
        
        Database.shared.createOrders([order])
        
        if initialMoneyIncomeOrder.supplierId > 0 {
            if let account_ = Database.shared.accountBy(supplierId: initialMoneyIncomeOrder.supplierId) {
                account_.balance = account_.balance + totalPriceWithDiscount__
                Database.shared.updateSupplierBalanceFor(supplierId: initialMoneyIncomeOrder.supplierId,
                                                         balance: account_.balance)
                
                let transactionId = Database.shared.uniqueIncreasingPrimaryKey()
                let transaction = Transaction(transactionId: transactionId,
                                              transactionDate: currentDate,
                                              isLastTransaction: false,
                                              transactionType: .supplierBalanceAfterOrderCorrection,
                                              moneyAmount: totalPriceWithDiscount__,
                                              orderId: order.orderId,
                                              customerId: initialMoneyIncomeOrder.customerId,
                                              supplierId: initialMoneyIncomeOrder.supplierId,
                                              userId: initialMoneyIncomeOrder.userId,
                                              ballanceAmount: 0,
                                              comment: "",
                                              transactionUuid: UUID().uuidString.lowercased())
                
                Database.shared.createTransactions([transaction])
                
                let operation = Event(syncOperationId: Database.shared.operationCid(),
                                      isSynchronized: false,
                                      syncOperationDate: Date().dateBySubtractingSeconds(2),
                                      operationType: .orderCreated,
                                      productId: 0,
                                      orderDetailId: 0,
                                      customerId: initialMoneyIncomeOrder.customerId,
                                      transactionId: transaction.transactionId,
                                      accountId: account_.accountId,
                                      supplierId: initialMoneyIncomeOrder.supplierId,
                                      staffId: staff.userId,
                                      categoryId: 0,
                                      orderId: order.orderId)
                Database.shared.createOperations([operation])
            }
        } else if initialMoneyIncomeOrder.customerId > 0 {
            if let account_ = Database.shared.accountBy(customerId: initialMoneyIncomeOrder.customerId) {
                account_.balance = account_.balance + totalPriceWithDiscount__
                Database.shared.updateCustomerBalanceFor(customerId: initialMoneyIncomeOrder.customerId,
                                                         balance: account_.balance)
                
                let transactionId = Database.shared.uniqueIncreasingPrimaryKey()
                let transaction = Transaction(transactionId: transactionId,
                                              transactionDate: currentDate,
                                              isLastTransaction: false,
                                              transactionType: .customerBalanceAfterOrderCorrection,
                                              moneyAmount: totalPriceWithDiscount__,
                                              orderId: order.orderId,
                                              customerId: initialMoneyIncomeOrder.customerId,
                                              supplierId: initialMoneyIncomeOrder.supplierId,
                                              userId: initialMoneyIncomeOrder.userId,
                                              ballanceAmount: 0,
                                              comment: "",
                                              transactionUuid: UUID().uuidString.lowercased())
                
                Database.shared.createTransactions([transaction])
                
                let operation = Event(syncOperationId: Database.shared.operationCid(),
                                      isSynchronized: false,
                                      syncOperationDate: Date().dateBySubtractingSeconds(2),
                                      operationType: .orderCreated,
                                      productId: 0,
                                      orderDetailId: 0,
                                      customerId: initialMoneyIncomeOrder.customerId,
                                      transactionId: transaction.transactionId,
                                      accountId: account_.accountId,
                                      supplierId: initialMoneyIncomeOrder.supplierId,
                                      staffId: staff.userId,
                                      categoryId: 0,
                                      orderId: order.orderId)
                Database.shared.createOperations([operation])
            }
        } else {
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                  isSynchronized: false,
                                  syncOperationDate: Date().dateBySubtractingSeconds(2),
                                  operationType: .orderCreated,
                                  productId: 0,
                                  orderDetailId: 0,
                                  customerId: 0,
                                  transactionId: 0,
                                  accountId: 0,
                                  supplierId: 0,
                                  staffId: staff.userId,
                                  categoryId: 0,
                                  orderId: order.orderId)
            Database.shared.createOperations([operation])
        }
        
        // ------------------------------------------------------------------------------------------
        Database.shared.updateOrderEdittedStatus(orderId: initialMoneyIncomeOrder.orderId, isEditted: true)
        if true {
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                  isSynchronized: false,
                                  syncOperationDate: Date().dateBySubtractingSeconds(3),
                                  operationType: .orderUpdated,
                                  productId: 0,
                                  orderDetailId: 0,
                                  customerId: 0,
                                  transactionId: 0,
                                  accountId: 0,
                                  supplierId: 0,
                                  staffId: 0,
                                  categoryId: 0,
                                  orderId: initialMoneyIncomeOrder.orderId)
            Database.shared.createOperations([operation])
        }
    }
    
    
    func ownerEdittedMoneyGone(initialMoneyGoneOrder: Order, staff: User) {
        
        let order = Order()
        order.orderId = Database.shared.uniqueIncreasingPrimaryKey()
        order.customerId = initialMoneyGoneOrder.customerId
        order.supplierId = initialMoneyGoneOrder.supplierId
        order.orderDocument = .moneyGoneEdited
        order.moneyMovementType = .none
        order.paymentId = 0
        order.comment = ""
        
        let currentDate = Date().dateBySubtractingSeconds(10)
        order.orderDate = currentDate
        let str = "№ \(order.orderId) от \(currentDate.toString(.custom("EEEE, dd MMMM yyyy HH:mm")))"
        order.billingNo = str
        order.userId = initialMoneyGoneOrder.userId
        order.isDeleted = false
        order.isMoneyForDebt = false
        order.isEdited = true
        
        var totalPriceWithDiscount__: Double = 0.0
        if let oldPayment = Database.shared.paymentBy(paymentId: initialMoneyGoneOrder.paymentId) {
            
            let payment = Payment()
            payment.paymentId = Database.shared.uniqueIncreasingPrimaryKey()
            let oldDiscount = Discount(payment: oldPayment)
            payment.setUpWith(discount: oldDiscount)
            
            totalPriceWithDiscount__ = oldDiscount.totalPriceWithDiscount()
            // -------------
            order.paymentId = payment.paymentId
            Database.shared.createPayments([payment])
        }
        
        Database.shared.createOrders([order])
        
        if initialMoneyGoneOrder.supplierId > 0 {
            if let account_ = Database.shared.accountBy(supplierId: initialMoneyGoneOrder.supplierId) {
                account_.balance = account_.balance - totalPriceWithDiscount__
                Database.shared.updateSupplierBalanceFor(supplierId: initialMoneyGoneOrder.supplierId,
                                                         balance: account_.balance)
                
                let transactionId = Database.shared.uniqueIncreasingPrimaryKey()
                let transaction = Transaction(transactionId: transactionId,
                                              transactionDate: currentDate,
                                              isLastTransaction: false,
                                              transactionType: .supplierBalanceAfterOrderCorrection,
                                              moneyAmount: totalPriceWithDiscount__,
                                              orderId: order.orderId,
                                              customerId: initialMoneyGoneOrder.customerId,
                                              supplierId: initialMoneyGoneOrder.supplierId,
                                              userId: initialMoneyGoneOrder.userId,
                                              ballanceAmount: 0,
                                              comment: "",
                                              transactionUuid: UUID().uuidString.lowercased())
                
                Database.shared.createTransactions([transaction])
                
                
                let operation = Event(syncOperationId: Database.shared.operationCid(),
                                      isSynchronized: false,
                                      syncOperationDate: Date().dateBySubtractingSeconds(2),
                                      operationType: .orderCreated,
                                      productId: 0,
                                      orderDetailId: 0,
                                      customerId: initialMoneyGoneOrder.customerId,
                                      transactionId: transaction.transactionId,
                                      accountId: account_.accountId,
                                      supplierId: initialMoneyGoneOrder.supplierId,
                                      staffId: staff.userId,
                                      categoryId: 0,
                                      orderId: order.orderId)
                Database.shared.createOperations([operation])
            }
        } else if initialMoneyGoneOrder.customerId > 0 {
            if let account_ = Database.shared.accountBy(customerId: initialMoneyGoneOrder.customerId) {
                account_.balance = account_.balance - totalPriceWithDiscount__
                Database.shared.updateCustomerBalanceFor(customerId: initialMoneyGoneOrder.customerId,
                                                         balance: account_.balance)
                
                let transactionId = Database.shared.uniqueIncreasingPrimaryKey()
                let transaction = Transaction(transactionId: transactionId,
                                              transactionDate: currentDate,
                                              isLastTransaction: false,
                                              transactionType: .customerBalanceAfterOrderCorrection,
                                              moneyAmount: totalPriceWithDiscount__,
                                              orderId: order.orderId,
                                              customerId: initialMoneyGoneOrder.customerId,
                                              supplierId: initialMoneyGoneOrder.supplierId,
                                              userId: initialMoneyGoneOrder.userId,
                                              ballanceAmount: 0,
                                              comment: "",
                                              transactionUuid: UUID().uuidString.lowercased())
                
                Database.shared.createTransactions([transaction])
                
                let operation = Event(syncOperationId: Database.shared.operationCid(),
                                      isSynchronized: false,
                                      syncOperationDate: Date().dateBySubtractingSeconds(2),
                                      operationType: .orderCreated,
                                      productId: 0,
                                      orderDetailId: 0,
                                      customerId: initialMoneyGoneOrder.customerId,
                                      transactionId: transaction.transactionId,
                                      accountId: account_.accountId,
                                      supplierId: initialMoneyGoneOrder.supplierId,
                                      staffId: staff.userId,
                                      categoryId: 0,
                                      orderId: order.orderId)
                Database.shared.createOperations([operation])
            }
        } else {
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                  isSynchronized: false,
                                  syncOperationDate: Date().dateBySubtractingSeconds(2),
                                  operationType: .orderCreated,
                                  productId: 0,
                                  orderDetailId: 0,
                                  customerId: 0,
                                  transactionId: 0,
                                  accountId: 0,
                                  supplierId: 0,
                                  staffId: staff.userId,
                                  categoryId: 0,
                                  orderId: order.orderId)
            Database.shared.createOperations([operation])
        }
        
        // ------------------------------------------------------------------------------------------
        Database.shared.updateOrderEdittedStatus(orderId: initialMoneyGoneOrder.orderId, isEditted: true)
        if true {
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                  isSynchronized: false,
                                  syncOperationDate: Date().dateBySubtractingSeconds(3),
                                  operationType: .orderUpdated,
                                  productId: 0,
                                  orderDetailId: 0,
                                  customerId: 0,
                                  transactionId: 0,
                                  accountId: 0,
                                  supplierId: 0,
                                  staffId: 0,
                                  categoryId: 0,
                                  orderId: initialMoneyGoneOrder.orderId)
            Database.shared.createOperations([operation])
        }
    }
    
    func updateInitialOrder(initialCustomerPreOrder: Order) {
        Database.shared.updateOrderEdittedStatus(orderId: initialCustomerPreOrder.orderId, isEditted: true)
        if true {
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                  isSynchronized: false,
                                  syncOperationDate: Date().dateBySubtractingSeconds(1),
                                  operationType: .orderUpdated,
                                  productId: 0,
                                  orderDetailId: 0,
                                  customerId: 0,
                                  transactionId: 0,
                                  accountId: 0,
                                  supplierId: 0,
                                  staffId: 0,
                                  categoryId: 0,
                                  orderId: initialCustomerPreOrder.orderId)
            Database.shared.createOperations([operation])
        }
    }
    
    func ownerEdittedPreOrderForCustomer(initialCustomerPreOrder: Order,
                                         orderDetails: [OrderDetail],
                                         staff: User,
                                         customerId: UInt64,
                                         orderDocumentType: OrderDocumentType,
                                         totalPrice: Double,
                                         discount: Discount) -> Order {
        
        Database.shared.updateOrderEdittedStatus(orderId: initialCustomerPreOrder.orderId, isEditted: true)
        if true {
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                  isSynchronized: false,
                                  syncOperationDate: Date().dateBySubtractingSeconds(3),
                                  operationType: .orderUpdated,
                                  productId: 0,
                                  orderDetailId: 0,
                                  customerId: 0,
                                  transactionId: 0,
                                  accountId: 0,
                                  supplierId: 0,
                                  staffId: 0,
                                  categoryId: 0,
                                  orderId: initialCustomerPreOrder.orderId)
            Database.shared.createOperations([operation])
        }
        
        return OrderManager.shared.ownerMadePreOrderForCustomer(orderDetails: orderDetails,
                                                                staff: staff,
                                                                customerId: customerId,
                                                                orderDocType: orderDocumentType,
                                                                totalPrice: totalPrice,
                                                                discount: discount)
    }
    
    func ownerEditedProductsSaleToCustomer(initialSaleOrder: Order,
                                           newOrderDetails: [OrderDetail],
                                           newStaff: User,
                                           newCustomerId: UInt64,
                                           newOrderDocumentType: OrderDocumentType,
                                           newTotalPrice: Double,
                                           newDiscount: Discount) -> Void {
        
        // --- Order --- //
        let order = Order()
        order.orderId = Database.shared.uniqueIncreasingPrimaryKey()
        order.customerId = initialSaleOrder.customerId
        order.orderDocument = newOrderDocumentType
        order.paymentId = 0
        
        let currentDate = Date().dateBySubtractingSeconds(10)
        order.orderDate = currentDate
        let str = "№ \(order.orderId) от \(currentDate.toString(.custom("EEEE, dd MMMM yyyy HH:mm")))"
        order.billingNo = str
        //order.staffId = newStaff.staffId
        order.userId = initialSaleOrder.userId
        order.isDeleted = false
        order.isMoneyForDebt = false
        order.isEdited = true
        
        var totalPriceWithDiscount__: Double = 0.0
        if let oldPayment = Database.shared.paymentBy(paymentId: initialSaleOrder.paymentId) {
            // --- Payment --- //
            let payment = Payment()
            payment.paymentId = Database.shared.uniqueIncreasingPrimaryKey()
            let oldDiscount = Discount(payment: oldPayment)
            payment.setUpWith(discount: oldDiscount)
            
            totalPriceWithDiscount__ = oldDiscount.totalPriceWithDiscount()
            // -------------
            order.paymentId = payment.paymentId
            // -------------
            Database.shared.createPayments([payment])
        }
        
        // --- OrderDetails --- ------- //
        let oldOdetails = Database.shared.orderDetailsBy(orderId: initialSaleOrder.orderId)
        var orderDetailId = Database.shared.uniqueIncreasingPrimaryKey()
        
        for (index, orderDetail) in oldOdetails.enumerated() {
            
            orderDetail.orderDetailId = orderDetailId
            orderDetail.orderId = order.orderId
            orderDetail.orderDetailDate = Date().dateBySubtractingSeconds(60).dateByAddingSeconds(index)
            orderDetail.billingNo = order.billingNo
            orderDetailId = orderDetailId + 1
        }
        
        Database.shared.createorderDetails(oldOdetails)
        Database.shared.createOrders([order])
        
        Database.shared.updateOrderEdittedStatus(orderId: initialSaleOrder.orderId, isEditted: true)
        if true {
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                  isSynchronized: false,
                                  syncOperationDate: Date().dateBySubtractingSeconds(3),
                                  operationType: .orderUpdated,
                                  productId: 0,
                                  orderDetailId: 0,
                                  customerId: 0,
                                  transactionId: 0,
                                  accountId: 0,
                                  supplierId: 0,
                                  staffId: 0,
                                  categoryId: 0,
                                  orderId: initialSaleOrder.orderId)
            Database.shared.createOperations([operation])
        }
        
        // ---------- Transactions for Product ----------
        OrderManager.shared.increaseProductsInStock(orderDetails: oldOdetails)
        
        // ---------- Transaction Customer ----------
        let transactionId = Database.shared.uniqueIncreasingPrimaryKey()
        let transaction = Transaction(transactionId: transactionId,
                                      transactionDate: currentDate,
                                      isLastTransaction: false,
                                      transactionType: .customerBalanceAfterOrderCorrection,
                                      moneyAmount: totalPriceWithDiscount__,
                                      orderId: order.orderId,
                                      customerId: initialSaleOrder.customerId,
                                      supplierId: 0,
                                      userId: newStaff.userId,
                                      ballanceAmount: 0,
                                      comment: "",
                                      transactionUuid: UUID().uuidString.lowercased())
        
        Database.shared.createTransactions([transaction])
        
        
        // ---- Change account of Customer
        if let account_ = Database.shared.accountBy(customerId: initialSaleOrder.customerId) {
            account_.balance = account_.balance - totalPriceWithDiscount__
            Database.shared.updateCustomerBalanceFor(customerId: initialSaleOrder.customerId,
                                                                    balance: account_.balance)
            
            // ------ Sync OPERATION ----- //
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                  isSynchronized: false,
                                  syncOperationDate: Date().dateBySubtractingSeconds(2),
                                  operationType: .orderCreated,
                                  productId: 0,
                                  orderDetailId: 0,
                                  customerId: initialSaleOrder.customerId,
                                  transactionId: transaction.transactionId,
                                  accountId: account_.accountId,
                                  supplierId: 0,
                                  staffId: newStaff.userId,
                                  categoryId: 0,
                                  orderId: order.orderId)
            Database.shared.createOperations([operation])
        }
        
        if newOrderDetails.count > 0 {
            let _ = OrderManager.shared.ownerSaledProductsToCustomer(orderDetails: newOrderDetails,
                                                                     staff: newStaff,
                                                                     customerId: newCustomerId,
                                                                     orderDocType: .saleToCustomer,
                                                                     totalPrice: newTotalPrice,
                                                                     discount: newDiscount)
        }
    }
    
    
    //----------------------------------------------------------------------------------------//
    func ownerEditedProductsReceiveFromSupplier(initialProductIncomeOrder: Order,
                                                newOrderDetails: [OrderDetail],
                                                newStaff: User,
                                                newSupplier: UInt64,
                                                newOrderDocumentType: OrderDocumentType,
                                                newTotalPrice: Double,
                                                newDiscount: Discount) -> Void {
        
        // --- Order --- //
        let order = Order()
        order.orderId = Database.shared.uniqueIncreasingPrimaryKey()
        order.customerId = 0
        order.supplierId = initialProductIncomeOrder.supplierId
        order.orderDocument = newOrderDocumentType
        order.paymentId = 0
        
        let currentDate = Date().dateBySubtractingSeconds(10)
        order.orderDate = currentDate
        let str = "№ \(order.orderId) от \(currentDate.toString(.custom("EEEE, dd MMMM yyyy HH:mm")))"
        order.billingNo = str
        order.userId = newStaff.userId
        order.isDeleted = false
        order.isMoneyForDebt = false
        order.isEdited = true
        
        // --- Payment --- //
        var totalPriceWithDiscount__: Double = 0.0
        if let oldPayment = Database.shared.paymentBy(paymentId: initialProductIncomeOrder.paymentId) {
            let payment = Payment()
            payment.paymentId = Database.shared.uniqueIncreasingPrimaryKey()
            let oldDiscount = Discount(payment: oldPayment)
            payment.setUpWith(discount: oldDiscount)
            totalPriceWithDiscount__ = oldDiscount.totalPriceWithDiscount()
            // -------------
            order.paymentId = payment.paymentId
            // -------------
            Database.shared.createPayments([payment])
        }
        
        
        // --- OrderDetails --- ------- //
        let oldOdetails = Database.shared.orderDetailsBy(orderId: initialProductIncomeOrder.orderId)
        var orderDetailId = Database.shared.uniqueIncreasingPrimaryKey()
        
        for (index, orderDetail) in oldOdetails.enumerated() {
            orderDetail.orderDetailId = orderDetailId
            orderDetail.orderId = order.orderId
            orderDetail.billingNo = order.billingNo
            orderDetail.orderDetailDate = Date().dateBySubtractingSeconds(60).dateByAddingSeconds(index)
            orderDetailId = orderDetailId + 1
        }
        
        Database.shared.createOrders([order])
        Database.shared.createorderDetails(oldOdetails)
        Database.shared.updateOrderEdittedStatus(orderId: initialProductIncomeOrder.orderId, isEditted: true)
        if true {
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                  isSynchronized: false,
                                  syncOperationDate: Date().dateBySubtractingSeconds(3),
                                  operationType: .orderUpdated,
                                  productId: 0,
                                  orderDetailId: 0,
                                  customerId: 0,
                                  transactionId: 0,
                                  accountId: 0,
                                  supplierId: 0,
                                  staffId: 0,
                                  categoryId: 0,
                                  orderId: initialProductIncomeOrder.orderId)
            Database.shared.createOperations([operation])
        }
        
        // ---------- Transactions for Product ----------
        OrderManager.shared.decreaseProductsInStock(orderDetails: oldOdetails)
        
        // ----- Transaction Supplier -----
        let transactionId = Database.shared.uniqueIncreasingPrimaryKey()
        let transaction = Transaction(transactionId: transactionId,
                                      transactionDate: currentDate,
                                      isLastTransaction: false,
                                      transactionType: .supplierBalanceAfterOrderCorrection,
                                      moneyAmount: totalPriceWithDiscount__,
                                      orderId: order.orderId,
                                      customerId: 0,
                                      supplierId: initialProductIncomeOrder.supplierId,
                                      userId: newStaff.userId,
                                      ballanceAmount: 0,
                                      comment: "",
                                      transactionUuid: UUID().uuidString.lowercased())
        
        Database.shared.createTransactions([transaction])
    
        // ---- Change account of Customer
        if let account_ = Database.shared.accountBy(supplierId: initialProductIncomeOrder.supplierId) {
            account_.balance = account_.balance + totalPriceWithDiscount__
            Database.shared.updateSupplierBalanceFor(supplierId: initialProductIncomeOrder.supplierId,
                                                                    balance: account_.balance)
            
            // ------ Sync OPERATION ----- //
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                  isSynchronized: false,
                                  syncOperationDate: Date().dateBySubtractingSeconds(2),
                                  operationType: .orderCreated,
                                  productId: 0,
                                  orderDetailId: 0,
                                  customerId: initialProductIncomeOrder.customerId,
                                  transactionId: transaction.transactionId,
                                  accountId: account_.accountId,
                                  supplierId: 0,
                                  staffId: newStaff.userId,
                                  categoryId: 0,
                                  orderId: order.orderId)
            Database.shared.createOperations([operation])
        }
        if newOrderDetails.count > 0 {
            OrderManager.shared.ownerReceivedProductsFromSupplier(orderDetails: newOrderDetails,
                                                                  staff: newStaff,
                                                                  supplierId: newSupplier,
                                                                  orderDocType: .receiveFromSupplier,
                                                                  totalPrice: newTotalPrice,
                                                                  discount: newDiscount)
        }
    }
}
