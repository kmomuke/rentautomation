//
//  CSVGenerator.swift
//  Rentautomation
//
//  Created by kanybek on 3/1/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

private struct OrderCSV {
    
    var orderId: String
    var orderDate: String
    var orderType: String
    var customerSupplierName: String
    var orderDiscount: String
    var orderTotalPrice: String
    var staffName: String
    var billingNo: String
    var comment: String
    
    var orderDetails: [OrderDetailCSV]
    
    init(orderId: String, orderDate: String, orderType: String,
         customerSupplierName: String, orderDiscount: String, orderTotalPrice: String,
         staffName: String, billingNo: String, comment: String, orderDetails: [OrderDetailCSV]) {
        
        self.orderId = orderId
        self.orderDate = orderDate
        self.orderType = orderType
        self.customerSupplierName = customerSupplierName
        self.orderDiscount = orderDiscount
        self.orderTotalPrice = orderTotalPrice
        self.staffName = staffName
        self.billingNo = billingNo
        self.comment = comment
        self.orderDetails = orderDetails
    }
}

private struct OrderDetailCSV {
    
    var orderDetailId: String
    var productName: String
    var price: String
    var quantityAmount: String
    var totalPrice: String
    var orderDetailDiscount: String
    var totalPriceWithDiscount: String
    
    init(orderDetailId: String, productName: String, price: String,
         quantityAmount: String, totalPrice: String, orderDetailDiscount: String,
         totalPriceWithDiscount: String) {
        
        self.orderDetailId = orderDetailId
        self.productName = productName
        self.price = price
        
        self.quantityAmount = quantityAmount
        self.totalPrice = totalPrice
        self.orderDetailDiscount = orderDetailDiscount
        
        self.totalPriceWithDiscount = totalPriceWithDiscount
    }
}


final class CSVGenerator {
    static let shared: CSVGenerator = CSVGenerator()
    var url: URL
    
    var isGeneratingCSV: Bool
    var isTotalDataComputing: Bool
    
    init () {
        self.isGeneratingCSV = false
        self.isTotalDataComputing = false
        
        let local_path =  "image_path.csv"
        let tempPath = NSTemporaryDirectory().stringByAppendingPathComponent(path: local_path)
        self.url = NSURL.fileURL(withPath: tempPath)
    }
    
    func someMethod1(_ sender: UIViewController) -> Void {
        
        if let filePath = Bundle.main.path(forResource: "large", ofType: "csv") {
            
            do {
                let contents = try String(contentsOfFile: filePath)
                var csv = try CSVParser(content: contents)
                
                csv.append(["test0", "test1", "test2"])
                csv.append(["---------------------"])
                csv.append(["test0", "test1", "test2", "test3", "test4", "test5"])
                csv.append([""])
                csv.append(["test0", "test1"])
                csv.append(["test0"])
                
                csv.append([""])
                csv.append(["test0", "test1", "test2", "test3", "test4", "test5"])
                
                for (index, row) in csv.enumerated() {
                    print("\(index)   || \(row)") // ["first column", "sceond column", "third column"]
                }
                
                print("-----------")
                
                // get row by int subscript
                let kokeok =  csv[10] // the No.10 row
                print(kokeok)
                
                //print("-----------")
                // get column by string subscript
                //if let kokeok23 = csv["id"] { // column with header key "id"
                //    print(kokeok23)
                //}
                
                //print("-----------")
                //for dic in csv.enumeratedWithDic() {
                //    print(dic) // dic is [String: String]
                //}
                
                //let local_path =  "image_path_\(UInt64(Date().timeIntervalSince1970 * 1000)).csv"
                let local_path =  "image_path.csv"
                
                
                let tempPath = NSTemporaryDirectory().stringByAppendingPathComponent(path: local_path)
                print("tempPath ==> \(tempPath)")
                
                try FileManager().removeItem(at: NSURL.fileURL(withPath: tempPath))
                
                try csv.wirite(toFilePath: tempPath)
                
                print("-----------")
                print("-----------")
                print("-----------")
                
                
                let anotherCSV = try CSVParser(filePath: tempPath)
                // get every row in csv
                for (index, row) in anotherCSV.enumerated() {
                    print("\(index)   || \(row)")
                }
                
                self.url = NSURL.fileURL(withPath: tempPath)
                let activityViewController = UIActivityViewController(activityItems: [self.url] , applicationActivities: nil)
                if activityViewController.responds(to: #selector(getter: UIViewController.popoverPresentationController)) {
                    activityViewController.popoverPresentationController?.sourceView = sender.view
                }
                
                sender.present(activityViewController, animated: true, completion: {
                })
                
            } catch let error as NSError {
                // contents could not be loaded
                print("Error CSVGenerator: \(error.localizedDescription)")
            }
        }
    }
    
    func generateCSVFrom(orders: [Order],
                         progress:@escaping (Float) -> (),
                         completion:@escaping (URL) -> ()) -> Void {
        
        if self.isGeneratingCSV {
            return
        }
        
        let totalOrders: Float = Float(orders.count)
        
        if !self.isGeneratingCSV {
            self.isGeneratingCSV = true
        
            var orderCSVs: [OrderCSV] = []
            
            generateQue.async {
                
                for (index, order) in orders.enumerated() {
                    
                    let orderIdStr = "\(order.orderId)"
                    let orderDateStr = "\(order.orderDate.toString(.custom("EEE dd MMM HH:mm:ss")))"
                    let orderTypeStr = Order.orderDocumentTypeStringName(orderDocType: order.orderDocument)
                    
                    var customerSupplierName: String = ""
                    if order.customerId > 0 {
                        if let customer = Database.shared.customerBy(customerId: order.customerId) {
                            customerSupplierName = "\(customer.firstName) \(customer.secondName)"
                        }
                    } else if order.supplierId > 0 {
                        if let supplier = Database.shared.supplierBy(supplierId: order.supplierId) {
                            customerSupplierName = "\(supplier.companyName)"
                        }
                    } else {
                        customerSupplierName = Order.moneyMovementTypeStringName(moneyType: order.moneyMovementType)
                    }
                    
                    var moneyMovementString: String = ""
                    if Order.moneyMovementTypeStringName(moneyType: order.moneyMovementType) != "Пусто" {
                        moneyMovementString = Order.moneyMovementTypeStringName(moneyType: order.moneyMovementType)
                    }
                    
                    var orderDetailCount: String = ""
                    if order.orderId > 0 {
                        let totalCount = Database.shared.countOrderDetailsBy(orderId: order.orderId)
                        if totalCount > 0 {
                            orderDetailCount = " \(totalCount)-товара"
                        }
                    }
                    customerSupplierName = customerSupplierName + orderDetailCount + " " + moneyMovementString
                    
                    if order.isEdited {
                        customerSupplierName = customerSupplierName + " документ редактирован"
                    }
                    
                    var discount: Double = 0.0
                    var totalSum: Double = 0.0
                    if let payment = Database.shared.paymentBy(paymentId: order.paymentId) {
                        discount = payment.discount
                        totalSum = payment.totalPriceWithDiscount
                    }
                    
                    let orderDiscountStr = "\(discount)%"
                    let orderTotalPriceStr = "\(totalSum)"
                    
                    var staffName: String = ""
                    if order.userId > 0 {
                        if let staff = Database.shared.userBy(userId: order.userId) {
                            staffName = "\(staff.firstName) \(staff.secondName)"
                        }
                    }
                    
                    //let billingNoStr__ = String(order.billingNo.characters.filter { !" \n\t\r".characters.contains($0) })
                    let billingNoStr__ = order.billingNo.replacingOccurrences(of: "\n", with: " ")
                    let billingNoStr = billingNoStr__.replacingOccurrences(of: ",", with: " ")
                    
                    let commentStr__ = order.comment.replacingOccurrences(of: "\n", with: " ")
                    let commentStr = commentStr__.replacingOccurrences(of: ",", with: " ")
                    
                    let orderDetails = Database.shared.orderDetailsBy(orderId: order.orderId)
                    var orderDetailCSVs: [OrderDetailCSV] = []
                    
                    if orderDetails.count > 0 {
                        for orderDetail in orderDetails {
                            
                            let orderDetailIdStr = "\(orderDetail.orderDetailId)"
                            var productName: String = ""
                            var quantityPerUnit: String = ""
                            if let product = Database.shared.productFor(productId: orderDetail.productId) {
                                productName = product.productName
                                quantityPerUnit = product.quantityPerUnit
                            }
                            
                            let price = "\(orderDetail.price)"
                            let orderQuantity = "\(orderDetail.orderQuantity)"// \(quantityPerUnit)"
                            
                            let totalPrice = orderDetail.price * orderDetail.orderQuantity
                            let totalPriceStr = "\(totalPrice)"
                            
                            let totalPriceWithDiscount = orderDetail.price * orderDetail.orderQuantity
                            
                            let totalPriceWithDiscountStr = "\(totalPriceWithDiscount)"
                            
                            let discountStr = "\(0)%"
                            
                            let orderDetailCSV = OrderDetailCSV(orderDetailId: orderDetailIdStr,
                                                                productName: productName,
                                                                price: price,
                                                                quantityAmount: orderQuantity,
                                                                totalPrice: totalPriceStr,
                                                                orderDetailDiscount: discountStr,
                                                                totalPriceWithDiscount: totalPriceWithDiscountStr)
                            
                            orderDetailCSVs.append(orderDetailCSV)
                        }
                    }
                    
                    let orderCSV: OrderCSV = OrderCSV(orderId: orderIdStr,
                                                      orderDate: orderDateStr,
                                                      orderType: orderTypeStr,
                                                      customerSupplierName: customerSupplierName,
                                                      orderDiscount: orderDiscountStr,
                                                      orderTotalPrice: orderTotalPriceStr,
                                                      staffName: staffName,
                                                      billingNo: billingNoStr,
                                                      comment: commentStr,
                                                      orderDetails: orderDetailCSVs)
                    
                    orderCSVs.append(orderCSV)
                    
                    DispatchQueue.main.async {
                        let progressFraction: Float = Float(index)/totalOrders
                        progress(progressFraction)
                    }
                }
                
                if let filePath = Bundle.main.path(forResource: "empty_fields", ofType: "csv") {
                    do {
                        let contents = try String(contentsOfFile: filePath)
                        //var csv = try CSVParser(content: contents, delimiter: ";", lineSeparator: "\n", hasHeader: true)
                        var csv = try CSVParser(content: contents)
                        
                        //             1        2              3              4               5              6        7          8          9
                        csv.append(["Номер", "Дата", "Тип операции", "Сотрудник", "Наименование", "Цена", "Колич", "Клиент", "Сумма",
                                    "Комментариий", "Дополнительно"]) //10, 11
                        
                        for (index, orderCSV) in orderCSVs.reversed().enumerated() {
                        //for (index, orderCSV) in orderCSVs.enumerated() {
                            //orderCSV.customerSupplierName
                            var name = " "
                            if orderCSV.orderType != "Продажа продукта к Клиенту" {
                                name = orderCSV.customerSupplierName
                            }
                            
                            csv.append([orderCSV.orderId, orderCSV.orderDate, orderCSV.orderType, orderCSV.staffName, " ", " ", " ", name, orderCSV.orderTotalPrice, orderCSV.comment, orderCSV.billingNo])
                            
                            if orderCSV.orderDetails.count > 0 {
                                
                                for orderDetailCSV in orderCSV.orderDetails {
                                    csv.append([" ", " ", " ", " ",
                                                orderDetailCSV.productName, orderDetailCSV.price, orderDetailCSV.quantityAmount,
                                                orderCSV.customerSupplierName, orderDetailCSV.totalPriceWithDiscount])
                                }
                            }
                            
                            csv.append([" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "])
                            
                            DispatchQueue.main.async {
                                let progressFraction: Float = Float(index)/totalOrders
                                progress(progressFraction)
                            }
                        }
                        
                        let local_path =  "\(UInt64(Date().timeIntervalSince1970 * 1000)).csv"
                        let tempPath = NSTemporaryDirectory().stringByAppendingPathComponent(path: local_path)
                        print("tempPath ==> \(tempPath)")
                        //try FileManager().removeItem(at: NSURL.fileURL(withPath: tempPath))
                        try csv.wirite(toFilePath: tempPath)
                        
                        self.url = NSURL.fileURL(withPath: tempPath)
                        
                    } catch let error as NSError {
                        print("Error CSVGenerator: \(error.localizedDescription)")
                    }
                }
                
                DispatchQueue.main.async {
                    self.isGeneratingCSV = false
                    completion(self.url)
                }
            }
        }
    }
    
    func generateOrdersToIntegrateWith1C(orders: [Order],
                                         progress:@escaping (Float) -> (),
                                         completion:@escaping (URL) -> ()) -> Void {
    }
    
    func compareTotalData(completion:@escaping ((UInt64,UInt64,UInt64,Double,Double,Double,Double)) -> ()) -> Void {
        
        if self.isTotalDataComputing {
            return
        }
        
        if !self.isTotalDataComputing {
            self.isTotalDataComputing = true
            
            var totalSaleSum: Double = 0.0
            var totalIncomeSum: Double = 0.0
            var totalProductAmount: Double = 0.0
            
            var totalSupplierDebt: Double = 0.0
            
            var totalCustomerDebt: Double = 0.0
            
            generateQue.async {
                
                let totalOrdersCount = Database.shared.totalOrdersCount()
                let totalOrderDetailsCount =  Database.shared.totalOrderDetails()
                let transactionsCount = Database.shared.totalTransactionsCount()
                
                let products_ = Database.shared.loadAllProducts(100000)
                
                for product in products_ {
                    totalSaleSum = totalSaleSum + product.saleUnitPrice * product.unitsInStock
                    totalIncomeSum = totalIncomeSum + product.incomeUnitPrice * product.unitsInStock
                    totalProductAmount = totalProductAmount + product.unitsInStock
                }
                
                let suppliers_ = Database.shared.loadAllSuppliers(100000)
                for supplier in suppliers_ {
                    if let account = Database.shared.accountBy(supplierId: supplier.supplierId) {
                        totalSupplierDebt = totalSupplierDebt + account.balance
                    }
                }
                
                let customers_ = Database.shared.loadAllCustomers(100000)
                for customer in customers_ {
                    if let account = Database.shared.accountBy(customerId: customer.customerId) {
                        if account.balance > 0 {
                            totalCustomerDebt = totalCustomerDebt + account.balance
                        }
                    }
                }
                
                DispatchQueue.main.async {
                    self.isTotalDataComputing = false
                    completion((UInt64(totalOrdersCount), UInt64(totalOrderDetailsCount), UInt64(transactionsCount),
                                totalSaleSum, totalProductAmount, totalSupplierDebt, totalCustomerDebt))
                }
            }
        }
    }
    
}
