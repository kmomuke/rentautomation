//
//  SignalPipesStore.swift
//  Rentautomation
//
//  Created by kanybek on 12/2/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit

public enum RpcConnectionType : UInt {
    case connecting = 1000
    case disconnected = 2000
    case connected = 3000
}

final class PipesStore {
    
    static let shared: PipesStore = PipesStore()
    
    required init() {
    }
    
    // MARK: Products
    let productCreatedPipe = ValuePipe<Product>()
    let productUpdatedPipe = ValuePipe<Product>()
    
    func productCreatedSignal() -> Signal<Product, Void> {
        return self.productCreatedPipe.signal()
    }
    
    func productUpdatedSignal() -> Signal<Product, Void> {
        return self.productUpdatedPipe.signal()
    }
    
    // MARK: Categories
    let categoryCreatedPipe = ValuePipe<Category>()
    let categoryUpdatedPipe = ValuePipe<Category>()
    
    func categoryCreatedSignal() -> Signal<Category, Void> {
        return self.categoryCreatedPipe.signal()
    }
    
    func categoryUpdatedSignal() -> Signal<Category, Void> {
        return self.categoryUpdatedPipe.signal()
    }
    
    // MARK: Suppliers
    let supplierCreatedPipe = ValuePipe<Supplier>()
    let supplierUpdatedPipe = ValuePipe<Supplier>()
    
    func supplierCreatedSignal() -> Signal<Supplier, Void> {
        return self.supplierCreatedPipe.signal()
    }
    
    func supplierUpdatedSignal() -> Signal<Supplier, Void> {
        return self.supplierUpdatedPipe.signal()
    }
    
    // MARK: Customers
    let customerCreatedPipe = ValuePipe<Customer>()
    let customerUpdatedPipe = ValuePipe<Customer>()
    
    func customerCreatedSignal() -> Signal<Customer, Void> {
        return self.customerCreatedPipe.signal()
    }
    
    func customerUpdatedSignal() -> Signal<Customer, Void> {
        return self.customerUpdatedPipe.signal()
    }
    
    // MARK: Staff
    let staffCreatedPipe = ValuePipe<User>()
    let staffUpdatedPipe = ValuePipe<User>()
    
    func staffCreatedSignal() -> Signal<User, Void> {
        return self.staffCreatedPipe.signal()
    }
    
    func staffUpdatedSignal() -> Signal<User, Void> {
        return self.staffUpdatedPipe.signal()
    }
    
    // MARK: Product selected
    let productSelectedPipe = ValuePipe<(Product, Bool)>()

    func productSelectedSignal() -> Signal<(Product, Bool), Void> {
        return self.productSelectedPipe.signal()
    }
    
    // MARK: All seleced OrderDetails
    let totalSelectedOrderDetailsPipe = ValuePipe<[OrderDetail]>()
    
    func totalSelectedOrderDetailsSignal() -> Signal<[OrderDetail], Void> {
        return self.totalSelectedOrderDetailsPipe.signal()
    }
    
    let productRemovedFromSaleListPipe = ValuePipe<OrderDetail>()
    func productRemovedFromSaleListSignal() -> Signal<OrderDetail, Void> {
        return self.productRemovedFromSaleListPipe.signal()
    }
    
    // MARK: Customer and Supplier balance updated
    let balanceUpdatedPipe = ValuePipe<UInt64>()
    
    func balanceUpdatedSignal() -> Signal<UInt64, Void> {
        return self.balanceUpdatedPipe.signal()
    }
    
    // MARK: One Sync operation finished
    let syncFinishedPipe = ValuePipe<Bool>()
    func syncFinishedSignal() -> Signal<Bool, Void> {
        return self.syncFinishedPipe.signal()
    }
    
    // MARK: ALL sync operation finished pipe
    let allSyncOperationsFinishedPipe = ValuePipe<Bool>()
    func allSyncOperationsFinishedSignal() -> Signal<Bool, Void> {
        return self.allSyncOperationsFinishedPipe.signal()
    }
    
    // MARK: RPC status
    let rpcConnectionStatusPipe = ValuePipe<RpcConnectionType>()
    func rpcConnectionStatusSignal() -> Signal<RpcConnectionType, Void> {
        return self.rpcConnectionStatusPipe.signal()
    }
    
    // MARK: Categories
    let sizeAndColorCreatedPipe = ValuePipe<String>()
    func sizeAndColorCreatedSignal() -> Signal<String, Void> {
        return self.sizeAndColorCreatedPipe.signal()
    }
    
    // MARK: Categories
    let userAccCreatedPipe = ValuePipe<UserAccount>()
    func userAccCreatedSignal() -> Signal<UserAccount, Void> {
        return self.userAccCreatedPipe.signal()
    }
    
}

