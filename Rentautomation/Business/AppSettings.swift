//
//  StaffSettingsManager.swift
//  Rentautomation
//
//  Created by kanybek on 12/30/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

final class AppSettings {
    
    static let shared: AppSettings = AppSettings()
    
    let timeForShowPasscode: Int32 = 60
    
    required init() {
    }
    
    private let lastDateKey = "mobilnik.lock.date"
    private let currencyKey = "rentautomation.lock.currency"
    private let maximumDigitsKey = "rentautomation.lock.maximumDigitsKey"
    private let ipAddressKey = "rentautomation.lock.ipAddressKey"
    private let customersStaffKey = "rentautomation.lock.shouldUseCustomersStaff"
    private let useDefaultCustomerKey = "rentautomation.lock.shouldUseDefaultCustomer"
    private let developerModeKey = "rentautomation.lock.developerMode"
    private let autoSyncronizeModeKey = "rentautomation.lock.autoSyncronizeMode"
    private let autoClearKey = "rentautomation.lock.autoClear"
    private let shouldFrontCameraKey = "rentautomation.lock.shouldFrontCamera"
    private let companyNameKey = "rentautomation.lock.companyName33333"
    private let passwordOnProductKey = "rentautomation.lock.passwordOnProduct"
    private let productUniqModeKey = "rentautomation.lock.productUniqMode"
    private let orderDraftModeKey = "rentautomation.lock.orderDraftMode"
    private let productAvailabilityKey = "rentautomation.lock.productAvailabilityMode"
    
    private lazy var defaults: UserDefaults = {
        return UserDefaults.standard
    }()
    
    func lastSavedDate() -> Date? {
        return defaults.value(forKey: lastDateKey) as? Date
    }
    
    func saveDate(date: Date) {
        defaults.set(date, forKey: lastDateKey)
        defaults.synchronize()
    }
    
    func shouldShowPasscode() -> Bool {
        
        if let lastSavedDate = lastSavedDate() {
            
            let nowDate = Date()
            let interval = nowDate.timeIntervalSince(lastSavedDate)
            
            if (Int32(interval) > timeForShowPasscode) {
                return true
            } else {
                return false
            }
        }
        
        return true
    }
    
    func delete() {
        defaults.removeObject(forKey: lastDateKey)
        defaults.synchronize()
    }
    
    // -------------- Currency ------------
    func currentCurrency() -> String {
        
        if let deviceCurrency_ = self.deviceCurrency {
            return deviceCurrency_
        }
        
        return "$"
    }
    
    // uncorrect logic here
    lazy var deviceCurrency: String? = {
        
        if let deviceCurrency_ = self.getDeviceCurrency() {
            return deviceCurrency_
        }
        
        self.removeDeviceCurrency()
        
        let deviceCurrency = "$"
        self.saveDeviceCurrency(deviceCurrency)
        return deviceCurrency
    }()
    
    func saveDeviceCurrency(_ currency: String) -> Void {
        
        defaults.set(currency, forKey: currencyKey)
        defaults.synchronize()
    }
    
    func getDeviceCurrency() -> String? {
        return defaults.value(forKey: currencyKey) as? String
    }
    
    func removeDeviceCurrency() -> Void {
        
        defaults.removeObject(forKey: currencyKey)
        defaults.synchronize()
        self.deviceCurrency = nil
    }
    
    
    // -------------- MaximumFractionDigits ------------
    lazy var maximumFractionDigits: Int = {
        return self.getMaximumFractionDigits()
    }()
    
    func savemaximumFractionDigits(_ maximumFractionDigits: Int) -> Void {
        self.maximumFractionDigits = maximumFractionDigits
        defaults.set(maximumFractionDigits, forKey: maximumDigitsKey)
        defaults.synchronize()
    }
    
    func getMaximumFractionDigits() -> Int {
        return defaults.integer(forKey: maximumDigitsKey)
    }
    
    func removeMaximumFractionDigits() -> Void {
        defaults.removeObject(forKey: maximumDigitsKey)
        defaults.synchronize()
        self.maximumFractionDigits = 0
    }
    
    // -------------- IP Address ------------
    func currentIpAddress() -> String {
        if let deviceIpAddress_ = self.getDeviceIpAddress() {
            return deviceIpAddress_
        }
        //let dairIpAdress = "localhost"
        let dairIpAdress = "138.197.44.189"
        self.saveDeviceIpAddress(dairIpAdress)
        return dairIpAdress
    }
    
    func saveDeviceIpAddress(_ ipAddress: String) -> Void {
        defaults.set(ipAddress, forKey: ipAddressKey)
        defaults.synchronize()
    }
    
    func getDeviceIpAddress() -> String? {
        return defaults.value(forKey: ipAddressKey) as? String
    }
    
    func removeDeviceIpAddress() -> Void {
        defaults.removeObject(forKey: ipAddressKey)
        defaults.synchronize()
    }
    
    // -------------- Customers staff ------------
    func saveCustomersStaff(_ value: Bool) -> Void {
        defaults.set(value, forKey: customersStaffKey)
        defaults.synchronize()
    }
    
    func getCustomersStaff() -> Bool {
        if let boolValue_ = defaults.value(forKey: customersStaffKey) as? Bool {
            return boolValue_
        }
        return false
    }
    
    // -------------- Developer mode ------------
    func saveDeveloperMode(_ value: Bool) -> Void {
        defaults.set(value, forKey: developerModeKey)
        defaults.synchronize()
    }
    
    func getDeveloperMode() -> Bool {
        if let boolValue_ = defaults.value(forKey: developerModeKey) as? Bool {
            return boolValue_
        }
        return false
    }
    
    // -------------- AutoSyncronize mode ------------
    func saveAutoSyncronizeMode(_ value: Bool) -> Void {
        defaults.set(value, forKey: autoSyncronizeModeKey)
        defaults.synchronize()
    }
    
    func getAutoSyncronizeMode() -> Bool {
        return false
        //if let boolValue_ = defaults.value(forKey: autoSyncronizeModeKey) as? Bool {
        //    return boolValue_
        //}
        //return false
    }
    
    // -------------- Auto Clear mode ------------
    func saveAutoClearMode(_ value: Bool) -> Void {
        defaults.set(value, forKey: autoClearKey)
        defaults.synchronize()
    }
    
    func getAutoClearMode() -> Bool {
        if let boolValue_ = defaults.value(forKey: autoClearKey) as? Bool {
            return boolValue_
        }
        return false
    }
    
    // -------------- Should updload image ------------
    func saveShouldUseFrontCamera(_ value: Bool) -> Void {
        defaults.set(value, forKey: shouldFrontCameraKey)
        defaults.synchronize()
    }
    
    func getShouldUseFrontCamera() -> Bool {
        if let boolValue_ = defaults.value(forKey: shouldFrontCameraKey) as? Bool {
            return boolValue_
        }
        return false
    }
    
    // -------------- Default Customer ------------
    func saveDefaultCustomer(_ value: Bool) -> Void {
        defaults.set(value, forKey: useDefaultCustomerKey)
        defaults.synchronize()
    }
    
    func shouldUseDefaultCustomer() -> Bool {
        if let boolValue_ = defaults.value(forKey: useDefaultCustomerKey) as? Bool {
            return boolValue_
        }
        return false
    }

    // -------------- Default Laser Scaner ------------
    func savePasswordOnProduct(_ value: Bool) -> Void {
        defaults.set(value, forKey: passwordOnProductKey)
        defaults.synchronize()
    }
    
    func getPassordOnProduct() -> Bool {
        if let boolValue_ = defaults.value(forKey: passwordOnProductKey) as? Bool {
            return boolValue_
        }
        return false
    }
    
    // -------------- Default Customer ------------
    func saveCompanyName(_ companyName: String) -> Void {
        defaults.set(companyName, forKey: companyNameKey)
        defaults.synchronize()
    }
    
    func getCompanyName() -> String {
        if let k = defaults.value(forKey: companyNameKey) as? String {
            return k
        }
        return ""
    }
    
    // -------------- Product Uniq mode ------------
    func saveProductUniqMode(_ value: Bool) -> Void {
        defaults.set(value, forKey: productUniqModeKey)
        defaults.synchronize()
    }
    
    func getProductUniqMode() -> Bool {
        if let boolValue_ = defaults.value(forKey: productUniqModeKey) as? Bool {
            return boolValue_
        }
        return false
    }
    
    // -------------- Order draft mode ------------
    func saveOrderDraftMode(_ value: Bool) -> Void {
        defaults.set(value, forKey: orderDraftModeKey)
        defaults.synchronize()
    }
    
    func getOrderDraftMode() -> Bool {
        if let boolValue_ = defaults.value(forKey: orderDraftModeKey) as? Bool {
            return boolValue_
        }
        return false
    }
    
    // -------------- Availability mode ------------
    func saveProductAvailability(_ value: Bool) -> Void {
        defaults.set(value, forKey: productAvailabilityKey)
        defaults.synchronize()
    }
    
    func getProductAvailability() -> Bool {
        if let boolValue_ = defaults.value(forKey: productAvailabilityKey) as? Bool {
            return boolValue_
        }
        return false
    }
    
    func setDefaultSettings() {
        AppSettings.shared.removeDeviceCurrency()
        AppSettings.shared.saveDeviceCurrency("сом")
        
        AppSettings.shared.savemaximumFractionDigits(0)
        BKCurrencyTextField.setIdGen(0)
        Global.shared.priceFormatter.maximumFractionDigits = 0
        Global.shared.amountFormatter.maximumFractionDigits = 0
        
        AppSettings.shared.saveAutoClearMode(true)
        AppSettings.shared.saveDefaultCustomer(true)
        AppSettings.shared.saveDeveloperMode(false)
    }
}
