//
//  RPCNetworkManager.swift
//  Rentautomation
//
//  Created by kanybek on 1/4/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import grpcPod
import SVProgressHUD
import SwiftSignalKit
import CoreFoundation

final class ParkBenchTimer {
    
    let startTime:CFAbsoluteTime
    var endTime:CFAbsoluteTime?
    
    init() {
        startTime = CFAbsoluteTimeGetCurrent()
    }
    
    func stop() -> CFAbsoluteTime {
        endTime = CFAbsoluteTimeGetCurrent()
        return duration!
    }
    
    var duration:CFAbsoluteTime? {
        if let endTime = endTime {
            return endTime - startTime
        } else {
            return nil
        }
    }
}

final class RPCNetwork {
    
    //private let kHostAddress: String = "10.113.1.23:50051"
    //private let kHostAddress: String = "165.227.186.105:50051"
    private let kHostAddress: String = "45.55.72.172:50051"
    //private let kHostAddress: String = "\(AppSettings.shared.currentIpAddress()):50051"
    
    private let isUseSSL: Bool = true
    private var protoService: RentauService
    
    private var updateStreamCall: GRPCProtoCall?
    private var updateStreamWriter: GRXBufferedPipe?
    var rpcStatus: RpcConnectionType
    
    private var connectionTimer: ParkBenchTimer?
    
    fileprivate lazy var timer: TGTimer = {
        
        let block: ()->() = { [weak self] in
            self?.refreshAndRetryConnectToSocket()
        }
        
        // 30 seconds on timeout
        var socketTimer = TGTimer(timeout: 30, repeat: true, completion: block, queue: DispatchQueue.main)
        return socketTimer!
    }()
    
    static let shared: RPCNetwork = RPCNetwork()
    
    init () {
        if isUseSSL {
            // This tells the GRPC library to trust a certificate that it might not be able to validate.
            // Typically this would be used to trust a self-signed certificate.
            if let certificateFilePath = Bundle.main.path(forResource: "ssl", ofType: "crt") {
                GRPCCall.useTestCertsPath(certificateFilePath, testName: "localhost", forHost: kHostAddress)
            }
        } else {
            GRPCCall.useInsecureConnections(forHost: kHostAddress)
        }
        
        self.protoService = RentauService(host: kHostAddress)
        self.rpcStatus = .disconnected
    }
    
    // ----------------------------------------------------------------------------------------------------
    @objc func refreshAndRetryConnectToSocket() -> Void {
        self.stopStreamingConnection()
        self.openStreamingConnection()
    }
    
    func stopStreamingConnection() -> Void {
        if self.rpcStatus != .disconnected {
            print(" ******************* stopStreamingConnection() ******************* ")
            self.timer.invalidate()
            self.updateStreamWriter?.finishWithError(nil)
            //self.updateStreamWriter?.writesFinishedWithError(nil)
            self.rpcStatus = .disconnected // not correct logic, will not work in not reschable servrer.
            
            //
            // on disconnected internter not stopping request to server.
            // self.updateStreamWriter?.finishWithError(nil) works if server reached.
            //
        }
    }
    
    func invalidateTimer() {
        self.timer.invalidate()
    }
    
    func openStreamingConnection() {
        
        if !ReachabilityManager.shared.isConnectedToInternet() {
            return
        }
        
        if self.rpcStatus != .disconnected {
            return
        }
        
        print(" <================= openStreamingConnection() ===================> ")
        
        self.updateStreamWriter = GRXBufferedPipe()
        if let updateStreamWriter_ = self.updateStreamWriter {
            
            self.updateStreamCall = self.protoService.rpcToUpdateStream(withRequestsWriter: updateStreamWriter_, eventHandler: {[weak self] (boolValue: Bool, response: StickyNoteResponse?, error: Error?) in
                
                if let strongSelf = self {
                    
                    if boolValue {
                        print("Disconnected from server")
                        strongSelf.timer.start()
                        strongSelf.rpcStatus = .disconnected
                        PipesStore.shared.rpcConnectionStatusPipe.putNext(.disconnected)
                    } else {
                        print("Connected to server, took \(String(describing: strongSelf.connectionTimer?.stop())) seconds.")
                        strongSelf.timer.invalidate()
                        strongSelf.rpcStatus = .connected
                        PipesStore.shared.rpcConnectionStatusPipe.putNext(.connected)
                        
                        Synchronizer.shared.synchronizeAllNeededOperations {(response: Bool) in
                        }
                    }
                    
                    if let error_ = error {
                        print("error ==> \(error_.localizedDescription)")
                    } else if let responseMessage = response?.message {
                        print("response_.message ==> \(responseMessage)")
                    }
                }
            })
            
            if let updateStreamCall_ = self.updateStreamCall {
                
                self.rpcStatus = .connecting
                PipesStore.shared.rpcConnectionStatusPipe.putNext(.connecting)
                
                self.connectionTimer = ParkBenchTimer()
                
                // start stream call
                RPCNetwork.registerKeys(updateStreamCall_)
                updateStreamCall_.start()
                
                if let updateStreamWriter_ = self.updateStreamWriter {
                    let request = StickyNoteRequest()
                    request.message = "hello rentautomation"
                    print("updateStreamWriter writeValue request")
                    updateStreamWriter_.writeValue(request)
                }
            }
        }
    }

    // ----------------------------------------------------------------------------------------------------    
    // MARK: REAL WORLD
    static private func registerKeys(_ call: GRPCProtoCall) -> Void {
        
        if let token = KeychainManager.shared.getToken() {
            // authenticate using an API key obtained from the Google Cloud Console
            call.requestHeaders.setObject(NSString(string:token),
                                          forKey:NSString(string:"authorization"))
            
            // if the API key has a bundle ID restriction, specify the bundle ID like this
            //call.requestHeaders.setObject(NSString(string:APP_ID),
            //                              forKey:NSString(string:"appid"))
            
            //print("HEADERS:\(call.requestHeaders)")
        }
    }
    
    //KeychainManager.shared
    //MARK: SignIn/SignUp
    // ------------------------------  ---------------------------------- //
    func getUserRPC() -> Signal<UserRequest, Error> {
        
        let createUserRpcSignal = Signal<UserRequest, Error> { subscriber in
            
            let call: GRPCProtoCall = self.protoService.rpcToGetUser(withRequest: GPBEmpty(), handler: { (userReqResp: UserRequest?, error: Error?) in
                
                if let error_ = error {
                    subscriber.putError(error_)
                    subscriber.putCompletion()
                }
                
                if let userReqResp_ = userReqResp {
                    subscriber.putNext(userReqResp_)
                    subscriber.putCompletion()
                }
            })
            
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return createUserRpcSignal
    }
    
    func signUpRPC(sUpReq: SignUpRequest) -> Signal<SignUpRequest, Error> {
        
        let createUserRpcSignal = Signal<SignUpRequest, Error> { subscriber in
            
            let call: GRPCProtoCall = self.protoService.rpcToSignUp(with: sUpReq, handler: { (sUpReqResp: SignUpRequest?, error: Error?) in
                
                    if let error_ = error {
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                    
                    if let sUpReqResp_ = sUpReqResp {
                        if sUpReqResp_.token.characters.count > 0 {
                            subscriber.putNext(sUpReqResp_)
                            subscriber.putCompletion()
                        } else {
                            
                        }
                    }
            })
            
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return createUserRpcSignal
    }
    
    func signInRPC(email: String, password: String) -> Signal<SignInRequest, Error> {
        
        print("starting signInRPC")
        
        let signInReq = SignInRequest()
        signInReq.email = email
        signInReq.password = password
        
        let signInRpcSignal = Signal<SignInRequest, Error> { subscriber in
            
            let call: GRPCProtoCall = self.protoService.rpcToSignIn(with: signInReq, handler: { (signInReq: SignInRequest?, error: Error?) in
                
                if let error_ = error {
                    print("Error happened signInRPC => \(error_.localizedDescription)")
                    subscriber.putError(error_)
                    subscriber.putCompletion()
                }
                
                if let signInReq_ = signInReq {
                    if signInReq_.token.characters.count > 0 {
                        subscriber.putNext(signInReq_)
                        subscriber.putCompletion()
                    }
                }
            })
            
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return signInRpcSignal
    }
    
    func emailExsistsRPC(email: String) -> Signal<SignInRequest, Error> {
        
        print("starting emailExsistsRPC")
        
        let signInReq = SignInRequest()
        signInReq.email = email
        signInReq.password = ""
        signInReq.token = ""
        
        let signInRpcSignal = Signal<SignInRequest, Error> { subscriber in
            
            let call: GRPCProtoCall = self.protoService.rpcToIsEmailRegistered(with: signInReq, handler: { (signInReq: SignInRequest?, error: Error?) in
                
                if let error_ = error {
                    print("Error happened emailExsistsRPC => \(error_.localizedDescription)")
                    subscriber.putError(error_)
                    subscriber.putCompletion()
                }
                
                if let signInReq_ = signInReq {
                    subscriber.putNext(signInReq_)
                    subscriber.putCompletion()
                }
            })
            
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return signInRpcSignal
    }
    
    func deleteAllDataFromServersRPC(email: String, password: String) -> Signal<SignInRequest, Error> {
        
        let signInReq = SignInRequest()
        signInReq.email = email
        signInReq.password = password
        
        let signInRpcSignal = Signal<SignInRequest, Error> { subscriber in
            
            let call: GRPCProtoCall = self.protoService.rpcToDeleteAllForCompany(with: signInReq, handler: { (signInReq: SignInRequest?, error: Error?) in
                
                if let error_ = error {
                    print("Error happened signInRPC => \(error_.localizedDescription)")
                    subscriber.putError(error_)
                    subscriber.putCompletion()
                }
                
                if let signInReq_ = signInReq {
                    subscriber.putNext(signInReq_)
                    subscriber.putCompletion()
                }
            })
            
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return signInRpcSignal
    }
    
    
    // MARK: Products
    // ------------------------------ REAL WORLD EXAMPLES ---------------------------------- //
    func createProductRPC(product: Product, orderDetail: OrderDetail) -> Signal<(UInt64, UInt64), Error> {
        
        let prodReq = ModelToProto.productRequestFrom(product)
        let orderDetailReq = ModelToProto.orderDetailRequestFrom(orderDetail)
        
        let createPr = CreateProductRequest()
        createPr.product = prodReq
        createPr.orderDetail = orderDetailReq
        
        let createProductRpcSignal = Signal<(UInt64, UInt64), Error> { subscriber in
            
            print(" ------------  ")
            print("starting createProductRPC queue:\(RPCNetwork.currentQueueName())")
            
            let call: GRPCProtoCall = self.protoService.rpcToCreateProduct(with: createPr) { (createPrResponse: CreateProductRequest?, error: Error?) in
                
                Synchronizer.syncQueue.async {
                    
                    print("response createProductRPC queue:\(RPCNetwork.currentQueueName())")
                    
                    if let error_ = error {
                        print("Error happened createProductRPC => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                    
                    if let createPrResponse_ = createPrResponse {
                        
                        print("createPrResponse_.product.productId => \(createPrResponse_.product.productId)")
                        print("createPrResponse_.orderDetail.orderDetailId => \(createPrResponse_.orderDetail.orderDetailId)")
                        
                        let productId = createPrResponse_.product.productId
                        let orderDetailId = createPrResponse_.orderDetail.orderDetailId
                        
                        if (productId > 0) && (orderDetailId > 0) {
                            subscriber.putNext((productId, orderDetailId))
                            subscriber.putCompletion()
                        }
                    }
                }
            }
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return createProductRpcSignal
    }
    
    func updateProductRPC(product: Product, orderDetail: OrderDetail) -> Signal<UInt64, Error>  {
        
        let prodReq = ModelToProto.productRequestFrom(product)
        let orderDetailReq = ModelToProto.orderDetailRequestFrom(orderDetail)
        
        let createPr = CreateProductRequest()
        createPr.product = prodReq
        createPr.orderDetail = orderDetailReq
        
        let updateProductRpcSignal = Signal<UInt64, Error> { subscriber in
            
            print(" ------------  ")
            print("starting updateProductRPC queue:\(RPCNetwork.currentQueueName())")
                 
            
            let call: GRPCProtoCall = self.protoService.rpcToUpdateProduct(with: createPr) { (createPrResponse: CreateProductRequest?, error: Error?) in
                
                Synchronizer.syncQueue.async {
                    
                    print("response updateProductRPC queue:\(RPCNetwork.currentQueueName())")
                    
                    if let error_ = error {
                         
                        print("Error happened updateProductRPC => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                    
                    if let createPrResponse_ = createPrResponse {
                        
                        print("createPrResponse_.product.productId => \(createPrResponse_.product.productId)")
                        print("createPrResponse_.orderDetail.orderDetailId => \(createPrResponse_.orderDetail.orderDetailId)")
                        
                        let orderDetailId = createPrResponse_.orderDetail.orderDetailId
                        
                        if orderDetailId > 0 {
                            subscriber.putNext(orderDetailId)
                            subscriber.putCompletion()
                        }
                    }
                }
            }
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return updateProductRpcSignal
    }
    
    // --------------
    //
    func allProductsRPC() -> Signal<[CreateProductRequest], Error>  {
        
        let productFilter = ProductFilter()
        productFilter.productKeyword = ""
        productFilter.productUpdatedAt = UInt64(0 * 1000)
        
        let allProductsRpcSignal = Signal<[CreateProductRequest], Error> { subscriber in
            
            print("-----")
            print("starting rpcToAllProductsForInitial queue:\(RPCNetwork.currentQueueName())")
            
            let call: GRPCProtoCall = self.protoService.rpcToAllProductsForInitial(withRequest: productFilter, handler: { (allPr: AllProductsResponse?, error: Error?) in
                
                Synchronizer.syncQueue.async {
                    print("response rpcToAllProductsForInitial queue:\(RPCNetwork.currentQueueName())")
                    
                    if let allProducts_ = allPr {
                        
                        var allProducts: [CreateProductRequest] = []
                        for someObj in allProducts_.productRequestsArray {
                            
                            if let createPrReq = someObj as? CreateProductRequest {
                                allProducts.append(createPrReq)
                            }
                        }
                        subscriber.putNext(allProducts)
                        subscriber.putCompletion()
                    }
                    
                    if let error_ = error {
                        print("Error happened allProductsRPC => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                }
            })
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return allProductsRpcSignal
    }
    
    func checkProductsForUpdateRPC(productUpdatedAt: Date) -> Signal<[CreateProductRequest], Error>  {
        
        let productFilter = ProductFilter()
        productFilter.productKeyword = ""
        productFilter.productUpdatedAt = UInt64(productUpdatedAt.timeIntervalSince1970 * 1000)
        
        let allProductsRpcSignal = Signal<[CreateProductRequest], Error> { subscriber in
            
            print("-----")
            print("starting checkProductsForUpdateRPC queue:\(RPCNetwork.currentQueueName())")
            let call: GRPCProtoCall = self.protoService.rpcToCheckProductsForUpdate(withRequest: productFilter, handler: { (allPr: AllProductsResponse?, error: Error?) in
                
                Synchronizer.syncQueue.async {
                    print("response checkProductsForUpdateRPC queue:\(RPCNetwork.currentQueueName())")
                    
                    if let allProducts_ = allPr {
                        var allProducts: [CreateProductRequest] = []
                        
                        for someObj in allProducts_.productRequestsArray {
                            if let createPrReq = someObj as? CreateProductRequest {
                                allProducts.append(createPrReq)
                            }
                        }
                        print("Received updated product count: \(allProducts.count)")
                        subscriber.putNext(allProducts)
                        subscriber.putCompletion()
                    }
                    
                    if let error_ = error {
                        print("Error happened checkProductsForUpdateRPC => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                }
            })
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return allProductsRpcSignal
    }
    
    //MARK: Order_Details
    func checkOrderDetailsForUpdateRPC(date: Date, limit: UInt16) -> Signal<[OrderDetailRequest], Error>  {
        
        let orderDetFilter = OrderDetailFilter()
        orderDetFilter.orderDetailKeyword = ""
        orderDetFilter.orderDetailDate = UInt64(date.timeIntervalSince1970 * 1000)
        orderDetFilter.limit = UInt32(limit)
        orderDetFilter.productId = 0
        
        let orderDetailsRpcSignal = Signal<[OrderDetailRequest], Error> { subscriber in
            
            print("-----")
            print("starting rpcToAllOrderDetailsForRecent queue:\(RPCNetwork.currentQueueName())")
            
            let call: GRPCProtoCall = self.protoService.rpcToCheckOrderDetailsForUpdate(withRequest: orderDetFilter, handler: { (resp: AllOrderDetailResponse?, error: Error?) in
                Synchronizer.syncQueue.async {
                    print("response rpcToAllOrderDetailsForRecent queue:\(RPCNetwork.currentQueueName())")
                    
                    if let orderDetailResp_ = resp {
                        
                        var orderDetails: [OrderDetailRequest] = []
                        for someObj in orderDetailResp_.orderDetailRequestsArray {
                            if let orderDetailReq = someObj as? OrderDetailRequest {
                                orderDetails.append(orderDetailReq)
                            }
                        }
                        subscriber.putNext(orderDetails)
                        subscriber.putCompletion()
                    }
                    if let error_ = error {
                        print("Error happened allProductsRPC => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                }
            })
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return orderDetailsRpcSignal
    }
    
    func getOrderDetailsForFilterRPC(productId: UInt64, date: Date, limit: UInt16) -> Signal<[OrderDetailRequest], Error>  {
        
        let orderDetFilter = OrderDetailFilter()
        orderDetFilter.orderDetailKeyword = ""
        orderDetFilter.orderDetailDate = UInt64(date.timeIntervalSince1970 * 1000)
        orderDetFilter.limit = UInt32(limit)
        orderDetFilter.productId = productId
        
        let orderDetailsRpcSignal = Signal<[OrderDetailRequest], Error> { subscriber in
            
            print("-----")
            print("starting rpcToAllOrderDetails queue:\(RPCNetwork.currentQueueName())")
            
            let call: GRPCProtoCall = self.protoService.rpcToAllOrderDetails(withRequest: orderDetFilter, handler: { (resp: AllOrderDetailResponse?, error: Error?) in
                Synchronizer.syncQueue.async {
                    print("response rpcToAllOrderDetails queue:\(RPCNetwork.currentQueueName())")
                    
                    if let orderDetailResp_ = resp {
                        
                        var orderDetails: [OrderDetailRequest] = []
                        for someObj in orderDetailResp_.orderDetailRequestsArray {
                            if let orderDetailReq = someObj as? OrderDetailRequest {
                                orderDetails.append(orderDetailReq)
                            }
                        }
                        subscriber.putNext(orderDetails)
                        subscriber.putCompletion()
                    }
                    if let error_ = error {
                        print("Error happened allProductsRPC => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                }
            })
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return orderDetailsRpcSignal
    }
    
    
    //MARK: Customers
    func createCustomerRPC(customer: Customer, transaction: Transaction, account: Account) -> Signal<(UInt64, UInt64, UInt64), Error> {
        
        let custReq = ModelToProto.customerRequestFrom(customer)
        let transReq = ModelToProto.transactionRequestFrom(transaction)
        
        let accReq = ModelToProto.accountRequestFrom(account)
        accReq.balance = 0.0
        
        let createCustReq = CreateCustomerRequest()
        createCustReq.customer = custReq
        createCustReq.transaction = transReq
        createCustReq.account = accReq
        
        let createCustomerRpcSignal = Signal<(UInt64, UInt64, UInt64), Error> { subscriber in
            
            print("-----")
            print("starting createCustomerRPC queue:\(RPCNetwork.currentQueueName())")
            
            let call: GRPCProtoCall = self.protoService.rpcToCreateCustomer(with: createCustReq, handler: { (createCustResponse: CreateCustomerRequest?, error: Error?) in
                
                Synchronizer.syncQueue.async {
                    print("response createCustomerRPC queue:\(RPCNetwork.currentQueueName())")
                    
                    if let error_ = error {
                        print("Error happened createCustomerRPC => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                    
                    if let createCustResponse_ = createCustResponse {
                        
                        print("createCustResponse_.customer.customerId => \(createCustResponse_.customer.customerId)")
                        print("createCustResponse_.transaction.transactionId => \(createCustResponse_.transaction.transactionId)")
                        print("createCustResponse_.account.accountId => \(createCustResponse_.account.accountId)")
                        
                        let customerId = createCustResponse_.customer.customerId
                        let transactionId = createCustResponse_.transaction.transactionId
                        let accountId = createCustResponse_.account.accountId
                        
                        if (customerId > 0) && (transactionId > 0) && (accountId > 0) {
                            subscriber.putNext((customerId, transactionId, accountId))
                            subscriber.putCompletion()
                        }
                    }
                }
            })
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return createCustomerRpcSignal
    }
    
    
    func updateCustomerRPC(customer: Customer) -> Signal<Bool, Error> {
        
        let custReq = ModelToProto.customerRequestFrom(customer)
        
        let updateCustomerRpcSignal = Signal<Bool, Error> { subscriber in
            
            print("--------")
            print("starting updateCustomerRPC")
            
            let call: GRPCProtoCall = self.protoService.rpcToUpdateCustomer(with: custReq, handler: { (customerReqResp:  CustomerRequest?, error: Error?) in
                Synchronizer.syncQueue.async {
                    print("response updateCustomerRPC")
                    
                    if let error_ = error {
                        print("Error happened updateCustomerRPC => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                    if let customerReq_ = customerReqResp {
                        
                        print("customerReq_.customerId => \(customerReq_.customerId)")
                        let customerId = customerReq_.customerId
                        
                        if customerId > 0 {
                            subscriber.putNext(true)
                            subscriber.putCompletion()
                        }
                    }
                }
            })
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return updateCustomerRpcSignal
    }
    
    func updateCustomerBalanceRPC(transaction: Transaction, account: Account) -> Signal<UInt64, Error> {
        
        let transReq = ModelToProto.transactionRequestFrom(transaction)
        let accReq = ModelToProto.accountRequestFrom(account)
        
        let createCustReq = CreateCustomerRequest()
        createCustReq.transaction = transReq
        createCustReq.account = accReq
        
        let updateCustomerBalanceRpcSignal = Signal<UInt64, Error> { subscriber in
            
            print("----------")
            print("starting updateCustomerBalanceRPC")
                 
            
            let call: GRPCProtoCall = self.protoService.rpcToUpdateCustomerBalance(with: createCustReq, handler: { (updCustBalanceResponse: CreateCustomerRequest?, error: Error?) in
                
                Synchronizer.syncQueue.async {
                    print("response updateCustomerBalanceRPC")
                    
                    if let error_ = error {
                        print("Error happened updateCustomerBalanceRPC => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                    
                    if let updCustBalanceResponse_ = updCustBalanceResponse {
                        
                        print("updCustBalanceResponse_.transaction.transactionId => \(updCustBalanceResponse_.transaction.transactionId)")
                        let transactionId = updCustBalanceResponse_.transaction.transactionId
                        
                        if transactionId > 0 {
                            subscriber.putNext(transactionId)
                            subscriber.putCompletion()
                        }
                    }
                }
            })
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return updateCustomerBalanceRpcSignal
    }
    
    func allCustomersForInitialStateRPC() -> Signal<[CreateCustomerRequest], Error> {
        
        let allCustomersForInitialStateRpcSignal = Signal<[CreateCustomerRequest], Error> { subscriber in
            
            print("----------")
            print("starting allCustomersForInitialState")
            
            let filter = CustomerFilter()
            filter.customerKeyword = ""
            filter.customerUpdatedAt = UInt64(Date().timeIntervalSince1970 * 1000)
            
            let call: GRPCProtoCall = self.protoService.rpcToAllCustomersForInitial(withRequest: filter, handler: { (allCust: AllCustomersResponse?, error: Error?) in
                Synchronizer.syncQueue.async {
                    print("response allCustomersForInitialState")
                    
                    if let error_ = error {
                        print("Error happened allCustomersForInitialState => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                    
                    if let allCust_ = allCust {
                        
                        var allCustomers: [CreateCustomerRequest] = []
                        
                        for someObj in allCust_.customerRequestsArray {
                            if let createCustReq = someObj as? CreateCustomerRequest {
                                allCustomers.append(createCustReq)
                            }
                        }
                        
                        subscriber.putNext(allCustomers)
                        subscriber.putCompletion()
                    }
                }
            })
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return allCustomersForInitialStateRpcSignal
    }
    
    func checkCustomersForUpdateRPC(customerUpdatedAt: Date) -> Signal<[CreateCustomerRequest], Error> {
        
        let allCustomersForInitialStateRpcSignal = Signal<[CreateCustomerRequest], Error> { subscriber in
            
            print("starting checkCustomersForUpdate")
            
            let filter = CustomerFilter()
            filter.customerKeyword = ""
            filter.customerUpdatedAt = UInt64(customerUpdatedAt.timeIntervalSince1970 * 1000)
            
            let call: GRPCProtoCall = self.protoService.rpcToCheckCustomersForUpdate(withRequest: filter, handler: { (allCust: AllCustomersResponse?, error: Error?) in
                
                Synchronizer.syncQueue.async {
                    print("response checkCustomersForUpdate")
                    
                    if let error_ = error {
                        print("Error happened allCustomersForInitialState => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                    
                    if let allCust_ = allCust {
                        
                        var allCustomers: [CreateCustomerRequest] = []
                        
                        for someObj in allCust_.customerRequestsArray {
                            if let createCustReq = someObj as? CreateCustomerRequest {
                                allCustomers.append(createCustReq)
                            }
                        }
                        
                        subscriber.putNext(allCustomers)
                        subscriber.putCompletion()
                    }
                }
            })
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return allCustomersForInitialStateRpcSignal
    }
    
    
    //MARK: Supplier
    func createSupplierRPC(supplier: Supplier, transaction: Transaction, account: Account) -> Signal<(UInt64, UInt64, UInt64), Error> {
        
        let suppReq = ModelToProto.supplierRequestFrom(supplier)
        let transReq = ModelToProto.transactionRequestFrom(transaction)
        let accReq = ModelToProto.accountRequestFrom(account)
        
        let createSuppReq = CreateSupplierRequest()
        createSuppReq.supplier = suppReq
        createSuppReq.transaction = transReq
        createSuppReq.account = accReq
        
        let createSupplierRpcSignal = Signal<(UInt64, UInt64, UInt64), Error> { subscriber in
            
            print("----------")
            print("starting createSupplierRPC")
            
            let call: GRPCProtoCall = self.protoService.rpcToCreateSupplier(with: createSuppReq, handler: { (createSuppReqRes: CreateSupplierRequest?, error: Error?) in
                
                Synchronizer.syncQueue.async {
                    
                    print("response createSupplierRPC")
                    
                    if let error_ = error {
                        print("Error happened createSupplierRPC => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                    
                    if let createSuppReq_ = createSuppReqRes {
                        
                        print("createSuppReq_.supplier.supplierId => \(createSuppReq_.supplier.supplierId)")
                        print("createSuppReq_.transaction.transactionId => \(createSuppReq_.transaction.transactionId)")
                        print("createSuppReq_.account.accountId => \(createSuppReq_.account.accountId)")
                        
                        let supplierId = createSuppReq_.supplier.supplierId
                        let transactionId = createSuppReq_.transaction.transactionId
                        let accountId = createSuppReq_.account.accountId
                        
                        if (supplierId > 0) && (transactionId > 0) && (accountId > 0) {
                            subscriber.putNext((supplierId, transactionId, accountId))
                            subscriber.putCompletion()
                        }
                    }
                }
            })
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return createSupplierRpcSignal
    }
    
    func updateSupplierRPC(supplier: Supplier) -> Signal<Bool, Error> {
        
        let suppReq = ModelToProto.supplierRequestFrom(supplier)
        
        let updateCustomerRpcSignal = Signal<Bool, Error> { subscriber in
            
            print("---------")
            print("starting updateSupplierRPC")
            
            let call: GRPCProtoCall = self.protoService.rpcToUpdateSupplier(with: suppReq, handler: { (suppReqResp: SupplierRequest?, error: Error?) in
                
                Synchronizer.syncQueue.async {
                    print("response updateSupplierRPC")
                    
                    if let error_ = error {
                        print("Error happened updateSupplierRPC => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                    
                    if let supplierReq_ = suppReqResp {
                        
                        print("supplierReq_.supplierId => \(supplierReq_.supplierId)")
                        let supplierId = supplierReq_.supplierId
                        
                        if supplierId > 0 {
                            subscriber.putNext(true)
                            subscriber.putCompletion()
                        }
                    }
                }
            })
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return updateCustomerRpcSignal
    }
    
    func updateSupplierBalanceRPC(transaction: Transaction, account: Account) -> Signal<UInt64, Error> {
        
        let transReq = ModelToProto.transactionRequestFrom(transaction)
        let accReq = ModelToProto.accountRequestFrom(account)
        
        let createSuppReq = CreateSupplierRequest()
        createSuppReq.transaction = transReq
        createSuppReq.account = accReq
        
        let updateSupplierBalanceRpcSignal = Signal<UInt64, Error> { subscriber in
            
            print("--------")
            print("starting updateSupplierBalanceRPC")
                 
            
            let call: GRPCProtoCall = self.protoService.rpcToUpdateSupplierBalance(with: createSuppReq, handler: { (updSuppBalanceResponse: CreateSupplierRequest?, error: Error?) in
                
                Synchronizer.syncQueue.async {
                    print("response updateSupplierBalanceRPC")
                    
                    if let error_ = error {
                        print("Error happened updateSupplierBalanceRPC => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                    
                    if let updSuppBalanceReq_ = updSuppBalanceResponse {
                        
                        print("updSuppBalanceReq_.transaction.transactionId => \(updSuppBalanceReq_.transaction.transactionId)")
                        let transactionId = updSuppBalanceReq_.transaction.transactionId
                        
                        if transactionId > 0 {
                            subscriber.putNext(transactionId)
                            subscriber.putCompletion()
                        }
                    }
                }
            })
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return updateSupplierBalanceRpcSignal
    }
    
    func allSuppliersForInitialStateRPC() -> Signal<[CreateSupplierRequest], Error> {
        
        let allSuppliersForInitialStateRpcSignal = Signal<[CreateSupplierRequest], Error> { subscriber in
            
            print("----------")
            print("starting allSuppliersForInitialState")
            
            let filter = SupplierFilter()
            filter.supplierKeyword = ""
            filter.supplierUpdatedAt = UInt64(0 * 1000)
            
            let call: GRPCProtoCall = self.protoService.rpcToAllSuppliersForInitial(withRequest: filter, handler: { (allSupp: AllSuppliersResponse?, error: Error?) in
                Synchronizer.syncQueue.async {
                    print("response allSuppliersForInitialState")
                    
                    if let error_ = error {
                        print("Error happened allSuppliersForInitialState => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                    
                    if let allSupp_ = allSupp {
                        
                        var allSuppliers: [CreateSupplierRequest] = []
                        for someObj in allSupp_.supplierRequestsArray {
                            if let createSuppReq = someObj as? CreateSupplierRequest {
                                allSuppliers.append(createSuppReq)
                            }
                        }
                        
                        subscriber.putNext(allSuppliers)
                        subscriber.putCompletion()
                    }
                }
            })
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return allSuppliersForInitialStateRpcSignal
    }
    
    func checkSuppliersForUpdateRPC(supplierUpdatedAt: Date) -> Signal<[CreateSupplierRequest], Error> {
        
        let allSuppliersForInitialStateRpcSignal = Signal<[CreateSupplierRequest], Error> { subscriber in
            
            print("----------")
            print("starting checkSuppliersForUpdateRPC")
            
            let filter = SupplierFilter()
            filter.supplierKeyword = ""
            filter.supplierUpdatedAt = UInt64(supplierUpdatedAt.timeIntervalSince1970 * 1000)
            
            let call: GRPCProtoCall = self.protoService.rpcToCheckSuppliersForUpdate(withRequest: filter, handler: { (allSupp: AllSuppliersResponse?, error: Error?) in
                Synchronizer.syncQueue.async {
                    print("response checkSuppliersForUpdateRPC")
                    
                    if let error_ = error {
                        print("Error happened checkSuppliersForUpdateRPC => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                    
                    if let allSupp_ = allSupp {
                        
                        var allSuppliers: [CreateSupplierRequest] = []
                        for someObj in allSupp_.supplierRequestsArray {
                            if let createSuppReq = someObj as? CreateSupplierRequest {
                                allSuppliers.append(createSuppReq)
                            }
                        }
                        print("Received updated supplier count: \(allSuppliers.count)")
                        subscriber.putNext(allSuppliers)
                        subscriber.putCompletion()
                    }
                }
            })
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return allSuppliersForInitialStateRpcSignal
    }
    
    //MARK: Transaction
    func checkTransactionsForUpdateRPC(date: Date, limit: UInt16) -> Signal<[TransactionRequest], Error> {
        
        let allTransactionsForFilterRpcSignal = Signal<[TransactionRequest], Error> { subscriber in
            
            print("starting checkTransactionsForUpdateRPC")
            
            let filter = TransactionFilter()
            filter.transactionKeyword = ""
            filter.customerId = 0
            filter.supplierId = 0
            filter.transactionDate = UInt64(date.timeIntervalSince1970 * 1000)
            filter.limit = UInt32(limit)
            
            let call: GRPCProtoCall = self.protoService.rpcToCheckTransactionsForUpdate(withRequest: filter, handler: { (allTrResp: AllTransactionResponse?, error: Error?) in
                Synchronizer.syncQueue.async {
                    print("response checkTransactionsForUpdateRPC")
                    
                    if let error_ = error {
                        print("Error happened checkTransactionsForUpdateRPC => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                    
                    if let allTrResp_ = allTrResp {
                        
                        var allTransactions: [TransactionRequest] = []
                        for someObj in allTrResp_.transactionRequestsArray {
                            if let transReq = someObj as? TransactionRequest {
                                allTransactions.append(transReq)
                            }
                        }
                        print("Received updated Transaction count: \(allTransactions.count)")
                        subscriber.putNext(allTransactions)
                        subscriber.putCompletion()
                    }
                }
            })
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return allTransactionsForFilterRpcSignal
    }
    
    func updateTransactionRPC(transaction_: Transaction) -> Signal<Transaction, Error> {
        let updateTransactionRpcSignal = Signal<Transaction, Error> { subscriber in
            
            let transaction = Database.shared.transactionBy(transactionId: transaction_.transactionId)!
            let transReq = ModelToProto.transactionRequestFrom(transaction)
            
            let call: GRPCProtoCall = self.protoService.rpcToUpdateTransaction(with: transReq, handler: { (resp: TransactionRequest?, err: Error?) in
                Synchronizer.syncQueue.async {
                    if let error_ = err {
                        print("Error happened updateOrderRPC => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                    
                    if let transactionReq = resp {
                        let transaction = ProtoToModel.transactionFrom(transactionReq)
                        subscriber.putNext(transaction)
                        subscriber.putCompletion()
                    }
                }
            })
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return updateTransactionRpcSignal
    }
    
    func allTransactionsForFilterRPC(customerId: UInt64, supplierId: UInt64, date: Date, limit: UInt16) -> Signal<[TransactionRequest], Error> {
        
        let allTransactionsForFilterRpcSignal = Signal<[TransactionRequest], Error> { subscriber in
            
            print("----------")
            print("starting allTransactionsForFilter")
            
            let filter = TransactionFilter()
            filter.transactionKeyword = ""
            filter.transactionDate = UInt64(date.timeIntervalSince1970 * 1000)
            filter.limit = UInt32(limit)
            filter.customerId = customerId
            filter.supplierId = supplierId
            
            let call: GRPCProtoCall = self.protoService.rpcToAllTransactionsForInitial(withRequest: filter, handler: { (allTrResp: AllTransactionResponse?, error: Error?) in
                Synchronizer.syncQueue.async {
                    print("response allTransactionsForFilter")
                    
                    if let error_ = error {
                        print("Error happened allTransactionsForFilter => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                    
                    if let allTrResp_ = allTrResp {
                        
                        var allTransactions: [TransactionRequest] = []
                        for someObj in allTrResp_.transactionRequestsArray {
                            if let transReq = someObj as? TransactionRequest {
                                allTransactions.append(transReq)
                            }
                        }
                        subscriber.putNext(allTransactions)
                        subscriber.putCompletion()
                    }
                }
            })
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return allTransactionsForFilterRpcSignal
    }
    
    
    //MARK: Staff
    func createUserRPC(user: User) -> Signal<UInt64, Error> {
        
        let userReq = ModelToProto.userRequestFrom(user)
        
        let createUserRpcSignal = Signal<UInt64, Error> { subscriber in
            
            print("--------")
            print("starting createStaffRPC")
            
            let call: GRPCProtoCall = self.protoService.rpcToCreateUser(with: userReq, handler: { (userReqRes: UserRequest?, error: Error?) in
                
                Synchronizer.syncQueue.async {
                    print("response createStaffRPC")
                    
                    if let error_ = error {
                        print("Error happened createStaffRPC => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                    
                    if let userReqRes_ = userReqRes {
                        print("staffReqRes_.supplierId => \(userReqRes_.userId)")
                        let userId = userReqRes_.userId
                        
                        if userId > 0 {
                            subscriber.putNext(userId)
                            subscriber.putCompletion()
                        }
                    }
                }
            })
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return createUserRpcSignal
    }
    
    func updateUserRPC(user: User) -> Signal<Bool, Error> {
        
        let userReq = ModelToProto.userRequestFrom(user)
        
        let updateStaffRpcSignal = Signal<Bool, Error> { subscriber in
            
            print("--------")
            print("starting updateStaffRPC")
            
            let call: GRPCProtoCall = self.protoService.rpcToUpdateUser(with: userReq, handler: { (userReqRes: UserRequest?, error: Error?) in
                
                print("response updateStaffRPC")
                Synchronizer.syncQueue.async {
                    
                    if let error_ = error {
                         
                        print("Error happened updateStaffRPC => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                    
                    if let userReqRes_ = userReqRes {
                        
                        print("staffUpdRespose_.staffId => \(userReqRes_.userId)")
                        let userId = userReqRes_.userId
                        
                        if userId > 0 {
                            subscriber.putNext(true)
                            subscriber.putCompletion()
                        }
                    }
                }
            })
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return updateStaffRpcSignal
    }

    func allUsersRPC() -> Signal<[UserRequest], Error>  {
        
        let userFilter = UserFilter()
        userFilter.userKeyword = ""
        userFilter.userUpdatedAt = UInt64(0 * 1000)
        
        let allCategoriesRpcSignal = Signal<[UserRequest], Error> { subscriber in
            
            print("-----")
            print("starting rpcToAllStaffForInitial queue:\(RPCNetwork.currentQueueName())")
            
            let call: GRPCProtoCall = self.protoService.rpcToAllUsersForInitial(withRequest: userFilter, handler: { (allUsersResp: AllUserResponse?, error: Error?) in
                
                Synchronizer.syncQueue.async {
                    print("response rpcToAllStaffForInitial queue:\(RPCNetwork.currentQueueName())")
                    
                    if let allUsersResp_ = allUsersResp {
                        
                        var allUsersReqs: [UserRequest] = []
                        for someObj in allUsersResp_.userRequestsArray {
                            if let userReq = someObj as? UserRequest {
                                allUsersReqs.append(userReq)
                            }
                        }
                        subscriber.putNext(allUsersReqs)
                        subscriber.putCompletion()
                    }
                    
                    if let error_ = error {
                        print("Error happened rpcToAllStaffForInitial => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                }
            })
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return allCategoriesRpcSignal
    }
    
    func checkUsersForUpdateRPC(userUpdatedAt: Date) -> Signal<[UserRequest], Error>  {
        
        let userFilter = UserFilter()
        userFilter.userKeyword = ""
        userFilter.userUpdatedAt = UInt64(userUpdatedAt.timeIntervalSince1970 * 1000)
        
        let allUpdatedUsersRpcSignal = Signal<[UserRequest], Error> { subscriber in
            
            print("-----")
            print("starting checkStaffForUpdateRPC queue:\(RPCNetwork.currentQueueName())")
            
            let call: GRPCProtoCall = self.protoService.rpcToCheckUsersForUpdate(withRequest: userFilter, handler: { (allUsersResp: AllUserResponse?, error: Error?) in
                
                Synchronizer.syncQueue.async {
                    print("response checkStaffForUpdateRPC queue:\(RPCNetwork.currentQueueName())")
                    
                    if let allUsersResp_ = allUsersResp {
                        
                        var allUsersReqs: [UserRequest] = []
                        for someObj in allUsersResp_.userRequestsArray {
                            if let userReq = someObj as? UserRequest {
                                allUsersReqs.append(userReq)
                            }
                        }
                        print("Received updated staff count: \(allUsersReqs.count)")
                        subscriber.putNext(allUsersReqs)
                        subscriber.putCompletion()
                    }
                    
                    if let error_ = error {
                        print("Error happened checkStaffForUpdateRPC => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                }
            })
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return allUpdatedUsersRpcSignal
    }
    
    //MARK: Category
    func createCategoryRPC(category: Category) -> Signal<UInt64, Error> {
        
        let ctReq = ModelToProto.categoryRequestFrom(category)
        
        let createCategoryRpcSignal = Signal<UInt64, Error> { subscriber in
            
            print("---------")
            print("starting createCategoryRPC")
            
            let call: GRPCProtoCall = self.protoService.rpcToCreateCategory(with: ctReq, handler: { (catReqRes: CategoryRequest?, error: Error?) in
              
                Synchronizer.syncQueue.async {
                    print("response createCategoryRPC")
                    
                    if let error_ = error {
                        print("Error happened createCategoryRPC => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                    
                    if let catReqRes_ = catReqRes {
                        print("catReqRes_.categoryId => \(catReqRes_.categoryId)")
                        let categoryId = catReqRes_.categoryId
                        
                        if categoryId > 0 {
                            subscriber.putNext(categoryId)
                            subscriber.putCompletion()
                        }
                    }
                }
            })
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return createCategoryRpcSignal
    }
    
    func updateCategoryRPC(category: Category) -> Signal<Bool, Error> {
        
        let ctReq = ModelToProto.categoryRequestFrom(category)
        
        let updateCategoryRpcSignal = Signal<Bool, Error> { subscriber in
            
            print("-----------")
            print("starting updateCategoryRPC")
            
            let call: GRPCProtoCall = self.protoService.rpcToUpdateCategory(with: ctReq, handler: { (catReqRes: CategoryRequest?, error: Error?) in
                
                Synchronizer.syncQueue.async {
                    print("response updateCategoryRPC")
                    
                    if let error_ = error {
                        print("Error happened updateCategoryRPC => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                    
                    if let catReqRes_ = catReqRes {
                        
                        print("catReqRes_.categoryId => \(catReqRes_.categoryId)")
                        let categoryId = catReqRes_.categoryId
                        
                        if categoryId > 0 {
                            subscriber.putNext(true)
                            subscriber.putCompletion()
                        }
                    }
                }
            })
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return updateCategoryRpcSignal
    }
    
    func allCategoriesRPC() -> Signal<[CategoryRequest], Error>  {
        
        let categoryFilter = CategoryFilter()
        categoryFilter.categoryKeyword = ""
        categoryFilter.categoryUpdatedAt = UInt64(0 * 1000)
        
        let allCategoriesRpcSignal = Signal<[CategoryRequest], Error> { subscriber in
            
            print("-----")
            print("starting rpcToAllCategoriesForInitial queue:\(RPCNetwork.currentQueueName())")
            
            let call: GRPCProtoCall = self.protoService.rpcToAllCategoriesForInitial(withRequest: categoryFilter, handler: { (allCategResp: AllCategoryResponse?, error: Error?) in
                
                Synchronizer.syncQueue.async {
                    print("response rpcToAllCategoriesForInitial queue:\(RPCNetwork.currentQueueName())")
                    
                    if let allCategResp_ = allCategResp {
                        
                        var allCategories: [CategoryRequest] = []
                        for someObj in allCategResp_.categoryRequestsArray {
                            if let categoryReq = someObj as? CategoryRequest {
                                allCategories.append(categoryReq)
                            }
                        }
                        subscriber.putNext(allCategories)
                        subscriber.putCompletion()
                    }
                    
                    if let error_ = error {
                        print("Error happened rpcToAllCategoriesForInitial => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                }
            })
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return allCategoriesRpcSignal
    }
    
    func checkCategoriesForUpdateRPC(categoryUpdatedAt: Date) -> Signal<[CategoryRequest], Error>  {
        
        let categoryFilter = CategoryFilter()
        categoryFilter.categoryKeyword = ""
        categoryFilter.categoryUpdatedAt = UInt64(categoryUpdatedAt.timeIntervalSince1970 * 1000)
        
        let allCategoriesRpcSignal = Signal<[CategoryRequest], Error> { subscriber in
            
            print("-----")
            print("starting checkCategoriesForUpdateRPC queue:\(RPCNetwork.currentQueueName())")
            let call: GRPCProtoCall = self.protoService.rpcToCheckCategoriesForUpdate(withRequest: categoryFilter, handler: { (allCategResp: AllCategoryResponse?, error: Error?) in
                
                Synchronizer.syncQueue.async {
                    print("response checkCategoriesForUpdateRPC queue:\(RPCNetwork.currentQueueName()) ")
                    if let allCategResp_ = allCategResp {
                        
                        var allCategories: [CategoryRequest] = []
                        for someObj in allCategResp_.categoryRequestsArray {
                            if let categoryReq = someObj as? CategoryRequest {
                                allCategories.append(categoryReq)
                            }
                        }
                        print("Received updated categories count: \(allCategories.count)")
                        subscriber.putNext(allCategories)
                        subscriber.putCompletion()
                    }
                    
                    if let error_ = error {
                        print("Error happened checkCategoriesForUpdateRPC => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                }
            })
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return allCategoriesRpcSignal
    }
    
    //MARK: Order
    func createOrderRPC(operation_: Event, order_: Order, payment_: Payment, transaction_: Transaction?, orderDetails_: [OrderDetail]) -> Signal<(UInt64, UInt64, UInt64, [UInt64: UInt64]), Error> {
        
        let createOrderRpcSignal = Signal<(UInt64, UInt64, UInt64, [UInt64: UInt64]), Error> { subscriber in
        
            let order = Database.shared.orderBy(orderId: operation_.orderId)!
            let payment = Database.shared.paymentBy(paymentId: order.paymentId)!
            let transaction = Database.shared.transactionBy(transactionId: operation_.transactionId)
            let orderDetails = Database.shared.orderDetailsBy(orderId: operation_.orderId)
        
            let orderReq = ModelToProto.orderRequestFrom(order)
            let paymentReq = ModelToProto.paymentRequestFrom(payment)
            
            var transacReq: TransactionRequest?
            if let transaction_ = transaction {
                transacReq = ModelToProto.transactionRequestFrom(transaction_)
                //let accountReq = ModelToProto.accountRequestFrom(account)
            }
            
            var orderDetailReqs: [OrderDetailRequest] = []
            for orderDetail in orderDetails {
                let orderDetailReq = ModelToProto.orderDetailRequestFrom(orderDetail)
                orderDetailReqs.append(orderDetailReq)
            }
            
            let createOrdReq = CreateOrderRequest()
            createOrdReq.order = orderReq
            createOrdReq.payment = paymentReq
            if let transactRequest_ = transacReq {
                createOrdReq.transaction = transactRequest_
                //createOrdReq.account = accountReq
            }
            createOrdReq.orderDetailsArray = NSMutableArray(array: orderDetailReqs)
            
            print(" ------------  ")
            print("starting rpcToCreateOrderWith queue:\(RPCNetwork.currentQueueName())")
            
            let call: GRPCProtoCall = self.protoService.rpcToCreateOrder(with: createOrdReq, handler: { (createOrdReqResp: CreateOrderRequest?, error: Error?) in
                Synchronizer.syncQueue.async {
                    print("response rpcToCreateOrderWith queue:\(RPCNetwork.currentQueueName())")
                    
                    if let error_ = error {
                        print("Error happened rpcToCreateOrderWith => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                    
                    if let crtOrdReqRes_ = createOrdReqResp {
                        
                        //1 - orderId
                        //2 - paymentId
                        //3 - transactionId
                        //4 - orderDetails
                        
                        print("crtOrdReqRes_.order.orderId => \(crtOrdReqRes_.order.orderId)")
                        print("crtOrdReqRes_.payment.paymentId => \(crtOrdReqRes_.payment.paymentId)")
                        print("crtOrdReqRes_.transaction.transactionId => \(crtOrdReqRes_.transaction.transactionId)")
                        
                        var oldNewValueDict: [UInt64: UInt64] = [:]
                        
                        for oldOrderDetail in orderDetails {
                            for orderDetailReq in crtOrdReqRes_.orderDetailsArray {
                                if let orderDetailReq_ = orderDetailReq as? OrderDetailRequest {
                                    //---------- //
                                    if oldOrderDetail.orderDetailUuid == orderDetailReq_.orderDetailUuid {
                                        oldNewValueDict[oldOrderDetail.orderDetailId] = orderDetailReq_.orderDetailId
                                    }
                                    //--------- //
                                }
                            }
                        }
                        
                        let orderId = crtOrdReqRes_.order.orderId
                        let paymentId = crtOrdReqRes_.payment.paymentId
                        let transactionId = crtOrdReqRes_.transaction.transactionId
                        
                        subscriber.putNext((orderId, paymentId, transactionId, oldNewValueDict))
                        subscriber.putCompletion()
                    }
                }
            })
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return createOrderRpcSignal
    }
    
    
    func updateOrderRPC(order_: Order) -> Signal<Order, Error> {
        let updateOrderRpcSignal = Signal<Order, Error> { subscriber in
            
            let order = Database.shared.orderBy(orderId: order_.orderId)!
            let orderReq = ModelToProto.orderRequestFrom(order)
            
            let call: GRPCProtoCall = self.protoService.rpcToUpdateOrder(with: orderReq, handler: { (orderReq: OrderRequest?, error: Error?) in
                Synchronizer.syncQueue.async {
                    print("response updateOrderRPC")
                    
                    if let error_ = error {
                        print("Error happened updateOrderRPC => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                    
                    if let orderReq_ = orderReq {
                        let order = ProtoToModel.orderFrom(orderReq_)
                        subscriber.putNext(order)
                        subscriber.putCompletion()
                    }
                }
            })
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return updateOrderRpcSignal
    }
    
    func allOrdersForFilterRPC(date: Date, limit: UInt16) -> Signal<[CreateOrderRequest], Error> {
        
        let allTransactionsForFilterRpcSignal = Signal<[CreateOrderRequest], Error> { subscriber in
            
            print("----------")
            print("starting rpcToAllOrdersForInitial")
            
            let filter = OrderFilter()
            filter.orderKeyword = ""
            filter.orderDate = UInt64(date.timeIntervalSince1970 * 1000)
            filter.limit = UInt32(limit)
            
            let call: GRPCProtoCall = self.protoService.rpcToAllOrdersForInitial(withRequest: filter, handler: { (allOrderResp: AllOrderResponse?, error: Error?) in
                Synchronizer.syncQueue.async {
                    print("response rpcToAllOrdersForInitial")
                    
                    if let error_ = error {
                        print("Error happened rpcToAllOrdersForInitial => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                    if let allOrderResp_ = allOrderResp {
                        var allOrders: [CreateOrderRequest] = []
                        for someObj in allOrderResp_.orderRequestsArray {
                            if let createOrdReq = someObj as? CreateOrderRequest {
                                allOrders.append(createOrdReq)
                            }
                        }
                        subscriber.putNext(allOrders)
                        subscriber.putCompletion()
                    }
                }
            })
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return allTransactionsForFilterRpcSignal
    }
    
    func checkOrdersForUpdateRPC(date: Date, limit: UInt16) -> Signal<[CreateOrderRequest], Error> {
        
        let allRecentOrdersForFilterRpcSignal = Signal<[CreateOrderRequest], Error> { subscriber in
            
            print("----------")
            print("starting allRecentOrdersForFilter")
            
            let filter = OrderFilter()
            filter.orderKeyword = ""
            filter.orderDate = UInt64(date.timeIntervalSince1970 * 1000)
            filter.limit = UInt32(limit)
            
            let call: GRPCProtoCall = self.protoService.rpcToCheckOrdersForUpdate(withRequest: filter, handler: { (allOrderResp: AllOrderResponse?, error: Error?) in
                
                Synchronizer.syncQueue.async {
                    print("response allRecentOrdersForFilter")
                    
                    if let error_ = error {
                        print("Error happened allRecentOrdersForFilter => \(error_.localizedDescription)")
                        subscriber.putError(error_)
                        subscriber.putCompletion()
                    }
                    if let allOrderResp_ = allOrderResp {
                        var allOrders: [CreateOrderRequest] = []
                        for someObj in allOrderResp_.orderRequestsArray {
                            if let createOrdReq = someObj as? CreateOrderRequest {
                                allOrders.append(createOrdReq)
                            }
                        }
                        subscriber.putNext(allOrders)
                        subscriber.putCompletion()
                    }
                }
            })
            RPCNetwork.registerKeys(call)
            call.start()
            return ActionDisposable {
                call.cancel()
            }
        }
        return allRecentOrdersForFilterRpcSignal
    }
    
    //MARK: Queue
    static func currentQueueName() -> String {
        let name = __dispatch_queue_get_label(nil)
        if let s = String(cString: name, encoding: .utf8) {
            return s
        }
        return "undefined"
    }
}
