//
//  ImageManager.swift
//  Rentautomation
//
//  Created by kanybek on 10/1/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import Photos

final class ImageManager {
    
    static let sharedInstance: ImageManager = ImageManager()
    
    let cachingImageManager: PHCachingImageManager = PHCachingImageManager()
    
    func requestCompressedImage(asset: PHAsset, completion: @escaping (_ result: Data?) -> Void) -> Void {
        
        let thumbOptions: PHImageRequestOptions = PHImageRequestOptions()
        thumbOptions.version = .current
        thumbOptions.deliveryMode = .fastFormat
        thumbOptions.resizeMode = .fast
        thumbOptions.isSynchronous = true
        
        cachingImageManager.requestImage(for: asset,
                                  targetSize: CGSize(width: 512, height: 512),
                                  contentMode: .aspectFill,
                                  options: thumbOptions) { (image: UIImage?, _: [AnyHashable : Any]?) in
                                    
                                    if let image_ = image {
                                        let data = UIImageJPEGRepresentation(image_, 0.5)
                                        completion(data)
                                    }
                                    
        }
    }
}
