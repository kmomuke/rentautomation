//
//  AppSession.swift
//  Rentautomation
//
//  Created by kanybek on 12/5/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

final class AppSession {
    
    var currentEmployee: User? {
        get {
            if(_currentEmployee == nil) {
                _currentEmployee = Database.shared.currentDeviceOwnerUser()
            }
            return _currentEmployee
        }
    }
    
    var _currentEmployee: User?
        
    static let shared: AppSession = AppSession()
    
    init() {
    }
    
    func endSession() -> Void {
        self._currentEmployee = nil
        Database.shared.dropDatabase(fullDrop: false)
        ReachabilityManager.shared.stopMonitoringNetworkReachability()
        RPCNetwork.shared.stopStreamingConnection()
        RPCNetwork.shared.invalidateTimer()
        
        //if let appDomain = Bundle.main.bundleIdentifier {
        //    UserDefaults.standard.removePersistentDomain(forName: appDomain)
        //}
    }
    
    //case owner = 1000
    //case admin = 2000
    //case agent = 3000
    
    func isAgent() -> Bool {
        if let currentEmployee_ = self.currentEmployee {
            if currentEmployee_.roleId != 1000 {
                    return true
            }
        }
        return false
    }
    
    func isOwner() -> Bool {
        if let currentEmployee_ = self.currentEmployee {
            if currentEmployee_.roleId == 1000 {
                return true
            }
        }
        return false
    }
}
