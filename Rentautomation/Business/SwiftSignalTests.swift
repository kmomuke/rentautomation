//
//  SwiftSignalTests.swift
//  Rentautomation
//
//  Created by kanybek on 4/18/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit

// META
//SSignalQueue *videoCompressQueue3() {
//    static SSignalQueue *instance3 = nil;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//    instance3 = [[SSignalQueue alloc] init];
//    });
//    return instance3;
//}

//enqueueSignal = [videoCompressQueue3() enqueue:compressSignal];


final class SwiftSignalTests {

    static let shared: SwiftSignalTests = SwiftSignalTests()
    var queDispose: Disposable?
    
    init() {
        
    }
    
    deinit {
        self.queDispose?.dispose()
        self.queDispose = nil
    }
    
    // MARK:SSIGNALS TEST
    // --------------------- TEST SAMPLE --------------------------- //
    func testCombineLatest3() {
        
        /*
         let signal = Signal<Int, Void> { [object] subscriber in
         subscriber.putNext(1)
         subscriber.putCompletion()
         
         return ActionDisposable {
         let _ = object?.description
         disposed = true
         }
         }
         
         let disposable = signal.start(next: { [object] next in
         generated = true
         let _ = object?.description
         }, completed: { [object]
         completed = true
         let _ = object?.description
         })
         */
        
        let s1 = Signal<Int, Void> { subscriber in
            subscriber.putNext(1)
            subscriber.putCompletion()
            //subscriber.putError(Void)
            return EmptyDisposable
        }
        
        let s2 = Signal<Int, Void> { subscriber in
            subscriber.putNext(2)
            subscriber.putCompletion()
            return EmptyDisposable
        }
        
        let s3 = Signal<Int, Void> { subscriber in
            subscriber.putNext(3)
            subscriber.putCompletion()
            return EmptyDisposable
        }
        
        
        let signal = combineLatest(s1, s2, s3)
        
        var completed = false
        let _ = signal.start(next: { next in
            
            print("next.0 ==> \(next.0)")
            print("next.1 ==> \(next.1)")
            print("next.2 ==> \(next.2)")
            
            return
        }, completed: {
            completed = true
        })
        
        print("completed ==> \(completed)")
    }
    
    func testSwitchToLatest() -> Void {
        
        //self.syncDispose.dispose()
        
        var signals: [Signal<Int, Void>] = []
        
        let one = Signal<Int, Void> { subscriber in
            print("started one")
            Synchronizer.syncQueue.async {
                sleep(9)
                subscriber.putNext(1)
                subscriber.putCompletion()
            }
            
            return ActionDisposable {
                print("1 disposed")
            }
        }
        signals.append(one)
        
        let two = Signal<Int, Void> { subscriber in
            print("started two")
            Synchronizer.syncQueue.async {
                sleep(8)
                subscriber.putNext(2)
                subscriber.putCompletion()
            }
            return ActionDisposable {
                print("2 disposed")
            }
        }
        signals.append(two)
        
        let three = Signal<Int, Void> { subscriber in
            print("started three")
            Synchronizer.syncQueue.async {
                sleep(7)
                //subscriber.putNext(3)
                subscriber.putError()
                subscriber.putCompletion()
            }
            return ActionDisposable {
                print("3 disposed")
            }
        }
        signals.append(three)
        
        let four = Signal<Int, Void> { subscriber in
            print("started four")
            Synchronizer.syncQueue.async {
                sleep(5)
                subscriber.putNext(4)
                subscriber.putCompletion()
            }
            return ActionDisposable {
                print("4 disposed")
            }
        }
        signals.append(four)
        
        var startingSignal = Signal<Int, Void> { subscriber in
            print("starting")
            subscriber.putNext(0)
            subscriber.putCompletion()
            return ActionDisposable {
                print("starting disposed")
            }
        }
        
        for signal in signals {
            startingSignal = startingSignal |> then(signal)
        }
        
        //let newSignal = singleSignalInt(combineLatest(signals)) |> queue |> runOn(SynchronizeManager.syncQueue)
        //let signal2 = singleSignalInt(one) |> then(singleSignalInt(two)) |> then(singleSignalInt(three))  |> runOn(SynchronizeManager.syncQueue)
        let signal2 = startingSignal |> runOn(Synchronizer.syncQueue)
        //let signal2 = one |> then(two) |> then(three) |> runOn(SynchronizeManager.syncQueue)
        
        self.queDispose = signal2.start(next: { (next: Int) in
            print("next=====>\(next)")
        }, error: { (Void) in
            print("error")
        }) {
            print("completed")
        }
        
        
        return
        
        //        print("return")
        //
        //        let signal11 = singleSignalInt(one) |> runOn(SynchronizeManager.syncQueue) |> queue
        //        let signal22 = singleSignalInt(two) |> runOn(SynchronizeManager.syncQueue) |> queue
        //        let signal33 = singleSignalInt(three) |> runOn(SynchronizeManager.syncQueue) |> queue
        //
        //        let newSignal = combineLatest([signal11, signal22, signal33])
        //        let disposable2 = newSignal.start(next: { (k: [Int]) in
        //            print("newSignal")
        //        }, error: { (Void) in
        //            print("error")
        //        }, completed: { (Void) in
        //            print("completed")
        //        })
        //        self.syncDispose.add(disposable2)
    }
    
    func singleSignalInt(_ value: Signal<Int, Void>) -> Signal<Signal<Int, Void>, Void> {
        return Signal { subscriber in
            subscriber.putNext(value)
            subscriber.putCompletion()
            return EmptyDisposable
        }
    }
}
