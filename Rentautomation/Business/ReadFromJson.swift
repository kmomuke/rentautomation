//
//  ReadFromJson.swift
//  Rentautomation
//
//  Created by kanybek on 8/1/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit


final class ReadFromJson {
    static let shared: ReadFromJson = ReadFromJson()
    init() {
    }
    
    
    
    func readFromCatLocalJson() {
        if let path = Bundle.main.path(forResource: "categories", ofType: "json") {
            do {
                let jsonData = try NSData(contentsOfFile: path, options: NSData.ReadingOptions.mappedIfSafe)
                do {
                    
                    let jsonResult: NSArray = try JSONSerialization.jsonObject(with: jsonData as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSArray
                    
                    for item in jsonResult {
                        print("\(item)")
                        var index: UInt64 = 0
                        if let prod: NSDictionary = item as? NSDictionary {
                            index = index + 1
                            self.createCatFromDict(prod, index)
                        }
                    }
                } catch {}
            } catch {}
        }
    }
    
    func readFromProdLocalJson() {
        if let path = Bundle.main.path(forResource: "products", ofType: "json") {
            do {
                let jsonData = try NSData(contentsOfFile: path, options: NSData.ReadingOptions.mappedIfSafe)
                do {
                    
                    let jsonResult: NSArray = try JSONSerialization.jsonObject(with: jsonData as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSArray
                    
                    for item in jsonResult {
                        print("\(item)")
                        var index: UInt64 = 0
                        if let prod: NSDictionary = item as? NSDictionary {
                            index = index + 1
                            self.createProductsFromDict(prod, index)
                        }
                    }
                } catch {}
            } catch {}
        }
    }
    
    func readFromCustomerLocalJson() {
        if let path = Bundle.main.path(forResource: "customers", ofType: "json") {
            do {
                let jsonData = try NSData(contentsOfFile: path, options: NSData.ReadingOptions.mappedIfSafe)
                do {
                    
                    let jsonResult: NSArray = try JSONSerialization.jsonObject(with: jsonData as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSArray
                    
                    for item in jsonResult {
                        print("\(item)")
                        var index: UInt64 = 0
                        if let prod: NSDictionary = item as? NSDictionary {
                            index = index + 1
                            self.createCustomersFromDict(prod, index)
                        }
                    }
                } catch {}
            } catch {}
        }
    }
    
    private func createCatFromDict(_ prod: NSDictionary, _ index: UInt64) -> Void {
        let creatingCategory: Category = Category()
        creatingCategory.categoryId = prod.object(forKey: "categoryId") as! UInt64
        creatingCategory.categoryName = prod.object(forKey: "categoryName") as! String
        Database.shared.createCategories([creatingCategory])
        
        let cid = Database.shared.operationCid()
        let operation = Event(syncOperationId: cid,
                                      isSynchronized: false,
                                      syncOperationDate: Date()/*.dateBySubtractingDays(300).dateByAddingMinutes(Int(cid))*/,
            operationType: .categoryCreated,
            productId: 0,
            orderDetailId: 0,
            customerId: 0,
            transactionId: 0,
            accountId: 0,
            supplierId: 0,
            staffId: 0,
            categoryId: creatingCategory.categoryId,
            orderId: 0)
        
        Database.shared.createOperations([operation])
    }
    
    private func createProductsFromDict(_ prod: NSDictionary, _ index: UInt64) -> Void {
        
        let creatingProduct = Product()
        creatingProduct.productId = prod.object(forKey: "productId") as! UInt64
        creatingProduct.productName = prod.object(forKey: "productName") as! String
        creatingProduct.productImagePath =  prod.object(forKey: "productImagePath") as! String
        creatingProduct.saleUnitPrice = prod.object(forKey: "saleUnitPrice") as! Double
        creatingProduct.quantityPerUnit = prod.object(forKey: "quantityPerUnit") as! String
        creatingProduct.barcode = prod.object(forKey: "barcode") as! String
        creatingProduct.unitsInStock = prod.object(forKey: "unitsInStock") as! Double
        creatingProduct.incomeUnitPrice = prod.object(forKey: "incomeUnitPrice") as! Double
        creatingProduct.supplierId = prod.object(forKey: "supplierId") as! UInt64
        creatingProduct.categoryId = prod.object(forKey: "categoryId") as! UInt64
        
        Database.shared.createProducts(products: [creatingProduct])
        
        let orderDetail = OrderDetail()
        orderDetail.orderDetailId = index
        orderDetail.isLast = true
        orderDetail.productId = creatingProduct.productId
        orderDetail.orderQuantity = creatingProduct.unitsInStock
        orderDetail.price = creatingProduct.saleUnitPrice
        orderDetail.productQuantity = creatingProduct.unitsInStock
        orderDetail.orderId = 0
        orderDetail.billingNo = "\(Date().toString(.custom("EEE, dd MMM HH:mm"))) "
        orderDetail.orderDetailDate = Date()
        
        Database.shared.createorderDetails([orderDetail])
        
        let cid = Database.shared.operationCid()
        let operation = Event(syncOperationId: cid,
                                      isSynchronized: false,
                                      syncOperationDate: Date()/*.dateBySubtractingDays(300).dateByAddingMinutes(Int(cid))*/,
            operationType: .productCreated,
            productId: creatingProduct.productId,
            orderDetailId: orderDetail.orderDetailId,
            customerId: 0,
            transactionId: 0,
            accountId: 0,
            supplierId: 0,
            staffId: 0,
            categoryId: 0,
            orderId: 0)
        
        Database.shared.createOperations([operation])
    }
    
    private func createCustomersFromDict(_ prod: NSDictionary, _ index: UInt64) -> Void {
        let key = Database.shared.uniqueIncreasingPrimaryKey()
        
        let creatingCustomer = Customer()
        creatingCustomer.customerId = prod.object(forKey: "customerId") as! UInt64
        creatingCustomer.customerImagePath = prod.object(forKey: "customerImagePath") as? String
        creatingCustomer.firstName = prod.object(forKey: "firstName") as! String
        creatingCustomer.secondName = prod.object(forKey: "secondName") as! String
        creatingCustomer.phoneNumber = prod.object(forKey: "phoneNumber") as! String
        creatingCustomer.address = prod.object(forKey: "address") as! String
        creatingCustomer.userId = prod.object(forKey: "userId") as! UInt64
        
        let account = Account()
        account.accountId = key + index
        account.customerId = creatingCustomer.customerId
        account.supplierId = 0
        account.balance = 0.0
        Database.shared.createAccounts([account])
        
        Database.shared.createCustomers([creatingCustomer])
        
        if let employee_ = AppSession.shared.currentEmployee {
            
            let transactionId = key + index
            
            let transaction = Transaction(transactionId: transactionId,
                                          transactionDate: Date(),
                                          isLastTransaction: true,
                                          transactionType: .customerBalanceModified,
                                          moneyAmount: 0.0,
                                          orderId: 0,
                                          customerId: creatingCustomer.customerId,
                                          supplierId: 0,
                                          userId: employee_.userId,
                                          ballanceAmount: account.balance,
                                          comment: "",
                                          transactionUuid: UUID().uuidString.lowercased())
            
            Database.shared.createTransactions([transaction])
            
            let cid = Database.shared.operationCid()
            let operation = Event(syncOperationId: cid,
                                          isSynchronized: false,
                                          syncOperationDate: Date()/*.dateBySubtractingDays(300).dateByAddingMinutes(Int(cid))*/,
                operationType: .customerCreated,
                productId: 0,
                orderDetailId: 0,
                customerId: creatingCustomer.customerId,
                transactionId: transaction.transactionId,
                accountId: account.accountId,
                supplierId: 0,
                staffId: 0,
                categoryId: 0,
                orderId: 0)
            
            Database.shared.createOperations([operation])
        }
    }
    
    //----------
    
}
