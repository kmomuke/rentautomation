//
//  ProductDevManager.swift
//  Rentautomation
//
//  Created by kanybek on 9/3/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

final class Creator {
    
    static let shared: Creator = Creator()
    init() {}
    
    private func increaseOrDecreaseProducts(shouldIncrease: Bool,
                                            orderDetails: [OrderDetail],
                                            staff: User,
                                            orderDocType: OrderDocumentType,
                                            totalPrice: Double,
                                            discount: Discount) -> Order {
        // --- Order --- //
        let order = Order()
        order.orderId = Database.shared.uniqueIncreasingPrimaryKey()
        order.orderDocument = orderDocType
        order.orderMovement = .orderDevelopeRelated
        
        let currentDate = Date()
        order.orderDate = currentDate
        order.billingNo = OrderMemomry.shared.billingNo
        order.comment = OrderMemomry.shared.comments
        order.userId = staff.userId
        order.isDeleted = false
        order.isMoneyForDebt = false
        order.isEdited = false
        
        // --- Payment --- //
        let payment = Payment()
        payment.paymentId = Database.shared.uniqueIncreasingPrimaryKey()
        discount.totalPrice = totalPrice
        payment.setUpWith(discount: discount)
        
        // -------------
        order.paymentId = payment.paymentId
        // -------------
        
        // --- OrderDetails --- ------- //
        var orderDetailId = Database.shared.uniqueIncreasingPrimaryKey()
        
        for orderDetail in orderDetails {
            
            orderDetail.orderDetailId = orderDetailId
            orderDetail.orderId = order.orderId
            orderDetail.billingNo = order.billingNo
            
            orderDetailId = orderDetailId + 1
        }
        
        Database.shared.createOrders([order])
        Database.shared.createPayments([payment])
        Database.shared.createorderDetails(orderDetails)
        
        // ---------- Transactions for Product ----------
        if shouldIncrease {
            OrderManager.shared.increaseProductsInStock(orderDetails: orderDetails)
        } else {
            OrderManager.shared.decreaseProductsInStock(orderDetails: orderDetails)
        }
        
        // ------ Sync OPERATION ----- //
        let operation = Event(syncOperationId: Database.shared.operationCid(),
                              isSynchronized: false,
                              syncOperationDate: Date(),
                              operationType: .orderCreated,
                              productId: 0,
                              orderDetailId: 0,
                              customerId: 0,
                              transactionId: 0,
                              accountId: 0,
                              supplierId: 0,
                              staffId: staff.userId,
                              categoryId: 0,
                              orderId: order.orderId)
        Database.shared.createOperations([operation])
        
        return order
    }
    
    private func increaseOrDecreaseProductsEditted(shouldIncrease: Bool,
                                                   initialOrder: Order,
                                                   newOrderDetails: [OrderDetail],
                                                   newStaff: User,
                                                   newCustomerId: UInt64,
                                                   newOrderDocType: OrderDocumentType,
                                                   newTotalPrice: Double,
                                                   newDiscount: Discount) -> Order {
        // --- Order --- //
        let order = Order()
        order.orderId = Database.shared.uniqueIncreasingPrimaryKey()
        order.orderDocument = newOrderDocType
        order.orderMovement = .orderDevelopeRelated
        
        let currentDate = Date().dateBySubtractingSeconds(1)
        order.orderDate = currentDate
        let str = "№ \(order.orderId) от \(currentDate.toString(.custom("EEEE, dd MMMM yyyy HH:mm")))"
        order.billingNo = str
        order.userId = newStaff.userId
        order.isDeleted = false
        order.isMoneyForDebt = false
        order.isEdited = true
        
        if let oldPayment = Database.shared.paymentBy(paymentId: initialOrder.paymentId) {
            // --- Payment --- //
            let newPayment = Payment()
            newPayment.paymentId = Database.shared.uniqueIncreasingPrimaryKey()
            let oldDiscount = Discount(payment: oldPayment)
            newPayment.setUpWith(discount: oldDiscount)
            // -------------
            order.paymentId = newPayment.paymentId
            // -------------
            Database.shared.createPayments([newPayment])
        }
        
        // --- OrderDetails --- ------- //
        let oldOdetails = Database.shared.orderDetailsBy(orderId: initialOrder.orderId)
        var orderDetailId = Database.shared.uniqueIncreasingPrimaryKey()
        
        for (index, orderDetail) in oldOdetails.enumerated() {
            orderDetail.orderDetailId = orderDetailId
            orderDetail.orderId = order.orderId
            orderDetail.orderDetailDate = Date().dateBySubtractingSeconds(10).dateByAddingSeconds(index)
            orderDetail.billingNo = order.billingNo
            orderDetailId = orderDetailId + 1
        }
        
        Database.shared.createorderDetails(oldOdetails)
        Database.shared.createOrders([order])
        
        // --------------------- ORDER EDITED UPDATE ---------------------
        Database.shared.updateOrderEdittedStatus(orderId: initialOrder.orderId, isEditted: true)
        if true {
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                  isSynchronized: false,
                                  syncOperationDate: Date().dateBySubtractingSeconds(3),
                                  operationType: .orderUpdated,
                                  productId: 0,
                                  orderDetailId: 0,
                                  customerId: 0,
                                  transactionId: 0,
                                  accountId: 0,
                                  supplierId: 0,
                                  staffId: 0,
                                  categoryId: 0,
                                  orderId: initialOrder.orderId)
            Database.shared.createOperations([operation])
        }
        
        // ---------- Product Stock ----------
        if shouldIncrease {
            OrderManager.shared.increaseProductsInStock(orderDetails: oldOdetails)
        } else {
            OrderManager.shared.decreaseProductsInStock(orderDetails: oldOdetails)
        }
        
        // ------ Sync OPERATION ----- //
        let operation = Event(syncOperationId: Database.shared.operationCid(),
                              isSynchronized: false,
                              syncOperationDate: Date().dateBySubtractingSeconds(2),
                              operationType: .orderCreated,
                              productId: 0,
                              orderDetailId: 0,
                              customerId: 0,
                              transactionId: 0,
                              accountId: 0,
                              supplierId: 0,
                              staffId: newStaff.userId,
                              categoryId: 0,
                              orderId: order.orderId)
        Database.shared.createOperations([operation])
        
        if newOrderDetails.count > 0 {
            
            if initialOrder.orderDocument == .rawProductGoneToProd {
                let _ = Creator.shared.rawProductsGoneToProduction(orderDetails: newOrderDetails,
                                                                   staff: newStaff,
                                                                   orderDocType: .rawProductGoneToProd,
                                                                   totalPrice: newTotalPrice,
                                                                   discount: newDiscount)
            } else if initialOrder.orderDocument == .readyProductReceivedFromProd {
                let _ = Creator.shared.readyProductReceivedFromProduction(orderDetails: newOrderDetails,
                                                                          staff: newStaff,
                                                                          orderDocType: .readyProductReceivedFromProd,
                                                                          totalPrice: newTotalPrice,
                                                                          discount: newDiscount)
            }
        }
        
        return order
    }
    
    // -----------------------------------------------------------------------------------------------------------------------
    
    //case rawProductGoneToProd = 21000 // Расход сырья
    //case rawProductGoneToProdEdited = 22000
    func rawProductsGoneToProduction(orderDetails: [OrderDetail],
                                     staff: User,
                                     orderDocType: OrderDocumentType,
                                     totalPrice: Double,
                                     discount: Discount) -> Order {
        
        return self.increaseOrDecreaseProducts(shouldIncrease: false,
                                               orderDetails: orderDetails,
                                               staff: staff,
                                               orderDocType: orderDocType,
                                               totalPrice: totalPrice,
                                               discount: discount)
    }
    
    func rawProductsGoneToProductionEditted(shouldIncrease: Bool,
                                            initialOrder: Order,
                                            newOrderDetails: [OrderDetail],
                                            newStaff: User,
                                            newCustomerId: UInt64,
                                            newOrderDocType: OrderDocumentType,
                                            newTotalPrice: Double,
                                            newDiscount: Discount) -> Order {
        
        return self.increaseOrDecreaseProductsEditted(shouldIncrease: true,
                                                      initialOrder: initialOrder,
                                                      newOrderDetails: newOrderDetails,
                                                      newStaff: newStaff,
                                                      newCustomerId: newCustomerId,
                                                      newOrderDocType: newOrderDocType,
                                                      newTotalPrice: newTotalPrice,
                                                      newDiscount: newDiscount)
    }
    
    //case readyProductReceivedFromProd = 23000 // Выход готовой продукции
    //case readyProductReceivedFromProdEdited = 24000
    func readyProductReceivedFromProduction(orderDetails: [OrderDetail],
                                            staff: User,
                                            orderDocType: OrderDocumentType,
                                            totalPrice: Double,
                                            discount: Discount) -> Order {
        
        return self.increaseOrDecreaseProducts(shouldIncrease: true,
                                               orderDetails: orderDetails,
                                               staff: staff,
                                               orderDocType: orderDocType,
                                               totalPrice: totalPrice,
                                               discount: discount)
    }
    
    func readyProductReceivedFromProductionEditted(shouldIncrease: Bool,
                                                   initialOrder: Order,
                                                   newOrderDetails: [OrderDetail],
                                                   newStaff: User,
                                                   newCustomerId: UInt64,
                                                   newOrderDocType: OrderDocumentType,
                                                   newTotalPrice: Double,
                                                   newDiscount: Discount) -> Order {
        
        return self.increaseOrDecreaseProductsEditted(shouldIncrease: false,
                                                      initialOrder: initialOrder,
                                                      newOrderDetails: newOrderDetails,
                                                      newStaff: newStaff,
                                                      newCustomerId: newCustomerId,
                                                      newOrderDocType: newOrderDocType,
                                                      newTotalPrice: newTotalPrice,
                                                      newDiscount: newDiscount)
    }
    
    //case incomeProductFromStock = 25000 // Приеммка продукта от склада
    //case incomeProductFromStockEdited = 26000
    func incomeProductFromStock(orderDetails: [OrderDetail],
                                staff: User,
                                orderDocType: OrderDocumentType,
                                totalPrice: Double,
                                discount: Discount) -> Order {
        
        return self.increaseOrDecreaseProducts(shouldIncrease: true,
                                               orderDetails: orderDetails,
                                               staff: staff,
                                               orderDocType: orderDocType,
                                               totalPrice: totalPrice,
                                               discount: discount)
    }
    
    //case removeProducts = 27000 // Списание товара
    //case removeProductsEdited = 28000
    func removeProductsFromStock(orderDetails: [OrderDetail],
                                 staff: User,
                                 orderDocType: OrderDocumentType,
                                 totalPrice: Double,
                                 discount: Discount) -> Order {
        
        return self.increaseOrDecreaseProducts(shouldIncrease: false,
                                               orderDetails: orderDetails,
                                               staff: staff,
                                               orderDocType: orderDocType,
                                               totalPrice: totalPrice,
                                               discount: discount)
    }
    
    //case productMovememntToStock = 29000 // Перемещение товара
    //case productMovememntToStockEdited = 30000
    func productMovememntToStock(orderDetails: [OrderDetail],
                                 staff: User,
                                 orderDocType: OrderDocumentType,
                                 totalPrice: Double,
                                 discount: Discount) -> Order {
        
        return self.increaseOrDecreaseProducts(shouldIncrease: false,
                                               orderDetails: orderDetails,
                                               staff: staff,
                                               orderDocType: orderDocType,
                                               totalPrice: totalPrice,
                                               discount: discount)
    }
}
