//
//  GlobalSharedManager.swift
//  Rentautomation
//
//  Created by kanybek on 9/29/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit

final class Global {
    
    static let shared: Global = Global()
    var isDebug: Bool
    
    var isShowTestButton:Bool {
        get {
            return AppSettings.shared.getDeveloperMode()
        }
    }
    
    var limitForStaticModels: UInt64
    let statisticShowDay: Int = 15
    
    required init() {
        self.isDebug = AppSettings.shared.getDeveloperMode()
        self.limitForStaticModels = 10000
    }
    
    lazy var targetName: String = {
        let targetName = Bundle.main.infoDictionary?["CFBundleName"] as? String ?? ""
        return targetName
    }()
    
    func contactsIconImage() -> (UIImage?, UIImage?) {
        let contactsIconImage = UIImage(named: "TabIconContacts.png")
        let selectedContactsIconImage = UIImage(named: "TabIconContacts_Highlighted.png")
        return (contactsIconImage, selectedContactsIconImage)
    }
    
    func messagesIconImage() -> (UIImage?, UIImage?) {
        let messagesIconImage = UIImage(named: "TabIconMessages.png")
        let selectedMessagesIconImage = UIImage(named: "TabIconMessages_Highlighted.png")
        return (messagesIconImage, selectedMessagesIconImage)
    }
    
    func settingsIconImage() -> (UIImage?, UIImage?) {
        let settingsIconImage = UIImage(named: "TabIconSettings.png")
        let selectedSettingsIconImage = UIImage(named: "TabIconSettings_Highlighted.png")
        return (settingsIconImage, selectedSettingsIconImage)
    }
        
    lazy var productPlaceholderImage: UIImage = {
        let logo_ = UIImage(named: "Product")!
        return logo_
    }()
    
    lazy var customerPlaceholderImage: UIImage = {
        let logo_ = UIImage(named: "Customer")!
        return logo_
    }()
    
    lazy var staffPlaceholderImage: UIImage = {
        let logo_ = UIImage(named: "Staff")!
        return logo_
    }()
    
    lazy var supplierPlaceholderImage: UIImage = {
        let logo_ = UIImage(named: "Supplier")!
        return logo_
    }()
    
    lazy var priceFormatter: NumberFormatter = {
        let numberFormatter_ = NumberFormatter()
        numberFormatter_.numberStyle = .currency
        numberFormatter_.minimumFractionDigits = 0
        numberFormatter_.maximumFractionDigits = AppSettings.shared.maximumFractionDigits
        numberFormatter_.currencyCode = "KGS"
        //numberFormatter_.currencySymbol = "$"
        numberFormatter_.currencySymbol = AppSettings.shared.currentCurrency()
        numberFormatter_.locale = Locale(identifier: "ru_RU")
        return numberFormatter_
    }()
    
    lazy var amountFormatter: NumberFormatter = {
        let numberFormatter_ = NumberFormatter()
        numberFormatter_.numberStyle = .currency
        numberFormatter_.minimumFractionDigits = 0
        numberFormatter_.maximumFractionDigits = AppSettings.shared.maximumFractionDigits
        numberFormatter_.currencyCode = "KGS"
        numberFormatter_.currencySymbol = ""
        numberFormatter_.locale = Locale(identifier: "ru_RU")
        return numberFormatter_
    }()
    
//    func getImageWithColor(color: UIColor, size: CGSize) -> UIImage {
//        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
//        let path = UIBezierPath(roundedRect: rect, cornerRadius: 5.0)
//        UIGraphicsBeginImageContextWithOptions(size, false, 0)
//        color.setFill()
//        path.fill()
//        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
//        UIGraphicsEndImageContext()
//        return image
//    }
    
    func getImageWithColor(color: UIColor, size: CGSize) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}

