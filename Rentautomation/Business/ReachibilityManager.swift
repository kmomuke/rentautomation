//
//  ReachibilityManager.swift
//  Rentautomation
//
//  Created by kanybek on 1/26/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import ReachabilitySwift

func dispatch_async_safely_to_main_queue(_ block: @escaping ()->()) {
    dispatch_async_safely_to_queue(DispatchQueue.main, block)
}

func dispatch_async_safely_to_queue(_ queue: DispatchQueue, _ block: @escaping ()->()) {
    if queue === DispatchQueue.main && Thread.isMainThread {
        block()
    } else {
        queue.async {
            block()
        }
    }
}

class ReachabilityManager {
    
    lazy var reachability: Reachability? = {
        
        let reachability_: Reachability?
        reachability_ = Reachability()

        return reachability_
    }()
    
    static let shared: ReachabilityManager = ReachabilityManager()
    
    required init() {
        setUpInitialValues()
    }
    
    func setUpInitialValues() {
        
            reachability?.whenReachable = { reachability in
                
                dispatch_async_safely_to_main_queue({
                    
                    if RPCNetwork.shared.rpcStatus == .disconnected {
                        //RPCNetwork.shared.refreshAndRetryConnectToSocket()
                        RPCNetwork.shared.stopStreamingConnection()
                        RPCNetwork.shared.openStreamingConnection()
                    }
                    
                    if reachability.isReachableViaWiFi {
                        print("Reachable via WiFi")
                    } else {
                        print("Reachable via Cellular")
                    }
                })
            }
            
            reachability?.whenUnreachable = { reachability in
                
                dispatch_async_safely_to_main_queue({
                    print("Not reachable")
                    RPCNetwork.shared.stopStreamingConnection()
                })
            }
    }
    
    func startMonitoringNetworkReachability() -> Void {
        do {
            try reachability?.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    func stopMonitoringNetworkReachability() -> Void {
        reachability?.stopNotifier()
    }
    
    func isConnectedToInternet() -> Bool {
        if let reachability_ = self.reachability {
            return reachability_.isReachable
        }
        return false
    }
    
    func isConnectedToInternetViaWiFi() -> Bool {
        if let reachability_ = self.reachability {
            return reachability_.isReachableViaWiFi
        }
        return false
    }
    
    func isConnectedToInternetViaCellular() -> Bool {
        if let reachability_ = self.reachability {
            return reachability_.isReachableViaWWAN
        }
        return false
    }
}
