//
//  GenerateJson.swift
//  Rentautomation
//
//  Created by kanybek on 8/1/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

final class GenerateJson {

    var url: URL
    var isGeneratingCSV: Bool
    var isTotalDataComputing: Bool
    
    init () {
        self.isGeneratingCSV = false
        self.isTotalDataComputing = false
        let local_path =  "image_path.csv"
        let tempPath = NSTemporaryDirectory().stringByAppendingPathComponent(path: local_path)
        self.url = NSURL.fileURL(withPath: tempPath)
    }
    
    static let shared: GenerateJson = GenerateJson()
    
    func generateJsonFileFromProducts(products: [Product],
                                      progress:@escaping (Float) -> (),
                                      completion:@escaping (URL) -> ()) -> Void {
        
        if self.isGeneratingCSV {
            return
        }
        
        if !self.isGeneratingCSV {
            self.isGeneratingCSV = true
            
            generateQue.async {
                
                var productJsons: [NSMutableDictionary] = []
                for product in products {
                    //var jsonObject: [Any: Any] = [:]
                    let jsonObject: NSMutableDictionary = NSMutableDictionary()
                    
                    jsonObject.setValue(NSNumber(value: product.productId), forKey: "productId")
                    jsonObject.setValue(product.productImagePath, forKey: "productImagePath")
                    jsonObject.setValue(product.productName, forKey: "productName")
                    jsonObject.setValue(NSNumber(value: product.supplierId), forKey: "supplierId")
                    jsonObject.setValue(NSNumber(value: product.categoryId), forKey: "categoryId")
                    jsonObject.setValue(product.barcode, forKey: "barcode")
                    jsonObject.setValue(product.quantityPerUnit, forKey: "quantityPerUnit")
                    jsonObject.setValue(NSNumber(value: product.saleUnitPrice), forKey: "saleUnitPrice")
                    jsonObject.setValue(NSNumber(value: product.incomeUnitPrice), forKey: "incomeUnitPrice")
                    jsonObject.setValue(NSNumber(value: product.unitsInStock), forKey: "unitsInStock")
                    
                    productJsons.append(jsonObject)
                }
                
                let jsonData: NSData
                
                do {
                    jsonData = try JSONSerialization.data(withJSONObject: productJsons, options: JSONSerialization.WritingOptions()) as NSData
                    let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String
                    //print("json string = \(jsonString)")
                    
                    let local_path = "products.json"
                    let tempPath = NSTemporaryDirectory().stringByAppendingPathComponent(path: local_path)
                    self.url = NSURL.fileURL(withPath: tempPath)
                    
                    do {
                        try jsonString.write(to: self.url, atomically: false, encoding: String.Encoding.utf8)
                    }
                    catch {
                        print ("write Failure")
                    }
                    
                } catch _ {
                    print ("JSON Failure")
                }
                
                
                DispatchQueue.main.async {
                    self.isGeneratingCSV = false
                    completion(self.url)
                }
            }
        }
    }
    
    func generateJsonFileFromCategories(categories: [Category],
                                        progress:@escaping (Float) -> (),
                                        completion:@escaping (URL) -> ()) -> Void {
        
        if self.isGeneratingCSV {
            return
        }
        
        if !self.isGeneratingCSV {
            self.isGeneratingCSV = true
            
            generateQue.async {
                
                var productJsons: [NSMutableDictionary] = []
                for category in categories {
                    let jsonObject: NSMutableDictionary = NSMutableDictionary()
                    jsonObject.setValue(NSNumber(value: category.categoryId), forKey: "categoryId")
                    jsonObject.setValue(category.categoryName, forKey: "categoryName")
                    productJsons.append(jsonObject)
                }
                
                let jsonData: NSData
                
                do {
                    jsonData = try JSONSerialization.data(withJSONObject: productJsons, options: JSONSerialization.WritingOptions()) as NSData
                    let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String
                    
                    let local_path = "categories.json"
                    let tempPath = NSTemporaryDirectory().stringByAppendingPathComponent(path: local_path)
                    self.url = NSURL.fileURL(withPath: tempPath)
                    
                    do {
                        try jsonString.write(to: self.url, atomically: false, encoding: String.Encoding.utf8)
                    }
                    catch {
                        print ("write Failure")
                    }
                    
                } catch _ {
                    print ("JSON Failure")
                }
                
                DispatchQueue.main.async {
                    self.isGeneratingCSV = false
                    completion(self.url)
                }
            }
        }
    }
    
    func generateJsonFileFromCustomers(customers: [Customer],
                                       progress:@escaping (Float) -> (),
                                       completion:@escaping (URL) -> ()) -> Void {
        
        if self.isGeneratingCSV {
            return
        }
        
        if !self.isGeneratingCSV {
            self.isGeneratingCSV = true
            
            generateQue.async {
                
                var productJsons: [NSMutableDictionary] = []
                for customer in customers {
                    //var jsonObject: [Any: Any] = [:]
                    let jsonObject: NSMutableDictionary = NSMutableDictionary()
                    jsonObject.setValue(NSNumber(value: customer.customerId), forKey: "customerId")
                    jsonObject.setValue(customer.customerImagePath, forKey: "customerImagePath")
                    jsonObject.setValue(customer.firstName, forKey: "firstName")
                    jsonObject.setValue(customer.secondName, forKey: "secondName")
                    jsonObject.setValue(customer.phoneNumber, forKey: "phoneNumber")
                    jsonObject.setValue(customer.address, forKey: "address")
                    jsonObject.setValue(customer.userId, forKey: "userId")
                    productJsons.append(jsonObject)
                }
                
                let jsonData: NSData
                
                do {
                    jsonData = try JSONSerialization.data(withJSONObject: productJsons, options: JSONSerialization.WritingOptions()) as NSData
                    let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String
                    //print("json string = \(jsonString)")
                    
                    let local_path = "customers.json"
                    let tempPath = NSTemporaryDirectory().stringByAppendingPathComponent(path: local_path)
                    self.url = NSURL.fileURL(withPath: tempPath)
                    
                    do {
                        try jsonString.write(to: self.url, atomically: false, encoding: String.Encoding.utf8)
                    }
                    catch {
                        print ("write Failure")
                    }
                    
                } catch _ {
                    print ("JSON Failure")
                }
                
                
                DispatchQueue.main.async {
                    self.isGeneratingCSV = false
                    completion(self.url)
                }
            }
        }
    }
    
}
