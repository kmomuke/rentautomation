//
//  SynchronizeManager.swift
//  Rentautomation
//
//  Created by kanybek on 1/18/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit
import Whisper
import grpcPod
import SVProgressHUD

final class Synchronizer {
    
    static let syncQueue = Queue(name: "sync_queue")
    
    var initialDataDispose: DisposableSet?
    var syncingDispose: Disposable?
    
    var isSynchronizing: Bool
    var synchronizationTimer: ParkBenchTimer?
    
    static let shared: Synchronizer = Synchronizer()
    
    init() {
        self.isSynchronizing = false
    }
    
    func stopAllSyncProcess() -> Void {

        self.syncingDispose?.dispose()
        self.syncingDispose = nil
        
        self.initialDataDispose?.dispose()
        self.initialDataDispose = nil
        
        Synchronizer.dismissWisperNotificationView()
        self.isSynchronizing = false
        SVProgressHUD.dismiss()
    }
    
    func synchronizeAllNeededOperations(completionHandler:@escaping (Bool) -> ()) -> Void {
        
        if !ReachabilityManager.shared.isConnectedToInternet() {
            Synchronizer.showAutoWisperNotificationView("Нет соединение с интернетом")
            return
        }
        
        if self.isSynchronizing {
            return
        }
        
        Synchronizer.showWisperNotificationView("Синхронизация данных ...")
        
        self.isSynchronizing = true
        
        let operations = Database.shared.allUnSyncedOperations()
        for operation in operations {
            let date_ = operation.syncOperationDate.toString(.custom("EEE, dd MMM HH:mm:ss"))
            let name_ = Event.syncOperationTypeStringName(syncType: operation.operationType)
            print("operation date is: \(date_)   \(name_)")
        }
        
        var signalOperations: [Signal<Bool, Error>] = []
        
        for operation in operations {
            
            switch operation.operationType {
            
            case .productCreated:
                if let createdProductSignal = self.createProductOperation(operation) {
                    signalOperations.append(createdProductSignal)
                }
                
            case .productUpdated:
                if let updateProductSignal = self.updateProductOperation(operation) {
                    signalOperations.append(updateProductSignal)
                }
            
            case .customerCreated:
                if let createCustomerSignal = self.createCustomerOperation(operation) {
                    signalOperations.append(createCustomerSignal)
                }
               
            case .customerUpdated:
                if let updateCustomerSignal = self.updateCustomerOperation(operation) {
                    signalOperations.append(updateCustomerSignal)
                }
                
            case .customerBalanceUpdated:
                if let updateCustomerBalanceSignal = self.updateCustomerBalanceOperation(operation) {
                    signalOperations.append(updateCustomerBalanceSignal)
                }
                
            case .supplierCreated:
                if let createSupplierSignal = self.createSupplierOperation(operation) {
                    signalOperations.append(createSupplierSignal)
                }
                
            case .supplierUpdated:
                if let updateSupplierSignal = self.updateSupplierOperation(operation) {
                    signalOperations.append(updateSupplierSignal)
                }
                
            case .supplierBalanceUpdated:
                if let updateSupplierBalanceSignal = self.updateSupplierBalanceOperation(operation) {
                    signalOperations.append(updateSupplierBalanceSignal)
                }
                
            case .staffCreated:
                if let createUserSignal = self.createUserOperation(operation) {
                    signalOperations.append(createUserSignal)
                }
                
            case .staffUpdated:
                if let updateStaffSignal = self.updateUserOperation(operation) {
                    signalOperations.append(updateStaffSignal)
                }
                
            case .categoryCreated:
                if let createCategorySignal = self.createCategoryOperation(operation) {
                    signalOperations.append(createCategorySignal)
                }
                
            case .categoryUpdated:
                if let updateCategorySignal = self.updateCategoryOperation(operation) {
                    signalOperations.append(updateCategorySignal)
                }
                
            case .orderCreated:
                if let createOrderSignal = self.createOrderOperation(operation) {
                    signalOperations.append(createOrderSignal)
                }
                
            case .orderUpdated:
                if let updateOrderSignal = self.updateOrderOperation(operation) {
                    signalOperations.append(updateOrderSignal)
                }
                
            case .transactionUpdated:
                if let updateTransactionSignal = self.updateTransactionOperation(operation) {
                    signalOperations.append(updateTransactionSignal)
                }
                
            case .none:
                print("none")
                
            default:
                print("default")
            }
        }
        
        var startingSignal = Signal<Bool, Error> { subscriber in
            print("starting synchronizeAllNeededOperations queue:\(RPCNetwork.currentQueueName())")
            subscriber.putNext(true)
            subscriber.putCompletion()
            return ActionDisposable {
                print("startingSignal disposed")
            }
        }
        
        for signal in signalOperations.prefix(500) {
            startingSignal = startingSignal |> then(signal)
        }
        
        var customersUpdateSignal = single(true, Error.self) |> then(self.getAllInitialData())
        
        if let lastSyncedDate = AppSettings.shared.lastSavedDate() {
            if true {
                customersUpdateSignal = customersUpdateSignal
                    |> then(self.checkForOrdersUpdatedAt(orderUpdatedAt: lastSyncedDate))
                    |> then(self.checkCustomersForUpdate(customerUpdatedAt: lastSyncedDate))
                    |> then(self.checkSuppliersForUpdate(supplierUpdatedAt: lastSyncedDate))
                    |> then(self.checkUsersForUpdate(staffUpdatedAt: lastSyncedDate))
                    |> then(self.checkCategoriesForUpdate(categoryUpdatedAt: lastSyncedDate))
                    |> then(self.checkProductsForUpdate(productUpdatedAt: lastSyncedDate))
                    |> then(self.checkForTransactionsUpdatedAt(transactionUpdatedAt: lastSyncedDate))
            }
        }
        
        //let finalSignal = combineLatest(signalOperations) |> runOn(SynchronizeManager.syncQueue)
        let finalSignal = customersUpdateSignal |> then(startingSignal) |> runOn(Synchronizer.syncQueue)
        self.synchronizationTimer = ParkBenchTimer()
        self.syncingDispose = finalSignal.start(next: { (b: Bool) in
            print("one signal finished \(b)")
            DispatchQueue.main.async {
                PipesStore.shared.syncFinishedPipe.putNext(true)
            }
        }, error: {[weak self] (error: Error) in
            self?.isSynchronizing = false
            print("update signal finished with error")
            DispatchQueue.main.async {
                Synchronizer.showAutoWisperNotificationView("\(error.localizedDescription)")
                completionHandler(false)
            }
        }, completed: {[weak self] (Void) in
            self?.isSynchronizing = false
            DispatchQueue.main.async {
                AppSettings.shared.saveDate(date: Date())
                Synchronizer.dismissWisperNotificationView()
                PipesStore.shared.allSyncOperationsFinishedPipe.putNext(true)
                print("Synchronization finished, took \(String(describing: self?.synchronizationTimer?.stop())) seconds.")
                completionHandler(true)
            }
        })
    }
    
    // MARK:Whisper
    static func showWisperNotificationView(_ text: String) -> Void {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            if let rootNVC = appDelegate.rootNavController {
                let message = Message(title: text,
                                      textColor: UIColor.white,
                                      backgroundColor: UIColor.carrotColor(),
                                      images: nil)
                Whisper.show(whisper: message, to: rootNVC, action: .present)
                hide(whisperFrom: rootNVC, after: 250.0)
            }
        }
    }
    
    static func showAutoWisperNotificationView(_ text: String) -> Void {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            if let rootNVC = appDelegate.rootNavController {
                
                let message = Message(title: text,
                                      textColor: UIColor.white,
                                      backgroundColor: UIColor.red,
                                      images: nil)
                Whisper.show(whisper: message, to: rootNVC, action: .present)
                
                hide(whisperFrom: rootNVC, after: 2.0)
            }
        }
    }
    
    static func dismissWisperNotificationView() -> Void {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            if let rootNVC = appDelegate.rootNavController {
                hide(whisperFrom: rootNVC)
            }
        }
    }
    
    // MARK:All Data
    func getAllInitialData() -> Signal<Bool, Error> {
        
        if Database.shared.loadAllProducts(10).count == 0 &&
            Database.shared.loadAllCustomers(10).count == 0
        {
            return self.getInitialStaticModelsSignal()
        } else {
            return Signal<Bool, Error> { subscriber in
                subscriber.putNext(true)
                subscriber.putCompletion()
                return ActionDisposable {}
            }
        }
    }
    
    func getInitialStaticModels(completion:@escaping (Bool) -> ()) -> Void {
        
        self.initialDataDispose?.dispose()
        self.initialDataDispose = nil
        
        let finalSignal = self.getInitialStaticModelsSignal()
        let testDispose = finalSignal.start(next: { (response: Bool) in
            print("response: \(response)")
        }, error: { (error: Error) in
            SVProgressHUD.showError(withStatus: error.localizedDescription)
        }, completed: { (Void) in
            completion(true)
            SVProgressHUD.dismiss()
        })
        
        self.initialDataDispose = DisposableSet()
        self.initialDataDispose?.add(testDispose)
    }
    
    private func getInitialStaticModelsSignal() -> Signal<Bool, Error> {
        
        var signalOperations: [Signal<Bool, Error>] = []
        
        let allProdSignal = self.getAllNewProducts()
        signalOperations.append(allProdSignal)
        
        let getCategoriesSignal = self.getAllCategories()
        signalOperations.append(getCategoriesSignal)
        
        let allStaffSignal = self.getAllUsers()
        signalOperations.append(allStaffSignal)
        
        let getOrderDetails = self.getOrderDetailsForFilter(productId: 0,
                                                            date: Date(),
                                                            limit: 1000)
        signalOperations.append(getOrderDetails)
        
        let allCustInitial = self.getAllCustomers()
        signalOperations.append(allCustInitial)
        
        let allSuppInitial = self.getAllSuppliers()
        signalOperations.append(allSuppInitial)
        
        let transactionsForFilter = self.getTransactionsForFilter(customerId: 0,
                                                                  supplierId: 0,
                                                                  date: Date(),
                                                                  limit: 500)
        signalOperations.append(transactionsForFilter)
        
        let ordersForFilter: Signal<Bool, Error>
        if let lastOrder = Database.shared.lastOrder() {
            ordersForFilter = self.getOrdersForFilter(date: lastOrder.orderDate,
                                                      limit: 3000,
                                                      isForInitialState: true)
        } else {
            ordersForFilter = self.getOrdersForFilter(date: Date(),
                                                      limit: 3000,
                                                      isForInitialState: true)
        }
        
        signalOperations.append(ordersForFilter)
        
        var startingSignal = Signal<Bool, Error> { subscriber in
            print("starting get")
            subscriber.putNext(true)
            subscriber.putCompletion()
            return ActionDisposable {}
        }
        
        for signal in signalOperations {
            startingSignal = startingSignal |> then(signal)
        }
        
        let finalSignal = startingSignal |> runOn(Synchronizer.syncQueue) |> deliverOn(Queue.mainQueue())
        return finalSignal
    }
    
    // MARK:PRODUCT
    func updateProductOperation(_ operation: Event) -> Signal<Bool, Error>? {
        if let rpcSignal = self.updateProductSignal(operation: operation) {
            
            let databaseSignal = rpcSignal |> mapToSignal({ (orderDetID: UInt64) -> Signal<Bool, Error> in
                
                let signal = Signal<Bool, Error> { subscriber in
                    
                    Database.shared.updateOrderDetailsPrimaryKey(oldId: operation.orderDetailId, newId: orderDetID)
                    Database.shared.markOperationAsSynced(operationId: operation.syncOperationId)
                    subscriber.putNext(true)
                    subscriber.putCompletion()
                    return ActionDisposable {}
                }
                return signal
            })
            
                    
            return databaseSignal
        }
        
        return nil
    }
    
    private func updateProductSignal(operation: Event) -> Signal<UInt64, Error>? {
        
        if let product_ = Database.shared.productFor(productId: operation.productId),
            let orderDetail_ = Database.shared.orderDetailBy(orderDetailId: operation.orderDetailId)
        {
            return RPCNetwork.shared.updateProductRPC(product: product_,
                                                      orderDetail: orderDetail_)
        }
        
        return nil
    }
    
    func createProductOperation(_ operation: Event) -> Signal<Bool, Error>? {
        
        if let rpcSignal = self.createProductSignal(operation: operation) {
            
            let databaseSignal = rpcSignal |> mapToSignal({ (prodId: UInt64, orderDetID: UInt64) -> Signal<Bool, Error> in
                
                let signal = Signal<Bool, Error> { subscriber in
                    
                    Database.shared.updateProducsPrimaryKey(oldProductId: operation.productId, newProduct: prodId)
                    Database.shared.updateOrderDetailsPrimaryKey(oldId: operation.orderDetailId, newId: orderDetID)
                    
                    Database.shared.updateOrderDetailsProductId(orderDetailId: orderDetID, productId: prodId)
                    
                    Database.shared.markOperationAsSynced(operationId: operation.syncOperationId)
                    
                    subscriber.putNext(true)
                    subscriber.putCompletion()
                    return ActionDisposable {}
                }
                
                return signal
            })
            
                    
            return databaseSignal
        }
        return nil
    }
    
    private func createProductSignal(operation: Event) -> Signal<(UInt64, UInt64), Error>? {
        
        if let product_ = Database.shared.productFor(productId: operation.productId),
            let orderDetail_ = Database.shared.orderDetailBy(orderDetailId: operation.orderDetailId)
        {
            return RPCNetwork.shared.createProductRPC(product: product_,
                                                      orderDetail: orderDetail_)
        }
        
        return nil
    }
    
    private func getAllNewProducts() -> Signal<Bool, Error> {
        
        let allProdSignal = RPCNetwork.shared.allProductsRPC()
        let allProdDatabaseSignal = allProdSignal |> mapToSignal({ (allProds: [CreateProductRequest]) -> Signal<Bool, Error> in
            
            let signal = Signal<Bool, Error> { subscriber in
                
                print("------------------------")
                print("Receive all products")
                
                var products: [Product] = []
                var orderDetails: [OrderDetail] = []
                
                for createProductRequest in allProds {
                    if let productRequest = createProductRequest.product {
                        let product = ProtoToModel.productFrom(productRequest)
                        products.append(product)
                    }
                    
                    if let orderDetailRequest = createProductRequest.orderDetail {
                        let orderDetail = ProtoToModel.orderDetailFrom(orderDetailRequest)
                        orderDetails.append(orderDetail)
                    }
                }
                
                Database.shared.createProducts(products: products)
                Database.shared.createorderDetails(orderDetails)
                
                subscriber.putNext(true)
                subscriber.putCompletion()
                return ActionDisposable {}
            }
            return signal
        })
        return allProdDatabaseSignal
    }
    
    private func checkProductsForUpdate(productUpdatedAt: Date) -> Signal<Bool, Error> {
        
        let allProdSignal = RPCNetwork.shared.checkProductsForUpdateRPC(productUpdatedAt: productUpdatedAt)
        let allProdDatabaseSignal = allProdSignal |> mapToSignal({ (allProds: [CreateProductRequest]) -> Signal<Bool, Error> in
            
            let signal = Signal<Bool, Error> { subscriber in
                
                print("------------------------")
                print("Receive all products")
                
                var products: [Product] = []
                var orderDetails: [OrderDetail] = []
                
                for createProductRequest in allProds {
                    if let productRequest = createProductRequest.product {
                        let product = ProtoToModel.productFrom(productRequest)
                        products.append(product)
                    }
                    if let orderDetailRequest = createProductRequest.orderDetail {
                        let orderDetail = ProtoToModel.orderDetailFrom(orderDetailRequest)
                        orderDetails.append(orderDetail)
                    }
                }
                
                Database.shared.createProducts(products: products)
                Database.shared.createorderDetails(orderDetails)
                
                subscriber.putNext(true)
                subscriber.putCompletion()
                return ActionDisposable {}
            }
            return signal
        })
        return allProdDatabaseSignal
    }
    
    // MARK:CUSTOMER
    // --------------------- CREATE CUSTOMER --------------------- //
    func createCustomerOperation(_ operation: Event) -> Signal<Bool, Error>? {
        
        if let rpcSignal = self.createCustomerSignal(operation: operation) {
            
            let databaseSignal = rpcSignal |> mapToSignal({ (customerId: UInt64, transactionId: UInt64, accountId: UInt64) -> Signal<Bool, Error> in
                
                let signal = Signal<Bool, Error> { subscriber in
                    
                    // ------------------------------------------------
                    Database.shared.updateSyncOperationsCustomerId(oldCustomerId: operation.customerId,
                                                                   newCustomerId: customerId)
                    Database.shared.updateOrdersCustomerId(oldCustomerId: operation.customerId,
                                                           newCustomerId: customerId)
                    Database.shared.updateTransactionsCustomerId(oldCustomerId: operation.customerId,
                                                                 newCustomerId: customerId)
                    // ------------------------------------------------
                    
                    
                    Database.shared.updateCustomersPrimaryKey(oldCustomerId: operation.customerId, newCustomerId: customerId)
                    Database.shared.updateTransactionsPrimaryKey(oldId: operation.transactionId, newId: transactionId)
                    Database.shared.updateAccountsPrimaryKey(oldId: operation.accountId, newId: accountId)
                    
                    Database.shared.updateTransactionsCustomerId(transactionId: transactionId, customerId: customerId)
                    Database.shared.updateAccountsCustomerId(accountId: accountId, customerId: customerId)
                    
                    Database.shared.markOperationAsSynced(operationId: operation.syncOperationId)
                    
                    subscriber.putNext(true)
                    subscriber.putCompletion()
                    return ActionDisposable {}
                }
                
                return signal
            })
            
                    
            return databaseSignal
        }
        return nil
    }
    
    private func createCustomerSignal(operation: Event) -> Signal<(UInt64, UInt64, UInt64), Error>? {
        
        if let customer_ = Database.shared.customerBy(customerId: operation.customerId),
            let transaction_ = Database.shared.transactionBy(transactionId: operation.transactionId),
            let account_ = Database.shared.accountBy(accountId: operation.accountId)
        {
            return RPCNetwork.shared.createCustomerRPC(customer: customer_,
                                                       transaction: transaction_,
                                                       account: account_)
        }
        
        return nil
    }
    
    // --------------------- UPDATE CUSTOMER --------------------- //
    func updateCustomerOperation(_ operation: Event) -> Signal<Bool, Error>? {
        
        if let rpcSignal = self.updateCustomerSignal(operation: operation) {
            
            let databaseSignal = rpcSignal |> mapToSignal({ (value: Bool) -> Signal<Bool, Error> in
                
                let signal = Signal<Bool, Error> { subscriber in
                    Database.shared.markOperationAsSynced(operationId: operation.syncOperationId)
                    subscriber.putNext(true)
                    subscriber.putCompletion()
                    return ActionDisposable {}
                }
                
                return signal
            })
            
                    
            return databaseSignal
        }
        return nil
    }
    
    private func updateCustomerSignal(operation: Event) -> Signal<Bool, Error>? {
        
        if let customer_ = Database.shared.customerBy(customerId: operation.customerId) {
            return RPCNetwork.shared.updateCustomerRPC(customer: customer_)
        }
        return nil
    }
    
    private func updateCustomerBalanceOperation(_ operation: Event) -> Signal<Bool, Error>? {
        
        if let rpcSignal = self.updateCustomerBalanceSignal(operation: operation) {
            
            let databaseSignal = rpcSignal |> mapToSignal({ (transactionId: UInt64) -> Signal<Bool, Error> in
                
                let signal = Signal<Bool, Error> { subscriber in
                    
                    Database.shared.updateTransactionsPrimaryKey(oldId: operation.transactionId, newId: transactionId)
                    Database.shared.markOperationAsSynced(operationId: operation.syncOperationId)
                    
                    subscriber.putNext(true)
                    subscriber.putCompletion()
                    return ActionDisposable {}
                }
                
                return signal
            })
            
                    
            return databaseSignal
        }
        return nil
    }
    
    private func updateCustomerBalanceSignal(operation: Event) -> Signal<UInt64, Error>? {
        
        if let transaction_ = Database.shared.transactionBy(transactionId: operation.transactionId),
           let account_ = Database.shared.accountBy(accountId: operation.accountId)
        {
            return RPCNetwork.shared.updateCustomerBalanceRPC(transaction: transaction_,
                                                              account: account_)
        }
        return nil
    }
    
    private func checkCustomersForUpdate(customerUpdatedAt: Date) -> Signal<Bool, Error> {
        
        let customersForUpdate = RPCNetwork.shared.checkCustomersForUpdateRPC(customerUpdatedAt: customerUpdatedAt)
        let allCustInitialSignal = customersForUpdate |> mapToSignal({ (createCustReqs: [CreateCustomerRequest]) -> Signal<Bool, Error> in
            
            let signal = Signal<Bool, Error> { subscriber in
                
                print("------------------------")
                print("Received updated customers count: \(createCustReqs.count)")
                
                var customers: [Customer] = []
                var transactions: [Transaction] = []
                var accounts: [Account] = []
                
                for createCustomerRequest in createCustReqs {
                    if let customerReq = createCustomerRequest.customer {
                        let customer = ProtoToModel.customerFrom(customerReq)
                        customers.append(customer)
                    }
                    if let accountRequest = createCustomerRequest.account {
                        let account = ProtoToModel.accountFrom(accountRequest)
                        accounts.append(account)
                    }
                    if let transactionReq = createCustomerRequest.transaction {
                        let transaction = ProtoToModel.transactionFrom(transactionReq)
                        transactions.append(transaction)
                    }
                }
                
                Database.shared.createCustomers(customers)
                Database.shared.createTransactions(transactions)
                for account in accounts {
                    if let _ = Database.shared.accountBy(accountId: account.accountId) {
                    } else {
                        Database.shared.createAccounts(accounts)
                    }
                }
                
                subscriber.putNext(true)
                subscriber.putCompletion()
                return ActionDisposable {}
            }
            return signal
        })
        return allCustInitialSignal
    }
    
    private func getAllCustomers() -> Signal<Bool, Error> {
        
        let allCustInitial = RPCNetwork.shared.allCustomersForInitialStateRPC()
        let allCustInitialSignal = allCustInitial |> mapToSignal({ (createCustReqs: [CreateCustomerRequest]) -> Signal<Bool, Error> in
            
            let signal = Signal<Bool, Error> { subscriber in
                
                print("------------------------")
                print("Receive all customers")
                
                var customers: [Customer] = []
                var transactions: [Transaction] = []
                var accounts: [Account] = []
                
                for createCustomerRequest in createCustReqs {
                    if let customerReq = createCustomerRequest.customer {
                        let customer = ProtoToModel.customerFrom(customerReq)
                        customers.append(customer)
                    }
                    if let accountRequest = createCustomerRequest.account {
                        let account = ProtoToModel.accountFrom(accountRequest)
                        accounts.append(account)
                    }
                    if let transactionReq = createCustomerRequest.transaction {
                        let transaction = ProtoToModel.transactionFrom(transactionReq)
                        transactions.append(transaction)
                    }
                }
                
                Database.shared.createCustomers(customers)
                Database.shared.createAccounts(accounts)
                Database.shared.createTransactions(transactions)
                
                subscriber.putNext(true)
                subscriber.putCompletion()
                return ActionDisposable {}
            }
            return signal
        })
        return allCustInitialSignal
    }
    
    
    // MARK:SUPPLIER
    // --------------------- CREATE SUPPLIER --------------------- //
    private func createSupplierOperation(_ operation: Event) -> Signal<Bool, Error>? {
        
        if let rpcSignal = self.createSupplierSignal(operation: operation) {
            
            let databaseSignal = rpcSignal |> mapToSignal({ (supplierId: UInt64, transactionId: UInt64, accountId: UInt64) -> Signal<Bool, Error> in
                
                let signal = Signal<Bool, Error> { subscriber in
                    
                    Database.shared.updateSuppliersPrimaryKey(oldSupplierId: operation.supplierId, newSupplierId: supplierId)
                    Database.shared.updateTransactionsPrimaryKey(oldId: operation.transactionId, newId: transactionId)
                    Database.shared.updateAccountsPrimaryKey(oldId: operation.accountId, newId: accountId)

                    Database.shared.updateTransactionsSupplierId(transactionId: transactionId, supplierId: supplierId)
                    Database.shared.updateAccountsSupplierId(accountId: accountId, supplierId: supplierId)
                    
                    Database.shared.markOperationAsSynced(operationId: operation.syncOperationId)
                    
                    subscriber.putNext(true)
                    subscriber.putCompletion()
                    return ActionDisposable {}
                }
                
                return signal
            })
            
                    
            return databaseSignal
        }
        return nil
    }
    
    private func createSupplierSignal(operation: Event) -> Signal<(UInt64, UInt64, UInt64), Error>? {
        
        if let supplier_ = Database.shared.supplierBy(supplierId: operation.supplierId),
            let transaction_ = Database.shared.transactionBy(transactionId: operation.transactionId),
            let account_ = Database.shared.accountBy(accountId: operation.accountId)
        {
            return RPCNetwork.shared.createSupplierRPC(supplier: supplier_,
                                                transaction: transaction_,
                                                account: account_)
        }
        
        return nil
    }
    
    // --------------------- UPDATE SUPPLIER --------------------- //
    func updateSupplierOperation(_ operation: Event) -> Signal<Bool, Error>? {
        
        if let rpcSignal = self.updateSupplierSignal(operation: operation) {
            
            let databaseSignal = rpcSignal |> mapToSignal({ (value: Bool) -> Signal<Bool, Error> in
                
                let signal = Signal<Bool, Error> { subscriber in
                    Database.shared.markOperationAsSynced(operationId: operation.syncOperationId)
                    subscriber.putNext(true)
                    subscriber.putCompletion()
                    return ActionDisposable {}
                }
                
                return signal
            })
            
                    
            return databaseSignal
        }
        return nil
    }
    
    private func updateSupplierSignal(operation: Event) -> Signal<Bool, Error>? {
        
        if let supplier_ = Database.shared.supplierBy(supplierId: operation.supplierId) {
            return RPCNetwork.shared.updateSupplierRPC(supplier: supplier_)
        }
        return nil
    }
    
    // --------------------- UPDATE SUPPLIER BALLANCE --------------------- //
    func updateSupplierBalanceOperation(_ operation: Event) -> Signal<Bool, Error>? {
        
        if let rpcSignal = self.updateSupplierBalanceSignal(operation: operation) {
            
            let databaseSignal = rpcSignal |> mapToSignal({ (transactionId: UInt64) -> Signal<Bool, Error> in
                
                let signal = Signal<Bool, Error> { subscriber in
                    
                    Database.shared.updateTransactionsPrimaryKey(oldId: operation.transactionId, newId: transactionId)
                    Database.shared.markOperationAsSynced(operationId: operation.syncOperationId)
                    
                    subscriber.putNext(true)
                    subscriber.putCompletion()
                    return ActionDisposable {}
                }
                
                return signal
            })
            
            return databaseSignal
        }
        return nil
    }
    
    private func updateSupplierBalanceSignal(operation: Event) -> Signal<UInt64, Error>? {
        
        if let transaction_ = Database.shared.transactionBy(transactionId: operation.transactionId),
            let account_ = Database.shared.accountBy(accountId: operation.accountId)
        {
            return RPCNetwork.shared.updateSupplierBalanceRPC(transaction: transaction_,
                                                              account: account_)
        }
        return nil
    }
    
    private func getAllSuppliers() -> Signal<Bool, Error> {
        let allSuppInitial = RPCNetwork.shared.allSuppliersForInitialStateRPC()
        let allSuppInitialSignal = allSuppInitial |> mapToSignal({ (createSuppReqs: [CreateSupplierRequest]) -> Signal<Bool, Error> in
            
            let signal = Signal<Bool, Error> { subscriber in
                
                print("------------------------")
                print("Receive all suppliers")
                
                var suppliers: [Supplier] = []
                var accounts: [Account] = []
                var transactions: [Transaction] = []
                
                for createSupplierRequest in createSuppReqs {
                    if let supplierReq = createSupplierRequest.supplier {
                        let supplier = ProtoToModel.supplierFrom(supplierReq)
                        suppliers.append(supplier)
                    }
                    if let accountRequest = createSupplierRequest.account {
                        let account = ProtoToModel.accountFrom(accountRequest)
                        accounts.append(account)
                    }
                    if let transactionReq = createSupplierRequest.transaction {
                        let transaction = ProtoToModel.transactionFrom(transactionReq)
                        transactions.append(transaction)
                    }
                }
                
                Database.shared.createSuppliers(suppliers)
                Database.shared.createAccounts(accounts)
                Database.shared.createTransactions(transactions)
                
                subscriber.putNext(true)
                subscriber.putCompletion()
                return ActionDisposable {}
            }
            return signal
        })
        return allSuppInitialSignal
    }
    
    private func checkSuppliersForUpdate(supplierUpdatedAt: Date) -> Signal<Bool, Error> {
        let allSuppInitial = RPCNetwork.shared.checkSuppliersForUpdateRPC(supplierUpdatedAt: supplierUpdatedAt)
        let allSuppInitialSignal = allSuppInitial |> mapToSignal({ (createSuppReqs: [CreateSupplierRequest]) -> Signal<Bool, Error> in
            
            let signal = Signal<Bool, Error> { subscriber in
                
                print("------------------------")
                print("Receive all suppliers")
                
                var suppliers: [Supplier] = []
                var accounts: [Account] = []
                var transactions: [Transaction] = []
                
                for createSupplierRequest in createSuppReqs {
                    if let supplierReq = createSupplierRequest.supplier {
                        let supplier = ProtoToModel.supplierFrom(supplierReq)
                        suppliers.append(supplier)
                    }
                    if let accountRequest = createSupplierRequest.account {
                        let account = ProtoToModel.accountFrom(accountRequest)
                        accounts.append(account)
                    }
                    if let transactionReq = createSupplierRequest.transaction {
                        let transaction = ProtoToModel.transactionFrom(transactionReq)
                        transactions.append(transaction)
                    }
                }
                
                Database.shared.createSuppliers(suppliers)
                Database.shared.createTransactions(transactions)
                for account in accounts {
                    if let _ = Database.shared.accountBy(accountId: account.accountId) {
                    } else {
                        Database.shared.createAccounts(accounts)
                    }
                }
                
                subscriber.putNext(true)
                subscriber.putCompletion()
                return ActionDisposable {}
            }
            return signal
        })
        return allSuppInitialSignal
    }
    
    
    // MARK:STAFF
    // --------------------- CREATE STAFF --------------------- //
    func createUserOperation(_ operation: Event) -> Signal<Bool, Error>? {
        
        if let rpcSignal = self.createUserSignal(operation: operation) {
            
            let databaseSignal = rpcSignal |> mapToSignal({ (userId: UInt64) -> Signal<Bool, Error> in
                
                let signal = Signal<Bool, Error> { subscriber in
                    
                    Database.shared.updateUsersPrimaryKey(oldUserId: operation.staffId, newUserId: userId)
                    Database.shared.markOperationAsSynced(operationId: operation.syncOperationId)
                    
                    subscriber.putNext(true)
                    subscriber.putCompletion()
                    return ActionDisposable {}
                }
                
                return signal
            })
            
            return databaseSignal
        }
        return nil
    }
    
    private func createUserSignal(operation: Event) -> Signal<UInt64, Error>? {
        if let user_ = Database.shared.userBy(userId: operation.staffId) {
            return RPCNetwork.shared.createUserRPC(user: user_)
        }
        return nil
    }
    
    
    // --------------------- UPDATE STAFF --------------------- //
    func updateUserOperation(_ operation: Event) -> Signal<Bool, Error>? {
        
        if let rpcSignal = self.updateUserSignal(operation: operation) {
            
            let databaseSignal = rpcSignal |> mapToSignal({ (boolValue: Bool) -> Signal<Bool, Error> in
                
                let signal = Signal<Bool, Error> { subscriber in
                    
                    if boolValue {
                        Database.shared.markOperationAsSynced(operationId: operation.syncOperationId)
                    }

                    subscriber.putNext(true)
                    subscriber.putCompletion()
                    return ActionDisposable {}
                }
                
                return signal
            })
                    
            return databaseSignal
        }
        return nil
    }
    
    private func updateUserSignal(operation: Event) -> Signal<Bool, Error>? {
        if let user_ = Database.shared.userBy(userId: operation.staffId) {
            return RPCNetwork.shared.updateUserRPC(user: user_)
        }
        return nil
    }
    
    private func getAllUsers() -> Signal<Bool, Error> {
        
        let getUsersSignal = RPCNetwork.shared.allUsersRPC()
        let allCategoriesDatabaseSignal = getUsersSignal |> mapToSignal({ (usersReqs: [UserRequest]) -> Signal<Bool, Error> in
            
            let signal = Signal<Bool, Error> { subscriber in
                
                var users: [User] = []
                for userRequest in usersReqs {
                    let user = ProtoToModel.userFrom(userRequest)
                    users.append(user)
                }
                Database.shared.createUser(users)
                
                subscriber.putNext(true)
                subscriber.putCompletion()
                return ActionDisposable {}
            }
            return signal
        })
        return allCategoriesDatabaseSignal
    }
    
    private func checkUsersForUpdate(staffUpdatedAt: Date) -> Signal<Bool, Error> {
        
        let getUsersSignal = RPCNetwork.shared.checkUsersForUpdateRPC(userUpdatedAt: staffUpdatedAt)
        let allCategoriesDatabaseSignal = getUsersSignal |> mapToSignal({ (userRequests: [UserRequest]) -> Signal<Bool, Error> in
            
            let signal = Signal<Bool, Error> { subscriber in
                
                var users: [User] = []
                for userRequest in userRequests {
                    let user = ProtoToModel.userFrom(userRequest)
                    users.append(user)
                }
                Database.shared.createUser(users)
                
                subscriber.putNext(true)
                subscriber.putCompletion()
                return ActionDisposable {}
            }
            return signal
        })
        return allCategoriesDatabaseSignal
    }


    // MARK:CATEGORY
    // --------------------- CREATE CATEGORY --------------------- //
    func createCategoryOperation(_ operation: Event) -> Signal<Bool, Error>? {
        
        if let rpcSignal = self.createCategorySignal(operation: operation) {
            
            let databaseSignal = rpcSignal |> mapToSignal({ (categoryId: UInt64) -> Signal<Bool, Error> in
                
                let signal = Signal<Bool, Error> { subscriber in
                    
                    Database.shared.updateCategorysPrimaryKey(oldCategoryId: operation.categoryId, newCategoryId: categoryId)
                    Database.shared.updateProductsCategoryId(oldCategoryId: operation.categoryId, newCategoryId: categoryId)
                    Database.shared.markOperationAsSynced(operationId: operation.syncOperationId)
                    
                    subscriber.putNext(true)
                    subscriber.putCompletion()
                    return ActionDisposable {}
                }
                
                return signal
            })
            
            return databaseSignal
        }
        return nil
    }
    
    private func createCategorySignal(operation: Event) -> Signal<UInt64, Error>? {
        
        if let category_ = Database.shared.categoryBy(categoryId: operation.categoryId) {
            return RPCNetwork.shared.createCategoryRPC(category: category_)
        }
        return nil
    }
    
    // --------------------- UPDATE CATEGORY --------------------- //
    func updateCategoryOperation(_ operation: Event) -> Signal<Bool, Error>? {
        
        if let rpcSignal = self.updateCategorySignal(operation: operation) {
            
            let databaseSignal = rpcSignal |> mapToSignal({ (boolValue: Bool) -> Signal<Bool, Error> in
                
                let signal = Signal<Bool, Error> { subscriber in
                    Database.shared.markOperationAsSynced(operationId: operation.syncOperationId)
                    subscriber.putNext(true)
                    subscriber.putCompletion()
                    return ActionDisposable {}
                }
                
                return signal
            })
            
            return databaseSignal
        }
        return nil
    }
    
    private func updateCategorySignal(operation: Event) -> Signal<Bool, Error>? {
        
        if let category_ = Database.shared.categoryBy(categoryId: operation.categoryId) {
            return RPCNetwork.shared.updateCategoryRPC(category: category_)
        }
        return nil
    }
    
    private func getAllCategories() -> Signal<Bool, Error> {
        
        let getCategoriesSignal = RPCNetwork.shared.allCategoriesRPC()
        let allCategoriesDatabaseSignal = getCategoriesSignal |> mapToSignal({ (catRes: [CategoryRequest]) -> Signal<Bool, Error> in
            
            let signal = Signal<Bool, Error> { subscriber in
                
                var categories: [Category] = []
                for categoryRequest in catRes {
                    let category = ProtoToModel.categoryFrom(categoryRequest)
                    categories.append(category)
                }
                Database.shared.createCategories(categories)
                
                subscriber.putNext(true)
                subscriber.putCompletion()
                return ActionDisposable {}
            }
            return signal
        })
        return allCategoriesDatabaseSignal
    }
    
    private func checkCategoriesForUpdate(categoryUpdatedAt: Date) -> Signal<Bool, Error> {
        
        let getCategoriesSignal = RPCNetwork.shared.checkCategoriesForUpdateRPC(categoryUpdatedAt: categoryUpdatedAt)
        let allCategoriesDatabaseSignal = getCategoriesSignal |> mapToSignal({ (catRes: [CategoryRequest]) -> Signal<Bool, Error> in
            
            let signal = Signal<Bool, Error> { subscriber in
                
                var categories: [Category] = []
                for categoryRequest in catRes {
                    let category = ProtoToModel.categoryFrom(categoryRequest)
                    categories.append(category)
                }
                
                Database.shared.createCategories(categories)
                
                subscriber.putNext(true)
                subscriber.putCompletion()
                return ActionDisposable {}
            }
            return signal
        })
        return allCategoriesDatabaseSignal
    }
    
    // MARK:ORDER
    // --------------------- CREATE ORDER --------------------- //
    func createOrderOperation(_ operation: Event) -> Signal<Bool, Error>? {
        
        if let rpcSignal = self.createOrderSignal(operation: operation) {
            
            let databaseSignal = rpcSignal |> mapToSignal({(orderId: UInt64, paymentId: UInt64, transactionId: UInt64,  oldNewValueDict: [UInt64: UInt64]) -> Signal<Bool, Error> in
                
                let signal = Signal<Bool, Error> { subscriber in

                    let oldOrder = Database.shared.orderBy(orderId: operation.orderId)!
                    
                    //payments primary key
                    Database.shared.updatePaymentsPrimaryKey(oldId: oldOrder.paymentId, newId: paymentId)
                    //orders primary key
                    Database.shared.updateOrdersPrimaryKey(oldId: operation.orderId, newId: orderId)
                    //orders payment_id
                    Database.shared.updateOrdersPaymentId(oldId: oldOrder.paymentId, newId: paymentId)
                    
                    //transactions primary key
                    Database.shared.updateTransactionsPrimaryKey(oldId: operation.transactionId, newId: transactionId)
                    //transactions order_id
                    Database.shared.updateTransactionsOrderId(oldOrderId: operation.orderId, newOrderId: orderId)
                    
                    //orderDetails order_id
                    Database.shared.updateOrderDetailsOrderId(oldOrderId: operation.orderId, newOrderId: orderId)
                    
                    //orderDetails primary key
                    for (key, value) in oldNewValueDict {
                        Database.shared.updateOrderDetailsPrimaryKey(oldId: key, newId: value)
                    }
                    
                    Database.shared.markOperationAsSynced(operationId: operation.syncOperationId)
                
                    subscriber.putNext(true)
                    subscriber.putCompletion()
                    return ActionDisposable {}
                }
                
                return signal
            })
            
            return databaseSignal
        }
        return nil
    }
    
    //1 - orderId
    //2 - paymentId
    //3 - transactionId
    //4 - orderDetails
    //(UInt64, UInt64, UInt64, UInt64, [UInt64])
    
    private func createOrderSignal(operation: Event) -> Signal<(UInt64, UInt64, UInt64, [UInt64: UInt64]), Error>? {
        
        if let order_ = Database.shared.orderBy(orderId: operation.orderId) {
            if let payment_ = Database.shared.paymentBy(paymentId: order_.paymentId)
            {
                let orderDetails = Database.shared.orderDetailsBy(orderId: operation.orderId)
                let transaction_ = Database.shared.transactionBy(transactionId: operation.transactionId)
                return RPCNetwork.shared.createOrderRPC(operation_: operation,
                                                        order_: order_,
                                                        payment_: payment_,
                                                        transaction_: transaction_,
                                                        orderDetails_: orderDetails)
            }
        }
        return nil
    }
    
    // --------------------- UPDATE ORDER --------------------- //
    func updateOrderOperation(_ operation: Event) -> Signal<Bool, Error>? {
        
        if let rpcSignal = self.updateOrderSignal(operation: operation) {
            
            let databaseSignal = rpcSignal |> mapToSignal({(order: Order) -> Signal<Bool, Error> in
                
                let signal = Signal<Bool, Error> { subscriber in
                    
                    Database.shared.markOperationAsSynced(operationId: operation.syncOperationId)
                    
                    subscriber.putNext(true)
                    subscriber.putCompletion()
                    return ActionDisposable {}
                }
                
                return signal
            })
            
            return databaseSignal
        }
        return nil
    }
    
    private func updateOrderSignal(operation: Event) -> Signal<Order, Error>? {
        if let order_ = Database.shared.orderBy(orderId: operation.orderId) {
            return RPCNetwork.shared.updateOrderRPC(order_: order_)
        }
        return nil
    }
    
    // MARK:Transaction
    // --------------------- UPDATE TRANSACTION --------------------- //
    func updateTransactionOperation(_ operation: Event) -> Signal<Bool, Error>? {
        
        if let rpcSignal = self.updateTransactionSignal(operation: operation) {
            
            let databaseSignal = rpcSignal |> mapToSignal({(transaction: Transaction) -> Signal<Bool, Error> in
                
                let signal = Signal<Bool, Error> { subscriber in
                    
                    Database.shared.markOperationAsSynced(operationId: operation.syncOperationId)
                    
                    subscriber.putNext(true)
                    subscriber.putCompletion()
                    return ActionDisposable {}
                }
                return signal
            })
            
            return databaseSignal
        }
        return nil
    }
    
    private func updateTransactionSignal(operation: Event) -> Signal<Transaction, Error>? {
        if let transaction_ = Database.shared.transactionBy(transactionId: operation.transactionId) {
            return RPCNetwork.shared.updateTransactionRPC(transaction_: transaction_)
        }
        return nil
    }
    
    
    // MARK:ADDITTIONAL GETTER HELLPERS
    func getOrderDetailsForFilter(productId: UInt64, date: Date, limit: UInt16) -> Signal<Bool, Error> {
        
        let getOrderDetails = RPCNetwork.shared.getOrderDetailsForFilterRPC(productId: productId,
                                                                            date: date,
                                                                            limit: limit)
        
        let getOrderDetailsDatabaseSignal = getOrderDetails |> mapToSignal({ (oDetReqs: [OrderDetailRequest]) -> Signal<Bool, Error> in
            
            let signal = Signal<Bool, Error> { subscriber in
                
                print("------------------------")
                print("Receive all orderDetailFilters")
                
                var orderDetails: [OrderDetail] = []
                for orderDetailRequest in oDetReqs {
                    let orderDetail = ProtoToModel.orderDetailFrom(orderDetailRequest)
                    orderDetails.append(orderDetail)
                }
                Database.shared.createorderDetails(orderDetails)
                
                subscriber.putNext(true)
                subscriber.putCompletion()
                return ActionDisposable {}
            }
            return signal
        })
        return getOrderDetailsDatabaseSignal
    }
    
    func getTransactionsForFilter(customerId: UInt64, supplierId: UInt64, date: Date, limit: UInt16) -> Signal<Bool, Error> {
        
        let transactionsForFilter = RPCNetwork.shared.allTransactionsForFilterRPC(customerId: customerId,
                                                                                  supplierId: supplierId,
                                                                                  date: date,
                                                                                  limit: limit)
        
        let transactionsForFilterSignal = transactionsForFilter |> mapToSignal({[weak self] (transReqs: [TransactionRequest]) -> Signal<Bool, Error> in
            
            let signal = Signal<Bool, Error> {[weak self] subscriber in
                
                print("------------------------")
                print("Receive all transactions for filter")
                
                var transactions: [Transaction] = []
                for transactionReq in transReqs {
                    let transaction = ProtoToModel.transactionFrom(transactionReq)
                    //self?.checkTransactionsForBalanceUpdate(transaction)
                    transactions.append(transaction)
                }
                
                Database.shared.createTransactions(transactions)
                
                subscriber.putNext(true)
                subscriber.putCompletion()
                return ActionDisposable {}
            }
            return signal
        })
        return transactionsForFilterSignal
    }
    
    private func checkTransactionsForBalanceUpdate(_ transaction: Transaction) {
        //VERY IMPORTANT, NEW GETTED TRANSACTIONS SHOULD NOT BE IN DATABASE
        if let _ = Database.shared.transactionBy(transactionId: transaction.transactionId) {
        } else {
            switch transaction.transactionType {
            case .none:
                print("transaction.transactionType none")
            case .customerBalanceModified:
                if let customer_ = Database.shared.customerBy(customerId: transaction.customerId) {
                    Database.shared.updateCustomerBalanceFor(customerId: customer_.customerId,
                                                             balance: transaction.moneyAmount)
                }
            case .supplierBalanceModified:
                if let supplier_ = Database.shared.supplierBy(supplierId: transaction.supplierId) {
                    Database.shared.updateSupplierBalanceFor(supplierId: supplier_.supplierId,
                                                             balance: transaction.moneyAmount)
                }
            default:
                print("transaction.transactionType default")
            }
        }
    }
    
    private func parseAndSaveOrdersWith(createOrdReqs: [CreateOrderRequest], isForInitialState: Bool) -> Signal<Bool, Error> {
        let signal = Signal<Bool, Error> { subscriber in
            
            print("------------------------")
            print("Receive all orders for filter")
            
            var orders: [Order] = []
            var payments: [Payment] = []
            var transactions: [Transaction] = []
            var orderDetails: [OrderDetail] = []
            
            for createOrderRequest in createOrdReqs {
                
                if let orderRequest = createOrderRequest.order {
                    
                    let order = ProtoToModel.orderFrom(orderRequest)
                    orders.append(order)
                    
                    var localOrderDetails: [OrderDetail] = []
                    
                    for someObj in createOrderRequest.orderDetailsArray {
                        if let orderDetailRequest = someObj as? OrderDetailRequest {
                            let orderDetail = ProtoToModel.orderDetailFrom(orderDetailRequest)
                            localOrderDetails.append(orderDetail)
                            orderDetails.append(orderDetail)
                        }
                    }
                    
                    if let paymentRequest = createOrderRequest.payment {
                        
                        let payment = ProtoToModel.paymentFrom(paymentRequest)
                        payments.append(payment)
                        
                        if isForInitialState == false {
                            
                            //VERY IMPORTANT, NEW GETTED ORDER SHOULD NOT BE IN DATABASE
                            if let _ = Database.shared.orderBy(orderId: order.orderId) {
                                print("ORDER \(order.orderId) ALLREADY EXSISTS!")
                            } else {
                                
                                switch order.orderDocument {
                                    
                                case .none:
                                    print("none")
                                    
                                case .saleToCustomer:
                                    
                                    OrderManager.shared.decreaseProductsInStock(orderDetails: localOrderDetails)
                                    
                                    if let account_ = Database.shared.accountBy(customerId: order.customerId) {
                                        account_.balance = account_.balance + payment.totalPriceWithDiscount
                                        Database.shared.updateCustomerBalanceFor(customerId: order.customerId,
                                                                                 balance: account_.balance)
                                    }
                                    
                                case .saleToCustomerEdited:
                                    
                                    OrderManager.shared.increaseProductsInStock(orderDetails: localOrderDetails)
                                    
                                    if let account_ = Database.shared.accountBy(customerId: order.customerId) {
                                        account_.balance = account_.balance - payment.totalPriceWithDiscount
                                        Database.shared.updateCustomerBalanceFor(customerId: order.customerId,
                                                                                 balance: account_.balance)
                                    }
                                    
                                case .receiveFromSupplier:
                                    
                                    OrderManager.shared.increaseProductsInStock(orderDetails: localOrderDetails)
                                    
                                    if let account_ = Database.shared.accountBy(supplierId: order.supplierId) {
                                        account_.balance = account_.balance - payment.totalPriceWithDiscount
                                        Database.shared.updateSupplierBalanceFor(supplierId: order.supplierId,
                                                                                 balance: account_.balance)
                                    }
                                    
                                case .receiveFromSupplierEdited:
                                    
                                    OrderManager.shared.decreaseProductsInStock(orderDetails: localOrderDetails)
                                    
                                    if let account_ = Database.shared.accountBy(supplierId: order.supplierId) {
                                        account_.balance = account_.balance + payment.totalPriceWithDiscount
                                        Database.shared.updateSupplierBalanceFor(supplierId: order.supplierId,
                                                                                 balance: account_.balance)
                                    }
                                    
                                case .returnFromCustomer:
                                    
                                    OrderManager.shared.increaseProductsInStock(orderDetails: localOrderDetails)
                                    
                                    if let account_ = Database.shared.accountBy(customerId: order.customerId) {
                                        account_.balance = account_.balance - payment.totalPriceWithDiscount
                                        Database.shared.updateCustomerBalanceFor(customerId: order.customerId,
                                                                                 balance: account_.balance)
                                    }
                                case .returnFromCustomerEdited:
                                    print(".productReturneEditedFromCustomer")
                                    
                                case .returnToSupplier:
                                    
                                    OrderManager.shared.decreaseProductsInStock(orderDetails: localOrderDetails)
                                    
                                    if let account_ = Database.shared.accountBy(supplierId: order.supplierId) {
                                        account_.balance = account_.balance + payment.totalPriceWithDiscount
                                        Database.shared.updateSupplierBalanceFor(supplierId: order.supplierId,
                                                                                 balance: account_.balance)
                                    }
                                case .returnToSupplierEdited:
                                    print(".productReturneEditedToSupplier")
                                    
                                case .moneyReceive:
                                    
                                    if order.customerId > 0 {
                                        if let account_ = Database.shared.accountBy(customerId: order.customerId) {
                                            account_.balance = account_.balance - payment.totalPriceWithDiscount
                                            Database.shared.updateCustomerBalanceFor(customerId: order.customerId,
                                                                                     balance: account_.balance)
                                        }
                                    } else if order.supplierId > 0 {
                                        if let account_ = Database.shared.accountBy(supplierId: order.supplierId) {
                                            account_.balance = account_.balance - payment.totalPriceWithDiscount
                                            Database.shared.updateSupplierBalanceFor(supplierId: order.supplierId,
                                                                                     balance: account_.balance)
                                        }
                                    }
                                    
                                case .moneyReceiveEdited:
                                    
                                    if order.customerId > 0 {
                                        if let account_ = Database.shared.accountBy(customerId: order.customerId) {
                                            account_.balance = account_.balance + payment.totalPriceWithDiscount
                                            Database.shared.updateCustomerBalanceFor(customerId: order.customerId,
                                                                                     balance: account_.balance)
                                        }
                                    } else if order.supplierId > 0 {
                                        if let account_ = Database.shared.accountBy(supplierId: order.supplierId) {
                                            account_.balance = account_.balance + payment.totalPriceWithDiscount
                                            Database.shared.updateSupplierBalanceFor(supplierId: order.supplierId,
                                                                                     balance: account_.balance)
                                        }
                                    }
                                    
                                case .moneyGone:
                                    
                                    if order.customerId > 0 {
                                        if let account_ = Database.shared.accountBy(customerId: order.customerId) {
                                            account_.balance = account_.balance + payment.totalPriceWithDiscount
                                            Database.shared.updateCustomerBalanceFor(customerId: order.customerId,
                                                                                     balance: account_.balance)
                                        }
                                    } else if order.supplierId > 0 {
                                        if let account_ = Database.shared.accountBy(supplierId: order.supplierId) {
                                            account_.balance = account_.balance + payment.totalPriceWithDiscount
                                            Database.shared.updateSupplierBalanceFor(supplierId: order.supplierId,
                                                                                     balance: account_.balance)
                                        }
                                    }
                                    
                                case .moneyGoneEdited:
                                    
                                    if order.customerId > 0 {
                                        if let account_ = Database.shared.accountBy(customerId: order.customerId) {
                                            account_.balance = account_.balance - payment.totalPriceWithDiscount
                                            Database.shared.updateCustomerBalanceFor(customerId: order.customerId,
                                                                                     balance: account_.balance)
                                        }
                                    } else if order.supplierId > 0 {
                                        if let account_ = Database.shared.accountBy(supplierId: order.supplierId) {
                                            account_.balance = account_.balance - payment.totalPriceWithDiscount
                                            Database.shared.updateSupplierBalanceFor(supplierId: order.supplierId,
                                                                                     balance: account_.balance)
                                        }
                                    }
                                    
                                case .preOrderToCustomer:
                                    print("customerMadePreOrder")
                                    
                                case .stockTaking:
                                    print("stockTaking")
                                    
                                // ---------------------------------
                                case .rawProductGoneToProd:
                                    print("rawProductGoneToProd")
                                    OrderManager.shared.decreaseProductsInStock(orderDetails: localOrderDetails)
                                case .rawProductGoneToProdEdited:
                                    print("rawProductGoneToProdEdited")
                                    OrderManager.shared.increaseProductsInStock(orderDetails: localOrderDetails)
                                case .readyProductReceivedFromProd:
                                    print("readyProductReceivedFromProd")
                                    OrderManager.shared.increaseProductsInStock(orderDetails: localOrderDetails)
                                case .readyProductReceivedFromProdEdited:
                                    print("readyProductReceivedFromProdEdited")
                                    OrderManager.shared.decreaseProductsInStock(orderDetails: localOrderDetails)
                                    
                                default:
                                    print("default")
                                }
                            }
                        }
                    }
                    
                    if let transactionReq = createOrderRequest.transaction {
                        let transaction = ProtoToModel.transactionFrom(transactionReq)
                        transactions.append(transaction)
                    }
                }
            }
            
            Database.shared.createOrders(orders)
            Database.shared.createPayments(payments)
            Database.shared.createTransactions(transactions)
            Database.shared.createorderDetails(orderDetails)
            
            subscriber.putNext(true)
            subscriber.putCompletion()
            return ActionDisposable {}
        }
        return signal
    }
    
    func getOrdersForFilter(date: Date, limit: UInt16, isForInitialState: Bool) -> Signal<Bool, Error> {
        
        // orders for filter mean all order <= order_date, limit = 50 <> 500
        let ordersForFilter = RPCNetwork.shared.allOrdersForFilterRPC(date: date,
                                                                      limit: limit)
        
        let ordersForFilterSignal = ordersForFilter |> mapToSignal({ (createOrdReqs_: [CreateOrderRequest]) -> Signal<Bool, Error> in
            return self.parseAndSaveOrdersWith(createOrdReqs: createOrdReqs_, isForInitialState: isForInitialState)
        })
        return ordersForFilterSignal
    }
    
    private func checkForTransactionsUpdatedAt(transactionUpdatedAt: Date) -> Signal<Bool, Error> {
        
        let recentTransactionsForFilter = RPCNetwork.shared.checkTransactionsForUpdateRPC(date: transactionUpdatedAt,
                                                                                          limit: 5000)
            
        let recentTransactionsSignal = recentTransactionsForFilter |> mapToSignal({[weak self] (transactReqs_: [TransactionRequest]) -> Signal<Bool, Error> in
            let signal = Signal<Bool, Error> {[weak self] subscriber in
                
                var transactions: [Transaction] = []
                for transactionReq in transactReqs_ {
                    let transaction = ProtoToModel.transactionFrom(transactionReq)
                    self?.checkTransactionsForBalanceUpdate(transaction)
                    transactions.append(transaction)
                }
                
                Database.shared.createTransactions(transactions)
                subscriber.putNext(true)
                subscriber.putCompletion()
                return ActionDisposable {}
            }
            return signal
        })
        return recentTransactionsSignal
    }
    
    private func checkForOrdersUpdatedAt(orderUpdatedAt: Date) -> Signal<Bool, Error> {
        
        // recent orders mean all order > order_date, limit <> 5000
        let recentOrdersForFilter = RPCNetwork.shared.checkOrdersForUpdateRPC(date: orderUpdatedAt,
                                                                              limit: 5000)
        
        let ordersForFilterSignal = recentOrdersForFilter |> mapToSignal({ (createOrdReqs_: [CreateOrderRequest]) -> Signal<Bool, Error> in
            return self.parseAndSaveOrdersWith(createOrdReqs: createOrdReqs_, isForInitialState: false)
        })
        return ordersForFilterSignal
    }
}
