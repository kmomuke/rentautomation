//
//  StatGenerator.swift
//  Rentautomation
//
//  Created by kanybek on 4/12/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit


struct CustomerOrder {
    
    var customerId: UInt64
    var orders: [Order]

    var productsSaledMoney: Double
    var totalIncomeMoney: Double
    var debtIncomeMoney: Double
    var saleIncomeMoney: Double
    var moneyProffit: Double
    var salesCount: Int32
    var sumOfMoneyOutgone: Double
    
    init(customerId: UInt64,
         orders: [Order],
         productsSaledMoney: Double,
         totalIncomeMoney: Double,
         debtIncomeMoney: Double,
         saleIncomeMoney: Double,
         moneyProffit: Double,
         salesCount: Int32,
         sumOfMoneyOutgone: Double)
    {
        self.customerId = customerId
        self.orders = orders
        self.productsSaledMoney = productsSaledMoney
        self.totalIncomeMoney = totalIncomeMoney
        self.debtIncomeMoney = debtIncomeMoney
        self.saleIncomeMoney = saleIncomeMoney
        self.moneyProffit = moneyProffit
        self.salesCount = salesCount
        self.sumOfMoneyOutgone = sumOfMoneyOutgone
    }
}


struct MostProduct {
    
    var productId: UInt64
    var productAmount: Double
    var totalSaledAmount: Double
    var orderCount: Int
    
    init(productId: UInt64,
         productAmount: Double,
         totalSaledAmount: Double,
         orderCount: Int)
    {
        self.productId = productId
        self.productAmount = productAmount
        self.totalSaledAmount = totalSaledAmount
        self.orderCount = orderCount
    }
}

struct CategoryStat {
    
    var categoryId: UInt64
    var categoryName: String
    var amountInStock: Double
    var categorySaledPrice: Double
    var categoryIncomePrice: Double
    
    init(categoryId: UInt64,
         categoryName: String,
         amountInStock: Double,
         categorySaledPrice: Double,
         categoryIncomePrice: Double)
    {
        self.categoryId = categoryId
        self.categoryName = categoryName
        self.amountInStock = amountInStock
        self.categorySaledPrice = categorySaledPrice
        self.categoryIncomePrice = categoryIncomePrice
    }
}

final class StatGenerator {
    
    static let shared: StatGenerator = StatGenerator()
    
    var isOrderCalculating: Bool
    var isStatiscticsCalculating: Bool
    
    var productAmount: [UInt64: Double] = [:]
    var saledProducts: [MostProduct] = []
    var categoryStats: [CategoryStat] = []
    
    var customerOrders: [UInt64: [Order]] = [:]
    
    init () {
        self.isOrderCalculating = false
        self.isStatiscticsCalculating = false
    }
    
    func calculateOrders(orders: [Order], completion:@escaping ((Double, UInt32, Double, Double)) -> ()) -> Void {
        
        if self.isOrderCalculating {
            return
        }
        
        if !self.isOrderCalculating {
            self.isOrderCalculating = true
            
            var productSaleToCustomer: Double = 0.0
            var totalSaleOrders: UInt32 = 0
            var moneyProffit: Double = 0.0
            var moneyReceived: Double = 0.0
            
            generateQue.async {
                
                for order in orders {
                    
                    if order.isEdited {
                        continue
                    }
                    
                    switch order.orderDocument {
                        
                    case .none:
                        print(".none")
                        
                    case .moneyReceive:
                        
                        switch order.moneyMovementType {
                        case .moneyFromCustomer, .moneyReceive, .otherMoneyReceive:
                            if let payment = Database.shared.paymentBy(paymentId: order.paymentId) {
                                moneyReceived = moneyReceived + payment.totalPriceWithDiscount
                            }
                        default:
                            print("default:")
                        }
                        
                    case .saleToCustomer:
                        totalSaleOrders = totalSaleOrders + 1
                        
                        if let payment = Database.shared.paymentBy(paymentId: order.paymentId) {
                            productSaleToCustomer = productSaleToCustomer + payment.totalPriceWithDiscount
                        }
                        
                        let orderDetails = Database.shared.orderDetailsBy(orderId: order.orderId)
                        
                        if orderDetails.count > 0 {
                            
                            for orderDetail in orderDetails {
                                if let product_ = Database.shared.productFor(productId: orderDetail.productId) {
                                    moneyProffit = moneyProffit + (orderDetail.price * (1) - product_.incomeUnitPrice) * orderDetail.orderQuantity
                                }
                            }
                        }
                    default:
                        print("default")
                    }
                }
                
                DispatchQueue.main.async {
                    self.isOrderCalculating = false
                    completion((productSaleToCustomer, totalSaleOrders, moneyProffit, moneyReceived))
                }
            }
        }
    }
    
    private func saveMostSaled(productId: UInt64, amount: Double) {
        if let inMemoryAmount_ = self.productAmount[productId] {
            self.productAmount[productId] = inMemoryAmount_ + amount
        } else {
            self.productAmount[productId] = amount
        }
    }
    
    private func addOrderToCustomer(customerId: UInt64, order: Order) {
        if var orders = self.customerOrders[customerId] {
            orders.append(order)
            self.customerOrders[customerId] = orders
        } else {
            self.customerOrders[customerId] = [order]
        }
    }
    
    func calculateMostSaledProducts(startDate: Date,
                                    endDate: Date,
                                    orderDocType: OrderDocumentType,
                                    userId: UInt64,
                                    completion:@escaping ([MostProduct]) -> ())
    {
        if self.isStatiscticsCalculating {
            return
        }
        
        if !self.isStatiscticsCalculating {
            self.isStatiscticsCalculating = true
            
            Database.shared.loadOrdersForFilters(documentType: orderDocType,
                                                 startDate: startDate,
                                                 endDate: endDate,
                                                 customerId: 0,
                                                 supplierId: 0,
                                                 userId: userId,
                                                 limit: 10000,
                                                 filterOptions: [],
                                                 completion: { (orders: [Order]) in
                                                    
                generateQue.async {
                    
                    self.productAmount.removeAll()
                    self.saledProducts.removeAll()
                    
                    var totalSaledAmount: Double = 0.0
                    var orderCount: Int = 0
                    
                    for order in orders {
                        
                        if order.isEdited {
                            continue
                        }
                        
                        switch orderDocType {
                        case .saleToCustomer:
                            if order.orderDocument == .saleToCustomer {
                                orderCount = orderCount + 1
                                let orderDetails = Database.shared.orderDetailsBy(orderId: order.orderId)
                                if orderDetails.count > 0 {
                                    for orderDetail in orderDetails {
                                        totalSaledAmount = totalSaledAmount + orderDetail.orderQuantity
                                        self.saveMostSaled(productId: orderDetail.productId, amount: orderDetail.orderQuantity)
                                    }
                                }
                            }
                        case .receiveFromSupplier:
                            if order.orderDocument == .receiveFromSupplier {
                                orderCount = orderCount + 1
                                let orderDetails = Database.shared.orderDetailsBy(orderId: order.orderId)
                                if orderDetails.count > 0 {
                                    for orderDetail in orderDetails {
                                        totalSaledAmount = totalSaledAmount + orderDetail.orderQuantity
                                        self.saveMostSaled(productId: orderDetail.productId, amount: orderDetail.orderQuantity)
                                    }
                                }
                            }
                            
                        case .preOrderToCustomer:
                            if order.orderDocument == .preOrderToCustomer {
                                orderCount = orderCount + 1
                                let orderDetails = Database.shared.orderDetailsBy(orderId: order.orderId)
                                if orderDetails.count > 0 {
                                    for orderDetail in orderDetails {
                                        totalSaledAmount = totalSaledAmount + orderDetail.orderQuantity
                                        self.saveMostSaled(productId: orderDetail.productId, amount: orderDetail.orderQuantity)
                                    }
                                }
                            }
                            
                        case .rawProductGoneToProd:
                            if order.orderDocument == .rawProductGoneToProd {
                                orderCount = orderCount + 1
                                let orderDetails = Database.shared.orderDetailsBy(orderId: order.orderId)
                                if orderDetails.count > 0 {
                                    for orderDetail in orderDetails {
                                        totalSaledAmount = totalSaledAmount + orderDetail.orderQuantity
                                        self.saveMostSaled(productId: orderDetail.productId, amount: orderDetail.orderQuantity)
                                    }
                                }
                            }
                            
                        case .readyProductReceivedFromProd:
                            if order.orderDocument == .readyProductReceivedFromProd {
                                orderCount = orderCount + 1
                                let orderDetails = Database.shared.orderDetailsBy(orderId: order.orderId)
                                if orderDetails.count > 0 {
                                    for orderDetail in orderDetails {
                                        totalSaledAmount = totalSaledAmount + orderDetail.orderQuantity
                                        self.saveMostSaled(productId: orderDetail.productId, amount: orderDetail.orderQuantity)
                                    }
                                }
                            }
                            
                        default:
                            print("default")
                        }
                    }
                    
                    for (key, value) in self.productAmount {
                        let mostProduct = MostProduct(productId: key,
                                                      productAmount: value,
                                                      totalSaledAmount: totalSaledAmount,
                                                      orderCount: orderCount)
                        self.saledProducts.append(mostProduct)
                    }
                    
                    self.saledProducts.sort(by: { (first: MostProduct, second: MostProduct) -> Bool in
                        first.productAmount > second.productAmount
                    })
                    
                    //print("ARRAY IS: \(self.saledProducts)")
                    
                    DispatchQueue.main.async {
                        self.isStatiscticsCalculating = false
                        completion(self.saledProducts)
                    }
                }
                                                    
                                                    
            })
        }
    }
    
    func calculateCustomerOrders(orders: [Order], completion:@escaping ([CustomerOrder]) -> ()) -> Void {
        
        if self.isStatiscticsCalculating {
            return
        }
        
        if !self.isStatiscticsCalculating {
            self.isStatiscticsCalculating = true
            
            print("calculateCustomerOrders")
            
            var customerOrders: [CustomerOrder] = []
            
            generateQue.async {
                
                self.customerOrders.removeAll()
                var totalSumOfMoneyOutgone: Double = 0.0
                
                for order in orders {
                    
                    if order.isEdited {
                        continue
                    }
                    
                    switch order.orderDocument {
                    
                    case .moneyGone:
                        if let payment = Database.shared.paymentBy(paymentId: order.paymentId) {
                            totalSumOfMoneyOutgone = totalSumOfMoneyOutgone + payment.totalPriceWithDiscount
                        }
                    
                    case .moneyReceive:
                        let customerId_ = order.customerId
                        self.addOrderToCustomer(customerId: customerId_, order: order)
                    
                    case .saleToCustomer:
                        let customerId_ = order.customerId
                        self.addOrderToCustomer(customerId: customerId_, order: order)
                    
//                    case .returnFromCustomer:
//                        let customerId_ = order.customerId
//                        self.addOrderToCustomer(customerId: customerId_, order: order)
                        
                    default:
                        print("order.orderDocument default")
                    }
                }
                
                for (key_, orders_) in self.customerOrders {
                    
                    var productsSaledMoney: Double = 0.0
                    var totalIncomeMoney: Double = 0.0
                    var debtIncomeMoney: Double = 0.0
                    var saleIncomeMoney: Double = 0.0
                    
                    var moneyProffit: Double = 0.0
                    var salesCount: Int32 = 0
                    
                    for order in orders_ {
                        
                        switch order.orderDocument {
                        case .moneyReceive:
                            if let payment = Database.shared.paymentBy(paymentId: order.paymentId) {
                                
                                totalIncomeMoney = totalIncomeMoney + payment.totalPriceWithDiscount
                                
                                saleIncomeMoney = saleIncomeMoney + payment.totalPriceWithDiscount
                                
                                //if order.isMoneyForDebt {
                                //    debtIncomeMoney = debtIncomeMoney + payment.totalPriceWithDiscount
                                //} else {
                                //    saleIncomeMoney = saleIncomeMoney + payment.totalPriceWithDiscount
                                //}
                            }
                            
                        case .saleToCustomer:
                            
                            salesCount = salesCount + 1
                            
                            if let payment = Database.shared.paymentBy(paymentId: order.paymentId) {
                                productsSaledMoney = productsSaledMoney + payment.totalPriceWithDiscount
                            }
                            
                            let orderDetails = Database.shared.orderDetailsBy(orderId: order.orderId)
                            
                            if orderDetails.count > 0 {
                                
                                for orderDetail in orderDetails {
                                    if let product_ = Database.shared.productFor(productId: orderDetail.productId) {
                                        moneyProffit = moneyProffit + (orderDetail.price - product_.incomeUnitPrice) * orderDetail.orderQuantity
                                    }
                                }
                            }
                            
                            
//                        case .returnFromCustomer:
//
//                            //salesCount = salesCount + 1
//
//                            if let payment = Database.shared.paymentBy(paymentId: order.paymentId) {
//                                productsSaledMoney = productsSaledMoney + payment.totalPriceWithDiscount,
//                            }
//
//                            let orderDetails = Database.shared.orderDetailsBy(orderId: order.orderId)
//
//                            if orderDetails.count > 0 {
//
//                                for orderDetail in orderDetails {
//                                    if let product_ = Database.shared.productFor(productId: orderDetail.productId) {
//                                        moneyProffit = moneyProffit + (orderDetail.price - product_.incomeUnitPrice) * orderDetail.orderQuantity
//                                    }
//                                }
//                            }
                            
                        default:
                            print("default")
                        }
                        
                    }
                    
                   let customerOrder = CustomerOrder(customerId: key_,
                                                     orders: orders_,
                                                     productsSaledMoney: productsSaledMoney,
                                                     totalIncomeMoney: totalIncomeMoney,
                                                     debtIncomeMoney: debtIncomeMoney,
                                                     saleIncomeMoney: saleIncomeMoney,
                                                     moneyProffit: moneyProffit,
                                                     salesCount: salesCount,
                                                     sumOfMoneyOutgone: totalSumOfMoneyOutgone)
                    customerOrders.append(customerOrder)
                }
                
                DispatchQueue.main.async {
                    self.isStatiscticsCalculating = false
                    completion(customerOrders)
                }
            }
        }
    }
    
    func calculateOrdersForCustomDates(completion:@escaping ([(Double, UInt32, Double, Double)]) -> ()) -> Void {
        
        if self.isStatiscticsCalculating {
            return
        }
        
        if !self.isStatiscticsCalculating {
            self.isStatiscticsCalculating = true
            
            print("calculateOrdersForCustomDates")
            
            var dates: [(Date, Date)] = []
            var calculatedNumbers: [(Double, UInt32, Double, Double)] = []
            
            generateQue.async {
            
                let startDate = Date()
                let startDateBeginningDate = startDate.dateAtStartOfDay()
                
                dates.append((startDateBeginningDate, startDate))
                
                for index: Int in 1...Global.shared.statisticShowDay {
                    
                    let dayBefore = startDate.dateBySubtractingDays(index)
                    
                    let dayBeforeEndOfDay = dayBefore.dateAtEndOfDay()
                    let dayBeforeStartOfDay = dayBefore.dateAtStartOfDay()
                    
                    dates.append((dayBeforeStartOfDay, dayBeforeEndOfDay))
                }
                
                for date in dates {
                    print(" ----------- ")
                    print("start: \(date.0.toString(.custom("EEE, dd MMM HH:mm:ss"))) end: \(date.1.toString(.custom("EEE, dd MMM HH:mm:ss")))")
                    print(" =========== ")
                    
                    let orders = Database.shared.loadStatisticOrdersForDates(startDate: date.0, endDate: date.1)
                    
                    var productSaleToCustomer: Double = 0.0
                    var totalSaleOrders: UInt32 = 0
                    var moneyProffit: Double = 0.0
                    var moneyReceived: Double = 0.0
                    
                    for order in orders {
                        
                        if order.isEdited {
                            continue
                        }
                        
                        switch order.orderDocument {
                            
                        case .none:
                            print(".none")
                        case .moneyReceive:
                            switch order.moneyMovementType {
                            case .moneyFromCustomer, .moneyReceive, .otherMoneyReceive:
                                if let payment = Database.shared.paymentBy(paymentId: order.paymentId) {
                                    moneyReceived = moneyReceived + payment.totalPriceWithDiscount
                                }
                            default:
                                print("default:")
                            }
                        case .saleToCustomer:
                            print(".productOrderSaledToCustomer")
                            totalSaleOrders = totalSaleOrders + 1
                            
                            if let payment = Database.shared.paymentBy(paymentId: order.paymentId) {
                                productSaleToCustomer = productSaleToCustomer + payment.totalPriceWithDiscount
                            }
                            
                            let orderDetails = Database.shared.orderDetailsBy(orderId: order.orderId)
                            
                            if orderDetails.count > 0 {
                                for orderDetail in orderDetails {
                                    if let product_ = Database.shared.productFor(productId: orderDetail.productId) {
                                        moneyProffit = moneyProffit + (orderDetail.price - product_.incomeUnitPrice) * orderDetail.orderQuantity
                                    }
                                }
                            }
                        default:
                            print("default")
                        }
                    }
                    calculatedNumbers.append((productSaleToCustomer, totalSaleOrders, moneyProffit, moneyReceived))
                }
                
                DispatchQueue.main.async {
                    let reversedArray = Array(calculatedNumbers.reversed())
                    self.isStatiscticsCalculating = false
                    completion(reversedArray)
                }
            }
        }
    }
    
    
    func calculateCategoryStats(completion:@escaping ([CategoryStat]) -> ()) -> Void {
        
        if self.isStatiscticsCalculating {
            return
        }
        
        if !self.isStatiscticsCalculating {
            self.isStatiscticsCalculating = true
            
            generateQue.async {
                
                self.categoryStats.removeAll()
                
                let categories = Database.shared.loadAllCategories()
                
                for category in categories {
                    let products = Database.shared.productsFor(categoryId: category.categoryId)
                    
                    var totalPriceInStock: Double = 0.0
                    var totalIncomePriceInStock: Double = 0.0
                    var totalAmountInStock: Double = 0.0
                    
                    for product in products {
                        
                        var unitsInStock_: Double = 0
                        if product.unitsInStock > 0 {
                            unitsInStock_ = product.unitsInStock
                        }
                        
                        totalPriceInStock = totalPriceInStock + product.saleUnitPrice * unitsInStock_
                        totalIncomePriceInStock = totalIncomePriceInStock + product.incomeUnitPrice * unitsInStock_
                        totalAmountInStock = totalAmountInStock + unitsInStock_
                    }
                    
                    let categoryStat = CategoryStat(categoryId: category.categoryId,
                                                    categoryName: category.categoryName,
                                                    amountInStock: totalAmountInStock,
                                                    categorySaledPrice: totalPriceInStock,
                                                    categoryIncomePrice: totalIncomePriceInStock)
                    
                    self.categoryStats.append(categoryStat)
                }
                
                let products = Database.shared.productsFor(categoryId: 0)
                
                var totalPriceInStock: Double = 0.0
                var totalIncomePriceInStock: Double = 0.0
                var totalAmountInStock: Double = 0.0
                
                for product in products {
                    
                    var unitsInStock_: Double = 0
                    if product.unitsInStock > 0 {
                        unitsInStock_ = product.unitsInStock
                    }
                    
                    totalPriceInStock = totalPriceInStock + product.saleUnitPrice * unitsInStock_
                    totalIncomePriceInStock = totalIncomePriceInStock + product.incomeUnitPrice * unitsInStock_
                    totalAmountInStock = totalAmountInStock + unitsInStock_
                }
                
                let categoryStat = CategoryStat(categoryId: 0,
                                                categoryName: "Без категории",
                                                amountInStock: totalAmountInStock,
                                                categorySaledPrice: totalPriceInStock,
                                                categoryIncomePrice: totalIncomePriceInStock)
                
                self.categoryStats.append(categoryStat)
                
                
                DispatchQueue.main.async {
                    self.isStatiscticsCalculating = false
                    completion(self.categoryStats)
                }
            }
        }
    }
    
}
