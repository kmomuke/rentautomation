//
//  MainStatisticsVC.swift
//  Rentautomation
//
//  Created by kanybek on 4/6/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class MainStatisticsVC: BaseListVC, ErrorPopoverRenderer {
    
    var isFirstTime: Bool = true
    var statisticsVC: StatisticsVC?
    var selectedSegmentIndex: Int = 0

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var clientButton: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    
    var initalString: String?
    
    convenience init() {
        self.init(initalString: "kokekoke")
    }
    
    init(initalString: String) {
        self.initalString = initalString
        OrderFilterMemory.shared.clearAllVariables()
        super.init(nibName: "MainStatisticsVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        OrderFilterMemory.shared.clearAllVariables()
        let font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightMedium)
        self.segmentedControl.setTitleTextAttributes([NSFontAttributeName: font],
                                                     for: .normal)
        self.title = "Отчеты о продаже и прибыли"
        self.statisticsVC = StatisticsVC()
        self.addCanceButton()
        self.dateLabel.textColor = UIColor.midnightBlueColor()
        self.clientButton.addTarget(self, action: #selector(customerDidPressed(_:)), for: .touchUpInside)
    }
    
    override func addCanceButton() -> Void {
        let cancelButton = UIBarButtonItem(
            title: "Закрыть",
            style: .plain,
            target: self,
            action: #selector(cancelVC(_:))
        )
        self.navigationItem.leftBarButtonItem = cancelButton
    }
    
    override func cancelVC(_ sender: AnyObject) -> Void {
        OrderFilterMemory.shared.clearAllVariables()
        let empty = EmptyVC()
        let detailNavigationController = RNNavigationController(rootViewController:empty)
        self.splitViewController?.showDetailViewController(detailNavigationController, sender: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (isFirstTime) {
            self.add(asChildViewController: self.statisticsVC!, frame_: self.containerView.frame)
            isFirstTime = false
        }
        
        if OrderFilterMemory.shared.customerId > 0 {
            let customerId = OrderFilterMemory.shared.customerId
            if let customer__ = Database.shared.customerBy(customerId:customerId) {
                self.clientButton.setTitle("\(customer__.firstName) \(customer__.secondName)", for: .normal)
            }
        }
        
        self.reloadAllData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func indexChanged(_ sender: UISegmentedControl) {
        self.selectedSegmentIndex = sender.selectedSegmentIndex
        self.reloadAllData()
    }
    
    @IBAction func customerDidPressed(_ sender: Any) {
        let customerListVC = CustomersListVC(initialString: nil, isComingFromOrder: true)
        self.navigationController?.pushViewController(customerListVC, animated: true)
    }
    
    func filterPressed(_ sender: UIBarButtonItem) {
        print("filterPressed")
    }
    
    private func updateIpadLabel(_ startDate: Date, endDate: Date) -> Void {
        OrderFilterMemory.shared.startDate = startDate
        OrderFilterMemory.shared.endDate = endDate
        
        self.dateLabel.text = "\(startDate.toString(.custom("EEE, dd MMMM HH:mm"))) - \(endDate.toString(.custom("EEE, dd MMMM HH:mm")))"
    }
    
    func reloadAllData() -> Void {
        switch self.selectedSegmentIndex
        {
        case 0:
            print("First Segment Selected")
            let startDate = Date().dateAtStartOfDay()
            let endDate = Date()
            self.updateIpadLabel(startDate, endDate: endDate)
            
            let customerId = OrderFilterMemory.shared.customerId
            self.statisticsVC!.reloadRows(startDate: startDate, endDate: endDate, customerId: customerId)
            
        case 1:
            print("Second Segment Selected")
            let startDate = Date().dateAtStartOfWeek()
            let endDate = Date()
            self.updateIpadLabel(startDate, endDate: endDate)
            
            let customerId = OrderFilterMemory.shared.customerId
            self.statisticsVC!.reloadRows(startDate: startDate, endDate: endDate, customerId: customerId)
            
        case 2:
            print("Third Segment Selected")
            let startDate = Date().dateAtTheStartOfMonth().dateAtStartOfDay()
            let endDate = Date()
            self.updateIpadLabel(startDate, endDate: endDate)
            
            let customerId = OrderFilterMemory.shared.customerId
            self.statisticsVC!.reloadRows(startDate: startDate, endDate: endDate, customerId: customerId)
        case 3:
            self.customDateDidPressed()
        default:
            print("default")
            break
        }
    }
    
    func customDateDidPressed() {
        
        let statVC = StatisticFilterVC(initialItem: "")
        statVC.applyFiltersBlock = {[weak self] (startDate_: Date, endDate_: Date) -> Void in
            if let strongSelf = self {
                
                let startDate = startDate_
                let endDate = endDate_
                strongSelf.updateIpadLabel(startDate, endDate: endDate)
                
                let customerId = OrderFilterMemory.shared.customerId
                
                strongSelf.statisticsVC!.reloadRows(startDate: startDate, endDate: endDate, customerId: customerId)
            }
        }
        statVC.cancelBlock = {[weak self] (finished: Bool) -> Void in
            self?.segmentedControl.selectedSegmentIndex = 0
            self?.selectedSegmentIndex = 0
            self?.reloadAllData()
        }
        self.presentSheet(viewController: statVC)
    }
}
