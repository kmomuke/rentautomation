//
//  DebtStatisticsVC.swift
//  Rentautomation
//
//  Created by kanybek on 5/14/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import CSNotificationView

class DebtStatisticsVC: BaseListVC, ErrorPopoverRenderer, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var containerView: UITableView!
    @IBOutlet weak var clientButton: UIButton!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var totalDebtLabel: UILabel!
    @IBOutlet weak var totalSaleLabel: UILabel!
    @IBOutlet weak var profitLabel: UILabel!
    @IBOutlet weak var moneyIncomeLabel: UILabel!
    @IBOutlet weak var totalSalesCountLabel: UILabel!
    @IBOutlet weak var totalOutgoneMoneyLabel: UILabel!
    @IBOutlet weak var cashLabel: UILabel!
    @IBOutlet weak var moneyIncomeDivisionLabel: UILabel!
    
    var selectedSegmentIndex: Int = 0
    var customerOrders: [CustomerOrder] = []
    var initalString: String?
    
    var notificationView: CSNotificationView?
    
    convenience init() {
        self.init(initalString: "")
    }
    
    init(initalString: String) {
        self.initalString = initalString
        OrderFilterMemory.shared.clearAllVariables()
        super.init(nibName: "DebtStatisticsVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("DebtStatisticsVC deallocated")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addCanceButton()
        
        if Global.shared.targetName != "TazaPos-lite" {
            let generateButton = UIBarButtonItem(
                title: "Сотрудник",
                style: .plain,
                target: self,
                action: #selector(staffPressed(_:))
            )
            self.navigationItem.rightBarButtonItem = generateButton
        }
        
        self.title = "Отчеты о продаже, прибыли и по долгам клиентов"
        let font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightMedium)
        self.segmentedControl.setTitleTextAttributes([NSFontAttributeName: font],
                                                     for: .normal)
        
        let nib = UINib(nibName: "CustomerOrderTVCell", bundle: nil)
        self.containerView.register(nib, forCellReuseIdentifier: "CustomerOrderTVCell")
        
        self.containerView.delegate = self
        self.containerView.dataSource = self
        
        self.totalSaleLabel.textColor = UIColor.belizeHoleColor()
        self.profitLabel.textColor = UIColor.pumpkinColor()
        self.moneyIncomeLabel.textColor = UIColor.greenSeaColor()
        self.totalDebtLabel.textColor = UIColor.wisteriaColor()
        self.totalSalesCountLabel.textColor = UIColor.midnightBlueColor()
        
        self.totalOutgoneMoneyLabel.textColor = UIColor.pomergranateColor()
        self.cashLabel.textColor = UIColor.peterRiverColor()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if OrderFilterMemory.shared.customerId > 0 {
            let customerId = OrderFilterMemory.shared.customerId
            if let customer__ = Database.shared.customerBy(customerId:customerId) {
                self.clientButton.setTitle("\(customer__.firstName) \(customer__.secondName)", for: .normal)
            }
        }
        
        self.reloadAllData()
    }
    
    override func cancelVC(_ sender: AnyObject) -> Void {
        OrderFilterMemory.shared.clearAllVariables()
        super.cancelVC(sender)
    }
    
    @IBAction func indexChanged(_ sender: UISegmentedControl) {
        self.selectedSegmentIndex = sender.selectedSegmentIndex
        if self.selectedSegmentIndex == 3 {
            self.customDateDidPressed()
        } else {
            self.reloadAllData()
        }
    }
    
    @IBAction func customerDidPressed(_ sender: Any) {
        let customerListVC = CustomersListVC(initialString: nil, isComingFromOrder: true)
        self.navigationController?.pushViewController(customerListVC, animated: true)
    }
    
    func filterPressed(_ sender: UIBarButtonItem) {
        print("filterPressed")
    }
    
    private func updateIpadLabel(_ startDate: Date, endDate: Date) -> Void {
        OrderFilterMemory.shared.startDate = startDate
        OrderFilterMemory.shared.endDate = endDate
        
        self.dateLabel.text = "\(startDate.toString(.custom("EEEE, dd MMMM HH:mm")))   -   \(endDate.toString(.custom("EEEE, dd MMMM HH:mm")))" 
    }
    
    func reloadAllData() -> Void {
        switch self.selectedSegmentIndex
        {
        case 0:
            let startDate = Date().dateAtStartOfDay()
            let endDate = Date()
            self.updateIpadLabel(startDate, endDate: endDate)
            
            let customerId = OrderFilterMemory.shared.customerId
            self.getOrders(startDate, endDate, customerId: customerId)
        case 1:
            let startDate = Date().dateAtStartOfWeek()
            let endDate = Date()
            self.updateIpadLabel(startDate, endDate: endDate)
            
            let customerId = OrderFilterMemory.shared.customerId
            self.getOrders(startDate, endDate, customerId: customerId)
        case 2:
            let startDate = Date().dateAtTheStartOfMonth().dateAtStartOfDay()
            let endDate = Date()
            self.updateIpadLabel(startDate, endDate: endDate)
            
            let customerId = OrderFilterMemory.shared.customerId
            self.getOrders(startDate, endDate, customerId: customerId)
        case 3:
            let customerId = OrderFilterMemory.shared.customerId
            let startDate = OrderFilterMemory.shared.startDate
            let endDate = OrderFilterMemory.shared.endDate
            if let startDate_ = startDate, let endDate_ = endDate {
                self.updateIpadLabel(startDate_, endDate: endDate_)
                self.getOrders(startDate_, endDate_, customerId: customerId)
            }
        default:
            print("default")
            break
        }
    }
    
    func updateLabels() {
        
        var totalSumOfSaledProuctsMoney: Double = 0.0
        var totalSumOfIncomeMoney: Double = 0.0
        var totalSumOfDebtIncomeMoney: Double = 0.0
        var totalSumOfSaleIncomeMoney: Double = 0.0
        var totalSumOfProfitMoney: Double = 0.0
        var totalSumOfSales: Int32 = 0
        var totalSumOfDebtGived: Double = 0.0
        for customerOrder in self.customerOrders {
            totalSumOfSaledProuctsMoney = totalSumOfSaledProuctsMoney + customerOrder.productsSaledMoney
            totalSumOfIncomeMoney = totalSumOfIncomeMoney + customerOrder.totalIncomeMoney
            totalSumOfDebtIncomeMoney = totalSumOfDebtIncomeMoney + customerOrder.debtIncomeMoney
            totalSumOfSaleIncomeMoney = totalSumOfSaleIncomeMoney + customerOrder.saleIncomeMoney
            totalSumOfProfitMoney = totalSumOfProfitMoney + customerOrder.moneyProffit
            totalSumOfSales = totalSumOfSales + customerOrder.salesCount
            
            let kkk = (customerOrder.productsSaledMoney - customerOrder.saleIncomeMoney)
            if kkk > 0 {
                totalSumOfDebtGived = totalSumOfDebtGived + kkk
            }
        }
        
        self.totalSaleLabel.text = Global.shared.priceFormatter.string(for: totalSumOfSaledProuctsMoney)
        self.moneyIncomeLabel.text = Global.shared.priceFormatter.string(for: totalSumOfIncomeMoney)
        
        self.totalDebtLabel.text = Global.shared.priceFormatter.string(for: totalSumOfDebtGived)
        
        self.profitLabel.text = Global.shared.priceFormatter.string(for: totalSumOfProfitMoney)
        self.totalSalesCountLabel.text = Global.shared.amountFormatter.string(for: totalSumOfSales)
        
        let totalDebtIncomeStr = Global.shared.priceFormatter.string(for: totalSumOfDebtIncomeMoney)!
        let totalSaleIncomeStr = Global.shared.priceFormatter.string(for: totalSumOfSaleIncomeMoney)!
        
        self.moneyIncomeDivisionLabel.text = "Приход от долгов: \(totalDebtIncomeStr) \nприход от продажи: \(totalSaleIncomeStr)"
        
        if let last = self.customerOrders.last {
            self.totalOutgoneMoneyLabel.text = Global.shared.priceFormatter.string(for: last.sumOfMoneyOutgone)
            self.cashLabel.text = Global.shared.priceFormatter.string(for: (totalSumOfIncomeMoney - last.sumOfMoneyOutgone))
        }
    }
    
    func customDateDidPressed() {
        
        let statVC = StatisticFilterVC(initialItem: "")
        statVC.applyFiltersBlock = {[weak self] (startDate_: Date, endDate_: Date) -> Void in
            if let strongSelf = self {
                
                strongSelf.updateIpadLabel(startDate_, endDate: endDate_)
                
                let customerId = OrderFilterMemory.shared.customerId
                
                OrderFilterMemory.shared.startDate = startDate_
                OrderFilterMemory.shared.endDate = endDate_
                
                strongSelf.getOrders(startDate_, endDate_, customerId: customerId)
            }
        }
        statVC.cancelBlock = {[weak self] (finished: Bool) -> Void in
            self?.segmentedControl.selectedSegmentIndex = 0
            self?.selectedSegmentIndex = 0
            self?.reloadAllData()
        }
        self.presentSheet(viewController: statVC)
    }
    
    func staffPressed(_ sender: UIBarButtonItem) {
        
        let staffListVC: StaffListVC = StaffListVC(initialString: nil, isCommingFromCustomer: true)
        staffListVC.staffSelectBlock = {[weak self]  (employee: User) -> Void in
            self?.showNotificationView(employee.firstName + " " + employee.secondName)
            OrderFilterMemory.shared.staffId = employee.userId
            self?.reloadAllData()
        }
        
        staffListVC.modalPresentationStyle = UIModalPresentationStyle.popover
        staffListVC.preferredContentSize = CGSize(width: 400, height: 400)
        
        present(staffListVC, animated: true, completion: nil)
        
        let popoverPresentationController = staffListVC.popoverPresentationController
        popoverPresentationController?.barButtonItem = sender
    }
    
    func getOrders(_ startDate: Date, _ endDate: Date, customerId: UInt64) {
        self.showLoading("Подсчет данных ...")
        Database.shared.loadOrdersForFilters(documentType: .none,
                                             startDate: startDate,
                                             endDate: endDate,
                                             customerId: customerId,
                                             supplierId: 0,
                                             userId: OrderFilterMemory.shared.staffId,
                                             limit: 5000,
                                             filterOptions: []) {[weak self] (orders_: [Order]) in
                                                print(orders_)
                                                self?.totalOutgoneMony(orders_)
                                                StatGenerator.shared.calculateCustomerOrders(orders: orders_, completion: {[weak self] (k:
                                                    [CustomerOrder]) in
                                                    self?.dismissLoading()
                                                    self?.customerOrders = k
                                                    self?.containerView.reloadData()
                                                    self?.updateLabels()
                                                })
        }
    }
    
    func totalOutgoneMony(_ orders_: [Order]) {
        var totalMoneyOutgone: Double = 0
        var totalMoneyIncome: Double = 0.0
        generateQue.async {
            for order in orders_ {
                if order.isEdited {
                    continue
                }
                switch order.orderDocument {
                case .moneyGone:
                    if let payment = Database.shared.paymentBy(paymentId: order.paymentId) {
                        totalMoneyOutgone = totalMoneyOutgone + payment.totalPriceWithDiscount
                    }
                case .moneyReceive:
                    if let payment = Database.shared.paymentBy(paymentId: order.paymentId) {
                        totalMoneyIncome = totalMoneyIncome + payment.totalPriceWithDiscount
                    }
                default:
                    print("")
                }
            }
            DispatchQueue.main.async {
                self.totalOutgoneMoneyLabel.text = Global.shared.priceFormatter.string(for: totalMoneyOutgone)
                self.cashLabel.text = Global.shared.priceFormatter.string(for: (totalMoneyIncome - totalMoneyOutgone))
            }
        }
    }
    
    private func showNotificationView(_ message_: String) -> Void {
        
        if let notifView = self.notificationView {
            notifView.setVisible(false, animated: false, completion: {[weak self] in
            })
        }
        
        self.notificationView = CSNotificationView(parentViewController: self,
                                                   tintColor: UIColor.midnightBlueColor(),
                                                   image: UIImage(named: "close-X")!,
                                                   message: message_)
        
        self.notificationView?.tapHandler = {[weak self] (Void) -> Void in
            self?.hideNotificationView()
        }
        
        self.notificationView?.setVisible(true, animated: true, completion: {[weak self] in
        })
    }
    
    private func hideNotificationView() -> Void {
        if let notifView = self.notificationView {
            notifView.setVisible(false, animated: true, completion: {[weak self] in
                if let strongSelf = self {
                    OrderFilterMemory.shared.staffId = 0
                    strongSelf.reloadAllData()
                }
            })
        }
    }
    
    // MARK: TableView DataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.customerOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomerOrderTVCell", for: indexPath) as! CustomerOrderTVCell
        cell.customerOrder = self.customerOrders[(indexPath as NSIndexPath).row]
        cell.reloadData(indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
