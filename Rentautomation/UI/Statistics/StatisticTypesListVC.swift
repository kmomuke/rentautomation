//
//  StatisticTypesListVC.swift
//  Rentautomation
//
//  Created by kanybek on 4/13/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class StatisticTypesListVC: FormViewController {
    
    var initialString: String?
    
    convenience init() {
        self.init(initialString: nil)
    }
    
    init(initialString: String?) {
        self.initialString = initialString
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Отчеты о продажах"
        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
    }
    
    // MARK: Private
    private func configure() {
        
        let backBarButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backBarButton
        
        // Create RowFormers
        let createMenu: ((String, UIColor, (() -> Void)?) -> RowFormer) = { text, color, onSelected in
            return LabelRowFormer<FormLabelCell>() {
                $0.titleLabel.textColor = color
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                }.configure {
                    $0.text = text
                    $0.rowHeight = 70
                }.onSelected { _ in
                    onSelected?()
            }
        }
        
        let allDocumentTypesRow = createMenu("Отчеты о продаже и прибыли", .formerColor()) { [weak self] in
            let navController = self?.splitViewController?.viewControllers.last
            let controller = navController?.childViewControllers.last
            
            if controller is MainStatisticsVC {
                return
            }
            
            let statisticsVC = MainStatisticsVC(initalString: "")
            let statisticsNC = RNNavigationController(rootViewController:statisticsVC)
            self?.splitViewController?.showDetailViewController(statisticsNC, sender: self)
        }
        
        let debtStatisticsRow = createMenu("Отчеты о продаже, прибыли и по долгам клиентов", .formerColor()) { [weak self] in
            let navController = self?.splitViewController?.viewControllers.last
            let controller = navController?.childViewControllers.last
            
            if controller is DebtStatisticsVC {
                return
            }
            
            let statisticsVC = DebtStatisticsVC(initalString: "")
            let statisticsNC = RNNavigationController(rootViewController:statisticsVC)
            self?.splitViewController?.showDetailViewController(statisticsNC, sender: self)
        }
        
        let saleColor = Order.orderDocumentTypeColor(orderDocType: .saleToCustomer)
        let customerPreOrderTypesRow = createMenu("Самые продоваемые товары", saleColor) { [weak self] in
            let statisticsVC = MostSaledProductsVC(initialOrderDocumentType: .saleToCustomer)
            let statisticsNC = RNNavigationController(rootViewController:statisticsVC)
            self?.splitViewController?.showDetailViewController(statisticsNC, sender: self)
        }
        
        let receiveColor = Order.orderDocumentTypeColor(orderDocType: .receiveFromSupplier)
        let supplierIncomeRow = createMenu("Закупаемые товары", receiveColor) { [weak self] in
            let statisticsVC = MostSaledProductsVC(initialOrderDocumentType: .receiveFromSupplier)
            let statisticsNC = RNNavigationController(rootViewController:statisticsVC)
            self?.splitViewController?.showDetailViewController(statisticsNC, sender: self)
        }
        
        let preOrderColor = Order.orderDocumentTypeColor(orderDocType: .preOrderToCustomer)
        let preOrderToCustomerRow = createMenu("Заказы", preOrderColor) { [weak self] in
            let statisticsVC = MostSaledProductsVC(initialOrderDocumentType: .preOrderToCustomer)
            let statisticsNC = RNNavigationController(rootViewController:statisticsVC)
            self?.splitViewController?.showDetailViewController(statisticsNC, sender: self)
        }
        
        
        let graph2Row = createMenu("Графика 2", .formerColor()) { [weak self] in
            
            let navController = self?.splitViewController?.viewControllers.last
            let controller = navController?.childViewControllers.last
            
            if controller is GraphsScreenVC {
                return
            }
            
            let statisticsVC = GraphsScreenVC(initalString: "")
            let statisticsNC = RNNavigationController(rootViewController:statisticsVC)
            self?.splitViewController?.showDetailViewController(statisticsNC, sender: self)
        }
        
        let graphRow = createMenu("График продаж за \(Global.shared.statisticShowDay) дней", .formerColor()) { [weak self] in
            
            let navController = self?.splitViewController?.viewControllers.last
            let controller = navController?.childViewControllers.last
            
            if controller is GraphsScreenVC2 {
                return
            }
            
            let statisticsVC = GraphsScreenVC2(initalString: "")
            let statisticsNC = RNNavigationController(rootViewController:statisticsVC)
            self?.splitViewController?.showDetailViewController(statisticsNC, sender: self)
        }
        
        let customerDebtsRow = createMenu("Общая сумма долгов клиентов", .formerColor()) { [weak self] in
            
            let navController = self?.splitViewController?.viewControllers.last
            let controller = navController?.childViewControllers.last
            
            if controller is TotalCustomerDebtsVC {
                return
            }
            
            let statisticsVC = TotalCustomerDebtsVC(initalString: "")
            let statisticsNC = RNNavigationController(rootViewController:statisticsVC)
            self?.splitViewController?.showDetailViewController(statisticsNC, sender: self)
        }
        
        // Create Headers and Footers
        let createHeader: ((String) -> ViewFormer) = { text in
            return LabelViewFormer<FormLabelHeaderView>()
                .configure {
                    $0.text = text
                    $0.viewHeight = 40
            }
        }
        
        let realExampleSection1 = SectionFormer(rowFormer: debtStatisticsRow, graphRow, customerPreOrderTypesRow, supplierIncomeRow, preOrderToCustomerRow, customerDebtsRow)
            .set(headerViewFormer: createHeader(" "))
        
        former.append(sectionFormer: realExampleSection1)
    }
}
