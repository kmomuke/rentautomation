//
//  DateValueFormatter.swift
//  Rentautomation
//
//  Created by kanybek on 4/14/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import Charts

class DateValueFormatter: NSObject, IAxisValueFormatter {
    
    let  dateFormatter: DateFormatter = DateFormatter()
    
    var initalString: String?
    
    convenience override init() {
        self.init(initalString: "")
    }
    
    init(initalString: String) {
        self.initalString = initalString
        dateFormatter.dateFormat = "dd MMM";
    }
    
    func stringForValue(_ value: Double,
                        axis: AxisBase?) -> String {
        print("value ==> \(value)")
        let k = Global.shared.statisticShowDay - Int(value)
        return dateFormatter.string(from: Date().dateBySubtractingDays(k))
    }
}
