//
//  MostSaledProductsVC.swift
//  Rentautomation
//
//  Created by kanybek on 4/13/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import CSNotificationView

class MostSaledProductsVC: BaseListVC, ErrorPopoverRenderer, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var totalSaledProductsLabel: UILabel!
    @IBOutlet weak var orderCountLabel: UILabel!
    
    var selectedSegmentIndex: Int = 0
    
    var notificationView: CSNotificationView?
    
    var isFirstTime: Bool = true
    var mostSaledProducts: [MostProduct] = []
    
    var initialOrderDocumentType: OrderDocumentType
    
    convenience init() {
        self.init(initialOrderDocumentType: .none)
    }
    
    init(initialOrderDocumentType: OrderDocumentType) {
        self.initialOrderDocumentType = initialOrderDocumentType
        super.init(nibName: "MostSaledProductsVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("MostSaledProductsVC deallocated")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addCanceButton()
        
        self.totalSaledProductsLabel.text = "0"
        self.orderCountLabel.text = "0"
        
        let targetName = Global.shared.targetName
        if targetName == "TazaPos-lite" || targetName == "TazaProd" {
        } else {
            let generateButton = UIBarButtonItem(
                title: "Сотрудник",
                style: .plain,
                target: self,
                action: #selector(staffPressed(_:))
            )
            self.navigationItem.rightBarButtonItem = generateButton
        }
        
        OrderFilterMemory.shared.clearAllVariables()
        
        let endDate = Date()
        let startDate = endDate.dateAtStartOfDay()
        OrderFilterMemory.shared.startDate = startDate
        OrderFilterMemory.shared.endDate = endDate
        
        self.title = Order.orderDocumentTypeStringName(orderDocType: self.initialOrderDocumentType)
        
        let font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightMedium)
        self.segmentedControl.setTitleTextAttributes([NSFontAttributeName: font],
                                                     for: .normal)
        
        let nib = UINib(nibName: "ProductStatisticCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "ProductStatisticCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (isFirstTime) {
            let endDate = Date()
            let startDate = endDate.dateAtStartOfDay()
            self.updateIpadLabel(startDate, endDate: endDate)
            self.reloadAllData(startDate_: startDate, endDate_: endDate)
            isFirstTime = false
        }
    }
    
    func reloadAllData(startDate_: Date, endDate_: Date) {
        
        self.showLoading("Получение данных...")
        StatGenerator.shared.calculateMostSaledProducts(startDate: startDate_,
                                                        endDate: endDate_,
                                                        orderDocType: self.initialOrderDocumentType,
                                                        userId: OrderFilterMemory.shared.staffId)
        {[weak self] (p: [MostProduct]) in
            if let strongSelf = self {
                strongSelf.dismissLoading()
                if let lastP_ = p.last {
                    strongSelf.totalSaledProductsLabel.text = Global.shared.amountFormatter.string(for: lastP_.totalSaledAmount)
                    strongSelf.orderCountLabel.text = Global.shared.amountFormatter.string(for: lastP_.orderCount)
                } else {
                    strongSelf.totalSaledProductsLabel.text = "0"
                    strongSelf.orderCountLabel.text = "0"
                }
                strongSelf.mostSaledProducts = p
                strongSelf.tableView.reloadData()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func customDateDidPressed() {
        
        let statVC = StatisticFilterVC(initialItem: "")
        statVC.applyFiltersBlock = {[weak self] (startDate_: Date, endDate_: Date) -> Void in
            if let strongSelf = self {
                strongSelf.updateIpadLabel(startDate_, endDate: endDate_)
                strongSelf.reloadAllData(startDate_: startDate_, endDate_: endDate_)
            }
        }
        statVC.cancelBlock = {[weak self] (finished: Bool) -> Void in
            if let strongSelf = self {
                strongSelf.segmentedControl.selectedSegmentIndex = 0
                strongSelf.indexChanged(strongSelf.segmentedControl)
            }
        }
        self.presentSheet(viewController: statVC)
    }
    
    @IBAction func indexChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex
        {
        case 0:
            let endDate = Date()
            let startDate = endDate.dateAtStartOfDay()
            self.updateIpadLabel(startDate, endDate: endDate)
            self.reloadAllData(startDate_: startDate, endDate_: endDate)
        case 1:
            let endDate = Date()
            let startDate = endDate.dateBySubtractingWeeks(1)
            self.updateIpadLabel(startDate, endDate: endDate)
            self.reloadAllData(startDate_: startDate, endDate_: endDate)
        case 2:
            let endDate = Date()
            let startDate = endDate.dateBySubtractingMonths(1)
            self.updateIpadLabel(startDate, endDate: endDate)
            self.reloadAllData(startDate_: startDate, endDate_: endDate)
        case 3:
            let endDate = Date()
            let startDate = endDate.dateBySubtractingMonths(3)
            self.updateIpadLabel(startDate, endDate: endDate)
            self.reloadAllData(startDate_: startDate, endDate_: endDate)
        case 4:
            self.customDateDidPressed()
        default:
            print("default")
            break
        }
    }
    
    private func updateIpadLabel(_ startDate: Date, endDate: Date) -> Void {
        OrderFilterMemory.shared.startDate = startDate
        OrderFilterMemory.shared.endDate = endDate
        self.dateLabel.text = "\(startDate.toString(.custom("EEE, dd MMM HH:mm")))    -    \(endDate.toString(.custom("EEE, dd MMM HH:mm")))"
    }
    
    // MARK: TableView DataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mostSaledProducts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductStatisticCell", for: indexPath) as! ProductStatisticCell
        let mostPr: MostProduct = mostSaledProducts[(indexPath as NSIndexPath).row]
        cell.mostProduct = mostPr
        cell.reloadData(row: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let _ = mostSaledProducts[(indexPath as NSIndexPath).row]
    }
}

//
extension MostSaledProductsVC {
    
    func staffPressed(_ sender: UIBarButtonItem) {
        
        if let notifView = self.notificationView {
            notifView.setVisible(false, animated: false, completion: {[weak self] in
            })
        }
        
        let staffListVC: StaffListVC = StaffListVC(initialString: nil, isCommingFromCustomer: true)
        staffListVC.staffSelectBlock = {[weak self]  (employee: User) -> Void in
            self?.showNotificationView(employee.firstName + " " + employee.secondName)
            OrderFilterMemory.shared.staffId = employee.userId
            if let startDate_ = OrderFilterMemory.shared.startDate,
                let endDate_ = OrderFilterMemory.shared.endDate {
                self?.reloadAllData(startDate_: startDate_,
                                    endDate_: endDate_)
            }
        }
        
        staffListVC.modalPresentationStyle = UIModalPresentationStyle.popover
        staffListVC.preferredContentSize = CGSize(width: 400, height: 400)
        
        present(staffListVC, animated: true, completion: nil)
        
        let popoverPresentationController = staffListVC.popoverPresentationController
        popoverPresentationController?.barButtonItem = sender
    }
    
    private func showNotificationView(_ message_: String) -> Void {
        
        self.notificationView = CSNotificationView(parentViewController: self,
                                                   tintColor: UIColor.midnightBlueColor(),
                                                   image: UIImage(named: "close-X")!,
                                                   message: message_)
        
        self.notificationView?.tapHandler = {[weak self] (Void) -> Void in
            self?.hideNotificationView()
        }
        
        self.notificationView?.setVisible(true, animated: true, completion: {[weak self] in
        })
    }
    
    private func hideNotificationView() -> Void {
        if let notifView = self.notificationView {
            notifView.setVisible(false, animated: true, completion: {[weak self] in
                if let strongSelf = self {
                    OrderFilterMemory.shared.staffId = 0
                    if let startDate_ = OrderFilterMemory.shared.startDate,
                        let endDate_ = OrderFilterMemory.shared.endDate {
                        strongSelf.reloadAllData(startDate_: startDate_,
                                                 endDate_: endDate_)
                    }
                }
            })
        }
    }
}



