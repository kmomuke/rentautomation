//
//  ProductStatisticCell.swift
//  Rentautomation
//
//  Created by kanybek on 4/14/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class ProductStatisticCell: UITableViewCell {

    var mostProduct: MostProduct?
    
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var saledAmount: UILabel!
    @IBOutlet weak var percentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func reloadData(row: Int) -> Void {
        if let mostProduct_ = self.mostProduct {
            self.countLabel.text = "(\(row + 1))"
            var productName: String = ""
            var quantityPerUnit: String = ""
            if let product = Database.shared.productFor(productId: mostProduct_.productId) {
                productName = product.productName
                quantityPerUnit = product.quantityPerUnit
            }
            self.nameLabel.text = productName
            
            let productSaledAmount = String(format: "%.02f", mostProduct_.productAmount)
            self.saledAmount.text = "\(productSaledAmount) \(quantityPerUnit)"
            let percent = (mostProduct_.productAmount / mostProduct_.totalSaledAmount) * 100.0
            let duration = String(format: "%.02f", percent)
            self.percentLabel.text = "\(duration) %"
            
        } else {
            self.resetLabels()
        }
    }
    
    override func prepareForReuse() {
        self.mostProduct = nil
        self.resetLabels()
    }
    
    func resetLabels() -> Void {
        self.countLabel.text = ""
        self.nameLabel.text = ""
        self.saledAmount.text = ""
        self.percentLabel.text = ""
    }
}
