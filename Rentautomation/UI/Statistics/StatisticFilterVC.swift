//
//  StatisticFilterVC.swift
//  Rentautomation
//
//  Created by kanybek on 5/3/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit


class StatisticFilterVC: FormViewController, ErrorPopoverRenderer {
    
    var initialItem: String?
    var applyFiltersBlock : ((Date, Date) -> Void)?
    var cancelBlock : ((Bool) -> Void)?
    
    convenience init() {
        self.init(initialItem: nil)
    }
    
    init(initialItem: String?) {
        self.initialItem = initialItem
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    // MARK: Public
    override func viewDidLoad() {
        
        super.viewDidLoad()
        let canceButton = UIBarButtonItem(
            title: "Отмена",
            style: .plain,
            target: self,
            action: #selector(cancelObject(_:))
        )
        self.view.backgroundColor = UIColor.white
        self.navigationItem.leftBarButtonItem = canceButton
        
        let createButton = UIBarButtonItem(
            title: "Применить",
            style: .plain,
            target: self,
            action: #selector(applyFilters(_:))
        )
        
        self.navigationItem.rightBarButtonItem = createButton
        configure()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
        for sections in self.former.sectionFormers {
            for row in sections.rowFormers {
                row.update()
            }
        }
    }
    
    func applyFilters(_ sender: AnyObject) -> Void {
        
        if let _ = OrderFilterMemory.shared.startDate {
            
        } else {
            let _ = self.showErrorAlert(text: "Выберите дата начало")
            return
        }
        
        if let _ = OrderFilterMemory.shared.endDate {
            
        } else {
            let _ = self.showErrorAlert(text: "Выберите дата конец")
            return
        }
        
        if let startDate_ = OrderFilterMemory.shared.startDate,
            let endDate_ = OrderFilterMemory.shared.endDate {
            
            if let applyFiltersBlock = self.applyFiltersBlock {
                applyFiltersBlock(startDate_, endDate_)
            }
            
            self.navigationController?.dismiss(animated: true, completion: {
                print("dismiss OrderFiltersVC")
            })
        }
    }
    
    func cancelObject(_ sender: AnyObject) -> Void {
        if let cancelBlock_ = self.cancelBlock {
            cancelBlock_(true)
        }
        self.navigationController?.dismiss(animated: true, completion: {
            print("dismiss OrderFiltersVC")
        })
    }
    
    private func configure() {
        
        title = "Промежуток времени"
        tableView.contentInset.top = 10
        tableView.contentInset.bottom = 30
        tableView.contentOffset.y = -10
        
        // Create Headers
        let createHeader: (() -> ViewFormer) = {
            return CustomViewFormer<FormHeaderFooterView>()
                .configure {
                    $0.viewHeight = 20
                    $0.view.backgroundColor = UIColor.white
            }
        }
        
        let calendarButtonRow: ((Int, String, (() -> Void)?) -> RowFormer) = { [weak self] index, text, onSelected in
            return LabelRowFormer<FormLabelCell>() {
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = .boldSystemFont(ofSize: 16)
                $0.subTextLabel.textColor = .formerSubColor()
                $0.subTextLabel.font = .boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                
                }.configure {
                    $0.text = text
                    $0.rowHeight = 60
                }.onSelected { [weak self] _ in
                    onSelected?()
                    self?.former.deselect(animated: true)
                }.onUpdate { [weak self] in
                    
                    if index == 1 {
                        if let startDate = OrderFilterMemory.shared.startDate {
                            $0.subText = "\(startDate.toString(.custom("EE, dd MMM HH:mm")))"
                        }
                    } else if index == 2 {
                        if let endDate = OrderFilterMemory.shared.endDate {
                            $0.subText = "\(endDate.toString(.custom("EE, dd MMM HH:mm")))"
                        }
                    }
            }
        }
        
        let calendarStartRow = calendarButtonRow(1, "Дата начало") { [weak self] in
            let calendarVC = CalendarPickerVC(initialItem: "Дата начало")
            self?.navigationController?.pushViewController(calendarVC, animated: true)
        }
        
        let calendarEndRow = calendarButtonRow(2, "Дата конец") { [weak self] in
            let calendarVC = CalendarPickerVC(initialItem: "Дата конец")
            self?.navigationController?.pushViewController(calendarVC, animated: true)
        }
        
        let dateSection = SectionFormer(rowFormer: calendarStartRow, calendarEndRow)
            .set(headerViewFormer: createHeader())
        
        former.append(sectionFormer: dateSection)
    }
}
