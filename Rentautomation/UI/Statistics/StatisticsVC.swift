//
//  StatisticsVC.swift
//  Rentautomation
//
//  Created by kanybek on 4/6/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit


class StatisticsVC: FormViewController, ErrorPopoverRenderer {
    
    var startDate: Date?
    var endDate: Date?
    
    var totalSumOfSaledProuctsMoney: Double = 0.0
    var totalSumOfProfitMoney: Double = 0.0
    var totalSumOfDebtGived: Double = 0.0
    var totalSumOfSales: UInt32 = 0
    
    var sumOfMoneyOutgone: Double = 0.0
    var totalSumOfIncomeMoney: Double = 0.0
    
    
    convenience init() {
        self.init(startDate: Date().dateAtStartOfDay(), endDate: Date())
    }
    
    init(startDate: Date, endDate: Date) {
        self.startDate = startDate
        self.endDate = endDate
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func cancelVC(_ sender: AnyObject) -> Void {
        let empty = EmptyVC()
        let detailNavigationController = RNNavigationController(rootViewController:empty)
        self.splitViewController?.showDetailViewController(detailNavigationController, sender: self)
    }
    
    private enum InsertPosition: Int {
        case Below, Above
    }
    private var insertRowPosition: InsertPosition = .Below
    
    // MARK: Public
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        self.applyFiltersToOrder(startDate: self.startDate!,
                                 endDate: self.endDate!,
                                 customerId: 0,
                                 supplierId: 0,
                                 staffId: 0,
                                 documentType: .none)
    }
    
    func reloadRows(startDate: Date, endDate: Date, customerId: UInt64) -> Void {
        self.applyFiltersToOrder(startDate: startDate,
                                 endDate: endDate,
                                 customerId: customerId,
                                 supplierId: 0,
                                 staffId: 0,
                                 documentType: .none)
    }
    
    private func applyFiltersToOrder(startDate: Date,
                                     endDate: Date,
                                     customerId: UInt64,
                                     supplierId: UInt64,
                                     staffId: UInt64,
                                     documentType: OrderDocumentType) -> Void {
        
        self.showLoading("Подсчет данных ...")
        Database.shared.loadOrdersForFilters(documentType: .none,
                                             startDate: startDate,
                                             endDate: endDate,
                                             customerId: customerId,
                                             supplierId: 0,
                                             userId: 0,
                                             limit: 5000,
                                             filterOptions: []) {[weak self] (orders_: [Order]) in
                                                self?.totalOutgoneMony(orders_)
                                                self?.calculateOrders(orders: orders_)
        }
    }
    
    func totalOutgoneMony(_ orders_: [Order]) {
        
        var totalMoneyOutgone: Double = 0
        var totalMoneyIncome: Double = 0.0
        
        generateQue.async {
            for order in orders_ {
                
                if order.isEdited {
                    continue
                }
                
                switch order.orderDocument {
                case .moneyGone:
                    if let payment = Database.shared.paymentBy(paymentId: order.paymentId) {
                        totalMoneyOutgone = totalMoneyOutgone + payment.totalPriceWithDiscount
                    }
                case .moneyReceive:
                    if let payment = Database.shared.paymentBy(paymentId: order.paymentId) {
                        totalMoneyIncome = totalMoneyIncome + payment.totalPriceWithDiscount
                    }
                default:
                    print("")
                }
            }
            DispatchQueue.main.async {
                self.sumOfMoneyOutgone = totalMoneyOutgone
                self.totalSumOfIncomeMoney = totalMoneyIncome
            }
        }
    }
    
    func updateLabels(_ custOrders: [CustomerOrder]) {
        
        var totalSumOfSaledProuctsMoney: Double = 0.0
        var totalSumOfIncomeMoney: Double = 0.0
        var totalSumOfDebtIncomeMoney: Double = 0.0
        var totalSumOfSaleIncomeMoney: Double = 0.0
        var totalSumOfProfitMoney: Double = 0.0
        var totalSumOfSales: Int32 = 0
        var totalSumOfDebtGived: Double = 0.0
        
        for customerOrder in custOrders {
            totalSumOfSaledProuctsMoney = totalSumOfSaledProuctsMoney + customerOrder.productsSaledMoney
            totalSumOfIncomeMoney = totalSumOfIncomeMoney + customerOrder.totalIncomeMoney
            totalSumOfDebtIncomeMoney = totalSumOfDebtIncomeMoney + customerOrder.debtIncomeMoney
            totalSumOfSaleIncomeMoney = totalSumOfSaleIncomeMoney + customerOrder.saleIncomeMoney
            totalSumOfProfitMoney = totalSumOfProfitMoney + customerOrder.moneyProffit
            totalSumOfSales = totalSumOfSales + customerOrder.salesCount
            
            let kkk = (customerOrder.productsSaledMoney - customerOrder.saleIncomeMoney)
            if kkk > 0 {
                totalSumOfDebtGived = totalSumOfDebtGived + kkk
            }
        }
        
        self.totalSumOfSaledProuctsMoney = totalSumOfSaledProuctsMoney
        self.totalSumOfProfitMoney = totalSumOfProfitMoney
        self.totalSumOfDebtGived = totalSumOfDebtGived
        self.totalSumOfSales = UInt32(totalSumOfSales)
        self.totalSumOfIncomeMoney = totalSumOfIncomeMoney
        
        if let last = custOrders.last {
            self.sumOfMoneyOutgone = last.sumOfMoneyOutgone
        }
        
        self.updateAllRows()
    }
    
    private func calculateOrders(orders: [Order]) -> Void {
        StatGenerator.shared.calculateCustomerOrders(orders: orders, completion: {[weak self] (k:
            [CustomerOrder]) in
            self?.dismissLoading()
            self?.updateLabels(k)
        })
    }
    
    private func hideNotificationView() -> Void {
        self.totalSumOfSaledProuctsMoney = 0
        self.totalSumOfProfitMoney = 0
        self.totalSumOfDebtGived = 0
        self.totalSumOfSales = 0
        self.totalSumOfIncomeMoney = 0
        self.sumOfMoneyOutgone = 0
        self.updateAllRows()
    }
    
    private func updateAllRows() -> Void {
        for sections in self.former.sectionFormers {
            for row in sections.rowFormers {
                row.update()
            }
        }
    }
    
    private func configure() {
        title = "Деньги за промежуток времени"
        
        tableView.contentInset.top = 0
        tableView.contentInset.bottom = 0
        tableView.backgroundColor = UIColor.white
        self.view.backgroundColor = UIColor.white
        
        let createCustomButtonRow: ((String, String, (() -> Void)?, ((LabelRowFormer<FormLabelCell>) -> Void)?) -> RowFormer) = { [weak self] text, subText, onSelected, onUpdated in
            return LabelRowFormer<FormLabelCell>() {
                
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .none
                $0.subTextLabel.textColor = .formerSubColor()
                $0.subTextLabel.font = .boldSystemFont(ofSize: 14)
                
                }.configure {
                    $0.text = text
                    $0.rowHeight = 55
                    $0.subText = subText
                }.onSelected { [weak self] _ in
                    onSelected?()
                    self?.former.deselect(animated: true)
                }.onUpdate { [weak self] m in
                    onUpdated?(m)
            }
        }
        
        let someText = Global.shared.priceFormatter.string(for: self.totalSumOfSaledProuctsMoney)
        let customerSaleRow = createCustomButtonRow("Продажи", someText!, {
            print("customSaleRow")
        }) { (subLabel: LabelRowFormer<FormLabelCell>) in
            subLabel.subText = Global.shared.priceFormatter.string(for: self.totalSumOfSaledProuctsMoney)
        }
        
        let moneyIncomeRow = createCustomButtonRow("Прибыль", "", {
            print("supplierIncomeRow")
        }) { (subLabel: LabelRowFormer<FormLabelCell>) in
            subLabel.subText = Global.shared.priceFormatter.string(for: self.totalSumOfProfitMoney)
        }
        
        let ordersAmountRow = createCustomButtonRow("Ко-во продаж", "", {
            print("incomeRow")
        }) { (subLabel: LabelRowFormer<FormLabelCell>) in
            subLabel.subText = Global.shared.amountFormatter.string(for: self.totalSumOfSales)
        }
        
        let realMoneyIncomeRow = createCustomButtonRow("Приход денег", "", {
            print("realMoneyIncomeRow")
        }) { (subLabel: LabelRowFormer<FormLabelCell>) in
            subLabel.subText = Global.shared.priceFormatter.string(for: self.totalSumOfIncomeMoney)
        }
        
        let realMoneyOutgoneRow = createCustomButtonRow("Расход денег", "", {
            print("realMoneyOutgoneRow")
        }) { (subLabel: LabelRowFormer<FormLabelCell>) in
            subLabel.subText = Global.shared.priceFormatter.string(for: self.sumOfMoneyOutgone)
        }
        
        let debtRow = createCustomButtonRow("В долг", "", {
            print("realMoneyIncomeRow")
        }) { (subLabel: LabelRowFormer<FormLabelCell>) in
            subLabel.subText = Global.shared.priceFormatter.string(for: self.totalSumOfDebtGived)
        }
        
        let differRow = createCustomButtonRow("Касса", "", {
            print("outgoneRow")
        }) { (subLabel: LabelRowFormer<FormLabelCell>) in
            subLabel.subText = Global.shared.priceFormatter.string(for: (self.totalSumOfIncomeMoney - self.sumOfMoneyOutgone))
        }
        
        let titleSection0 = SectionFormer(rowFormer:customerSaleRow, moneyIncomeRow, debtRow, ordersAmountRow, realMoneyIncomeRow, realMoneyOutgoneRow, differRow)
            //.set(headerViewFormer: createHeader())
        
        former.append(sectionFormer: titleSection0)
    }
    
    private func configureCell(cell: RowFormer) -> Void {
    }
}
