//
//  TotalCustomerDebtsVC.swift
//  Rentautomation
//
//  Created by kanybek on 5/15/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class TotalCustomerDebtsVC: BaseListVC, ErrorPopoverRenderer {
    var initalString: String?
    
    @IBOutlet weak var totalCustomersDebtLabel: UILabel!
    @IBOutlet weak var totalMyDebtLabel: UILabel!
    
    convenience init() {
        self.init(initalString: "")
    }
    
    init(initalString: String) {
        self.initalString = initalString
        super.init(nibName: "TotalCustomerDebtsVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
            
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addCanceButton()
        self.title = "Общая сумма долгов клиентов"
        
        var totalCustomerDebt: Double = 0.0
        var totalMyDebt: Double = 0.0
        generateQue.async {
            let customers_ = Database.shared.loadAllCustomers(100000)
            for customer in customers_ {
                if let account = Database.shared.accountBy(customerId: customer.customerId) {
                    if account.balance > 0 {
                        totalCustomerDebt = totalCustomerDebt + account.balance
                    } else {
                        totalMyDebt = totalMyDebt + account.balance
                    }
                }
            }
            
            DispatchQueue.main.async {
                self.totalCustomersDebtLabel.text = Global.shared.priceFormatter.string(for: totalCustomerDebt)
                self.totalMyDebtLabel.text = Global.shared.priceFormatter.string(for: totalMyDebt)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
