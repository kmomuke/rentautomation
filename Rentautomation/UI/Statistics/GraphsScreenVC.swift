//
//  GraphsScreenVC.swift
//  Rentautomation
//
//  Created by kanybek on 4/13/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import Charts

class GraphsScreenVC: BaseListVC, ErrorPopoverRenderer {
    
    @IBOutlet weak var chartView: LineChartView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var isFirstTime: Bool = true
    var initalString: String?
    
    convenience init() {
        self.init(initalString: "graphs")
    }
    
    init(initalString: String) {
        self.initalString = initalString
        super.init(nibName: "GraphsScreenVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addCanceButton()
        
        self.title = "График за \(Global.shared.statisticShowDay) дней"
        let font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightMedium)
        self.segmentedControl.setTitleTextAttributes([NSFontAttributeName: font],
                                                     for: .normal)
        
        self.chartView.delegate = self
        self.chartView.chartDescription?.enabled = false
        self.chartView.dragEnabled = true
        self.chartView.setScaleEnabled(true)
        self.chartView.pinchZoomEnabled = true
        self.chartView.drawGridBackgroundEnabled = false
        
        
        // x-axis limit line
        
        let llXAxis = ChartLimitLine(limit: 10.0, label: "Index 10")
        llXAxis.lineWidth = 4.0
        llXAxis.lineDashLengths = [10.0, 10.0, 0.0]
        llXAxis.labelPosition = .rightBottom
        llXAxis.valueFont = UIFont.systemFont(ofSize: 15, weight: UIFontWeightMedium)
        
        //[_chartView.xAxis addLimitLine:llXAxis];
        
        //let xAxis = self.chartView.xAxis
        //xAxis.labelFont = UIFont.systemFont(ofSize: 11, weight: UIFontWeightMedium)
        //xAxis.labelTextColor = UIColor.white
        //xAxis.drawGridLinesEnabled = true
        //xAxis.drawAxisLineEnabled = true
        
        self.chartView.xAxis.gridLineDashLengths = [10.0, 10.0]
        self.chartView.xAxis.gridLineDashPhase = 0.0
        
        let ll1 = ChartLimitLine(limit: 10000.0, label: "Upper Limit")
        ll1.lineWidth = 4.0
        ll1.lineDashLengths = [5.0, 5.0]
        ll1.labelPosition = .rightTop
        ll1.valueFont = UIFont.systemFont(ofSize: 15, weight: UIFontWeightMedium)
        
        let ll2 = ChartLimitLine(limit: -10000.0, label: "Lower Limit")
        ll2.lineWidth = 4.0
        ll2.lineDashLengths = [5.0, 5.0]
        ll2.labelPosition = .rightBottom
        ll2.valueFont = UIFont.systemFont(ofSize: 15, weight: UIFontWeightMedium)
        
        let leftAxis = self.chartView.leftAxis
        leftAxis.removeAllLimitLines()
        leftAxis.addLimitLine(ll1)
        leftAxis.addLimitLine(ll2)
        leftAxis.axisMaximum = 50000.0
        leftAxis.axisMinimum = -50000.0
        leftAxis.gridLineDashLengths = [5.0, 5.0]
        leftAxis.drawZeroLineEnabled = false
        leftAxis.drawLimitLinesBehindDataEnabled = true
        
        self.chartView.rightAxis.enabled = false
        
        //[_chartView.viewPortHandler setMaximumScaleY: 2.f];
        //[_chartView.viewPortHandler setMaximumScaleX: 2.f];
        
        let marker = BalloonMarker(color: UIColor.alizarinColor(),
                                   font: UIFont.systemFont(ofSize: 12, weight: UIFontWeightMedium),
                                   textColor: UIColor.black,
                                   insets: UIEdgeInsets(top: 8.0, left: 8.0, bottom: 20.0, right: 8.0))
        
        marker.chartView = self.chartView
        marker.minimumSize = CGSize(width: 80, height: 40)
        self.chartView.marker = marker
        self.chartView.legend.form = .line
        
        //_sliderX.value = 45.0;
        //_sliderY.value = 100.0;
        
        //[_chartView animateWithXAxisDuration:2.5];
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (isFirstTime) {
            self.showLoading("Получение данных...")
            StatGenerator.shared.calculateOrdersForCustomDates(completion: {[weak self] (calcNumbers: [(Double, UInt32, Double, Double)]) in
                if let strongSelf = self {
                    strongSelf.dismissLoading()
                    strongSelf.setDataCount(calcNumbers: calcNumbers)
                }
            })
            isFirstTime = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setDataCount(calcNumbers: [(Double, UInt32, Double, Double)]) -> Void {
        var moneyProfitEntries: [ChartDataEntry] = []
        var saleEntries: [ChartDataEntry] = []
        for (index, calcNumber) in calcNumbers.enumerated() {
            print("|||||+++++++ \(calcNumber.0)", "\(calcNumber.1)", "\(calcNumber.2)")
            
            let entry = ChartDataEntry(x: Double(index), y: calcNumber.2)
            moneyProfitEntries.append(entry)
            
            let sale = ChartDataEntry(x: Double(index), y: calcNumber.0)
            saleEntries.append(sale)
        }
        
        if let data_ = self.chartView.data {
            if data_.dataSetCount > 0 {
                if let lineChartDataSet = self.chartView.data?.dataSets[0] as? LineChartDataSet {
                    lineChartDataSet.values = moneyProfitEntries
                    self.chartView.data?.notifyDataChanged()
                    self.chartView.notifyDataSetChanged()
                }
            }
        } else {
            
            let set1 = LineChartDataSet(values: moneyProfitEntries, label: "Прибыль")
            
            //set1.drawIconsEnabled = NO;
            
            set1.lineDashLengths = [5.0, 2.5]
            set1.highlightLineDashLengths = [5.0, 2.5]
            set1.setColor(UIColor.blue)
            set1.setCircleColor(UIColor.white)
            set1.lineWidth = 2.0
            set1.circleRadius = 1.0
            set1.fillAlpha = 65/255.0
            set1.drawCircleHoleEnabled = false;
            set1.valueFont = UIFont.systemFont(ofSize: 9, weight: UIFontWeightMedium)
            //set1.formLineDashLengths = []
            //set1.formLineWidth = 1.0
            set1.formSize = 15.0
            
            let set3 = LineChartDataSet(values: saleEntries, label: "Продажи")
            set3.axisDependency = .right
            set3.setColor(UIColor.green)
            set3.setCircleColor(UIColor.white)
            set3.lineWidth = 2.0
            set3.circleRadius = 3.0
            set3.fillAlpha = 65/255.0;
            set3.fillColor = UIColor.yellow
            set3.highlightColor = UIColor.yellow
            set3.drawCircleHoleEnabled = false
            
            //let k1 = ChartColorTemplates.colorFromString("#00ff0000").cgColor
            //let k2 = ChartColorTemplates.colorFromString("#ffff0000").cgColor
            //let gradient = CGGradientCreateWithColors(nil, [k1, k2], nil)
            
            set1.fillAlpha = 1.0
            
            set1.fill = Fill.fillWithCGColor(UIColor.white.cgColor)
            set1.drawFilledEnabled = true
            
            let data = LineChartData(dataSets: [set1, set3])
            self.chartView.data = data
        }
    }
}

extension GraphsScreenVC: ChartViewDelegate {
}

