//
//  NeonExamplesVC.swift
//  Rentautomation
//
//  Created by kanybek on 2/26/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import Neon
class NeonExamplesVC: UIViewController {
    
    var initialString: String?

    convenience init() {
        self.init(initialString: nil)
    }
    
    init(initialString: String?) {
        self.initialString = initialString
        super.init(nibName: "NeonExamplesVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewDidLayoutSubviews() {
//        productImageView.anchorInCorner(.topLeft, xPad: 5, yPad: 5, width: 150, height: 150)
//        historyProductMovementContainerView.align(.underMatchingLeft,
//                                                  relativeTo: productImageView,
//                                                  padding: 500,
//                                                  width: 200,
//                                                  height: 200)
    }
}
