//
//  CreateUserAccountVC.swift
//  Rentautomation
//
//  Created by kanybek on 9/28/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

final class UserAccount {
    
    var nameOfPoint: String
    var password: String
    
    convenience init() {
        self.init(nameOfPoint: "",
                  password: "")
    }
    
    init(nameOfPoint: String,
         password: String) {
        self.nameOfPoint = nameOfPoint
        self.password = password
    }
    
    deinit {
    }
}

class CreateUserAccountVC: FormViewController, ErrorPopoverRenderer {
    
    var initialUserAccount: UserAccount?
    var isKgPhoneMask: Bool = true
    
    convenience init() {
        self.init(initialUserAccount: nil)
    }
    
    init(initialUserAccount: UserAccount?) {
        self.initialUserAccount = initialUserAccount
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("CreateUserAccountVC deallocated")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        StaffMemoryInstance.sharedInstance.clearAllVariables()
        StaffMemoryInstance.sharedInstance.firstName = ""
        StaffMemoryInstance.sharedInstance.password = ""
        
        configure()
        
        self.title = "Создать аккаунт"
        
        let canceButton = UIBarButtonItem(
            title: "Закрыть",
            style: .plain,
            target: self,
            action: #selector(cancelObject(_:))
        )
        
        self.navigationItem.leftBarButtonItem = canceButton
        
        let createButton = UIBarButtonItem(
            title: "Сохранить",
            style: .plain,
            target: self,
            action: #selector(createCustomer(_:))
        )
        
        self.navigationItem.rightBarButtonItem = createButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.neededProductSection.firstRowFormer?.former?.becomeEditingNext()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func cancelObject(_ sender: AnyObject) -> Void {
        StaffMemoryInstance.sharedInstance.clearAllVariables()
        self.navigationController?.dismiss(animated: true, completion: {[weak self] in
            print("dismiss CreateUserAccountVC")
        })
    }
    
    // MARK: CREATE
    func createCustomer(_ sender: AnyObject) -> Bool {
        if (StaffMemoryInstance.sharedInstance.firstName!.characters.count > 0 &&
            StaffMemoryInstance.sharedInstance.password!.characters.count > 0) {
            
            let userAcc = UserAccount(nameOfPoint: StaffMemoryInstance.sharedInstance.firstName!,
                                      password: StaffMemoryInstance.sharedInstance.password!)
            
            UserAccountsStore.shared.save(userAccount: userAcc)
            
            PipesStore.shared.userAccCreatedPipe.putNext(userAcc)
            
            StaffMemoryInstance.sharedInstance.clearAllVariables()
            self.navigationController?.dismiss(animated: true, completion: {[weak self] in
                print("dismiss CreateCustomerVC")
            })
            return true
        }
        return false
    }
    
    // MARK: Private
    private lazy var formerInputAccessoryView: FormerInputAccessoryView = FormerInputAccessoryView(former: self.former)
    
    
    // MARK: Needed product section
    private lazy var neededProductSection: SectionFormer = { [weak self] in
        
        let nameRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            $0.titleLabel.text = "Имя"
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.keyboardType = .emailAddress
            $0.textField.autocapitalizationType = .none
            $0.textField.returnKeyType = .next
            }.configure { [weak self] in
                $0.placeholder = "Имя"
                $0.text = ""
                $0.rowHeight = 50
                if let userAccount_ = self?.initialUserAccount {
                    $0.text = userAccount_.nameOfPoint
                }
            }.onTextChanged {
                StaffMemoryInstance.sharedInstance.firstName = $0
        }
        
        let secondNameRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            $0.titleLabel.text = "Пароль."
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.returnKeyType = .next
            $0.textField.keyboardType = .emailAddress
            $0.textField.autocapitalizationType = .none
            }.configure { [weak self] in
                $0.placeholder = "Пароль."
                $0.text = ""
                $0.rowHeight = 50
                if let userAccount_ = self?.initialUserAccount {
                    $0.text = userAccount_.password
                }
            }.onTextChanged {
                StaffMemoryInstance.sharedInstance.password = $0
        }
        
        let section = SectionFormer(rowFormer: nameRow, secondNameRow)
        section.set(headerViewFormer: self?.createHeaderFunc(s: " "))
        return section
    }()
    
    private func createHeaderFunc(s: String) -> ViewFormer {
        return LabelViewFormer<FormLabelHeaderView>()
            .configure {
                $0.viewHeight = 30
                $0.text = s
        }
    }
    
    private func configure() {
        
        tableView.contentInset.top = 0
        tableView.contentInset.bottom = 0
        
        former.append(sectionFormer:self.neededProductSection)
            .onCellSelected { [weak self] _ in
                self?.formerInputAccessoryView.update()
        }
    }
}
