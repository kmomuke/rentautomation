//
//  createAccountVC.swift
//  Rentautomation
//
//  Created by kanybek on 12/29/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import SVProgressHUD
import grpcPod
import SwiftSignalKit

class createAccountVC: BaseListVC, ErrorPopoverRenderer {

    var initialItem: String?
    var disposeSet: DisposableSet? = DisposableSet()
    var signUpDisposable: Disposable?
    
    @IBOutlet weak var inputContainerView: UIView!
    var isFirstTime: Bool = true
    
    private lazy var createAccountForm: CreateAccountFormVC = {
        let accountForm_ = CreateAccountFormVC()
        return accountForm_
    }()
    
    convenience init() {
        self.init(initialItem: nil)
    }

    init(initialItem: String?) {
        self.initialItem = initialItem
        super.init(nibName: "createAccountVC", bundle: nil)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.disposeSet?.dispose()
        self.disposeSet = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Создать аккаунт"
        self.inputContainerView.backgroundColor = UIColor.clear
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (isFirstTime) {
            self.add(asChildViewController: self.createAccountForm, frame_: self.inputContainerView.frame)
            isFirstTime = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func createNewAccountDidPressed(_ sender: Any) {
        
        if self.createAccountForm.name.characters.count < 3 {
            let _ = self.showErrorAlert(text: "Введите имя")
            return
        }
        
        if self.createAccountForm.email.characters.count < 3 {
            let _ = self.showErrorAlert(text: "Введите почту")
            return
        }
        
        if self.createAccountForm.password.characters.count < 4 {
            let _ = self.showErrorAlert(text: "Введите пароль")
            return
        }
        
        if self.createAccountForm.confirmPassword.characters.count < 4 {
            let _ = self.showErrorAlert(text: "Введите потверждение пароля")
            return
        }
        
        if self.createAccountForm.confirmPassword != self.createAccountForm.password {
            let _ = self.showErrorAlert(text: "Пароли не совподают")
            return
        }
        
        if self.createAccountForm.adminPassword != "Ali1986" {
            let _ = self.showErrorAlert(text: "Пароль администратора")
            return
        }
        
        let signUpReq = SignUpRequest()
        signUpReq.email = self.createAccountForm.email
        signUpReq.password = self.createAccountForm.password
        signUpReq.firstName = self.createAccountForm.name
        signUpReq.secondName = self.createAccountForm.name
        
        let signUpSignal = RPCNetwork.shared.signUpRPC(sUpReq: signUpReq)
        let finalSignal = signUpSignal |> mapToSignal({ (signUpReq: SignUpRequest) -> Signal<UserRequest, Error> in
            KeychainManager.shared.removeToken()
            let _ = KeychainManager.shared.saveToken(signUpReq.token)
            return RPCNetwork.shared.getUserRPC()
        })
        
        self.showLoading("Creating account")
        let signUpDispose = finalSignal.start(next: {[weak self] (uReq: UserRequest) in
            self?.dismissLoading()
            let user = ProtoToModel.userFrom(uReq)
            user.isCurrentUser = true
            Database.shared.createUser([user])
            AppSession.shared._currentEmployee = user
            
            AppSettings.shared.setDefaultSettings()
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.enterMainApplication()
            
        }, error: {[weak self] (e: Error ) in
            let _ = self?.showErrorAlert(text: e.localizedDescription)
        }) {
        }
        
        self.signUpDisposable = signUpDispose
    }
    
    @IBAction func signInDidPressed(_ sender: Any) {
        let loginVCC = loginVC(initialItem: nil)
        self.navigationController?.setViewControllers([loginVCC], animated: true)
    }
    
    @IBAction func listDidPressed(_ sender: Any) {
        let userAccountsVC = UserAccountsListVC(initalString: "")
        self.navigationController?.pushViewController(userAccountsVC, animated: true)
    }
    
    @IBAction func tryDemoDidPressed(_ sender: Any) {
        let signal = RPCNetwork.shared.signInRPC(email: "test2",
                                                 password: "test2")
        
        let finalUserSignal = signal |> mapToSignal({ (signInReq: SignInRequest) -> Signal<UserRequest, Error> in
            KeychainManager.shared.removeToken()
            let _ = KeychainManager.shared.saveToken(signInReq.token)
            return RPCNetwork.shared.getUserRPC()
        })
        
        let finalSignal = finalUserSignal |> runOn(Queue.mainQueue()) |> deliverOn(Queue.mainQueue())
        
        self.showLoading("Signing in")
        let demoDispose = finalSignal.start(next: { (userReq: UserRequest) in
            self.dismissLoading()
            let user = ProtoToModel.userFrom(userReq)
            user.isCurrentUser = true
            Database.shared.createUser([user])
            AppSession.shared._currentEmployee = user
            
            AppSettings.shared.setDefaultSettings()
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.enterMainApplication()
            
        }, error: { (error: Error) in
            self.dismissLoading()
            let _ = self.showErrorAlert(text: error.localizedDescription)
            print("SignIn finished with error")
        }, completed: {
            self.dismissLoading()
            print("finalSignal completed")
        })
        
        self.disposeSet?.add(demoDispose)
    }
}
