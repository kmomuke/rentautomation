//
//  UserAccountsListVC.swift
//  Rentautomation
//
//  Created by kanybek on 9/28/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit
import grpcPod

class UserAccountsListVC: BaseListVC, ErrorPopoverRenderer, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var userAccounts: [UserAccount] = []
    var disposeSet: DisposableSet?
    
    var initalString: String?
    
    convenience init() {
        self.init(initalString: "")
    }
    
    init(initalString: String) {
        self.initalString = initalString
        self.disposeSet = DisposableSet()
        super.init(nibName: "UserAccountsListVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.disposeSet?.dispose()
        self.disposeSet = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let addButton = UIBarButtonItem(
            title: "Добавить",
            style: .plain,
            target: self,
            action: #selector(createCustomer(_:))
        )

        self.navigationItem.rightBarButtonItems = [addButton]
        
        self.title = "Аккаунты"
        self.tableView.register(CategoriesListTVCell.self)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.reloadAllAccounts()
        
        
        let customerCreateSignal: Signal = PipesStore.shared.userAccCreatedSignal()
        let createCustomerDispose = customerCreateSignal.start(next: {[weak self] (customer: UserAccount) in
            self?.userAccounts.insert(customer, at: 0)
            self?.tableView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
            self?.tableView.beginUpdates()
            self?.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
            self?.tableView.endUpdates()
        })
        self.disposeSet?.add(createCustomerDispose)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func reloadAllAccounts() {
        self.userAccounts = UserAccountsStore.shared.getAllUserAccounts()
        self.tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("viewWillAppear")
    }
    
    func createCustomer(_ sender: AnyObject) {
        let createAccountVC = CreateUserAccountVC()
        self.presentSheet(viewController: createAccountVC)
    }
    
    // MARK: TableView DataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userAccounts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoriesListTVCell", for: indexPath) as! CategoriesListTVCell
        let object = userAccounts[(indexPath as NSIndexPath).row]
        cell.categoryNameLabel.text = object.nameOfPoint
        cell.categoryPrimaryKeyLabel.text = ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let customer = userAccounts[(indexPath as NSIndexPath).row]
        self.tryDemoDidPressed(email: customer.nameOfPoint,
                               password: customer.password)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            let userAccount = userAccounts[(indexPath as NSIndexPath).row]
            UserAccountsStore.shared.delete(userAccount: userAccount)
            self.userAccounts.remove(at: (indexPath as NSIndexPath).row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    private func tryDemoDidPressed(email: String, password: String) {
        let signal = RPCNetwork.shared.signInRPC(email: email,
                                                 password: password)
        let finalUserSignal = signal |> mapToSignal({ (signInReq: SignInRequest) -> Signal<UserRequest, Error> in
            KeychainManager.shared.removeToken()
            let _ = KeychainManager.shared.saveToken(signInReq.token)
            return RPCNetwork.shared.getUserRPC()
        })
        
        let finalSignal = finalUserSignal |> runOn(Queue.mainQueue()) |> deliverOn(Queue.mainQueue())
        
        self.showLoading("Signing in")
        let demoDispose = finalSignal.start(next: { (userReq: UserRequest) in
            self.dismissLoading()
            let user = ProtoToModel.userFrom(userReq)
            user.isCurrentUser = true
            Database.shared.createUser([user])
            AppSession.shared._currentEmployee = user
            
            AppSettings.shared.setDefaultSettings()
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.enterMainApplication()
            
        }, error: { (error: Error) in
            self.dismissLoading()
            let _ = self.showErrorAlert(text: error.localizedDescription)
            print("SignIn finished with error")
        }, completed: {
            self.dismissLoading()
            print("finalSignal completed")
        })
        
        self.disposeSet?.add(demoDispose)
    }
    
}
