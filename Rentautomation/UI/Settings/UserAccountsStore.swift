//
//  UserAccountsStore.swift
//  Rentautomation
//
//  Created by kanybek on 9/28/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

final class UserAccountsStore {
    
    static let shared: UserAccountsStore = UserAccountsStore()
    
    var isGeneratingCSV: Bool
    init () {
        self.isGeneratingCSV = false
    }
    
    func getAllUserAccounts() -> [UserAccount] {

        var accounts: [UserAccount] = []
        if let loadedCart = UserDefaults.standard.array(forKey: "myCart") as? [[String: String]] {
            for item in loadedCart {
                let userAccount = UserAccount(nameOfPoint: item["name"]!,
                                              password: item["password"]!)
                accounts.append(userAccount)
            }
        }
        return accounts
    }
    
    func save(userAccount: UserAccount) {
        let dictAccount: [String: String] = ["name": userAccount.nameOfPoint,
                                            "password": userAccount.password]
        if var loadedCart = UserDefaults.standard.array(forKey: "myCart") as? [[String: String]] {
            loadedCart.append(dictAccount)
            UserDefaults.standard.set(loadedCart, forKey: "myCart")
        } else {
            var cart: [[String: String]] = []
            cart.append(dictAccount)
            UserDefaults.standard.set(cart, forKey: "myCart")
        }
    }
    
    func delete(userAccount: UserAccount) {
        if let loadedCart = UserDefaults.standard.array(forKey: "myCart") as? [[String: String]] {
            var cart: [[String: String]] = []
            for item in loadedCart {
                
                if (userAccount.nameOfPoint == item["name"]! &&
                    userAccount.password == item["password"]!) {
                    // remove
                } else {
                    cart.append(item)
                }
            }
            
            UserDefaults.standard.set(cart, forKey: "myCart")
        }
    }
    
    
    
    
    func some() -> Void {
        var cart: [[String: Any]] = []
        
        let dict1: [String: Any] = ["name": "A",   //dict1
                                    "price": 19.99,
                                    "qty": 1]
        cart.append(dict1)
        
        cart.append(["name": "B", //dict1
                     "price": 4.99,
                     "qty": 2])
        
        UserDefaults.standard.set(cart, forKey: "myCart")
        
        if let loadedCart = UserDefaults.standard.array(forKey: "myCart") as? [[String: Any]] {
            print(loadedCart)  // [[price: 19.99, qty: 1, name: A], [price: 4.99, qty: 2, name: B]]"
            for item in loadedCart {
                print(item["name"]  as! String)    // A, B
                print(item["price"] as! Double)    // 19.99, 4.99
                print(item["qty"]   as! Int)       // 1, 2
            }
        }
    }
}


