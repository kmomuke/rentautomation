//
//  loginVC.swift
//  Rentautomation
//
//  Created by kanybek on 12/29/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit
import grpcPod

class loginVC: BaseListVC, ErrorPopoverRenderer {

    var initialItem: String?
    var createStaffDispose: Disposable?
    @IBOutlet weak var inputContainerView: UIView!
    
    var isFirstTime: Bool = true
    
    private lazy var loginForm: LoginFromVC = {
        let loginForm_ = LoginFromVC()
        return loginForm_
    }()
    
    convenience init() {
        self.init(initialItem: nil)
    }

    init(initialItem: String?) {
        self.initialItem = initialItem
        super.init(nibName: "loginVC", bundle: nil)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.createStaffDispose?.dispose()
        self.createStaffDispose = nil
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        KeychainManager.shared.removeToken()
        self.title = "Войти"
        self.inputContainerView.backgroundColor = UIColor.clear
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (isFirstTime) {
            self.add(asChildViewController: self.loginForm, frame_: self.inputContainerView.frame)
            isFirstTime = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func tryDemoDidPressed(_ sender: Any) {
        let createStaffSessionVC = CreateStaffSessionVC(initialString: nil)
        self.navigationController?.pushViewController(createStaffSessionVC, animated: true)
    }
    
    @IBAction func signInDidPressed(_ sender: Any) -> Void {
        
        self.createStaffDispose?.dispose()
        self.createStaffDispose = nil
        
        let email = loginForm.email.removingWhitespaces()
        let password = loginForm.password.removingWhitespaces()
        
        if email.characters.count < 2 {
            let _ = showErrorAlert(text: "Введите почту")
            return
        }
        
        if password.characters.count < 2 {
            let _ = showErrorAlert(text: "Введите пароль")
            return
        }
        
        
        let signal = RPCNetwork.shared.signInRPC(email: loginForm.email,
                                                 password: loginForm.password)
        
        let finalUserSignal = signal |> mapToSignal({ (signInReq: SignInRequest) -> Signal<UserRequest, Error> in
            KeychainManager.shared.removeToken()
            let _ = KeychainManager.shared.saveToken(signInReq.token)
            return RPCNetwork.shared.getUserRPC()
        })
        
        let finalSignal = finalUserSignal |> runOn(Queue.mainQueue()) |> deliverOn(Queue.mainQueue())
        
        self.showLoading("Signing in")
        self.createStaffDispose = finalSignal.start(next: { (userReq: UserRequest) in
            self.dismissLoading()
            let user = ProtoToModel.userFrom(userReq)
            user.isCurrentUser = true
            Database.shared.createUser([user])
            
            AppSession.shared._currentEmployee = user
            
            AppSettings.shared.setDefaultSettings()
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.enterMainApplication()
            
        }, error: { (error: Error) in
            self.dismissLoading()
            let _ = self.showErrorAlert(text: error.localizedDescription)
            print("SignIn finished with error")
        }, completed: {
            self.dismissLoading()
            print("finalSignal completed")
        })
    }
    
    @IBAction func createAccountPressed(_ sender: Any) {
        let createAccountDidVC = createAccountVC(initialItem: nil)
        self.navigationController?.setViewControllers([createAccountDidVC], animated: true)
    }
}
