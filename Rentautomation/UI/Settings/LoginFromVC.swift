//
//  LoginFromVC.swift
//  Rentautomation
//
//  Created by kanybek on 12/30/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

class LoginFromVC: FormViewController, ErrorPopoverRenderer {
    
    var email: String
    var password: String
    
    var initialItem: String?
    
    convenience init() {
        self.init(initialItem: nil)
    }
    
    init(initialItem: String?) {
        self.initialItem = initialItem
        self.email = ""
        self.password = ""
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: Private
    private lazy var formerInputAccessoryView: FormerInputAccessoryView = FormerInputAccessoryView(former: self.former)
    
    // MARK: Needed product section
    private lazy var neededProductSection: SectionFormer = { [weak self] in
        
        let emailRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            $0.titleLabel.text = "Почта"
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.returnKeyType = .next
            $0.textField.keyboardType = .emailAddress
            $0.textField.autocapitalizationType = .none
            $0.textField.autocorrectionType = .no
            }.configure { [weak self] in
                $0.placeholder = "Почта"
                $0.text = self?.email
                $0.rowHeight = 50
                
            }.onTextChanged { [weak self] in
                self?.email = $0
        }
        
        let passwordRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            $0.titleLabel.text = "Пароль"
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.isSecureTextEntry = true
            $0.textField.returnKeyType = .go
            $0.textField.autocapitalizationType = .none
            }.configure { [weak self] in
                $0.placeholder = "Пароль"
                $0.text = self?.password
                $0.rowHeight = 50
            }.onTextChanged { [weak self] in
                self?.password = $0
        }
        
        let section = SectionFormer(rowFormer: emailRow, passwordRow)
        section.set(headerViewFormer: self?.createHeaderFunc(s: ""))
        return section
    }()
    
    private func createHeaderFunc(s: String) -> ViewFormer {
        return LabelViewFormer<FormLabelHeaderView>()
            .configure {
                $0.viewHeight = 40
                $0.text = s
                $0.view.formTitleLabel().backgroundColor = UIColor.clear
                $0.view.contentView.backgroundColor = UIColor.clear
        }
    }
    
    private func configure() {
        
        //let text: UITextField = UITextField(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        //text.isSecureTextEntry = true
        
        tableView.contentInset.top = 40
        tableView.contentInset.bottom = 40
        tableView.backgroundColor = UIColor.clear
        self.view.backgroundColor = UIColor.clear
        former.append(sectionFormer:self.neededProductSection)
            .onCellSelected { [weak self] _ in
                self?.formerInputAccessoryView.update()
        }
    }
}
