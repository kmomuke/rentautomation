//
//  DeveloperSettingsVC.swift
//  Rentautomation
//
//  Created by kanybek on 4/1/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class DeveloperSettingsVC: FormViewController, ErrorPopoverRenderer, KKPasscodeViewControllerDelegate {

    var initialString: String?
    
    convenience init() {
        self.init(initialString: nil)
    }
    
    init(initialString: String?) {
        self.initialString = initialString
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: Public
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Настройки разработчика"
        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
    }
    
    // MARK: Private
    private func configure() {
        
        //let logo = UIImage(named: "TGUserInfo.png")!
        //navigationItem.titleView = UIImageView(image: logo)
        
        let backBarButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backBarButton
        
        // Create RowFormers
        let createMenu: ((String, (() -> Void)?) -> RowFormer) = { text, onSelected in
            return LabelRowFormer<FormLabelCell>() {
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                }.configure {
                    $0.text = text
                    $0.rowHeight = 70
                }.onSelected { _ in
                    onSelected?()
            }
        }
        
        let customersStaffRow = SwitchRowFormer<FormSwitchCell>() {
            $0.titleLabel.text = "Клиент по сотруднику"
            $0.titleLabel.textColor = .formerColor()
            $0.titleLabel.font = .boldSystemFont(ofSize: 16)
            $0.switchButton.onTintColor = .formerSubColor()
            }.configure {
                $0.switched = AppSettings.shared.getCustomersStaff()
                $0.rowHeight = 70
            }.onSwitchChanged { switched in
                AppSettings.shared.saveCustomersStaff(switched)
        }
        
        let shouldUseDefaultCustomerRow = SwitchRowFormer<FormSwitchCell>() {
            $0.titleLabel.text = "Клиент по умолчанию"
            $0.titleLabel.textColor = .formerColor()
            $0.titleLabel.font = .boldSystemFont(ofSize: 16)
            $0.switchButton.onTintColor = .formerSubColor()
            }.configure {
                $0.switched = AppSettings.shared.shouldUseDefaultCustomer()
                $0.rowHeight = 70
            }.onSwitchChanged { switched in
                AppSettings.shared.saveDefaultCustomer(switched)
        }
        
        let developerModeRow = SwitchRowFormer<FormSwitchCell>() {
            $0.titleLabel.text = "Developer mode"
            $0.titleLabel.textColor = .formerColor()
            $0.titleLabel.font = .boldSystemFont(ofSize: 16)
            $0.switchButton.onTintColor = .formerSubColor()
            }.configure {
                $0.switched = AppSettings.shared.getDeveloperMode()
                $0.rowHeight = 70
            }.onSwitchChanged { switched in
                AppSettings.shared.saveDeveloperMode(switched)
        }
        
        let autoSyncModeRow = SwitchRowFormer<FormSwitchCell>() {
            $0.titleLabel.text = "Автосохранение"
            $0.titleLabel.textColor = .formerColor()
            $0.titleLabel.font = .boldSystemFont(ofSize: 16)
            $0.switchButton.onTintColor = .formerSubColor()
            }.configure {
                $0.switched = AppSettings.shared.getAutoSyncronizeMode()
                $0.rowHeight = 70
            }.onSwitchChanged { switched in
                AppSettings.shared.saveAutoSyncronizeMode(switched)
        }
        
        let autoClearModeRow = SwitchRowFormer<FormSwitchCell>() {
            $0.titleLabel.text = "Авто очистка поле ввода"
            $0.titleLabel.textColor = .formerColor()
            $0.titleLabel.font = .boldSystemFont(ofSize: 16)
            $0.switchButton.onTintColor = .formerSubColor()
            }.configure {
                $0.switched = AppSettings.shared.getAutoClearMode()
                $0.rowHeight = 70
            }.onSwitchChanged { switched in
                AppSettings.shared.saveAutoClearMode(switched)
        }
        
        let isFronCameraRow = SwitchRowFormer<FormSwitchCell>() {
            $0.titleLabel.text = "Фронтальная камера"
            $0.titleLabel.textColor = .formerColor()
            $0.titleLabel.font = .boldSystemFont(ofSize: 16)
            $0.switchButton.onTintColor = .formerSubColor()
            }.configure {
                $0.switched = AppSettings.shared.getShouldUseFrontCamera()
                $0.rowHeight = 70
            }.onSwitchChanged { switched in
                AppSettings.shared.saveShouldUseFrontCamera(switched)
        }
        
        let productUniqModeRow = SwitchRowFormer<FormSwitchCell>() {
            $0.titleLabel.text = "Ввод товара по отдельности"
            $0.titleLabel.textColor = .formerColor()
            $0.titleLabel.font = .boldSystemFont(ofSize: 16)
            $0.switchButton.onTintColor = .formerSubColor()
            }.configure {
                $0.switched = AppSettings.shared.getProductUniqMode()
                $0.rowHeight = 70
            }.onSwitchChanged { switched in
                AppSettings.shared.saveProductUniqMode(switched)
        }
        
        let productAvailabilityModeRow = SwitchRowFormer<FormSwitchCell>() {
            $0.titleLabel.text = "Продажа по доступности товара"
            $0.titleLabel.textColor = .formerColor()
            $0.titleLabel.font = .boldSystemFont(ofSize: 16)
            $0.switchButton.onTintColor = .formerSubColor()
            }.configure {
                $0.switched = AppSettings.shared.getProductAvailability()
                $0.rowHeight = 70
            }.onSwitchChanged { switched in
                AppSettings.shared.saveProductAvailability(switched)
        }
        
        let orderDraftModeRow = SwitchRowFormer<FormSwitchCell>() {
            $0.titleLabel.text = "Включить опцию черновик"
            $0.titleLabel.textColor = .formerColor()
            $0.titleLabel.font = .boldSystemFont(ofSize: 16)
            $0.switchButton.onTintColor = .formerSubColor()
            }.configure {
                $0.switched = AppSettings.shared.getOrderDraftMode()
                $0.rowHeight = 70
            }.onSwitchChanged { switched in
                AppSettings.shared.saveOrderDraftMode(switched)
        }
        
        let passwordOnProductRow = SwitchRowFormer<FormSwitchCell>() {
            $0.titleLabel.text = "Пароль на товар и закупку"
            $0.titleLabel.textColor = .formerColor()
            $0.titleLabel.font = .boldSystemFont(ofSize: 16)
            $0.switchButton.onTintColor = .formerSubColor()
            }.configure {
                $0.switched = AppSettings.shared.getPassordOnProduct()
                $0.rowHeight = 70
            }.onSwitchChanged {[weak self] switched in
                if switched == false {
                    if let strongSelf = self {
                        if KKPasscodeLock.shared().isPasscodeRequired() {
                            let vc = KKPasscodeViewController(nibName: nil, bundle: nil)
                            vc.passcodeMode = "products"
                            vc.mode = UInt(KKPasscodeModeEnter)
                            vc.delegate = strongSelf
                            strongSelf.presentSheet(viewController: vc)
                        }
                    }
                } else {
                    AppSettings.shared.savePasswordOnProduct(switched)
                }
            }.onUpdate {
                $0.switched = AppSettings.shared.getPassordOnProduct()
        }
        
        // Create Headers and Footers
        let createHeader: ((String) -> ViewFormer) = { text in
            return LabelViewFormer<FormLabelHeaderView>()
                .configure {
                    $0.text = text
                    $0.viewHeight = 40
            }
        }
        
        // Create SectionFormers
        let customersStaffSection = SectionFormer(rowFormer: customersStaffRow, shouldUseDefaultCustomerRow, developerModeRow, autoSyncModeRow, autoClearModeRow, productUniqModeRow, productAvailabilityModeRow, orderDraftModeRow, isFronCameraRow,  passwordOnProductRow)
            .set(headerViewFormer: createHeader("Настройка для компании"))
        
        former.append(sectionFormer: customersStaffSection)
    }
    
    //MARK: KKPasscodeViewControllerDelegate methods
    func didPasscodeEnteredCorrectly(_ viewController: KKPasscodeViewController!) {
        AppSettings.shared.savePasswordOnProduct(false)
        for section in former.sectionFormers {
            for row in section.rowFormers {
                row.update()
            }
        }
    }
    
    func didPasscodeEnteredIncorrectly(_ viewController: KKPasscodeViewController!) {
        print("didPasscodeEnteredIncorrectly")
        for section in former.sectionFormers {
            for row in section.rowFormers {
                row.update()
            }
        }
    }
}
