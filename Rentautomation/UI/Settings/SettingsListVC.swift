//
//  SettingsListVC.swift
//  Rentautomation
//
//  Created by kanybek on 11/29/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit
import grpcPod
import Localize_Swift

class SettingsListVC: FormViewController, ErrorPopoverRenderer {

    var initialString: String?
    private let repository: UserDefaultsPasscodeRepository
    private let configuration: PasscodeLockConfigurationType
    var setDispose: DisposableSet? = DisposableSet()
    
    var actionSheet: UIAlertController!
    let availableLanguages = Localize.availableLanguages()
    
    convenience init() {
        self.init(initialString: nil)
    }
    
    init(initialString: String?) {
        self.initialString = initialString
        self.repository = UserDefaultsPasscodeRepository()
        self.configuration = PasscodeLockConfiguration(repository: repository)
        super.init(nibName: nil, bundle: nil)
        let (image1, image2) = Global.shared.settingsIconImage()
        tabBarItem = UITabBarItem(title: "Настройки", image: image1, selectedImage: image2)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("SettingsListVC deallocated")
        self.setDispose?.dispose()
        self.setDispose = nil
    }
    
    // MARK: Public
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Настройки"
        self.addCanceButton()
        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
    }
    
    // MARK: Private
    func addCanceButton() -> Void {
        let cancelButton = UIBarButtonItem(
            title: "Закрыть",
            style: .plain,
            target: self,
            action: #selector(cancelVC(_:))
        )
        self.navigationItem.leftBarButtonItem = cancelButton
    }
    
    private func removeAllDataFromServer() {
        let disposable = RPCNetwork.shared.deleteAllDataFromServersRPC(email: "", password: "").start(next: { (s: SignInRequest) in
            AppSession.shared.endSession()
            let passcodeRepository = UserDefaultsPasscodeRepository()
            passcodeRepository.delete()
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.enterLoginApplication()
        }, error: { (e: Error) in
        }) {
        }
        self.setDispose?.add(disposable)
    }
    
    func cancelVC(_ sender: AnyObject) -> Void {
        let empty = EmptyVC()
        let detailNavigationController = RNNavigationController(rootViewController:empty)
        self.splitViewController?.showDetailViewController(detailNavigationController, sender: self)
    }
    
    private func pushSelectorRowSelected(options: [String]) -> (RowFormer) -> Void {
        return { [weak self] rowFormer in
            if let rowFormer = rowFormer as? LabelRowFormer<FormLabelCell> {
                let controller = TextSelectorViewContoller()
                controller.texts = options
                controller.selectedText = rowFormer.subText
                controller.onSelected = {
                    rowFormer.subText = $0
                    rowFormer.update()
                    
                    if $0 == "$" {
                        print("$")
                        AppSettings.shared.removeDeviceCurrency()
                        AppSettings.shared.saveDeviceCurrency("$")
                        Global.shared.priceFormatter.currencySymbol = "$"
                    } else if $0 == "сом" {
                        print("сом")
                        AppSettings.shared.removeDeviceCurrency()
                        AppSettings.shared.saveDeviceCurrency("сом")
                        Global.shared.priceFormatter.currencySymbol = "сом"
                    } else if $0 == "руб" {
                        print("руб")
                        AppSettings.shared.removeDeviceCurrency()
                        AppSettings.shared.saveDeviceCurrency("руб")
                        Global.shared.priceFormatter.currencySymbol = "руб"
                    }
                    
                }
                self?.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }

    private func pushSelectorRowSelected2(options: [String]) -> (RowFormer) -> Void {
        return { [weak self] rowFormer in
            if let rowFormer = rowFormer as? LabelRowFormer<FormLabelCell> {
                let controller = TextSelectorViewContoller()
                controller.texts = options
                controller.selectedText = rowFormer.subText
                controller.onSelected = {
                    rowFormer.subText = $0
                    rowFormer.update()
                    
                    if $0 == "0" {
                        AppSettings.shared.savemaximumFractionDigits(0)
                        BKCurrencyTextField.setIdGen(0)
                        Global.shared.priceFormatter.maximumFractionDigits = 0
                        Global.shared.amountFormatter.maximumFractionDigits = 0
                    } else if $0 == "1" {
                        AppSettings.shared.savemaximumFractionDigits(1)
                        BKCurrencyTextField.setIdGen(1)
                        Global.shared.priceFormatter.maximumFractionDigits = 1
                        Global.shared.amountFormatter.maximumFractionDigits = 1
                    } else if $0 == "2" {
                        AppSettings.shared.savemaximumFractionDigits(2)
                        BKCurrencyTextField.setIdGen(2)
                        Global.shared.priceFormatter.maximumFractionDigits = 2
                        Global.shared.amountFormatter.maximumFractionDigits = 2
                    } else if $0 == "3" {
                        AppSettings.shared.savemaximumFractionDigits(3)
                        BKCurrencyTextField.setIdGen(3)
                        Global.shared.priceFormatter.maximumFractionDigits = 3
                        Global.shared.amountFormatter.maximumFractionDigits = 3
                    }
                }
                self?.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    // MARK: IBActions
    @IBAction func doChangeLanguage(_ sender: AnyObject) {

        let alert: UIAlertController = UIAlertController(title: "Switch Language",
                                                         message: "",
                                                         preferredStyle: .alert)
        
        for language in availableLanguages {
            let displayName = Localize.displayNameForLanguage(language)
            alert.addAction(UIAlertAction(title: displayName, style: .default, handler: {[weak self]
                (UIAlertAction) -> Void in
                Localize.setCurrentLanguage(language)
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: {[weak self]
            (UIAlertAction) -> Void in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func doResetLanguage(_ sender: AnyObject) {
        Localize.resetCurrentLanguageToDefault()
    }
    
    private func configure() {
        
        //let logo = UIImage(named: "TGUserInfo.png")!
        //navigationItem.titleView = UIImageView(image: logo)
        
        let backBarButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backBarButton
        
        // Create RowFormers
        let createMenu: ((String, (() -> Void)?) -> RowFormer) = { text, onSelected in
            return LabelRowFormer<FormLabelCell>() {
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                }.configure {
                    $0.text = text
                    $0.rowHeight = 70
                }.onSelected { _ in
                    onSelected?()
                }.onUpdate { _ in
                    
            }
        }
        
        let switchDateStyleRow = SwitchRowFormer<FormSwitchCell>() {
            $0.titleLabel.text = "Установить пароль"
            $0.titleLabel.textColor = .formerColor()
            $0.titleLabel.font = .boldSystemFont(ofSize: 16)
            $0.switchButton.onTintColor = .formerSubColor()
            }.configure {
                //$0.switched = false
                let hasPasscode = configuration.repository.hasPasscode
                $0.switched = hasPasscode
                $0.rowHeight = 70
                
            }.onSwitchChanged {[weak self] switched in
                
                if let strongSelf = self {
                    let passcodeVC: PasscodeLockViewController
                    if switched {
                        passcodeVC = PasscodeLockViewController(state: .set, configuration: strongSelf.configuration)
                    } else {
                        passcodeVC = PasscodeLockViewController(state: .remove, configuration: strongSelf.configuration)
                    }
                    
                    passcodeVC.dismissCompletionCallback = {(Void) in
                        print("dismissCompletionCallback")
                        //let hasPasscode = self?.configuration.repository.hasPasscode
                        //switchDateStyleRow.switched = hasPasscode
                    }
                    
                    passcodeVC.successCallback = { (_ lock: PasscodeLockType) in
                        if lock.repository.hasPasscode {
                            print("hasPasscode true")
                        } else {
                            print("hasPasscode false")
                        }
                        print("successCallback")
                    }
                    
                    strongSelf.present(passcodeVC, animated: true, completion: nil)
                }
        }
        let changePasswordRow = createMenu("Поменять пароль") { [weak self] in
            let repo = UserDefaultsPasscodeRepository()
            let config = PasscodeLockConfiguration(repository: repo)
            let passcodeLock = PasscodeLockViewController(state: .change, configuration: config)
            self?.present(passcodeLock, animated: true, completion: nil)
        }
        
        let createSelectorRow = { (
            text: String,
            subText: String,
            onSelected: ((RowFormer) -> Void)?
            ) -> RowFormer in
            return LabelRowFormer<FormLabelCell>() {
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = .boldSystemFont(ofSize: 16)
                $0.subTextLabel.textColor = .formerSubColor()
                $0.subTextLabel.font = .boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                }.configure { form in
                    _ = onSelected.map { form.onSelected($0) }
                    form.text = text
                    form.subText = subText
                    form.rowHeight = 70
            }
        }
        
        let options = ["$", "сом", "руб"]
        let currentCurrency = AppSettings.shared.currentCurrency()
        let currencyRow = createSelectorRow("Валюта", currentCurrency, pushSelectorRowSelected(options: options))
        
        
        let options2 = ["0", "1", "2"]
        let currentCurrency2 = AppSettings.shared.getMaximumFractionDigits()
        let currencyRow2 = createSelectorRow("Точка", "\(currentCurrency2)", pushSelectorRowSelected2(options: options2))
        
        let ipAddress = AppSettings.shared.currentIpAddress()
        let last7 = String(ipAddress.characters.suffix(7))
        
        let customCellRow = createMenu("Настройки разработчика \(last7)") { [weak self] in
            if let strongSelf = self {
                let developerSettingsVC = DeveloperSettingsVC()
                strongSelf.navigationController?.pushViewController(developerSettingsVC, animated: true)
            }
        }
        
        let stopAllProcessRow = createMenu("Остановить синхронизацию") {[weak self] in
            self?.former.deselect(animated: true)
            Synchronizer.shared.stopAllSyncProcess()
        }
        
        let getStaticModelsRow = createMenu("Обновить все данные из сервера") {[weak self] in
            self?.former.deselect(animated: true)
            
            let alert: UIAlertController = UIAlertController(title: "Обновить все",
                                                             message: "Вы действительно хотите обновить все данные из сервера?",
                                                             preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
            
            alert.addAction(UIAlertAction(title: "Обновить", style: .default, handler: {
                (UIAlertAction) -> Void in
                
                Database.shared.countUnSyncedOperations {[weak self] (totalCount: Int32) in
                    if let strongSelf = self {
                        strongSelf.showLoading("Получение статических данных ...")
                        Synchronizer.shared.getInitialStaticModels(completion: {[weak self] (boolValue: Bool) in
                            DispatchQueue.main.async {
                                self?.dismissLoading()
                            }
                        })
                    }
                }
            }))
            
            self?.present(alert, animated: true, completion: nil)
        }
        
        let removeAllDataRow = createMenu("Удалить данные из сервера") {[weak self] in
            self?.former.deselect(animated: true)
            
            let alertController = UIAlertController(title: "УДАЛИТЬ ВСЕ", message: "Вы действительно хотите удалить все", preferredStyle: .alert)
            alertController.addTextField(configurationHandler: {(_ textField: UITextField) -> Void in
                textField.placeholder = "Пароль для удаления"
                textField.isSecureTextEntry = true
            })
            
            let confirmAction = UIAlertAction(title: "Удалить", style: .default, handler: { (acction: UIAlertAction) in
                if let text__ = alertController.textFields?[0].text {
                    if String(describing: text__) == "Kano1986" {
                        self?.removeAllDataFromServer()
                    } else {
                        let _ = self?.showErrorAlert(text: "Не верный пароль")
                    }
                }
            })
            alertController.addAction(confirmAction)
            
            let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
            })
            alertController.addAction(cancelAction)
            self?.present(alertController, animated: true, completion: { _ in })
        }
        
        let imageUploadsRow = createMenu("Сохранить все картинки в удаленный сервер") {[weak self] in
            self?.former.deselect(animated: true)
            AmazonUploader.shared.uploadAndChangeAllProductImages(completion: {() in
                Synchronizer.shared.synchronizeAllNeededOperations(completionHandler: { (f: Bool) in
                })
            })
        }
        
        let checkDataRow = createMenu("Свертка всех данных из базы") {[weak self] in
            self?.former.deselect(animated: true)
            if let strongSelf = self {
                let compareDataVC = CompareDataVC()
                strongSelf.navigationController?.pushViewController(compareDataVC, animated: true)
            }
        }
        
        let languageRow = LabelRowFormer<FormLabelCell>()
            .configure {
                $0.rowHeight = 70
                $0.text = "Company".localized();
                $0.subText = "Settings"
            }.onSelected { [weak self] _ in
                self?.doChangeLanguage(NSObject())
            }.onUpdate {
                $0.text = "Company".localized();
                $0.subText = "Settings"
        }
        
        let companyNameRow = LabelRowFormer<FormLabelCell>()
            .configure {
                $0.rowHeight = 70
                $0.text = "Компания"
                $0.subText = AppSettings.shared.getCompanyName()
            }.onSelected { [weak self] _ in
                self?.former.deselect(animated: true)
                let coVC = CompanyNameVC(initialCompanyName: "")
                self?.navigationController?.pushViewController(coVC, animated: true)
            }.onUpdate {
                $0.text = "Компания"
                $0.subText = AppSettings.shared.getCompanyName()
        }
        
        let passwordOnStatisticsRow = LabelRowFormer<FormLabelCell>()
            .configure {
                $0.rowHeight = 70
                $0.text = "Пароль на отчеты"
                $0.subText = ""
            }.onSelected { [weak self] _ in
                self?.former.deselect(animated: true)
                let coVC = StatisticPasscodeVC(initialItem: "")
                self?.navigationController?.pushViewController(coVC, animated: true)
            }.onUpdate {
                $0.text = "Пароль на отчеты"
                
                if KKPasscodeLock.shared().isPasscodeRequired() {
                    $0.subText = "Установлен"
                } else {
                    $0.subText = "Не установлен"
                }
        }
        
        let releaseVersionRow = LabelRowFormer<FormLabelCell>()
            .configure {
                $0.rowHeight = 70
                $0.text = "Версия"
                if let releaseVersion = Bundle.main.releaseVersionNumber {
                    $0.subText = releaseVersion
                }
            }.onSelected { [weak self] _ in
                self?.former.deselect(animated: true)
            }.onUpdate {
                $0.text = "Версия"
                //$0.subText = "10 000"
        }
        
        let buildVersionNumberRow = LabelRowFormer<FormLabelCell>()
            .configure {
                $0.rowHeight = 70
                $0.text = "Сборка"
                if let buildVersion = Bundle.main.buildVersionNumber {
                    $0.subText = buildVersion
                }
            }.onSelected { [weak self] _ in
                self?.former.deselect(animated: true)
            }.onUpdate {
                $0.text = "Сборка"
                //$0.subText = "10 000"
        }
        
        // Create Headers and Footers
        let createHeader: ((String) -> ViewFormer) = { text in
            return LabelViewFormer<FormLabelHeaderView>()
                .configure {
                    $0.text = text
                    $0.viewHeight = 40
            }
        }
        
        let createFooter: ((String) -> ViewFormer) = { text in
            return LabelViewFormer<FormLabelFooterView>()
                .configure {
                    $0.text = text
                    $0.viewHeight = 100
            }
        }
        
        let checkDataSection = SectionFormer(rowFormer: checkDataRow, imageUploadsRow)
            .set(headerViewFormer: createHeader(""))
        //.set(footerViewFormer: createFooter("Default UI"))
        
        // Create SectionFormers
        let realExampleSection = SectionFormer(rowFormer: switchDateStyleRow, changePasswordRow)
            .set(headerViewFormer: createHeader("Блокировка экрана"))
            //.set(footerViewFormer: createFooter("Редактирование"))
        
        let useCaseSection = SectionFormer(rowFormer:languageRow, companyNameRow, passwordOnStatisticsRow, releaseVersionRow, buildVersionNumberRow, currencyRow, currencyRow2)
            .set(headerViewFormer: createHeader(""))
            //.set(footerViewFormer: createFooter("Use Case"))
        
        let defaultSection = SectionFormer(rowFormer: getStaticModelsRow, stopAllProcessRow, removeAllDataRow)
            .set(headerViewFormer: createHeader(""))
            //.set(footerViewFormer: createFooter("Default UI"))
        
        let developerSection = SectionFormer(rowFormer:customCellRow)
            .set(headerViewFormer: createHeader("Разработчик"))
        
        former.append(sectionFormer: checkDataSection, realExampleSection, useCaseSection, defaultSection, developerSection)
    }

}
