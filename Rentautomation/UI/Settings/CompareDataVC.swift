//
//  CompareDataVC.swift
//  Rentautomation
//
//  Created by kanybek on 3/17/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit


class CompareDataVC: FormViewController, ErrorPopoverRenderer {
    
    var initialItem: String?
    
    // ------
    var totalOrdersCount: UInt64 = 0
    var totalTransactionsCount: UInt64 = 0
    var totalOrderDetailsCount: UInt64 = 0
    var totalProductsQuantityCount: Double = 0.0
    
    var totalProductPricesCount: Double = 0.0
    
    var customersTotalDebt: Double = 0.0
    var suppliersTotalDebt: Double = 0.0
    // ------
    
    convenience init() {
        self.init(initialItem: nil)
    }
    
    init(initialItem: String?) {
        self.initialItem = initialItem
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private enum InsertPosition: Int {
        case iPadClient, server
    }
    private var insertRowPosition: InsertPosition = .iPadClient
    
    // MARK: Public
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let addButton = UIBarButtonItem(
            title: "Данные из базы",
            style: .plain,
            target: self,
            action: #selector(takeAllDataPressed(_:))
        )
        self.navigationItem.rightBarButtonItem = addButton
        
        configure()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.takeAllDataPressed(self.navigationItem.rightBarButtonItem!)
    }
    
    func takeAllDataPressed(_ sender: AnyObject) {
        self.showLoading("Получение данных...")
        
        //completion((UInt64(totalOrdersCount), UInt64(totalOrderDetailsCount), UInt64(transactionsCount),
        //            totalSaleSum, totalProductAmount, totalSupplierDebt, totalCustomerDebt))
        CSVGenerator.shared.compareTotalData {[weak self] (m: (totalOrdersCount_:UInt64, totalOrderDetailsCount_:UInt64, transactionsCount_:UInt64, totalSaleSum_:Double, totalProductAmount_:Double, totalSupplierDebt_:Double, totalCustomerDebt_:Double)) in
            
            if let strongSelf = self {
                strongSelf.dismissLoading()
                
                strongSelf.totalOrdersCount = m.totalOrdersCount_
                strongSelf.totalTransactionsCount = m.transactionsCount_
                strongSelf.totalOrderDetailsCount = m.totalOrderDetailsCount_
                strongSelf.totalProductsQuantityCount = m.totalProductAmount_

                strongSelf.totalProductPricesCount = m.totalSaleSum_
                
                strongSelf.customersTotalDebt = m.totalCustomerDebt_
                strongSelf.suppliersTotalDebt = m.totalSupplierDebt_
                
                strongSelf.updateAllRows()
            }
        }
    }
    
    private func updateAllRows() -> Void {
        for sections in self.former.sectionFormers {
            for row in sections.rowFormers {
                row.update()
            }
        }
    }
    
    private func configure() {
        title = "Свертка данных"
        
        tableView.contentInset.top = 40
        tableView.contentInset.bottom = 40
        tableView.backgroundColor = UIColor.white
        self.view.backgroundColor = UIColor.white
        
        //tableView.contentInset.top = 10
        //tableView.contentInset.bottom = 30
        //tableView.contentOffset.y = -10
        
        let positions = ["     iPad    ", "     Server     "]
        let insertRowPositionRow = SegmentedRowFormer<FormSegmentedCell>(instantiateType: .Class) {[weak self] in
            $0.tintColor = .formerSubColor()
            $0.titleLabel.text = "Данные получены из \(self?.insertRowPosition.rawValue)"
            
            }.configure {
                $0.rowHeight = 60
                $0.segmentTitles = positions
                $0.selectedIndex = insertRowPosition.rawValue
            }.onSegmentSelected { [weak self] index, _ in
                self?.insertRowPosition = InsertPosition(rawValue: index)!
            }
        
        // Create Headers
        let createHeader: (() -> ViewFormer) = {
            return CustomViewFormer<FormHeaderFooterView>()
                .configure {
                    $0.viewHeight = 30
            }
        }
        
        let createCustomButtonRow: ((String, String, (() -> Void)?, ((LabelRowFormer<FormLabelCell>) -> Void)?) -> RowFormer) = { [weak self] text, subText, onSelected, onUpdated in
            return LabelRowFormer<FormLabelCell>() {
                
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .none
                $0.subTextLabel.textColor = .formerSubColor()
                $0.subTextLabel.font = .boldSystemFont(ofSize: 14)
                
                }.configure {
                    $0.text = text
                    $0.rowHeight = 60
                    $0.subText = subText
                }.onSelected { [weak self] _ in
                    onSelected?()
                    self?.former.deselect(animated: true)
                }.onUpdate { [weak self] m in
                    onUpdated?(m)
            }
        }
        
        
        let totalOrdersCountString = Global.shared.amountFormatter.string(for: self.totalOrdersCount)
        let customerSupplierRow = createCustomButtonRow("Количество документов", totalOrdersCountString!, {
            print("Количество документов")
        }) { (subLabel: LabelRowFormer<FormLabelCell>) in
            subLabel.subText = Global.shared.amountFormatter.string(for: self.totalOrdersCount)
        }
        
        let totalTransactionsCountString = Global.shared.amountFormatter.string(for: self.totalTransactionsCount)
        let customSaleRow = createCustomButtonRow("Количество транзакции", totalTransactionsCountString!, {
            print("Количество транзакции")
        }) { (subLabel: LabelRowFormer<FormLabelCell>) in
            subLabel.subText = Global.shared.amountFormatter.string(for: self.totalTransactionsCount)
        }
        
        let totalOrderDetailsCountString = Global.shared.amountFormatter.string(for: self.totalOrderDetailsCount)
        let editSaleRow = createCustomButtonRow("Количество O_Details", totalOrderDetailsCountString!, {
            print("Количество O_Details")
        }) { (subLabel: LabelRowFormer<FormLabelCell>) in
            subLabel.subText = Global.shared.amountFormatter.string(for: self.totalOrderDetailsCount)
        }
        
        let totalProductsCountString = Global.shared.amountFormatter.string(for: self.totalProductsQuantityCount)
        let supplierIncomeRow = createCustomButtonRow("Количество товаров", totalProductsCountString!, {
            print("Количество товаров")
        }) { (subLabel: LabelRowFormer<FormLabelCell>) in
            subLabel.subText = Global.shared.amountFormatter.string(for: self.totalProductsQuantityCount)
        }
        
        let totalProductPricesCountString = Global.shared.priceFormatter.string(for: self.totalProductPricesCount)
        let editSupplierIncomeRow = createCustomButtonRow("Количество суммы цены товаров", totalProductPricesCountString!, {
            print("Количество суммы цены товаров")
        }) { (subLabel: LabelRowFormer<FormLabelCell>) in
            subLabel.subText = Global.shared.priceFormatter.string(for: self.totalProductPricesCount)
        }
        
        let customersTotalDebtString = Global.shared.priceFormatter.string(for: self.customersTotalDebt)
        let incomeRow = createCustomButtonRow("Количество суммы долгов клиента", customersTotalDebtString!, {
            print("Количество суммы долгов клиента")
        }) { (subLabel: LabelRowFormer<FormLabelCell>) in
            subLabel.subText = Global.shared.priceFormatter.string(for: self.customersTotalDebt)
        }
        
        let suppliersTotalDebtString = Global.shared.priceFormatter.string(for: self.suppliersTotalDebt)
        let outgoneRow = createCustomButtonRow("Количество суммы долгов поставщиков", suppliersTotalDebtString!, {
            print("Количество суммы долгов поставщиков")
        }) { (subLabel: LabelRowFormer<FormLabelCell>) in
            subLabel.subText = Global.shared.priceFormatter.string(for: self.suppliersTotalDebt)
        }
        
        let returnSaleRow = createCustomButtonRow("", "", {
            print("returnSaleRow")
        }) { (subLabel: LabelRowFormer<FormLabelCell>) in
            //subLabel.subText = Global.shared.numberFormatter.string(for: self.returnedFromCustomer)
        }
        
        let returnSupplierIncomeRow = createCustomButtonRow("", "", {
            print("returnSupplierIncomeRow")
        }) { (subLabel: LabelRowFormer<FormLabelCell>) in
            //subLabel.subText = Global.shared.numberFormatter.string(for: self.returnedToSupplier)
        }
        
        let titleSection0 = SectionFormer(rowFormer:customerSupplierRow, customSaleRow, editSaleRow, supplierIncomeRow, editSupplierIncomeRow, incomeRow, outgoneRow, returnSaleRow, returnSupplierIncomeRow)
            .set(headerViewFormer: createHeader())
        
        let dateSection = SectionFormer(rowFormer: insertRowPositionRow)
            .set(headerViewFormer: createHeader())
        
        former.append(sectionFormer: titleSection0, dateSection)
    }
}
