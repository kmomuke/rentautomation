//
//  StatisticPasscodeVC.swift
//  Rentautomation
//
//  Created by kanybek on 5/6/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class StatisticPasscodeVC: BaseListVC, ErrorPopoverRenderer, KKPasscodeViewControllerDelegate {
    
    var initialItem: String?
    
    @IBOutlet weak var activateButton: UIButton!
    @IBOutlet weak var testButton: UIButton!
    @IBOutlet weak var changeButton: UIButton!
    @IBOutlet weak var turnOffButton: UIButton!
    
    convenience init() {
        self.init(initialItem: nil)
    }
    
    init(initialItem: String?) {
        self.initialItem = initialItem
        super.init(nibName: "StatisticPasscodeVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.refreshUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func refreshUI() {
        if KKPasscodeLock.shared().isPasscodeRequired() {
            self.activateButton.isEnabled = false
            self.testButton.isEnabled = true
            self.changeButton.isEnabled = true
            self.turnOffButton.isEnabled = true
        } else {
            self.activateButton.isEnabled = true
            self.testButton.isEnabled = false
            self.changeButton.isEnabled = false
            self.turnOffButton.isEnabled = false
        }
    }
    
    @IBAction func enableDidPressed(_ sender: UIButton) {
        let vc = KKPasscodeViewController(nibName: nil, bundle: nil)
        vc.mode = UInt(KKPasscodeModeSet)
        vc.delegate = self
        self.presentSheet(viewController: vc)
    }
    
    @IBAction func testDidPressed(_ sender: UIButton) {
        let vc = KKPasscodeViewController(nibName: nil, bundle: nil)
        vc.mode = UInt(KKPasscodeModeEnter)
        vc.delegate = self
        self.presentSheet(viewController: vc)
    }
    
    @IBAction func changePasswordDidPressed(_ sender: UIButton) {
        let vc = KKPasscodeViewController(nibName: nil, bundle: nil)
        vc.mode = UInt(KKPasscodeModeChange)
        vc.delegate = self
        self.presentSheet(viewController: vc)
    }
    
    @IBAction func turnOffDidPressed(_ sender: UIButton) {
        
        if AppSettings.shared.getDeveloperMode() {
            KKPasscodeLock.shared().resetSettings()
        }
        
        let vc = KKPasscodeViewController(nibName: nil, bundle: nil)
        vc.mode = UInt(KKPasscodeModeDisabled)
        vc.delegate = self
        self.presentSheet(viewController: vc)
    }
    
    //MARK: KKPasscodeViewControllerDelegate methods
    func didPasscodeEnteredCorrectly(_ viewController: KKPasscodeViewController!) {
        print("didPasscodeEnteredCorrectly")
    }
    
    func didPasscodeEnteredIncorrectly(_ viewController: KKPasscodeViewController!) {
        print("didPasscodeEnteredIncorrectly")
    }
    
    func shouldLockApplication(_ viewController: KKPasscodeViewController!) {
        print("shouldLockApplication")
    }
    
    func shouldEraseApplicationData(_ viewController: KKPasscodeViewController!) {
        print("shouldEraseApplicationData")
    }
    
    func didSettingsChanged(_ viewController: KKPasscodeViewController!) {
        print("didSettingsChanged")
        self.refreshUI()
    }
}
