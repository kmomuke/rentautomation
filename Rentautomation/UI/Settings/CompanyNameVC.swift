//
//  CompanyNameVC.swift
//  Rentautomation
//
//  Created by kanybek on 4/25/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
class CompanyNameVC: FormViewController, ErrorPopoverRenderer {
    
    var initialCompanyName: String?
    
    var newPriceSetBlock : ((Double) -> Void)?
    
    convenience init() {
        self.init(initialCompanyName: "")
    }
    
    init(initialCompanyName: String?) {
        self.initialCompanyName = initialCompanyName
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.balanceSumRow.former?.becomeEditingNext()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func cancelObject(_ sender: AnyObject) -> Void {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    // MARK: CREATE
    func saveBalance(_ sender: AnyObject) -> Bool {
        self.navigationController?.dismiss(animated: true, completion: nil)
        return true
    }
    
    // MARK: Private
    private lazy var formerInputAccessoryView: FormerInputAccessoryView = FormerInputAccessoryView(former: self.former)
    
    // MARK: Optional product section
    private lazy var balanceSumRow: TextFieldRowFormer<ProfileFieldCell> = {
        
        let productNameRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            $0.titleLabel.text = "Наимен."
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.returnKeyType = .next
            }.configure {
                $0.placeholder = "Наименование компании"
                $0.text = ""
                $0.rowHeight = 55
                $0.text = AppSettings.shared.getCompanyName()
            }.onTextChanged {
                AppSettings.shared.saveCompanyName($0)
        }
        
        return productNameRow
    }()
    
    private lazy var nameCategorySection: SectionFormer = { [weak self] in
        let section = SectionFormer(rowFormer: (self?.balanceSumRow)!)
        section.set(headerViewFormer: self?.createHeaderFunc(s: (self?.initialCompanyName)!))
        return section
    }()
    
    private func createHeaderFunc(s: String) -> ViewFormer {
        return LabelViewFormer<FormLabelHeaderView>()
            .configure {
                $0.viewHeight = 40
                $0.text = s
        }
    }
    
    private func configure() {
        
        //tableView.contentInset.top = 40
        //tableView.contentInset.bottom = 40
        
        former.append(sectionFormer:self.nameCategorySection)
            .onCellSelected { [weak self] _ in
                self?.formerInputAccessoryView.update()
        }
    }
}
