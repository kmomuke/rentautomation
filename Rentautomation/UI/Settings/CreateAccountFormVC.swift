//
//  CreateAccountFormVC.swift
//  Rentautomation
//
//  Created by kanybek on 12/30/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

class CreateAccountFormVC: FormViewController, ErrorPopoverRenderer {
    
    var name: String
    var email: String
    var password: String
    var confirmPassword: String
    var adminPassword: String
    
    var initialItem: String?
    
    convenience init() {
        self.init(initialItem: nil)
    }
    
    init(initialItem: String?) {
        self.initialItem = initialItem
        
        self.name = ""
        self.email = ""
        self.password = ""
        self.confirmPassword = ""
        self.adminPassword = ""
        
        //self.name = "TazaPos"
        //self.email = "tazapos"
        //self.password = "tazapos1984"
        //self.confirmPassword = "tazapos1984"
        //self.adminPassword = "Ali1986"
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func cancelObject(_ sender: AnyObject) -> Void {
        StaffMemoryInstance.sharedInstance.clearAllVariables()
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    // MARK: CREATE
    func createStaff(_ sender: AnyObject) -> Bool {
        return true
    }
    
    // MARK: Private
    private lazy var formerInputAccessoryView: FormerInputAccessoryView = FormerInputAccessoryView(former: self.former)
    
    // MARK: Needed product section
    private lazy var neededProductSection: SectionFormer = { [weak self] in
        
        let nameRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            $0.titleLabel.text = "Имя"
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.returnKeyType = .next
            }.configure { [weak self] in
                $0.placeholder = "Имя"
                $0.text = self?.name
                $0.rowHeight = 50
            }.onTextChanged { [weak self] in
                self?.name = $0
        }
        
        let emailRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            $0.titleLabel.text = "Почта"
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.returnKeyType = .next
            $0.textField.keyboardType = .emailAddress
            $0.textField.autocapitalizationType = .none
            }.configure { [weak self] in
                $0.placeholder = "Почта"
                $0.text = self?.email
                $0.rowHeight = 50
                
            }.onTextChanged { [weak self] in
                self?.email = $0
        }
        
        let passwordRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            $0.titleLabel.text = "Пароль"
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.isSecureTextEntry = true
            $0.textField.returnKeyType = .go
            $0.textField.autocapitalizationType = .none
            }.configure { [weak self] in
                $0.placeholder = "Пароль"
                $0.text = self?.password
                $0.rowHeight = 50
            }.onTextChanged { [weak self] in
                self?.password = $0
        }
        
        let confirmPasswordRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            $0.titleLabel.text = "Потв пароля"
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.isSecureTextEntry = true
            $0.textField.returnKeyType = .go
            $0.textField.autocapitalizationType = .none
            }.configure { [weak self] in
                $0.placeholder = "Потверждение пароля"
                $0.text = self?.confirmPassword
                $0.rowHeight = 50
            }.onTextChanged { [weak self] in
                self?.confirmPassword = $0
        }
        
        let adminPasswordRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            $0.titleLabel.text = "Админ пароль"
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.isSecureTextEntry = true
            $0.textField.returnKeyType = .go
            $0.textField.autocapitalizationType = .none
            }.configure { [weak self] in
                $0.placeholder = "Пароль администратора"
                $0.text = self?.adminPassword
                $0.rowHeight = 50
            }.onTextChanged { [weak self] in
                self?.adminPassword = $0
        }
        
        let section = SectionFormer(rowFormer: nameRow, emailRow, passwordRow, confirmPasswordRow, adminPasswordRow)
        section.set(headerViewFormer: self?.createHeaderFunc(s: ""))
        return section
    }()
    

    private func createHeaderFunc(s: String) -> ViewFormer {
        return LabelViewFormer<FormLabelHeaderView>()
            .configure {
                $0.viewHeight = 40
                $0.text = s
                $0.view.formTitleLabel().backgroundColor = UIColor.clear
                $0.view.contentView.backgroundColor = UIColor.clear
        }
    }
    
    private func configure() {
        
        tableView.contentInset.top = 40
        tableView.contentInset.bottom = 40
        tableView.backgroundColor = UIColor.clear
        self.view.backgroundColor = UIColor.clear
        former.append(sectionFormer:self.neededProductSection)
            .onCellSelected { [weak self] _ in
                self?.formerInputAccessoryView.update()
        }
    }
}
