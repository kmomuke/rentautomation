//
//  SupplierSearchResultsTVC.swift
//  Rentautomation
//
//  Created by kanybek on 2/3/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class SupplierSearchResultsTVC: UITableViewController {

    var supplierSelectBlock : ((Supplier) -> Void)?
    
    var initalString: String?
    
    convenience init() {
        self.init(initalString: "koke")
    }
    
    init(initalString: String) {
        self.initalString = initalString
        super.init(style: .plain)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    var filteredSuppliers = [Supplier]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "SuppliersListTVCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "SuppliersListTVCell")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredSuppliers.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SuppliersListTVCell", for: indexPath) as! SuppliersListTVCell
        let object = filteredSuppliers[(indexPath as NSIndexPath).row]
        cell.supplier = object
        cell.reloadData()
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let supplier = filteredSuppliers[(indexPath as NSIndexPath).row]
        if let supplierSelectBlock_ = self.supplierSelectBlock {
            supplierSelectBlock_(supplier)
        }
    }
}
