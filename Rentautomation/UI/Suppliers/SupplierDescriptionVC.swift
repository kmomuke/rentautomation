//
//  SupplierDescriptionVC.swift
//  Rentautomation
//
//  Created by kanybek on 12/5/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit

class SupplierDescriptionVC: BaseListVC, ErrorPopoverRenderer {

    @IBOutlet weak var currentSupplierBalanceLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var supplierNameLabel: UILabel!
    @IBOutlet weak var supplierPhoneLabel: UILabel!
    @IBOutlet weak var supplierAddressLabel: UILabel!
    
    var isFirstTime: Bool = true
    var initialSupplier: Supplier?
    var updateSupplierDispose: Disposable?
    var balanceUpdateSupplierDispose: Disposable?
    
    private lazy var moneyOperationsVC: CustomerMoneyOperationsVC = { [weak self] in
        let moneyOperationsVC_ = CustomerMoneyOperationsVC(initialCustomer: nil, initialSupplier: self?.initialSupplier)
        return moneyOperationsVC_
    }()
    
    convenience init() {
        self.init(initialSupplier: nil)
    }
    
    init(initialSupplier: Supplier?) {
        self.initialSupplier = initialSupplier
        super.init(nibName: "SupplierDescriptionVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.updateSupplierDispose?.dispose()
        self.updateSupplierDispose = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let editButton = UIBarButtonItem(
            title: "Редактировать",
            style: .plain,
            target: self,
            action: #selector(editSupplier(_:))
        )
        self.navigationItem.rightBarButtonItem = editButton
        
        if let currentSupplier = self.initialSupplier {
            if let account_ = Database.shared.accountBy(supplierId: currentSupplier.supplierId) {
                self.currentSupplierBalanceLabel.text = Global.shared.priceFormatter.string(for: account_.balance)
            }
        }
        
        let supplierUpdateSignal: Signal = PipesStore.shared.supplierUpdatedSignal()
        self.updateSupplierDispose = supplierUpdateSignal.start(next: {[weak self] (supplier: Supplier) in
            if let currentSupplier = self?.initialSupplier {
                if currentSupplier.supplierId == supplier.supplierId {
                    self?.initialSupplier = supplier
                    self?.updateUI()
                }
            }
        })
        
        let balanceUpdatedSignal: Signal = PipesStore.shared.balanceUpdatedSignal()
        self.balanceUpdateSupplierDispose = balanceUpdatedSignal.start(next: {[weak self] (supplierId: UInt64) in
            
            if let currentSupplier = self?.initialSupplier {
                if currentSupplier.supplierId == supplierId {
                    if let account_ = Database.shared.accountBy(supplierId: supplierId) {
                        self?.currentSupplierBalanceLabel.text = Global.shared.priceFormatter.string(for: account_.balance)
                        self?.moneyOperationsVC.reloadOperations()
                    }
                }
            }
        })
        
        self.updateUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (isFirstTime) {
            self.containerView.backgroundColor = UIColor.white
            self.add(asChildViewController: self.moneyOperationsVC, frame_: self.containerView.frame)
            isFirstTime = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func updateUI() -> Void {
        if let initalSupplier_ = self.initialSupplier {
            self.title = initalSupplier_.companyName
            self.supplierNameLabel.text = "\(initalSupplier_.companyName)"
            self.supplierPhoneLabel.text = initalSupplier_.phoneNumber
            self.supplierAddressLabel.text = initalSupplier_.address
        }
    }
    
    func editSupplier(_ sender: AnyObject) -> Void {
        let editVC = CreateSupplierVC(initialSupplier: self.initialSupplier, isCommingFromProduct: false)
        self.presentSheet(viewController: editVC)
    }
    
    @IBAction func setSupplierBalanceDidPressed(_ sender: Any) {
        if let currentSupplier = self.initialSupplier {
            if let account_ = Database.shared.accountBy(supplierId: currentSupplier.supplierId) {
                
                var debitType: DebitType = .ownerShouldTakeMoneyFromClient
                
                if (account_.balance >= 0.0) {
                    debitType = .ownerShouldTakeMoneyFromClient
                } else {
                    debitType = .ownerShouldGiveMoneyToClient
                }
                
                let setBallanceVC = SetBallanceVC(initialBalance: account_.balance,
                                                  initialDebitType: debitType,
                                                  customerId: 0,
                                                  supplierId: currentSupplier.supplierId)
                self.presentSheet(viewController: setBallanceVC)
            }
        }
    }
}
