//
//  CreateSupplierVC.swift
//  Rentautomation
//
//  Created by kanybek on 11/30/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import Kingfisher
import Photos
import SwiftSignalKit

class CreateSupplierVC: FormViewController, ErrorPopoverRenderer {

    var initialSupplier: Supplier?
    var isCommingFromProduct: Bool = false
    
    convenience init() {
        self.init(initialSupplier: nil, isCommingFromProduct: false)
    }
    
    init(initialSupplier: Supplier?, isCommingFromProduct: Bool) {
        self.initialSupplier = initialSupplier
        self.isCommingFromProduct = isCommingFromProduct
        super.init(nibName: nil, bundle: nil)
        self.setUpInitialRows()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    deinit {
    }
    
    private func setUpInitialRows () -> Void {
        if let supplier_ = self.initialSupplier {
            SupplierMemoryInstance.sharedInstance.companyName = supplier_.companyName
            SupplierMemoryInstance.sharedInstance.phoneNumber = supplier_.phoneNumber
            SupplierMemoryInstance.sharedInstance.address = supplier_.address
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure()
        
        self.title = "Создать/редактировать поставщика"
        
        
        let canceButton = UIBarButtonItem(
            title: "Закрыть",
            style: .plain,
            target: self,
            action: #selector(cancelObject(_:))
        )
        
        self.navigationItem.leftBarButtonItem = canceButton
        
        let createButton = UIBarButtonItem(
            title: "Сохранить",
            style: .plain,
            target: self,
            action: #selector(createSupplier(_:))
        )
        
        self.navigationItem.rightBarButtonItem = createButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.neededProductSection.firstRowFormer?.former?.becomeEditingNext()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func cancelObject(_ sender: AnyObject) -> Void {
        SupplierMemoryInstance.sharedInstance.clearAllVariables()
        if self.isCommingFromProduct {
            let _ = self.navigationController?.popViewController(animated: true)
        } else {
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK: CREATE
    func createSupplier(_ sender: AnyObject) -> Bool {
        
        let creatingSupplier: Supplier
        var isUpdatingSupplier: Bool = false
        if let supplier_ = self.initialSupplier {
            creatingSupplier = supplier_
            isUpdatingSupplier = true
        } else {
            creatingSupplier = Supplier()
            creatingSupplier.supplierId = Database.shared.uniqueIncreasingPrimaryKey()
        }
        
        if let companyName_ = SupplierMemoryInstance.sharedInstance.companyName {
            creatingSupplier.companyName = companyName_
            if companyName_.characters.count < 2 {
                return showErrorAlert(text: "Наименования")
            }
        } else {
            return showErrorAlert(text: "Наименования")
        }
        
        if let address_ = SupplierMemoryInstance.sharedInstance.address {
            creatingSupplier.address = address_
        }
        
        if let phoneNumber_ = SupplierMemoryInstance.sharedInstance.phoneNumber {
            creatingSupplier.phoneNumber = phoneNumber_
            if phoneNumber_.characters.count < 2 {
                return showErrorAlert(text: "Тел.")
            }
        } else {
            return showErrorAlert(text: "Тел.")
        }
        
        if !isUpdatingSupplier {
            
            let account = Account()
            account.accountId = Database.shared.uniqueIncreasingPrimaryKey()
            account.supplierId = creatingSupplier.supplierId
            account.customerId = 0
            account.balance = 0
            Database.shared.createAccounts([account])
            
            // ----------- Transaction test mode create for Supplier ------------------
            if let employee_ = AppSession.shared.currentEmployee {
                
                let transactionId = Database.shared.uniqueIncreasingPrimaryKey()
                let transaction = Transaction(transactionId: transactionId,
                                              transactionDate: Date(),
                                              isLastTransaction: false,
                                              transactionType: .supplierBalanceModified,
                                              moneyAmount: 0.0,
                                              orderId: 0,
                                              customerId: 0,
                                              supplierId: creatingSupplier.supplierId,
                                              userId: employee_.userId,
                                              ballanceAmount: 0,
                                              comment: "",
                                              transactionUuid: UUID().uuidString.lowercased())
                
                Database.shared.createTransactions([transaction])
                
                let operation = Event(syncOperationId: Database.shared.operationCid(),
                                              isSynchronized: false,
                                              syncOperationDate: Date(),
                                              operationType: .supplierCreated,
                                              productId: 0,
                                              orderDetailId: 0,
                                              customerId: 0,
                                              transactionId: transaction.transactionId,
                                              accountId: account.accountId,
                                              supplierId: creatingSupplier.supplierId,
                                              staffId: 0,
                                              categoryId: 0,
                                              orderId: 0)
                
                Database.shared.createOperations([operation])
                
            }
            
        }
        
        if isUpdatingSupplier {
            
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                          isSynchronized: false,
                                          syncOperationDate: Date(),
                                          operationType: .supplierUpdated,
                                          productId: 0,
                                          orderDetailId: 0,
                                          customerId: 0,
                                          transactionId: 0,
                                          accountId: 0,
                                          supplierId: creatingSupplier.supplierId,
                                          staffId: 0,
                                          categoryId: 0,
                                          orderId: 0)
            
            Database.shared.createOperations([operation])
            
            Database.shared.updateSupplier(creatingSupplier)
            PipesStore.shared.supplierUpdatedPipe.putNext(creatingSupplier)
        } else {
            Database.shared.createSuppliers([creatingSupplier])
            PipesStore.shared.supplierCreatedPipe.putNext(creatingSupplier)
        }
        
        SupplierMemoryInstance.sharedInstance.clearAllVariables()
        if self.isCommingFromProduct {
            let _ = self.navigationController?.popViewController(animated: true)
        } else {
            self.navigationController?.dismiss(animated: true, completion: nil)
        }

        return true
    }
        
    // MARK: Private
    private lazy var formerInputAccessoryView: FormerInputAccessoryView = FormerInputAccessoryView(former: self.former)
    
    // MARK: Needed product section
    private lazy var neededProductSection: SectionFormer = { [weak self] in
        
        let companyNameRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            $0.titleLabel.text = "Комп"
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.returnKeyType = .next
            $0.textField.autocapitalizationType = .words
            }.configure { [weak self] in
                $0.placeholder = "Компания"
                $0.text = ""
                $0.rowHeight = 50
                if let supplier_ = self?.initialSupplier {
                    $0.text = supplier_.companyName
                }
            }.onTextChanged {
                SupplierMemoryInstance.sharedInstance.companyName = $0
        }
        
        let phoneRow = TextFieldRowFormer<PhoneFiledTVCell>() { [weak self] in
            $0.titleLabel.text = "Тел"
            $0.textField.keyboardType = .numberPad
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.returnKeyType = .next
            }.configure { [weak self] in
                if let supplier_ = self?.initialSupplier {
                    $0.text = supplier_.phoneNumber
                }
            }.onTextChanged {
                SupplierMemoryInstance.sharedInstance.phoneNumber = $0
        }

        let section = SectionFormer(rowFormer: companyNameRow, phoneRow)
        section.set(headerViewFormer: self?.createHeaderFunc(s: ""))
        return section
    }()

    private lazy var addressInfoSection: SectionFormer = { [weak self] in
        
        let addressRow = TextViewRowFormer<FormTextViewCell>() { [weak self] in
            $0.textView.textColor = .formerSubColor()
            $0.textView.font = .systemFont(ofSize: 20)
            $0.textView.inputAccessoryView = self?.formerInputAccessoryView
            }.configure { [weak self] in
                $0.placeholder = "Адрес"
                if let supplier_ = self?.initialSupplier {
                    $0.text = supplier_.address
                }
            }.onTextChanged {
                SupplierMemoryInstance.sharedInstance.address = $0
        }
        
        let section = SectionFormer(rowFormer: addressRow)
        section.set(headerViewFormer: self?.createHeaderFunc(s: "Адрес"))
        return section
    }()
    
    
    private func createHeaderFunc(s: String) -> ViewFormer {
        return LabelViewFormer<FormLabelHeaderView>()
            .configure {
                $0.viewHeight = 30
                $0.text = s
        }
    }
    
    private func configure() {
        
        tableView.contentInset.top = 0
        tableView.contentInset.bottom = 0
        
        // Create Headers
        /*let createHeader: ((String) -> ViewFormer) = { text in
         return LabelViewFormer<FormLabelHeaderView>()
         .configure {
         $0.viewHeight = 40
         $0.text = text
         }
         }*/
        
        former.append(sectionFormer:self.neededProductSection, self.addressInfoSection)
            .onCellSelected { [weak self] _ in
                self?.formerInputAccessoryView.update()
        }
    }
}
