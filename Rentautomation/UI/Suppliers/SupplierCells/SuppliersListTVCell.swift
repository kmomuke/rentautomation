//
//  SuppliersListTVCell.swift
//  Rentautomation
//
//  Created by kanybek on 12/3/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

class SuppliersListTVCell: UITableViewCell {

    var supplier: Supplier?
    
    @IBOutlet weak var supplierPrimaryKeyLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var adressLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var accountBalanceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.cellImageView.layer.cornerRadius = 37.5;
        self.cellImageView.layer.masksToBounds = true;
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func reloadData() -> Void {
        self.cellImageView.image = Global.shared.supplierPlaceholderImage
        if let supplier_ = self.supplier {
            
            if Global.shared.isDebug {
                self.supplierPrimaryKeyLabel.text = "\(supplier_.supplierId)"
                self.supplierPrimaryKeyLabel.backgroundColor = UIColor.clear
            } else {
                self.supplierPrimaryKeyLabel.isHidden = true
            }
            
            self.nameLabel.text = "\(supplier_.companyName)"
            self.adressLabel.text = "\(supplier_.address)"
            self.phoneLabel.text = "\(supplier_.phoneNumber)"
            var balanceString = "0.0"
            if let account_ = Database.shared.accountBy(supplierId: supplier_.supplierId) {
                if let blStr = Global.shared.priceFormatter.string(for: account_.balance) {
                    balanceString = blStr
                }
            }
            self.accountBalanceLabel.text = balanceString
            
            if Database.shared.isModelSynchronized(primaryKey: supplier_.supplierId) {
                self.selectionStyle = .default
                self.nameLabel.textColor = UIColor.black
            } else {
                self.selectionStyle = .none
                self.nameLabel.textColor = UIColor.pomergranateColor()
            }
            
        } else {
            self.resetLabels()
        }
    }
    
    override func prepareForReuse() {
        self.supplier = nil
        self.resetLabels()
    }
    
    func resetLabels() -> Void {
        self.nameLabel.text = ""
        self.phoneLabel.text = ""
        self.adressLabel.text = ""
        self.cellImageView.image = nil
        self.supplierPrimaryKeyLabel.text = ""
    }
}
