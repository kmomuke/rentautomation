//
//  StaffListTVCell.swift
//  Rentautomation
//
//  Created by kanybek on 12/5/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

class StaffListTVCell: UITableViewCell {

    var staff: User?
    
    @IBOutlet weak var staffPrimaryKeyLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    //@IBOutlet weak var adressLabel: UILabel!
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var addressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.cellImageView.layer.cornerRadius = 37.5;
        self.cellImageView.layer.masksToBounds = true;
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func reloadData() -> Void {
        self.cellImageView.image = Global.shared.staffPlaceholderImage
        if let staff_ = self.staff {
            
            if Global.shared.isDebug {
                self.staffPrimaryKeyLabel.text = "\(staff_.userId)"
                self.staffPrimaryKeyLabel.backgroundColor = UIColor.clear
            } else {
                self.staffPrimaryKeyLabel.isHidden = true
            }
            
            self.phoneLabel.text = "\(staff_.phoneNumber)"
            if staff_.isCurrentUser {
                self.nameLabel.textColor = UIColor.pumpkinColor()
                self.nameLabel.text = "\(staff_.firstName)  \(staff_.secondName), текущий владелец планшета"
            } else {
                self.nameLabel.textColor = UIColor.midnightBlueColor()
                self.nameLabel.text = "\(staff_.firstName)  \(staff_.secondName)"
            }
            
            self.addressLabel.text = staff_.address
            if Database.shared.isModelSynchronized(primaryKey: staff_.userId) {
                self.selectionStyle = .default
                if staff_.isCurrentUser {
                    self.nameLabel.textColor = UIColor.pumpkinColor()
                } else {
                    self.nameLabel.textColor = UIColor.midnightBlueColor()
                }
            } else {
                self.selectionStyle = .none
                self.nameLabel.textColor = UIColor.pomergranateColor()
            }
            
        } else {
            self.resetLabels()
        }
    }
    
    override func prepareForReuse() {
        self.staff = nil
        self.resetLabels()
    }
    
    func resetLabels() -> Void {
        self.nameLabel.text = ""
        self.phoneLabel.text = ""
        //self.adressLabel.text = ""
        self.cellImageView.image = nil
        self.staffPrimaryKeyLabel.text = ""
    }
    
}
