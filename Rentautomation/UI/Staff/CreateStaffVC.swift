//
//  CreateStaffVC.swift
//  Rentautomation
//
//  Created by kanybek on 12/5/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit
import grpcPod

class CreateStaffVC: FormViewController, ErrorPopoverRenderer {

    var initialStaff: User?
    var disposeSet: DisposableSet = DisposableSet()
    
    convenience init() {
        self.init(initialStaff: nil)
    }
    
    init(initialStaff: User?) {
        self.initialStaff = initialStaff
        super.init(nibName: nil, bundle: nil)
        self.setUpInitialRows()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    private func setUpInitialRows () -> Void {
        if let staff_ = self.initialStaff {
            
            StaffMemoryInstance.sharedInstance.roleId = staff_.roleId
            StaffMemoryInstance.sharedInstance.firstName = staff_.firstName
            StaffMemoryInstance.sharedInstance.secondName = staff_.secondName
            StaffMemoryInstance.sharedInstance.phoneNumber = staff_.phoneNumber
            StaffMemoryInstance.sharedInstance.address = staff_.address
            StaffMemoryInstance.sharedInstance.email = staff_.email
            StaffMemoryInstance.sharedInstance.password = staff_.password
            StaffMemoryInstance.sharedInstance.confirmPassword = staff_.password
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure()
        
        self.title = "Создать/редактировать сотрудника"
        
        let canceButton = UIBarButtonItem(
            title: "Закрыть",
            style: .plain,
            target: self,
            action: #selector(cancelObject(_:))
        )
        
        self.navigationItem.leftBarButtonItem = canceButton
        
        let createButton = UIBarButtonItem(
            title: "Сохранить",
            style: .plain,
            target: self,
            action: #selector(createStaff(_:))
        )
        
        self.navigationItem.rightBarButtonItem = createButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.neededProductSection.firstRowFormer?.former?.becomeEditingNext()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func cancelObject(_ sender: AnyObject) -> Void {
        StaffMemoryInstance.sharedInstance.clearAllVariables()
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    // MARK: CREATE
    func createStaff(_ sender: AnyObject) -> Bool {
        
        let creatingStaff: User
        var isUpdatingStaff: Bool = false
        if let staff_ = self.initialStaff {
            creatingStaff = staff_
            isUpdatingStaff = true
        } else {
            creatingStaff = User()
            creatingStaff.userId = Database.shared.uniqueIncreasingPrimaryKey()
        }
        
        if let firstName_ = StaffMemoryInstance.sharedInstance.firstName {
            creatingStaff.firstName = firstName_
            if firstName_.characters.count < 2 {
                return showErrorAlert(text: "Имя")
            }
        } else {
            return showErrorAlert(text: "Имя")
        }
        
        if let secondName_ = StaffMemoryInstance.sharedInstance.secondName {
            creatingStaff.secondName = secondName_
            if secondName_.characters.count < 2 {
                return showErrorAlert(text: "Фамилия")
            }
        } else {
            return showErrorAlert(text: "Фамилия")
        }
        
        if let phoneNumber_ = StaffMemoryInstance.sharedInstance.phoneNumber {
            creatingStaff.phoneNumber = phoneNumber_
            if phoneNumber_.characters.count < 2 {
                return showErrorAlert(text: "Тел.")
            }
        } else {
            return showErrorAlert(text: "Тел.")
        }
        
        if let address_ = StaffMemoryInstance.sharedInstance.address {
            creatingStaff.address = address_
            if address_.characters.count < 2 {
                return showErrorAlert(text: "Адрес")
            }
        } else {
            return showErrorAlert(text: "Адрес")
        }
        
        if StaffMemoryInstance.sharedInstance.roleId > 0 {
            creatingStaff.roleId = StaffMemoryInstance.sharedInstance.roleId
        } else {
            return showErrorAlert(text: "Роль")
        }
        
        
        //------------
        if let email_ = StaffMemoryInstance.sharedInstance.email {
            creatingStaff.email = email_
            
            if email_.characters.count < 2 {
                return showErrorAlert(text: "Введите почту")
            }
            
            if !isUpdatingStaff {
                if let _ = Database.shared.userBy(email: email_) {
                    return showErrorAlert(text: "Почта должна быть уникальна, такая почта уже существует!")
                }
            }
            
        } else {
            return showErrorAlert(text: "Введите почту")
        }
        
        if let password_ = StaffMemoryInstance.sharedInstance.password {
            creatingStaff.password = password_
            if password_.characters.count < 2 {
                return showErrorAlert(text: "Введите пароль")
            }
        } else {
            return showErrorAlert(text: "Введите пароль")
        }
        
        if let confirmPassword_ = StaffMemoryInstance.sharedInstance.confirmPassword {
            creatingStaff.password = confirmPassword_
            if confirmPassword_.characters.count < 2 {
                return showErrorAlert(text: "Введите потверждение пароля")
            }
        } else {
            return showErrorAlert(text: "Введите потверждение пароля")
        }
        
        if let password_ = StaffMemoryInstance.sharedInstance.password, let confirmPassword_ = StaffMemoryInstance.sharedInstance.confirmPassword {
            if password_ != confirmPassword_ {
                return showErrorAlert(text: "Пароли не совподают")
            }
        }
        
        if isUpdatingStaff {
            
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                          isSynchronized: false,
                                          syncOperationDate: Date(),
                                          operationType: .staffUpdated,
                                          productId: 0,
                                          orderDetailId: 0,
                                          customerId: 0,
                                          transactionId: 0,
                                          accountId: 0,
                                          supplierId: 0,
                                          staffId: creatingStaff.userId,
                                          categoryId: 0,
                                          orderId: 0)
            
            Database.shared.createOperations([operation])
            
            Database.shared.updateUser(creatingStaff)
            PipesStore.shared.staffUpdatedPipe.putNext(creatingStaff)
            
            StaffMemoryInstance.sharedInstance.clearAllVariables()
            self.navigationController?.dismiss(animated: true, completion: nil)
            
        } else {
            
            let dispose = RPCNetwork.shared.emailExsistsRPC(email: creatingStaff.email).start(next: {[weak self] (f: SignInRequest) in
                
                print("f.email === >\(f.email)")
                
                if f.email == "empty" {
                    let operation = Event(syncOperationId: Database.shared.operationCid(),
                                                  isSynchronized: false,
                                                  syncOperationDate: Date(),
                                                  operationType: .staffCreated,
                                                  productId: 0,
                                                  orderDetailId: 0,
                                                  customerId: 0,
                                                  transactionId: 0,
                                                  accountId: 0,
                                                  supplierId: 0,
                                                  staffId: creatingStaff.userId,
                                                  categoryId: 0,
                                                  orderId: 0)
                    
                    Database.shared.createOperations([operation])
                    
                    Database.shared.createUser([creatingStaff])
                    PipesStore.shared.staffCreatedPipe.putNext(creatingStaff)
                    
                    StaffMemoryInstance.sharedInstance.clearAllVariables()
                    self?.navigationController?.dismiss(animated: true, completion: nil)
                } else {
                    let _ = self?.showErrorAlert(text: "Почта должна быть уникальна, такая почта уже существует!")
                }
                
            }, error: { (e: Error) in
            }, completed: {
            })
            
            disposeSet.add(dispose)
        }
        
        return true
    }
    
    // MARK: Private
    private lazy var formerInputAccessoryView: FormerInputAccessoryView = FormerInputAccessoryView(former: self.former)
    
    // MARK: Needed product section
    private lazy var neededProductSection: SectionFormer = {
        
        let nameRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            $0.titleLabel.text = "Имя"
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.returnKeyType = .next
            }.configure {
                $0.placeholder = "Имя"
                $0.text = ""
                $0.rowHeight = 50
                if let staff_ = self.initialStaff {
                    $0.text = staff_.firstName
                }
            }.onTextChanged {
                StaffMemoryInstance.sharedInstance.firstName = $0
        }
        
        let secondNameRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            $0.titleLabel.text = "Фамил."
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.returnKeyType = .next
            }.configure {
                $0.placeholder = "Фамил."
                $0.text = ""
                $0.rowHeight = 50
                if let staff_ = self.initialStaff {
                    $0.text = staff_.secondName
                }
            }.onTextChanged {
                StaffMemoryInstance.sharedInstance.secondName = $0
        }
        
        let phoneRow = TextFieldRowFormer<PhoneFiledTVCell>() { [weak self] in
            $0.titleLabel.text = "Тел"
            $0.textField.keyboardType = .numberPad
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.returnKeyType = .next
            }.configure {
                if let staff_ = self.initialStaff {
                    $0.text = staff_.phoneNumber
                }
            }.onTextChanged {
                StaffMemoryInstance.sharedInstance.phoneNumber = $0
        }
        
        let emailRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            $0.titleLabel.text = "Почта"
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.returnKeyType = .next
            $0.textField.autocapitalizationType = .none
            $0.textField.keyboardType = .emailAddress
            }.configure { [weak self] in
                $0.placeholder = "Почта"
                $0.rowHeight = 50
                
                if let staff_ = self?.initialStaff {
                    $0.text = staff_.email
                }
                
            }.onTextChanged { [weak self] in
                StaffMemoryInstance.sharedInstance.email = $0
        }
        
        if let staff_ = self.initialStaff {
            emailRow.enabled = false
        }
        
        let passwordRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            $0.titleLabel.text = "Пароль"
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.isSecureTextEntry = true
            $0.textField.autocapitalizationType = .none
            $0.textField.returnKeyType = .next
            }.configure { [weak self] in
                $0.placeholder = "Пароль"
                $0.rowHeight = 50
                if let staff_ = self?.initialStaff {
                    $0.text = staff_.password
                }
                
            }.onTextChanged { [weak self] in
                StaffMemoryInstance.sharedInstance.password = $0
        }
        
        let confirmPasswordRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            $0.titleLabel.text = "Потв пароля"
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.isSecureTextEntry = true
            $0.textField.autocapitalizationType = .none
            $0.textField.returnKeyType = .next
            }.configure { [weak self] in
                $0.placeholder = "Потверждение пароля"
                $0.rowHeight = 50
                if let staff_ = self?.initialStaff {
                    $0.text = staff_.password
                }
                
            }.onTextChanged { [weak self] in
                StaffMemoryInstance.sharedInstance.confirmPassword = $0
        }
        
        let section = SectionFormer(rowFormer: nameRow, secondNameRow, phoneRow, emailRow, passwordRow, confirmPasswordRow)
        //section.set(headerViewFormer: self.createHeaderFunc(s: "Main needed rows"))
        return section
    }()
    
    private lazy var addressInfoSection: SectionFormer = { [weak self] in
        
        let addressRow = TextViewRowFormer<FormTextViewCell>() { [weak self] in
            $0.textView.textColor = .formerSubColor()
            $0.textView.font = .systemFont(ofSize: 20)
            $0.textView.inputAccessoryView = self?.formerInputAccessoryView
            }.configure { [weak self] in
                $0.placeholder = "Адрес"
                if let staff_ = self?.initialStaff {
                    $0.text = staff_.address
                }
            }.onTextChanged {
                StaffMemoryInstance.sharedInstance.address = $0
        }
        
        let section = SectionFormer(rowFormer: addressRow)
        section.set(headerViewFormer: self?.createHeaderFunc(s: "Адрес"))
        return section
    }()
    
    private func createHeaderFunc(s: String) -> ViewFormer {
        return LabelViewFormer<FormLabelHeaderView>()
            .configure {
                $0.viewHeight = 40
                $0.text = s
        }
    }
    
    func someFunc() -> SectionFormer {
        
        let createSelectorRow = { (
            text: String,
            subText: String,
            onSelected: ((RowFormer) -> Void)?
            ) -> RowFormer in
            return LabelRowFormer<FormLabelCell>() {
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
                $0.subTextLabel.textColor = .formerSubColor()
                $0.subTextLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                }.configure { form in
                    _ = onSelected.map { form.onSelected($0) }
                    form.text = text
                    form.subText = subText
            }
        }
        
        let roleOptions = ["Владелец", "Администратор", "Агент"]
        
        var roleName: String = "Не выбрано"
        if let staff_ = self.initialStaff {
            
            if staff_.roleId == 1000 {
                roleName = "Владелец"
            } else if staff_.roleId == 2000 {
                roleName = "Администратор"
            } else if staff_.roleId == 3000 {
                roleName = "Агент"
            }
        }
        
        let roleSelectorRow = createSelectorRow("Роль", roleName, pushSelectorRowSelected(options: roleOptions))
        
        let section = SectionFormer(rowFormer: roleSelectorRow)
        //section.set(headerViewFormer: self.createHeaderFunc(s: "Роль"))
        return section
    }
    
    private func pushSelectorRowSelected(options: [String]) -> (RowFormer) -> Void {
        return { [weak self] rowFormer in
            if let rowFormer = rowFormer as? LabelRowFormer<FormLabelCell> {
                let controller = TextSelectorViewContoller()
                controller.texts = options
                controller.selectedText = rowFormer.subText
                controller.onSelected = {
                    
                    rowFormer.subText = $0
                    rowFormer.update()
                    
                    if $0 == "Владелец" {
                        print("Owner")
                        StaffMemoryInstance.sharedInstance.roleId = 1000
                    } else if $0 == "Администратор" {
                        print("Admin")
                        StaffMemoryInstance.sharedInstance.roleId = 2000
                    } else if $0 == "Агент" {
                        print("Agent")
                        StaffMemoryInstance.sharedInstance.roleId = 3000
                    }
                }
                self?.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    private func configure() {
        
        tableView.contentInset.top = 0
        tableView.contentInset.bottom = 0
        
        former.append(sectionFormer:self.neededProductSection, self.someFunc(), self.addressInfoSection)
            .onCellSelected { [weak self] _ in
                self?.formerInputAccessoryView.update()
        }
    }
}
