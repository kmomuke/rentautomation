//
//  StaffDescriptionVC.swift
//  Rentautomation
//
//  Created by kanybek on 12/5/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit

class StaffDescriptionVC: BaseListVC, ErrorPopoverRenderer {

    var initialStaff: User?
    var updateStaffDispose: Disposable?
    
    convenience init() {
        self.init(initialStaff: nil)
    }
    
    init(initialStaff: User?) {
        self.initialStaff = initialStaff
        super.init(nibName: "StaffDescriptionVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.updateStaffDispose?.dispose()
        self.updateStaffDispose = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let editButton = UIBarButtonItem(
            title: "Редактировать",
            style: .plain,
            target: self,
            action: #selector(editStaff(_:))
        )
        self.navigationItem.rightBarButtonItem = editButton
        
        self.updateUI()
        
        let staffUpdateSignal: Signal = PipesStore.shared.staffUpdatedSignal()
        self.updateStaffDispose = staffUpdateSignal.start(next: {[weak self] (employee: User) in
            if let currentStaff = self?.initialStaff {
                if currentStaff.userId == employee.userId {
                    self?.initialStaff = employee
                    self?.updateUI()
                }
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func updateUI() -> Void {
        if let staff_ = self.initialStaff {
            self.title = staff_.firstName + " " + staff_.secondName
        }
    }
    
    func editStaff(_ sender: AnyObject) -> Void {
        let editVC = CreateStaffVC(initialStaff:self.initialStaff)
        self.presentSheet(viewController: editVC)
    }
}
