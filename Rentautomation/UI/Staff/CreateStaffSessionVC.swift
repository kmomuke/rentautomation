//
//  CreateStaffSessionVC.swift
//  Rentautomation
//
//  Created by kanybek on 1/9/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit

class CreateStaffSessionVC: FormViewController, ErrorPopoverRenderer {
    
    var initialString: String?
    var createStaffDispose: Disposable?
    var generateSignInQue: Queue
    
    convenience init() {
        self.init(initialString: nil)
    }
    
    init(initialString: String?) {
        self.initialString = initialString
        self.generateSignInQue = Queue(name: "generateSignInQue.queueue")
        StaffMemoryInstance.sharedInstance.clearAllVariables()
        StaffMemoryInstance.sharedInstance.roleId = 1000
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.createStaffDispose?.dispose()
        self.createStaffDispose = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure()
        
        self.title = "Создать новый аккаунт"
        
        let canceButton = UIBarButtonItem(
            title: "Назад",
            style: .plain,
            target: self,
            action: #selector(cancelObject(_:))
        )
        
        self.navigationItem.leftBarButtonItem = canceButton
        
        let createButton = UIBarButtonItem(
            title: "Создать",
            style: .plain,
            target: self,
            action: #selector(createStaff(_:))
        )
        
        self.navigationItem.rightBarButtonItem = createButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.neededProductSection.firstRowFormer?.former?.becomeEditingNext()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func cancelObject(_ sender: AnyObject) -> Void {
        StaffMemoryInstance.sharedInstance.clearAllVariables()
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: CREATE
    func createStaff(_ sender: AnyObject) -> Bool {
        
        self.createStaffDispose?.dispose()
        self.createStaffDispose = nil
        
        let creatingStaff = User()
        creatingStaff.userId = Database.shared.uniqueIncreasingPrimaryKey()
        creatingStaff.isCurrentUser = true
        
        if let ipAdress_ = StaffMemoryInstance.sharedInstance.ipAdress {
            AppSettings.shared.saveDeviceIpAddress(ipAdress_)
            if ipAdress_.characters.count < 2 {
                return showErrorAlert(text: "Ip adress")
            }
        } else {
            return showErrorAlert(text: "Ip adress")
        }
        
        if let firstName_ = StaffMemoryInstance.sharedInstance.firstName {
            creatingStaff.firstName = firstName_
            if firstName_.characters.count < 2 {
                return showErrorAlert(text: "Имя")
            }
        } else {
            return showErrorAlert(text: "Имя")
        }
        
        if let secondName_ = StaffMemoryInstance.sharedInstance.secondName {
            creatingStaff.secondName = secondName_
            if secondName_.characters.count < 2 {
                return showErrorAlert(text: "Фамилия")
            }
        } else {
            return showErrorAlert(text: "Фамилия")
        }
        
        if let phoneNumber_ = StaffMemoryInstance.sharedInstance.phoneNumber {
            creatingStaff.phoneNumber = phoneNumber_
            if phoneNumber_.characters.count < 2 {
                return showErrorAlert(text: "Тел.")
            }
        } else {
            return showErrorAlert(text: "Тел.")
        }
        
        if let address_ = StaffMemoryInstance.sharedInstance.address {
            creatingStaff.address = address_
            if address_.characters.count < 2 {
                return showErrorAlert(text: "Адрес")
            }
        } else {
            return showErrorAlert(text: "Адрес")
        }
        
        if StaffMemoryInstance.sharedInstance.roleId > 0 {
            creatingStaff.roleId = StaffMemoryInstance.sharedInstance.roleId
        } else {
            return showErrorAlert(text: "Роль")
        }
        
        
        if let email_ = StaffMemoryInstance.sharedInstance.email {
            creatingStaff.email = email_
            
            if email_.characters.count < 2 {
                return showErrorAlert(text: "Введите почту")
            }
            
            if let _ = Database.shared.userBy(email: email_) {
                return showErrorAlert(text: "Почта должна быть уникальна, такая почта уже существует!")
            }
            
        } else {
            return showErrorAlert(text: "Введите почту")
        }
        
        if let password_ = StaffMemoryInstance.sharedInstance.password {
            creatingStaff.password = password_
            if password_.characters.count < 2 {
                return showErrorAlert(text: "Введите пароль")
            }
        } else {
            return showErrorAlert(text: "Введите пароль")
        }
        
        if let confirmPassword_ = StaffMemoryInstance.sharedInstance.confirmPassword {
            creatingStaff.password = confirmPassword_
            if confirmPassword_.characters.count < 2 {
                return showErrorAlert(text: "Введите потверждение пароля")
            }
        } else {
            return showErrorAlert(text: "Введите потверждение пароля")
        }
        
        if let password_ = StaffMemoryInstance.sharedInstance.password, let confirmPassword_ = StaffMemoryInstance.sharedInstance.confirmPassword {
            if password_ != confirmPassword_ {
                return showErrorAlert(text: "Пароли не совподают")
            }
        }
        
        /*
        let createStaffSignal = RPCNetwork.shared.createStaffRPC(staff: creatingStaff)
        let databaseSignal = createStaffSignal |> mapToSignal({ (newStaffId: UInt64) -> Signal<Bool, Error> in
            
            let signal = Signal<Bool, Error> { subscriber in
                
                creatingStaff.staffId = newStaffId
                
                subscriber.putNext(true)
                subscriber.putCompletion()
                return ActionDisposable {}
            }
            
            return signal
        })
        
        let finalSignal = databaseSignal |> runOn(self.generateSignInQue) |> deliverOn(Queue.mainQueue())
        
        self.showLoading("Creating account")
        self.createStaffDispose = finalSignal.start(next: { (b: Bool) in
            self.dismissLoading()
            print("finalSignal finished success \(b)")
            
            Database.shared.createStaff([creatingStaff])
            AppSession.shared.currentEmployee = creatingStaff
            StaffMemoryInstance.sharedInstance.clearAllVariables()
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.enterMainApplication()
            
        }, error: { (Void) in
            self.dismissLoading()
            let _ = self.showErrorAlert(text: "Create account error")
            print("finalSignal finished with error")
            
            //Database.shared.createStaff([creatingStaff])
            //AppSession.shared.currentEmployee = creatingStaff
            //StaffMemoryInstance.sharedInstance.clearAllVariables()
            //let appDelegate = UIApplication.shared.delegate as! AppDelegate
            //appDelegate.enterMainApplication()
            
        }, completed: {
            self.dismissLoading()
            print("finalSignal completed")
        })
        
        */
        
        return true
    }
    
    // MARK: Private
    private lazy var formerInputAccessoryView: FormerInputAccessoryView = FormerInputAccessoryView(former: self.former)
    
    // MARK: Needed product section
    private lazy var neededProductSection: SectionFormer = { [weak self] in
        
        let ipAdressRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            $0.titleLabel.text = "ip adress"
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.returnKeyType = .next
            $0.textField.keyboardType = .emailAddress
            $0.textField.autocapitalizationType = .none
            
            }.configure { [weak self] in
                $0.placeholder = "ip adress"
                $0.text = ""
                $0.rowHeight = 50
                
            }.onTextChanged { [weak self] in
                StaffMemoryInstance.sharedInstance.ipAdress = $0
        }
        
        let nameRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            $0.titleLabel.text = "Имя"
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.returnKeyType = .next
            }.configure {
                $0.placeholder = "Имя"
                $0.text = ""
                $0.rowHeight = 50
            }.onTextChanged {
                StaffMemoryInstance.sharedInstance.firstName = $0
        }
        
        let secondNameRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            $0.titleLabel.text = "Фамил."
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.returnKeyType = .next
            }.configure {
                $0.placeholder = "Фамил."
                $0.text = ""
                $0.rowHeight = 50
            }.onTextChanged {
                StaffMemoryInstance.sharedInstance.secondName = $0
        }
        
        let phoneRow = TextFieldRowFormer<PhoneFiledTVCell>() { [weak self] in
            $0.titleLabel.text = "Тел"
            $0.textField.keyboardType = .numberPad
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.returnKeyType = .next
            }.configure {
                $0.text = ""
            }.onTextChanged {
                StaffMemoryInstance.sharedInstance.phoneNumber = $0
        }
        
        let emailRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            $0.titleLabel.text = "Почта"
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.returnKeyType = .next
            $0.textField.keyboardType = .emailAddress
            $0.textField.autocapitalizationType = .none
            }.configure { [weak self] in
                $0.placeholder = "Почта"
                $0.rowHeight = 50
                $0.text = ""
                
            }.onTextChanged { [weak self] in
                StaffMemoryInstance.sharedInstance.email = $0
        }
        
        let passwordRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            $0.titleLabel.text = "Пароль"
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.isSecureTextEntry = true
            $0.textField.autocapitalizationType = .none
            $0.textField.returnKeyType = .next
            }.configure { [weak self] in
                $0.placeholder = "Пароль"
                $0.rowHeight = 50
                $0.text = ""
                
            }.onTextChanged { [weak self] in
                StaffMemoryInstance.sharedInstance.password = $0
        }
        
        let confirmPasswordRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            $0.titleLabel.text = "Потв пароля"
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.isSecureTextEntry = true
            $0.textField.autocapitalizationType = .none
            $0.textField.returnKeyType = .next
            }.configure { [weak self] in
                $0.placeholder = "Потверждение пароля"
                $0.rowHeight = 50
                $0.text = ""
                
            }.onTextChanged { [weak self] in
                StaffMemoryInstance.sharedInstance.confirmPassword = $0
        }
        
        let section = SectionFormer(rowFormer: ipAdressRow, nameRow, secondNameRow, phoneRow, emailRow, passwordRow, confirmPasswordRow)
        section.set(headerViewFormer: self?.createHeaderFunc(s: "Main needed rows"))
        return section
    }()
    
    private lazy var addressInfoSection: SectionFormer = { [weak self] in
        
        let addressRow = TextViewRowFormer<FormTextViewCell>() { [weak self] in
            $0.textView.textColor = .formerSubColor()
            $0.textView.font = .systemFont(ofSize: 20)
            $0.textView.inputAccessoryView = self?.formerInputAccessoryView
            }.configure {
                $0.placeholder = "Address"
                $0.text = ""
            }.onTextChanged {
                StaffMemoryInstance.sharedInstance.address = $0
        }
        
        let section = SectionFormer(rowFormer: addressRow)
        section.set(headerViewFormer: self?.createHeaderFunc(s: "Address"))
        return section
    }()
    
    private func createHeaderFunc(s: String) -> ViewFormer {
        return LabelViewFormer<FormLabelHeaderView>()
            .configure {
                $0.viewHeight = 40
                $0.text = s
        }
    }
    
    func someFunc() -> SectionFormer {
        
        let createSelectorRow = { (
            text: String,
            subText: String,
            onSelected: ((RowFormer) -> Void)?
            ) -> RowFormer in
            return LabelRowFormer<FormLabelCell>() {
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
                $0.subTextLabel.textColor = .formerSubColor()
                $0.subTextLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                }.configure { form in
                    _ = onSelected.map { form.onSelected($0) }
                    form.text = text
                    form.subText = subText
            }
        }
        
        let roleOptions = ["Владелец", "Администратор", "Агент"]
        
        var roleName: String = "Не выбрано"
        if StaffMemoryInstance.sharedInstance.roleId == 1000 {
            roleName = "Владелец"
        } else if StaffMemoryInstance.sharedInstance.roleId == 2000 {
            roleName = "Администратор"
        } else if StaffMemoryInstance.sharedInstance.roleId == 3000 {
            roleName = "Агент"
        }
        
        let roleSelectorRow = createSelectorRow("Роль", roleName, pushSelectorRowSelected(options: roleOptions))
        
        let section = SectionFormer(rowFormer: roleSelectorRow)
        section.set(headerViewFormer: self.createHeaderFunc(s: "Роль"))
        return section
    }
    
    private func pushSelectorRowSelected(options: [String]) -> (RowFormer) -> Void {
        return { [weak self] rowFormer in
            if let rowFormer = rowFormer as? LabelRowFormer<FormLabelCell> {
                let controller = TextSelectorViewContoller()
                controller.texts = options
                controller.selectedText = rowFormer.subText
                controller.onSelected = {
                    
                    rowFormer.subText = $0
                    rowFormer.update()
                    
                    if $0 == "Владелец" {
                        print("Owner")
                        StaffMemoryInstance.sharedInstance.roleId = 1000
                    } else if $0 == "Администратор" {
                        print("Admin")
                        StaffMemoryInstance.sharedInstance.roleId = 2000
                    } else if $0 == "Агент" {
                        print("Agent")
                        StaffMemoryInstance.sharedInstance.roleId = 3000
                    }
                }
                self?.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    private func configure() {
        
        tableView.contentInset.top = 40
        tableView.contentInset.bottom = 40
        
        former.append(sectionFormer:self.neededProductSection, self.addressInfoSection)
            .onCellSelected { [weak self] _ in
                self?.formerInputAccessoryView.update()
        }
    }
}
