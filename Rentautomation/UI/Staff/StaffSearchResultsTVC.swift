//
//  StaffSearchResultsTVC.swift
//  Rentautomation
//
//  Created by kanybek on 2/3/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class StaffSearchResultsTVC: UITableViewController {

    var staffSelectBlock : ((User) -> Void)?
    
    var initalString: String?
    
    convenience init() {
        self.init(initalString: "koke")
    }
    
    init(initalString: String) {
        self.initalString = initalString
        super.init(style: .plain)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    var filteredStaff = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "StaffListTVCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "StaffListTVCell")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredStaff.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "StaffListTVCell", for: indexPath) as! StaffListTVCell
        let object = filteredStaff[(indexPath as NSIndexPath).row]
        cell.staff = object
        cell.reloadData()
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let staff = filteredStaff[(indexPath as NSIndexPath).row]
        if let staffSelectBlock_ = self.staffSelectBlock {
            staffSelectBlock_(staff)
        }
    }

}
