//
//  AdditionalDocTypesVC.swift
//  Rentautomation
//
//  Created by kanybek on 8/22/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class AdditionalDocTypesVC: FormViewController {
    
    var initialString: String?
    
    convenience init() {
        self.init(initialString: nil)
    }
    
    init(initialString: String?) {
        self.initialString = initialString
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    deinit {
    }
    
    // MARK: Public
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Дополнительно"
        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
    }
    
    // MARK: Private
    private func configure() {
        
        // Create RowFormers
        let createMenu: ((String, (() -> Void)?) -> RowFormer) = {[weak self] text, onSelected in
            return LabelRowFormer<FormLabelCell>() {
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                }.configure {
                    $0.text = text
                    $0.rowHeight = 70
                }.onSelected { _ in
                    self?.former.deselect(animated: true)
                    onSelected?()
            }
        }
        
        // Create Headers and Footers
        let createHeader: ((String, UIColor) -> ViewFormer) = { text, color in
            return LabelViewFormer<FormLabelHeaderView>()
                .configure {
                    $0.text = text
                    $0.textColor = color
                    $0.viewHeight = 40
            }
        }
        
        let createFooter: ((String) -> ViewFormer) = { text in
            return LabelViewFormer<FormLabelFooterView>()
                .configure {
                    $0.text = text
                    $0.viewHeight = 55
            }
        }
        
        let receiveFromCustomerRow = createMenu("Возврат от продажи") {[weak self] (Void) in
            let documentVC = MainDocumentVC(orderDocument: .returnFromCustomer, initalOrder: nil)
            documentVC.title = "Возврат от продажи"
            let navController = RNNavigationController(rootViewController: documentVC)
            navController.modalPresentationStyle = .fullScreen
            self?.present(navController, animated: true, completion:{_ in
            })
        }
        
        let resendSupplierRow = createMenu("Возврат от закупки") {[weak self] (Void) in
            let documentVC = MainDocumentVC(orderDocument: .returnToSupplier, initalOrder: nil)
            documentVC.title = "Возврат от закупки"
            let navController = RNNavigationController(rootViewController: documentVC)
            navController.modalPresentationStyle = .fullScreen
            self?.present(navController, animated: true, completion:{_ in
            })
        }
        
        let requestForCustomerRow = createMenu("Сделать заказ для клиента") {[weak self] (Void) in
            let documentVC = MainDocumentVC(orderDocument: .preOrderToCustomer, initalOrder: nil)
            documentVC.title = "Сделать заказ для клиента"
            let navController = RNNavigationController(rootViewController: documentVC)
            navController.modalPresentationStyle = .fullScreen
            self?.present(navController, animated: true, completion:{_ in
            })
        }
        
        let inventoryRow = createMenu("Инвентаризация / Черновик") {[weak self] (Void) in
            let documentVC = MainDocumentVC(orderDocument: .stockTaking, initalOrder: nil)
            documentVC.title = "Инвентаризация / Черновик"
            let navController = RNNavigationController(rootViewController: documentVC)
            navController.modalPresentationStyle = .fullScreen
            self?.present(navController, animated: true, completion:{_ in
            })
        }
        
        let productCreationRow = createMenu("Производство") {[weak self] (Void) in
            let inventoryVC = InventoryVC(initialString: nil)
            inventoryVC.title = "Производство"
            let navController = RNNavigationController(rootViewController: inventoryVC)
            navController.modalPresentationStyle = .fullScreen
            self?.present(navController, animated: true, completion:{_ in
            })
        }
        
        let productRemoveRow = createMenu("Списание товара") {[weak self] (Void) in
            let inventoryVC = InventoryVC(initialString: nil)
            inventoryVC.title = "Списание товара"
            let navController = RNNavigationController(rootViewController: inventoryVC)
            navController.modalPresentationStyle = .fullScreen
            self?.present(navController, animated: true, completion:{_ in
            })
        }
        
        let defaultSection = SectionFormer(rowFormer: requestForCustomerRow, productCreationRow, inventoryRow, productRemoveRow)
            .set(headerViewFormer: createHeader("Инвентаризация / Черновик", UIColor.wisteriaColor()))
        //.set(footerViewFormer: createFooter(""))
        
        let resendSection = SectionFormer(rowFormer: receiveFromCustomerRow, resendSupplierRow)
            .set(headerViewFormer: createHeader("Возврат", UIColor.alizarinColor()))
        
        //for (index, row) in activitySection.rowFormers.enumerated() {
        //    if (index == 1) {
        //        row.enabled = false
        //    } else {
        //        row.enabled = true
        //    }
        //}
        
        for (index, row) in defaultSection.rowFormers.enumerated() {
            if (index == 2 || index == 0) {
                row.enabled = true
            } else {
                row.enabled = false
            }
        }
        
        former.append(sectionFormer: defaultSection, resendSection)
    }
}
