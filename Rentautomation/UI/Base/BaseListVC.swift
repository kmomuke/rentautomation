//
//  BaseListVC.swift
//  Rentautomation
//
//  Created by kanybek on 12/8/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import MZFormSheetPresentationController
import SVProgressHUD

struct AlertOptions {
    
    let message: String
    let showArrow: Bool
    let backgroundColor: UIColor
    let size: CGSize
    let canDismissByTap: Bool
    
    init(message: String = "Error!", shouldShowArrow: Bool = true, backgroundColor: UIColor = UIColor.white, size: CGSize = CGSize.zero, canDismissByTappingAnywhere canDismiss: Bool = true) {
        self.message = message
        self.showArrow = shouldShowArrow
        self.backgroundColor = backgroundColor
        self.size = size
        self.canDismissByTap = canDismiss
    }
}

protocol ErrorPopoverRenderer {
    func presentError(_ errorOptions: AlertOptions?)
    
    func showSuccessAlert(text: String) -> Void
    func showErrorAlert(text: String) -> Bool
    
    func showLoading(_ text: String) -> Void
    func showProgress(progress: Float, text: String) -> Void
    func dismissLoading() -> Void
    
    func presentSheet(viewController: UIViewController) -> Void
    
    func add(asChildViewController viewController: UIViewController, frame_: CGRect) -> Void
    func remove(asChildViewController viewController: UIViewController) -> Void
}

extension ErrorPopoverRenderer where Self: UIViewController {
    func presentError(_ errorOptions: AlertOptions?) {
        // show alert here
    }

    func showSuccessAlert(text: String) -> Void {
        SVProgressHUD.showSuccess(withStatus: text)
    }
    
    func showErrorAlert(text: String) -> Bool {
        SVProgressHUD.showError(withStatus: text)
        return false
    }
    
    func showLoading(_ text: String) -> Void {
        SVProgressHUD.show(withStatus: text)
    }
    
    func showProgress(progress: Float, text: String) -> Void {
        SVProgressHUD.showProgress(progress, status: text)
    }
    
    func dismissLoading() -> Void {
        SVProgressHUD.dismiss()
    }

    func presentSheet(viewController: UIViewController) -> Void {

        let navController = RNNavigationController(rootViewController: viewController)
        let formSheetController = MZFormSheetPresentationViewController(contentViewController: navController)
        //formSheetController.allowDismissByPanningPresentedView = true
        formSheetController.contentViewControllerTransitionStyle = .slideAndBounceFromBottom
        formSheetController.presentationController?.contentViewSize = CGSize(width: 800, height: 700)
        self.present(formSheetController, animated: true, completion:{_ in
        })
    }
    
    // MARK: - Helper Methods
    
    func add(asChildViewController viewController: UIViewController, frame_: CGRect) {
        // Add Child View Controller
        addChildViewController(viewController)
        
        // Add Child View as Subview
        view.addSubview(viewController.view)
        
        // Configure Child View
        viewController.view.frame = frame_
        //viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Notify Child View Controller
        viewController.didMove(toParentViewController: self)
    }
    
    func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParentViewController: nil)
        
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        
        // Notify Child View Controller
        viewController.removeFromParentViewController()
    }
    
}

class KrakenViewController: UIViewController, ErrorPopoverRenderer {
    
    func failedToEatHuman() {
        presentError(AlertOptions(message: "Oh noes! I didn't get to eat the Human!", size: CGSize(width: 1000.0, height: 200.0)))
    }
}


class BaseListVC: UIViewController {

//    var initialItem: String?
//    
//    convenience init() {
//        self.init(initialItem: nil)
//    }
//    
//    init(initialItem: String?) {
//        self.initialItem = initialItem
//        super.init(nibName: nil, bundle: nil)
//    }
//    
//    required init(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//    
//    deinit {
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func addCanceButton() -> Void {
        let cancelButton = UIBarButtonItem(
            title: "Закрыть",
            style: .plain,
            target: self,
            action: #selector(cancelVC(_:))
        )
        self.navigationItem.leftBarButtonItem = cancelButton
    }
    
    func cancelVC(_ sender: AnyObject) -> Void {
        let empty = EmptyVC()
        let detailNavigationController = RNNavigationController(rootViewController:empty)
        self.splitViewController?.showDetailViewController(detailNavigationController, sender: self)
    }
    
    func clearSplitDetailVC() -> Void {
        let empty = EmptyVC()
        let detailNavigationController = RNNavigationController(rootViewController:empty)
        self.splitViewController?.showDetailViewController(detailNavigationController, sender: self)
    }
}
