//
//  DebtsListVC.swift
//  Rentautomation
//
//  Created by kanybek on 4/19/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit
import CSNotificationView

class DebtsListVC: BaseListVC, ErrorPopoverRenderer, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var searchTextfield: UITextField!
    @IBOutlet weak var tableView: UITableView!
    var customers: [Customer] = []
    var disposeSet: DisposableSet?
    
    var notificationView: CSNotificationView?
    
    var initalString: String?
    
    convenience init() {
        self.init(initalString: "kokekoke")
    }
    
    init(initalString: String) {
        self.initalString = initalString
        self.disposeSet = DisposableSet()
        super.init(nibName: "DebtsListVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.disposeSet?.dispose()
        self.disposeSet = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addCanceButton()
        
        if let initalString_ = self.initalString {
            self.title = initalString_
        }
        
        let addButton = UIBarButtonItem(
            title: "Добавить",
            style: .plain,
            target: self,
            action: #selector(createCustomer(_:))
        )
        
        let generateButton = UIBarButtonItem(
            title: "Сотрудник",
            style: .plain,
            target: self,
            action: #selector(userPressed(_:))
        )
        
        self.navigationItem.rightBarButtonItems = [addButton, generateButton]
        
        let nib = UINib(nibName: "CustomersListTVCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "CustomersListTVCell")
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.reloadAllCustomers()
        
        self.searchTextfield.delegate = self
        self.searchTextfield.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        let customerCreateSignal: Signal = PipesStore.shared.customerCreatedSignal()
        let createCustomerDispose = customerCreateSignal.start(next: {[weak self] (customer: Customer) in
            self?.customers.insert(customer, at: 0)
            self?.tableView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
            self?.tableView.beginUpdates()
            self?.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
            self?.tableView.endUpdates()
        })
        self.disposeSet?.add(createCustomerDispose)
        
        let customerUpdateSignal: Signal = PipesStore.shared.customerUpdatedSignal()
        let updateCustomerDispose = customerUpdateSignal.start(next: {[weak self] (customer: Customer) in
            if let i = self?.customers.index(where: {$0.customerId == customer.customerId}) {
                self?.customers[i] = customer
                self?.tableView.reloadRows(at: [IndexPath(row: i, section: 0)], with: .fade)
            }
        })
        self.disposeSet?.add(updateCustomerDispose)
        
        let balanceUpdatedSignal: Signal = PipesStore.shared.balanceUpdatedSignal()
        let updateCustomerBalanceDispose = balanceUpdatedSignal.start(next: {[weak self] (customerId: UInt64) in
            if let indexRow = self?.customers.index(where: {$0.customerId == customerId}) {
                self?.tableView.reloadRows(at: [IndexPath(row: indexRow, section: 0)], with: .fade)
            }
        })
        self.disposeSet?.add(updateCustomerBalanceDispose)
        
        let allSyncFinishedSignal: Signal = PipesStore.shared.allSyncOperationsFinishedSignal()
        let allSyncFinishedDispose = allSyncFinishedSignal.start(next: {[weak self] (finished: Bool) in
            self?.reloadAllCustomers()
        })
        self.disposeSet?.add(allSyncFinishedDispose)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func reloadAllCustomers() {
        Database.shared.loadAllCustomers(limit: Global.shared.limitForStaticModels, userId: 0) {[weak self] (customers_: [Customer]) in
            self?.customers = customers_
            self?.tableView.reloadData()
        }
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        if let searchText = textField.text {
            if searchText.characters.count > 0 {
                self.showLoading("Поиск")
                Database.shared.searchCustomerBy(name: searchText, limit: 100, completion: {[weak self] (foundCustomers: [Customer]) in
                    self?.dismissLoading()
                    self?.customers = foundCustomers
                    self?.tableView.reloadData()
                })
            } else {
                self.reloadAllCustomers()
            }
        }
    }
    
    func createCustomer(_ sender: AnyObject) {
        let createCustomerVC = CreateCustomerVC()
        self.presentSheet(viewController: createCustomerVC)
    }
    
    func userPressed(_ sender: UIBarButtonItem) {
        self.hideNotificationView()
        let staffListVC: StaffListVC = StaffListVC(initialString: nil, isCommingFromCustomer: true)
        staffListVC.staffSelectBlock = {[weak self]  (employee: User) -> Void in
            Database.shared.loadAllCustomers(limit: Global.shared.limitForStaticModels, userId: employee.userId) {[weak self] (customers_: [Customer]) in
                
                self?.customers = customers_
                self?.tableView.reloadData()
                
                let stringStr = "Сотрудник: \(employee.firstName) \(employee.secondName)"
                self?.showNotificationView(stringStr)
            }
        }
        
        staffListVC.modalPresentationStyle = UIModalPresentationStyle.popover
        staffListVC.preferredContentSize = CGSize(width: 400, height: 400)
        
        present(staffListVC, animated: true, completion: nil)
        
        let popoverPresentationController = staffListVC.popoverPresentationController
        popoverPresentationController?.barButtonItem = sender
    }
    
    
    private func showNotificationView(_ message_: String) -> Void {
        
        self.notificationView = CSNotificationView(parentViewController: self,
                                                   tintColor: UIColor.midnightBlueColor(),
                                                   image: UIImage(named: "close-X")!,
                                                   message: message_)
        
        self.notificationView?.tapHandler = {[weak self] (Void) -> Void in
            self?.hideNotificationView()
        }
        
        self.notificationView?.setVisible(true, animated: true, completion: {[weak self] in
            
            if let strongSelf = self {
                
                let originalInset: UIEdgeInsets = strongSelf.tableView.contentInset
                var inset: UIEdgeInsets = originalInset
                inset.top = 10
                let originalContentOffset: CGPoint = strongSelf.tableView.contentOffset;
                let contentOffset: CGPoint = originalContentOffset;
                
                UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions(), animations: {
                    strongSelf.tableView.bounds = CGRect(x: 0, y: contentOffset.y, width: strongSelf.tableView.frame.size.width, height: strongSelf.tableView.frame.size.height)
                    strongSelf.tableView.contentInset = inset;
                    strongSelf.tableView.scrollIndicatorInsets = inset;
                }, completion: { (finished:Bool) in
                })
                print("show completed")
            }
        })
    }
    
    private func hideNotificationView() -> Void {
        if let notifView = self.notificationView {
            notifView.setVisible(false, animated: true, completion: {[weak self] in
                if let strongSelf = self {
                    strongSelf.tableView.contentInset.top = 0
                    strongSelf.reloadAllCustomers()
                    print("hide completed")
                }
            })
        }
    }
    
    // MARK: TableView DataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return customers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomersListTVCell", for: indexPath) as! CustomersListTVCell
        let object = customers[(indexPath as NSIndexPath).row]
        cell.customer = object
        cell.reloadData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let customer = customers[(indexPath as NSIndexPath).row]
        let customerDebtVC = CustomerDebtVC(initalCustomer: customer)
        self.navigationController?.pushViewController(customerDebtVC, animated: true)
    }
}

extension DebtsListVC: UITextFieldDelegate {
    
    //    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    //
    //    }
    
    //    func textFieldDidBeginEditing(_ textField: UITextField) {
    //
    //    }
    
    //    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
    //
    //    }
    
    //    func textFieldDidEndEditing(_ textField: UITextField) {
    //
    //    }
    
    //    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
    //
    //    }
    
    //    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    //
    //    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("textFieldShouldClear")
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("textFieldShouldReturn")
        textField.resignFirstResponder()
        return true
    }
}
