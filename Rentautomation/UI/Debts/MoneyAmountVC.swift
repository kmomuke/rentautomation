//
//  MoneyAmountVC.swift
//  Rentautomation
//
//  Created by kanybek on 4/19/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class MoneyAmountVC: UIViewController {
    
    var orderDocument: OrderDocumentType
    
    @IBOutlet weak var forDebtLabel: UILabel!
    @IBOutlet weak var debtSwitch: UISwitch!
    
    convenience init() {
        self.init(orderDocument: .none)
    }
    
    init(orderDocument: OrderDocumentType) {
        self.orderDocument = orderDocument
        super.init(nibName: "MoneyAmountVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    var totalPrice: Double = 0.0
    
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var priceTextField: BKCurrencyTextField!
    @IBOutlet weak var onTextFieldLabel: UILabel!
    

    var performSaveBlock : ((Void) -> Void)?
    var dismissBlock : ((_ staffEnteredMoney: Double, _ isForDebt: Bool) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let canceButton = UIBarButtonItem(
            title: "Отмена",
            style: .plain,
            target: self,
            action: #selector(cancelObject(_:))
        )
        self.navigationItem.leftBarButtonItem = canceButton
        
        switch orderDocument {
        case .saleToCustomer:
            self.title = "Продажа на сумму"
            self.onTextFieldLabel.text = "Продажа на сумму"
            self.forDebtLabel.isHidden = true
            self.debtSwitch.isHidden = true
            self.debtSwitch.setOn(false, animated: false)
        case .moneyReceive:
            self.title = "Приход денег на сумму"
            self.onTextFieldLabel.text = "Приход денег на сумму"
        default:
            print("default:")
        }
        
        self.priceTextField.numberValue = NSDecimalNumber(value: 0.0)
        
        let events: [(Selector, UIControlEvents)] = [(#selector(MoneyAmountVC.textChanged(textField:)), .editingChanged),
                                                     (#selector(MoneyAmountVC.editingDidBegin(textField:)), .editingDidBegin),
                                                     (#selector(MoneyAmountVC.editingDidEnd(textField:)), .editingDidEnd)]
        events.forEach {
            self.priceTextField.addTarget(self, action: $0.0, for: $0.1)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.priceTextField.becomeFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func cancelObject(_ sender: AnyObject) -> Void {
        self.navigationController?.dismiss(animated: true, completion: {[weak self] in
        })
    }
    
    @IBAction func switchDidChanged(_ sender: UISwitch) {
    }
    
    @IBAction func okButtonPressed(_ sender: UIButton) {
        
        if let performSaveBlock_ = self.performSaveBlock {
            performSaveBlock_()
        }
        
        var number: Double = 0.0
        if (self.priceTextField.text?.characters.count)! > 0 {
            number = self.priceTextField.numberValue.doubleValue
            print("number is:\(number)")
        } else {
            print("number is:\(number)")
        }
        
        self.navigationController?.dismiss(animated: true, completion: {[weak self] in
            if let dismissBlock_ = self?.dismissBlock, let strongSelf = self {
                dismissBlock_(number, strongSelf.debtSwitch.isOn)
            }
        })
    }
    
    dynamic func textChanged(textField: UITextField) {
        if (self.priceTextField.text?.characters.count)! > 0 {
            let number = self.priceTextField.numberValue.doubleValue
            print("number is:\(number)")
            self.totalPrice = number
        } else {
            let number: Double = 0.0
            print("number is:\(number)")
            self.totalPrice = number
        }
    }
    
    dynamic func editingDidBegin(textField: UITextField) {
    }
    
    dynamic func editingDidEnd(textField: UITextField) {
    }
    
}
