//
//  CustomerDebtVC.swift
//  Rentautomation
//
//  Created by kanybek on 4/19/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit

class CustomerDebtVC: BaseListVC, ErrorPopoverRenderer {
    
    var initalCustomer: Customer?
    var disposeSet: DisposableSet?
    var isFirstTime: Bool = true
    
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var operationsContainerView: UIView!
    
    @IBOutlet weak var correctBallanceButton: UIButton!
    @IBOutlet weak var adressLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    convenience init() {
        self.init(initalCustomer: nil)
    }
    
    private lazy var moneyOperationsVC: CustomerMoneyOperationsVC = { [weak self] in
        let moneyOperationsVC_ = CustomerMoneyOperationsVC(initialCustomer: self?.initalCustomer, initialSupplier: nil)
        return moneyOperationsVC_
    }()
    
    init(initalCustomer: Customer?) {
        self.initalCustomer = initalCustomer
        self.disposeSet = DisposableSet()
        super.init(nibName: "CustomerDebtVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.disposeSet?.dispose()
        self.disposeSet = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let customerUpdateSignal: Signal = PipesStore.shared.customerUpdatedSignal()
        let updateCustomerDispose = customerUpdateSignal.start(next: {[weak self] (customer: Customer) in
            if let currentCustomer = self?.initalCustomer {
                if currentCustomer.customerId == customer.customerId {
                    self?.initalCustomer = customer
                    self?.updateUI()
                }
            }
        })
        self.disposeSet?.add(updateCustomerDispose)
        
        
        let balanceUpdatedSignal: Signal = PipesStore.shared.balanceUpdatedSignal()
        let updateCustomerBalanceDispose = balanceUpdatedSignal.start(next: {[weak self] (customerId: UInt64) in
            if let currentCustomer = self?.initalCustomer {
                if currentCustomer.customerId == customerId {
                    if let account_ = Database.shared.accountBy(customerId: customerId) {
                        self?.balanceLabel.text = Global.shared.priceFormatter.string(for: account_.balance)
                        self?.updateUI()
                        self?.moneyOperationsVC.reloadOperations()
                    }
                }
            }
        })
        self.disposeSet?.add(updateCustomerBalanceDispose)
        
        let allSyncFinishedSignal: Signal = PipesStore.shared.allSyncOperationsFinishedSignal()
        let allSyncFinishedDispose = allSyncFinishedSignal.start(next: {[weak self] (finished: Bool) in
            if let cust = self?.initalCustomer {
                if let customerNew = Database.shared.customerBy(firstName: cust.firstName, secondName: cust.secondName) {
                    self?.initalCustomer = customerNew
                    self?.moneyOperationsVC.initialCustomer = customerNew
                    self?.updateUI()
                    self?.moneyOperationsVC.reloadOperations()
                }
            }
        })
        self.disposeSet?.add(allSyncFinishedDispose)
        
        let editButton = UIBarButtonItem(
            title: "Редактировать",
            style: .plain,
            target: self,
            action: #selector(editProduct(_:))
        )
        self.navigationItem.rightBarButtonItem = editButton
        self.updateUI()
        
        
        //if AppSettings.shared.getSimpleDebtMode() {
        //    self.correctBallanceButton.isHidden = true
        //}
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (isFirstTime) {
            self.operationsContainerView.backgroundColor = UIColor.white
            self.add(asChildViewController: self.moneyOperationsVC, frame_: self.operationsContainerView.frame)
            isFirstTime = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func updateUI() -> Void {
        if let initalCustomer_ = self.initalCustomer {
            self.title = "\(initalCustomer_.firstName) \(initalCustomer_.secondName)"
            if let account_ = Database.shared.accountBy(customerId: initalCustomer_.customerId) {
                self.balanceLabel.text = Global.shared.priceFormatter.string(for: account_.balance)
                self.phoneLabel.text = initalCustomer_.phoneNumber
                self.adressLabel.text = initalCustomer_.address
            }
        }
    }
    
    func editProduct(_ sender: AnyObject) -> Void {
        let editVC = CreateCustomerVC(initialCustomer:self.initalCustomer)
        self.presentSheet(viewController: editVC)
    }
    
    @IBAction func saleButtonPressed(_ sender: UIButton) {
        let moneyAmount = MoneyAmountVC(orderDocument: .saleToCustomer)
        moneyAmount.performSaveBlock = {[weak self] (Void) -> Void in
        }
        moneyAmount.dismissBlock = {[weak self] (staffEnteredMoney: Double, isForDebt: Bool) -> Void in
            if staffEnteredMoney > 0.01 {
                if let initalCustomer_ = self?.initalCustomer, let empl_ = AppSession.shared.currentEmployee {
                    
                    let orderDetail = OrderDetail()
                    orderDetail.productId = 1000000
                    orderDetail.orderQuantity = 1
                    orderDetail.orderDetailDate = Date()
                    orderDetail.productQuantity = 0
                    orderDetail.price = staffEnteredMoney
                    let _ = OrderManager.shared.ownerSaledProductsToCustomer(orderDetails: [orderDetail],
                                                                                           staff: empl_,
                                                                                           customerId: initalCustomer_.customerId,
                                                                                           orderDocType: .saleToCustomer,
                                                                                           totalPrice: staffEnteredMoney,
                                                                                           discount: Discount())
                    self?.showSuccessAlert(text: "Документ успешно сохранен!")
                    
                    let delay = DispatchTime.now() + DispatchTimeInterval.seconds(1)
                    DispatchQueue.main.asyncAfter(deadline: delay, execute: {
                        self?.updateUI()
                        self?.moneyOperationsVC.reloadOperations()
                        PipesStore.shared.balanceUpdatedPipe.putNext(initalCustomer_.customerId)
                    })
                }
            }
        }
        self.presentSheet(viewController: moneyAmount)
    }
    
    @IBAction func moneyIncomePressed(_ sender: UIButton) {
        
        let moneyAmount = MoneyAmountVC(orderDocument: .moneyReceive)
        moneyAmount.performSaveBlock = {[weak self] (Void) -> Void in
        }
        moneyAmount.dismissBlock = {[weak self] (staffEnteredMoney: Double, isForDebt: Bool) -> Void in
            if staffEnteredMoney > 0.01 {
                if let initalCustomer_ = self?.initalCustomer, let empl_ = AppSession.shared.currentEmployee {
                    OrderManager.shared.ownerReceivedMoneyFromCustomer(customerId: initalCustomer_.customerId,
                                                                                     staffId: empl_.userId,
                                                                                     receivedMoney: staffEnteredMoney,
                                                                                     isMoneyForDebt: isForDebt,
                                                                                     comment: "")
                    
                    self?.showSuccessAlert(text: "Документ успешно сохранен!")
                    
                    let delay = DispatchTime.now() + DispatchTimeInterval.seconds(1)
                    DispatchQueue.main.asyncAfter(deadline: delay, execute: {
                        self?.updateUI()
                        self?.moneyOperationsVC.reloadOperations()
                        PipesStore.shared.balanceUpdatedPipe.putNext(initalCustomer_.customerId)
                    })
                }
            }
        }
        self.presentSheet(viewController: moneyAmount)
    }
    
    @IBAction func correctBallancePressed(_ sender: UIButton) {
        if let currentCustomer = self.initalCustomer {
            if Database.shared.isModelSynchronized(primaryKey: currentCustomer.customerId) {
                
                if let account_ = Database.shared.accountBy(customerId: currentCustomer.customerId) {
                    
                    var debitType: DebitType = .ownerShouldTakeMoneyFromClient
                    if (account_.balance >= 0.0) {
                        debitType = .ownerShouldTakeMoneyFromClient
                    } else {
                        debitType = .ownerShouldGiveMoneyToClient
                    }
                    
                    let setBallanceVC = SetBallanceVC(initialBalance: account_.balance, initialDebitType: debitType, customerId: currentCustomer.customerId, supplierId: 0)
                    self.presentSheet(viewController: setBallanceVC)
                }
                
            } else {
                let _ = self.showErrorAlert(text: "не сохранен")
            }
        }
    }
}
