//
//  PriceFieldTVCell.swift
//  Rentautomation
//
//  Created by kanybek on 10/7/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

import SwiftSignalKit
import Neon

class PriceFieldTVCell: UITableViewCell, TextFieldFormableRow, UITextFieldDelegate {
    
    var textField: BKCurrencyTextField!
    var titleLabel: UILabel!
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    open func updateWithRowFormer(_ rowFormer: RowFormer) {}
    
    open func setup() {
        
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        textLabel?.backgroundColor = .clear
        
        self.titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 80, height: 40))
        self.titleLabel.textColor = .formerColor()
        self.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
        self.titleLabel.numberOfLines = 2
        contentView.addSubview(self.titleLabel)
        
        self.textField = BKCurrencyTextField(frame: CGRect(x: 20, y: 5, width: 300, height: 40))
        self.textField.backgroundColor    = UIColor.clear
        self.textField.textColor = .formerSubColor()
        self.textField.clearButtonMode = UITextFieldViewMode.whileEditing;
        contentView.addSubview(self.textField)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func reloadData() -> Void {
    }
    
    override func prepareForReuse() {
        self.setNeedsLayout()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.titleLabel.anchorToEdge(.left, padding: 18, width: 80, height: 40)
        
        var width_: CGFloat = 0.0
        if (self.bounds.size.width - 130) > 500.0 {
            width_ = 500.0
        } else {
            width_ = self.bounds.size.width - CGFloat(130)
        }
        
        self.textField.align(.toTheRightCentered, relativeTo: self.titleLabel, padding: 20, width: width_, height: 40)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func formTextField() -> UITextField {
        return self.textField
    }
    
    func formTitleLabel() -> UILabel? {
        return self.titleLabel
    }
}
