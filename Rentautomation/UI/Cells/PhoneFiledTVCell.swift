//
//  PhoneFiledTVCell.swift
//  Rentautomation
//
//  Created by kanybek on 9/28/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

import SwiftSignalKit

class PhoneFiledTVCell: UITableViewCell, TextFieldFormableRow, UITextFieldDelegate {

    var textField: VMaskTextField!
    var titleLabel: UILabel!
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    open func updateWithRowFormer(_ rowFormer: RowFormer) {}
    
    open func setup() {
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        textLabel?.backgroundColor = .clear
        
        self.titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 80, height: 40))
        self.titleLabel.textColor = .formerColor()
        self.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)

        contentView.addSubview(self.titleLabel)
        
        self.textField = VMaskTextField(frame: CGRect(x: 20, y: 5, width: 300, height: 40))
        self.textField.mask = "+996(###)-##-##-##"
        self.textField.placeholder = "+996(552)-23-23-11"
        //self.textField.mask = "+#(###) ###-##-##"
        //self.textField.placeholder = "+7(926) 915-25-35"
        
        
        self.textField.textColor = .formerSubColor()
        self.textField.backgroundColor    = UIColor.clear
        self.textField.clearButtonMode = UITextFieldViewMode.whileEditing;
        //self.textField.delegate = self
        //self.textField.addTarget(self, action: #selector(PhoneFiledTVCell.textFieldValueChanged(_:)), for: .editingChanged)
        
        contentView.addSubview(self.textField)
    }
    
    func textFieldValueChanged(_ sender: VMaskTextField){
        print("sdfdfdsffsdfdssender.text=>\(sender.text)")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func reloadData() -> Void {
    }
    
    override func prepareForReuse() {
        self.setNeedsLayout()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.titleLabel.anchorToEdge(.left, padding: 15, width: 80, height: 40)
        self.textField.align(.toTheRightCentered, relativeTo: self.titleLabel, padding: 7, width: 500, height: 40)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func formTextField() -> UITextField {
        return self.textField
    }
    
    func formTitleLabel() -> UILabel? {
        return self.titleLabel
    }

//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        let maskTextField: VMaskTextField = textField as! VMaskTextField
//        return maskTextField.shouldChangeCharacters(in: range, replacementString: string)
//    }
    
//    func textFieldDidEndEditing(_ textField: UITextField) -> Void {
//        let q = Queue.mainQueue()
//        q.async {
//            if let title = self.titleLabel.text, let phone = textField.text {
//                if title == "Тел 1" {
//                    Profile.sharedInstance.firstPhoneNumber = phone
//                } else {
//                    Profile.sharedInstance.secondPhoneNumber = phone
//                }
//            }
//        }
//    }
}
