//
//  FastFoodVC.swift
//  Rentautomation
//
//  Created by kanybek on 10/9/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class FastFoodVC: BaseListVC, ErrorPopoverRenderer {
    
    var initialOrder: Order?
    var orderDocument: OrderDocumentType
    var isFirstTime: Bool = true
    
    @IBOutlet weak var selectedProductsContainer: UIView!
    @IBOutlet weak var productsToSelectContainer: UIView!
    
    lazy var prodcutsToSelectCVC: SelectProductsCVC = { [weak self] in
        let productListVC = SelectProductsCVC(initialOrderDocument: (self?.orderDocument)!, initialOrder: self?.initialOrder)
        return productListVC
    }()
        
    convenience init() {
        self.init(orderDocument: .none, initalOrder: nil)
    }
    
    init(orderDocument: OrderDocumentType, initalOrder: Order?) {
        self.orderDocument = orderDocument
        self.initialOrder = initalOrder
        OrderMemomry.shared.clearAllVariables()
        OrderMemomry.shared.orderDocument = orderDocument
        super.init(nibName: "FastFoodVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("FastFoodVC deleted")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let canceButton = UIBarButtonItem(
            title: "Вернутся на Главную",
            style: .plain,
            target: self,
            action: #selector(dismiss(_:))
        )
        self.navigationItem.leftBarButtonItem = canceButton
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (isFirstTime) {
            self.add(asChildViewController: self.prodcutsToSelectCVC, frame_: self.productsToSelectContainer.frame)
            isFirstTime = false
        }
    }
    
    func dismiss(_ sender: AnyObject) {
        OrderMemomry.shared.clearAllVariables()
        self.navigationController?.dismiss(animated: true, completion: {
            print("dismiss FastFoodVC")
        })
    }
}
