//
//  SearchCVCell.swift
//  MyListery
//
//  Created by kanybek on 7/3/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import Kingfisher

class SearchCVCell: UICollectionViewCell {

    var product: Product?
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func reloadData() -> Void {
        if let product_ = self.product {
            
//            if Global.shared.isDebug {
//                self.productPrimaryKeyLabel.text = "\(product_.productId)"
//                self.productPrimaryKeyLabel.backgroundColor = UIColor.clear
//            } else {
//                self.productPrimaryKeyLabel.isHidden = true
//            }
//
//            var salePrice = ""
//            if let stringSalePrice = Global.shared.numberFormatter.string(for: product_.saleUnitPrice) {
//                salePrice = stringSalePrice
//            }
//            self.salePriceLabel.text = "прод \(salePrice)"
//
//            var incomePrice = ""
//            if let stringIncomePrice = Global.shared.numberFormatter.string(for: product_.incomeUnitPrice) {
//                incomePrice = stringIncomePrice
//            }
//            self.incomePriceLabel.text = "закуп \(incomePrice)"
//

            self.nameLabel.text = product_.productName

//            var unitsInStock = ""
//            if let text_ = Global.shared.numberFormatterForAmount.string(for: product_.unitsInStock) {
//                unitsInStock = text_
//            }
//
//            self.productKeyLabel.text = "остаток на складе: \(unitsInStock) \(product_.quantityPerUnit)"
            if let url_ = product_.productImagePath {
                if url_.characters.count > 4 {
                    let url = URL(string: url_)
                    if url_.lowercased().range(of:"https") != nil {
                        let processor = ResizingImageProcessor(referenceSize: CGSize(width: 100, height: 100), mode: .none)
                        self.imageView.kf.indicatorType = .activity
                        self.imageView.kf.setImage(with: url,
                                                       placeholder: Global.shared.productPlaceholderImage,
                                                       options: [.processor(processor), .transition(.fade(0.2))],
                                                       progressBlock: nil,
                                                       completionHandler: { (im: Image?, err: NSError?, typr: CacheType, url: URL?) in
                        })
                    } else {
                        self.imageView.kf.setImage(with: url,
                                                       placeholder: Global.shared.productPlaceholderImage,
                                                       options: nil,
                                                       progressBlock: nil,
                                                       completionHandler: { (im: Image?, err: NSError?, typr: CacheType, url: URL?) in
                        })
                    }
                } else {
                    //self.cellImageView.image = Global.shared.productPlaceholderImage
                    self.imageView.setImage(string: product_.productName)
                }
            } else {
                //self.cellImageView.image = Global.shared.productPlaceholderImage
                self.imageView.setImage(string: product_.productName)
            }
            
            if Database.shared.isModelSynchronized(primaryKey: product_.productId) {
                //self.selectionStyle = .default
                //self.nameLabel.textColor = UIColor.black
            } else {
                //self.selectionStyle = .none
                //self.nameLabel.textColor = UIColor.pomergranateColor()
            }
            
        } else {
            self.resetLabels()
        }
    }
    
    
    override func prepareForReuse() {
        self.product = nil
        self.resetLabels()
    }
    
    func resetLabels() -> Void {
        self.imageView.image = nil
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //self.imageView.pin.topLeft().bottomRight()
    }
}
