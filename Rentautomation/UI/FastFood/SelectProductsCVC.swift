//
//  SelectProductsCVC.swift
//  Rentautomation
//
//  Created by kanybek on 10/10/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit

public struct JJStaggeredConfiguration
{
    public var numColumns: Int
    public var itemSpacing : Int
    public var lineSpacing: Int
    public var sectionInset: UIEdgeInsets
    public var positionType: JJStaggeredGridCellPositionArrangeType
    public var verticalScroll : Bool
    
    init() {
        self.numColumns = Int(3)
        self.itemSpacing = Int(3)
        self.lineSpacing = Int(5)
        self.sectionInset = UIEdgeInsetsMake(1, 1, 1, 1)
        self.positionType = JJStaggeredGridCellPositionArrangeType.Default
        self.verticalScroll = true
    }
}


class SelectProductsCVC: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var config : JJStaggeredConfiguration = JJStaggeredConfiguration()
    
    var initialOrder: Order?
    
    var products: [Product] = []
    var badgeDict: [UInt64: Int] = [:]
    
    var orderDocument: OrderDocumentType
    
    var priceString: String
    var shouldUseSalePrice: Bool = false
    
    var productRemovedDispose: Disposable?
    
    convenience init() {
        self.init(initialOrderDocument: .none, initialOrder: nil)
    }
    
    init(initialOrderDocument: OrderDocumentType, initialOrder: Order?) {
        self.orderDocument = initialOrderDocument
        self.initialOrder = initialOrder
        self.priceString = ""
        self.shouldUseSalePrice = false
        super.init(nibName: "SelectProductsCVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.productRemovedDispose?.dispose()
        self.productRemovedDispose = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout = JJStaggeredGridCollectionViewLayout()
        layout.minimumLineSpacing = CGFloat(config.lineSpacing)
        layout.minimumInteritemSpacing = CGFloat(config.itemSpacing)
        layout.sectionInset = config.sectionInset
        layout.numColumns = config.numColumns
        
        self.collectionView.collectionViewLayout = layout
        self.collectionView.delegate = self.provideCollectionViewDelegate()
        self.collectionView.dataSource = self.provideCollectionViewDataSource()
        self.collectionView.backgroundColor = UIColor.groupTableViewBackground
        
        let nib = UINib(nibName: "SearchCVCell", bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: "SearchCVCell")
        
        let headerNib = UINib(nibName: "HeaderCRView", bundle: nil)
        self.collectionView.register(headerNib, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "HeaderCRView")
        
        self.collectionView.register(headerNib, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "HeaderCRView")
        
        self.takeIntialDataFromDatabase()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func takeIntialDataFromDatabase() -> Void {
        Database.shared.loadAllProductsBy(categoryId: 0,
                                          limit: Global.shared.limitForStaticModels) {[weak self] (products_: [Product]) in
                                            
                                            self?.products = products_
                                            
                                            for prod in products_ {
                                                let key = prod.productId
                                                let value = 0
                                                self?.badgeDict[key] = value
                                            }
                                            
                                            if let order_ = self?.initialOrder {
                                                let orderDetails = Database.shared.orderDetailsBy(orderId: order_.orderId)
                                                for orderDetail in orderDetails {
                                                    self?.badgeDict[orderDetail.productId] = 1
                                                }
                                            }
                                            
                                            self?.collectionView.reloadData()
        }
    }
}


//MARK: CollectionViewDelegate
extension SelectProductsCVC : UICollectionViewDelegateFlowLayout
{
    public func provideCollectionViewDelegate()-> UICollectionViewDelegateFlowLayout
    {
        return self
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let theLayout = collectionViewLayout as! JJStaggeredGridCollectionViewLayout
        let numCols = theLayout.numColumns
        
        var inset = theLayout.sectionInset.left + theLayout.sectionInset.right
        var w = ((collectionView.bounds.size.width - inset ) / CGFloat(numCols)) - theLayout.minimumInteritemSpacing;
        var h = w * 1.2
        if (theLayout.scrollDirection == .horizontal){
            inset = theLayout.sectionInset.top + theLayout.sectionInset.bottom
            h = ((collectionView.bounds.size.height - inset ) / CGFloat(numCols)) - theLayout.minimumInteritemSpacing;
            w = h/2
        }
        return CGSize(width: w, height: h)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return config.sectionInset
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let theLayout = collectionViewLayout as! JJStaggeredGridCollectionViewLayout
        if ( theLayout.scrollDirection == .vertical){
            return CGSize(width: collectionView.bounds.size.width, height: 1)
        }else{
            return CGSize(width: 60, height: collectionView.bounds.size.height)
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        let theLayout = collectionViewLayout as! JJStaggeredGridCollectionViewLayout
        if ( theLayout.scrollDirection == .vertical){
            return CGSize(width: collectionView.bounds.size.width, height: 60)
        }else{
            return CGSize(width: 60, height: collectionView.bounds.size.height)
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView,
                               willDisplaySupplementaryView view: UICollectionReusableView,
                               forElementKind elementKind: String,
                               at indexPath: IndexPath) {
        if elementKind == UICollectionElementKindSectionFooter {
            print("Footer")
            //self.loadMoreMessagesBelow()
        } else {
            print("Header")
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
}

//MARK: CollectionViewDataSource
extension SelectProductsCVC: UICollectionViewDataSource
{
    public func provideCollectionViewDataSource()-> UICollectionViewDataSource {
        return self
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.products.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = String(describing:SearchCVCell.self)
        let cell = collectionView .dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! SearchCVCell
        cell.product = self.products[(indexPath as NSIndexPath).row]
        cell.reloadData()
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let identifier = kind == UICollectionElementKindSectionHeader  ? String(describing: HeaderCRView.self) : "HeaderCRView"
        let cell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: identifier, for: indexPath)
        if let cellHeaderFooter = cell as? HeaderCRView
        {
            //let text = kind == UICollectionElementKindSectionHeader ? "This is the header" : "This is the footer"
            //cellHeaderFooter.drawWithText(text:text)
        }
        return cell
    }
}

//extension SelectProductsCVC: UISearchControllerDelegate {
//
//    func presentSearchController(searchController: UISearchController) {
//        debugPrint("presentSearchController")
//    }
//
//    func willPresentSearchController(searchController: UISearchController) {
//        debugPrint("willPresentSearchController")
//    }
//
//    func didPresentSearchController(searchController: UISearchController) {
//        debugPrint("didPresentSearchController")
//    }
//
//    func willDismissSearchController(searchController: UISearchController) {
//        debugPrint("willDismissSearchController")
//    }
//
//    func didDismissSearchController(searchController: UISearchController) {
//        debugPrint("didDismissSearchController")
//    }
//}
//
//extension SelectProductsCVC:  UISearchResultsUpdating {
//
//    func updateSearchResults(for searchController: UISearchController) {
//        print("updateSearchResultsForSearchController")
//        searchController.searchResultsController?.view.isHidden = false
//        if let searchText = searchController.searchBar.text {
//
//            var text: String = ""
//            if searchText.characters.count > 0 {
//                text = searchText
//            }
//
//            self.reloadSearchResults(text: text, searchController: searchController)
//        }
//    }
//}
//
//extension SelectProductsCVC:  UISearchBarDelegate {
//
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        debugPrint("searchBar:textDidChange: \(searchText)")
//    }
//
//    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        debugPrint("searchBar:shouldChangeTextInRange:replacementText: - range: \(range), text: \(text)")
//        return true
//    }
//
//    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
//        debugPrint("searchBarShouldBeginEditing")
//        return true
//    }
//
//    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        debugPrint("searchBarTextDidBeginEditing")
//    }
//
//    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
//        debugPrint("searchBarShouldEndEditing")
//        return true
//    }
//
//    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//        debugPrint("searchBarTextDidEndEditing")
//    }
//
//    func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
//        debugPrint("searchBarBookmarkButtonClicked")
//    }
//
//    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//        debugPrint("searchBarCancelButtonClicked")
//    }
//
//    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
//        debugPrint("searchBarSearchButtonClicked")
//    }
//
//    func searchBarResultsListButtonClicked(_ searchBar: UISearchBar) {
//        debugPrint("searchBarResultsListButtonClicked")
//    }
//
//    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope_: Int) {
//        debugPrint("searchBar:selectedScopeButtonIndexDidChange: selectedScope: \(selectedScope_)")
//
//        if let searchText = searchBar.text {
//
//            var text: String = ""
//            if searchText.characters.count > 0 {
//                text = searchText
//            }
//
//            self.selectedScope = selectedScope_
//            self.searchResultsVC.selectedScope = selectedScope_
//            self.reloadSearchResults(text: text, searchController: self.searchController)
//        }
//    }
//
//}


