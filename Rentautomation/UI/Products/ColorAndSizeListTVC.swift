//
//  ColorAndSizeListTVC.swift
//  Rentautomation
//
//  Created by kanybek on 5/4/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit

class ColorAndSizeListTVC: UITableViewController, ErrorPopoverRenderer {
    
    var initialItem: String?
    var colorAndSizes: [String] = []
    var disposeSet: DisposableSet?
    
    convenience init() {
        self.init(initialItem: nil)
    }
    
    init(initialItem: String?) {
        self.initialItem = initialItem
        self.disposeSet = DisposableSet()
        if let title_ = self.initialItem {
            if title_ == "Цвета" {
                self.colorAndSizes = ProductMemoryInstance.sharedInstance.colors
            } else if title_ == "Размеры" {
                self.colorAndSizes = ProductMemoryInstance.sharedInstance.productSizes
            }
        }
        super.init(style: .plain)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.disposeSet?.dispose()
        self.disposeSet = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let title_ = self.initialItem {
            self.title = title_
        }
        
        let canceButton = UIBarButtonItem(
            title: "Назад",
            style: .plain,
            target: self,
            action: #selector(cancelObject(_:))
        )
        self.navigationItem.leftBarButtonItem = canceButton
        
        let addButton = UIBarButtonItem(
            title: "Добавить",
            style: .plain,
            target: self,
            action: #selector(createColorAndSize(_:))
        )
        self.navigationItem.rightBarButtonItems = [addButton]
        
        self.tableView.register(SizeAndColorTVCell.self)
        
        let sizeCreateSignal: Signal = PipesStore.shared.sizeAndColorCreatedSignal()
        let createSizeDispose = sizeCreateSignal.start(next: {[weak self] (sizeOrColor: String) in
            self?.colorAndSizes.insert(sizeOrColor, at: 0)
            self?.tableView.beginUpdates()
            self?.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
            self?.tableView.endUpdates()
        })
        self.disposeSet?.add(createSizeDispose)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func createColorAndSize(_ sender: UIBarButtonItem) {
        let createVC = CreateColorAndSizeVC(initialItem: self.initialItem)
        self.navigationController?.pushViewController(createVC, animated: true)
    }
    
    func cancelObject(_ sender: AnyObject) -> Void {
        
        if let title_ = self.initialItem {
            if title_ == "Цвета" {
                ProductMemoryInstance.sharedInstance.colors = self.colorAndSizes
            } else if title_ == "Размеры" {
                ProductMemoryInstance.sharedInstance.productSizes = self.colorAndSizes
            }
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.colorAndSizes.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SizeAndColorTVCell", for: indexPath) as! SizeAndColorTVCell
        let name = colorAndSizes[(indexPath as NSIndexPath).row]
        cell.nameLabel.text = name
        cell.deleteBlock = {[weak self] (Void) -> Void in
            if let indexFound = self?.colorAndSizes.index(where: {$0 == name}) {
                let indexPath_ = IndexPath(row: indexFound, section: 0)
                self?.colorAndSizes.remove(at: indexPath_.row)
                tableView.deleteRows(at: [indexPath_], with: .fade)
            }
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.colorAndSizes.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
        }    
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
