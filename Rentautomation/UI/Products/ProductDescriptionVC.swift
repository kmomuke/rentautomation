//
//  ProductDescriptionVC.swift
//  Rentautomation
//
//  Created by kanybek on 11/29/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftSignalKit
import MZFormSheetPresentationController
import Neon
import TZImagePickerController
import IDMPhotoBrowser

class ProductDescriptionVC: BaseListVC, ErrorPopoverRenderer {

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var barcodeLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var historyProductMovementContainerView: UIView!
    
    private lazy var productMovementsVC: ProdcutMovementsVC = { [weak self] in
        let productMovementsVC_ = ProdcutMovementsVC(initialProduct: self?.initialProduct)
        return productMovementsVC_
    }()
    
    var initialProduct: Product?
    var isFirstTime: Bool = true
    var updateProductDispose: Disposable?
    
    convenience init() {
        self.init(initialProduct: nil)
    }
    
    init(initialProduct: Product?) {
        self.initialProduct = initialProduct
        super.init(nibName: "ProductDescriptionVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    deinit {
        self.updateProductDispose?.dispose()
        self.updateProductDispose = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let editButton = UIBarButtonItem(
            title: "Редактировать",
            style: .plain,
            target: self,
            action: #selector(editProduct(_:))
        )
        self.navigationItem.rightBarButtonItem = editButton

        
        let productUpdateSignal: Signal = PipesStore.shared.productUpdatedSignal()
        self.updateProductDispose = productUpdateSignal.start(next: {[weak self] (product: Product) in
            
            if let currentProduct = self?.initialProduct {
                if currentProduct.productId == product.productId {
                    self?.initialProduct = product
                    self?.updateLabels()
                }
            }
        })
        
        self.updateLabels()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (isFirstTime) {
            self.add(asChildViewController: self.productMovementsVC, frame_: self.historyProductMovementContainerView.frame)
            isFirstTime = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews() {
        productImageView.anchorInCorner(.topLeft, xPad: 5, yPad: 5, width: 150, height: 150)
        historyProductMovementContainerView.align(.underMatchingLeft,
                                                  relativeTo: productImageView,
                                                  padding: 500,
                                                  width: 200,
                                                  height: 200)
    }
    
    func updateUImageView() -> Void {
        if let productImagePath_ = self.initialProduct?.productImagePath {
            ImageCache.default.retrieveImage(forKey: productImagePath_,
                                             options: nil,
                                             completionHandler: {[weak self] (image: UIImage?, type: CacheType) in
                                                if let imageExsist = image {
                                                    self?.productImageView.image = imageExsist
                                                }
            })
        }
    }
    
    func updateLabels() -> Void {
        
        if let product_ = self.initialProduct {
            self.title = product_.productName
            self.barcodeLabel.text = "\(product_.barcode)"
            self.quantityLabel.text = "\(product_.unitsInStock) \(product_.quantityPerUnit)"
            if let unitsInStock_ = Global.shared.amountFormatter.string(for: product_.unitsInStock) {
                self.quantityLabel.text = unitsInStock_ + " \(product_.quantityPerUnit)"
            }
        
            var salePrice = ""
            if let stringSalePrice = Global.shared.priceFormatter.string(for: product_.saleUnitPrice) {
                salePrice = "\(stringSalePrice)"
            }
            
            var incomePrice = ""
            if let stringIncomePrice = Global.shared.priceFormatter.string(for: product_.incomeUnitPrice) {
                incomePrice = "цена закуп: \(stringIncomePrice)"
            }
            
            self.priceLabel.text = "\(salePrice), \(incomePrice)"
            self.nameLabel.text = "\(product_.productName)"
            
            if let url_ = product_.productImagePath {
                let url = URL(string: url_)
                self.productImageView.kf.indicatorType = .activity
                self.productImageView.kf.setImage(with: url,
                                                  placeholder: Global.shared.productPlaceholderImage,
                                                  options: nil,
                                                  progressBlock: nil,
                                                  completionHandler: { (im: Image?, err: NSError?, typr: CacheType, url: URL?) in
                })
            } else {
                self.productImageView.image = Global.shared.productPlaceholderImage
            }
            
            self.productMovementsVC.reloadTableData()
        }
    }
    
    func editProduct(_ sender: AnyObject) -> Void {
        
        let createProductVC = CreateProductVC(initialProduct:self.initialProduct, initialCategoryId: 0)
        createProductVC.dismissBlock = {[weak self] (Void) -> Void in
            self?.updateUImageView()
        }
        
        let navController = UINavigationController(rootViewController: createProductVC)
        
        let formSheetController = MZFormSheetPresentationViewController(contentViewController: navController)
        formSheetController.contentViewControllerTransitionStyle = .slideAndBounceFromBottom
        formSheetController.presentationController?.contentViewSize = CGSize(width: 800, height: 700)
        self.present(formSheetController, animated: true, completion:{_ in
        })
    }
    
    @IBAction func imageIpadDidPressed(_ sender: UIButton) {
        
        if let productImagePath_ = self.initialProduct?.productImagePath {
            ImageCache.default.retrieveImage(forKey: productImagePath_,
                                             options: nil,
                                             completionHandler: {[weak self] (image: UIImage?, type: CacheType) in
                                                if let imageExsist = image {
                                                    var photos: [IDMPhoto] = []
                                                    let photo = IDMPhoto(image: imageExsist)
                                                    photos.append(photo!)
                                                    //let browser = IDMPhotoBrowser.init(photos: photos)
                                                    let browser: IDMPhotoBrowser = IDMPhotoBrowser(photos: photos, animatedFrom: self?.productImageView)
                                                    browser.scaleImage = self?.productImageView.image
                                                    self?.present(browser, animated: true, completion: nil)
                                                }
            })
        }
        
        //let productImVC = ProductImageVC(initialProduct: self.initialProduct)
        //self.navigationController?.pushViewController(productImVC, animated: true)
    }
}
