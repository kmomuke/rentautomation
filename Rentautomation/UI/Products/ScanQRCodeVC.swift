//
//  ScanQRCodeVC.swift
//  Rentautomation
//
//  Created by kanybek on 2/25/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
class ScanQRCodeVC: UIViewController, ErrorPopoverRenderer {
    
    var barcodePickerVC: SBSBarcodePicker
    var scanerScannedBlock : ((String) -> Void)?
    var initialItem: String?
    var isFirstTime: Bool = true
    
    convenience init() {
        self.init(initialItem: nil)
    }
    
    init(initialItem: String?) {
        
        self.initialItem = initialItem
        let settings = SBSScanSettings.default()
        let symbologies: Set<SBSSymbology> = [.EAN8, .EAN13, .QR, .UPC12, .UPCE]
        for symbology in symbologies {
            settings.setSymbology(symbology, enabled: true)
        }
        
        if AppSettings.shared.getShouldUseFrontCamera() {
            settings.cameraFacingPreference = .front
        } else {
            settings.cameraFacingPreference = .back
        }
        
        barcodePickerVC = SBSBarcodePicker(settings:settings)
        //barcodePickerVC.supportsCameraFacing(.back)
        //barcodePickerVC.supportsCameraFacing(.front)
        barcodePickerVC.overlayController.setCameraSwitchVisibility(.always)
//        let cameraSwitchVisibility = SBSCameraSwitchVisibilityAlways
//        
//        [picker.overlayController setCameraSwitchVisibility:cameraSwitchVisibility];
//        [picker.overlayController setCameraSwitchButtonRightMargin:[settings integerForKey:@"cameraSwitchButtonRightMargin"]
//            topMargin:[settings integerForKey:@"cameraSwitchButtonTopMargin"]
//            width:40
//            height:40];
    
    
    
        super.init(nibName: nil, bundle: nil)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let string_ = self.initialItem {
            if string_ == "cameFromIphone" {
                let buton = UIButton(type: .custom)
                buton.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
                buton.setImage(UIImage(named: "cancel-32"), for: .normal)
                buton.addTarget(self, action: #selector(self.cancelPressed(_:)), for: .touchUpInside)
                self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: buton)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (isFirstTime) {
            // Set the delegate to receive scan event callbacks
            self.barcodePickerVC.scanDelegate = self
            self.add(asChildViewController: self.barcodePickerVC, frame_: self.view.bounds)
            isFirstTime = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Set the allowed interface orientations. The value UIInterfaceOrientationMaskAll is the
        // default and is only shown here for completeness.
        self.barcodePickerVC.allowedInterfaceOrientations = .all
        self.barcodePickerVC.startScanning()
    }
    
    func cancelPressed(_ button: UIButton){
        self.dismiss(animated: true) {}
    }
    
    private func setupScanner() {
        // Scandit Barcode Scanner Integration
        // The following method calls illustrate how the Scandit Barcode Scanner can be integrated
        // into your app.
        
        // Create the scan settings and enabling some symbologies
        let settings = SBSScanSettings.default()
        let symbologies: Set<SBSSymbology> = [.EAN8, .EAN13, .QR, .UPC12, .UPCE]
        for symbology in symbologies {
            settings.setSymbology(symbology, enabled: true)
        }
        
        // Create the barcode picker with the settings just created
        let barcodePicker = SBSBarcodePicker(settings:settings)
        
        // Add the barcode picker as a child view controller
        addChildViewController(barcodePicker)
        view.addSubview(barcodePicker.view)
        barcodePicker.didMove(toParentViewController: self)
        
        // Set the allowed interface orientations. The value UIInterfaceOrientationMaskAll is the
        // default and is only shown here for completeness.
        barcodePicker.allowedInterfaceOrientations = .all
        // Set the delegate to receive scan event callbacks
        barcodePicker.scanDelegate = self
        barcodePicker.startScanning()
    }
}

extension ScanQRCodeVC: SBSScanDelegate {
    // This delegate method of the SBSScanDelegate protocol needs to be implemented by
    // every app that uses the Scandit Barcode Scanner and this is where the custom application logic
    // goes. In the example below, we are just showing an alert view with the result.
    func barcodePicker(_ picker: SBSBarcodePicker, didScan session: SBSScanSession) {
        
        // Call pauseScanning on the session (and on the session queue) to immediately pause scanning
        // and close the camera. This is the preferred way to pause scanning barcodes from the
        // SBSScanDelegate as it is made sure that no new codes are scanned.
        // When calling pauseScanning on the picker, another code may be scanned before pauseScanning
        // has completely paused the scanning process.
        
        
        
        session.pauseScanning()
        
        
        let code = session.newlyRecognizedCodes[0]
        // The barcodePicker(_:didScan:) delegate method is invoked from a picker-internal queue. To
        // display the results in the UI, you need to dispatch to the main queue. Note that it's not
        // allowed to use SBSScanSession in the dispatched block as it's only allowed to access the
        // SBSScanSession inside the barcodePicker(_:didScan:) callback. It is however safe to
        // use results returned by session.newlyRecognizedCodes etc.
        DispatchQueue.main.async {
            
            let delay = DispatchTime.now() + DispatchTimeInterval.milliseconds(700)
            DispatchQueue.main.asyncAfter(deadline: delay, execute: {
                picker.resumeScanning()
            })
            
            print("code.symbologyString == \(code.symbologyString)")
            
            if let scanerBlock = self.scanerScannedBlock, let codeDate = code.data {
                
                ProductMemoryInstance.sharedInstance.barcode = codeDate
                scanerBlock(codeDate)
                let _ = self.navigationController?.popViewController(animated: true)
            }
        }
    }
}
