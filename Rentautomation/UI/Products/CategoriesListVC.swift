//
//  CategoriesListVC.swift
//  Rentautomation
//
//  Created by kanybek on 11/29/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit
import MZFormSheetPresentationController

class CategoriesListVC: BaseListVC, ErrorPopoverRenderer, UITableViewDelegate, UITableViewDataSource {
    
    private var searchController: UISearchController!
    private var searchResultsVC: CategorySearchResultsTVC!
    
    var categorySelectBlock : ((Category) -> Void)?
    
    var tableView: UITableView
    var categories: [Category] = []
    
    var disposeSet: DisposableSet?
    
    var initialString: String?
    var isCommingFromProduct: Bool = false
    
    convenience init() {
        self.init(initialString: nil, isCommingFromProduct: false)
    }
    
    init(initialString: String?, isCommingFromProduct: Bool) {
        self.initialString = initialString
        self.isCommingFromProduct = isCommingFromProduct
        self.disposeSet = DisposableSet()
        self.tableView = UITableView(frame: CGRect.zero, style: .plain)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.searchController?.view.removeFromSuperview()
        self.disposeSet?.dispose()
        self.disposeSet = nil
    }
    
    override func loadView() {
        super.loadView()
        
        if !self.isCommingFromProduct {
            self.addCanceButton()
        }
        
        self.title = "Все категории"
        
        self.tableView.register(CategoriesListTVCell.self)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.view.addSubview(self.tableView)
        
        var shouldAddBarButton: Bool = true
        if let initialString_ = self.initialString {
            if initialString_ == "comeFromSaleOrder" {
                shouldAddBarButton = false
            }
        }
        
        if shouldAddBarButton {
            let addButton = UIBarButtonItem(
                title: "Добавить",
                style: .plain,
                target: self,
                action: #selector(createCategory(_:))
            )
            
            self.navigationItem.rightBarButtonItem = addButton
        }
        
        self.searchResultsVC = CategorySearchResultsTVC()
        self.searchResultsVC.categorySelectBlock = {[weak self] (category: Category) -> Void in
            if let strongSelf = self {
                strongSelf.didSelectCategory(category)
            }
        }
        
        self.searchController = UISearchController(searchResultsController: self.searchResultsVC)
        self.searchController.searchResultsUpdater = self
        self.searchController.searchBar.sizeToFit()
        self.tableView.tableHeaderView = searchController.searchBar
        
        self.searchController.delegate = self
        self.searchController.dimsBackgroundDuringPresentation = false // default is YES
        self.searchController.searchBar.delegate = self    // so we can monitor text changes + others
        
        self.searchController.searchBar.barStyle = .default
        self.searchController.searchBar.placeholder = "Search"
        self.searchController.searchBar.backgroundColor = UIColor.white
        
        /*
         Search is now just presenting a view controller. As such, normal view controller
         presentation semantics apply. Namely that presentation will walk up the view controller
         hierarchy until it finds the root view controller or one that defines a presentation context.
         */
        definesPresentationContext = true
        
        let categoryCreateSignal: Signal = PipesStore.shared.categoryCreatedSignal()
        let createCategoryDispose = categoryCreateSignal.start(next: {[weak self] (product: Category) in
            self?.categories.insert(product, at: 0)
            self?.tableView.beginUpdates()
            self?.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
            self?.tableView.endUpdates()
        })
        self.disposeSet?.add(createCategoryDispose)
        
        let categoryUpdateSignal: Signal = PipesStore.shared.categoryUpdatedSignal()
        let updateCategoryDispose = categoryUpdateSignal.start(next: {[weak self] (category: Category) in
            if let i = self?.categories.index(where: {$0.categoryId == category.categoryId}) {
                self?.categories[i] = category
                self?.tableView.reloadRows(at: [IndexPath(row: i, section: 0)], with: .fade)
            }
        })
        self.disposeSet?.add(updateCategoryDispose)
        
        let allSyncFinishedSignal: Signal = PipesStore.shared.allSyncOperationsFinishedSignal()
        let allSyncFinishedDispose = allSyncFinishedSignal.start(next: {[weak self] (finished: Bool) in
            self?.reloadAllCategories()
        })
        self.disposeSet?.add(allSyncFinishedDispose)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.reloadAllCategories()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = self.view.bounds
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func reloadAllCategories() {
        self.categories = Database.shared.loadAllCategories()
        self.tableView.reloadData()
    }
    
    func createCategory(_ sender: AnyObject) {
        
        let createCategoryVC = CreateCategoryVC(initialCategory: nil, isCommingFromProduct: self.isCommingFromProduct)
        
        if isCommingFromProduct {
            self.navigationController?.pushViewController(createCategoryVC, animated: true)
        } else {
            let navController = UINavigationController(rootViewController: createCategoryVC)
            
            let formSheetController = MZFormSheetPresentationViewController(contentViewController: navController)
            formSheetController.contentViewControllerTransitionStyle = .slideAndBounceFromBottom
            formSheetController.presentationController?.contentViewSize = CGSize(width: 800, height: 700)
            self.present(formSheetController, animated: true, completion:{_ in
            })
        }
    }
    
    func dismissCategory(_ sender: AnyObject) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    // MARK: TableView DataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoriesListTVCell", for: indexPath) as! CategoriesListTVCell
        cell.category = categories[(indexPath as NSIndexPath).row]
        cell.reloadData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            categories.remove(at: (indexPath as NSIndexPath).row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        let category = categories[(indexPath as NSIndexPath).row]
        self.didSelectCategory(category)
    }
    
    private func didSelectCategory(_ category: Category) {
        
        if Database.shared.isModelSynchronized(primaryKey: category.categoryId) {
            
            if let initialString_ = self.initialString {
                if initialString_ == "comeFromSaleOrder" {
                    if let categorySelectBlock_ = self.categorySelectBlock {
                        categorySelectBlock_(category)
                        self.dismiss(animated: true, completion: {})
                    }
                    return
                }
            }
            
            if isCommingFromProduct {
                ProductMemoryInstance.sharedInstance.categoryId = category.categoryId
                let _ = self.navigationController?.popViewController(animated: true)
            } else {
                let prodcutListVC = ProductsListVC(initialString: nil, initialCategoryId: category.categoryId)
                self.navigationController?.pushViewController(prodcutListVC, animated: true)
            }
            
        } else {
            let _ = self.showErrorAlert(text: "не сохранен")
        }
    }
    
}

extension CategoriesListVC: UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating {
    
    //----------
    func presentSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    func willPresentSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    func didPresentSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    func willDismissSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    func didDismissSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    // MARK: UISearchResultsUpdating
    func updateSearchResults(for searchController: UISearchController) {
        print("updateSearchResultsForSearchController")
        if let searchText = searchController.searchBar.text {
            if searchText.characters.count > 0 {
                
                self.showLoading("Поиск")
                Database.shared.searchCategoryBy(name: searchText, completion: {[weak self] (foundCategories: [Category]) in
                    self?.dismissLoading()
                    let resultsController = searchController.searchResultsController as! CategorySearchResultsTVC
                    resultsController.filteredCategories = foundCategories
                    resultsController.tableView.reloadData()
                })
            }
        }
    }
    
}
