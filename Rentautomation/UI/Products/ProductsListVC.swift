//
//  ProductsListVC.swift
//  Rentautomation
//
//  Created by kanybek on 11/29/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit
import MZFormSheetPresentationController

class ProductsListVC: BaseListVC, ErrorPopoverRenderer, UITableViewDelegate, UITableViewDataSource {

    private var searchController: UISearchController!
    private var searchResultsVC: ProductResultsTVC!
    
    var disposeSet: DisposableSet?
    
    var tableView: UITableView
    var products: [Product] = []
    
    var initialString: String?
    var initialCategoryId: UInt64 = 0
    
    convenience init() {
        self.init(initialString: nil, initialCategoryId: 0)
    }
    
    init(initialString: String?, initialCategoryId: UInt64) {
        self.initialString = initialString
        self.initialCategoryId = initialCategoryId
        self.tableView = UITableView(frame: CGRect.zero, style: .plain)
        self.disposeSet = DisposableSet()
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.searchController?.view.removeFromSuperview()
        
        self.disposeSet?.dispose()
        self.disposeSet = nil
    }
    
    override func loadView() {
        super.loadView()
        
        if let category = Database.shared.categoryBy(categoryId: self.initialCategoryId) {
            self.title = "\(category.categoryName)"
        } else {
            self.title = "Товары"
            self.addCanceButton()
        }
        
        self.tableView.register(ProductsListTVCell.self)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.view.addSubview(self.tableView)
    
        let addButton = UIBarButtonItem(
            title: "Добавить",
            style: .plain,
            target: self,
            action: #selector(createProduct(_:))
        )
        
        if (self.initialCategoryId > 0){
            
            let editButton = UIBarButtonItem(
                title: "Категория",
                style: .plain,
                target: self,
                action: #selector(editCategory(_:))
            )
    
            self.navigationItem.rightBarButtonItems = [addButton, editButton]
        } else {
            self.navigationItem.rightBarButtonItem = addButton
        }

        self.searchResultsVC = ProductResultsTVC(initalString: "boke")
        self.searchResultsVC.productSelectBlock = {[weak self]  (product: Product) -> Void in
            if let strongSelf = self {
                strongSelf.didSelectProduct(product)
            }
        }
        
        self.searchController = UISearchController(searchResultsController: self.searchResultsVC)
        self.searchController.searchResultsUpdater = self
        self.searchController.searchBar.sizeToFit()
        self.tableView.tableHeaderView = searchController.searchBar
        
        self.searchController.delegate = self
        self.searchController.dimsBackgroundDuringPresentation = false // default is YES
        self.searchController.searchBar.delegate = self    // so we can monitor text changes + others
        
        self.searchController.searchBar.barStyle = .default
        self.searchController.searchBar.placeholder = "Search"
        self.searchController.searchBar.backgroundColor = UIColor.white
        
        /*
         Search is now just presenting a view controller. As such, normal view controller
         presentation semantics apply. Namely that presentation will walk up the view controller
         hierarchy until it finds the root view controller or one that defines a presentation context.
         */
        definesPresentationContext = true
        //self.searchController.hidesNavigationBarDuringPresentation = false
        
        let productCreateSignal: Signal = PipesStore.shared.productCreatedSignal()
        let createProductDispose = productCreateSignal.start(next: {[weak self] (product: Product) in
            self?.products.insert(product, at: 0)
            self?.tableView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
            self?.tableView.beginUpdates()
            self?.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
            self?.tableView.endUpdates()
        })
        self.disposeSet?.add(createProductDispose)
        
        let productUpdateSignal: Signal = PipesStore.shared.productUpdatedSignal()
        let updateProductDispose = productUpdateSignal.start(next: {[weak self] (product: Product) in
            if let i = self?.products.index(where: {$0.productId == product.productId}) {
                self?.products[i] = product
                self?.tableView.reloadRows(at: [IndexPath(row: i, section: 0)], with: .fade)
            }
        })
        self.disposeSet?.add(updateProductDispose)
        
        let categoryUpdateSignal: Signal = PipesStore.shared.categoryUpdatedSignal()
        let updateCategoryDispose = categoryUpdateSignal.start(next: {[weak self] (category: Category) in
            self?.title = "Товары \(category.categoryName)"
        })
        self.disposeSet?.add(updateCategoryDispose)
        
        let allSyncFinishedSignal: Signal = PipesStore.shared.allSyncOperationsFinishedSignal()
        let allSyncFinishedDispose = allSyncFinishedSignal.start(next: {[weak self] (finished: Bool) in
            self?.reloadAllProducts()
        })
        self.disposeSet?.add(allSyncFinishedDispose)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.reloadAllProducts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = self.view.bounds
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func reloadAllProducts() {
        Database.shared.loadAllProductsBy(categoryId: self.initialCategoryId,
                                          limit: Global.shared.limitForStaticModels) {[weak self] (products_: [Product]) in
                                            self?.products = products_
                                            self?.tableView.reloadData()
        }
    }
    
    func editCategory(_ sender: AnyObject) {
        
        if let category = Database.shared.categoryBy(categoryId: self.initialCategoryId) {
            let createProductVC = CreateCategoryVC(initialCategory: category, isCommingFromProduct: false)
            let navController = UINavigationController(rootViewController: createProductVC)
            let formSheetController = MZFormSheetPresentationViewController(contentViewController: navController)
            formSheetController.contentViewControllerTransitionStyle = .slideAndBounceFromBottom
            formSheetController.presentationController?.contentViewSize = CGSize(width: 800, height: 700)
            self.present(formSheetController, animated: true, completion:{_ in
            })
        }
    }
    
    func createProduct(_ sender: AnyObject) {
        
        let createProductVC = CreateProductVC(initialProduct:nil, initialCategoryId: self.initialCategoryId)
        
        let navController = UINavigationController(rootViewController: createProductVC)

        let formSheetController = MZFormSheetPresentationViewController(contentViewController: navController)
        formSheetController.contentViewControllerTransitionStyle = .slideAndBounceFromBottom
        formSheetController.presentationController?.contentViewSize = CGSize(width: 800, height: 700)
        self.present(formSheetController, animated: true, completion:{_ in
        })
    }
    
    // MARK: TableView DataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductsListTVCell", for: indexPath) as! ProductsListTVCell

        cell.product = products[(indexPath as NSIndexPath).row]
        cell.reloadData()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            products.remove(at: (indexPath as NSIndexPath).row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let object = self.products[(indexPath as NSIndexPath).row]
        self.didSelectProduct(object)
    }
    
    func didSelectProduct(_ product: Product) {
        if Database.shared.isModelSynchronized(primaryKey: product.productId) {
            let detailViewController = ProductDescriptionVC(initialProduct: product)
            self.navigationController?.pushViewController(detailViewController, animated: true)
        } else {
            let _ = self.showErrorAlert(text: "Не сохранен")
        }
    }
    
    static func screenSize() -> CGSize {
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        print("screenWidth ==> \(screenWidth) +++ screenHeight===> \(screenHeight)")
        return CGSize(width: screenWidth, height: screenHeight)
    }
}


extension ProductsListVC:  UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating {
    //----------
    func presentSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    func willPresentSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    func didPresentSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    func willDismissSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    func didDismissSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    // MARK: UISearchResultsUpdating
    func updateSearchResults(for searchController: UISearchController) {
        print("updateSearchResultsForSearchController")
        if let searchText = searchController.searchBar.text {
            if searchText.characters.count > 0 {
                self.showLoading("Поиск")
                Database.shared.search(productName: searchText, completion: { (products_: [Product]) in
                    self.dismissLoading()
                    let resultsController = searchController.searchResultsController as! ProductResultsTVC
                    resultsController.filteredProducts = products_
                    resultsController.tableView.reloadData()
                })
            }
        }
    }
}



