//
//  ProductResultsTVC.swift
//  Rentautomation
//
//  Created by kanybek on 1/18/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class ProductResultsTVC: UITableViewController {

    var productSelectBlock : ((Product) -> Void)?
    
    var initalString: String?
    
    convenience init() {
        self.init(initalString: "koke")
    }
    
    init(initalString: String) {
        self.initalString = initalString
        super.init(style: .plain)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    var filteredProducts = [Product]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "ProductsListTVCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "ProductsListTVCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredProducts.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductsListTVCell", for: indexPath) as! ProductsListTVCell
        cell.product = filteredProducts[(indexPath as NSIndexPath).row]
        cell.reloadData()
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let product = filteredProducts[(indexPath as NSIndexPath).row]
        if let productBlock_ = self.productSelectBlock {
            productBlock_(product)
        }
    }
}
