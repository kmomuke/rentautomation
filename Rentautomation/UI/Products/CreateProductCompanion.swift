//
//  CreateProductCompanion.swift
//  Rentautomation
//
//  Created by kanybek on 5/5/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import Kingfisher

final class CreateProductCompanion {
    
    var initialItem: String?
    
    convenience init() {
        self.init(initialItem: nil)
    }
    
    init(initialItem: String?) {
        self.initialItem = initialItem
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("MainDocumentCompanion deleted")
    }
    
    func createProducts(productName_: String,
                        saleUnitPrice_: Double,
                        unitsInStock_: Double,
                        incomeUnitPrice_: Double,
                        quantityPerUnit_: String,
                        categoryId_: UInt64,
                        supplierId_: UInt64,
                        barcode__: String?,
                        productImage__: UIImage?,
                        colors_: [String],
                        sizes_: [String],
                        completion:@escaping (Bool) -> ()) {
        
        var productId = Database.shared.uniqueIncreasingPrimaryKey()
        
        if colors_.count > 0 && sizes_.count > 0 {
            
            for color in colors_ {
                for size in sizes_ {
                    self.createOneProduct_(productId_: productId,
                                           productName_: productName_ + " " + color + " " + size,
                                           saleUnitPrice_: saleUnitPrice_,
                                           unitsInStock_: unitsInStock_,
                                           incomeUnitPrice_: incomeUnitPrice_,
                                           quantityPerUnit_: quantityPerUnit_,
                                           categoryId_: categoryId_,
                                           supplierId_: supplierId_,
                                           barcode__: barcode__,
                                           productImage__: productImage__)
                    productId = productId + 1
                }
            }
            
        } else if colors_.count > 0 && sizes_.count == 0 {
            
            for color in colors_ {
                self.createOneProduct_(productId_: productId,
                                       productName_: productName_ + " " + color,
                                       saleUnitPrice_: saleUnitPrice_,
                                       unitsInStock_: unitsInStock_,
                                       incomeUnitPrice_: incomeUnitPrice_,
                                       quantityPerUnit_: quantityPerUnit_,
                                       categoryId_: categoryId_,
                                       supplierId_: supplierId_,
                                       barcode__: barcode__,
                                       productImage__: productImage__)
                productId = productId + 1
            }
            
        } else if colors_.count == 0 && sizes_.count > 0 {
            
            for size in sizes_ {
                self.createOneProduct_(productId_: productId,
                                       productName_: productName_ + " " + size,
                                       saleUnitPrice_: saleUnitPrice_,
                                       unitsInStock_: unitsInStock_,
                                       incomeUnitPrice_: incomeUnitPrice_,
                                       quantityPerUnit_: quantityPerUnit_,
                                       categoryId_: categoryId_,
                                       supplierId_: supplierId_,
                                       barcode__: barcode__,
                                       productImage__: productImage__)
                productId = productId + 1
            }
            
        } else if colors_.count == 0 && sizes_.count == 0 {
            self.createOneProduct_(productId_: productId,
                                   productName_: productName_,
                                   saleUnitPrice_: saleUnitPrice_,
                                   unitsInStock_: unitsInStock_,
                                   incomeUnitPrice_: incomeUnitPrice_,
                                   quantityPerUnit_: quantityPerUnit_,
                                   categoryId_: categoryId_,
                                   supplierId_: supplierId_,
                                   barcode__: barcode__,
                                   productImage__: productImage__)
            productId = productId + 1
        }
        
        completion(true)
    }
    
    func createOneProduct_(productId_: UInt64,
                           productName_: String,
                           saleUnitPrice_: Double,
                           unitsInStock_: Double,
                           incomeUnitPrice_: Double,
                           quantityPerUnit_: String,
                           categoryId_: UInt64,
                           supplierId_: UInt64,
                           barcode__: String?,
                           productImage__: UIImage?) {
        
        let creatingProduct = Product()
        creatingProduct.productId = productId_ 
        
        creatingProduct.productName = productName_
        creatingProduct.saleUnitPrice = saleUnitPrice_
        creatingProduct.unitsInStock = unitsInStock_
        creatingProduct.incomeUnitPrice = incomeUnitPrice_
        creatingProduct.quantityPerUnit = quantityPerUnit_
        
        creatingProduct.categoryId = categoryId_
        creatingProduct.supplierId = supplierId_
        
        if let productImage_ = productImage__ {
            let uuid = NSUUID().uuidString.lowercased()
            ImageCache.default.store(productImage_, forKey: uuid)
            creatingProduct.productImagePath = uuid
        }
        
        if let barcode_ = barcode__ {
            creatingProduct.barcode = barcode_
        }
        
        let orderDetail = OrderDetail()
        orderDetail.orderDetailId = productId_ + 11
        orderDetail.orderDetailDate = Date()
        orderDetail.isLast = true
        orderDetail.productId = creatingProduct.productId
        orderDetail.orderQuantity = ProductMemoryInstance.sharedInstance.unitsInStock
        orderDetail.price = ProductMemoryInstance.sharedInstance.saleUnitPrice
        orderDetail.productQuantity = creatingProduct.unitsInStock
        orderDetail.orderId = 0
        orderDetail.billingNo = "\(Date().toString(.custom("EEE, dd MMM HH:mm"))) "
        
        Database.shared.createorderDetails([orderDetail])
        
        
        let operation = Event(syncOperationId: Database.shared.operationCid(),
                                      isSynchronized: false,
                                      syncOperationDate: Date(),
                                      operationType: .productCreated,
                                      productId: creatingProduct.productId,
                                      orderDetailId: orderDetail.orderDetailId,
                                      customerId: 0,
                                      transactionId: 0,
                                      accountId: 0,
                                      supplierId: 0,
                                      staffId: 0,
                                      categoryId: 0,
                                      orderId: 0)
        
        Database.shared.createOperations([operation])
        
        Database.shared.createProducts(products: [creatingProduct])
        PipesStore.shared.productCreatedPipe.putNext(creatingProduct)
    }
}
