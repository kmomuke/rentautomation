//
//  CreateCategoryVC.swift
//  Rentautomation
//
//  Created by kanybek on 11/29/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

class CreateCategoryVC: FormViewController, ErrorPopoverRenderer {
    
    var initialCategory: Category?
    var isCommingFromProduct: Bool = false
    
    convenience init() {
        self.init(initialCategory: nil, isCommingFromProduct: false)
    }
    
    init(initialCategory: Category?, isCommingFromProduct: Bool) {
        self.initialCategory = initialCategory
        self.isCommingFromProduct = isCommingFromProduct
        super.init(nibName: nil, bundle: nil)
        self.setUpInitialRows()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    deinit {
        print("CreateCategoryVC deallocated")
    }
    
    private func setUpInitialRows () -> Void {
        if let category = self.initialCategory {
            CategoryMemoryInstance.sharedInstance.categoryName = category.categoryName
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure()
        
        self.title = "Создать/редактировать категорию"
        
        let canceButton = UIBarButtonItem(
            title: "Закрыть",
            style: .plain,
            target: self,
            action: #selector(cancelObject(_:))
        )
        
        self.navigationItem.leftBarButtonItem = canceButton
        
        let createButton = UIBarButtonItem(
            title: "Сохранить",
            style: .plain,
            target: self,
            action: #selector(createCategory(_:))
        )
        
        self.navigationItem.rightBarButtonItem = createButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.categoryNameRow.former?.becomeEditingNext()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func cancelObject(_ sender: AnyObject) -> Void {
        CategoryMemoryInstance.sharedInstance.clearAllVariables()
        if self.isCommingFromProduct {
            let _ = self.navigationController?.popViewController(animated: true)
        } else {
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK: CREATE
    func createCategory(_ sender: AnyObject) -> Bool {
        
        let creatingCategory: Category
        var isUpdatingCategory: Bool = false
        if let category_ = self.initialCategory {
            creatingCategory = category_
            isUpdatingCategory = true
        } else {
            creatingCategory = Category()
            creatingCategory.categoryId = Database.shared.uniqueIncreasingPrimaryKey()
        }
        
        if let categoryName_ = CategoryMemoryInstance.sharedInstance.categoryName {
            creatingCategory.categoryName = categoryName_
            if categoryName_.characters.count < 2 {
                return showErrorAlert(text: "Наименование")
            }
        } else {
            return showErrorAlert(text: "Наименование")
        }
        
        if isUpdatingCategory {
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                          isSynchronized: false,
                                          syncOperationDate: Date(),
                                          operationType: .categoryUpdated,
                                          productId: 0,
                                          orderDetailId: 0,
                                          customerId: 0,
                                          transactionId: 0,
                                          accountId: 0,
                                          supplierId: 0,
                                          staffId: 0,
                                          categoryId: creatingCategory.categoryId,
                                          orderId: 0)
            Database.shared.createOperations([operation])
            
            Database.shared.updateCategory(creatingCategory)
            PipesStore.shared.categoryUpdatedPipe.putNext(creatingCategory)
        } else {
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                          isSynchronized: false,
                                          syncOperationDate: Date(),
                                          operationType: .categoryCreated,
                                          productId: 0,
                                          orderDetailId: 0,
                                          customerId: 0,
                                          transactionId: 0,
                                          accountId: 0,
                                          supplierId: 0,
                                          staffId: 0,
                                          categoryId: creatingCategory.categoryId,
                                          orderId: 0)
            Database.shared.createOperations([operation])
            
            Database.shared.createCategories([creatingCategory])
            PipesStore.shared.categoryCreatedPipe.putNext(creatingCategory)
        }
        
        print("____RICHED_END____")
        CategoryMemoryInstance.sharedInstance.clearAllVariables()
        if self.isCommingFromProduct {
            let _ = self.navigationController?.popViewController(animated: true)
        } else {
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
        
        return true
    }
        
    // MARK: Private
    private lazy var formerInputAccessoryView: FormerInputAccessoryView = FormerInputAccessoryView(former: self.former)
    
  
    // MARK: Optional product section
    private lazy var categoryNameRow: TextFieldRowFormer<ProfileFieldCell> = {
        TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            
            $0.titleLabel.text = "Наименование"
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            //$0.textField.keyboardType = .numberPad
            }.configure {
                $0.placeholder = "Наименование категории"
                $0.text = ""
                if let category_ = self.initialCategory {
                    $0.text = category_.categoryName
                }
            }.onTextChanged {
                CategoryMemoryInstance.sharedInstance.categoryName = $0
        }
    }()

    
    private lazy var nameCategorySection: SectionFormer = { [weak self] in
        let section = SectionFormer(rowFormer: (self?.categoryNameRow)!)
        section.set(headerViewFormer: self?.createHeaderFunc(s: "Наименование категории"))
        return section
    }()
    
    private func createHeaderFunc(s: String) -> ViewFormer {
        return LabelViewFormer<FormLabelHeaderView>()
            .configure {
                $0.viewHeight = 40
                $0.text = s
        }
    }
    
    private func configure() {
        
        tableView.contentInset.top = 40
        tableView.contentInset.bottom = 40
        
        former.append(sectionFormer:self.nameCategorySection)
            .onCellSelected { [weak self] _ in
                self?.formerInputAccessoryView.update()
        }
    }
}
