//
//  CreateColorAndSizeVC.swift
//  Rentautomation
//
//  Created by kanybek on 5/4/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class CreateColorAndSizeVC: FormViewController, ErrorPopoverRenderer {
    
    var initialItem: String?
    var sizeAndColorName: String
    
    convenience init() {
        self.init(initialItem: nil)
    }
    
    init(initialItem: String?) {
        self.initialItem = initialItem
        self.sizeAndColorName = ""
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure()
        
        self.title = "Добавить" + " в " + self.initialItem!
        
        let canceButton = UIBarButtonItem(
            title: "Закрыть",
            style: .plain,
            target: self,
            action: #selector(cancelObject(_:))
        )
        self.navigationItem.leftBarButtonItem = canceButton
        
        let createButton = UIBarButtonItem(
            title: "Сохранить",
            style: .plain,
            target: self,
            action: #selector(createCategory(_:))
        )
        
        self.navigationItem.rightBarButtonItem = createButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.categoryNameRow.former?.becomeEditingNext()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func cancelObject(_ sender: AnyObject) -> Void {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: CREATE
    func createCategory(_ sender: AnyObject) -> Bool {
        if self.sizeAndColorName.characters.count > 0 {
            PipesStore.shared.sizeAndColorCreatedPipe.putNext(self.sizeAndColorName)
            self.navigationController?.popViewController(animated: true)
            return true
        }
        return false
    }
    
    // MARK: Private
    private lazy var formerInputAccessoryView: FormerInputAccessoryView = FormerInputAccessoryView(former: self.former)
    
    // MARK: Optional product section
    private lazy var categoryNameRow: TextFieldRowFormer<ProfileFieldCell> = {
        TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            
            $0.titleLabel.text = "Наим."
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            
            }.configure {
                $0.placeholder = "Наименование"
                $0.text = self.sizeAndColorName
            }.onTextChanged {
                self.sizeAndColorName = $0
        }
    }()
    
    private lazy var nameCategorySection: SectionFormer = { [weak self] in
        let section = SectionFormer(rowFormer: (self?.categoryNameRow)!)
        section.set(headerViewFormer: self?.createHeaderFunc(s: ""))
        return section
    }()
    
    private func createHeaderFunc(s: String) -> ViewFormer {
        return LabelViewFormer<FormLabelHeaderView>()
            .configure {
                $0.viewHeight = 40
                $0.text = s
        }
    }
    
    private func configure() {
        
        tableView.contentInset.top = 40
        tableView.contentInset.bottom = 40
        
        former.append(sectionFormer:self.nameCategorySection)
            .onCellSelected { [weak self] _ in
                self?.formerInputAccessoryView.update()
        }
    }
}
