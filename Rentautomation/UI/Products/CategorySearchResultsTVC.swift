//
//  CategorySearchResultsTVC.swift
//  Rentautomation
//
//  Created by kanybek on 2/3/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class CategorySearchResultsTVC: UITableViewController {

    var categorySelectBlock : ((Category) -> Void)?
    
    var initalString: String?
    
    convenience init() {
        self.init(initalString: "koke")
    }
    
    init(initalString: String) {
        self.initalString = initalString
        super.init(style: .plain)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    var filteredCategories = [Category]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(CategoriesListTVCell.self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredCategories.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoriesListTVCell", for: indexPath) as! CategoriesListTVCell
        cell.category = filteredCategories[(indexPath as NSIndexPath).row]
        cell.reloadData()
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let category_ = filteredCategories[(indexPath as NSIndexPath).row]
        if let categorySelectBlock_ = self.categorySelectBlock {
            categorySelectBlock_(category_)
        }
    }

}
