//
//  OrderDetailHistoryTVCell.swift
//  Rentautomation
//
//  Created by kanybek on 12/29/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

class OrderDetailHistoryTVCell: UITableViewCell {

    var orderDetail: OrderDetail?
    
    @IBOutlet weak var orderDetailPrimaryKeyLabel: UILabel!
    @IBOutlet weak var numberOrderDetalLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var nameOrderDetalLabel: UILabel!
    @IBOutlet weak var priceOrderDetalLabel: UILabel!
    @IBOutlet weak var quantityOrderDetalLabel: UILabel!
    @IBOutlet weak var totalOrderDetalLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func reloadData() -> Void {
        
        if let orderDetail_ = self.orderDetail {
            
            if Global.shared.isDebug {
                self.orderDetailPrimaryKeyLabel.text = "\(orderDetail_.orderDetailId)"
                self.orderDetailPrimaryKeyLabel.backgroundColor = UIColor.clear
            } else {
                self.orderDetailPrimaryKeyLabel.isHidden = true
            }
            
            self.numberOrderDetalLabel.text = "\(orderDetail_.orderDetailId)"
            self.dateLabel.text = "\(orderDetail_.orderDetailDate.toString(.custom("EEE, dd MMM HH:mm:ss")))"
            
            var name: String = "Не определено"
            var color: UIColor = UIColor.groupTableViewBackground
            
            if let order = Database.shared.orderBy(orderId: orderDetail_.orderId) {
                color = UIColor.groupTableViewBackground
                name = Order.orderDocumentTypeStringName(orderDocType: order.orderDocument)
                if order.isEdited {
                    name = "Документ откатан. " + name
                    self.nameOrderDetalLabel.textColor = UIColor.red
                } else {
                    name = "" + name
                    self.nameOrderDetalLabel.textColor = Order.orderDocumentTypeColor(orderDocType: order.orderDocument)
                }
                
            } else {
                color = UIColor.sunflowerColor()
                if orderDetail_.orderId == 0 {
                    name = "Владелец редактировал"
                } else {
                    name = "Не определено пока"
                }
                self.nameOrderDetalLabel.textColor = UIColor.red
            }
            
            if orderDetail_.isLast {
                self.nameOrderDetalLabel.backgroundColor = UIColor.greenSeaColor()
            } else {
                self.nameOrderDetalLabel.backgroundColor = color
            }
            
            var quantityPerUnit: String = ""
            if let product = Database.shared.productFor(productId: orderDetail_.productId) {
                quantityPerUnit = product.quantityPerUnit
            }
            
            self.nameOrderDetalLabel.text = "\(name)"
            self.priceOrderDetalLabel.text = Global.shared.priceFormatter.string(for: orderDetail_.price)
            
            let text_ = Global.shared.amountFormatter.string(for: orderDetail_.orderQuantity)!
            self.quantityOrderDetalLabel.text = "\(text_) \(quantityPerUnit)"
            
            let totalPrice = orderDetail_.price * orderDetail_.orderQuantity
            self.totalOrderDetalLabel.text = Global.shared.priceFormatter.string(for: totalPrice)
            
            if Database.shared.isModelSynchronized(primaryKey: orderDetail_.orderDetailId) {
                self.numberOrderDetalLabel.textColor = UIColor.black
                self.numberOrderDetalLabel.backgroundColor = UIColor.groupTableViewBackground
            } else {
                self.numberOrderDetalLabel.textColor = UIColor.white
                self.numberOrderDetalLabel.backgroundColor = UIColor.pomergranateColor()
            }
            
        } else {
            self.resetLabels()
        }
    }
    
    override func prepareForReuse() {
        self.orderDetail = nil
        self.resetLabels()
    }
    
    func resetLabels() -> Void {
        self.numberOrderDetalLabel.text = ""
        self.nameOrderDetalLabel.text = ""
        self.priceOrderDetalLabel.text = ""
        self.quantityOrderDetalLabel.text = ""
        self.totalOrderDetalLabel.text = ""
        self.orderDetailPrimaryKeyLabel.text = ""
    }    
}
