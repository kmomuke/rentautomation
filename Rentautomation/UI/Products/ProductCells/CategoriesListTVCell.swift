//
//  CategoriesListTVCell.swift
//  Rentautomation
//
//  Created by kanybek on 12/3/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

class CategoriesListTVCell: UITableViewCell {

    var category: Category?
    
    @IBOutlet weak var categoryPrimaryKeyLabel: UILabel!
    @IBOutlet weak var categoryNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func reloadData() -> Void {
        if let category_ = self.category {
            
            if Global.shared.isDebug {
                self.categoryPrimaryKeyLabel.text = "\(category_.categoryId)"
                self.categoryPrimaryKeyLabel.backgroundColor = UIColor.clear
            } else {
                self.categoryPrimaryKeyLabel.isHidden = true
            }
            
            self.categoryNameLabel.text = category_.categoryName
            
            if Database.shared.isModelSynchronized(primaryKey: category_.categoryId) {
                self.selectionStyle = .default
                self.categoryNameLabel.textColor = UIColor.black
            } else {
                self.selectionStyle = .none
                self.categoryNameLabel.textColor = UIColor.pomergranateColor()
            }
            
        } else {
            self.resetLabels()
        }
    }
    
    override func prepareForReuse() {
        self.category = nil
        self.resetLabels()
    }
    
    func resetLabels() -> Void {
        self.categoryPrimaryKeyLabel.text = ""
        self.categoryNameLabel.text = ""
    }
    
}
