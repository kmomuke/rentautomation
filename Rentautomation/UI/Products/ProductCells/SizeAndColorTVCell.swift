//
//  SizeAndColorTVCell.swift
//  Rentautomation
//
//  Created by kanybek on 5/5/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class SizeAndColorTVCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    var deleteBlock : ((Void) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func deleteDidPressed(_ sender: UIButton) {
        if let deleteBlock_ = self.deleteBlock {
            deleteBlock_()
        }
    }
}
