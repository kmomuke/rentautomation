//
//  ProductsListTVCell.swift
//  Rentautomation
//
//  Created by kanybek on 11/29/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import Kingfisher

class ProductsListTVCell: TDBadgedCell {

    var product: Product?
    
    @IBOutlet weak var productPrimaryKeyLabel: UILabel!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var productKeyLabel: UILabel!
    @IBOutlet weak var cellImageView: UIImageView!
    
    @IBOutlet weak var incomePriceLabel: UILabel!
    @IBOutlet weak var salePriceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        badgeColor = UIColor.blue
        self.incomePriceLabel.textColor = UIColor.wetAsphaltColor()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func reloadData() -> Void {
        if let product_ = self.product {

            if Global.shared.isDebug {
                self.productPrimaryKeyLabel.text = "\(product_.productId)"
                self.productPrimaryKeyLabel.backgroundColor = UIColor.clear
            } else {
                self.productPrimaryKeyLabel.isHidden = true
            }
            
            var salePrice = ""
            if let stringSalePrice = Global.shared.priceFormatter.string(for: product_.saleUnitPrice) {
                salePrice = stringSalePrice
            }
            self.salePriceLabel.text = "прод \(salePrice)"
            
            var incomePrice = ""
            if let stringIncomePrice = Global.shared.priceFormatter.string(for: product_.incomeUnitPrice) {
                incomePrice = stringIncomePrice
            }
            self.incomePriceLabel.text = "закуп \(incomePrice)"
            
            
            self.nameLabel.text = product_.productName
            
            var unitsInStock = ""
            if let text_ = Global.shared.amountFormatter.string(for: product_.unitsInStock) {
                unitsInStock = text_
            }
            
            self.productKeyLabel.text = "остаток на складе: \(unitsInStock) \(product_.quantityPerUnit)"
            if let url_ = product_.productImagePath {
                if url_.characters.count > 4 {
                    let url = URL(string: url_)
                    if url_.lowercased().range(of:"https") != nil {
                        let processor = ResizingImageProcessor(referenceSize: CGSize(width: 100, height: 100), mode: .none)
                        self.cellImageView.kf.indicatorType = .activity
                        self.cellImageView.kf.setImage(with: url,
                                                       placeholder: Global.shared.productPlaceholderImage,
                                                       options: [.processor(processor), .transition(.fade(0.2))],
                                                       progressBlock: nil,
                                                       completionHandler: { (im: Image?, err: NSError?, typr: CacheType, url: URL?) in
                        })    
                    } else {
                        self.cellImageView.kf.setImage(with: url,
                                                       placeholder: Global.shared.productPlaceholderImage,
                                                       options: nil,
                                                       progressBlock: nil,
                                                       completionHandler: { (im: Image?, err: NSError?, typr: CacheType, url: URL?) in
                        })
                    }
                } else {
                    self.cellImageView.image = Global.shared.productPlaceholderImage
                    //self.cellImageView.setImage(string: product_.productName)
                }
            } else {
                self.cellImageView.image = Global.shared.productPlaceholderImage
                //self.cellImageView.setImage(string: product_.productName)
            }
            
            if Database.shared.isModelSynchronized(primaryKey: product_.productId) {
                self.selectionStyle = .default
                self.nameLabel.textColor = UIColor.black
            } else {
                self.selectionStyle = .none
                self.nameLabel.textColor = UIColor.pomergranateColor()
            }
            
        } else {
            self.resetLabels()
        }
    }
    
    override func prepareForReuse() {
        self.product = nil
        self.resetLabels()
    }
    
    func resetLabels() -> Void {
        self.nameLabel.text = ""
        self.productKeyLabel.text = ""
        self.cellImageView.image = nil
        self.salePriceLabel.text = ""
        self.incomePriceLabel.text = ""
    }
}
