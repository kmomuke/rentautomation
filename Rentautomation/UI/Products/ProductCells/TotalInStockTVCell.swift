//
//  TotalInStockTVCell.swift
//  Rentautomation
//
//  Created by kanybek on 5/14/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class TotalInStockTVCell: UITableViewCell {
    
    var category: CategoryStat?
    
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var amountInStockLabel: UILabel!
    @IBOutlet weak var totalIncomePriceStockLabel: UILabel!
    @IBOutlet weak var totalPriceInStockLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func reloadData(_ indexPath: IndexPath) -> Void {
        if let categoryStat_ = self.category {
            self.categoryNameLabel.text = "\((indexPath.row + 1)). " + categoryStat_.categoryName
            self.amountInStockLabel.text = Global.shared.amountFormatter.string(for: categoryStat_.amountInStock)
            self.totalIncomePriceStockLabel.text = Global.shared.priceFormatter.string(for: categoryStat_.categoryIncomePrice)
            self.totalPriceInStockLabel.text = Global.shared.priceFormatter.string(for: categoryStat_.categorySaledPrice)
        } else {
            self.resetLabels()
        }
    }
    
    override func prepareForReuse() {
        self.category = nil
        self.resetLabels()
    }
    
    func resetLabels() -> Void {
        self.categoryNameLabel.text = ""
        self.amountInStockLabel.text = ""
        self.totalPriceInStockLabel.text = ""
        self.totalIncomePriceStockLabel.text = ""
    }
}
