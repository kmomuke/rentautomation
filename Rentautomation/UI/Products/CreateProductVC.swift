//
//  CreateProductVC.swift
//  Rentautomation
//
//  Created by kanybek on 11/29/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import Kingfisher
import Photos
import SwiftSignalKit
import TZImagePickerController

class CreateProductVC: FormViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, ErrorPopoverRenderer {

    var initialProduct: Product?
    var initialCategoryId: UInt64
    var picker: UIImagePickerController?
    var productCompanion: CreateProductCompanion?
    var dismissBlock : ((Void) -> Void)?
    
    convenience init() {
        self.init(initialProduct: nil, initialCategoryId: 0)
    }
    
    init(initialProduct: Product?, initialCategoryId: UInt64) {
        ProductMemoryInstance.sharedInstance.clearAllVariables()
        self.initialProduct = initialProduct
        self.initialCategoryId = initialCategoryId
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("CreateProductVC deallocated")
    }
    
    private func setUpInitialRows () -> Void {
        
        if self.initialCategoryId > 0 {
            ProductMemoryInstance.sharedInstance.categoryId = self.initialCategoryId
        }
        
        if let product = self.initialProduct {
            
            ProductMemoryInstance.sharedInstance.productName = product.productName
            ProductMemoryInstance.sharedInstance.saleUnitPrice = product.saleUnitPrice
            ProductMemoryInstance.sharedInstance.incomeUnitPrice = product.incomeUnitPrice
            ProductMemoryInstance.sharedInstance.unitsInStock = product.unitsInStock
            
            if let productImagePath_ = product.productImagePath {
                if let productImage_ = ImageCache.default.retrieveImageInDiskCache(forKey: productImagePath_) {
                    ProductMemoryInstance.sharedInstance.productImage = productImage_
                }
            }
            
            ProductMemoryInstance.sharedInstance.barcode = product.barcode
            ProductMemoryInstance.sharedInstance.quantityPerUnit = product.quantityPerUnit
            
            ProductMemoryInstance.sharedInstance.categoryId = product.categoryId
            ProductMemoryInstance.sharedInstance.supplierId = product.supplierId
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpInitialRows()
        
        self.productCompanion = CreateProductCompanion()
        
        self.picker = UIImagePickerController()
        self.picker?.delegate = self
        
        self.configureForms()
        
        self.title = "Создать/редактировать товар"
        
        let canceButton = UIBarButtonItem(
            title: "Закрыть",
            style: .plain,
            target: self,
            action: #selector(cancelObject(_:))
        )
        
        self.navigationItem.leftBarButtonItem = canceButton
        
        let createButton = UIBarButtonItem(
            title: "Сохранить",
            style: .plain,
            target: self,
            action: #selector(createProductDidPressed(_:))
        )
        
        self.navigationItem.rightBarButtonItem = createButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        former.deselect(animated: true)
        for row in self.optionalProductSection.rowFormers {
            row.update()
        }

        for row in self.colorAndSizeSection.rowFormers {
            row.update()
        }

        if ProductMemoryInstance.sharedInstance.colors.count > 0 ||
            ProductMemoryInstance.sharedInstance.productSizes.count > 0 {
            ProductMemoryInstance.sharedInstance.unitsInStock = 0.0
            for row in self.quantitySection.rowFormers {
                row.enabled = false
                row.update()
            }
        } else {
            for row in self.quantitySection.rowFormers {
                row.enabled = true
                row.update()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.neededProductSection.firstRowFormer?.former?.becomeEditingNext()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func cancelObject(_ sender: AnyObject) -> Void {
        ProductMemoryInstance.sharedInstance.clearAllVariables()
        self.navigationController?.dismiss(animated: true, completion: {[weak self] in
            print("dismiss CreateProductVC")
            if let dismissBlock_ = self?.dismissBlock {
                dismissBlock_()
            }
        })
    }
    
    // MARK: CREATE
    private func scanBarcodeDidPressed() -> Void {
        let scanCodeVC = ScanQRCodeVC()
        scanCodeVC.scanerScannedBlock = {[weak self] (barcode: String) -> Void in
            print("barcode == \(barcode)")
        }
        self.navigationController?.pushViewController(scanCodeVC, animated: true)
    }
    
    
    func createProductDidPressed(_ sender: UIBarButtonItem) -> Bool {
        if let _ = self.initialProduct {
            return self.updateProduct(sender)
        } else if ((ProductMemoryInstance.sharedInstance.colors.count == 0) &&
                   (ProductMemoryInstance.sharedInstance.productSizes.count == 0)) {
            return self.updateProduct(sender)
        } else {
            
            if let productName_ = ProductMemoryInstance.sharedInstance.productName {
                if productName_.characters.count < 2 {
                    return showErrorAlert(text: "Наименование")
                }
            } else {
                return showErrorAlert(text: "Наименование")
            }
            
            if (ProductMemoryInstance.sharedInstance.saleUnitPrice > 0.01) {
                
            } else {
                return showErrorAlert(text: "Цена на продажу")
            }
            
            if ProductMemoryInstance.sharedInstance.colors.count > 0 ||
                ProductMemoryInstance.sharedInstance.productSizes.count > 0 {
            } else {
                //if (ProductMemoryInstance.sharedInstance.unitsInStock > 0.01) {
                //} else {
                //    return showErrorAlert(text: "Количество")
                //}
            }
            
            let alertVC = ProductAlertVC(colors: ProductMemoryInstance.sharedInstance.colors,
                                         sizes: ProductMemoryInstance.sharedInstance.productSizes,
                                         productName: ProductMemoryInstance.sharedInstance.productName!)
            
            alertVC.performSaveBlock = {[weak self] (Void) -> Void in
                
                self?.showLoading("Создаются товары...")
                self?.productCompanion?.createProducts(productName_: ProductMemoryInstance.sharedInstance.productName!,
                                                saleUnitPrice_: ProductMemoryInstance.sharedInstance.saleUnitPrice,
                                                unitsInStock_: ProductMemoryInstance.sharedInstance.unitsInStock,
                                                incomeUnitPrice_: ProductMemoryInstance.sharedInstance.incomeUnitPrice,
                                                quantityPerUnit_: ProductMemoryInstance.sharedInstance.quantityPerUnit,
                                                categoryId_: ProductMemoryInstance.sharedInstance.categoryId,
                                                supplierId_: ProductMemoryInstance.sharedInstance.supplierId,
                                                barcode__: ProductMemoryInstance.sharedInstance.barcode,
                                                productImage__: ProductMemoryInstance.sharedInstance.productImage,
                                                colors_: ProductMemoryInstance.sharedInstance.colors,
                                                sizes_: ProductMemoryInstance.sharedInstance.productSizes,
                                                completion: {[weak self] (response: Bool) in
                                                    print("____RICHED_END____")
                                                    ProductMemoryInstance.sharedInstance.clearAllVariables()
                                                    self?.dismissLoading()
                                                    self?.navigationController?.dismiss(animated: true, completion: {
                                                        print("dismiss CreateProductVC")
                                                        self?.navigationController?.dismiss(animated: true, completion: {
                                                            print("dismiss CreateProductVC")
                                                            if let dismissBlock_ = self?.dismissBlock {
                                                                dismissBlock_()
                                                            }
                                                        })
                                                    })
                })
            }
            alertVC.dismissBlock = {[weak self] (Void) -> Void in
                ProductMemoryInstance.sharedInstance.clearAllVariables()
                self?.dismissLoading()
                self?.navigationController?.dismiss(animated: true, completion: {
                    print("dismiss CreateProductVC")
                    self?.navigationController?.dismiss(animated: true, completion: {
                        print("dismiss CreateProductVC")
                    })
                })
            }
            self.presentSheet(viewController: alertVC)
        }
        return true
    }
    
    
    func updateProduct(_ sender: UIBarButtonItem) -> Bool {
        
        let creatingProduct: Product
        var isUpdatingProduct: Bool = false
        if let product_ = self.initialProduct {
            creatingProduct = product_
            isUpdatingProduct = true
        } else {
            creatingProduct = Product()
            creatingProduct.productId = Database.shared.uniqueIncreasingPrimaryKey()
        }
        
        if let productName_ = ProductMemoryInstance.sharedInstance.productName {
            creatingProduct.productName = productName_
            if productName_.characters.count < 2 {
                return showErrorAlert(text: "Наименование")
            }
        } else {
            return showErrorAlert(text: "Наименование")
        }
        
        if (ProductMemoryInstance.sharedInstance.saleUnitPrice > 0.01) {
            creatingProduct.saleUnitPrice = ProductMemoryInstance.sharedInstance.saleUnitPrice
        } else {
            return showErrorAlert(text: "Цена на продажу")
        }
        
        
        if ProductMemoryInstance.sharedInstance.colors.count > 0 ||
            ProductMemoryInstance.sharedInstance.productSizes.count > 0 {
            creatingProduct.unitsInStock = 0.0
        } else {
            //if (ProductMemoryInstance.sharedInstance.unitsInStock > 0.01) {
            //    creatingProduct.unitsInStock = ProductMemoryInstance.sharedInstance.unitsInStock
            //} else {
            //    return showErrorAlert(text: "Количество")
            //}
            creatingProduct.unitsInStock = ProductMemoryInstance.sharedInstance.unitsInStock
        }
        
        if let productImage_ = ProductMemoryInstance.sharedInstance.productImage {
            
            var uuid: String
            
            if let productImagePath_ = creatingProduct.productImagePath {
                
                if productImagePath_.characters.count > 4 {
                    uuid = productImagePath_
                    if ProductMemoryInstance.sharedInstance.isNewImageCaputed  {
                        ImageCache.default.removeImage(forKey: uuid)
                        uuid = NSUUID().uuidString.lowercased()
                    }
                } else {
                    uuid = NSUUID().uuidString.lowercased()
                }

            } else {
                uuid = NSUUID().uuidString.lowercased()
            }
            
            ImageCache.default.store(productImage_, forKey: uuid)
            creatingProduct.productImagePath = uuid
        }
        
        if let barcode_ = ProductMemoryInstance.sharedInstance.barcode {
            creatingProduct.barcode = barcode_
        }
        
        creatingProduct.quantityPerUnit = ProductMemoryInstance.sharedInstance.quantityPerUnit
        creatingProduct.categoryId = ProductMemoryInstance.sharedInstance.categoryId
        creatingProduct.supplierId = ProductMemoryInstance.sharedInstance.supplierId
        creatingProduct.incomeUnitPrice = ProductMemoryInstance.sharedInstance.incomeUnitPrice
        
        let orderDetail = OrderDetail()
        orderDetail.orderDetailId = Database.shared.uniqueIncreasingPrimaryKey()
        orderDetail.orderDetailDate = Date()
        
        if isUpdatingProduct {
            orderDetail.isLast = false
        } else {
            orderDetail.isLast = true
        }
        
        orderDetail.productId = creatingProduct.productId
        orderDetail.orderQuantity = ProductMemoryInstance.sharedInstance.unitsInStock
        orderDetail.price = ProductMemoryInstance.sharedInstance.saleUnitPrice
        orderDetail.productQuantity = creatingProduct.unitsInStock
        orderDetail.orderId = 0
        orderDetail.billingNo = "\(Date().toString(.custom("EEE, dd MMM HH:mm"))) "
        
        Database.shared.createorderDetails([orderDetail])
        
        
        if isUpdatingProduct {
            
            if Database.shared.isModelSynchronized(primaryKey: creatingProduct.productId) {
                
                let operation = Event(syncOperationId: Database.shared.operationCid(),
                                              isSynchronized: false,
                                              syncOperationDate: Date(),
                                              operationType: .productUpdated,
                                              productId: creatingProduct.productId,
                                              orderDetailId: orderDetail.orderDetailId,
                                              customerId: 0,
                                              transactionId: 0,
                                              accountId: 0,
                                              supplierId: 0,
                                              staffId: 0,
                                              categoryId: 0,
                                              orderId: 0)
                
                Database.shared.createOperations([operation])
            }
            
            Database.shared.updateProduct(creatingProduct)
            PipesStore.shared.productUpdatedPipe.putNext(creatingProduct)
        } else {
            
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                          isSynchronized: false,
                                          syncOperationDate: Date(),
                                          operationType: .productCreated,
                                          productId: creatingProduct.productId,
                                          orderDetailId: orderDetail.orderDetailId,
                                          customerId: 0,
                                          transactionId: 0,
                                          accountId: 0,
                                          supplierId: 0,
                                          staffId: 0,
                                          categoryId: 0,
                                          orderId: 0)
            
            Database.shared.createOperations([operation])
            
            Database.shared.createProducts(products: [creatingProduct])
            PipesStore.shared.productCreatedPipe.putNext(creatingProduct)
        }
        
        print("____RICHED_END____")
        ProductMemoryInstance.sharedInstance.clearAllVariables()
        self.navigationController?.dismiss(animated: true, completion: {[weak self] in
            print("dismiss CreateProductVC")
            if let dismissBlock_ = self?.dismissBlock {
                dismissBlock_()
            }
        })

        return true
    }
    
    // MARK: Private
    private lazy var formerInputAccessoryView: FormerInputAccessoryView = FormerInputAccessoryView(former: self.former)
    
    let disableRowText: ((Bool) -> String) = {
        return ($0 ? "Enable" : "Disable") + " All Cells"
    }
    
    private lazy var productImageRow: LabelRowFormer<ProfileImageCell> = { [weak self] in
        
        LabelRowFormer<ProfileImageCell>(instantiateType: .Nib(nibName: "ProfileImageCell")) { [weak self] in
            
            if let product = self?.initialProduct {
                if let imagePath = product.productImagePath {
                    let image = ImageCache.default.retrieveImageInDiskCache(forKey: imagePath)
                    $0.iconView.image = image
                }
            }
            
            }.configure {
                $0.text = "Сделать фото"
                $0.rowHeight = 100
            }.onSelected { [weak self] _ in
                self?.former.deselect(animated:true)
                
                let alert: UIAlertController = UIAlertController(title: "Выбрать",
                                                                 message: "Выбрать",
                                                                 preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "Снять на камеру", style: .default, handler: {
                    (UIAlertAction) -> Void in
                    self?.presentImagePicker()
                }))
                //alert.addAction(UIAlertAction(title: "Выбрать из фото", style: .default, handler: {
                //    (UIAlertAction) -> Void in
                //   self?.presentImagePickerFromLibrary()
                //}))
                alert.addAction(UIAlertAction(title: "Выбрать из фото", style: .default, handler: {
                    (UIAlertAction) -> Void in
                    self?.presentImagePickerFromLibraryDetail()
                }))
                self?.present(alert, animated: true, completion: nil)
        }
    }()
    
    private lazy var imageSelectionSection: SectionFormer = { [weak self] in
        let section = SectionFormer(rowFormer: (self?.productImageRow)!)
        section.set(headerViewFormer: self?.createHeaderFunc("Select image", UIColor.midnightBlueColor()))
        return section
    }()
    
    // MARK: Optional product section
    private lazy var optionalProductSection: SectionFormer = { [weak self] in
        
        let productBarcodeRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            $0.titleLabel.text = "Штрихкод"
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.keyboardType = .numberPad
            }.configure { [weak self] in
                $0.placeholder = "Штрихкод"
                $0.text = ""
                $0.rowHeight = 55.0
                if let product_ = self?.initialProduct {
                    $0.text = product_.barcode
                }
            }.onTextChanged { [weak self] in
                ProductMemoryInstance.sharedInstance.barcode = $0
            }.onUpdate { [weak self] in
                if let barcode = ProductMemoryInstance.sharedInstance.barcode {
                    $0.text =  "\(barcode)"
                }
        }
        
        let createScanBarcodeRow: ((String, (() -> Void)?) -> RowFormer) = { [weak self] text, onSelected in
            return LabelRowFormer<FormLabelCell>() { [weak self] in
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                $0.subTextLabel.textColor = .formerSubColor()
                $0.subTextLabel.font = .boldSystemFont(ofSize: 14)
                }.configure { [weak self] in
                    $0.text = text
                    $0.rowHeight = 55
                }.onSelected { [weak self] _ in
                    onSelected?()
                    self?.former.deselect(animated: true)
                }.onUpdate { [weak self] in
                    $0.text = "Сканировать штрихкод"
                    if let barcode = ProductMemoryInstance.sharedInstance.barcode {
                        $0.subText = "\(barcode)"
                    }
            }
        }
        
        let scanBarcodeRow = createScanBarcodeRow("Сканировать штрихкод") { [weak self] in
            self?.scanBarcodeDidPressed()
        }
        
        let productIncomeUnitPriceRow = TextFieldRowFormer<PriceFieldTVCell>() { [weak self] in
            $0.titleLabel.text = "Цена закупки"
            $0.textField.keyboardType = .numberPad
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.returnKeyType = .next
            }.configure { [weak self] in
                $0.text = ""
                $0.rowHeight = 55
                if let product_ = self?.initialProduct {
                    if let text_ = Global.shared.amountFormatter.string(for: product_.incomeUnitPrice) {
                        $0.text =  "\(text_)"
                    }
                }
            }.onTextChanged { [weak self] in
                if let doubleVar = $0.getDoubleFromString() {
                    print("Цена закупки ===> \(doubleVar)")
                    ProductMemoryInstance.sharedInstance.incomeUnitPrice = doubleVar
                } else {
                    print("Цена закупки ===> \(0.0)")
                    ProductMemoryInstance.sharedInstance.incomeUnitPrice = 0.0
                }
        }
        
        let quantityPerUnitRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            $0.titleLabel.text = "Ед изм"
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.autocapitalizationType = .none
            
            }.configure { [weak self] in
                $0.rowHeight = 55
                $0.placeholder = "QuantPer"
                $0.text = ProductMemoryInstance.sharedInstance.quantityPerUnit
            }.onTextChanged { [weak self] in
                ProductMemoryInstance.sharedInstance.quantityPerUnit = $0
        }
        
        let createButtonRow1: ((String, (() -> Void)?) -> RowFormer) = { [weak self] text, onSelected in
            return LabelRowFormer<FormLabelCell>() { [weak self] in
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                $0.subTextLabel.textColor = .formerSubColor()
                $0.subTextLabel.font = .boldSystemFont(ofSize: 14)
                }.configure { [weak self] in
                    $0.text = text
                    $0.rowHeight = 55
                }.onSelected { [weak self] _ in
                    onSelected?()
                    self?.former.deselect(animated: true)
                }.onUpdate { [weak self] in
                    var categoryName: String = ""
                    if (ProductMemoryInstance.sharedInstance.categoryId > 0) {
                        if let category =  Database.shared.categoryBy(categoryId: ProductMemoryInstance.sharedInstance.categoryId) {
                            categoryName = category.categoryName
                        }
                    }
                    if (categoryName.characters.count > 0) {
                        $0.subText =  "\(categoryName)"
                    }
            }
        }
        
        let createButtonRow2: ((String, (() -> Void)?) -> RowFormer) = { [weak self] text, onSelected in
            return LabelRowFormer<FormLabelCell>() { [weak self] in
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                $0.subTextLabel.textColor = .formerSubColor()
                $0.subTextLabel.font = .boldSystemFont(ofSize: 14)
                }.configure { [weak self] in
                    $0.text = text
                    $0.rowHeight = 55
                }.onSelected { [weak self] _ in
                    onSelected?()
                    self?.former.deselect(animated: true)
                }.onUpdate { [weak self] in
                    var supplierName: String = ""
                    if (ProductMemoryInstance.sharedInstance.supplierId > 0) {
                        if let supplier =  Database.shared.supplierBy(supplierId: ProductMemoryInstance.sharedInstance.supplierId) {
                            supplierName = supplier.companyName
                        }
                    }
                    if (supplierName.characters.count > 0) {
                        $0.subText =  "\(supplierName)"
                    }
            }
        }
        
        let productCategoryRow = createButtonRow1("Категория") { [weak self] in
            
            let categoryVC: CategoriesListVC = CategoriesListVC(initialString: nil, isCommingFromProduct: true)
            self?.navigationController?.pushViewController(categoryVC, animated: true)
        }
        
        let productSupplierRow = createButtonRow2("Поставщик") { [weak self] in
            
            let supplierVC = SuppliersListVC(initialString: nil, isCommingFromProduct: true, isComingFromOrder: false)
            self?.navigationController?.pushViewController(supplierVC, animated: true)
        }
        
        let section = SectionFormer(rowFormer: productIncomeUnitPriceRow, quantityPerUnitRow, productCategoryRow, productSupplierRow, scanBarcodeRow, (self?.productImageRow)!)
        //section.set(headerViewFormer: self.createHeaderFunc(s: "Дополнительно"))
        return section
    }()
    
    // MARK: Needed product section
    private lazy var neededProductSection: SectionFormer = { [weak self] in
        
        let productNameRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            $0.titleLabel.text = "Наимен."
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.returnKeyType = .next
            }.configure { [weak self] in
                $0.placeholder = "Наименование"
                $0.text = ""
                $0.rowHeight = 55
                if let product_ = self?.initialProduct {
                    $0.text = product_.productName
                }
            }.onTextChanged { [weak self] in
                ProductMemoryInstance.sharedInstance.productName = $0
        }
        
        let productSaleUnitPriceRow = TextFieldRowFormer<PriceFieldTVCell>() { [weak self] in
            $0.titleLabel.text = "Цена продажи"
            $0.textField.keyboardType = .numberPad
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.returnKeyType = .next
            }.configure {[weak self] in
                $0.text = ""
                $0.rowHeight = 55
                if let product_ = self?.initialProduct {
                    //$0.text =  "\(product_.saleUnitPrice)"
                    if let text_ = Global.shared.amountFormatter.string(for: product_.saleUnitPrice) {
                        $0.text =  "\(text_)"
                    }
                    
                }
            }.onTextChanged { [weak self] in
                
                if let doubleVar = $0.getDoubleFromString() {
                    print("Цена продажи ===> \(doubleVar)")
                    ProductMemoryInstance.sharedInstance.saleUnitPrice = doubleVar
                }
        }
        
        let section = SectionFormer(rowFormer: productNameRow, productSaleUnitPriceRow)
        return section
    }()
    
    // MARK: Needed product section
    private lazy var quantitySection: SectionFormer = { [weak self] in
        
        let unitsInStockRow = TextFieldRowFormer<PriceFieldTVCell>() { [weak self] in
            $0.titleLabel.text = "Остатки"
            $0.textField.keyboardType = .numberPad
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.returnKeyType = .next
            }.configure {[weak self] in
                $0.placeholder = "Остатки на складе"
                $0.text = ""
                $0.rowHeight = 55
                if let product_ = self?.initialProduct {
                    if let text_ = Global.shared.amountFormatter.string(for: product_.unitsInStock) {
                        $0.text =  "\(text_)"
                    }
                }
            }.onTextChanged { [weak self] in
                if let doubleVar = $0.getDoubleFromString() {
                    print("Количество ===> \(doubleVar)")
                    ProductMemoryInstance.sharedInstance.unitsInStock = doubleVar
                } else {
                    print("Количество ===> \(0)")
                    ProductMemoryInstance.sharedInstance.unitsInStock = 0
                }
            }.onUpdate { [weak self] in
                if let text_ = Global.shared.amountFormatter.string(for: ProductMemoryInstance.sharedInstance.unitsInStock) {
                    $0.text =  "\(text_)"
                }
        }
        
        let section = SectionFormer(rowFormer: unitsInStockRow)
        return section
    }()
    
    private lazy var colorAndSizeSection: SectionFormer = { [weak self] in
        
        let productColorRow: ((String, (() -> Void)?) -> RowFormer) = { [weak self] text, onSelected in
            return LabelRowFormer<FormLabelCell>() { [weak self] in
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                $0.subTextLabel.textColor = .formerSubColor()
                $0.subTextLabel.font = .boldSystemFont(ofSize: 14)
                }.configure { [weak self] in
                    $0.text = text
                    $0.rowHeight = 55
                }.onSelected { [weak self] _ in
                    onSelected?()
                    self?.former.deselect(animated: true)
                }.onUpdate { [weak self] in
                    var colorsName: String = ""
                    if (ProductMemoryInstance.sharedInstance.colors.count > 0) {
                        for color in ProductMemoryInstance.sharedInstance.colors {
                            if (colorsName.characters.count == 0) {
                                colorsName = colorsName + color
                            } else {
                                colorsName = colorsName + ", " + color
                            }
                        }
                    }
                    $0.subText =  "\(colorsName)"
            }
        }
        
        let productSizeRow: ((String, (() -> Void)?) -> RowFormer) = { [weak self] text, onSelected in
            return LabelRowFormer<FormLabelCell>() { [weak self] in
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                $0.subTextLabel.textColor = .formerSubColor()
                $0.subTextLabel.font = .boldSystemFont(ofSize: 14)
                }.configure { [weak self] in
                    $0.text = text
                    $0.rowHeight = 55
                }.onSelected { [weak self] _ in
                    onSelected?()
                    self?.former.deselect(animated: true)
                }.onUpdate { [weak self] in
                    var sizesName: String = ""
                    if (ProductMemoryInstance.sharedInstance.productSizes.count > 0) {
                        for size in ProductMemoryInstance.sharedInstance.productSizes {
                            if (sizesName.characters.count == 0) {
                                sizesName = sizesName + size
                            } else {
                                sizesName = sizesName + ", " + size
                            }
                        }
                    }
                    $0.subText =  "\(sizesName)"
            }
        }
        
        let productCategoryRow = productColorRow("Цвета") { [weak self] in
            let colorAndSizeVC = ColorAndSizeListTVC(initialItem: "Цвета")
            self?.navigationController?.pushViewController(colorAndSizeVC, animated: true)
        }
        
        let productSupplierRow = productSizeRow("Размеры") { [weak self] in
            let colorAndSizeVC = ColorAndSizeListTVC(initialItem: "Размеры")
            self?.navigationController?.pushViewController(colorAndSizeVC, animated: true)
        }
        
        let section = SectionFormer(rowFormer: productCategoryRow, productSupplierRow)
        return section
    }()
    
    
    private func pushSelectorRowSelected(options: [String], numberIndex:UInt8) -> (RowFormer) -> Void {
        return { [weak self] rowFormer in
            if let rowFormer = rowFormer as? LabelRowFormer<FormLabelCell> {
                let controller = TextSelectorViewContoller()
                controller.texts = options
                controller.selectedText = rowFormer.subText
                controller.onSelected = { [weak self] in
                    rowFormer.subText = $0
                    rowFormer.update()
                    
                    switch numberIndex {
                    case 1:
                        print("Add 1")
                    case 20:
                        print("Add 20")
                        //ItemSkyMemoryInstance.sharedInstance.typeMode = $0
                    case 30:
                        print("Add 30")
                        //ItemSkyMemoryInstance.sharedInstance.viewMode = $0
                    default:
                        print("Everything")
                    }
                }
                self?.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    private func someFunc() -> SectionFormer {
        
        let createSelectorRow = {(text: String, subText: String, onSelected: ((RowFormer) -> Void)?) -> RowFormer in
            return LabelRowFormer<FormLabelCell>() { [weak self] in
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
                $0.subTextLabel.textColor = .formerSubColor()
                $0.subTextLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                }.configure { form in
                    _ = onSelected.map { form.onSelected($0) }
                    form.text = text
                    form.subText = subText
            }
        }
        
        let options = ["Option1", "Option2", "Option3"]
        let pushSelectorOptionsRow = createSelectorRow("Options", options[0], pushSelectorRowSelected(options: options, numberIndex: 10))
        
        let section = SectionFormer(rowFormer: pushSelectorOptionsRow)
        section.set(headerViewFormer: self.createHeaderFunc("Дополнительно", UIColor.asbestosColor()))
        return section
    }
    
    private func createHeaderFunc(_ s: String, _ color: UIColor) -> ViewFormer {
        return LabelViewFormer<FormLabelHeaderView>()
            .configure { [weak self] in
                $0.textColor = color
                $0.viewHeight = 40
                $0.text = s
        }
    }
    
    private func configureForms() {
        
        //tableView.contentInset.top = 40
        self.tableView.contentInset.bottom = 40
        self.quantitySection.set(footerViewFormer: createHeaderFunc("", UIColor.concreteColor()))
        self.optionalProductSection.set(headerViewFormer: createHeaderFunc("Дополнительная информация", UIColor.concreteColor()))
        
        former.append(sectionFormer:self.neededProductSection, self.quantitySection, self.optionalProductSection)
            .onCellSelected { [weak self] _ in
                self?.formerInputAccessoryView.update()
        }
        
        if self.initialProduct == nil {
            former.append(sectionFormer: self.colorAndSizeSection)
        }
    }
    
    private func presentImagePickerFromLibrary() {
        
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .photoLibrary
        picker.allowsEditing = false
        picker.modalPresentationStyle = .popover
        picker.popoverPresentationController?.sourceView = self.view
        present(picker, animated: true, completion: nil)
    }
    
    private func presentImagePickerFromLibraryDetail() {
        let vc = TZImagePickerController(maxImagesCount: 1000,
                                         columnNumber: 4,
                                         delegate: nil)!
        vc.maxImagesCount = 1
        vc.allowPickingVideo = false
        vc.allowTakePicture = true
        vc.allowCrop = true
        
        vc.didFinishPickingPhotosHandle = {[weak self] (g: [UIImage]?, l: [Any]?, f: Bool) -> Void in
            if let localImage = g?.last {
                self?.updateImageRow(localImage: localImage)
            }
        }
        self.present(vc, animated: true) { 
        }
    }
    
    
    private func presentImagePicker() {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)) {
            self.picker?.sourceType = UIImagePickerControllerSourceType.camera
            self.present(self.picker!, animated: true, completion: nil)
        }
    }
    
//    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
//        picker.dismiss(animated: true, completion: nil)
//        //Profile.sharedInstance.image = image
//        //imageRow.cellUpdate {
//        //    $0.iconView.image = image
//        //}
//    }
    
    func updateImageRow(localImage: UIImage) -> Void {
        generateQue.async {
            
            let croppedImage = Toucan(image: localImage).resize(CGSize(width: 512, height: 512), fitMode: Toucan.Resize.FitMode.crop).image
            let dataCompress = UIImageJPEGRepresentation(croppedImage, 0.5)
            let imageCompressed = UIImage(data: dataCompress!)
            
            DispatchQueue.main.async {
                ProductMemoryInstance.sharedInstance.isNewImageCaputed = true
                ProductMemoryInstance.sharedInstance.productImage = imageCompressed
                
                self.productImageRow.cellUpdate {[weak self] in
                    $0.iconView.image = imageCompressed
                }
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true) {}
        let localImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.updateImageRow(localImage: localImage!)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("picker cancel.")
        picker.dismiss(animated: true) {}
    }
}
