//
//  ProductAlertVC.swift
//  Rentautomation
//
//  Created by kanybek on 5/5/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit

class ProductAlertVC: BaseListVC, ErrorPopoverRenderer, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var productNames: [String] = []
    var disposeSet: DisposableSet?
    
    var sizes_: [String]
    var colors_: [String]
    var productName_: String
    
    var performSaveBlock : ((Void) -> Void)?
    var dismissBlock : ((Void) -> Void)?
    
    convenience init() {
        self.init(colors: [], sizes:[], productName: "")
    }
    
    init(colors: [String], sizes:[String], productName: String) {
        self.disposeSet = DisposableSet()
        
        self.sizes_ = sizes
        self.colors_ = colors
        self.productName_ = productName
        
        super.init(nibName: "ProductAlertVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.disposeSet?.dispose()
        self.disposeSet = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if colors_.count > 0 && sizes_.count > 0 {
            
            for color in colors_ {
                for size in sizes_ {
                    let k =  productName_ + " " + color + " " + size
                    productNames.append(k)
                }
            }
            
        } else if colors_.count > 0 && sizes_.count == 0 {
            
            for color in colors_ {
                let k =  productName_ + " " + color
                productNames.append(k)
            }
            
        } else if colors_.count == 0 && sizes_.count > 0 {
            
            for size in sizes_ {
                let k =  productName_ + " " + size
                productNames.append(k)
            }
            
        } else if colors_.count == 0 && sizes_.count == 0 {
            let k =  productName_
            productNames.append(k)
        }
        
        
        let nib = UINib(nibName: "CategoriesListTVCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "CategoriesListTVCell")
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK:
    @IBAction func createProductsDidPressed(_ sender: UIButton) {
        if let performSaveBlock_ = self.performSaveBlock {
            performSaveBlock_()
        }
        self.navigationController?.dismiss(animated: true, completion: {[weak self] in
            if let dismissBlock_ = self?.dismissBlock {
                dismissBlock_()
            }
        })
    }
    
    @IBAction func cancelDidPressed(_ sender: UIButton) {
        self.navigationController?.dismiss(animated: true, completion: {[weak self] in
        })
    }
    
    // MARK: TableView DataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoriesListTVCell", for: indexPath) as! CategoriesListTVCell
        let object = productNames[(indexPath as NSIndexPath).row]
        cell.categoryNameLabel.text = "(" + "\((indexPath.row + 1))" + ")" + "  " + object
        cell.categoryPrimaryKeyLabel.isHidden = true
        cell.selectionStyle = .none
        return cell
    }
}
