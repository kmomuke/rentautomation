//
//  TotalAmountInStockVC.swift
//  Rentautomation
//
//  Created by kanybek on 2/10/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class TotalAmountInStockVC: BaseListVC, ErrorPopoverRenderer, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var salePriceTotal: UILabel!
    @IBOutlet weak var incomePriceTotalLabel: UILabel!

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextfield: UITextField!
    
    var initialCategories: [CategoryStat] = []
    var categories: [CategoryStat] = []
    
    var initialItem: String?
    
    convenience init() {
        self.init(initialItem: nil)
    }
    
    init(initialItem: String?) {
        self.initialItem = initialItem
        super.init(nibName: "TotalAmountInStockVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Остатки на складе"
        
        let nib = UINib(nibName: "TotalInStockTVCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "TotalInStockTVCell")
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.addCanceButton()
        
        if Global.shared.isShowTestButton {
            let editButton = UIBarButtonItem(
                title: "JSON",
                style: .plain,
                target: self,
                action: #selector(generateJson(_:))
            )
            self.navigationItem.rightBarButtonItem = editButton
        }
        
        self.searchTextfield.delegate = self
        self.searchTextfield.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        var totalCount = 0.0
        var totalSaleSum = 0.0
        var totalIncomeSum = 0.0
        
        Database.shared.loadAllProductsBy(categoryId: 0,
                                          limit: Global.shared.limitForStaticModels) {[weak self] (products_: [Product]) in
                                            
                                            for product in products_ {
                                                
                                                var unitsInStock_: Double = 0
                                                if product.unitsInStock > 0 {
                                                    unitsInStock_ = product.unitsInStock
                                                }
                                                
                                                totalCount = totalCount + unitsInStock_
                                                totalSaleSum = totalSaleSum + product.saleUnitPrice * unitsInStock_
                                                totalIncomeSum = totalIncomeSum + product.incomeUnitPrice * unitsInStock_
                                            }
                                            
                                            var salePrice = ""
                                            if let stringSalePrice = Global.shared.priceFormatter.string(for: totalSaleSum) {
                                                salePrice = stringSalePrice
                                            }
                                            self?.salePriceTotal.text = "\(salePrice)"
                                            
                                            var incomePrice = ""
                                            if let stringIncomePrice = Global.shared.priceFormatter.string(for: totalIncomeSum) {
                                                incomePrice = stringIncomePrice
                                            }
                                            self?.incomePriceTotalLabel.text = "\(incomePrice)"
        }
        
        self.reloadAllCategories()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func reloadAllCategories() {
        self.showLoading("Генерация ...")
        StatGenerator.shared.calculateCategoryStats {[weak self] (categoryStats: [CategoryStat]) in
            self?.dismissLoading()
            self?.initialCategories = categoryStats
            self?.categories = categoryStats
            self?.tableView.reloadData()
        }
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        if let searchText = textField.text {
            if searchText.characters.count > 0 {
                let filteredArray = self.initialCategories.filter({ (cat: CategoryStat) -> Bool in
                    if cat.categoryName.range(of:searchText) != nil{
                        return true
                    }
                    return false
                })
                self.categories = filteredArray
                self.tableView.reloadData()
            } else {
                self.categories = self.initialCategories
                self.tableView.reloadData()
            }
        }
    }
    
    func generateJson(_ sender: UIBarButtonItem) {
        let alert: UIAlertController = UIAlertController(title: "Сгенерировать JSON",
                                                         message: "Вы действительно хотите сгенерировать JSON?",
                                                         preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Сгенерировать товары", style: .default, handler: {[weak self]
            (UIAlertAction) -> Void in
            if let stringSelf = self {
                
                stringSelf.showLoading("Генерация JSON...")
                Database.shared.loadAllProductsBy(categoryId: 0,
                                                  limit: Global.shared.limitForStaticModels) {[weak self] (products_: [Product]) in
                                                    
                                                    GenerateJson.shared.generateJsonFileFromProducts(products: products_,
                                                                                                     progress: { (fraction: Float) in
                                                    },
                                                                                                     completion: { (url:URL) in
                                                                                                        if let strongSelf = self {
                                                                                                            strongSelf.dismissLoading()
                                                                                                            
                                                                                                            let activityViewController = UIActivityViewController(activityItems: [url] , applicationActivities: nil)
                                                                                                            if activityViewController.responds(to: #selector(getter: UIViewController.popoverPresentationController)) {
                                                                                                                activityViewController.popoverPresentationController?.barButtonItem = sender
                                                                                                            }
                                                                                                            
                                                                                                            strongSelf.present(activityViewController, animated: true, completion: {
                                                                                                            })
                                                                                                        }
                                                    })
                }
            }
        }))
        
        
        alert.addAction(UIAlertAction(title: "Читать", style: .default, handler: {[weak self]
            (UIAlertAction) -> Void in
            if let stringSelf = self {
                ReadFromJson.shared.readFromCatLocalJson()
                ReadFromJson.shared.readFromProdLocalJson()
                ReadFromJson.shared.readFromCustomerLocalJson()
            }
        }))
            
        
        alert.addAction(UIAlertAction(title: "Сгенерировать категорию", style: .default, handler: {[weak self]
            (UIAlertAction) -> Void in
            if let stringSelf = self {
                
                stringSelf.showLoading("Генерация JSON...")
                Database.shared.loadAllProductsBy(categoryId: 0,
                                                  limit: Global.shared.limitForStaticModels) {[weak self] (products_: [Product]) in
                                                    
                                                    let categories = Database.shared.loadAllCategories()
                                                    GenerateJson.shared.generateJsonFileFromCategories(categories: categories,
                                                                                                       progress: { (f: Float) in
                                                    },
                                                                                                       completion: { (url: URL) in
                                                                                                        if let strongSelf = self {
                                                                                                            strongSelf.dismissLoading()
                                                                                                            
                                                                                                            let activityViewController = UIActivityViewController(activityItems: [url] , applicationActivities: nil)
                                                                                                            if activityViewController.responds(to: #selector(getter: UIViewController.popoverPresentationController)) {
                                                                                                                activityViewController.popoverPresentationController?.barButtonItem = sender
                                                                                                            }
                                                                                                            
                                                                                                            strongSelf.present(activityViewController, animated: true, completion: {
                                                                                                            })
                                                                                                        }
                                                    })
                }
            }
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: TableView DataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TotalInStockTVCell", for: indexPath) as! TotalInStockTVCell
        cell.category = categories[(indexPath as NSIndexPath).row]
        cell.reloadData(indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension TotalAmountInStockVC: UITextFieldDelegate {
    
    //    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    //
    //    }
    
    //    func textFieldDidBeginEditing(_ textField: UITextField) {
    //
    //    }
    
    //    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
    //
    //    }
    
    //    func textFieldDidEndEditing(_ textField: UITextField) {
    //
    //    }
    
    //    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
    //
    //    }
    
    //    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    //
    //    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("textFieldShouldClear")
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("textFieldShouldReturn")
        textField.resignFirstResponder()
        return true
    }
}
