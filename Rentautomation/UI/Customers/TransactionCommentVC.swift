//
//  TransactionCommentVC.swift
//  Rentautomation
//
//  Created by kanybek on 5/10/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
class TransactionCommentVC: FormViewController, ErrorPopoverRenderer {
    
    var intialTransaction: Transaction?
    
    convenience init() {
        self.init(intialTransaction: nil)
    }
    
    init(intialTransaction: Transaction?) {
        self.intialTransaction = intialTransaction
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("TransactionCommentVC deallocated")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure()
        
        self.title = "Комментарии"
        
        let canceButton = UIBarButtonItem(
            title: "Закрыть",
            style: .plain,
            target: self,
            action: #selector(cancelObject(_:))
        )
        self.navigationItem.leftBarButtonItem = canceButton
        
        let createButton = UIBarButtonItem(
            title: "Сохранить",
            style: .plain,
            target: self,
            action: #selector(saveCommentUpdate(_:))
        )
        self.navigationItem.rightBarButtonItem = createButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func cancelObject(_ sender: AnyObject) -> Void {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    // MARK: UPDATE
    func saveCommentUpdate(_ sender: AnyObject) -> Bool {
        if let transaction_ = self.intialTransaction {
            
            print("transaction_.comment = \(transaction_.comment)")
            
            Database.shared.updateTransactionsComment(transactionId: transaction_.transactionId,
                                                      comment: transaction_.comment)
            
            if Database.shared.isModelSynchronized(primaryKey: transaction_.transactionId) {
                
                let operation = Event(syncOperationId: Database.shared.operationCid(),
                                              isSynchronized: false,
                                              syncOperationDate: Date(),
                                              operationType: .transactionUpdated,
                                              productId: 0,
                                              orderDetailId: 0,
                                              customerId: 0,
                                              transactionId: transaction_.transactionId,
                                              accountId: 0,
                                              supplierId: 0,
                                              staffId: 0,
                                              categoryId: 0,
                                              orderId: 0)
                Database.shared.createOperations([operation])
            }
        }
        
        self.navigationController?.dismiss(animated: true, completion: nil)
        return true
    }
    
    // MARK: Private
    private lazy var formerInputAccessoryView: FormerInputAccessoryView = FormerInputAccessoryView(former: self.former)
    
    private lazy var commentInfoSection: SectionFormer = { [weak self] in
        
        let commentRow = TextViewRowFormer<FormTextViewCell>() { [weak self] in
            $0.textView.textColor = .formerSubColor()
            $0.textView.font = .systemFont(ofSize: 20)
            $0.textView.inputAccessoryView = self?.formerInputAccessoryView
            }.configure {[weak self] in
                $0.rowHeight = 120.0
                $0.placeholder = "Комментарии"
                $0.text = ""
                if let transaction_ = self?.intialTransaction {
                    $0.text = transaction_.comment
                }
            }.onTextChanged {[weak self] in
                if let transaction_ = self?.intialTransaction {
                    transaction_.comment = $0
                }
        }
        
        let section = SectionFormer(rowFormer: commentRow)
        section.set(headerViewFormer: self?.createHeaderFunc(s: ""))
        return section
    }()
    
    private func createHeaderFunc(s: String) -> ViewFormer {
        return LabelViewFormer<FormLabelHeaderView>()
            .configure {
                $0.viewHeight = 20
                $0.text = s
        }
    }
    
    private func configure() {
        
        tableView.contentInset.top = 0
        tableView.contentInset.bottom = 0
        
        former.append(sectionFormer:self.commentInfoSection)
            .onCellSelected { [weak self] _ in
                self?.formerInputAccessoryView.update()
        }
    }
}
