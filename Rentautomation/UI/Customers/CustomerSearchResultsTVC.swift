//
//  CustomerSearchResultsTVC.swift
//  Rentautomation
//
//  Created by kanybek on 2/3/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit

class CustomerSearchResultsTVC: UITableViewController {
    
    var customerSelectBlock : ((Customer) -> Void)?
    
    var initalString: String?
    var disposeSet: DisposableSet?
    var filteredCustomers = [Customer]()
    
    convenience init() {
        self.init(initalString: "koke")
    }
    
    init(initalString: String) {
        self.initalString = initalString
        self.disposeSet = DisposableSet()
        super.init(style: .plain)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.disposeSet?.dispose()
        self.disposeSet = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "CustomersListTVCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "CustomersListTVCell")
        
        let customerUpdateSignal: Signal = PipesStore.shared.customerUpdatedSignal()
        let updateCustomerDispose = customerUpdateSignal.start(next: {[weak self] (customer: Customer) in
            if let i = self?.filteredCustomers.index(where: {$0.customerId == customer.customerId}) {
                self?.filteredCustomers[i] = customer
                self?.tableView.reloadRows(at: [IndexPath(row: i, section: 0)], with: .fade)
            }
        })
        self.disposeSet?.add(updateCustomerDispose)
        
        let balanceUpdatedSignal: Signal = PipesStore.shared.balanceUpdatedSignal()
        let updateCustomerBalanceDispose = balanceUpdatedSignal.start(next: {[weak self] (customerId: UInt64) in
            if let indexRow = self?.filteredCustomers.index(where: {$0.customerId == customerId}) {
                self?.tableView.reloadRows(at: [IndexPath(row: indexRow, section: 0)], with: .fade)
            }
        })
        self.disposeSet?.add(updateCustomerBalanceDispose)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredCustomers.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomersListTVCell", for: indexPath) as! CustomersListTVCell
        let object = filteredCustomers[(indexPath as NSIndexPath).row]
        cell.customer = object
        cell.reloadData()
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let customer = filteredCustomers[(indexPath as NSIndexPath).row]
        if let customerBlock_ = self.customerSelectBlock {
            customerBlock_(customer)
        }
    }
}
