//
//  CustomerDescriptionVC.swift
//  Rentautomation
//
//  Created by kanybek on 12/5/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit

class CustomerDescriptionVC: BaseListVC, ErrorPopoverRenderer {
    
    @IBOutlet weak var currentBalanceLabel: UILabel!
    @IBOutlet weak var nameSurnameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var operationsContainerView: UIView!
    
    var isFirstTime: Bool = true
    var initialCustomer: Customer?
    var updateCustomerDispose: Disposable?
    var balanceUpdateCustomerDispose: Disposable?
    
    private lazy var moneyOperationsVC: CustomerMoneyOperationsVC = { [weak self] in
        let moneyOperationsVC_ = CustomerMoneyOperationsVC(initialCustomer: self?.initialCustomer, initialSupplier: nil)
        return moneyOperationsVC_
    }()
    
    convenience init() {
        self.init(initialCustomer: nil)
    }
    
    init(initialCustomer: Customer?) {
        self.initialCustomer = initialCustomer
        super.init(nibName: "CustomerDescriptionVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.updateCustomerDispose?.dispose()
        self.updateCustomerDispose = nil
        self.balanceUpdateCustomerDispose?.dispose()
        self.balanceUpdateCustomerDispose = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let editButton = UIBarButtonItem(
            title: "Редактировать",
            style: .plain,
            target: self,
            action: #selector(editProduct(_:))
        )
        self.navigationItem.rightBarButtonItem = editButton
        self.updateUI()
        
        if let currentCustomer = self.initialCustomer {
            if let account_ = Database.shared.accountBy(customerId: currentCustomer.customerId) {
                self.currentBalanceLabel.text = Global.shared.priceFormatter.string(for: account_.balance)
            }
        }
        
        let customerUpdateSignal: Signal = PipesStore.shared.customerUpdatedSignal()
        self.updateCustomerDispose = customerUpdateSignal.start(next: {[weak self] (customer: Customer) in
            
            if let currentCustomer = self?.initialCustomer {
                if currentCustomer.customerId == customer.customerId {
                    self?.initialCustomer = customer
                    self?.updateUI()
                }
            }
        })
        
        let balanceUpdatedSignal: Signal = PipesStore.shared.balanceUpdatedSignal()
        self.updateCustomerDispose = balanceUpdatedSignal.start(next: {[weak self] (customerId: UInt64) in
            
            if let currentCustomer = self?.initialCustomer {
                if currentCustomer.customerId == customerId {
                    if let account_ = Database.shared.accountBy(customerId: customerId) {
                        self?.currentBalanceLabel.text = Global.shared.priceFormatter.string(for: account_.balance)
                        self?.moneyOperationsVC.reloadOperations()
                    }
                }
            }
        })
    }
    
    func updateUI() -> Void {
        
        if let initalCustomer_ = self.initialCustomer {
            self.title = "\(initalCustomer_.firstName) \(initalCustomer_.secondName)"
            self.nameSurnameLabel.text = "\(initalCustomer_.firstName) \(initalCustomer_.secondName)"
            self.phoneLabel.text = initalCustomer_.phoneNumber
            self.addressLabel.text = initalCustomer_.address
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (isFirstTime) {
            self.operationsContainerView.backgroundColor = UIColor.white
            self.add(asChildViewController: self.moneyOperationsVC, frame_: self.operationsContainerView.frame)
            isFirstTime = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        print("self.operationsContainerView.frame.size.width ===> \(self.operationsContainerView.frame.size.width)")
    }
    
    func editProduct(_ sender: AnyObject) -> Void {
        let editVC = CreateCustomerVC(initialCustomer:self.initialCustomer)
        self.presentSheet(viewController: editVC)
    }
    
    // ---- Private methods
    @IBAction func setBalanceDidPressed(_ sender: Any) {
        
        if let currentCustomer = self.initialCustomer {
            if Database.shared.isModelSynchronized(primaryKey: currentCustomer.customerId) {
                
                if let account_ = Database.shared.accountBy(customerId: currentCustomer.customerId) {
                    
                    var debitType: DebitType = .ownerShouldTakeMoneyFromClient
                    if (account_.balance >= 0.0) {
                        debitType = .ownerShouldTakeMoneyFromClient
                    } else {
                        debitType = .ownerShouldGiveMoneyToClient
                    }
                    
                    let setBallanceVC = SetBallanceVC(initialBalance: account_.balance, initialDebitType: debitType, customerId: currentCustomer.customerId, supplierId: 0)
                    self.presentSheet(viewController: setBallanceVC)
                }
                
            } else {
                let _ = self.showErrorAlert(text: "не сохранен")
            }
        }
    }
}
