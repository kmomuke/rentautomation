//
//  SetBallanceVC.swift
//  Rentautomation
//
//  Created by kanybek on 12/21/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

public enum DebitType : UInt {
    case ownerShouldGiveMoneyToClient = 1000        //"Я должен деньги клиенту"
    case ownerShouldTakeMoneyFromClient = 2000      //"Клиент должен деньги мне"
}

class SetBallanceVC: FormViewController, ErrorPopoverRenderer {

    var initialBalance: Double
    var initialDebitType: DebitType
    
    var comment: String?
    
    var customerId: UInt64
    var supplierId: UInt64
    
    convenience init() {
        self.init(initialBalance: 0.0, initialDebitType: .ownerShouldGiveMoneyToClient, customerId: 0, supplierId: 0)
    }
    
    init(initialBalance: Double, initialDebitType: DebitType, customerId: UInt64, supplierId: UInt64 ) {
        self.initialBalance = initialBalance
        self.initialDebitType = initialDebitType
        self.customerId = customerId
        self.supplierId = supplierId
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("SetBallanceVC deallocated")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure()
        
        self.title = "Редактировать баланс"
        
        let canceButton = UIBarButtonItem(
            title: "Закрыть",
            style: .plain,
            target: self,
            action: #selector(cancelObject(_:))
        )
        
        self.navigationItem.leftBarButtonItem = canceButton
        
        let createButton = UIBarButtonItem(
            title: "Сохранить",
            style: .plain,
            target: self,
            action: #selector(saveBalance(_:))
        )
        
        self.navigationItem.rightBarButtonItem = createButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.balanceSumRow.former?.becomeEditingNext()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func cancelObject(_ sender: AnyObject) -> Void {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    // MARK: CREATE
    func saveBalance(_ sender: AnyObject) -> Bool {
        
        var balance: Double = 0.0
        if self.initialDebitType == .ownerShouldGiveMoneyToClient {
            
            if self.initialBalance > 0 {
                balance = -self.initialBalance
            } else {
                balance = self.initialBalance
            }
            
        } else if self.initialDebitType == .ownerShouldTakeMoneyFromClient {
            
            if self.initialBalance > 0 {
                balance = self.initialBalance
            } else {
                balance = -self.initialBalance
            }
        }
        
        var transacationType: TransactionType = .none
        var syncType: EventType = .none
        
        if self.customerId > 0 {
            Database.shared.updateCustomerBalanceFor(customerId: self.customerId, balance: balance)
            transacationType = .customerBalanceModified
            syncType = .customerBalanceUpdated
        } else if self.supplierId > 0 {
            Database.shared.updateSupplierBalanceFor(supplierId: self.supplierId, balance: balance)
            transacationType = .supplierBalanceModified
            syncType = .supplierBalanceUpdated
        }
        
        var commentString: String = ""
        if let comment_ = self.comment {
            commentString = comment_
        }
        
        if let employee_ = AppSession.shared.currentEmployee {
            
            let transactionId = Database.shared.uniqueIncreasingPrimaryKey()
            let transaction = Transaction(transactionId: transactionId,
                                          transactionDate: Date(),
                                          isLastTransaction: false,
                                          transactionType: transacationType,
                                          moneyAmount: balance,
                                          orderId: 0,
                                          customerId: self.customerId,
                                          supplierId: self.supplierId,
                                          userId: employee_.userId,
                                          ballanceAmount: 0,
                                          comment: commentString,
                                          transactionUuid: UUID().uuidString.lowercased())
            
            Database.shared.createTransactions([transaction])
            
            var account_: Account?
            
            if self.customerId > 0 {
                account_ = Database.shared.accountBy(customerId: self.customerId)
            } else if self.supplierId > 0 {
                account_ = Database.shared.accountBy(supplierId: self.supplierId)
            }
            
            if let account__ = account_ {
                let operation = Event(syncOperationId: Database.shared.operationCid(),
                                              isSynchronized: false,
                                              syncOperationDate: Date(),
                                              operationType: syncType,
                                              productId: 0,
                                              orderDetailId: 0,
                                              customerId: self.customerId,
                                              transactionId: transaction.transactionId,
                                              accountId: account__.accountId,
                                              supplierId: self.supplierId,
                                              staffId: 0,
                                              categoryId: 0,
                                              orderId: 0)
                
                Database.shared.createOperations([operation])
            }
            
        } else {
            return self.showErrorAlert(text: "Сотрудник")
        }
        
        if self.customerId > 0 {
            PipesStore.shared.balanceUpdatedPipe.putNext(self.customerId)
        } else if self.supplierId > 0 {
            PipesStore.shared.balanceUpdatedPipe.putNext(self.supplierId)
        }
        
        self.navigationController?.dismiss(animated: true, completion: nil)
        return true
    }
    
    // MARK: Private
    private lazy var formerInputAccessoryView: FormerInputAccessoryView = FormerInputAccessoryView(former: self.former)
    
    // MARK: Optional product section
    private lazy var balanceSumRow: TextFieldRowFormer<PriceFieldTVCell> = {
        
        let totalUnitPriceRow = TextFieldRowFormer<PriceFieldTVCell>() { [weak self] in
            $0.titleLabel.text = "Сумма"
            $0.textField.keyboardType = .numberPad
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.returnKeyType = .next
            }.configure { [weak self] in
                
                if let strongBalance = self?.initialBalance {
                    if let text_ = Global.shared.amountFormatter.string(for: strongBalance) {
                        $0.text =  "\(text_)"
                    }
                }
                
                $0.rowHeight = 60
            }.onTextChanged { [weak self] in
                
                if let doubleVar = $0.getDoubleFromString() {
                    print("self?.initialBalance ==> \(doubleVar)")
                    self?.initialBalance = doubleVar
                } else {
                    self?.initialBalance = 0.0
                }
        }
        return totalUnitPriceRow
    }()
    
    private lazy var nameCategorySection: SectionFormer = { [weak self] in
        let section = SectionFormer(rowFormer: (self?.balanceSumRow)!)
        section.set(headerViewFormer: self?.createHeaderFunc(s: ""))
        return section
    }()
    
    private lazy var commentInfoSection: SectionFormer = { [weak self] in
        
        let commentRow = TextViewRowFormer<FormTextViewCell>() { [weak self] in
            $0.textView.textColor = .formerSubColor()
            $0.textView.font = .systemFont(ofSize: 20)
            $0.textView.inputAccessoryView = self?.formerInputAccessoryView
            }.configure {[weak self] in
                $0.rowHeight = 120.0
                $0.placeholder = "Комментарии"
                $0.text = ""
                if let comment_ = self?.comment {
                    $0.text = comment_
                }
            }.onTextChanged {[weak self] in
                self?.comment = $0
        }
        
        let section = SectionFormer(rowFormer: commentRow)
        section.set(headerViewFormer: self?.createHeaderFunc(s: ""))
        return section
    }()
    
    func someFunc() -> SectionFormer {
        
        let createSelectorRow = { (
            text: String,
            subText: String,
            onSelected: ((RowFormer) -> Void)?
            ) -> RowFormer in
            return LabelRowFormer<FormLabelCell>() {
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
                $0.subTextLabel.textColor = .formerSubColor()
                $0.subTextLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                }.configure { form in
                    _ = onSelected.map { form.onSelected($0) }
                    form.text = text
                    form.subText = subText
                    form.rowHeight = 60.0
            }
        }
        
        let roleOptions = ["Мне должен", "Я должен"]
        
        var roleName: String = ""
        
        if self.initialDebitType == .ownerShouldGiveMoneyToClient {
            roleName = "Я должен"
        } else if self.initialDebitType == .ownerShouldTakeMoneyFromClient {
            roleName = "Мне должен"
        }
        
        let roleSelectorRow = createSelectorRow("Кто кому должен", roleName, pushSelectorRowSelected(options: roleOptions))
        
        let section = SectionFormer(rowFormer: roleSelectorRow)
        section.set(headerViewFormer: self.createHeaderFunc(s: ""))
        return section
    }
    
    private func pushSelectorRowSelected(options: [String]) -> (RowFormer) -> Void {
        return { [weak self] rowFormer in
            if let rowFormer = rowFormer as? LabelRowFormer<FormLabelCell> {
                let controller = TextSelectorViewContoller()
                controller.texts = options
                controller.selectedText = rowFormer.subText
                controller.onSelected = { [weak self] in
                    
                    rowFormer.subText = $0
                    rowFormer.update()
                    
                    if $0 == "Мне должен" {
                        print("Мне должен")
                        self?.initialDebitType = .ownerShouldTakeMoneyFromClient
                    } else if $0 == "Я должен" {
                        print("Я должен")
                        self?.initialDebitType = .ownerShouldGiveMoneyToClient
                    }
                }
                self?.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    private func createHeaderFunc(s: String) -> ViewFormer {
        return LabelViewFormer<FormLabelHeaderView>()
            .configure {
                $0.viewHeight = 15
                $0.text = s
        }
    }
    
    private func configure() {
        
        tableView.contentInset.top = 0
        tableView.contentInset.bottom = 0
        
        former.append(sectionFormer:self.nameCategorySection, self.commentInfoSection, someFunc())
            .onCellSelected { [weak self] _ in
                self?.formerInputAccessoryView.update()
        }
    }
}
