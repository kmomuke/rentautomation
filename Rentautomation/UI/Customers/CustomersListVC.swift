//
//  CustomersListVC.swift
//  Rentautomation
//
//  Created by kanybek on 11/29/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit
import MZFormSheetPresentationController
import CSNotificationView

class CustomersListVC: BaseListVC, ErrorPopoverRenderer, UITableViewDelegate, UITableViewDataSource {

    private var searchController: UISearchController!
    private var searchResultsVC: CustomerSearchResultsTVC!
    
    var notificationView: CSNotificationView?
    
    var tableView: UITableView
    var customers: [Customer] = []
    var disposeSet: DisposableSet?
    
    var initialString: String?
    var isComingFromOrder: Bool = false
    
    convenience init() {
        self.init(initialString: nil, isComingFromOrder: false)
    }
    
    init(initialString: String?, isComingFromOrder: Bool) {
        self.initialString = initialString
        self.isComingFromOrder = isComingFromOrder
        self.tableView = UITableView(frame: CGRect.zero, style: .plain)
        self.disposeSet = DisposableSet()
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.searchController?.view.removeFromSuperview()
        self.disposeSet?.dispose()
        self.disposeSet = nil
    }
    
    override func loadView() {
        super.loadView()
        
        if !self.isComingFromOrder {
            self.addCanceButton()
            self.title = "Клиенты"
        } else {
            self.title = "Выберите клиента"
        }
        
        
        
        self.tableView.register(CustomersListTVCell.self)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.view.addSubview(self.tableView)
        
        let addButton = UIBarButtonItem(
            title: "Добавить",
            style: .plain,
            target: self,
            action: #selector(createCustomer(_:))
        )
        
        let generateButton = UIBarButtonItem(
            title: "Сотрудник",
            style: .plain,
            target: self,
            action: #selector(staffPressed(_:))
        )
        
        self.navigationItem.rightBarButtonItems = [addButton, generateButton]

        self.searchResultsVC = CustomerSearchResultsTVC()
        self.searchResultsVC.customerSelectBlock = {[weak self]  (customer: Customer) -> Void in
            if let strongSelf = self {
                strongSelf.didSelectCustomer(customer)
            }
        }
        
        self.searchController = UISearchController(searchResultsController: self.searchResultsVC)
        self.searchController.searchResultsUpdater = self
        self.searchController.searchBar.sizeToFit()
        self.tableView.tableHeaderView = searchController.searchBar
        
        self.searchController.delegate = self
        self.searchController.dimsBackgroundDuringPresentation = false // default is YES
        self.searchController.searchBar.delegate = self    // so we can monitor text changes + others
        
        self.searchController.searchBar.barStyle = .default
        self.searchController.searchBar.placeholder = "Search"
        self.searchController.searchBar.backgroundColor = UIColor.white
        
        //let image = Global.shared.getImageWithColor(color: UIColor.groupTableViewBackground, size: CGSize(width: 600, height: 80))
        //self.searchController.searchBar.setSearchFieldBackgroundImage(image, for: .normal)
        
        /*
         Search is now just presenting a view controller. As such, normal view controller
         presentation semantics apply. Namely that presentation will walk up the view controller
         hierarchy until it finds the root view controller or one that defines a presentation context.
         */
        definesPresentationContext = true
        
        let customerCreateSignal: Signal = PipesStore.shared.customerCreatedSignal()
        let createCustomerDispose = customerCreateSignal.start(next: {[weak self] (customer: Customer) in
            self?.customers.insert(customer, at: 0)
            self?.tableView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
            self?.tableView.beginUpdates()
            self?.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
            self?.tableView.endUpdates()
        })
        self.disposeSet?.add(createCustomerDispose)
        
        let customerUpdateSignal: Signal = PipesStore.shared.customerUpdatedSignal()
        let updateCustomerDispose = customerUpdateSignal.start(next: {[weak self] (customer: Customer) in
            if let i = self?.customers.index(where: {$0.customerId == customer.customerId}) {
                self?.customers[i] = customer
                self?.tableView.reloadRows(at: [IndexPath(row: i, section: 0)], with: .fade)
            }
        })
        self.disposeSet?.add(updateCustomerDispose)
        
        let balanceUpdatedSignal: Signal = PipesStore.shared.balanceUpdatedSignal()
        let updateCustomerBalanceDispose = balanceUpdatedSignal.start(next: {[weak self] (customerId: UInt64) in
            if let indexRow = self?.customers.index(where: {$0.customerId == customerId}) {
                self?.tableView.reloadRows(at: [IndexPath(row: indexRow, section: 0)], with: .fade)
            }
        })
        self.disposeSet?.add(updateCustomerBalanceDispose)
        
        let allSyncFinishedSignal: Signal = PipesStore.shared.allSyncOperationsFinishedSignal()
        let allSyncFinishedDispose = allSyncFinishedSignal.start(next: {[weak self] (finished: Bool) in
            self?.reloadAllCustomers()
        })
        self.disposeSet?.add(allSyncFinishedDispose)   
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.reloadAllCustomers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let notifView = self.notificationView {
            notifView.setVisible(false, animated: true, completion: {Void in })
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = self.view.bounds
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func reloadAllCustomers() {
        Database.shared.loadAllCustomers(limit: Global.shared.limitForStaticModels, userId: 0) {[weak self] (customers_:[Customer]) in
            self?.customers = customers_
            self?.tableView.reloadData()
        }
    }
    
    func staffPressed(_ sender: UIBarButtonItem) {
        self.hideNotificationView()
        let staffListVC: StaffListVC = StaffListVC(initialString: nil, isCommingFromCustomer: true)
        staffListVC.staffSelectBlock = {[weak self]  (employee: User) -> Void in
            Database.shared.loadAllCustomers(limit: Global.shared.limitForStaticModels, userId: employee.userId) {[weak self] (customers_: [Customer]) in
                
                self?.customers = customers_
                self?.tableView.reloadData()
                
                let stringStr = "Сотрудник: \(employee.firstName) \(employee.secondName)"
                self?.showNotificationView(stringStr)
            }
        }
        
        staffListVC.modalPresentationStyle = UIModalPresentationStyle.popover
        staffListVC.preferredContentSize = CGSize(width: 400, height: 400)
        
        present(staffListVC, animated: true, completion: nil)
        
        let popoverPresentationController = staffListVC.popoverPresentationController
        popoverPresentationController?.delegate = self
        popoverPresentationController?.barButtonItem = sender
    }
    
    func createCustomer(_ sender: AnyObject) {
        
        let createCustomerVC = CreateCustomerVC()
        let navController = UINavigationController(rootViewController: createCustomerVC)
        
        let formSheetController = MZFormSheetPresentationViewController(contentViewController: navController)
        formSheetController.contentViewControllerTransitionStyle = .slideAndBounceFromBottom
        formSheetController.presentationController?.contentViewSize = CGSize(width: 800, height: 700)
        self.present(formSheetController, animated: true, completion:{_ in
        })
    }
    
    private func showNotificationView(_ message_: String) -> Void {
        
        self.notificationView = CSNotificationView(parentViewController: self,
                                                   tintColor: UIColor.midnightBlueColor(),
                                                   image: UIImage(named: "close-X")!,
                                                   message: message_)
        
        self.notificationView?.tapHandler = {[weak self] (Void) -> Void in
            self?.hideNotificationView()
        }
        
        self.notificationView?.setVisible(true, animated: true, completion: {[weak self] in
            
            if let strongSelf = self {
                
                let originalInset: UIEdgeInsets = strongSelf.tableView.contentInset
                var inset: UIEdgeInsets = originalInset
                inset.top = 10
                let originalContentOffset: CGPoint = strongSelf.tableView.contentOffset;
                let contentOffset: CGPoint = originalContentOffset;
                
                UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions(), animations: {
                    strongSelf.tableView.bounds = CGRect(x: 0, y: contentOffset.y, width: strongSelf.tableView.frame.size.width, height: strongSelf.tableView.frame.size.height)
                    strongSelf.tableView.contentInset = inset;
                    strongSelf.tableView.scrollIndicatorInsets = inset;
                }, completion: { (finished:Bool) in
                })
                print("show completed")
            }
        })
    }
    
    private func hideNotificationView() -> Void {
        if let notifView = self.notificationView {
            notifView.setVisible(false, animated: true, completion: {[weak self] in
                if let strongSelf = self {
                    strongSelf.tableView.contentInset.top = 0
                    strongSelf.reloadAllCustomers()
                    print("hide completed")
                }
            })
        }
    }
    
    // MARK: TableView DataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return customers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomersListTVCell", for: indexPath) as! CustomersListTVCell
        let object = customers[(indexPath as NSIndexPath).row]
        cell.customer = object
        cell.reloadData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            customers.remove(at: (indexPath as NSIndexPath).row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        let customer = customers[(indexPath as NSIndexPath).row]
        self.didSelectCustomer(customer)
    }
    
    func didSelectCustomer(_ customer: Customer) {
        
        //if Database.shared.isModelSynchronized(primaryKey: customer.customerId) {
            
            if isComingFromOrder {
                OrderMemomry.shared.customerId = customer.customerId
                OrderFilterMemory.shared.customerId = customer.customerId
                let _ = self.navigationController?.popViewController(animated: true)
            } else {
                let detailViewController = CustomerDescriptionVC(initialCustomer: customer)
                self.navigationController?.pushViewController(detailViewController, animated: true)
            }
            
        //} else {
        //    let _ = self.showErrorAlert(text: "Не синхронизирован")
        //}
    }
}

extension CustomersListVC:  UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating {
    //----------
    func presentSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    func willPresentSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    func didPresentSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    func willDismissSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    func didDismissSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    // MARK: UISearchResultsUpdating
    func updateSearchResults(for searchController: UISearchController) {
        print("updateSearchResultsForSearchController")
        if let searchText = searchController.searchBar.text {
            if searchText.characters.count > 0 {
                
                self.showLoading("Поиск")
                Database.shared.searchCustomerBy(name: searchText, limit: 100, completion: { (foundCustomers: [Customer]) in
                    self.dismissLoading()
                    let resultsController = searchController.searchResultsController as! CustomerSearchResultsTVC
                    resultsController.filteredCustomers = foundCustomers
                    resultsController.tableView.reloadData()
                })
            }
        }
    }
}

extension CustomersListVC: UIPopoverPresentationControllerDelegate {
    
    public func prepareForPopoverPresentation(_ popoverPresentationController: UIPopoverPresentationController) {
    }
    
    public func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
    
    public func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
    }
    
    public func popoverPresentationController(_ popoverPresentationController: UIPopoverPresentationController, willRepositionPopoverTo rect: UnsafeMutablePointer<CGRect>, in view: AutoreleasingUnsafeMutablePointer<UIView>) {
    }
}


