//
//  CreateCustomerVC.swift
//  Rentautomation
//
//  Created by kanybek on 12/5/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

class CreateCustomerVC: FormViewController, ErrorPopoverRenderer {

    var initialCustomer: Customer?
    var isKgPhoneMask: Bool = true
    
    convenience init() {
        self.init(initialCustomer: nil)
    }
    
    init(initialCustomer: Customer?) {
        self.initialCustomer = initialCustomer
        super.init(nibName: nil, bundle: nil)
        self.setUpInitialRows()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("CreateCustomerVC deallocated")
    }
    
    private func setUpInitialRows () -> Void {
        CustomerMemoryInstance.sharedInstance.clearAllVariables()
        if let currentEmployee = AppSession.shared.currentEmployee {
            CustomerMemoryInstance.sharedInstance.staffId = currentEmployee.userId
        }
        
        if let customer_ = self.initialCustomer {
            CustomerMemoryInstance.sharedInstance.firstName = customer_.firstName
            CustomerMemoryInstance.sharedInstance.secondName = customer_.secondName
            CustomerMemoryInstance.sharedInstance.phoneNumber = customer_.phoneNumber
            CustomerMemoryInstance.sharedInstance.address = customer_.address
            CustomerMemoryInstance.sharedInstance.staffId = customer_.userId
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure()
        
        self.title = "Создать/редактировать клиента"
        
        let canceButton = UIBarButtonItem(
            title: "Закрыть",
            style: .plain,
            target: self,
            action: #selector(cancelObject(_:))
        )
        
        self.navigationItem.leftBarButtonItem = canceButton
                
        let createButton = UIBarButtonItem(
            title: "Сохранить",
            style: .plain,
            target: self,
            action: #selector(createCustomer(_:))
        )
        
        self.navigationItem.rightBarButtonItem = createButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
        for row in self.neededProductSection.rowFormers {
            row.update()
        }
        
        if !AppSettings.shared.getCustomersStaff() {
            for (index, row) in self.staffSection.rowFormers.enumerated() {
                if (index == 0) {
                    row.enabled = false
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.neededProductSection.firstRowFormer?.former?.becomeEditingNext()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func cancelObject(_ sender: AnyObject) -> Void {
        
        CustomerMemoryInstance.sharedInstance.clearAllVariables()
        self.navigationController?.dismiss(animated: true, completion: {[weak self] in
            print("dismiss CreateCustomerVC")
        })
    }
    
    // MARK: CREATE
    func createCustomer(_ sender: AnyObject) -> Bool {
        
        let creatingCustomer: Customer
        var isUpdatingCustomer: Bool = false
        if let customer_ = self.initialCustomer {
            creatingCustomer = customer_
            isUpdatingCustomer = true
        } else {
            creatingCustomer = Customer()
            creatingCustomer.customerId = Database.shared.uniqueIncreasingPrimaryKey()
        }
        
        if let firstName_ = CustomerMemoryInstance.sharedInstance.firstName {
            creatingCustomer.firstName = firstName_
            if firstName_.characters.count < 2 {
                return showErrorAlert(text: "Имя")
            }
        } else {
            return showErrorAlert(text: "Имя")
        }
        
        if let secondName_ = CustomerMemoryInstance.sharedInstance.secondName {
            creatingCustomer.secondName = secondName_
        }
        
        if let address_ = CustomerMemoryInstance.sharedInstance.address {
            creatingCustomer.address = address_
        }
        
        if let phoneNumber_ = CustomerMemoryInstance.sharedInstance.phoneNumber {
            creatingCustomer.phoneNumber = phoneNumber_
            if phoneNumber_.characters.count < 2 {
                return showErrorAlert(text: "Тел.")
            }
        } else {
            return showErrorAlert(text: "Тел.")
        }
        
        if CustomerMemoryInstance.sharedInstance.staffId > 0 {
            creatingCustomer.userId = CustomerMemoryInstance.sharedInstance.staffId
        } else {
            return showErrorAlert(text: "Сотрудник клиента")
        }
        
        // ----------- Set inital balance for Customer, create Account ------------------
        if !isUpdatingCustomer {
            
            let account = Account()
            account.accountId = Database.shared.uniqueIncreasingPrimaryKey()
            account.customerId = creatingCustomer.customerId
            account.supplierId = 0
            account.balance = 0.0
            
            Database.shared.createAccounts([account])
            
            // ----------- Transaction test mode create for Customer ------------------
            if let employee_ = AppSession.shared.currentEmployee {
                
                let transactionId = Database.shared.uniqueIncreasingPrimaryKey()
                let transaction = Transaction(transactionId: transactionId,
                                              transactionDate: Date(),
                                              isLastTransaction: true,
                                              transactionType: .customerBalanceModified,
                                              moneyAmount: 0.0,
                                              orderId: 0,
                                              customerId: creatingCustomer.customerId,
                                              supplierId: 0,
                                              userId: employee_.userId,
                                              ballanceAmount: 0,
                                              comment: "",
                                              transactionUuid: UUID().uuidString.lowercased())
                
                Database.shared.createTransactions([transaction])
                
                let operation = Event(syncOperationId: Database.shared.operationCid(),
                                              isSynchronized: false,
                                              syncOperationDate: Date(),
                                              operationType: .customerCreated,
                                              productId: 0,
                                              orderDetailId: 0,
                                              customerId: creatingCustomer.customerId,
                                              transactionId: transaction.transactionId,
                                              accountId: account.accountId,
                                              supplierId: 0,
                                              staffId: 0,
                                              categoryId: 0,
                                              orderId: 0)
                
                Database.shared.createOperations([operation])
            }
        }
        
        if isUpdatingCustomer {
            
            if Database.shared.isModelSynchronized(primaryKey: creatingCustomer.customerId) {
                let operation = Event(syncOperationId: Database.shared.operationCid(),
                                              isSynchronized: false,
                                              syncOperationDate: Date(),
                                              operationType: .customerUpdated,
                                              productId: 0,
                                              orderDetailId: 0,
                                              customerId: creatingCustomer.customerId,
                                              transactionId: 0,
                                              accountId: 0,
                                              supplierId: 0,
                                              staffId: 0,
                                              categoryId: 0,
                                              orderId: 0)
                Database.shared.createOperations([operation])
            }
            
            Database.shared.updateCustomer(creatingCustomer)
            PipesStore.shared.customerUpdatedPipe.putNext(creatingCustomer)
        } else {
            Database.shared.createCustomers([creatingCustomer])
            PipesStore.shared.customerCreatedPipe.putNext(creatingCustomer)
        }
        
        CustomerMemoryInstance.sharedInstance.clearAllVariables()
        
        self.navigationController?.dismiss(animated: true, completion: {[weak self] in
            print("dismiss CreateCustomerVC")
        })
        
        return true
    }
    
    // MARK: Private
    private lazy var formerInputAccessoryView: FormerInputAccessoryView = FormerInputAccessoryView(former: self.former)
    
    // MARK: Needed product section
    private lazy var neededProductSection: SectionFormer = { [weak self] in
        
        let nameRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            $0.titleLabel.text = "Имя"
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.autocapitalizationType = .words
            $0.textField.returnKeyType = .next
            }.configure { [weak self] in
                $0.placeholder = "Имя"
                $0.text = ""
                $0.rowHeight = 50
                if let customer_ = self?.initialCustomer {
                    $0.text = customer_.firstName
                }
            }.onTextChanged {
                CustomerMemoryInstance.sharedInstance.firstName = $0
        }
        
        let secondNameRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            $0.titleLabel.text = "Фамил."
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.returnKeyType = .next
            $0.textField.autocapitalizationType = .words
            }.configure { [weak self] in
                $0.placeholder = "Фамил."
                $0.text = ""
                $0.rowHeight = 50
                if let customer_ = self?.initialCustomer {
                    $0.text = customer_.secondName
                }
            }.onTextChanged {
                CustomerMemoryInstance.sharedInstance.secondName = $0
        }
        
        let phoneRow = TextFieldRowFormer<PhoneFiledTVCell>() { [weak self] in
            $0.titleLabel.text = "Тел"
            $0.textField.keyboardType = .numberPad
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.returnKeyType = .next
            }.configure { [weak self] in
                if let customer_ = self?.initialCustomer {
                    $0.text = customer_.phoneNumber
                }
            }.onTextChanged {
                CustomerMemoryInstance.sharedInstance.phoneNumber = $0
            }.onUpdate { [weak self] in
                if let strongSelf = self {
                    
                    if let customer_ = strongSelf.initialCustomer {
                        $0.text = CustomerMemoryInstance.sharedInstance.phoneNumber
                        $0.updateCellPhoneMask(isOn: strongSelf.isKgPhoneMask)
                    } else {
                        $0.text = ""
                        $0.updateCellPhoneMask(isOn: strongSelf.isKgPhoneMask)
                    }
                }
        }
        
        let switchDateStyleRow = SwitchRowFormer<FormSwitchCell>() {
            $0.titleLabel.text = "Формат тел КГ?"
            $0.titleLabel.textColor = .formerColor()
            $0.titleLabel.font = .boldSystemFont(ofSize: 14)
            $0.switchButton.onTintColor = .formerSubColor()
            }.configure { [weak self] in
                $0.rowHeight = 50
                if let strongSelf = self {
                    $0.switched = strongSelf.isKgPhoneMask
                }
            }.onSwitchChanged {[weak self] switched in
                self?.isKgPhoneMask = switched
                phoneRow.update()
        }
        
        let section = SectionFormer(rowFormer: nameRow, secondNameRow, phoneRow, switchDateStyleRow)
        section.set(headerViewFormer: self?.createHeaderFunc(s: " "))
        return section
    }()
    
    // MARK: Needed product section
    private lazy var staffSection: SectionFormer = {
        
        let createButtonRow1: ((String, (() -> Void)?) -> RowFormer) = { [weak self] text, onSelected in
            return LabelRowFormer<FormLabelCell>() {
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                $0.subTextLabel.textColor = .formerSubColor()
                $0.subTextLabel.font = .boldSystemFont(ofSize: 14)
                
                }.configure {
                    $0.text = text
                    $0.rowHeight = 50
                }.onSelected { [weak self] _ in
                    onSelected?()
                    self?.former.deselect(animated: true)
                }.onUpdate { [weak self] in
                    var emplyeeName: String = ""
                    if (CustomerMemoryInstance.sharedInstance.staffId > 0) {
                        if let emplyee = Database.shared.userBy(userId: CustomerMemoryInstance.sharedInstance.staffId) {
                            emplyeeName = emplyee.firstName + " " + emplyee.secondName
                        }
                    }
                    if (emplyeeName.characters.count > 0) {
                        $0.subText = "\(emplyeeName)"
                    }
            }
        }
        
        let customerStaffRow = createButtonRow1("Сотрудник") { [weak self] in
            let staffList: StaffListVC = StaffListVC(initialString: nil, isCommingFromCustomer: true)
            self?.navigationController?.pushViewController(staffList, animated: true)
        }
        
        let section = SectionFormer(rowFormer: customerStaffRow)
        return section
    }()
    
    private lazy var addressInfoSection: SectionFormer = { [weak self] in
        
        let addressRow = TextViewRowFormer<FormTextViewCell>() { [weak self] in
            $0.textView.textColor = .formerSubColor()
            $0.textView.font = .systemFont(ofSize: 20)
            $0.textView.inputAccessoryView = self?.formerInputAccessoryView
            }.configure { [weak self] in
                $0.placeholder = "Адрес"
                if let customer_ = self?.initialCustomer {
                    $0.text = customer_.address
                }
            }.onTextChanged {
                CustomerMemoryInstance.sharedInstance.address = $0
        }
        
        let section = SectionFormer(rowFormer: addressRow)
        section.set(headerViewFormer: self?.createHeaderFunc(s: "Адрес"))
        return section
    }()
    
    
    private func createHeaderFunc(s: String) -> ViewFormer {
        return LabelViewFormer<FormLabelHeaderView>()
            .configure {
                $0.viewHeight = 30
                $0.text = s
        }
    }
    
    private func configure() {
        
        tableView.contentInset.top = 0
        tableView.contentInset.bottom = 0
        
        // Create Headers
        /*let createHeader: ((String) -> ViewFormer) = { text in
         return LabelViewFormer<FormLabelHeaderView>()
         .configure {
         $0.viewHeight = 40
         $0.text = text
         }
         }*/
        
        former.append(sectionFormer:self.neededProductSection, self.addressInfoSection, self.staffSection)
            .onCellSelected { [weak self] _ in
                self?.formerInputAccessoryView.update()
        }
    }
}
