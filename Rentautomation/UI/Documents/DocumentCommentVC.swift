//
//  DocumentCommentVC.swift
//  Rentautomation
//
//  Created by kanybek on 3/1/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit


class DocumentCommentVC: FormViewController, ErrorPopoverRenderer {
    
    var initialOrder: Order?
    
    convenience init() {
        self.init(initialOrder: nil)
    }
    
    init(initialOrder: Order?) {
        self.initialOrder = initialOrder
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    func setUpInitialValues() -> Void {
        if let order_ = self.initialOrder {
            OrderMemomry.shared.billingNo = order_.billingNo
            OrderMemomry.shared.comments = order_.comment
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpInitialValues()
        configure()
        
        self.title = "Коментарии"
        
        let canceButton = UIBarButtonItem(
            title: "Закрыть",
            style: .plain,
            target: self,
            action: #selector(cancelObject(_:))
        )
        
        self.navigationItem.leftBarButtonItem = canceButton
        
        let createButton = UIBarButtonItem(
            title: "Сохранить",
            style: .plain,
            target: self,
            action: #selector(saveComment(_:))
        )
        
        self.navigationItem.rightBarButtonItem = createButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.addressInfoSection.firstRowFormer?.former?.becomeEditingNext()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func saveComment(_ sender: AnyObject) -> Void {
        
        if let order = self.initialOrder {
            if Database.shared.isModelSynchronized(primaryKey: order.orderId) {
                let operation = Event(syncOperationId: Database.shared.operationCid(),
                                              isSynchronized: false,
                                              syncOperationDate: Date(),
                                              operationType: .orderUpdated,
                                              productId: 0,
                                              orderDetailId: 0,
                                              customerId: 0,
                                              transactionId: 0,
                                              accountId: 0,
                                              supplierId: 0,
                                              staffId: 0,
                                              categoryId: 0,
                                              orderId: order.orderId)
                Database.shared.createOperations([operation])
            }
            order.comment = OrderMemomry.shared.comments
            order.billingNo = OrderMemomry.shared.billingNo
            Database.shared.updateOrdersCommentAndBillingNo(comment: OrderMemomry.shared.comments,
                                                            billingNo: OrderMemomry.shared.billingNo,
                                                            orderId: order.orderId)
        }
        
        self.navigationController?.dismiss(animated: true, completion: {[weak self] in
            print("cancelObject")
        })
    }
    
    // MARK: CREATE
    func cancelObject(_ sender: AnyObject) -> Bool {
        self.navigationController?.dismiss(animated: true, completion: {[weak self] in
            print("createCustomer")
        })
        return true
    }
    
    // MARK: Private
    private lazy var formerInputAccessoryView: FormerInputAccessoryView = FormerInputAccessoryView(former: self.former)
    
    // MARK: Needed product section
    private lazy var billingNoSection: SectionFormer = { [weak self] in
        
        let billingNoRow = TextViewRowFormer<FormTextViewCell>() { [weak self] in
            $0.textView.textColor = .formerSubColor()
            $0.textView.font = .systemFont(ofSize: 20)
            $0.textView.inputAccessoryView = self?.formerInputAccessoryView
            }.configure {
                $0.placeholder = "Текст документа"
                $0.text = OrderMemomry.shared.billingNo
            }.onTextChanged {
                OrderMemomry.shared.billingNo = $0
        }
        
        let section = SectionFormer(rowFormer: billingNoRow)
        section.set(headerViewFormer: self?.createHeaderFunc(s: "Текст документа"))
        return section
    }()
    
    private lazy var addressInfoSection: SectionFormer = { [weak self] in
        
        let addressRow = TextViewRowFormer<FormTextViewCell>() { [weak self] in
            $0.textView.textColor = .formerSubColor()
            $0.textView.font = .systemFont(ofSize: 20)
            $0.textView.inputAccessoryView = self?.formerInputAccessoryView
            }.configure {
                $0.placeholder = "Комментарии"
                $0.text = OrderMemomry.shared.comments
            }.onTextChanged {
                OrderMemomry.shared.comments = $0
        }
        
        let section = SectionFormer(rowFormer: addressRow)
        section.set(headerViewFormer: self?.createHeaderFunc(s: "Добавить комментарии"))
        section.set(footerViewFormer: self?.createFooterFunc(s: ""))
        return section
    }()
    
    private func createHeaderFunc(s: String) -> ViewFormer {
        return LabelViewFormer<FormLabelHeaderView>()
            .configure {
                $0.viewHeight = 20
                $0.text = s
        }
    }

    private func createFooterFunc(s: String) -> ViewFormer {
        return LabelViewFormer<FormLabelFooterView>()
            .configure {
                $0.text = s
                $0.viewHeight = 40
        }
    }


    private func configure() {
        
        tableView.contentInset.top = 20
        tableView.contentInset.bottom = 20
        
        former.append(sectionFormer:self.addressInfoSection, self.billingNoSection)
            .onCellSelected { [weak self] _ in
                self?.formerInputAccessoryView.update()
        }
    }
}
