//
//  MainDocumentVC.swift
//  Rentautomation
//
//  Created by kanybek on 12/4/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import AFDateHelper
import SwiftSignalKit
import SVProgressHUD

class MainDocumentVC: BaseListVC, ErrorPopoverRenderer {
    
    @IBOutlet weak var searchTextfield: UITextField!
    
    @IBOutlet weak var productsView: UIView!
    @IBOutlet weak var ordersView: UIView!
    @IBOutlet weak var totalAmountView: UIView!
    
    @IBOutlet weak var totalSumLabel: UILabel!
    @IBOutlet weak var totalSumLabelWithDiscount: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    
    @IBOutlet weak var scannerButton: UIButton!
    @IBOutlet weak var chooseCategoryButton: UIButton!
    @IBOutlet weak var clientButton: UIButton!
    @IBOutlet weak var discountButton: UIButton!
    @IBOutlet weak var makeOrderButton: UIButton!
    @IBOutlet weak var orderInfoButton: UIButton!
    
    /*
    var searchString: String = ""
    // In a UIView/UIViewController subclass:
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    override var keyCommands: [UIKeyCommand]? {
        let km0 = UIKeyCommand(input: "0", modifierFlags: [], action: #selector(pressed0))
        let km1 = UIKeyCommand(input: "1", modifierFlags: [], action: #selector(pressed1))
        let km2 = UIKeyCommand(input: "2", modifierFlags: [], action: #selector(pressed2))
        let km3 = UIKeyCommand(input: "3", modifierFlags: [], action: #selector(pressed3))
        let km4 = UIKeyCommand(input: "4", modifierFlags: [], action: #selector(pressed4))
        let km5 = UIKeyCommand(input: "5", modifierFlags: [], action: #selector(pressed5))
        let km6 = UIKeyCommand(input: "6", modifierFlags: [], action: #selector(pressed6))
        let km7 = UIKeyCommand(input: "7", modifierFlags: [], action: #selector(pressed7))
        let km8 = UIKeyCommand(input: "8", modifierFlags: [], action: #selector(pressed8))
        let km9 = UIKeyCommand(input: "9", modifierFlags: [], action: #selector(pressed9))
        let kmEnter = UIKeyCommand(input: "\r", modifierFlags: [], action: #selector(enterPressed))
        
        return [km0, km1, km2, km3, km4, km5, km6, km7, km8, km9, kmEnter]
    }
    
    @objc func pressed0() { self.searchString = self.searchString + "0"}
    @objc func pressed1() { self.searchString = self.searchString + "1"}
    @objc func pressed2() { self.searchString = self.searchString + "2"}
    @objc func pressed3() { self.searchString = self.searchString + "3"}
    @objc func pressed4() { self.searchString = self.searchString + "4"}
    @objc func pressed5() { self.searchString = self.searchString + "5"}
    @objc func pressed6() { self.searchString = self.searchString + "6"}
    @objc func pressed7() { self.searchString = self.searchString + "7"}
    @objc func pressed8() { self.searchString = self.searchString + "8"}
    @objc func pressed9() { self.searchString = self.searchString + "9"}
     
    @objc func enterPressed() {
         print("barcode \(self.searchString)")
         self.searchProductFor(barcode: self.searchString)
         self.searchString = ""
    }
    */
    
    func searchProductFor(barcode: String) {
        if let product_ = Database.shared.productFor(barcode: barcode) {
            if let _ = self.prodcutListSelectTVC.badgeDict[product_.productId] {
                self.prodcutListSelectTVC.badgeDict[product_.productId] = 1
            }
            if let indexFound = self.prodcutListSelectTVC.products.index(where: {$0.productId == product_.productId}) {
                self.prodcutListSelectTVC.tableView.reloadRows(at: [IndexPath(row: indexFound, section: 0)], with: .fade)
            }
            PipesStore.shared.productSelectedPipe.putNext((product_, self.prodcutListSelectTVC.shouldUseSalePrice))
        } else {
            let _ = self.showErrorAlert(text: "Не найдено!")
        }
    }
    
    lazy var titleView: TitleView = {
        let view = TitleView()
        return view
    }()
    
    override var title: String? {
        set {
            titleView.titleLabel.text = newValue
        }
        get {
            return titleView.titleLabel.text
        }
    }

    var documentCompanion: MainDocumentCompanion?
    
    var currentEmployee: User?
    var initialOrder: Order?
    var orderDocument: OrderDocumentType
    
    var totalPrice: Double = 0.0
    var totalDiscount: Discount = Discount()
    
    var isFirstTime: Bool = true
    var totalSelectedOrederDetailsDispose: Disposable?
    
    var orderDetails: [OrderDetail]?
    
    lazy var prodcutListSelectTVC: ProductsSelectTVC = { [weak self] in
        let productListVC = ProductsSelectTVC(initialOrderDocument: (self?.orderDocument)!, initialOrder: self?.initialOrder)
        return productListVC
    }()
    
    private lazy var selectedProductListTVC: SelectedProductsTVC = { [weak self] in
        let selectTVC = SelectedProductsTVC(initialOrder: self?.initialOrder)
        return selectTVC
    }()
    
    convenience init() {
        self.init(orderDocument: .none, initalOrder: nil)
    }
    
    init(orderDocument: OrderDocumentType, initalOrder: Order?) {
        self.orderDocument = orderDocument
        self.initialOrder = initalOrder
        
        if let initialOrder_ = self.initialOrder {
            if let employee_ = Database.shared.userBy(userId: initialOrder_.userId) {
                self.currentEmployee = employee_
            }
        }
        
        if let _ = self.currentEmployee {
        } else {
            self.currentEmployee = AppSession.shared.currentEmployee
        }
        
        self.documentCompanion = MainDocumentCompanion(orderDocument: orderDocument, initalOrder: initalOrder)
        OrderMemomry.shared.clearAllVariables()
        OrderMemomry.shared.orderDocument = orderDocument
        super.init(nibName: "MainDocumentVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("MainDocumentVC deleted")
    }
    
    internal func focus(gesture: UITapGestureRecognizer) {
        //let point = gesture.location(in: self.view)
        DispatchQueue.main.async {
            UIApplication.shared.keyWindow?.endEditing(true)
        }
    }
    
    private func setUpBillingNo() -> Void {
        if let employee_ = self.currentEmployee {
            let date_ = Date()
            let key = Database.shared.uniqueIncreasingPrimaryKey()
            let str = "Документ №\(key) \nот \(date_.toString(.custom("EEEE, dd MMMM yyyy HH:mm"))) \nсотрудник: \(employee_.firstName) \(employee_.secondName)"
            OrderMemomry.shared.billingNo = str
            self.orderInfoButton.setTitle(str, for: .normal)
        }
    }
    
    private func setUpInitialLabelTexts() -> Void {
        self.setUpBillingNo()
        self.totalSumLabel.text = ""
    }
    
    private func setUpInitialOrderForEddit() -> Void {
        
        if let order_ = self.initialOrder {
            OrderMemomry.shared.customerId = order_.customerId
            if self.orderDocument == .receiveFromSupplier {
                OrderMemomry.shared.customerId = 0
            }
            OrderMemomry.shared.supplierId = order_.supplierId
            OrderMemomry.shared.moneyMovement = order_.moneyMovementType
            OrderMemomry.shared.comments = order_.comment
            OrderMemomry.shared.billingNo = order_.billingNo
            
            self.orderDetails = Database.shared.orderDetailsBy(orderId: order_.orderId)
            var totalAmount: Double = 0.0
            if let orderDetail_ = self.orderDetails {
                for orderDetail in orderDetail_ {
                    totalAmount = totalAmount + orderDetail.orderQuantity
                }
            }
            
            if let payment = Database.shared.paymentBy(paymentId: order_.paymentId) {
                self.totalPrice = payment.totalOrderPrice
                self.totalDiscount = Discount(payment: payment)
            }
            
            self.orderInfoButton.setTitle(order_.billingNo, for: .normal)
            self.totalSumLabel.text = Global.shared.priceFormatter.string(for: self.totalPrice)
            let totalPriceWithDiscount = self.totalDiscount.totalPriceWithDiscount()
            self.totalSumLabelWithDiscount.text = Global.shared.priceFormatter.string(for: totalPriceWithDiscount)
            self.totalAmountLabel.text = Global.shared.amountFormatter.string(for: totalAmount)
            
            self.updateDisctountButtonTitle()
        }
    }
    
    func updateDisctountButtonTitle() {
        let diffValue = self.totalDiscount.finlaDifferenceValue()
        let finalValue = Global.shared.amountFormatter.string(for: (0 - diffValue))!
        self.discountButton.setTitle(finalValue, for: .normal)
        
        let totalPriceWithDiscount = self.totalDiscount.totalPriceWithDiscount()
        self.totalSumLabelWithDiscount.text = Global.shared.priceFormatter.string(for: totalPriceWithDiscount)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.searchTextfield.delegate = self
        self.searchTextfield.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        self.orderInfoButton.titleLabel!.lineBreakMode = NSLineBreakMode.byWordWrapping
        self.orderInfoButton.titleLabel?.numberOfLines = 0
        
        navigationItem.titleView = self.titleView
        self.titleView.titleLabel.text = ""
        
        if let gestureRecognizers = self.view.gestureRecognizers {
            gestureRecognizers.forEach({ self.view.removeGestureRecognizer($0) })
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(focus(gesture:)))
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)
        
        let canceButton = UIBarButtonItem(
            title: "Вернутся на Главную",
            style: .plain,
            target: self,
            action: #selector(dismiss(_:))
        )
        self.navigationItem.leftBarButtonItem = canceButton
        
        self.makeOrderButton.titleLabel?.numberOfLines = 0
        self.titleView.titleLabel.textColor = Order.orderDocumentTypeColor(orderDocType: orderDocument)
        switch orderDocument {
        
        case .saleToCustomer:
            self.title = "Продажа"
            self.clientButton.setTitle("Клиент", for: .normal)
            self.makeOrderButton.setTitle("Оформить продажу", for: .normal)
            self.discountButton.isEnabled = true
            if AppSettings.shared.shouldUseDefaultCustomer() {
                if let customer_ = Database.shared.customerBy(customerName: "Розничный") {
                    OrderMemomry.shared.customerId = customer_.customerId
                }
            }
            self.setUpInitialLabelTexts()
            self.setUpInitialOrderForEddit()
            
        case .receiveFromSupplier:
            self.title = "Закупка"
            self.clientButton.setTitle("Поставщик", for: .normal)
            self.makeOrderButton.setTitle("Оформить закупку", for: .normal)
            self.discountButton.isEnabled = true
            self.setUpInitialLabelTexts()
            self.setUpInitialOrderForEddit()
            
        case .returnFromCustomer:
            self.title = "Возврат продукта от клиента"
            self.clientButton.setTitle("От клиента", for: .normal)
            self.makeOrderButton.setTitle("Оформить возврат от", for: .normal)
            self.discountButton.isEnabled = true
            self.setUpInitialLabelTexts()
            
        case .returnToSupplier:
            self.title = "Возврат продукта к потавщику"
            self.clientButton.setTitle("К поставщику", for: .normal)
            self.makeOrderButton.setTitle("Оформить возврат к", for: .normal)
            self.discountButton.isEnabled = true
            self.setUpInitialLabelTexts()
            
        case .saleToCustomerEdited:
            self.title = "Редактирование Продажи"
            self.makeOrderButton.setTitle("Оформить редактирование продажи", for: .normal)
            self.setUpInitialOrderForEddit()
            
        case .receiveFromSupplierEdited:
            self.title = "Редактирование Закупки"
            self.makeOrderButton.setTitle("Оформить редактирование закупки", for: .normal)
            self.setUpInitialOrderForEddit()
            
        case .preOrderToCustomer, .preOrderToCustomerEdited:
            self.title = "Заказ для клиента"
            self.clientButton.setTitle("Клиент", for: .normal)
            self.makeOrderButton.setTitle("Оформить заказ", for: .normal)
            self.discountButton.isEnabled = true
            
            self.setUpInitialLabelTexts()
            self.setUpInitialOrderForEddit()
            
        case .stockTaking:
            self.title = "Инвентаризация / Черновик"
            self.clientButton.setTitle("Клиент", for: .normal)
            self.clientButton.isEnabled = true
            self.makeOrderButton.setTitle("Сохранить документ", for: .normal)
            self.discountButton.isEnabled = true
            self.setUpInitialLabelTexts()
        
        case .rawProductGoneToProd:
            self.title = "Расход сырья"
            self.clientButton.isHidden = true
            self.makeOrderButton.setTitle("Сохранить документ", for: .normal)
            self.discountButton.isHidden = true
            self.setUpInitialLabelTexts()
            
        case .rawProductGoneToProdEdited:
            self.title = "Редактирование расход сырья"
            self.clientButton.isHidden = true
            self.makeOrderButton.setTitle("Сохранить документ", for: .normal)
            self.discountButton.isHidden = true
            self.setUpInitialLabelTexts()
            
        case .readyProductReceivedFromProd:
            self.title = "Выход готовой продукции"
            self.clientButton.isHidden = true
            self.makeOrderButton.setTitle("Сохранить документ", for: .normal)
            self.discountButton.isHidden = true
            self.setUpInitialLabelTexts()
            
        case .readyProductReceivedFromProdEdited:
            self.title = "Редактирование выход готовой продукции"
            self.clientButton.isHidden = true
            self.makeOrderButton.setTitle("Сохранить документ", for: .normal)
            self.discountButton.isHidden = true
            self.setUpInitialLabelTexts()
            
        default:
           print("default:")
        }
        
        let totalSelectedOrederDetailsSignal: Signal = PipesStore.shared.totalSelectedOrderDetailsSignal()
        self.totalSelectedOrederDetailsDispose = totalSelectedOrederDetailsSignal.start(next: {[weak self] (orderDetails: [OrderDetail]) in
            
            if let strongSelf = self {
                
                strongSelf.orderDetails = orderDetails
                
                var totalPrice_: Double = 0.0
                var totalAmount: Double = 0.0
                for orderDetail in orderDetails {
                    totalAmount = totalAmount + orderDetail.orderQuantity
                    let totalPriceWithDiscount = orderDetail.price * orderDetail.orderQuantity
                    totalPrice_ = totalPrice_ + totalPriceWithDiscount
                }
                
                strongSelf.totalPrice = totalPrice_
                strongSelf.totalDiscount.totalPrice = totalPrice_
                
                strongSelf.totalSumLabel.text = Global.shared.priceFormatter.string(for: strongSelf.totalPrice)
                strongSelf.totalAmountLabel.text = Global.shared.amountFormatter.string(for: totalAmount)
                
                let totalPriceWithDiscount = strongSelf.totalDiscount.totalPriceWithDiscount()
                strongSelf.totalSumLabelWithDiscount.text = Global.shared.priceFormatter.string(for: totalPriceWithDiscount)
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateUIElemts()
        if (isFirstTime) {
            self.add(asChildViewController: self.prodcutListSelectTVC, frame_: self.productsView.frame)
            self.add(asChildViewController: self.selectedProductListTVC, frame_: self.ordersView.frame)
            isFirstTime = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()   
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        if let text_ = textField.text {
            print("searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)")
            if text_.characters.count > 0 {
                self.showLoading("Поиск")
                Database.shared.search(productName: text_, completion: {[weak self] (products_: [Product]) in
                    self?.dismissLoading()
                    self?.prodcutListSelectTVC.reloadTableData(products_)
                })
            } else {
                Database.shared.loadAllProductsBy(categoryId: 0,
                                                  limit: Global.shared.limitForStaticModels,
                                                  completion: {[weak self] (products_: [Product]) in
                                                    self?.prodcutListSelectTVC.reloadTableData(products_)
                })
            }
        }
    }
    
    func updateUIElemts() -> Void {
        
        if OrderMemomry.shared.customerId > 0 {
            let customerId = OrderMemomry.shared.customerId
            if let customer__ = Database.shared.customerBy(customerId:customerId) {
                
                self.clientButton.setTitle("\(customer__.firstName) \(customer__.secondName)", for: .normal)
                
                if AppSettings.shared.getCustomersStaff() {
                    if let employee_ = Database.shared.userBy(userId: customer__.userId) {
                        self.currentEmployee = employee_
                        self.setUpBillingNo()
                    } else {
                        self.currentEmployee = AppSession.shared.currentEmployee
                        self.setUpBillingNo()
                    }
                } else {
                    self.currentEmployee = AppSession.shared.currentEmployee
                    self.setUpBillingNo()
                }
                
            }
        } else if OrderMemomry.shared.supplierId > 0 {
            let supplierId = OrderMemomry.shared.supplierId
            if let supplier__ = Database.shared.supplierBy(supplierId: supplierId) {
                self.clientButton.setTitle(supplier__.companyName, for: .normal)
            }
        }
    }

    private func clearAndDismiss() {
        OrderMemomry.shared.clearAllVariables()
        self.navigationController?.dismiss(animated: true, completion: {
            print("dismiss MainDocumentVC")
        })
    }
    
    func dismiss(_ sender: AnyObject) -> Void {
        if let oDetails_ = self.orderDetails {
            if ((self.orderDocument == .saleToCustomer ||
                self.orderDocument == .receiveFromSupplier)
                && (oDetails_.count > 0)
                && AppSettings.shared.getOrderDraftMode())
            {
                let alert: UIAlertController = UIAlertController(title: "Сохранить черновик?",
                                                                 message: "",
                                                                 preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Сохранить", style: .default, handler: {[weak self]
                    (UIAlertAction) -> Void in
                    if let strongSelf = self {
                        if let employee_ = strongSelf.currentEmployee {
                                strongSelf.documentCompanion?.performSaveOrder(senderVC: strongSelf,
                                                                               employee: employee_,
                                                                               orderDetails: oDetails_,
                                                                               orderDocType: .stockTaking,
                                                                               totalPrice: strongSelf.totalPrice,
                                                                               totalDiscount: strongSelf.totalDiscount)
                        }
                    }
                }))
                alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: {[weak self]
                    (UIAlertAction) -> Void in
                    self?.clearAndDismiss()
                }))
                self.present(alert, animated: true, completion: nil)
            } else {
                self.clearAndDismiss()
            }
        } else {
            self.clearAndDismiss()
        }
    }
    
    @IBAction func orderInfoButtonPressed(_ sender: UIButton) {
        let documentCommentVC = DocumentCommentVC()
        self.presentSheet(viewController: documentCommentVC)
    }
    
    @IBAction func categoryButtonPressed(_ sender: UIButton) {
        
        let laserScanerVC = ScannerLaserVC()
        laserScanerVC.scanerScannedBlock = {[weak self] (barcode: String) -> Void in
            self?.searchProductFor(barcode: barcode)
        }
        
        let categoriesListVC = CategoriesListVC(initialString: "comeFromSaleOrder", isCommingFromProduct: true)
        categoriesListVC.categorySelectBlock = {[weak self]  (category: Category) -> Void in
            print("category.name = \(category.categoryName)")
            Database.shared.loadAllProductsBy(categoryId: category.categoryId,
                                              limit: Global.shared.limitForStaticModels,
                                              completion: {[weak self] (products_: [Product]) in
                                                self?.prodcutListSelectTVC.reloadTableData(products_)
                                                self?.prodcutListSelectTVC.showNotificationView(category.categoryName)
            })
        }
        
        let suppliersListVC = SuppliersListVC(initialString: "comeFromSaleOrder", isCommingFromProduct: true, isComingFromOrder: false)
        suppliersListVC.supplierSelectBlock = {[weak self]  (supplier: Supplier) -> Void in
            print("supplier.companyName = \(supplier.companyName)")
            Database.shared.loadAllProductsBy(supplierId: supplier.supplierId,
                                              limit: Global.shared.limitForStaticModels,
                                              completion: {[weak self] (products_: [Product]) in
                                                self?.prodcutListSelectTVC.reloadTableData(products_)
                                                self?.prodcutListSelectTVC.showNotificationView(supplier.companyName)
            })
        }
        
        let targetName = Global.shared.targetName
        var navController: UINavigationController!
        if targetName == "TazaPos-lite" || targetName == "TazaProd" {
            navController = UINavigationController(rootViewController: categoriesListVC)
        } else {
            let tableTVC = ProductAdditionsTVC(laserScanerVC: laserScanerVC,
                                               categoriesListVC: categoriesListVC,
                                               suppliersListVC: suppliersListVC)
            
            navController = UINavigationController(rootViewController: tableTVC)
        }
         
        navController.modalPresentationStyle = UIModalPresentationStyle.popover
        navController.preferredContentSize = CGSize(width: 400, height: 500)
        
        present(navController, animated: true, completion: nil)
        
        let popoverPresentationController = navController.popoverPresentationController
        popoverPresentationController?.delegate = self
        popoverPresentationController?.sourceView = sender
        popoverPresentationController?.sourceRect = CGRect(x: 0, y: 0, width: sender.frame.size.width, height: sender.frame.size.height)
    }
    
    @IBAction func scanerButtonPressed(_ sender: UIButton) {
        
        let scanerVC = ScanQRCodeVC()
        scanerVC.scanerScannedBlock = {[weak self] (barcode: String) -> Void in
            self?.searchProductFor(barcode: barcode)
        }
        
        scanerVC.modalPresentationStyle = UIModalPresentationStyle.popover
        scanerVC.preferredContentSize = CGSize(width: 400, height: 400)
        
        present(scanerVC, animated: true, completion: nil)
        
        let popoverPresentationController = scanerVC.popoverPresentationController
        popoverPresentationController?.delegate = self
        popoverPresentationController?.sourceView = sender
    }

    @IBAction func customerDidPressed(_ sender: Any) {
        
        switch orderDocument {
        case .saleToCustomer, .returnFromCustomer, .saleToCustomerEdited, .preOrderToCustomer, .stockTaking:
            
            let customerListVC = CustomersListVC(initialString: nil, isComingFromOrder: true)
            self.navigationController?.pushViewController(customerListVC, animated: true)
            
        case .receiveFromSupplier, .returnToSupplier, .receiveFromSupplierEdited:
            
            let customerListVC = SuppliersListVC(initialString: nil, isCommingFromProduct: false, isComingFromOrder: true)
            self.navigationController?.pushViewController(customerListVC, animated: true)
        default:
            print("_")
        }
    }
    
    @IBAction func discountButtonPressed(_ sender: UIButton) {
        
        let additionalPaymsVC = AdditionalPaymentVC(initialDiscount: self.totalDiscount)
        additionalPaymsVC.dicsountSelectBlock = {[weak self]  (discount: Double) -> Void in
            if let strongSelf = self {
                strongSelf.totalDiscount.discount = discount
                strongSelf.updateDisctountButtonTitle()
            }
        }
        additionalPaymsVC.minusSelectBlock = {[weak self]  (minusPrice: Double) -> Void in
            if let strongSelf = self {
                strongSelf.totalDiscount.minusPrice = minusPrice
                strongSelf.updateDisctountButtonTitle()
            }
        }
        additionalPaymsVC.plusSelectBlock = {[weak self]  (plusPrice: Double) -> Void in
            if let strongSelf = self {
                strongSelf.totalDiscount.plusPrice = plusPrice
                strongSelf.updateDisctountButtonTitle()
            }
        }
        
        additionalPaymsVC.modalPresentationStyle = UIModalPresentationStyle.popover
        additionalPaymsVC.preferredContentSize = CGSize(width: 600, height: 500)
        
        present(additionalPaymsVC, animated: true, completion: nil)
        
        let popoverPresentationController = additionalPaymsVC.popoverPresentationController
        popoverPresentationController?.delegate = self
        popoverPresentationController?.sourceView = sender
        popoverPresentationController?.sourceRect = CGRect(x: 0, y: 0, width: sender.frame.size.width, height: sender.frame.size.height)
    }
    
    @IBAction func saveOrderDidPressed(_ sender: Any) {
        
        var isSavedToDatabase: Bool = false
        
        //(1) orderDetails_ = self.orderDetails  //[OrderDetail]?
        //(2) let employee_ = AppSession.shared.currentEmployee //Staff
        //(3) OrderMemomry.shared.customerId // UInt64
        //(4) OrderMemomry.shared.supplierId // UInt64
        //(5) self.orderDocument // OrderDocumentType
        //(6) self.totalPrice // Double
        //(7) discount // Double
        
        switch orderDocument {
        case .saleToCustomer, .returnFromCustomer, .saleToCustomerEdited, .preOrderToCustomer:
            if OrderMemomry.shared.customerId < 1 {
                let _ = self.showErrorAlert(text: "Выберите клиента!")
                self.customerDidPressed(123)
                return
            }
        case .receiveFromSupplier, .returnToSupplier:
            if OrderMemomry.shared.supplierId < 1 {
                let _ = self.showErrorAlert(text: "Выберите поставщика!")
                self.customerDidPressed(123)
                return
            }
        default:
            print("")
        }
        
        if let orderDetails_ = self.orderDetails {
            
            if orderDetails_.count == 0 {
                let _ = self.showErrorAlert(text: "Выберите товар(продукт)")
                return
            }
            
            if AppSettings.shared.getProductAvailability() &&
                (orderDocument == .saleToCustomer || orderDocument == .rawProductGoneToProd) {
                let (isProductAvailable, productName) = Availability.shared.checkForProductPresence(orderDetails_)
                if !isProductAvailable {
                    let _ = self.showErrorAlert(text: "\(productName) нет достаточного количества")
                    return
                }
            }
            
        } else {
            let _ = self.showErrorAlert(text: "Выберите товар(продукт)")
            return
        }
        
        if let orderDetails_ = self.orderDetails, let employee_ = self.currentEmployee {
            
            isSavedToDatabase = true
            
            if self.orderDocument == .saleToCustomer ||
                self.orderDocument == .receiveFromSupplier ||
                self.orderDocument == .returnFromCustomer {
                
                self.documentCompanion?.performSaveOrder(senderVC: self,
                                                         employee: employee_,
                                                         orderDetails: orderDetails_,
                                                         orderDocType: self.orderDocument,
                                                         totalPrice: self.totalPrice,
                                                         totalDiscount: self.totalDiscount)
            } else {
                let msgString = "\"" + Order.orderDocumentTypeStringName(orderDocType: self.orderDocument) + "\""
                let alert: UIAlertController = UIAlertController(title: "Сохранить документ",
                                                                 message: "Вы действительно хотите сохранить документ \(msgString)?",
                    preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "Сохранить", style: .default, handler: {[weak self]
                    (UIAlertAction) -> Void in
                    if let strongSelf = self {
                        strongSelf.documentCompanion?.performSaveOrder(senderVC: strongSelf,
                                                                      employee: employee_,
                                                                      orderDetails: orderDetails_,
                                                                      orderDocType: strongSelf.orderDocument,
                                                                      totalPrice: strongSelf.totalPrice,
                                                                      totalDiscount: strongSelf.totalDiscount)
                    }
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        if !isSavedToDatabase {
            let _ = self.showErrorAlert(text: "Не все поля!")
        }
    }
}

extension MainDocumentVC: UIPopoverPresentationControllerDelegate {
    
    public func prepareForPopoverPresentation(_ popoverPresentationController: UIPopoverPresentationController) {
    }
    
    public func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
    
    public func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
    }
    
    public func popoverPresentationController(_ popoverPresentationController: UIPopoverPresentationController, willRepositionPopoverTo rect: UnsafeMutablePointer<CGRect>, in view: AutoreleasingUnsafeMutablePointer<UIView>) {
    }
}


extension MainDocumentVC: UITextFieldDelegate {
    
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {}
    
//    func textFieldDidBeginEditing(_ textField: UITextField) {}
    
//    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {}
    
//    func textFieldDidEndEditing(_ textField: UITextField) {}
    
//    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {}
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {}
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("textFieldShouldClear")
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("textFieldShouldReturn")
        textField.resignFirstResponder()
        return true
    }
}

extension MainDocumentVC: UISearchBarDelegate {
    
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("searchBarTextDidBeginEditing")
    }
    
    public func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print("searchBarTextDidEndEditing")
    }
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)")
        if searchText.characters.count > 0 {
            self.showLoading("Поиск")
            Database.shared.search(productName: searchText, completion: {[weak self] (products_: [Product]) in
                self?.dismissLoading()
                self?.prodcutListSelectTVC.reloadTableData(products_)
            })
        } else {
            Database.shared.loadAllProductsBy(categoryId: 0,
                                              limit: Global.shared.limitForStaticModels,
                                              completion: {[weak self] (products_: [Product]) in
                                                self?.prodcutListSelectTVC.reloadTableData(products_)
            })
        }
    }
    
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("searchBarSearchButtonClicked ==> \(searchBar.text!)")
        searchBar.resignFirstResponder()
    }
    
    public func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        print("searchBarBookmarkButtonClicked")
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("searchBarCancelButtonClicked")
        Database.shared.loadAllProductsBy(categoryId: 0,
                                          limit: Global.shared.limitForStaticModels,
                                          completion: {[weak self] (products_: [Product]) in
                                            self?.prodcutListSelectTVC.reloadTableData(products_)
        })
    }
    
    public func searchBarResultsListButtonClicked(_ searchBar: UISearchBar) {
        print("searchBarResultsListButtonClicked")
    }
    
    public func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        print("searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope")
    }
}
