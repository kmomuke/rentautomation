//
//  ProductAdditionsTVC.swift
//  Rentautomation
//
//  Created by kanybek on 3/21/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class ProductAdditionsTVC: UITableViewController {
    
    var categoriesListVC: CategoriesListVC?
    var suppliersListVC: SuppliersListVC?
    var laserScanerVC: ScannerLaserVC?
    
    lazy var localTitles: [String] = {
        var localTitles_: [String] = []
        localTitles_.append("Категория")
        localTitles_.append("Поставщик")
        //localTitles_.append("Поиск по номеру")
        return localTitles_
    }()
    
    convenience init() {
        self.init(laserScanerVC: nil,
                  categoriesListVC: nil,
                  suppliersListVC: nil)
    }
    
    init(laserScanerVC: ScannerLaserVC?,
         categoriesListVC: CategoriesListVC?,
         suppliersListVC: SuppliersListVC?)
    {
        self.laserScanerVC = laserScanerVC
        self.categoriesListVC = categoriesListVC
        self.suppliersListVC = suppliersListVC
        super.init(nibName: "ProductAdditionsTVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("ProductAdditionsTVC deleted")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "CategoriesListTVCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "CategoriesListTVCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return localTitles.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoriesListTVCell", for: indexPath) as! CategoriesListTVCell
        let object = localTitles[(indexPath as NSIndexPath).row]
        cell.categoryNameLabel.text = "\(object)"
        cell.categoryPrimaryKeyLabel.isHidden = true
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 0 {
            if let categoriesVC_ = self.categoriesListVC {
                self.navigationController?.pushViewController(categoriesVC_, animated: true)
            }
        } else if indexPath.row == 1 {
            if let supplierVC_ = self.suppliersListVC {
                self.navigationController?.pushViewController(supplierVC_, animated: true)
            }
        } else if indexPath.row == 2 {
            if let laserScanerVC_ = self.laserScanerVC {
                self.navigationController?.pushViewController(laserScanerVC_, animated: true)
            }
        }
    }
}
