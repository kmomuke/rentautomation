//
//  PaymentVC.swift
//  Rentautomation
//
//  Created by kanybek on 12/9/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

class PaymentVC: UIViewController {

    var userId: UInt64 = 0
    var totalPrice: Double = 0.0
    var orderDocument: OrderDocumentType = .none
    
    lazy var titleView: TitleView = {
        let view = TitleView()
        return view
    }()
    
    override var title: String? {
        set {
            titleView.titleLabel.text = newValue
        }
        get {
            return titleView.titleLabel.text
        }
    }
    
    @IBOutlet weak var moneySegmentedCotrol: RASegmentedControl!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var priceTextField: BKCurrencyTextField!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var borrowLabel: UILabel!
    @IBOutlet weak var onTextFieldLabel: UILabel!
    @IBOutlet weak var giveOrTakeLabel: UILabel!
    @IBOutlet weak var customerLabel: UILabel!
    
    var selectedValue0: Double = 0
    var value0: Double = 0
    var value1: UInt32 = 0
    var value2: UInt32 = 0
    var value3: UInt32 = 0
    var value4: UInt32 = 0
    
    var performSaveBlock : ((Void) -> Void)?
    var dismissBlock : ((_ staffEnteredMoney: Double) -> Void)?
    
    convenience init() {
        self.init(totalPrice: 0.0, orderDocument: .none, userId: 0)
    }
    
    init(totalPrice: Double, orderDocument: OrderDocumentType, userId: UInt64) {
        self.totalPrice = totalPrice
        self.orderDocument = orderDocument
        self.userId = userId
        super.init(nibName: "PaymentVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightMedium)
        moneySegmentedCotrol.setTitleTextAttributes([NSFontAttributeName: font],
                                                    for: .normal)
        
        let canceButton = UIBarButtonItem(
            title: "Отмена",
            style: .plain,
            target: self,
            action: #selector(cancelObject(_:))
        )
        self.navigationItem.leftBarButtonItem = canceButton
        
        navigationItem.titleView = self.titleView
        self.titleView.titleLabel.textColor = Order.orderDocumentTypeColor(orderDocType: orderDocument)
        
        switch orderDocument {
        case .saleToCustomer:
            self.title = "Продажа"
            self.onTextFieldLabel.text = "Принять оплату"
            self.giveOrTakeLabel.text = "продажа на сумму:"
            if self.userId > 0 {
                if let customer = Database.shared.customerBy(customerId: self.userId) {
                    let customerOrSupplierName = "\(customer.firstName) \(customer.secondName)"
                    self.customerLabel.text = customerOrSupplierName
                }
            }
        case .receiveFromSupplier:
            self.title = "Закупка"
            self.onTextFieldLabel.text = "Заплатить"
            self.giveOrTakeLabel.text = "закупка на сумму:"
            if self.userId > 0 {
                if let supplier = Database.shared.supplierBy(supplierId: self.userId) {
                    let customerOrSupplierName = "\(supplier.companyName)"
                    self.customerLabel.text = customerOrSupplierName
                }
            }
        case .returnFromCustomer:
            self.title = "Возврат от продажи"
            self.onTextFieldLabel.text = "Вернуть сумму"
            self.giveOrTakeLabel.text = "возврат на сумму:"
            if self.userId > 0 {
                if let customer = Database.shared.customerBy(customerId: self.userId) {
                    let customerOrSupplierName = "\(customer.firstName) \(customer.secondName)"
                    self.customerLabel.text = customerOrSupplierName
                }
            }
        default:
            print("default:")
        }
        
        self.selectedValue0 = self.totalPrice
        self.value0 = self.totalPrice
        self.value1 = UInt32((self.totalPrice / 10).rounded(.up)) * 10
        self.value2 = UInt32((self.totalPrice / 50).rounded(.up)) * 50
        self.value3 = UInt32((self.totalPrice / 100).rounded(.up)) * 100
        self.value4 = UInt32((self.totalPrice / 500).rounded(.up)) * 500
        
        self.totalPriceLabel.text = Global.shared.priceFormatter.string(for: self.totalPrice)
        self.priceTextField.numberValue = NSDecimalNumber(value: self.totalPrice)
        
        
        self.moneySegmentedCotrol.setTitle(Global.shared.priceFormatter.string(for: self.value0),
                                           forSegmentAt: 0)
        self.moneySegmentedCotrol.setTitle(Global.shared.priceFormatter.string(for: self.value1),
                                           forSegmentAt: 1)
        self.moneySegmentedCotrol.setTitle(Global.shared.priceFormatter.string(for: self.value2),
                                           forSegmentAt: 2)
        self.moneySegmentedCotrol.setTitle(Global.shared.priceFormatter.string(for: self.value3),
                                           forSegmentAt: 3)
        self.moneySegmentedCotrol.setTitle(Global.shared.priceFormatter.string(for: self.value4),
                                           forSegmentAt: 4)
        
        
        //self.priceTextField.numberValue = NSDecimalNumber(value: 0)
        let events: [(Selector, UIControlEvents)] = [(#selector(PaymentVC.textChanged(textField:)), .editingChanged),
                                                     (#selector(PaymentVC.editingDidBegin(textField:)), .editingDidBegin),
                                                     (#selector(PaymentVC.editingDidEnd(textField:)), .editingDidEnd)]
        events.forEach {
            self.priceTextField.addTarget(self, action: $0.0, for: $0.1)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.priceTextField.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func segmentedPressed(_ sender: RASegmentedControl) {
        switch sender.selectedSegmentIndex
        {
        case 0:
            print("First Segment Selected")
            self.selectedValue0 = self.value0
            self.borrowLabel.text = "сдачи: \(self.selectedValue0 - self.totalPrice)"
        case 1:
            print("Second Segment Selected")
            self.selectedValue0 = Double(self.value1)
            self.borrowLabel.text = "сдачи: \(self.selectedValue0 - self.totalPrice)"
        case 2:
            print("Third Segment Selected")
            self.selectedValue0 = Double(self.value2)
            self.borrowLabel.text = "сдачи: \(self.selectedValue0 - self.totalPrice)"
        case 3:
            print("Fourth Segment Selected")
            self.selectedValue0 = Double(self.value3)
            self.borrowLabel.text = "сдачи: \(self.selectedValue0 - self.totalPrice)"
        case 4:
            print("Fifth Segment Selected")
            self.selectedValue0 = Double(self.value4)
            self.borrowLabel.text = "сдачи: \(self.selectedValue0 - self.totalPrice)"
        default:
            print("default")
            break
        }
    }
    
    func cancelObject(_ sender: AnyObject) -> Void {
        self.navigationController?.dismiss(animated: true, completion: {[weak self] in
        })
    }
    
    @IBAction func okButtonPressed(_ sender: Any) {
        
        if let performSaveBlock_ = self.performSaveBlock {
            performSaveBlock_()
        }
        
        var number: Double = 0.0
        if (self.priceTextField.text?.characters.count)! > 0 {
            number = self.priceTextField.numberValue.doubleValue
            print("number is:\(number)")
        } else {
            print("number is:\(number)")
        }
        
        self.navigationController?.dismiss(animated: true, completion: {[weak self] in
            if let dismissBlock_ = self?.dismissBlock {
                dismissBlock_(number)
            }
        })
    }
    
    private dynamic func textChanged(textField: UITextField) {
        if (self.priceTextField.text?.characters.count)! > 0 {
            let number = self.priceTextField.numberValue.doubleValue
            print("number is:\(number)")
            self.totalPrice = number
            let k = Global.shared.priceFormatter.string(for: self.selectedValue0 - self.totalPrice)!
            self.borrowLabel.text = "сдачи: \(String(describing: k))"
        } else {
            let number: Double = 0.0
            print("number is:\(number)")
            self.totalPrice = number
            let k = Global.shared.priceFormatter.string(for: self.selectedValue0 - self.totalPrice)!
            self.borrowLabel.text = "сдачи: \(k)"
        }
    }
    
    private dynamic func editingDidBegin(textField: UITextField) {
    }
    
    private dynamic func editingDidEnd(textField: UITextField) {
    }
    
}

