//
//  MoneyIncomeVC.swift
//  Rentautomation
//
//  Created by kanybek on 12/10/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

class MoneyIncomeVC: FormViewController, ErrorPopoverRenderer {
    
    var orderDocument: OrderDocumentType
    var currentEmployee: User?
    
    var isMoneyForDebt: Bool = false
    
    lazy var titleView: TitleView = {
        let view = TitleView()
        return view
    }()
    
    override var title: String? {
        set {
            titleView.titleLabel.text = newValue
        }
        get {
            return titleView.titleLabel.text
        }
    }
    
    convenience init() {
        self.init(orderDocument: .none)
    }
    
    init(orderDocument: OrderDocumentType) {
        self.orderDocument = orderDocument
        OrderMemomry.shared.clearAllVariables()
        OrderMemomry.shared.orderDocument = orderDocument
        OrderMemomry.shared.moneyMovement = .moneyFromCustomer
        self.currentEmployee = AppSession.shared.currentEmployee
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("MoneyIncomeVC deallocated")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        
        navigationItem.titleView = self.titleView
        self.titleView.titleLabel.textColor = Order.orderDocumentTypeColor(orderDocType: orderDocument)
        
        self.title = "Приход"
        
        let canceButton = UIBarButtonItem(
            title: "Закрыть",
            style: .plain,
            target: self,
            action: #selector(cancelObject(_:))
        )
        self.navigationItem.leftBarButtonItem = canceButton
        
        let createButton = UIBarButtonItem(
            title: "Сохранить",
            style: .plain,
            target: self,
            action: #selector(createOrder(_:))
        )
        self.navigationItem.rightBarButtonItem = createButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
        
        if OrderMemomry.shared.staffId > 0 {
            if let employee_ = Database.shared.userBy(userId: OrderMemomry.shared.staffId) {
                self.currentEmployee = employee_
            }
        }
        
        if AppSettings.shared.getCustomersStaff() {
            if OrderMemomry.shared.customerId > 0 {
                if let customer__ = Database.shared.customerBy(customerId:OrderMemomry.shared.customerId) {
                    if let employee_ = Database.shared.userBy(userId: customer__.userId) {
                        self.currentEmployee = employee_
                    }
                }
            }
        }
        
        for row in self.neededProductSection.rowFormers {
            row.update()
        }
        
        for row in self.staffSection.rowFormers {
            row.update()
        }
        
        if OrderMemomry.shared.moneyMovement == .none {
            disableOrEnableCustomerOrSupplierRow(shouldEnable: false)
        } 
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func cancelObject(_ sender: AnyObject) -> Void {
        OrderMemomry.shared.clearAllVariables()
        let empty = EmptyVC()
        let detailNavigationController = RNNavigationController(rootViewController:empty)
        self.splitViewController?.showDetailViewController(detailNavigationController, sender: self)
    }
    
    // MARK: CREATE
    func createOrder(_ sender: AnyObject) -> Bool {
        
        if OrderMemomry.shared.moneyIncomeOutgoneSum < 0.1 {
            return self.showErrorAlert(text: "Сумма")
        }
        
        switch OrderMemomry.shared.moneyMovement {
            
        case .moneyFromCustomer:
            if OrderMemomry.shared.customerId == 0 {
                return self.showErrorAlert(text: "Клиент контрагент")
            }
        case .moneyFromSupplierReturn:
            if OrderMemomry.shared.supplierId == 0 {
                return self.showErrorAlert(text: "Поставщик контрагент")
            }
        case .none:
            return self.showErrorAlert(text: "Выберите тип прихода")
        default:
            print("default")
        }
        
        
        //------ success passed check ---------//
        if let employee_ = self.currentEmployee {
            
            let msgString = "\"" + Order.orderDocumentTypeStringName(orderDocType: self.orderDocument) + "\""
            let alert: UIAlertController = UIAlertController(title: "Сохранить документ",
                                                             message: "Вы действительно хотите сохранить документ \(msgString)?",
                preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Сохранить", style: .default, handler: {
                (UIAlertAction) -> Void in
                self.someMethod(employee_: employee_)
            }))
            self.present(alert, animated: true, completion: nil)
            
            
        } else {
            return self.showErrorAlert(text: "Сотрудник")
        }
        return true
    }
    
    func someMethod(employee_: User) -> Void {
        
        switch OrderMemomry.shared.moneyMovement {
            
        case .moneyFromCustomer:
            
            let customerId = OrderMemomry.shared.customerId
            print("customerId ==>\(customerId)")
            OrderManager.shared.ownerReceivedMoneyFromCustomer(customerId: customerId,
                                                                             staffId: employee_.userId,
                                                                             receivedMoney: OrderMemomry.shared.moneyIncomeOutgoneSum,
                                                                             isMoneyForDebt: self.isMoneyForDebt,
                                                                             comment: OrderMemomry.shared.comments)
        case .moneyFromSupplierReturn:
            
            let supplierId = OrderMemomry.shared.supplierId
            print("supplierId ==>\(supplierId)")
            OrderManager.shared.ownerReceivedRetrunMoneyFromSupplier(supplierId: supplierId,
                                                                                   staffId: employee_.userId,
                                                                                   receivedReturnMoney: OrderMemomry.shared.moneyIncomeOutgoneSum,
                                                                                   comment: OrderMemomry.shared.comments)
        default:
            
            
            // --------------------------- Order --------------------------- //
            let order = Order()
            order.orderId = Database.shared.uniqueIncreasingPrimaryKey()
            
            order.orderDocument = self.orderDocument
            order.moneyMovementType = OrderMemomry.shared.moneyMovement
            
            order.customerId = OrderMemomry.shared.customerId
            order.supplierId = OrderMemomry.shared.supplierId
            order.comment = OrderMemomry.shared.comments
            
            let currentDate = Date()
            order.orderDate = currentDate
            let str = "№ \(order.orderId) от \(currentDate.toString(.custom("EEEE, dd MMMM yyyy HH:mm")))"
            order.billingNo = str
            order.userId = employee_.userId
            
            // --------------------------- Payment --------------------------- //
            let payment = Payment()
            payment.paymentId = Database.shared.uniqueIncreasingPrimaryKey()
            payment.discount = 0.0
            payment.totalOrderPrice = OrderMemomry.shared.moneyIncomeOutgoneSum
            payment.totalPriceWithDiscount = OrderMemomry.shared.moneyIncomeOutgoneSum
            
            order.paymentId = payment.paymentId
            
            Database.shared.createOrders([order])
            Database.shared.createPayments([payment])
            
            let operation = Event(syncOperationId: Database.shared.operationCid(),
                                          isSynchronized: false,
                                          syncOperationDate: Date(),
                                          operationType: .orderCreated,
                                          productId: 0,
                                          orderDetailId: 0,
                                          customerId: 0,
                                          transactionId: 0,
                                          accountId: 0,
                                          supplierId: 0,
                                          staffId: employee_.userId,
                                          categoryId: 0,
                                          orderId: order.orderId)
            Database.shared.createOperations([operation])
            
            print("default")
        }
        
        self.showSuccessAlert(text: "Документ успешно сохранен!")
        OrderMemomry.shared.clearAllVariables()

        let empty = EmptyVC()
        let detailNavigationController = RNNavigationController(rootViewController:empty)
        self.splitViewController?.showDetailViewController(detailNavigationController, sender: self)
    }
    
    
    // MARK: Private
    private lazy var formerInputAccessoryView: FormerInputAccessoryView = FormerInputAccessoryView(former: self.former)
    
    // MARK: Needed product section
    private lazy var neededProductSection: SectionFormer = { [weak self] in
        
        let createButtonRow2: ((String, (() -> Void)?) -> RowFormer) = { [weak self] text, onSelected in
            
            return LabelRowFormer<FormLabelCell>() {
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                $0.subTextLabel.textColor = .formerSubColor()
                $0.subTextLabel.font = .boldSystemFont(ofSize: 14)
                
                }.configure {
                    $0.text = text
                    $0.rowHeight = 60
                }.onSelected { [weak self] _ in
                    onSelected?()
                    self?.former.deselect(animated: true)
                }.onUpdate { [weak self] in
                    
                    var userOperandName: String = "   "
                    
                    if (OrderMemomry.shared.supplierId > 0) {
                        if let supplier =  Database.shared.supplierBy(supplierId: OrderMemomry.shared.supplierId) {
                            userOperandName = supplier.companyName
                        }
                    }
                    
                    if (OrderMemomry.shared.customerId > 0) {
                        if let customer =  Database.shared.customerBy(customerId: OrderMemomry.shared.customerId) {
                            userOperandName = customer.firstName + " " + customer.secondName
                        }
                    }
                    
                    if (userOperandName.characters.count > 0) {
                        $0.subText =  "\(userOperandName)"
                    }
            }
        }
        
        let supplierRow = createButtonRow2("Клиент или поставщик") { [weak self] in
            
            switch OrderMemomry.shared.moneyMovement {
                
            case .moneyFromCustomer:
                print("moneyFromCustomer")
                let customerVC = CustomersListVC(initialString: nil, isComingFromOrder: true)
                self?.navigationController?.pushViewController(customerVC, animated: true)
            case .moneyFromSupplierReturn:
                let supplierVC = SuppliersListVC(initialString: nil, isCommingFromProduct: false, isComingFromOrder: true)
                self?.navigationController?.pushViewController(supplierVC, animated: true)
            case .moneyCorrection:
                print("moneyCorrection")
            case .moneyReceive:
                print("moneyReceive")
            case .otherMoneyReceive:
                print("otherMoneyReceive")
            default:
                print("default")
            }
        }
        
        let totalUnitPriceRow = TextFieldRowFormer<PriceFieldTVCell>() { [weak self] in
            $0.titleLabel.text = "Сумма"
            $0.textField.keyboardType = .numberPad
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.returnKeyType = .next
            }.configure {
                $0.text = ""
                $0.rowHeight = 60
            }.onTextChanged {
                if let doubleVar = $0.getDoubleFromString() {
                    OrderMemomry.shared.moneyIncomeOutgoneSum = doubleVar
                } else {
                    OrderMemomry.shared.moneyIncomeOutgoneSum = 0.0
                }
        }
        
        let switchDateStyleRow = SwitchRowFormer<FormSwitchCell>() {
            $0.titleLabel.text = "Оплата за долг?"
            $0.titleLabel.textColor = .formerColor()
            $0.titleLabel.font = .boldSystemFont(ofSize: 14)
            $0.switchButton.onTintColor = .formerSubColor()
            }.configure { [weak self] in
                $0.rowHeight = 60
                if let strongSelf = self {
                    $0.switched = strongSelf.isMoneyForDebt
                }
            }.onSwitchChanged {[weak self] switched in
                self?.isMoneyForDebt = switched
            }.onUpdate { [weak self] in
                if let strongSelf = self {
                    $0.switched = strongSelf.isMoneyForDebt
                }
        }
        
        let section = SectionFormer(rowFormer:supplierRow, totalUnitPriceRow, switchDateStyleRow)
        section.set(footerViewFormer: self?.createHeaderFunc(s: " "))
        return section
    }()
    
    private lazy var staffSection: SectionFormer = { [weak self] in
        
        let createButtonRow1: ((String, (() -> Void)?) -> RowFormer) = { [weak self] text, onSelected in
            return LabelRowFormer<FormLabelCell>() {
                
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                $0.subTextLabel.textColor = .formerSubColor()
                $0.subTextLabel.font = .boldSystemFont(ofSize: 14)
                
                }.configure {
                    $0.text = text
                    $0.rowHeight = 60
                }.onSelected { [weak self] _ in
                    onSelected?()
                    self?.former.deselect(animated: true)
                }.onUpdate { [weak self] in
                    var emplyeeName: String = ""
                    if let employee_ = self?.currentEmployee {
                        emplyeeName = employee_.firstName + " " + employee_.secondName
                    }
                    if (emplyeeName.characters.count > 0) {
                        $0.subText = "\(emplyeeName)"
                    }
            }
        }
        
        let staffRow = createButtonRow1("Сотрудник") { [weak self] in
            let staffList: StaffListVC = StaffListVC(initialString: nil, isCommingFromCustomer: true)
            self?.navigationController?.pushViewController(staffList, animated: true)
        }
        
        let section = SectionFormer(rowFormer:staffRow)
        section.set(headerViewFormer: self?.createHeaderFunc(s: "Дополнительно"))
        return section
        }()
    
    private lazy var commentSection: SectionFormer = { [weak self] in
        
        let addressRow = TextViewRowFormer<FormTextViewCell>() { [weak self] in
            $0.textView.textColor = .formerSubColor()
            $0.textView.font = .systemFont(ofSize: 20)
            $0.textView.inputAccessoryView = self?.formerInputAccessoryView
            }.configure {
                $0.placeholder = "Комментарий"
            }.onTextChanged {
                OrderMemomry.shared.comments = $0
        }
        
        let section = SectionFormer(rowFormer: addressRow)
        return section
    }()
    
    private func createHeaderFunc(s: String) -> ViewFormer {
        return LabelViewFormer<FormLabelHeaderView>()
            .configure {
                $0.viewHeight = 40
                $0.text = s
        }
    }
    
    func typeIncomeMoneyOptions() -> SectionFormer {
        
        let createSelectorRow = { (
            text: String,
            subText: String,
            onSelected: ((RowFormer) -> Void)?
            ) -> RowFormer in
            return LabelRowFormer<FormLabelCell>() {
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.subTextLabel.textColor = .formerSubColor()
                $0.subTextLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                }.configure { form in
                    _ = onSelected.map { form.onSelected($0) }
                    form.text = text
                    form.subText = subText
                    form.rowHeight = 60
            }
        }
        
        let incomePaymentOptions = ["Оплата от клиента",
                                    "Возврат денежных средств поставщиком",
                                    "Корректировка баланса счета",
                                    "Прием денежных средств",
                                    "Прочий приход денежных средств"
        ]
        
        //let roleName: String = "Не выбрано"
        //if let staff_ = self.initialStaff {
        //
        //    if staff_.roleId == 1000 {
        //        roleName = "Владелец"
        //    } else if staff_.roleId == 2000 {
        //        roleName = "Администратор"
        //    } else if staff_.roleId == 3000 {
        //        roleName = "Агент"
        //    }
        //}
        let roleSelectorRow = createSelectorRow("Тип денежного прихода", incomePaymentOptions[0], pushSelectorRowSelected(options: incomePaymentOptions))
        
        let section = SectionFormer(rowFormer: roleSelectorRow)
        section.set(headerViewFormer: self.createHeaderFunc(s: " "))
        return section
    }
    
    private func pushSelectorRowSelected(options: [String]) -> (RowFormer) -> Void {
        return { [weak self] rowFormer in
            
            if let rowFormer = rowFormer as? LabelRowFormer<FormLabelCell> {
                let controller = TextSelectorViewContoller()
                controller.texts = options
                controller.selectedText = rowFormer.subText
                controller.onSelected = {
                    
                    rowFormer.subText = $0
                    rowFormer.update()
                    
                    self?.isMoneyForDebt = false
                    
                    if $0 == "Оплата от клиента" {
                        print("Оплата от клиента")
                        self?.isMoneyForDebt = true
                        self?.disableOrEnableCustomerOrSupplierRow(shouldEnable: true)
                        OrderMemomry.shared.moneyMovement = .moneyFromCustomer
                        OrderMemomry.shared.supplierId = 0
                    } else if $0 == "Возврат денежных средств поставщиком" {
                        print("Возврат денежных средств поставщиком")
                        self?.disableOrEnableCustomerOrSupplierRow(shouldEnable: true)
                        OrderMemomry.shared.moneyMovement = .moneyFromSupplierReturn
                        OrderMemomry.shared.customerId = 0
                    } else if $0 == "Корректировка баланса счета" {
                        print("Корректировка баланса счета")
                        self?.disableOrEnableCustomerOrSupplierRow(shouldEnable: false)
                        OrderMemomry.shared.moneyMovement = .moneyCorrection
                        OrderMemomry.shared.customerId = 0
                        OrderMemomry.shared.supplierId = 0
                    } else if $0 == "Прием денежных средств" {
                        print("Прием денежных средств")
                        self?.disableOrEnableCustomerOrSupplierRow(shouldEnable: false)
                        OrderMemomry.shared.moneyMovement = .moneyReceive
                        OrderMemomry.shared.customerId = 0
                        OrderMemomry.shared.supplierId = 0
                    } else if $0 == "Прочий приход денежных средств" {
                        print("Прочий приход денежных средств")
                        self?.disableOrEnableCustomerOrSupplierRow(shouldEnable: false)
                        OrderMemomry.shared.moneyMovement = .otherMoneyReceive
                        OrderMemomry.shared.customerId = 0
                        OrderMemomry.shared.supplierId = 0
                    }
                }
                self?.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    func disableOrEnableCustomerOrSupplierRow(shouldEnable: Bool) -> Void {
        let rowFormer__ = self.neededProductSection.rowFormers[0]
        rowFormer__.enabled = shouldEnable
    }

    private func configure() {
        
        tableView.contentInset.top = 0
        tableView.contentInset.bottom = 0
        
        former.append(sectionFormer:self.typeIncomeMoneyOptions(), self.neededProductSection, self.staffSection, self.commentSection)
            .onCellSelected { [weak self] _ in
                self?.formerInputAccessoryView.update()
        }
    }
}
