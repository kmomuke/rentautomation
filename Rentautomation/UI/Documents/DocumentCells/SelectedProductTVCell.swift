//
//  SelectedProductTVCell.swift
//  Rentautomation
//
//  Created by kanybek on 12/6/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

class SelectedProductTVCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var quantityTextField: BKCurrencyTextField!
    @IBOutlet weak var countNumberLabel: UILabel!
    @IBOutlet weak var priceButton: UIButton!
    
    var deleteBlock : (() -> Void)?
    var priceButtonBlock : ((UIButton, OrderDetail?) -> Void)?
    var quantityEditinEndBlock : (() -> Void)?
    
    var orderDetail: OrderDetail?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
        self.accessoryType = .none
        self.editingAccessoryType = .none
        
        //
        let events: [(Selector, UIControlEvents)] = [(#selector(SelectedProductTVCell.textChanged(textField:)), .editingChanged),
                                                     (#selector(SelectedProductTVCell.editingDidBegin(textField:)), .editingDidBegin),
                                                     (#selector(SelectedProductTVCell.editingDidEnd(textField:)), .editingDidEnd)]
        events.forEach {
            self.quantityTextField.addTarget(self, action: $0.0, for: $0.1)
        }
        
        if AppSettings.shared.getAutoClearMode() {
            quantityTextField.clearsOnBeginEditing = true
        }
        quantityTextField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func reloadData() -> Void {
        if let orderDetail_ = self.orderDetail {
            
            var productName: String = ""
            var quantityPerUnit: String = ""
            if let product = Database.shared.productFor(productId: orderDetail_.productId) {
                productName = product.productName
                quantityPerUnit = product.quantityPerUnit
            }
            self.nameLabel.text = "\(productName)  \n\(quantityPerUnit)"
            let price = Global.shared.priceFormatter.string(for: orderDetail_.price)!
            
            self.priceLabel.text = price
            self.priceButton.setTitle("\(price)", for: .normal)
            
            let totalPriceWithDiscount = orderDetail_.price * orderDetail_.orderQuantity
            self.totalAmountLabel.text = Global.shared.priceFormatter.string(for: totalPriceWithDiscount)
            
            
            self.quantityTextField.text = Global.shared.priceFormatter.string(for: orderDetail_.orderQuantity)
            self.quantityTextField.numberValue = NSDecimalNumber(value: orderDetail_.orderQuantity)
        } else {
            self.resetLabels()
        }
    }
    
    override func prepareForReuse() {
        self.orderDetail = nil
        self.resetLabels()
    }
    
    func resetLabels() -> Void {
        self.nameLabel.text = ""
        self.priceLabel.text = ""
        self.totalAmountLabel.text = ""
        self.countNumberLabel.text = ""
    }
    
    
    private dynamic func textChanged(textField: UITextField) {
        if (self.quantityTextField.text?.characters.count)! > 0 {
            let number = self.quantityTextField.numberValue.doubleValue
            print("textChanged number is:\(number)")
            
            if let orderDetail_ = self.orderDetail {
                
                orderDetail_.orderQuantity = number
                let totalPriceWithDiscount = orderDetail_.price * orderDetail_.orderQuantity
                self.totalAmountLabel.text = Global.shared.priceFormatter.string(for: totalPriceWithDiscount)
            }
        } else {
            self.orderDetail!.orderQuantity = 0.0
            self.totalAmountLabel.text = "\(self.orderDetail!.price * 0.0)"
        }
    }
    
    private dynamic func editingDidBegin(textField: UITextField) {
        if (self.quantityTextField.text?.characters.count)! > 0 {
            let number = self.quantityTextField.numberValue.doubleValue
            print("editingDidBegin number is:\(number)")
        }
    }
    
    private dynamic func editingDidEnd(textField: UITextField) {
        
        if (self.quantityTextField.text?.characters.count)! > 0 {
            let number = self.quantityTextField.numberValue.doubleValue
            print("editingDidEnd number is:\(number)")
            
            if let endEditinBlock_ = self.quantityEditinEndBlock {
                endEditinBlock_()
            }
        } else {
            print("editingDidEnd number is:\(0)")
            if let endEditinBlock_ = self.quantityEditinEndBlock {
                endEditinBlock_()
            }
            
            if let deleteBlock_ = self.deleteBlock {
                deleteBlock_()
            }
        }
    }
    
    internal dynamic func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("textFieldShouldReturn")
        textField.resignFirstResponder()
        return false
    }
    
    @IBAction func removeDidPressed(_ sender: Any) {
        
        if let deleteBlock_ = self.deleteBlock {
            deleteBlock_()
        }
    }
    
    @IBAction func priceDidPressed(_ sender: UIButton) {
        if let priceBlock_ = self.priceButtonBlock {
            priceBlock_(sender, self.orderDetail)
        }
    }
}


