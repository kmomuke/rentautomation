//
//  SetNewPriceVC.swift
//  Rentautomation
//
//  Created by kanybek on 2/26/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class SetNewPriceVC: FormViewController, ErrorPopoverRenderer {
    
    var initialPrice: Double
    var initialPoductName: String
    
    var newPriceSetBlock : ((Double) -> Void)?
    
    convenience init() {
        self.init(initialPrice: 0.0, initialPoductName: "")
    }
    
    init(initialPrice: Double, initialPoductName: String) {
        self.initialPrice = initialPrice
        self.initialPoductName = initialPoductName
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("SetNewPriceVC deallocated")
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardWillShow(_ notification: NSNotification){
    }
    
    func keyboardWillHide(_ notification: NSNotification){
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.balanceSumRow.former?.becomeEditingNext()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func cancelObject(_ sender: AnyObject) -> Void {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    // MARK: CREATE
    func saveBalance(_ sender: AnyObject) -> Bool {
        self.navigationController?.dismiss(animated: true, completion: nil)
        return true
    }
    
    // MARK: Private
    private lazy var formerInputAccessoryView: FormerInputAccessoryView = FormerInputAccessoryView(former: self.former)
    
    // MARK: Optional product section
    private lazy var balanceSumRow: TextFieldRowFormer<PriceFieldTVCell> = {
        
        let totalUnitPriceRow = TextFieldRowFormer<PriceFieldTVCell>() { [weak self] in
            $0.titleLabel.text = "Цена"
            $0.textField.keyboardType = .numberPad
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.returnKeyType = .done
            }.configure { [weak self] in
                
                if let strongSelf = self {
                    if let text_ = Global.shared.amountFormatter.string(for: strongSelf.initialPrice) {
                        $0.text =  "\(text_)"
                    }
                }
                $0.rowHeight = 60
                
            }.onTextChanged { [weak self] in
                if let strongSelf = self {
                    if let doubleVar = $0.getDoubleFromString() {
                        print("set new price ==> \(doubleVar)")
                        if let priceBlock = strongSelf.newPriceSetBlock {
                            priceBlock(doubleVar)
                        }
                    }
                }
        }
        return totalUnitPriceRow
    }()
    
    private lazy var nameCategorySection: SectionFormer = { [weak self] in
        let section = SectionFormer(rowFormer: (self?.balanceSumRow)!)
        if let strongSelf = self {
            section.set(headerViewFormer: strongSelf.createHeaderFunc(s: strongSelf.initialPoductName + ":" + "\(strongSelf.initialPrice)"))
        }
        return section
    }()
    
    private func createHeaderFunc(s: String) -> ViewFormer {
        return LabelViewFormer<FormLabelHeaderView>()
            .configure {
                $0.viewHeight = 40
                $0.text = s
        }
    }
    
    private func configure() {
        
        //tableView.contentInset.top = 40
        //tableView.contentInset.bottom = 40
        
        former.append(sectionFormer:self.nameCategorySection)
            .onCellSelected { [weak self] _ in
                self?.formerInputAccessoryView.update()
        }
    }
}


