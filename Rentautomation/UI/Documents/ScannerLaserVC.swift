//
//  ScannerLaserVC.swift
//  Rentautomation
//
//  Created by kanybek on 2/28/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import Neon

class ScannerLaserVC: UIViewController {

    @IBOutlet weak var textField: UITextField!
    
    var scanerScannedBlock : ((String) -> Void)?
    
    var initialItem: String?

    convenience init() {
        self.init(initialItem: nil)
    }

    init(initialItem: String?) {
        self.initialItem = initialItem
        super.init(nibName: "ScannerLaserVC", bundle: nil)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.view.backgroundColor = UIColor.turquoiseColor()
        self.textField.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.textField.becomeFirstResponder()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //self.textField.anchorToEdge(.left, padding: 50, width: 300, height: 50)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}


extension ScannerLaserVC: UITextFieldDelegate {
    
    
//    @available(iOS 2.0, *)
//    optional public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool // return NO to disallow editing.
//    
//    @available(iOS 2.0, *)
//    optional public func textFieldDidBeginEditing(_ textField: UITextField) // became first responder
//    
//    @available(iOS 2.0, *)
//    optional public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool // return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
//    
//    @available(iOS 2.0, *)
//    optional public func textFieldDidEndEditing(_ textField: UITextField) // may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called
//    
//    @available(iOS 10.0, *)
//    optional public func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) // if implemented, called in place of textFieldDidEndEditing:
//    
//    
//    @available(iOS 2.0, *)
//    optional public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool // return NO to not change text
//    
//    
//    @available(iOS 2.0, *)
//    optional public func textFieldShouldClear(_ textField: UITextField) -> Bool // called when clear button pressed. return NO to ignore (no notifications)
//    
//    @available(iOS 2.0, *)
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if let scanerBlock = self.scanerScannedBlock, let text = textField.text {
        
            if text.characters.count > 0 {
                scanerBlock(text)
            }
        }
        textField.text = ""
        return true
    }
}
