//
//  AdditionalPaymentVC.swift
//  Rentautomation
//
//  Created by kanybek on 10/3/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class AdditionalPaymentVC: UIViewController, ErrorPopoverRenderer {

    var initialDiscount: Discount?
    var isFirstTime: Bool = true
    
    var dicsountSelectBlock : ((Double) -> Void)?
    var minusSelectBlock : ((Double) -> Void)?
    var plusSelectBlock : ((Double) -> Void)?
    
    lazy var discountSelectTVC: SelectDiscountTVC = { [weak self] in
        let tableViewController = SelectDiscountTVC(initialDiscount: self?.initialDiscount)
        tableViewController.dicsountSelectBlock = self?.dicsountSelectBlock
        return tableViewController
    }()
    
    lazy var minusPriceVC: MinusPriceVC = { [weak self] in
        let minusPriceVC_ = MinusPriceVC(initialDiscount: self?.initialDiscount, shouldMinusPrice: true)
        minusPriceVC_.minusSelectBlock = self?.minusSelectBlock
        return minusPriceVC_
    }()
    
    lazy var plusPriceVC: MinusPriceVC = { [weak self] in
        let plusPriceVC_ = MinusPriceVC(initialDiscount: self?.initialDiscount, shouldMinusPrice: false)
        plusPriceVC_.plusSelectBlock = self?.plusSelectBlock
        return plusPriceVC_
    }()
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    convenience init() {
        self.init(initialDiscount: nil)
    }
    
    init(initialDiscount: Discount?) {
        self.initialDiscount = initialDiscount
        super.init(nibName: "AdditionalPaymentVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.containerView.backgroundColor = UIColor.white
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if (isFirstTime) {
            self.add(asChildViewController: self.minusPriceVC, frame_: self.containerView.frame)
            self.titleLabel.text = "Минус от суммы"
            isFirstTime = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func segmentDidPressed(_ sender: UISegmentedControl) {
        
        self.remove(asChildViewController: self.discountSelectTVC)
        self.remove(asChildViewController: self.minusPriceVC)
        self.remove(asChildViewController: self.plusPriceVC)
        
        switch sender.selectedSegmentIndex
        {
        case 0: // minus
            self.add(asChildViewController: self.minusPriceVC, frame_: self.containerView.frame)
            self.titleLabel.text = "Минус от суммы"
        case 1: // plus/cargoo
            self.add(asChildViewController: self.plusPriceVC, frame_: self.containerView.frame)
            self.titleLabel.text = "Карго"
        case 2:// discount
            self.add(asChildViewController: self.discountSelectTVC, frame_: self.containerView.frame)
            self.titleLabel.text = "% Скидка от суммы"
        default:
            print("default")
            break
        }
    }
}
