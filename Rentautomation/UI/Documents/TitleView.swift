//
//  TitleView.swift
//  Rentautomation
//
//  Created by kanybek on 1/9/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class TitleView: UIView {
    
    var titleLabel: UILabel!
    //var detailLabel: UILabel!
    
    deinit {
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        titleLabel = UILabel()
        titleLabel.font = UIFont.systemFont(ofSize: 18, weight: UIFontWeightSemibold)
        titleLabel.textColor = UIColor.black
        titleLabel.textAlignment = .center
        titleLabel.backgroundColor = UIColor.white
        addSubview(titleLabel)
        
        //detailLabel = UILabel()
        //detailLabel.font = UIFont.systemFont(ofSize: 12)
        //detailLabel.textColor = UIColor.gray
        //detailLabel.textAlignment = .center
        //addSubview(detailLabel)
        //detailLabel.text = "last seen yesterday at 5:56 PM"
        
        verticalLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func verticalLayout() {
        frame = CGRect(x: 0, y: 0, width: 400, height: 44)
        titleLabel.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
        //titleLabel.frame = CGRect(x: 0, y: 4, width: frame.width, height: 21)
        //detailLabel.frame = CGRect(x: 0, y: titleLabel.frame.maxY + 2, width: frame.width, height: 15)
    }
    
    //    func horizontalLayout() {
    //        frame = CGRect(x: 0, y: 0, width: 300, height: 40)
    //
    //        let titleSize = titleLabel.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: 21))
    //        //let detailSize = detailLabel.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: 15))
    //
    //        //let contentWidth = titleSize.width + 6 + detailSize.width
    //
    //        var currentX = frame.width / 2 - contentWidth / 2
    //
    //        titleLabel.frame = CGRect(x: currentX, y: frame.height / 2 - titleSize.height / 2, width: titleSize.width, height: titleSize.height)
    //
    //        currentX += titleSize.width + 6
    //
    //        //detailLabel.frame = CGRect(x: currentX, y: frame.height / 2 - detailSize.height / 2, width: detailSize.width, height: detailSize.height)
    //    }
    
}
