//
//  SelectedProductsTVC.swift
//  Rentautomation
//
//  Created by kanybek on 12/6/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit

class SelectedProductsTVC: UITableViewController {
    
    var initialOrder: Order?
    var productSelectedDispose: Disposable?
    var selectedProducts: [OrderDetail] = []
    
    convenience init() {
        self.init(initialOrder: nil)
    }
    
    init(initialOrder: Order?) {
        self.initialOrder = initialOrder
        super.init(nibName: "SelectedProductsTVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.productSelectedDispose?.dispose()
        self.productSelectedDispose = nil
    }
    
    internal func focus(gesture: UITapGestureRecognizer) {
        UIApplication.shared.keyWindow?.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.keyboardDismissMode = .onDrag
        self.tableView.alwaysBounceVertical = true
        //if let gestureRecognizers = self.view.gestureRecognizers {
        //    gestureRecognizers.forEach({ self.view.removeGestureRecognizer($0) })
        //}
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(focus(gesture:)))
        tapGesture.cancelsTouchesInView = false
        self.tableView.addGestureRecognizer(tapGesture)
        
        let nib = UINib(nibName: "SelectedProductTVCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "SelectedProductTVCell")
        
        let nib2 = UINib(nibName: "SelectedProductTVCell2", bundle: nil)
        self.tableView.register(nib2, forCellReuseIdentifier: "SelectedProductTVCell2")
        
        if let initialOrder_ = self.initialOrder {
            let orderDetails = Database.shared.orderDetailsBy(orderId: initialOrder_.orderId)
            self.selectedProducts = orderDetails
            self.tableView.reloadData()
        }
        
        let productSelectedSignal: Signal = PipesStore.shared.productSelectedSignal()
        self.productSelectedDispose = productSelectedSignal.start(next: {[weak self] (product: Product, isSalePrice: Bool) in
            
            if AppSettings.shared.getProductUniqMode() && isSalePrice {
                self?.insertOdetail(product: product, isSalePrice: isSalePrice)
            } else {
                if let indexFound = self?.selectedProducts.index(where: {$0.productId == product.productId}) {
                    if let orderDetail = self?.selectedProducts[indexFound] {
                        orderDetail.orderQuantity = orderDetail.orderQuantity + Double(1)
                        self?.selectedProducts[indexFound] = orderDetail
                        self?.tableView.scrollToRow(at: IndexPath(row: indexFound, section: 0), at: .middle, animated: false)
                        self?.tableView.reloadRows(at: [IndexPath(row: indexFound, section: 0)], with: .fade)
                    }
                } else {
                    self?.insertOdetail(product: product, isSalePrice: isSalePrice)
                }
            }
            
            PipesStore.shared.totalSelectedOrderDetailsPipe.putNext((self?.selectedProducts)!)
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    private func insertOdetail(product: Product, isSalePrice: Bool) {
        let orderDetail = OrderDetail()
        orderDetail.productId = product.productId
        orderDetail.orderQuantity = Double(1)
        orderDetail.orderDetailDate = Date()
        
        if isSalePrice {
            orderDetail.price = product.saleUnitPrice
        } else {
            orderDetail.price = product.incomeUnitPrice
        }
        orderDetail.productQuantity = product.unitsInStock
        self.selectedProducts.insert(orderDetail, at: 0)
        self.tableView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
        self.tableView.beginUpdates()
        self.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .top)
        self.tableView.endUpdates()
    }
    
    // MARK: - Table view data source
    //override func scrollViewDidScroll(_ scrollView: UIScrollView) {
    //    self.tableView.endEditing(true)
    //}

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedProducts.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectedProductTVCell", for: indexPath) as! SelectedProductTVCell
        
        let orderDetail = selectedProducts[(indexPath as NSIndexPath).row]
        cell.orderDetail = orderDetail
        cell.countNumberLabel.text = "\(indexPath.row + 1)"
        cell.deleteBlock = {[weak self] (Void) -> Void in
            
            if let indexFound = self?.selectedProducts.index(where: {$0.orderDetailUuid == orderDetail.orderDetailUuid}) {
                PipesStore.shared.productRemovedFromSaleListPipe.putNext(orderDetail)
                
                self?.selectedProducts.remove(at: indexFound)
                let indexPath_ = IndexPath(row: indexFound, section: 0)
                self?.tableView.deleteRows(at: [indexPath_], with: .none)
                PipesStore.shared.totalSelectedOrderDetailsPipe.putNext((self?.selectedProducts)!)
            }
        }
        
        cell.quantityEditinEndBlock = {[weak self] (Void) -> Void in
            PipesStore.shared.totalSelectedOrderDetailsPipe.putNext((self?.selectedProducts)!)
        }
        
        // -------- AAAAAAAA ---------
        cell.priceButtonBlock = {[weak self]  (button: UIButton, orderDetail_: OrderDetail?) -> Void in
            
            if Global.shared.targetName == "TazaPos-lite" {
                return
            }
            
            if let orderDetail = orderDetail_ {
                var productName: String = ""
                if let product = Database.shared.productFor(productId: orderDetail.productId) {
                    productName = product.productName
                }
                
                let setPriceVC = SetNewPriceVC(initialPrice: orderDetail.price, initialPoductName: productName)
                setPriceVC.newPriceSetBlock = {[weak self]  (newPrice: Double) -> Void in
                    orderDetail.price = newPrice
                    if let indexFound = self?.selectedProducts.index(where: {$0.orderDetailUuid == orderDetail.orderDetailUuid}) {
                        let indexPath_ = IndexPath(row: indexFound, section: 0)
                        self?.tableView.reloadRows(at: [indexPath_], with: .none)
                        PipesStore.shared.totalSelectedOrderDetailsPipe.putNext((self?.selectedProducts)!)
                    }
                }
                
                setPriceVC.modalPresentationStyle = UIModalPresentationStyle.popover
                setPriceVC.preferredContentSize = CGSize(width: 400, height: 200)
                
                self?.present(setPriceVC, animated: true, completion: nil)
                
                let popoverPresentationController = setPriceVC.popoverPresentationController
                popoverPresentationController?.delegate = self
                popoverPresentationController?.sourceView = button
                popoverPresentationController?.sourceRect = CGRect(x: 0, y: 0, width: button.frame.size.width, height: button.frame.size.height)
            }
        }
        // -------- FINISH ---------
        
        cell.reloadData()
        return cell
    }
    
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
}


extension SelectedProductsTVC: UIPopoverPresentationControllerDelegate {
    
    // optional
    public func prepareForPopoverPresentation(_ popoverPresentationController: UIPopoverPresentationController) {
    }
    
    // optional
    // Called on the delegate when the popover controller will dismiss the popover. Return NO to prevent the
    // dismissal of the view.
    public func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
    
    // optional
    // Called on the delegate when the user has taken action to dismiss the popover. This is not called when the popover is dimissed programatically.
    public func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
    }
    
    // optional
    // -popoverPresentationController:willRepositionPopoverToRect:inView: is called on your delegate when the
    // popover may require a different view or rectangle.
    public func popoverPresentationController(_ popoverPresentationController: UIPopoverPresentationController, willRepositionPopoverTo rect: UnsafeMutablePointer<CGRect>, in view: AutoreleasingUnsafeMutablePointer<UIView>) {
    }
}
