//
//  ReceipPrinter.swift
//  Rentautomation
//
//  Created by kanybek on 9/5/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

final class ReceipPrinter {
    
    static let shared: ReceipPrinter = ReceipPrinter()
    init() {}
    
    func printReceipt__(order: Order) {
        
        let host: String = "192.168.123.100"
        let port: UInt16 = 9100
        let timeout: TimeInterval = 10
        
        let manager = MMReceiptManager(host: host,
                                       port: port,
                                       timeout: timeout)
        
        manager?.basicSetting()
        
        let name = AppSettings.shared.getCompanyName()
        manager?.writeData_title(name, scale: scale_2, type: MiddleAlignment)
        
        var customerOrSupplierName: String = ""
        if let employee_ = Database.shared.userBy(userId: order.userId) {
            customerOrSupplierName = employee_.firstName
        }
        manager?.writeData_items([customerOrSupplierName])
        
        let time = "Дата:\(order.orderDate.toString(.custom("dd.MM.yyyy HH:mm")))"
        let idNumnber = "Чек:\(order.orderId)"
        manager?.writeData_content([["key01":time, "key02":idNumnber]])
        
        manager?.writeData_line()
        manager?.writeData_content([["key01":"Наименование", "key02":"Цена", "key03":"Кол.", "key04":"Итог"]])
        manager?.writeData_line()
        
        
        var arrayOfDict = [[ : ]]
        let orderDetails_ = Database.shared.orderDetailsBy(orderId: order.orderId)
        for (localIndex, orderDetail_) in orderDetails_.enumerated() {
            
            let numberString = "\(localIndex + 1). "
            
            var productName: String = ""
            //var quantityPerUnit: String = ""
            if let product = Database.shared.productFor(productId: orderDetail_.productId) {
                productName = product.productName
                //quantityPerUnit = product.quantityPerUnit
            }
            
            let productName__ = "\(numberString + productName)"
            let productPrice__  = Global.shared.amountFormatter.string(for: orderDetail_.price)!
            
            var orderQuantity: String = ""
            if let text_ = Global.shared.amountFormatter.string(for: orderDetail_.orderQuantity) {
                orderQuantity = text_
            }
            
            //let productQuantity__ = "\(orderQuantity) \(quantityPerUnit)"
            let productQuantity__ = "\(orderQuantity)"
            
            let totalPriceWithDiscount = orderDetail_.price * orderDetail_.orderQuantity
            let totalPriceQuantity__ =  Global.shared.amountFormatter.string(for: totalPriceWithDiscount)!
            
            arrayOfDict.append(["key01":productName__,
                                "key02":productPrice__,
                                "key03":productQuantity__,
                                "key04":totalPriceQuantity__])
        }
        
        manager?.writeData_content(arrayOfDict)
        manager?.writeData_line()
        
        
        var totalSumWithDiscount: Double = 0.0
        if let payment = Database.shared.paymentBy(paymentId: order.paymentId) {
            totalSumWithDiscount = payment.totalPriceWithDiscountF()
        }
        let ddd = Global.shared.priceFormatter.string(for: totalSumWithDiscount)!
        let ll = "\(String(describing: ddd))"
        
        manager?.writeData_items(["Сумма: \(ll)"])
        manager?.writeData_title("Made By TazaPos", scale: scale_1, type: MiddleAlignment)
        
        manager?.openCashDrawer()
        manager?.printReceipt()
    }
}
