//
//  MainDocumentCompanion.swift
//  Rentautomation
//
//  Created by kanybek on 1/7/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class MainDocumentCompanion {

    var initialOrder: Order?
    var orderDocument: OrderDocumentType
    
    convenience init() {
        self.init(orderDocument: .none, initalOrder: nil)
    }
    
    init(orderDocument: OrderDocumentType, initalOrder: Order?) {
        
        self.orderDocument = orderDocument
        self.initialOrder = initalOrder
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("MainDocumentCompanion deleted")
    }
    
    func performSaveOrder<T: BaseListVC>(senderVC: T,
                                         employee: User,
                                         orderDetails: [OrderDetail],
                                         orderDocType: OrderDocumentType,
                                         totalPrice: Double,
                                         totalDiscount: Discount) where T: ErrorPopoverRenderer {
        
        //(1) orderDetails_ = self.orderDetails  //[OrderDetail]?
        //(2) let employee_ = AppSession.shared.currentEmployee //Staff
        //(3) OrderMemomry.shared.customerId // UInt64
        //(4) OrderMemomry.shared.supplierId // UInt64
        //(5) self.orderDocument // OrderDocumentType
        //(6) self.totalPrice // Double
        //(7) discount // Double
        
        switch orderDocType {
            
        case .saleToCustomer:
            print("Продажа:")
            if OrderMemomry.shared.customerId > 0 {
                let customerId = OrderMemomry.shared.customerId
                
                let paymentVC = PaymentVC(totalPrice: totalDiscount.totalPriceWithDiscount(), orderDocument: orderDocType, userId: customerId)
                paymentVC.performSaveBlock = {[weak self] (Void) -> Void in
                    
                    if let initialOrder_ = self?.initialOrder {
                        OrderEditManager.shared.updateInitialOrder(initialCustomerPreOrder: initialOrder_)
                    }
                    
                    let order_ = OrderManager.shared.ownerSaledProductsToCustomer(orderDetails: orderDetails,
                                                                                  staff: employee,
                                                                                  customerId: customerId,
                                                                                  orderDocType: orderDocType,
                                                                                  totalPrice: totalPrice,
                                                                                  discount: totalDiscount)
                    
                    
                    ReceipPrinter.shared.printReceipt__(order: order_)
                }
                
                paymentVC.dismissBlock = {[weak self] (staffEnteredMoney: Double) -> Void in
                    
                    if staffEnteredMoney > 0.01 {
                        OrderManager.shared.ownerReceivedMoneyFromCustomer(customerId: customerId,
                                                                           staffId: employee.userId,
                                                                           receivedMoney: staffEnteredMoney,
                                                                           isMoneyForDebt: false,
                                                                           comment: OrderMemomry.shared.comments)
                    }
                    
                    OrderMemomry.shared.clearAllVariables()
                    senderVC.navigationController?.dismiss(animated: true, completion: {[weak self] in
                        senderVC.showSuccessAlert(text: "Документ успешно сохранен!")
                        senderVC.clearSplitDetailVC()
                        print("dismiss MainDocumentVC")
                    })
                }
                senderVC.presentSheet(viewController: paymentVC)
            }
        case .receiveFromSupplier:
            print("Закупка")
            if (OrderMemomry.shared.supplierId > 0) {
                
                let supplierId = OrderMemomry.shared.supplierId
                let paymentVC = PaymentVC(totalPrice: totalDiscount.totalPriceWithDiscount(), orderDocument: orderDocType, userId: supplierId)
                paymentVC.performSaveBlock = {[weak self] (Void) -> Void in
                    OrderManager.shared.ownerReceivedProductsFromSupplier(orderDetails: orderDetails,
                                                                          staff: employee,
                                                                          supplierId: supplierId,
                                                                          orderDocType: orderDocType,
                                                                          totalPrice: totalPrice,
                                                                          discount: totalDiscount)
                }
                
                paymentVC.dismissBlock = {[weak self] (staffEnteredMoney: Double) -> Void in
                    
                    if staffEnteredMoney > 0.01 {
                        OrderManager.shared.ownerGivedMoneyToSupplierForProduct(supplierId: supplierId,
                                                                                staffId: employee.userId,
                                                                                givedMoney: staffEnteredMoney,
                                                                                comment: OrderMemomry.shared.comments)
                    }
                    
                    OrderMemomry.shared.clearAllVariables()
                    senderVC.navigationController?.dismiss(animated: true, completion: {[weak self] in
                        senderVC.showSuccessAlert(text: "Документ успешно сохранен!")
                        senderVC.clearSplitDetailVC()
                        print("dismiss MainDocumentVC")
                    })
                }
                senderVC.presentSheet(viewController: paymentVC)
            }
            
        case .returnFromCustomer:
            print("Возврат продукта от клиента")
            if OrderMemomry.shared.customerId > 0 {
                
                let customerId = OrderMemomry.shared.customerId
                
                let paymentVC = PaymentVC(totalPrice: totalDiscount.totalPriceWithDiscount(), orderDocument: orderDocType, userId: customerId)
                paymentVC.performSaveBlock = {[weak self] (Void) -> Void in
                    OrderManager.shared.ownerReceivedRetrunProductsFromCustomer(orderDetails: orderDetails,
                                                                                staff: employee,
                                                                                customerId: customerId,
                                                                                orderDocType: orderDocType,
                                                                                totalPrice: totalPrice,
                                                                                discount: totalDiscount)
                }
                
                paymentVC.dismissBlock = {[weak self] (staffEnteredMoney: Double) -> Void in
                    
                    if staffEnteredMoney > 0.01 {
                        OrderManager.shared.ownerReturnedMoneyToCustomer(customerId: customerId,
                                                                         staffId: employee.userId,
                                                                         returnedMoney: staffEnteredMoney,
                                                                         comment: OrderMemomry.shared.comments)
                    }
                    
                    OrderMemomry.shared.clearAllVariables()
                    senderVC.navigationController?.dismiss(animated: true, completion: {[weak self] in
                        senderVC.showSuccessAlert(text: "Документ успешно сохранен!")
                        senderVC.clearSplitDetailVC()
                        print("dismiss MainDocumentVC")
                    })
                }
                
                senderVC.presentSheet(viewController: paymentVC)
            }
            
        case .returnToSupplier:
            print("Возврат продукта к потавщику")
            if (OrderMemomry.shared.supplierId > 0) {
                
                let supplierId = OrderMemomry.shared.supplierId
                
                OrderManager.shared.ownerReturnedProductsToSupplier(orderDetails: orderDetails,
                                                                    staff: employee,
                                                                    supplierId: supplierId,
                                                                    orderDocType: orderDocType,
                                                                    totalPrice: totalPrice,
                                                                    discount: totalDiscount)
                
                OrderMemomry.shared.clearAllVariables()
                senderVC.navigationController?.dismiss(animated: true, completion: {[weak self] in
                    senderVC.showSuccessAlert(text: "Документ успешно сохранен!")
                    senderVC.clearSplitDetailVC()
                    print("dismiss MainDocumentVC")
                })
            }
            
        case .saleToCustomerEdited:
            print("Редактирование продажи")
            if OrderMemomry.shared.customerId > 0 {
                
                let customerId = OrderMemomry.shared.customerId
                OrderEditManager.shared.ownerEditedProductsSaleToCustomer(initialSaleOrder: self.initialOrder!,
                                                                          newOrderDetails: orderDetails,
                                                                          newStaff: employee,
                                                                          newCustomerId: customerId,
                                                                          newOrderDocumentType: orderDocType,
                                                                          newTotalPrice: totalPrice,
                                                                          newDiscount: totalDiscount)
                OrderMemomry.shared.clearAllVariables()
                senderVC.navigationController?.dismiss(animated: true, completion: {[weak self] in
                    senderVC.showSuccessAlert(text: "Документ успешно сохранен!")
                    senderVC.clearSplitDetailVC()
                    print("dismiss MainDocumentVC")
                })
            }
            
        case .receiveFromSupplierEdited:
            print("Редактирование закупки")
            if OrderMemomry.shared.supplierId > 0 {
                
                let supplierId = OrderMemomry.shared.supplierId
                OrderEditManager.shared.ownerEditedProductsReceiveFromSupplier(initialProductIncomeOrder: self.initialOrder!,
                                                                               newOrderDetails: orderDetails,
                                                                               newStaff: employee,
                                                                               newSupplier: supplierId,
                                                                               newOrderDocumentType: orderDocType,
                                                                               newTotalPrice: totalPrice,
                                                                               newDiscount: totalDiscount)
                OrderMemomry.shared.clearAllVariables()
                senderVC.navigationController?.dismiss(animated: true, completion: {[weak self] in
                    senderVC.showSuccessAlert(text: "Документ успешно сохранен!")
                    senderVC.clearSplitDetailVC()
                    print("dismiss MainDocumentVC")
                })
            }
        
        case .preOrderToCustomer:
            print("Заказ для клиента")
            if OrderMemomry.shared.customerId > 0 {
                
                let customerId = OrderMemomry.shared.customerId
                let _ = OrderManager.shared.ownerMadePreOrderForCustomer(orderDetails: orderDetails,
                                                                         staff: employee,
                                                                         customerId: customerId,
                                                                         orderDocType: orderDocType,
                                                                         totalPrice: totalPrice,
                                                                         discount: totalDiscount)
                OrderMemomry.shared.clearAllVariables()
                senderVC.navigationController?.dismiss(animated: true, completion: {[weak self] in
                    senderVC.showSuccessAlert(text: "Документ успешно сохранен!")
                    senderVC.clearSplitDetailVC()
                    print("dismiss MainDocumentVC")
                })
            }
            
        case .preOrderToCustomerEdited:
            print("Заказ для клиента редактирование")
            if OrderMemomry.shared.customerId > 0 {
                
                let customerId = OrderMemomry.shared.customerId
                let _ = OrderEditManager.shared.ownerEdittedPreOrderForCustomer(initialCustomerPreOrder: self.initialOrder!,
                                                                                orderDetails: orderDetails,
                                                                                staff: employee,
                                                                                customerId: customerId,
                                                                                orderDocumentType: .preOrderToCustomer,
                                                                                totalPrice: totalPrice,
                                                                                discount: totalDiscount)
                
                OrderMemomry.shared.clearAllVariables()
                senderVC.navigationController?.dismiss(animated: true, completion: {[weak self] in
                    senderVC.showSuccessAlert(text: "Документ успешно сохранен!")
                    senderVC.clearSplitDetailVC()
                    print("dismiss MainDocumentVC")
                })
            }
            
        case .stockTaking:
            print("Инвентаризация / Черновик")
            let customerId = OrderMemomry.shared.customerId
            let _ = OrderManager.shared.ownerMadePreOrderForCustomer(orderDetails: orderDetails,
                                                                     staff: employee,
                                                                     customerId: customerId,
                                                                     orderDocType: orderDocType,
                                                                     totalPrice: totalPrice,
                                                                     discount: totalDiscount)
            OrderMemomry.shared.clearAllVariables()
            senderVC.navigationController?.dismiss(animated: true, completion: {[weak self] in
                senderVC.showSuccessAlert(text: "Документ успешно сохранен!")
                senderVC.clearSplitDetailVC()
                print("dismiss MainDocumentVC")
            })
            
            
        case .rawProductGoneToProd:
            print("rawProductGoneToProd")
            let _ = Creator.shared.rawProductsGoneToProduction(orderDetails: orderDetails,
                                                               staff: employee,
                                                               orderDocType: orderDocType,
                                                               totalPrice: totalPrice,
                                                               discount: totalDiscount)
            OrderMemomry.shared.clearAllVariables()
            senderVC.navigationController?.dismiss(animated: true, completion: {[weak self] in
                senderVC.showSuccessAlert(text: "Документ успешно сохранен!")
                senderVC.clearSplitDetailVC()
            })
        case .rawProductGoneToProdEdited:
            print("rawProductGoneToProdEdited")
            let _ = Creator.shared.rawProductsGoneToProductionEditted(shouldIncrease: true,
                                                                      initialOrder: self.initialOrder!,
                                                                      newOrderDetails: orderDetails,
                                                                      newStaff: employee,
                                                                      newCustomerId: 0,
                                                                      newOrderDocType: orderDocType,
                                                                      newTotalPrice: totalPrice,
                                                                      newDiscount: totalDiscount)
            OrderMemomry.shared.clearAllVariables()
            senderVC.navigationController?.dismiss(animated: true, completion: {[weak self] in
                senderVC.showSuccessAlert(text: "Документ успешно сохранен!")
                senderVC.clearSplitDetailVC()
            })
            
        case .readyProductReceivedFromProd:
            print("readyProductReceivedFromProd")
            let _ = Creator.shared.readyProductReceivedFromProduction(orderDetails: orderDetails,
                                                                      staff: employee,
                                                                      orderDocType: orderDocType,
                                                                      totalPrice: totalPrice,
                                                                      discount: totalDiscount)
            OrderMemomry.shared.clearAllVariables()
            senderVC.navigationController?.dismiss(animated: true, completion: {[weak self] in
                senderVC.showSuccessAlert(text: "Документ успешно сохранен!")
                senderVC.clearSplitDetailVC()
            })
        case .readyProductReceivedFromProdEdited:
            print("readyProductReceivedFromProdEdited")
            let _ = Creator.shared.readyProductReceivedFromProductionEditted(shouldIncrease: false,
                                                                             initialOrder: self.initialOrder!,
                                                                             newOrderDetails: orderDetails,
                                                                             newStaff: employee,
                                                                             newCustomerId: 0,
                                                                             newOrderDocType: orderDocType,
                                                                             newTotalPrice: totalPrice,
                                                                             newDiscount: totalDiscount)
            OrderMemomry.shared.clearAllVariables()
            senderVC.navigationController?.dismiss(animated: true, completion: {[weak self] in
                senderVC.showSuccessAlert(text: "Документ успешно сохранен!")
                senderVC.clearSplitDetailVC()
            })
            
        case .incomeProductFromStock:
            let _ = Creator.shared.incomeProductFromStock(orderDetails: orderDetails,
                                                          staff: employee,
                                                          orderDocType: orderDocType,
                                                          totalPrice: totalPrice,
                                                          discount: totalDiscount)
            OrderMemomry.shared.clearAllVariables()
            senderVC.navigationController?.dismiss(animated: true, completion: {[weak self] in
                senderVC.showSuccessAlert(text: "Документ успешно сохранен!")
                senderVC.clearSplitDetailVC()
            })
        case .incomeProductFromStockEdited:
            print("incomeProductFromStockEdited")
        
            
            
        case .removeProducts:
            let _ = Creator.shared.removeProductsFromStock(orderDetails: orderDetails,
                                                           staff: employee,
                                                           orderDocType: orderDocType,
                                                           totalPrice: totalPrice,
                                                           discount: totalDiscount)
            OrderMemomry.shared.clearAllVariables()
            senderVC.navigationController?.dismiss(animated: true, completion: {[weak self] in
                senderVC.showSuccessAlert(text: "Документ успешно сохранен!")
                senderVC.clearSplitDetailVC()
            })
        case .removeProductsEdited:
            print("removeProductsEdited")
            
            
            
        case .productMovememntToStock:
            print("productMovememntToStock")
            let _ = Creator.shared.productMovememntToStock(orderDetails: orderDetails,
                                                           staff: employee,
                                                           orderDocType: orderDocType,
                                                           totalPrice: totalPrice,
                                                           discount: totalDiscount)
            OrderMemomry.shared.clearAllVariables()
            senderVC.navigationController?.dismiss(animated: true, completion: {[weak self] in
                senderVC.showSuccessAlert(text: "Документ успешно сохранен!")
                senderVC.clearSplitDetailVC()
            })
        case .productMovememntToStockEdited:
            print("productMovememntToStockEdited")
            
        default:
            print("default:")
        }
    }
}
