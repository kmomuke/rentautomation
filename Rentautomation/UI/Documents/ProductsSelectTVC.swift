//
//  ProductsSelectTVC.swift
//  Rentautomation
//
//  Created by kanybek on 12/6/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit
import Kingfisher
import CSNotificationView

class ProductsSelectTVC: UITableViewController, ErrorPopoverRenderer {
    
    var initialOrder: Order?
    var notificationView: CSNotificationView?
    
    var products: [Product] = []
    var badgeDict: [UInt64: Int] = [:]
    
    var orderDocument: OrderDocumentType
    
    var priceString: String
    var shouldUseSalePrice: Bool = false
    
    var productRemovedDispose: Disposable?
    
    convenience init() {
        self.init(initialOrderDocument: .none, initialOrder: nil)
    }
    
    init(initialOrderDocument: OrderDocumentType, initialOrder: Order?) {
        self.orderDocument = initialOrderDocument
        self.initialOrder = initialOrder
        self.priceString = ""
        self.shouldUseSalePrice = false
        super.init(nibName: "ProductsSelectTVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.productRemovedDispose?.dispose()
        self.productRemovedDispose = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.keyboardDismissMode = .onDrag
        self.tableView.alwaysBounceVertical = true
        
        switch self.orderDocument {
        
        case .saleToCustomer:
            self.priceString = ""
            self.shouldUseSalePrice = true
        
        case .receiveFromSupplier:
            self.priceString = "закуп."
            self.shouldUseSalePrice = false
        
        case .returnFromCustomer:
            self.priceString = ""
            self.shouldUseSalePrice = true
        
        case .returnToSupplier:
            self.priceString = "закуп."
            self.shouldUseSalePrice = false
            
        case .saleToCustomerEdited:
            self.priceString = ""
            self.shouldUseSalePrice = true
            
        case .receiveFromSupplierEdited:
            self.priceString = "закуп."
            self.shouldUseSalePrice = false
            
        default:
            self.priceString = "прод."
            self.shouldUseSalePrice = true
        }
        
        self.takeIntialDataFromDatabase()
        
        self.title = "Товары"
        let nib = UINib(nibName: "ProductsListTVCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "ProductsListTVCell")
        
        let productRemovedSignal: Signal = PipesStore.shared.productRemovedFromSaleListSignal()
        self.productRemovedDispose = productRemovedSignal.start(next: {[weak self] (orderDetail: OrderDetail) in
            
            if let indexFound = self?.products.index(where: {$0.productId == orderDetail.productId}) {
                self?.badgeDict[orderDetail.productId] = 0
                let indexPath_ = IndexPath(row: indexFound, section: 0)
                self?.tableView.reloadRows(at: [indexPath_], with: .none)
            } else {
                self?.badgeDict[orderDetail.productId] = 0
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    private func takeIntialDataFromDatabase() -> Void {
        Database.shared.loadAllProductsBy(categoryId: 0,
                                          limit: Global.shared.limitForStaticModels) {[weak self] (products_: [Product]) in
                                            
                                            self?.products = products_
                                            
                                            for prod in products_ {
                                                let key = prod.productId
                                                let value = 0
                                                self?.badgeDict[key] = value
                                            }
                                            
                                            if let order_ = self?.initialOrder {
                                                let orderDetails = Database.shared.orderDetailsBy(orderId: order_.orderId)
                                                for orderDetail in orderDetails {
                                                    self?.badgeDict[orderDetail.productId] = 1
                                                }
                                            }
                                            
                                            self?.tableView.reloadData()
        }
    }
    
    private func reloadAllDataFromDatabase() -> Void {
        Database.shared.loadAllProductsBy(categoryId: 0,
                                          limit: Global.shared.limitForStaticModels) {[weak self] (products_: [Product]) in
                                            self?.products = products_
                                            self?.tableView.reloadData()
        }
    }
    
    public func reloadTableData(_ products_: [Product]) -> Void {
        self.products = products_
        self.tableView.reloadData()
    }
    
    public func showNotificationView(_ message_: String) -> Void {
        
        self.notificationView = CSNotificationView(parentViewController: self,
                                                   tintColor: UIColor.midnightBlueColor(),
                                                   image: UIImage(named: "close-X")!,
                                                   message: message_)
        
        self.notificationView?.tapHandler = {[weak self] (Void) -> Void in
            self?.hideNotificationView()
            self?.reloadAllDataFromDatabase()
        }
        
        self.notificationView?.setVisible(true, animated: true, completion: {[weak self] in
            if let strongSelf = self {
                print("show completed")
            }
        })
    }
    
    public func hideNotificationView() -> Void {
        if let notifView = self.notificationView {
            notifView.setVisible(false, animated: true, completion: {[weak self] in
                if let strongSelf = self {
                    print("hide completed")
                }
            })
        }
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 81.0
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductsListTVCell", for: indexPath) as! ProductsListTVCell
        let product = products[(indexPath as NSIndexPath).row]
        //cell.product = product
        //cell.reloadData()
        
        if Global.shared.isDebug {
            cell.productPrimaryKeyLabel.text = "\(product.productId)"
            cell.productPrimaryKeyLabel.backgroundColor = UIColor.clear
        } else {
            cell.productPrimaryKeyLabel.isHidden = true
        }
        
        cell.incomePriceLabel.text = ""
        
        var price: Double = 0.0
        if self.shouldUseSalePrice {
            price = product.saleUnitPrice
        } else {
            price = product.incomeUnitPrice
        }
        
        if let stringPrice = Global.shared.priceFormatter.string(for: price) {
            cell.salePriceLabel.text = "\(self.priceString) \(stringPrice)"
        }
        
        cell.nameLabel.text = product.productName
        
        
        var unitsInStock = ""
        if let text_ = Global.shared.amountFormatter.string(for: product.unitsInStock) {
            unitsInStock = text_
        }
        cell.productKeyLabel.text = "ост: \(unitsInStock) \(product.quantityPerUnit)"
        
        if let url_ = product.productImagePath {
            
            if url_.characters.count > 4 {
                let url = URL(string: url_)
                if url_.lowercased().range(of:"https") != nil {
                    let processor = ResizingImageProcessor(referenceSize: CGSize(width: 100, height: 100), mode: .none)
                    cell.cellImageView.kf.indicatorType = .activity
                    cell.cellImageView.kf.setImage(with: url,
                                                   placeholder: Global.shared.productPlaceholderImage,
                                                   options: [.processor(processor), .transition(.fade(0.2))],
                                                   progressBlock: nil,
                                                   completionHandler: { (im: Image?, err: NSError?, typr: CacheType, url: URL?) in
                    })
                } else {
                    cell.cellImageView.kf.setImage(with: url,
                                                   placeholder: Global.shared.productPlaceholderImage,
                                                   options: nil,
                                                   progressBlock: nil,
                                                   completionHandler: { (im: Image?, err: NSError?, typr: CacheType, url: URL?) in
                    })
                }
            } else {
                cell.cellImageView.image = Global.shared.productPlaceholderImage
            }
        } else {
            cell.cellImageView.image = Global.shared.productPlaceholderImage
        }
        
        //App installation failed
        //A valid provision profile for this executable was not found
        if let badgeValue = self.badgeDict[product.productId] {
            if badgeValue > 0 {
                cell.badgeString = "\(badgeValue)"
            } else {
                cell.badgeString = ""
            }
        } else {
            cell.badgeString = ""
        }
        
        if Database.shared.isModelSynchronized(primaryKey: product.productId) {
            cell.selectionStyle = .default
            cell.nameLabel.textColor = UIColor.black
        } else {
            cell.selectionStyle = .none
            cell.nameLabel.textColor = UIColor.pomergranateColor()
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let product = products[(indexPath as NSIndexPath).row]
        self.didSelectProduct(product_: product, indexPath_: indexPath)
    }
    
    private func didSelectProduct(product_: Product, indexPath_: IndexPath) {
        if Database.shared.isModelSynchronized(primaryKey: product_.productId) {
            
            if let _ = self.badgeDict[product_.productId] {
                self.badgeDict[product_.productId] = 1
            }
            
            tableView.reloadRows(at: [indexPath_], with: .none)
            
            PipesStore.shared.productSelectedPipe.putNext((product_, self.shouldUseSalePrice))
        } else {
            let _ = self.showErrorAlert(text: "Не сохранен")
        }
    }
    
}
