//
//  MinusPriceVC.swift
//  Rentautomation
//
//  Created by kanybek on 10/3/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class MinusPriceVC: UIViewController {

    @IBOutlet weak var titlePriceLabel: UILabel!
    @IBOutlet weak var priceTextField: BKCurrencyTextField!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var totalPriceWithDiscountLabel: UILabel!
    
    var minusSelectBlock : ((Double) -> Void)?
    var plusSelectBlock : ((Double) -> Void)?
    
    var initialDiscount: Discount?
    var shouldMinusPrice: Bool
    
    convenience init() {
        self.init(initialDiscount: nil, shouldMinusPrice: true)
    }

    init(initialDiscount: Discount?, shouldMinusPrice: Bool) {
        self.initialDiscount = initialDiscount
        self.shouldMinusPrice = shouldMinusPrice
        super.init(nibName: "MinusPriceVC", bundle: nil)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {
        print("MinusPriceVC deallocated")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.priceTextField.delegate = self
        
        if let initialDiscount_ = self.initialDiscount {
            self.totalPriceLabel.text = Global.shared.priceFormatter.string(for: initialDiscount_.totalPrice)
            self.totalPriceWithDiscountLabel.text = Global.shared.priceFormatter.string(for: initialDiscount_.totalPriceWithDiscount())
            
            if shouldMinusPrice {
                self.titlePriceLabel.text = "Минус от суммы"
                self.priceTextField.numberValue = NSDecimalNumber(value: initialDiscount_.minusPrice)
            } else {
                self.titlePriceLabel.text = "Плюс к сумме"
                self.priceTextField.numberValue = NSDecimalNumber(value: initialDiscount_.plusPrice)
            }
        }
                
        let events: [(Selector, UIControlEvents)] = [(#selector(MinusPriceVC.textChanged(textField:)), .editingChanged),
                                                     (#selector(MinusPriceVC.editingDidBegin(textField:)), .editingDidBegin),
                                                     (#selector(MinusPriceVC.editingDidEnd(textField:)), .editingDidEnd)]
        events.forEach {
            self.priceTextField.addTarget(self, action: $0.0, for: $0.1)
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.priceTextField.becomeFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:
    private func updateLabels(_ number: Double) {
        if let initialDiscount_ = self.initialDiscount {
            
            if shouldMinusPrice {
                initialDiscount_.minusPrice = number
                if let minusSelectBlock_ = self.minusSelectBlock {
                    minusSelectBlock_(number)
                }
            } else {
                initialDiscount_.plusPrice = number
                if let plusSelectBlock_ = self.plusSelectBlock {
                    plusSelectBlock_(number)
                }
            }
            
            let k = Global.shared.priceFormatter.string(for: initialDiscount_.totalPriceWithDiscount())!
            self.totalPriceWithDiscountLabel.text = k
        }
    }
    
    private dynamic func textChanged(textField: UITextField) {
        if (self.priceTextField.text?.characters.count)! > 0 {
            let number = self.priceTextField.numberValue.doubleValue
            self.updateLabels(number)
        } else {
            let number: Double = 0.0
            self.updateLabels(number)
        }
    }
    
    private dynamic func editingDidBegin(textField: UITextField) {
    }
    
    private dynamic func editingDidEnd(textField: UITextField) {
    }
    
    //MARK:
    func keyboardWillShow(_ notification: NSNotification) {
    }
    
    func keyboardWillHide(_ notification: NSNotification) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension MinusPriceVC: UITextFieldDelegate {
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.dismiss(animated: true, completion: nil)
        return true
    }
}
