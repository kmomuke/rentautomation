//
//  SelectDiscountTVC.swift
//  Rentautomation
//
//  Created by kanybek on 1/6/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class SelectDiscountTVC: UITableViewController {
    
    var initialDiscount: Discount?

    var badgeDict: [UInt32: Int] = [:]
    
    lazy var discounts: [UInt32] = {
        var discounts_: [UInt32] = []
        for index in 0...99 {
            discounts_.append(UInt32(index))
        }
        return discounts_
    }()
    
    var dicsountSelectBlock : ((Double) -> Void)?
    
    convenience init() {
        self.init(initialDiscount: nil)
    }
    
    init(initialDiscount: Discount?) {
        self.initialDiscount = initialDiscount
        super.init(nibName: "SelectDiscountTVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "CategoriesListTVCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "CategoriesListTVCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let initialDiscount_ = self.initialDiscount {
            let discount = UInt32(initialDiscount_.discount)
            if let indexFound = self.discounts.index(where: {$0 == discount}) {
                self.badgeDict.removeAll()
                self.badgeDict[discount] = 1
                self.tableView.scrollToRow(at: IndexPath(row: indexFound, section: 0), at: .middle, animated: false)
                self.tableView.reloadRows(at: [IndexPath(row: indexFound, section: 0)], with: .fade)
            }
        }
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return discounts.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoriesListTVCell", for: indexPath) as! CategoriesListTVCell
        let object = discounts[(indexPath as NSIndexPath).row]
        cell.categoryNameLabel.text = "\(object)%"
        cell.categoryPrimaryKeyLabel.isHidden = true
        
        if let badgeValue = self.badgeDict[object] {
            if badgeValue > 0 {
                cell.categoryNameLabel.font = UIFont.systemFont(ofSize: 17, weight: UIFontWeightBold)
                cell.categoryNameLabel.textColor = UIColor.pomergranateColor()
                cell.contentView.backgroundColor = UIColor.concreteColor()
            } else {
                cell.categoryNameLabel.font = UIFont.systemFont(ofSize: 17, weight: UIFontWeightRegular)
                cell.categoryNameLabel.textColor = UIColor.darkGray
                cell.contentView.backgroundColor = UIColor.white
            }
        } else {
            cell.categoryNameLabel.font = UIFont.systemFont(ofSize: 17, weight: UIFontWeightRegular)
            cell.categoryNameLabel.textColor = UIColor.darkGray
            cell.contentView.backgroundColor = UIColor.white
        }
        
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        let object = discounts[(indexPath as NSIndexPath).row]
        
        if let discountBlock_ = self.dicsountSelectBlock {
            discountBlock_(Double(object))
            self.dismiss(animated: true, completion: { 
                print("SelectDiscountTVC dismissed")
            })
        }
    }
}
