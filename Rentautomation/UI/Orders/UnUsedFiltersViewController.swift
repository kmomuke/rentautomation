//
//  UnUsedFiltersViewController.swift
//  Rentautomation
//
//  Created by kanybek on 2/10/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class UnUsedFiltersViewController: FormViewController, ErrorPopoverRenderer {
    
    var initialItem: String?
    
    private enum InsertPosition: Int {
        case Below, Above
    }
    private var insertRowPosition: InsertPosition = .Below
    
    convenience init() {
        self.init(initialItem: nil)
    }
    
    init(initialItem: String?) {
        self.initialItem = initialItem
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    // MARK: Public
    override func viewDidLoad() {
        super.viewDidLoad()
        let canceButton = UIBarButtonItem(
            title: "Закрыть",
            style: .plain,
            target: self,
            action: #selector(cancelObject(_:))
        )
        self.view.backgroundColor = UIColor.white
        self.navigationItem.leftBarButtonItem = canceButton
        
        let createButton = UIBarButtonItem(
            title: "Применить фильтры",
            style: .plain,
            target: self,
            action: #selector(applyFilters(_:))
        )
        
        self.navigationItem.rightBarButtonItem = createButton
        configure()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
        
        //if ProductMemoryInstance.sharedInstance.showDetailedInfo {
        //    for row in self.optionalProductSection.rowFormers {
        //        row.update()
        //    }
        //}
    }
    
    func applyFilters(_ sender: AnyObject) -> Void {
    }
    
    func cancelObject(_ sender: AnyObject) -> Void {
        self.navigationController?.dismiss(animated: true, completion: {
            print("dismiss CreateCustomerVC")
        })
    }
    
    // MARK: Private
    private enum Repeat {
        case Never, Daily, Weekly, Monthly, Yearly
        func title() -> String {
            switch self {
            case .Never: return "Never"
            case .Daily: return "Daily"
            case .Weekly: return "Weekly"
            case .Monthly: return "Monthly"
            case .Yearly: return "Yearly"
            }
        }
        static func values() -> [Repeat] {
            return [Daily, Weekly, Monthly, Yearly]
        }
    }
    
    private enum Alert {
        case None, AtTime, Five, Thirty, Hour, Day, Week
        func title() -> String {
            switch self {
            case .None: return "None"
            case .AtTime: return "At time of event"
            case .Five: return "5 minutes before"
            case .Thirty: return "30 minutes before"
            case .Hour: return "1 hour before"
            case .Day: return "1 day before"
            case .Week: return "1 week before"
            }
        }
        static func values() -> [Alert] {
            return [AtTime, Five, Thirty, Hour, Day, Week]
        }
    }
    
    private func configure() {
        title = "Add Event"
        tableView.contentInset.top = 10
        tableView.contentInset.bottom = 30
        tableView.contentOffset.y = -10
        
        // Create RowFomers
        
        let titleRow = TextFieldRowFormer<FormTextFieldCell>() {
            $0.textField.textColor = .formerColor()
            $0.textField.font = .systemFont(ofSize: 15)
            }.configure {
                $0.placeholder = "Event title"
        }
        let locationRow = TextFieldRowFormer<FormTextFieldCell>() {
            $0.textField.textColor = .formerColor()
            $0.textField.font = .systemFont(ofSize: 15)
            }.configure {
                $0.placeholder = "Location"
        }
        
        let positions = ["Below", "Above"]
        
        let insertRowPositionRow = SegmentedRowFormer<FormSegmentedCell>(instantiateType: .Class) {
            $0.tintColor = .formerSubColor()
            $0.titleLabel.text = "Segmented"
            
            }.configure {
                $0.rowHeight = 60
                $0.segmentTitles = positions
                $0.selectedIndex = insertRowPosition.rawValue
            }.onSegmentSelected { [weak self] index, _ in
                self?.insertRowPosition = InsertPosition(rawValue: index)!
        }
        
        let startRow = InlineDatePickerRowFormer<FormInlineDatePickerCell>() {
            $0.titleLabel.text = "Start"
            $0.titleLabel.textColor = .formerColor()
            $0.titleLabel.font = .boldSystemFont(ofSize: 15)
            $0.displayLabel.textColor = .formerSubColor()
            $0.displayLabel.font = .systemFont(ofSize: 15)
            }.inlineCellSetup {
                $0.datePicker.datePickerMode = .dateAndTime
            }.displayTextFromDate(String.mediumDateShortTime).configure { (k: InlineDatePickerRowFormer<FormInlineDatePickerCell>) in
                k.rowHeight = 60
        }
        
        let endRow = InlineDatePickerRowFormer<FormInlineDatePickerCell>() {
            $0.titleLabel.text = "Start"
            $0.titleLabel.textColor = .formerColor()
            $0.titleLabel.font = .boldSystemFont(ofSize: 15)
            $0.displayLabel.textColor = .formerSubColor()
            $0.displayLabel.font = .systemFont(ofSize: 15)
            }.inlineCellSetup {
                $0.datePicker.datePickerMode = .dateAndTime
            }.displayTextFromDate(String.mediumDateShortTime).configure { (k: InlineDatePickerRowFormer<FormInlineDatePickerCell>) in
                k.rowHeight = 60
        }
        
        let allDayRow = SwitchRowFormer<FormSwitchCell>() {
            $0.titleLabel.text = "All-day"
            $0.titleLabel.textColor = .formerColor()
            $0.titleLabel.font = .boldSystemFont(ofSize: 15)
            $0.switchButton.onTintColor = .formerSubColor()
            }.onSwitchChanged { on in
                startRow.update {
                    $0.displayTextFromDate(
                        on ? String.mediumDateNoTime : String.mediumDateShortTime
                    )
                }
                startRow.inlineCellUpdate {
                    $0.datePicker.datePickerMode = on ? .date : .dateAndTime
                }
                endRow.update {
                    $0.displayTextFromDate(
                        on ? String.mediumDateNoTime : String.mediumDateShortTime
                    )
                }
                endRow.inlineCellUpdate {
                    $0.datePicker.datePickerMode = on ? .date : .dateAndTime
                }
            }.configure { (k: SwitchRowFormer<FormSwitchCell>) in
                k.rowHeight = 60
        }
        
        
        
        
        
        let repeatRow = InlinePickerRowFormer<FormInlinePickerCell, Repeat>() {
            $0.titleLabel.text = "Repeat"
            $0.titleLabel.textColor = .formerColor()
            $0.titleLabel.font = .boldSystemFont(ofSize: 15)
            $0.displayLabel.textColor = .formerSubColor()
            $0.displayLabel.font = .systemFont(ofSize: 15)
            }.configure {
                let never = Repeat.Never
                $0.pickerItems.append(
                    InlinePickerItem(title: never.title(),
                                     displayTitle: NSAttributedString(string: never.title(),
                                                                      attributes: [NSForegroundColorAttributeName: UIColor.lightGray]),
                                     value: never)
                )
                $0.pickerItems += Repeat.values().map {
                    InlinePickerItem(title: $0.title(), value: $0)
                }
        }
        let alertRow = InlinePickerRowFormer<FormInlinePickerCell, Alert>() {
            $0.titleLabel.text = "Alert"
            $0.titleLabel.textColor = .formerColor()
            $0.titleLabel.font = .boldSystemFont(ofSize: 15)
            $0.displayLabel.textColor = .formerSubColor()
            $0.displayLabel.font = .systemFont(ofSize: 15)
            }.configure {
                let none = Alert.None
                $0.pickerItems.append(
                    InlinePickerItem(title: none.title(),
                                     displayTitle: NSAttributedString(string: none.title(),
                                                                      attributes: [NSForegroundColorAttributeName: UIColor.lightGray]),
                                     value: none)
                )
                $0.pickerItems += Alert.values().map {
                    InlinePickerItem(title: $0.title(), value: $0)
                }
        }
        let urlRow = TextFieldRowFormer<FormTextFieldCell>() {
            $0.textField.textColor = .formerSubColor()
            $0.textField.font = .systemFont(ofSize: 15)
            $0.textField.keyboardType = .alphabet
            }.configure {
                $0.placeholder = "URL"
        }
        let noteRow = TextViewRowFormer<FormTextViewCell>() {
            $0.textView.textColor = .formerSubColor()
            $0.textView.font = .systemFont(ofSize: 15)
            }.configure {
                $0.placeholder = "Note"
                $0.rowHeight = 150
        }
        
        // Create Headers
        
        let createHeader: (() -> ViewFormer) = {
            return CustomViewFormer<FormHeaderFooterView>()
                .configure {
                    $0.viewHeight = 20
            }
        }
        
        // Create SectionFormers
        let labelRow = LabelRowFormer<FormLabelCell>()
            .configure {
                $0.text = "Text"
                $0.subText = "SubText"
            }.onSelected { [weak self] _ in
                self?.former.deselect(animated: true)
            }.onUpdate {
                $0.text = "Text _"
                $0.subText = "SubText _"
        }
        
        let titleSection0 = SectionFormer(rowFormer: labelRow)
            .set(headerViewFormer: createHeader())
        
        let titleSection = SectionFormer(rowFormer: titleRow, locationRow)
            .set(headerViewFormer: createHeader())
        
        let dateSection = SectionFormer(rowFormer: insertRowPositionRow, allDayRow, startRow, endRow)
            .set(headerViewFormer: createHeader())
        let repeatSection = SectionFormer(rowFormer: repeatRow, alertRow)
            .set(headerViewFormer: createHeader())
        
        let noteSection = SectionFormer(rowFormer: urlRow, noteRow)
            .set(headerViewFormer: createHeader())
        
        former.append(sectionFormer:titleSection0, dateSection)
    }
}
