//
//  CollapsibleTableViewHeader.swift
//  Rentautomation
//
//  Created by kanybek on 2/12/17.
//  Copyright © 2017 kanybek. All rights reserved.
//
//
//import UIKit
//
//protocol CollapsibleTableViewHeaderDelegate {
//    func toggleSection(header: CollapsibleTableViewHeader, section: Int)
//}
//
//class CollapsibleTableViewHeader: UITableViewHeaderFooterView {
//    
//    var delegate: CollapsibleTableViewHeaderDelegate?
//    var section: Int = 0
//    
//    let titleLabel = UILabel()
//    let arrowLabel = UILabel()
//    
//    override init(reuseIdentifier: String?) {
//        super.init(reuseIdentifier: reuseIdentifier)
//        
//        //
//        // Constraint the size of arrow label for auto layout
//        //
//        arrowLabel.widthAnchor.constraint(equalToConstant: 12).isActive = true
//        arrowLabel.heightAnchor.constraint(equalToConstant: 12).isActive = true
//        
//        titleLabel.translatesAutoresizingMaskIntoConstraints = false
//        arrowLabel.translatesAutoresizingMaskIntoConstraints = false
//        
//        contentView.addSubview(titleLabel)
//        contentView.addSubview(arrowLabel)
//        
//        //
//        // Call tapHeader when tapping on this header
//        //
//        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CollapsibleTableViewHeader.tapHeader(gestureRecognizer:))))
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        
//        contentView.backgroundColor = UIColor(hex: 0x2E3944)
//        
//        titleLabel.textColor = UIColor.white
//        arrowLabel.textColor = UIColor.white
//        
//        //
//        // Autolayout the lables
//        //
//        let views = [
//            "titleLabel" : titleLabel,
//            "arrowLabel" : arrowLabel,
//            ]
//        
//        contentView.addConstraints(NSLayoutConstraint.constraints(
//            withVisualFormat: "H:|-20-[titleLabel]-[arrowLabel]-20-|",
//            options: [],
//            metrics: nil,
//            views: views
//        ))
//        
//        contentView.addConstraints(NSLayoutConstraint.constraints(
//            withVisualFormat: "V:|-[titleLabel]-|",
//            options: [],
//            metrics: nil,
//            views: views
//        ))
//        
//        contentView.addConstraints(NSLayoutConstraint.constraints(
//            withVisualFormat: "V:|-[arrowLabel]-|",
//            options: [],
//            metrics: nil,
//            views: views
//        ))
//    }
//    
//    //
//    // Trigger toggle section when tapping on the header
//    //
//    func tapHeader(gestureRecognizer: UITapGestureRecognizer) {
//        guard let cell = gestureRecognizer.view as? CollapsibleTableViewHeader else {
//            return
//        }
//        
//        delegate?.toggleSection(header: self, section: cell.section)
//    }
//    
//    func setCollapsed(collapsed: Bool) {
//        //
//        // Animate the arrow rotation (see Extensions.swf)
//        //
//        arrowLabel.rotate(toValue: collapsed ? 0.0 : CGFloat(M_PI_2))
//    }
//    
//}
//
//extension UIColor {
//    
//    convenience init(hex:Int, alpha:CGFloat = 1.0) {
//        self.init(
//            red:   CGFloat((hex & 0xFF0000) >> 16) / 255.0,
//            green: CGFloat((hex & 0x00FF00) >> 8)  / 255.0,
//            blue:  CGFloat((hex & 0x0000FF) >> 0)  / 255.0,
//            alpha: alpha
//        )
//    }
//    
//}
//
//extension UIView {
//    
//    func rotate(toValue: CGFloat, duration: CFTimeInterval = 0.2) {
//        let animation = CABasicAnimation(keyPath: "transform.rotation")
//        
//        animation.toValue = toValue
//        animation.duration = duration
//        animation.isRemovedOnCompletion = false
//        animation.fillMode = kCAFillModeForwards
//        
//        self.layer.add(animation, forKey: nil)
//    }
//    
//}

