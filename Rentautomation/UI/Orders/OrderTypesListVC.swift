//
//  OrderTypesListVC.swift
//  Rentautomation
//
//  Created by kanybek on 12/23/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

class OrderTypesListVC: FormViewController {

    var initialString: String?
    
    convenience init() {
        self.init(initialString: nil)
    }
    
    init(initialString: String?) {
        self.initialString = initialString
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    deinit {
    }
    
    // MARK: Public
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "История"
        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
    }
    
    // MARK: Private
    private func configure() {
        
        let backBarButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backBarButton
        
        // Create RowFormers
        let createMenu: ((String, UIColor, (() -> Void)?) -> RowFormer) = { text, color, onSelected in
            return LabelRowFormer<FormLabelCell>() {
                $0.titleLabel.textColor = color
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                }.configure {
                    $0.text = text
                    $0.rowHeight = 70
                }.onSelected { _ in
                    onSelected?()
            }
        }
        
        let allDocumentTypesRow = createMenu("Все операции по кассе", .formerColor()) { [weak self] in
            let orderListVC = OrdersListVC(initialOrderDocumentType: .none)
            let productListNC = RNNavigationController(rootViewController:orderListVC)
            self?.splitViewController?.showDetailViewController(productListNC, sender: self)
        }
        
        let saleColor = Order.orderDocumentTypeColor(orderDocType: .saleToCustomer)
        let saleDocumentTypesTRow = createMenu("Продажи", saleColor) { [weak self] in
            let orderListVC = OrdersListVC(initialOrderDocumentType: .saleToCustomer)
            let productListNC = RNNavigationController(rootViewController:orderListVC)
            self?.splitViewController?.showDetailViewController(productListNC, sender: self)
        }
        
        let preOrderToCustomerColor = Order.orderDocumentTypeColor(orderDocType: .preOrderToCustomer)
        let customerPreOrderTypesRow = createMenu("Заказы", preOrderToCustomerColor) { [weak self] in
            let orderListVC = OrdersListVC(initialOrderDocumentType: .preOrderToCustomer)
            let productListNC = RNNavigationController(rootViewController:orderListVC)
            self?.splitViewController?.showDetailViewController(productListNC, sender: self)
        }
        
        let receiveColor = Order.orderDocumentTypeColor(orderDocType: .receiveFromSupplier)
        let receiveDocumentTypesRow = createMenu("Закупки", receiveColor) { [weak self] in
            let orderListVC = OrdersListVC(initialOrderDocumentType: .receiveFromSupplier);
            let productListNC = RNNavigationController(rootViewController:orderListVC)
            self?.splitViewController?.showDetailViewController(productListNC, sender: self)
        }
        
        let monReceiveColor = Order.orderDocumentTypeColor(orderDocType: .moneyReceive)
        let incomeMoneyDocumentTypesRow = createMenu("Приход денег", monReceiveColor) { [weak self] in
            let orderListVC = OrdersListVC(initialOrderDocumentType: .moneyReceive);
            let productListNC = RNNavigationController(rootViewController:orderListVC)
            self?.splitViewController?.showDetailViewController(productListNC, sender: self)
        }
        
        let monGoneColor = Order.orderDocumentTypeColor(orderDocType: .moneyGone)
        let outgoneMoneyDocumentTypesRow = createMenu("Расход денег", monGoneColor) { [weak self] in
            let orderListVC = OrdersListVC(initialOrderDocumentType: .moneyGone);
            let productListNC = RNNavigationController(rootViewController:orderListVC)
            self?.splitViewController?.showDetailViewController(productListNC, sender: self)
        }
        
        //let moneyIncomeAutgoneRow = createMenu("Приход/Расход денег за промежуток времени", .formerColor()) { [weak self] in
        //let totalMoneyVC = TotalMoneyOnDateVC(initialItem: "koke")
        //let totalMoneyNC = RNNavigationController(rootViewController:totalMoneyVC)
        //self?.splitViewController?.showDetailViewController(totalMoneyNC, sender: self)
        //}
        
        let detailSaleRow = createMenu("Подробная свертка продажи", saleColor) { [weak self] in
            let detailVC = DetailedOrderListTVC(initialOrderType: .saleToCustomer)
            let detailNC = RNNavigationController(rootViewController:detailVC)
            self?.splitViewController?.showDetailViewController(detailNC, sender: self)
        }
        
        let detailIncomeRow = createMenu("Подробная свертка закупки", receiveColor) { [weak self] in
            let detailVC = DetailedOrderListTVC(initialOrderType: .receiveFromSupplier)
            let detailNC = RNNavigationController(rootViewController:detailVC)
            self?.splitViewController?.showDetailViewController(detailNC, sender: self)
        }
        
        //let allMoneyTransactionsRow = createMenu("Платежи с клиентами и поставщикам", .formerColor()) { [weak self] in
        //    let transactionsListVC = TransactionsListVC(initialString: nil)
        //    let transactionsListNC = RNNavigationController(rootViewController:transactionsListVC)
        //    self?.splitViewController?.showDetailViewController(transactionsListNC, sender: self)
        //}
        
        // Create Headers and Footers
        let createHeader: ((String) -> ViewFormer) = { text in
            return LabelViewFormer<FormLabelHeaderView>()
                .configure {
                    $0.text = text
                    $0.viewHeight = 40
            }
        }
        
        let createFooter: ((String) -> ViewFormer) = { text in
            return LabelViewFormer<FormLabelFooterView>()
                .configure {
                    $0.text = text
                    $0.viewHeight = 30
            }
        }
        
        //
        let realExampleSection1 = SectionFormer(rowFormer: allDocumentTypesRow, saleDocumentTypesTRow, customerPreOrderTypesRow, receiveDocumentTypesRow, incomeMoneyDocumentTypesRow, outgoneMoneyDocumentTypesRow)
            .set(headerViewFormer: createHeader("История"))
        
        let realExampleSection2 = SectionFormer(rowFormer: detailSaleRow, detailIncomeRow)
            .set(headerViewFormer: createHeader("Детально"))
            .set(footerViewFormer: createFooter("История продаж, закупок и расход денег"))
        
        former.append(sectionFormer: realExampleSection1, realExampleSection2)
    }
}
