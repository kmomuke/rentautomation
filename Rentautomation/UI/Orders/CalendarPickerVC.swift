//
//  CalendarPickerVC.swift
//  Rentautomation
//
//  Created by kanybek on 5/1/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import FSCalendar
class CalendarPickerVC: UIViewController, FSCalendarDataSource, FSCalendarDelegate, FSCalendarDelegateAppearance {
    
    var calendar: FSCalendar
    var initialItem: String?
    
    convenience init() {
        self.init(initialItem: nil)
    }
    
    init(initialItem: String?) {
        self.initialItem = initialItem
        calendar = FSCalendar(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func loadView() {
        super.loadView()
        calendar.firstWeekday = 2
        calendar.delegate = self
        calendar.dataSource = self
        calendar.scrollDirection = .horizontal
        calendar.backgroundColor = UIColor.white
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let title_ = self.initialItem {
            self.title = title_
        }
        self.view.backgroundColor = UIColor.groupTableViewBackground
        self.view.addSubview(calendar)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        calendar.frame = self.view.bounds
        calendar.layoutSubviews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: FSCalendarDelegate
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if let title_ = self.initialItem {
            if title_ == "Дата начало" {
                OrderFilterMemory.shared.startDate = date.dateAtStartOfDay()
                self.navigationController?.popViewController(animated: true)
            } else if title_ == "Дата конец" {
                OrderFilterMemory.shared.endDate = date.dateAtEndOfDay()
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        print("calendarCurrentPageDidChange")
    }
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        print("boundingRectWillChange")
    }
    
    //MARK: FSCalendarDataSource
    func minimumDate(for calendar: FSCalendar) -> Date {
        if let lastOrder = Database.shared.lastOrder() {
            return lastOrder.orderDate
        } else {
            return Date()
        }
    }
    
    func maximumDate(for calendar: FSCalendar) -> Date {
        return Date()
    }
    
//    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
//        return UIColor.alizarinColor()
//    }
//    
//    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillSelectionColorFor date: Date) -> UIColor? {
//        return UIColor.midnightBlueColor()
//    }
}
