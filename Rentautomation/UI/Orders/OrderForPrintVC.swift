//
//  OrderForPrintVC.swift
//  Rentautomation
//
//  Created by kanybek on 4/25/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class OrderForPrintVC: BaseListVC, ErrorPopoverRenderer, UITableViewDelegate, UITableViewDataSource {
    
    var initialOrder: Order
    var tableView: UITableView
    var orderDetails: [OrderDetail] = []
    var totalSaledAmount: Double = 0.0
    
    convenience init() {
        self.init(initialOrder: Order())
    }
    
    init(initialOrder: Order) {
        self.initialOrder = initialOrder
        self.tableView = UITableView(frame: CGRect.zero, style: .plain)
        let orderDetails_ = Database.shared.orderDetailsBy(orderId: initialOrder.orderId)
        var totalSaledAmount: Double = 0.0
        for orderDetail in orderDetails_ {
            totalSaledAmount = totalSaledAmount + orderDetail.orderQuantity
        }
        self.totalSaledAmount = totalSaledAmount
        self.orderDetails = orderDetails_
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let generateButton = UIBarButtonItem(
            title: "Отправить",
            style: .plain,
            target: self,
            action: #selector(generatePressed(_:))
        )
        self.navigationItem.rightBarButtonItem = generateButton
        
        let nib1 = UINib(nibName: "MainFirstRowCell", bundle: nil)
        self.tableView.register(nib1, forCellReuseIdentifier: "MainFirstRowCell")
        
        let nib2 = UINib(nibName: "CustomerSupplierRowCell", bundle: nil)
        self.tableView.register(nib2, forCellReuseIdentifier: "CustomerSupplierRowCell")
        
        let nib3 = UINib(nibName: "OrdersHeaderRowCell", bundle: nil)
        self.tableView.register(nib3, forCellReuseIdentifier: "OrdersHeaderRowCell")
        
        let nib4 = UINib(nibName: "OrdersRowCell", bundle: nil)
        self.tableView.register(nib4, forCellReuseIdentifier: "OrdersRowCell")
        
        let nib5 = UINib(nibName: "TotalAmountOrderCell", bundle: nil)
        self.tableView.register(nib5, forCellReuseIdentifier: "TotalAmountOrderCell")
        
        let nib6 = UINib(nibName: "MoneyWordsRowCell", bundle: nil)
        self.tableView.register(nib6, forCellReuseIdentifier: "MoneyWordsRowCell")
        
        let nib7 = UINib(nibName: "SignaturesRowCell", bundle: nil)
        self.tableView.register(nib7, forCellReuseIdentifier: "SignaturesRowCell")
        
        //MainFirstRowCell
        //CustomerSupplierRowCell
        //OrdersHeaderRowCell
        
        //OrdersRowCell
        
        //TotalAmountOrderCell
        //MoneyWordsRowCell
        //SignaturesRowCell
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.separatorColor = UIColor.clear
        
        self.view.addSubview(self.tableView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = self.view.bounds
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.generatePressed(self.navigationItem.rightBarButtonItem!)
    }
    
    func generatePressed(_ sender: UIBarButtonItem) {
        
        self.showLoading("Накладная ...")
        let delay = DispatchTime.now() + DispatchTimeInterval.milliseconds(300)
        DispatchQueue.main.asyncAfter(deadline: delay, execute: {
            if let image = self.tableView.screenshot {
                self.dismissLoading()
                
                let activityViewController = UIActivityViewController(activityItems: [image] , applicationActivities: nil)
                if activityViewController.responds(to: #selector(getter: UIViewController.popoverPresentationController)) {
                    activityViewController.popoverPresentationController?.barButtonItem = sender
                }
                self.present(activityViewController, animated: true, completion: {
                })
            }
        })
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                return 55.0
            case 1,2:
                return 50.0
            case 3:
                return 50.0
            default:
                return 0
            }
        case 1:
            return 50.0
        case 2:
            switch indexPath.row {
            case 0,1,2,3:
                return 30.0
            case 4:
                return 70.0
            case 5:
                return 60.0
            default:
                return 0
            }
        default:
            return 0
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 4
        case 1:
            return self.orderDetails.count
        case 2:
            return 6
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell!
        switch indexPath.section {
        case 0:
            
            switch indexPath.row {
            case 0:
                cell = tableView.dequeueReusableCell(withIdentifier: "MainFirstRowCell", for: indexPath) as! MainFirstRowCell
                if let cell_ = cell as? MainFirstRowCell {
                    let str = "Расходная накладная №\(self.initialOrder.orderId) от \(self.initialOrder.orderDate.toString(.custom("EEEE, dd MMMM yyyy HH:mm")))"
                    cell_.fullTitleLabel.text = str
                }
            case 1,2:
                cell = tableView.dequeueReusableCell(withIdentifier: "CustomerSupplierRowCell", for: indexPath) as! CustomerSupplierRowCell
                if let cell_ = cell as? CustomerSupplierRowCell {
                    if indexPath.row == 1 {
                        cell_.supplierLabel.text = "Поставщик"
                        cell_.supplierContentLabel.text = AppSettings.shared.getCompanyName()
                    } else {
                        
                        var customerOrSupplierName: String = ""
                        if self.initialOrder.customerId > 0 {
                            if let customer = Database.shared.customerBy(customerId: self.initialOrder.customerId) {
                                customerOrSupplierName = "\(customer.firstName) \(customer.secondName)"
                            }
                        } else if self.initialOrder.supplierId > 0 {
                            if let supplier = Database.shared.supplierBy(supplierId: self.initialOrder.supplierId) {
                                customerOrSupplierName = "\(supplier.companyName)"
                            }
                        }
                        
                        cell_.supplierLabel.text = "Покупатель"
                        cell_.supplierContentLabel.text = customerOrSupplierName
                    }
                }
            case 3:
                cell = tableView.dequeueReusableCell(withIdentifier: "OrdersHeaderRowCell", for: indexPath) as! OrdersHeaderRowCell
            default:
                cell = nil
            }
            
        case 1:
            let orderDetail = self.orderDetails[indexPath.row]
            cell = tableView.dequeueReusableCell(withIdentifier: "OrdersRowCell", for: indexPath) as! OrdersRowCell
            if let cell_ = cell as? OrdersRowCell {
                cell_.orderDetail = orderDetail
                cell_.reloadData(row: indexPath.row + 1)
            }
            if indexPath.row == (self.orderDetails.count - 1) {
            }
            
        case 2:
            switch indexPath.row {
            case 0,1,2,3:
                cell = tableView.dequeueReusableCell(withIdentifier: "TotalAmountOrderCell", for: indexPath) as! TotalAmountOrderCell
                if let cell_ = cell as? TotalAmountOrderCell {
                    
                    var discount: Double = 0.0
                    var totalSum: Double = 0.0
                    var totalSumWithDiscount: Double = 0.0
                    if let payment = Database.shared.paymentBy(paymentId: self.initialOrder.paymentId) {
                        discount = payment.discount
                        totalSum = payment.totalOrderPrice
                        totalSumWithDiscount = payment.totalPriceWithDiscountF()
                    }
                    
                    let d = "\(discount)%"
                    let dd = Global.shared.priceFormatter.string(for: totalSum)
                    let ddd = Global.shared.priceFormatter.string(for: totalSumWithDiscount)
                    
                    if indexPath.row == 0 {
                        cell_.totalStringLabel.text = "Общее количество"
                        cell_.totalAmountLabel.text = Global.shared.amountFormatter.string(for: self.totalSaledAmount)
                    } else if indexPath.row == 1 {
                        cell_.totalStringLabel.text = "Итого"
                        cell_.totalAmountLabel.text = ddd
                    } else if indexPath.row == 2 {
                        cell_.totalStringLabel.text = "в том числе НДС"
                        cell_.totalAmountLabel.text = "________ сом"
                    } else if indexPath.row == 3 {
                        cell_.totalStringLabel.text = "в том числе НФП"
                        cell_.totalAmountLabel.text = "________ сом"
                    }
                }
                
            case 4:
                cell = tableView.dequeueReusableCell(withIdentifier: "MoneyWordsRowCell", for: indexPath) as! MoneyWordsRowCell
                if let cell_ = cell as? MoneyWordsRowCell {
                    
                    var totalSumWithDiscount: Double = 0.0
                    if let payment = Database.shared.paymentBy(paymentId: self.initialOrder.paymentId) {
                        totalSumWithDiscount = payment.totalPriceWithDiscountF()
                    }
                    let ddd = Global.shared.priceFormatter.string(for: totalSumWithDiscount)!
                    
                    cell_.totalLabel.text = "Всего наименовании \(self.orderDetails.count) на сумму \(String(describing: ddd))"
                    cell_.moneyStringLabel.text = ""
                }
            case 5:
                cell = tableView.dequeueReusableCell(withIdentifier: "SignaturesRowCell", for: indexPath) as! SignaturesRowCell
            default:
                cell = nil
            }
        default:
            cell = nil
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
