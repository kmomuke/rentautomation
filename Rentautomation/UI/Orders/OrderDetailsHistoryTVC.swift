//
//  OrderDetailsHistoryTVC.swift
//  Rentautomation
//
//  Created by kanybek on 1/6/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class OrderDetailsHistoryTVC: UITableViewController {
    
    var initialOrder: Order?
    var selectedProducts: [OrderDetail] = []
    
    var totalAmountBlock : ((Double) -> Void)?
    
    convenience init() {
        self.init(initialOrder: nil)
    }
    
    init(initialOrder: Order?) {
        self.initialOrder = initialOrder
        super.init(nibName: "OrderDetailsHistoryTVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "OrderDetailsHistoryTVCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "OrderDetailsHistoryTVCell")
        self.tableView.alwaysBounceVertical = true
        
        if let initialOrder_ = self.initialOrder {
            Database.shared.orderDetailsBy(orderId: initialOrder_.orderId, completion: {[weak self] (oD: [OrderDetail]) in
                self?.selectedProducts = oD
                self?.tableView.reloadData()
                
                var totalSaledAmount: Double = 0.0
                for orderDetail in oD {
                    totalSaledAmount = totalSaledAmount + orderDetail.orderQuantity
                }
                
                if let block = self?.totalAmountBlock {
                    block(totalSaledAmount)
                }
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedProducts.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailsHistoryTVCell", for: indexPath) as! OrderDetailsHistoryTVCell
        let orderDetail = selectedProducts[(indexPath as NSIndexPath).row]
        cell.orderDetail = orderDetail
        cell.numberLabel.text = "\(indexPath.row + 1). "
        cell.reloadData()
        return cell
    }
}
