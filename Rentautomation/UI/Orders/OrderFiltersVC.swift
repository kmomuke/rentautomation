//
//  OrderFiltersVC.swift
//  Rentautomation
//
//  Created by kanybek on 12/22/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

struct FilterOptions : OptionSet {
    let rawValue: Int
    static let isAgentOption  = FilterOptions(rawValue: 1 << 0)
    static let documentOption = FilterOptions(rawValue: 1 << 1)
    static let customerOption = FilterOptions(rawValue: 1 << 2)
    static let supplierOption = FilterOptions(rawValue: 1 << 3)
    static let staffOption    = FilterOptions(rawValue: 1 << 4)
}

class OrderFiltersVC: FormViewController, ErrorPopoverRenderer {
    
    var initialItem: String?
    var applyFiltersBlock : ((Date, Date, UInt64, UInt64, UInt64, OrderDocumentType, FilterOptions) -> Void)?
    var isDatePickersShort: Bool
    
    private enum InsertPosition: Int {
        case Below, Above
    }
    private var insertRowPosition: InsertPosition = .Below
    
    convenience init() {
        self.init(initialItem: nil)
    }
    
    init(initialItem: String?) {
        self.initialItem = initialItem
        self.isDatePickersShort = true
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    // MARK: Public
    override func viewDidLoad() {
        
        OrderFilterMemory.shared.clearAllVariables()
        
        super.viewDidLoad()
        let canceButton = UIBarButtonItem(
            title: "Отмена",
            style: .plain,
            target: self,
            action: #selector(cancelObject(_:))
        )
        self.view.backgroundColor = UIColor.white
        self.navigationItem.leftBarButtonItem = canceButton
        
        let createButton = UIBarButtonItem(
            title: "Применить",
            style: .plain,
            target: self,
            action: #selector(applyFilters(_:))
        )
        
        self.navigationItem.rightBarButtonItem = createButton
        configure()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
        for sections in self.former.sectionFormers {
            for row in sections.rowFormers {
                row.update()
            }
        }
    }
    
    func applyFilters(_ sender: AnyObject) -> Void {
        
        if let _ = OrderFilterMemory.shared.startDate {
            
        } else {
            let _ = self.showErrorAlert(text: "Выберите дата начало")
            return
        }
        
        if let _ = OrderFilterMemory.shared.endDate {
            
        } else {
            let _ = self.showErrorAlert(text: "Выберите дата конец")
            return
        }
        
        //if (OrderFilterMemory.shared.customerId == 0) &&
        //    (OrderFilterMemory.shared.supplierId == 0) {
        //    let _ = self.showErrorAlert(text: "Выберите контрагента")
        //    return
        //}
        
        //let multipleOptions: FilterOptions = [.firstOption, .secondOption, .sixsOption]
        //if multipleOptions.contains([.secondOption, .sixsOption, .fifthOption]) {
        //    print("multipleOptions has SecondOption and sixsOption")
        //}
        
        var multipleFilterOptions: FilterOptions = []
        
        if let startDate_ = OrderFilterMemory.shared.startDate,
            let endDate_ = OrderFilterMemory.shared.endDate {
            
            let customerId = OrderFilterMemory.shared.customerId
            if customerId > 0 {
                multipleFilterOptions.insert(.customerOption)
            }
            
            let supplierId = OrderFilterMemory.shared.supplierId
            if supplierId > 0 {
                multipleFilterOptions.insert(.supplierOption)
            }
            
            let staffId = OrderFilterMemory.shared.staffId
            if staffId > 0 {
                multipleFilterOptions.insert(.staffOption)
            }
            
            let documentType = OrderFilterMemory.shared.orderDocument
            if documentType != .none {
                multipleFilterOptions.insert(.documentOption)
            }
            
            if AppSession.shared.isAgent() {
                multipleFilterOptions.insert(.isAgentOption)
            }
            
            if let applyFiltersBlock = self.applyFiltersBlock {
                applyFiltersBlock(startDate_, endDate_, customerId, supplierId, staffId, documentType, multipleFilterOptions)
            }
            
            self.navigationController?.dismiss(animated: true, completion: {[weak self] in
                print("dismiss OrderFiltersVC")
            })
        }
    }
    
    func cancelObject(_ sender: AnyObject) -> Void {
        self.navigationController?.dismiss(animated: true, completion: {[weak self] in
            print("dismiss OrderFiltersVC")
        })
    }
    
    private func pushSelectorRowSelected(options: [String]) -> (RowFormer) -> Void {
        return { [weak self] rowFormer in
            if let rowFormer = rowFormer as? LabelRowFormer<FormLabelCell> {
                let controller = TextSelectorViewContoller()
                controller.texts = options
                controller.selectedText = rowFormer.subText
                controller.onSelected = {
                    rowFormer.subText = $0
                    rowFormer.update()
                    
                    switch $0 {
                    case "Все операции по кассе":
                        OrderFilterMemory.shared.orderDocument = .none
                    case "Продажи":
                        OrderFilterMemory.shared.orderDocument = .saleToCustomer
                    case "Закупки":
                        OrderFilterMemory.shared.orderDocument = .receiveFromSupplier
                    case "Приход денег":
                        OrderFilterMemory.shared.orderDocument = .moneyReceive
                    case "Расход денег":
                        OrderFilterMemory.shared.orderDocument = .moneyGone
                    case "Выход готовой продукции":
                        OrderFilterMemory.shared.orderDocument = .readyProductReceivedFromProd
                    case "Расход сырья":
                        OrderFilterMemory.shared.orderDocument = .rawProductGoneToProd
                    case "Возвраты от клиента":
                        OrderFilterMemory.shared.orderDocument = .returnFromCustomer
                    case "Возвраты к поставщику":
                        OrderFilterMemory.shared.orderDocument = .returnToSupplier
                    case "Заказы для клиента":
                        OrderFilterMemory.shared.orderDocument = .preOrderToCustomer
                    case "Инвентаризация / Черновик":
                        OrderFilterMemory.shared.orderDocument = .stockTaking
                    default:
                        print("default")
                    }
                }
                self?.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    private func configure() {
        
        title = "Фильтры"
        tableView.contentInset.top = 10
        tableView.contentInset.bottom = 30
        tableView.contentOffset.y = -10
        
    
        OrderFilterMemory.shared.startDate = Date().dateAtStartOfDay()
        OrderFilterMemory.shared.endDate = Date().dateAtEndOfDay()
        
        // Create Headers
        let createHeader: (() -> ViewFormer) = {
            return CustomViewFormer<FormHeaderFooterView>()
                .configure {
                    $0.viewHeight = 20
                    $0.view.backgroundColor = UIColor.white
            }
        }
        
        let calendarButtonRow: ((Int, String, (() -> Void)?) -> RowFormer) = { [weak self] index, text, onSelected in
            return LabelRowFormer<FormLabelCell>() {
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = .boldSystemFont(ofSize: 16)
                $0.subTextLabel.textColor = .formerSubColor()
                $0.subTextLabel.font = .boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                
                }.configure {
                    $0.text = text
                    $0.rowHeight = 60
                }.onSelected { [weak self] _ in
                    onSelected?()
                    self?.former.deselect(animated: true)
                }.onUpdate { [weak self] in
                    
                    if index == 1 {
                        if let startDate = OrderFilterMemory.shared.startDate {
                            $0.subText = "\(startDate.toString(.custom("EE, dd MMM HH:mm")))"
                        }
                    } else if index == 2 {
                        if let endDate = OrderFilterMemory.shared.endDate {
                            $0.subText = "\(endDate.toString(.custom("EE, dd MMM HH:mm")))"
                        }
                    }
            }
        }
        
        let customerButtonRow1: ((String, (() -> Void)?) -> RowFormer) = { [weak self] text, onSelected in
            return LabelRowFormer<FormLabelCell>() {
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = .boldSystemFont(ofSize: 16)
                $0.subTextLabel.textColor = .formerSubColor()
                $0.subTextLabel.font = .boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                
                }.configure {
                    $0.text = text
                    $0.rowHeight = 60
                }.onSelected { [weak self] _ in
                    onSelected?()
                    self?.former.deselect(animated: true)
                }.onUpdate { [weak self] in
                    var customerName: String = ""
                    if (OrderFilterMemory.shared.customerId > 0) {
                        
                        if let customer = Database.shared.customerBy(customerId: OrderFilterMemory.shared.customerId) {
                            customerName = customer.firstName + " " + customer.secondName
                        }
                    }
                    $0.subText = customerName
            }
        }
        
        let supplierButtonRow2: ((String, (() -> Void)?) -> RowFormer) = { [weak self] text, onSelected in
            return LabelRowFormer<FormLabelCell>() {
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = .boldSystemFont(ofSize: 16)
                $0.subTextLabel.textColor = .formerSubColor()
                $0.subTextLabel.font = .boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                
                }.configure {
                    $0.text = text
                    $0.rowHeight = 60
                }.onSelected { [weak self] _ in
                    onSelected?()
                    self?.former.deselect(animated: true)
                }.onUpdate { [weak self] in
                    var supplierName: String = ""
                    if (OrderFilterMemory.shared.supplierId > 0) {
                        if let supplier =  Database.shared.supplierBy(supplierId: OrderFilterMemory.shared.supplierId) {
                            supplierName = supplier.companyName
                        }
                    }
                    $0.subText = supplierName
            }
        }
        
        let createStaffRow1: ((String, (() -> Void)?) -> RowFormer) = { [weak self] text, onSelected in
            return LabelRowFormer<FormLabelCell>() {
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = .boldSystemFont(ofSize: 16)
                $0.subTextLabel.textColor = .formerSubColor()
                $0.subTextLabel.font = .boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                
                }.configure {
                    $0.text = text
                    $0.rowHeight = 60
                }.onSelected { [weak self] _ in
                    onSelected?()
                    self?.former.deselect(animated: true)
                }.onUpdate { [weak self] in
                    var emplyeeName: String = ""
                    if (OrderFilterMemory.shared.staffId > 0) {
                        if let emplyee = Database.shared.userBy(userId: OrderFilterMemory.shared.staffId) {
                            emplyeeName = emplyee.firstName + " " + emplyee.secondName
                        }
                    }
                    if (emplyeeName.characters.count > 0) {
                        $0.subText = "\(emplyeeName)"
                    }
            }
        }
        
        let calendarStartRow = calendarButtonRow(1, "Дата начало") { [weak self] in
            let calendarVC = CalendarPickerVC(initialItem: "Дата начало")
            self?.navigationController?.pushViewController(calendarVC, animated: true)
        }
        
        let calendarEndRow = calendarButtonRow(2, "Дата конец") { [weak self] in
            let calendarVC = CalendarPickerVC(initialItem: "Дата конец")
            self?.navigationController?.pushViewController(calendarVC, animated: true)
        }
        
        let customerRow = customerButtonRow1("Клиент") { [weak self] in
            let customerVC = CustomersListVC(initialString: nil, isComingFromOrder: true)
            self?.navigationController?.pushViewController(customerVC, animated: true)
        }
        
        let supplierRow = supplierButtonRow2("Поставщик") { [weak self] in
            let supplierVC = SuppliersListVC(initialString: nil, isCommingFromProduct: true, isComingFromOrder: false)
            self?.navigationController?.pushViewController(supplierVC, animated: true)
        }
        
        let selectStaffRow = createStaffRow1("Сотрудник") { [weak self] in
            let staffList: StaffListVC = StaffListVC(initialString: nil, isCommingFromCustomer: true)
            self?.navigationController?.pushViewController(staffList, animated: true)
        }
        
        let createSelectorRow = { (
            text: String,
            subText: String,
            onSelected: ((RowFormer) -> Void)?
            ) -> RowFormer in
            return LabelRowFormer<FormLabelCell>() {
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = .boldSystemFont(ofSize: 16)
                $0.subTextLabel.textColor = .formerSubColor()
                $0.subTextLabel.font = .boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                }.configure { form in
                    _ = onSelected.map { form.onSelected($0) }
                    form.text = text
                    form.subText = subText
                    form.rowHeight = 60
            }
        }
        
        let options = ["Все операции по кассе",
                       "Продажи",
                       "Закупки",
                       "Приход денег",
                       "Расход денег",
                       "Выход готовой продукции",
                       "Расход сырья",
                       "Возвраты от клиента",
                       "Возвраты к поставщику",
                       "Заказы для клиента",
                       "Инвентаризация / Черновик"]
        let currencyRow = createSelectorRow("Тип операции", options[0], pushSelectorRowSelected(options: options))
        
        let dateSection = SectionFormer(rowFormer: calendarStartRow, calendarEndRow, customerRow, supplierRow, selectStaffRow, currencyRow)
            .set(headerViewFormer: createHeader())
        
        former.append(sectionFormer: dateSection)
    }
}

