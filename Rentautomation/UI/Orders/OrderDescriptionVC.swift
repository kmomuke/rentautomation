//
//  OrderDescriptionVC.swift
//  Rentautomation
//
//  Created by kanybek on 12/9/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

class OrderDescriptionVC: BaseListVC, ErrorPopoverRenderer {

    var initialOrder: Order?
    var isFirstTime: Bool = true
    var selectTVC: OrderDetailsHistoryTVC?
    
    var performReloadBlock : ((Void) -> Void)?
    
    @IBOutlet weak var orderEdittedStatusLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var staffLabel: UILabel!
    @IBOutlet weak var customerSupplierLabel: UILabel!
    @IBOutlet weak var documentTypeLabel: UILabel!
    @IBOutlet weak var moneyMovementTypeLabel: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var totalSumLabel: UILabel!
    @IBOutlet weak var totalSumWithDiscountLabel: UILabel!
    @IBOutlet weak var totalAmount: UILabel!
    @IBOutlet weak var orderDetailsView: UIView!
    
    convenience init() {
        self.init(initialOrderId: 0)
    }
    
    init(initialOrderId: UInt64) {
        self.initialOrder = Database.shared.orderBy(orderId: initialOrderId)
        super.init(nibName: "OrderDescriptionVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("OrderDescriptionVC deallocated")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let order_ = self.initialOrder {
            
            if ((order_.orderDocument == .moneyReceive) ||
                (order_.orderDocument == .moneyGone) ||
                (order_.orderDocument == .preOrderToCustomer) ||
                (order_.orderDocument == .saleToCustomer) ||
                (order_.orderDocument == .receiveFromSupplier) ||
                (order_.orderDocument == .rawProductGoneToProd) ||
                (order_.orderDocument == .readyProductReceivedFromProd)) &&
                !order_.isEdited &&
                Database.shared.isModelSynchronized(primaryKey: order_.orderId) {
                
                let editButton = UIBarButtonItem(
                    title: "Редактировать",
                    style: .plain,
                    target: self,
                    action: #selector(editOrderDetails(_:))
                )
                
                let generateButton = UIBarButtonItem(
                    title: "Накладная",
                    style: .plain,
                    target: self,
                    action: #selector(generatePressed(_:))
                )
                self.navigationItem.rightBarButtonItems = [editButton, generateButton]
            } else {
                let generateButton = UIBarButtonItem(
                    title: "Накладная",
                    style: .plain,
                    target: self,
                    action: #selector(generatePressed(_:))
                )
                self.navigationItem.rightBarButtonItem = generateButton
            }
        }
        
        self.updateUI()
        self.selectTVC = OrderDetailsHistoryTVC(initialOrder: self.initialOrder)
        self.selectTVC?.totalAmountBlock = {[weak self] (totalAmount: Double) -> Void in
            self?.totalAmount.text = Global.shared.amountFormatter.string(for: totalAmount)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if (isFirstTime) {
            self.add(asChildViewController: self.selectTVC!, frame_: self.orderDetailsView.frame)
            isFirstTime = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func updateUI() -> Void {    
        if let order_ = self.initialOrder {
            
            self.title = order_.billingNo
            
            if order_.isEdited {
                if ((order_.orderDocument == .preOrderToCustomer)
                    || (order_.orderDocument == .preOrderToCustomerEdited)) {
                    self.orderEdittedStatusLabel.text = "Документ был оформлен/редактирован"
                    self.orderEdittedStatusLabel.textColor = UIColor.pomergranateColor()
                } else {
                    self.orderEdittedStatusLabel.text = "Документ был откатан назад"
                }
            } else {
                self.orderEdittedStatusLabel.isHidden = true
            }
            
            self.numberLabel.text = "\(order_.orderId)"
            self.dateLabel.text = "\(order_.orderDate.toString(.custom("EEE, dd MMM HH:mm")))"
            
            var staffName: String = ""
            if order_.userId > 0 {
                if let employee = Database.shared.userBy(userId: order_.userId) {
                    staffName = "\(employee.firstName) \(employee.secondName)"
                }
            }
            self.staffLabel.text = staffName
            
            var customerOrSupplierName: String = ""
            if order_.customerId > 0 {
                if let customer = Database.shared.customerBy(customerId: order_.customerId) {
                    customerOrSupplierName = "\(customer.firstName) \(customer.secondName)"
                }
            } else if order_.supplierId > 0 {
                if let supplier = Database.shared.supplierBy(supplierId: order_.supplierId) {
                    customerOrSupplierName = "\(supplier.companyName)"
                }
            }
            self.customerSupplierLabel.text = customerOrSupplierName
            
            self.documentTypeLabel.text = Order.orderDocumentTypeStringName(orderDocType: order_.orderDocument)
            self.documentTypeLabel.textColor = Order.orderDocumentTypeColor(orderDocType: order_.orderDocument)
            
            self.moneyMovementTypeLabel.text = Order.moneyMovementTypeStringName(moneyType: order_.moneyMovementType)
            
            var discount = Discount()
            if let payment = Database.shared.paymentBy(paymentId: order_.paymentId) {
                discount = Discount(payment: payment)
            }
            
            let diffValue = discount.finlaDifferenceValue()
            let finalValue = Global.shared.amountFormatter.string(for: (0 - diffValue))!
            self.discountLabel.text = finalValue
            
            self.totalSumLabel.text = Global.shared.priceFormatter.string(for: discount.totalPrice)
            self.totalSumWithDiscountLabel.text = Global.shared.priceFormatter.string(for: discount.totalPriceWithDiscount())
        }
    }
    
    // MARK: - Helper Methods
    func editOrderDetails(_ sender: AnyObject) -> Void {
        
        if let order_ = self.initialOrder {
            
            if order_.orderDocument == .saleToCustomer {
                let alert: UIAlertController = UIAlertController(title: "Редактировать",
                                                                 message: "",
                                                                 preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "Редактировать продажу", style: .default, handler: {[weak self]
                    (UIAlertAction) -> Void in
                    if let strongSelf = self, let initialOrder_ = self?.initialOrder {
                        let documentVC = MainDocumentVC(orderDocument: .saleToCustomerEdited, initalOrder: initialOrder_)
                        let navController = RNNavigationController(rootViewController: documentVC)
                        navController.modalPresentationStyle = .fullScreen
                        strongSelf.present(navController, animated: true, completion:{_ in
                        })
                    }
                }))
                alert.addAction(UIAlertAction(title: "Отменить продажу", style: .default, handler: {[weak self]
                    (UIAlertAction) -> Void in
                    if let _ = self, let initialOrder_ = self?.initialOrder, let employee = AppSession.shared.currentEmployee {
                        OrderEditManager.shared.ownerEditedProductsSaleToCustomer(initialSaleOrder: initialOrder_,
                                                                                  newOrderDetails: [],
                                                                                  newStaff: employee,
                                                                                  newCustomerId: initialOrder_.customerId,
                                                                                  newOrderDocumentType: .saleToCustomerEdited,
                                                                                  newTotalPrice: 0,
                                                                                  newDiscount: Discount())
                        self?.navigationController?.popViewController(animated: true)
                        if let performPopBlock_ = self?.performReloadBlock {
                            performPopBlock_()
                        }
                    }
                }))
                self.present(alert, animated: true, completion: nil)
            } else if order_.orderDocument == .receiveFromSupplier {
                let alert: UIAlertController = UIAlertController(title: "Редактировать",
                                                                 message: "",
                                                                 preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "Редактировать закупку", style: .default, handler: {[weak self]
                    (UIAlertAction) -> Void in
                    if let strongSelf = self, let initialOrder_ = self?.initialOrder {
                        let documentVC = MainDocumentVC(orderDocument: .receiveFromSupplierEdited, initalOrder: initialOrder_)
                        let navController = RNNavigationController(rootViewController: documentVC)
                        navController.modalPresentationStyle = .fullScreen
                        strongSelf.present(navController, animated: true, completion:{_ in})
                    }
                }))
                alert.addAction(UIAlertAction(title: "Отменить закупку", style: .default, handler: {[weak self]
                    (UIAlertAction) -> Void in
                    if let _ = self, let initialOrder_ = self?.initialOrder, let employee = AppSession.shared.currentEmployee {
                        OrderEditManager.shared.ownerEditedProductsReceiveFromSupplier(initialProductIncomeOrder: initialOrder_,
                                                                                       newOrderDetails: [],
                                                                                       newStaff: employee,
                                                                                       newSupplier: initialOrder_.supplierId,
                                                                                       newOrderDocumentType: .receiveFromSupplierEdited,
                                                                                       newTotalPrice: 0,
                                                                                       newDiscount: Discount())
                        self?.navigationController?.popViewController(animated: true)
                        if let performPopBlock_ = self?.performReloadBlock {
                            performPopBlock_()
                        }
                    }
                }))
                self.present(alert, animated: true, completion: nil)
            } else if order_.orderDocument == .preOrderToCustomer {
                let alert: UIAlertController = UIAlertController(title: "Редактировать",
                                                                 message: "",
                                                                 preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "Оформить продажу", style: .default, handler: {[weak self]
                    (UIAlertAction) -> Void in
                    let documentVC = MainDocumentVC(orderDocument: .saleToCustomer, initalOrder: order_)
                    let navController = RNNavigationController(rootViewController: documentVC)
                    navController.modalPresentationStyle = .fullScreen
                    self?.present(navController, animated: true, completion:{_ in
                    })
                }))
                alert.addAction(UIAlertAction(title: "Редактировать заказ", style: .default, handler: {[weak self]
                    (UIAlertAction) -> Void in
                    let documentVC = MainDocumentVC(orderDocument: .preOrderToCustomerEdited, initalOrder: order_)
                    let navController = RNNavigationController(rootViewController: documentVC)
                    navController.modalPresentationStyle = .fullScreen
                    self?.present(navController, animated: true, completion:{_ in
                    })
                }))
                alert.addAction(UIAlertAction(title: "Отменить заказ", style: .default, handler: {[weak self]
                    (UIAlertAction) -> Void in
                    OrderEditManager.shared.updateInitialOrder(initialCustomerPreOrder: order_)
                    self?.navigationController?.popViewController(animated: true)
                    if let performPopBlock_ = self?.performReloadBlock {
                        performPopBlock_()
                    }
                }))
                self.present(alert, animated: true, completion: nil)
            } else if order_.orderDocument == .moneyGone {
                let alert: UIAlertController = UIAlertController(title: "Откатить назад документ \"Расход денег\"? ",
                                                                 message: "",
                                                                 preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "Откатить документ", style: .default, handler: {[weak self]
                    (UIAlertAction) -> Void in
                    if let empl = AppSession.shared.currentEmployee {
                        //OrderEditManager.shared.updateInitialOrder(initialCustomerPreOrder: order_)
                        OrderEditManager.shared.ownerEdittedMoneyGone(initialMoneyGoneOrder: order_,
                                                                      staff: empl)
                        self?.navigationController?.popViewController(animated: true)
                        if let performPopBlock_ = self?.performReloadBlock {
                            performPopBlock_()
                        }
                    }
                }))
                self.present(alert, animated: true, completion: nil)
            } else if order_.orderDocument == .moneyReceive {
                let alert: UIAlertController = UIAlertController(title: "Откатить назад документ \"Приход денег\"? ",
                                                                 message: "",
                                                                 preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "Откатить документ", style: .default, handler: {[weak self]
                    (UIAlertAction) -> Void in
                    if let empl = AppSession.shared.currentEmployee {
                        //OrderEditManager.shared.updateInitialOrder(initialCustomerPreOrder: order_)
                        OrderEditManager.shared.ownerEdittedMoneyIncome(initialMoneyIncomeOrder: order_,
                                                                        staff: empl)
                        self?.navigationController?.popViewController(animated: true)
                        if let performPopBlock_ = self?.performReloadBlock {
                            performPopBlock_()
                        }
                    }
                }))
                self.present(alert, animated: true, completion: nil)
            } else if order_.orderDocument == .rawProductGoneToProd {
                let alert: UIAlertController = UIAlertController(title: "Редактировать",
                                                                 message: "",
                                                                 preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "Редактировать расход сыря", style: .default, handler: {[weak self]
                    (UIAlertAction) -> Void in
                    let documentVC = MainDocumentVC(orderDocument: .rawProductGoneToProdEdited, initalOrder: order_)
                    let navController = RNNavigationController(rootViewController: documentVC)
                    navController.modalPresentationStyle = .fullScreen
                    self?.present(navController, animated: true, completion:{_ in
                        //if let performPopBlock_ = self?.performReloadBlock {
                        //    performPopBlock_()
                        //    self?.navigationController?.popViewController(animated: true)
                        //}
                    })
                }))
                alert.addAction(UIAlertAction(title: "Отменить расход сыря", style: .default, handler: {[weak self]
                    (UIAlertAction) -> Void in
                    if let empl = AppSession.shared.currentEmployee {
                        let _ = Creator.shared.rawProductsGoneToProductionEditted(shouldIncrease: false,
                                                                                  initialOrder: order_,
                                                                                  newOrderDetails: [],
                                                                                  newStaff: empl,
                                                                                  newCustomerId: 0,
                                                                                  newOrderDocType: .rawProductGoneToProdEdited,
                                                                                  newTotalPrice: 0,
                                                                                  newDiscount: Discount())
                    }
                    self?.navigationController?.popViewController(animated: true)
                    if let performPopBlock_ = self?.performReloadBlock {
                        performPopBlock_()
                    }
                }))
                self.present(alert, animated: true, completion: nil)
            } else if order_.orderDocument == .readyProductReceivedFromProd {
                let alert: UIAlertController = UIAlertController(title: "Редактировать",
                                                                 message: "",
                                                                 preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "Редактировать выход продукции", style: .default, handler: {[weak self]
                    (UIAlertAction) -> Void in
                    let documentVC = MainDocumentVC(orderDocument: .readyProductReceivedFromProdEdited, initalOrder: order_)
                    let navController = RNNavigationController(rootViewController: documentVC)
                    navController.modalPresentationStyle = .fullScreen
                    self?.present(navController, animated: true, completion:{_ in
                        //if let performPopBlock_ = self?.performReloadBlock {
                        //    performPopBlock_()
                        //    self?.navigationController?.popViewController(animated: true)
                        //}
                    })
                }))
                alert.addAction(UIAlertAction(title: "Отменить выход продукции", style: .default, handler: {[weak self]
                    (UIAlertAction) -> Void in
                    if let empl = AppSession.shared.currentEmployee {
                        let _ = Creator.shared.readyProductReceivedFromProductionEditted(shouldIncrease: false,
                                                                                         initialOrder: order_,
                                                                                         newOrderDetails: [],
                                                                                         newStaff: empl,
                                                                                         newCustomerId: 0,
                                                                                         newOrderDocType: .rawProductGoneToProdEdited,
                                                                                         newTotalPrice: 0,
                                                                                         newDiscount: Discount())
                    }
                    self?.navigationController?.popViewController(animated: true)
                    if let performPopBlock_ = self?.performReloadBlock {
                        performPopBlock_()
                    }
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func getTextFromOrderDidPressed(_ sender: UIButton) {
        //let vc = TextFromOrderVC(initialItem: self.initialOrder?.comment)
        let vc = DocumentCommentVC(initialOrder: self.initialOrder)
        self.presentSheet(viewController: vc)
    }
    
    func editMoneyGonePressed() -> Void {
        let moneyAmount = MoneyAmountVC(orderDocument: .saleToCustomer)
        moneyAmount.performSaveBlock = {[weak self] (Void) -> Void in
        }
        moneyAmount.dismissBlock = {[weak self] (staffEnteredMoney: Double, isForDebt: Bool) -> Void in
            if staffEnteredMoney > 0.01 {
            }
        }
        self.presentSheet(viewController: moneyAmount)
    }
    
    
    func generatePressed(_ sender: UIBarButtonItem) {
        
        if let order = self.initialOrder {
            let alert: UIAlertController = UIAlertController(title: "Накладная",
                                                             message: "",
                                                             preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
            
            if order.orderDocument == .stockTaking {
                alert.addAction(UIAlertAction(title: "Оформить продажу", style: .default, handler: {[weak self]
                    (UIAlertAction) -> Void in
                    if let strongSelf = self, let initialOrder_ = self?.initialOrder {
                        let documentVC = MainDocumentVC(orderDocument: .saleToCustomer, initalOrder: initialOrder_)
                        let navController = RNNavigationController(rootViewController: documentVC)
                        navController.modalPresentationStyle = .fullScreen
                        strongSelf.present(navController, animated: true, completion:{_ in
                        })
                    }
                }))
                alert.addAction(UIAlertAction(title: "Оформить закупку", style: .destructive, handler: {[weak self]
                    (UIAlertAction) -> Void in
                    if let strongSelf = self, let initialOrder_ = self?.initialOrder {
                        let documentVC = MainDocumentVC(orderDocument: .receiveFromSupplier, initalOrder: initialOrder_)
                        let navController = RNNavigationController(rootViewController: documentVC)
                        navController.modalPresentationStyle = .fullScreen
                        strongSelf.present(navController, animated: true, completion:{_ in
                        })
                    }
                }))
            }
            
            alert.addAction(UIAlertAction(title: "Напечатать чек", style: .default, handler: {[weak self]
                (UIAlertAction) -> Void in
                if let stringSelf = self, let initialOrder_ = self?.initialOrder {
                    if initialOrder_.orderDocument == .saleToCustomer {
                        ReceipPrinter.shared.printReceipt__(order: initialOrder_)
                    }
                }
            }))
            
            alert.addAction(UIAlertAction(title: "Накладная", style: .default, handler: {[weak self]
                (UIAlertAction) -> Void in
                if let stringSelf = self, let initialOrder_ = self?.initialOrder {
                    let forPrintVC = OrderForPrintVC(initialOrder: initialOrder_)
                    stringSelf.navigationController?.pushViewController(forPrintVC, animated: true)
                }
            }))
            
            alert.addAction(UIAlertAction(title: "Excel CSV", style: .default, handler: {[weak self]
                (UIAlertAction) -> Void in
                if let stringSelf = self {
                    
                    stringSelf.showLoading("Excel CSV...")
                    CSVGenerator.shared.generateCSVFrom(orders: [order],
                                                        progress: {[weak self] (fraction: Float) in
                                                            if let strongSelf = self {
                                                                strongSelf.showProgress(progress: fraction, text: "Генерация CSV")
                                                            }
                                                            
                        }, completion: {[weak self] (url: URL) in
                            
                            if let strongSelf = self {
                                strongSelf.dismissLoading()
                                
                                let activityViewController = UIActivityViewController(activityItems: [url] , applicationActivities: nil)
                                if activityViewController.responds(to: #selector(getter: UIViewController.popoverPresentationController)) {
                                    activityViewController.popoverPresentationController?.barButtonItem = sender
                                }
                                
                                strongSelf.present(activityViewController, animated: true, completion: {
                                })
                            }
                    })
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
}
