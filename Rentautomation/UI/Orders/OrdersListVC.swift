//
//  OrdersListVC.swift
//  Rentautomation
//
//  Created by kanybek on 11/29/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit
import CSNotificationView
import SwiftSignalKit

class OrdersListVC:  BaseListVC, ErrorPopoverRenderer, UITableViewDelegate, UITableViewDataSource {

    var tableView: UITableView
    var orders: [Order] = []
    var disposeSet: DisposableSet?
    
    var initialOrderDocumentType: OrderDocumentType
    
    var enableAboveHistoryRequests: Bool
    var enableBelowHistoryRequests: Bool
    
    var isFiltersApplied: Bool
    
    var notificationView: CSNotificationView?
    
    convenience init() {
        self.init(initialOrderDocumentType: .none)
    }
    
    init(initialOrderDocumentType: OrderDocumentType) {
        
        self.initialOrderDocumentType = initialOrderDocumentType
        self.enableAboveHistoryRequests = true
        self.enableBelowHistoryRequests = true
        self.isFiltersApplied = false
        self.disposeSet = DisposableSet()
        self.tableView = UITableView(frame: CGRect.zero, style: .plain)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.disposeSet?.dispose()
        self.disposeSet = nil
    }
    
    override func loadView() {
        super.loadView()
        self.addCanceButton()
        
        if (self.initialOrderDocumentType == .none) {
            self.title = "Все операции"
        } else {
            self.title = Order.orderDocumentTypeStringName(orderDocType: self.initialOrderDocumentType)
        }
        
        
        let nib = UINib(nibName: "OrderListTVCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "OrderListTVCell")
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.view.addSubview(self.tableView)
        
        let filterButton = UIBarButtonItem(
            title: "Фильтры",
            style: .plain,
            target: self,
            action: #selector(filterPressed(_:))
        )
        
        let generateButton = UIBarButtonItem(
            title: "CSV",
            style: .plain,
            target: self,
            action: #selector(generatePressed(_:))
        )
        
        self.navigationItem.rightBarButtonItems = [filterButton, generateButton]
        
        let allSyncFinishedSignal: Signal = PipesStore.shared.allSyncOperationsFinishedSignal()
        let allSyncFinishedDispose = allSyncFinishedSignal.start(next: {[weak self] (finished: Bool) in
            if let strongSelf = self {
                if !strongSelf.isFiltersApplied {
                    strongSelf.reloadAllOrders()
                }
            }
        })
        self.disposeSet?.add(allSyncFinishedDispose)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.reloadAllOrders()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let lastOrderId = Database.shared.lastSyncedOrder()
        print("lastOrderId====> \(lastOrderId)")
        
        let lastOrderDetailId = Database.shared.lastSyncedOrderDetail()
        print("lastOrderDetailId====> \(lastOrderDetailId)")
        
        let transactionId = Database.shared.lastSyncedTransaction()
        print("transactionId====> \(transactionId)")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = self.view.bounds
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func reloadAllOrders() {
        self.tableView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
        SmartLoader.shared.loadOrders(orderDocumentType: self.initialOrderDocumentType,
                                      date: Date(),
                                      limit: 50) { (orders_: [Order]) in
                                        self.orders = orders_
                                        self.tableView.reloadData()
        }
    }
    
    
    func generatePressed(_ sender: UIBarButtonItem) {
        let alert: UIAlertController = UIAlertController(title: "Сгенерировать CSV",
                                                         message: "Вы действительно хотите сгенерировать CSV?",
                                                         preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Сгенерировать", style: .default, handler: {[weak self]
            (UIAlertAction) -> Void in
            if let stringSelf = self {
                
                stringSelf.showLoading("Генерация CSV...")
                CSVGenerator.shared.generateCSVFrom(orders: stringSelf.orders,
                                                    progress: {[weak self] (fraction: Float) in
                                                        if let strongSelf = self {
                                                            strongSelf.showProgress(progress: fraction, text: "Генерация CSV")
                                                        }
                                                        
                }, completion: {[weak self] (url: URL) in
                    
                    if let strongSelf = self {
                        strongSelf.dismissLoading()
                        
                        let activityViewController = UIActivityViewController(activityItems: [url] , applicationActivities: nil)
                        if activityViewController.responds(to: #selector(getter: UIViewController.popoverPresentationController)) {
                            activityViewController.popoverPresentationController?.barButtonItem = sender
                        }
                        
                        strongSelf.present(activityViewController, animated: true, completion: {
                        })
                    }
                })
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: filterPressed
    func filterPressed(_ sender: UIBarButtonItem) {
        
        self.hideNotificationView()
        
        let ordersFilterVC = OrderFiltersVC(initialItem: nil)
        ordersFilterVC.applyFiltersBlock = {[weak self] (startDate_: Date, endDate_: Date, customerId_: UInt64, supplierId_: UInt64, staffId: UInt64, documentType_: OrderDocumentType, filterOptions_: FilterOptions) -> Void in
            
            self?.applyFiltersToOrder(startDate: startDate_,
                                      endDate: endDate_,
                                      customerId: customerId_,
                                      supplierId: supplierId_,
                                      staffId: staffId,
                                      documentType: documentType_,
                                      filterOptions: filterOptions_)
        }
        self.presentSheet(viewController: ordersFilterVC)
    }
    
    private func applyFiltersToOrder(startDate: Date, endDate: Date, customerId: UInt64, supplierId: UInt64, staffId: UInt64, documentType: OrderDocumentType, filterOptions: FilterOptions) -> Void {
        
        self.isFiltersApplied = true
        
        let documentString = Order.orderDocumentTypeStringName(orderDocType: documentType)
        let startDateString = "\(startDate.toString(.custom("EEEE, dd MMM HH:mm:ss")))"
        let endDateString = "\(endDate.toString(.custom("EEEE, dd MMM HH:mm:ss")))"
        
        var customerOrSupplierName: String = ""
        if customerId > 0 {
            if let customer = Database.shared.customerBy(customerId: customerId) {
                customerOrSupplierName = "\(customer.firstName) \(customer.secondName)"
            }
        } else if supplierId > 0 {
            if let supplier = Database.shared.supplierBy(supplierId: supplierId) {
                customerOrSupplierName = "\(supplier.companyName)"
            }
        }
        
        if staffId > 0 {
            if let employee = Database.shared.userBy(userId: staffId) {
                customerOrSupplierName = customerOrSupplierName + "|" + "сотр:" + "\(employee.firstName) \(employee.secondName)"
            }
        }
        
        Database.shared.loadOrdersForFilters(documentType: documentType,
                                             startDate: startDate,
                                             endDate: endDate,
                                             customerId: customerId,
                                             supplierId: supplierId,
                                             userId: staffId,
                                             limit: 500,
                                             filterOptions: filterOptions) {[weak self] (orders_: [Order]) in
                                                
                                                if let strongSelf = self {
                                                    
                                                    strongSelf.enableBelowHistoryRequests = false
                                                    strongSelf.orders = orders_
                                                    strongSelf.tableView.reloadData()
                                                    
                                                    let fullString = "Найд:\(orders_.count)| тип-док:\(documentString)| нач:\(startDateString)| кон:\(endDateString)| контр:\(customerOrSupplierName)"
                                                    
                                                    strongSelf.showNotificationView(fullString)
                                                }
        }
    }
    
    private func showNotificationView(_ message_: String) -> Void {
        
        self.notificationView = CSNotificationView(parentViewController: self,
                                                   tintColor: UIColor.midnightBlueColor(),
                                                   image: UIImage(named: "close-X")!,
                                                   message: message_)
        
        self.notificationView?.tapHandler = {[weak self] (Void) -> Void in
            self?.hideNotificationView()
        }
        
        self.notificationView?.setVisible(true, animated: true, completion: {[weak self] in
            
            if let strongSelf = self {
                
                let originalInset: UIEdgeInsets = strongSelf.tableView.contentInset
                var inset: UIEdgeInsets = originalInset
                inset.top = 45
                let originalContentOffset: CGPoint = strongSelf.tableView.contentOffset;
                let contentOffset: CGPoint = originalContentOffset;
                
                UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions(), animations: {
                    strongSelf.tableView.bounds = CGRect(x: 0, y: contentOffset.y, width: strongSelf.tableView.frame.size.width, height: strongSelf.tableView.frame.size.height)
                    strongSelf.tableView.contentInset = inset;
                    strongSelf.tableView.scrollIndicatorInsets = inset;
                }, completion: { (finished:Bool) in
                })
                print("show completed")
            }
        })
    }
    
    private func hideNotificationView() -> Void {
        if let notifView = self.notificationView {
            notifView.setVisible(false, animated: true, completion: {[weak self] in
                if let strongSelf = self {
                    strongSelf.isFiltersApplied = false
                    strongSelf.tableView.contentInset.top = 0
                    SmartLoader.shared.loadOrders(orderDocumentType: strongSelf.initialOrderDocumentType,
                                                  date: Date(),
                                                  limit: 50) {[weak self] (orders_: [Order]) in
                                                    self?.enableBelowHistoryRequests = true
                                                    self?.orders = orders_
                                                    self?.tableView.reloadData()
                    }
                    print("hide completed")
                }
            })
        }
    }
    
    // MARK: TableView DataSource
    private func loadMoreMessagesAbove() -> Void {
        
    }
    
    private func loadMoreMessagesBelow() -> Void {
        
        if !self.enableBelowHistoryRequests {
            return
        }
        
        if let currentLastOrder = self.orders.last {
            self.showLoading("Загружаем")
            self.enableBelowHistoryRequests = false
            SmartLoader.shared.loadOrders(orderDocumentType: self.initialOrderDocumentType,
                                          date: currentLastOrder.orderDate,
                                          limit: 50) { (orders_: [Order]) in
                                            self.dismissLoading()
                                            
                                            if orders_.count > 0 {
                                                var indicicesToInsert :[IndexPath] = []
                                                for (index, _) in orders_.enumerated() {
                                                    let indexPath = IndexPath(row: index + self.orders.count, section: 0)
                                                    indicicesToInsert.append(indexPath)
                                                }
                                            
                                                self.orders.insert(contentsOf: orders_, at: self.orders.count)
                                            
                                                self.tableView.beginUpdates()
                                                self.tableView.insertRows(at: indicicesToInsert, with: .top)
                                                self.tableView.endUpdates()
                                            
                                                let delay = DispatchTime.now() + DispatchTimeInterval.seconds(1)
                                                DispatchQueue.main.asyncAfter(deadline: delay, execute: {
                                                    self.enableBelowHistoryRequests = true
                                                })
                                            } else {
                                                self.enableBelowHistoryRequests = false
                                            }
            }
        }
    }
    
    // MARK: TableView DataSource
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.tableView {
            let flt_epsilon: CGFloat = 0.1
            if scrollView.contentSize.height > flt_epsilon {
                
                if (enableBelowHistoryRequests && scrollView.contentOffset.y > scrollView.contentSize.height - 500 * 2.0) && (scrollView.contentSize.height > flt_epsilon) {
                    print("loadMoreMessagesBelow")
                    self.loadMoreMessagesBelow()
                }
                
                if (enableAboveHistoryRequests && scrollView.contentOffset.y < 60 * 2.0) {
                    print("loadMoreMessagesAbove")
                    self.loadMoreMessagesAbove()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderListTVCell", for: indexPath) as! OrderListTVCell
        let order = orders[(indexPath as NSIndexPath).row]
        cell.order = order
        cell.reloadData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let order = orders[(indexPath as NSIndexPath).row]
        let orderDescVC = OrderDescriptionVC(initialOrderId: order.orderId)
        orderDescVC.performReloadBlock = {[weak self] (Void) -> Void in
            self?.reloadAllOrders()
        }
        self.navigationController?.pushViewController(orderDescVC, animated: true)
    }
}
