//
//  TextFromOrderVC.swift
//  Rentautomation
//
//  Created by kanybek on 1/18/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class TextFromOrderVC: BaseListVC {
    
    @IBOutlet weak var commentsTextView: UITextView!
    var initialItem: String?
    
    convenience init() {
        self.init(initialItem: nil)
    }
    
    init(initialItem: String?) {
        self.initialItem = initialItem
        super.init(nibName: "TextFromOrderVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addCanceButton()
        let createButton = UIBarButtonItem(
            title: "Сохранить комментарии",
            style: .plain,
            target: self,
            action: #selector(applyFilters(_:))
        )
        self.navigationItem.rightBarButtonItem = createButton
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let initialItem_ = self.initialItem {
            self.commentsTextView.text = initialItem_
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.commentsTextView.anchorInCorner(.topLeft, xPad: 40, yPad: 40, width: 720, height: 400)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func applyFilters(_ sender: AnyObject) -> Void {
        self.cancelVC(sender)
    }
    
    override func cancelVC(_ sender: AnyObject) -> Void {
        self.navigationController?.dismiss(animated: true, completion: {
            print("dismiss TextFromOrderVC")
        })
    }
}
