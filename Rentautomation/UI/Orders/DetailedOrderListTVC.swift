//
//  DetailedOrderListTVC.swift
//  Rentautomation
//
//  Created by kanybek on 2/12/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import CSNotificationView

//
// MARK: - Section Data Structure
//
struct TPSection {
    var name: String
    var order: Order
    var payment: Payment
    var orderDetails: [OrderDetail]
    var collapsed: Bool
    var totalSaleAmount: Double
    
    init(name: String,
         order: Order,
         payment: Payment,
         orderDetails: [OrderDetail],
         collapsed: Bool = false,
         totalSaleAmount: Double) {
        
        self.name = name
        self.order = order
        self.payment = payment
        self.orderDetails = orderDetails
        self.collapsed = collapsed
        self.totalSaleAmount = totalSaleAmount
    }
}

//
// MARK: - View Controller
//
class DetailedOrderListTVC: UITableViewController, ErrorPopoverRenderer {
    
    var initialOrderType: OrderDocumentType
    var sections = [TPSection]()
    
    var notificationView: CSNotificationView?
    
    convenience init() {
        self.init(initialOrderType: .none)
    }
    
    init(initialOrderType: OrderDocumentType) {
        self.initialOrderType = initialOrderType
        super.init(style: .plain)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addCanceButton()
        
        let addButton = UIBarButtonItem(
            title: "Фильтры",
            style: .plain,
            target: self,
            action: #selector(filterPressed(_:))
        )
        self.navigationItem.rightBarButtonItem = addButton
        
        let nib1 = UINib(nibName: "CategoriesListTVCell", bundle: nil)
        self.tableView.register(nib1, forCellReuseIdentifier: "CategoriesListTVCell")
        
        let nib2 = UINib(nibName: "OrderDetailsHistoryTVCell", bundle: nil)
        self.tableView.register(nib2, forCellReuseIdentifier: "OrderDetailsHistoryTVCell")
        
        let headerNib = UINib(nibName: "CustomHeaderView", bundle: nil)
        self.tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "CustomHeaderView")
        
        self.tableView.alwaysBounceVertical = true
        
        self.title = "Свертка \(Order.orderDocumentTypeStringName(orderDocType: self.initialOrderType).lowercased())"
        
        SmartLoader.shared.loadOrders(orderDocumentType: self.initialOrderType,
                                      date: Date(),
                                      limit: 50) {[weak self] (orders_: [Order]) in
                                        self?.reloadOrders(orders_: orders_)
        }
    }
    
    private func reloadOrders(orders_: [Order]) -> Void {
        self.sections.removeAll()
        
        for order in orders_ {
            let orderDetails = Database.shared.orderDetailsBy(orderId: order.orderId)
            var totalSaledAmount: Double = 0.0
            for orderDetail in orderDetails {
                totalSaledAmount = totalSaledAmount + orderDetail.orderQuantity
            }
            
            var customerOrSupplierName: String = ""
            if order.customerId > 0 {
                if let customer = Database.shared.customerBy(customerId: order.customerId) {
                    customerOrSupplierName = "\(customer.firstName) \(customer.secondName)"
                }
            } else if order.supplierId > 0 {
                if let supplier = Database.shared.supplierBy(supplierId: order.supplierId) {
                    customerOrSupplierName = "\(supplier.companyName)"
                }
            }
            
            let payment = Database.shared.paymentBy(paymentId: order.paymentId)!
            
            let section: TPSection = TPSection(name: customerOrSupplierName,
                                           order: order,
                                           payment: payment,
                                           orderDetails: orderDetails,
                                           collapsed: false,
                                           totalSaleAmount: totalSaledAmount)
            self.sections.append(section)
        }
        self.tableView.reloadData()
    }
    
    func addCanceButton() -> Void {
        let cancelButton = UIBarButtonItem(
            title: "Закрыть",
            style: .plain,
            target: self,
            action: #selector(cancelVC(_:))
        )
        self.navigationItem.leftBarButtonItem = cancelButton
    }
    
    func cancelVC(_ sender: AnyObject) -> Void {
        let empty = EmptyVC()
        let detailNavigationController = RNNavigationController(rootViewController:empty)
        self.splitViewController?.showDetailViewController(detailNavigationController, sender: self)
    }
    
    
    // MARK: filterPressed
    func filterPressed(_ sender: AnyObject) {
        
        self.hideNotificationView()
        
        let ordersFilterVC = OrderFiltersVC(initialItem: nil)
        ordersFilterVC.applyFiltersBlock = {[weak self] (startDate_: Date, endDate_: Date, customerId_: UInt64, supplierId_: UInt64, staffId_: UInt64, documentType_: OrderDocumentType, filterOptions: FilterOptions) -> Void in
            
            self?.applyFiltersToOrder(startDate: startDate_,
                                      endDate: endDate_,
                                      customerId: customerId_,
                                      supplierId: supplierId_,
                                      staffId: staffId_,
                                      documentType: documentType_)
        }
        self.presentSheet(viewController: ordersFilterVC)
    }
    
    private func applyFiltersToOrder(startDate: Date, endDate: Date, customerId: UInt64, supplierId: UInt64, staffId: UInt64, documentType: OrderDocumentType) -> Void {
        
        let documentString = Order.orderDocumentTypeStringName(orderDocType: documentType)
        let startDateString = "\(startDate.toString(.custom("EEEE, dd MMM HH:mm:ss")))"
        let endDateString = "\(endDate.toString(.custom("EEEE, dd MMM HH:mm:ss")))"
        
        var customerOrSupplierName: String = ""
        if customerId > 0 {
            if let customer = Database.shared.customerBy(customerId: customerId) {
                customerOrSupplierName = "\(customer.firstName) \(customer.secondName)"
            }
        } else if supplierId > 0 {
            if let supplier = Database.shared.supplierBy(supplierId: supplierId) {
                customerOrSupplierName = "\(supplier.companyName)"
            }
        }
        if staffId > 0 {
            if let employee = Database.shared.userBy(userId: staffId) {
                customerOrSupplierName = customerOrSupplierName + "|" + "сотр:" + "\(employee.firstName) \(employee.secondName)"
            }
        }
        
        self.showLoading("загрузка ...")
        Database.shared.loadOrdersForFilters(documentType: documentType,
                                             startDate: startDate,
                                             endDate: endDate,
                                             customerId: customerId,
                                             supplierId: supplierId,
                                             userId: staffId,
                                             limit: 500,
                                             filterOptions: []) {[weak self] (orders_: [Order]) in
                                                self?.dismissLoading()
                                                self?.reloadOrders(orders_: orders_)
                                                let fullString = "Найд:\(orders_.count)| тип-док:\(documentString)| нач:\(startDateString)| кон:\(endDateString)| контр:\(customerOrSupplierName)"
                                                
                                                self?.showNotificationView(fullString)
        }
    }
    
    private func showNotificationView(_ message_: String) -> Void {
        
        self.notificationView = CSNotificationView(parentViewController: self,
                                                   tintColor: UIColor.midnightBlueColor(),
                                                   image: UIImage(named: "close-X")!,
                                                   message: message_)
        
        self.notificationView?.tapHandler = {[weak self] (Void) -> Void in
            self?.hideNotificationView()
        }
        
        self.notificationView?.setVisible(true, animated: true, completion: {[weak self] in
            
            if let strongSelf = self {
                
                let originalInset: UIEdgeInsets = strongSelf.tableView.contentInset
                var inset: UIEdgeInsets = originalInset
                inset.top = 45
                let originalContentOffset: CGPoint = strongSelf.tableView.contentOffset;
                let contentOffset: CGPoint = originalContentOffset;
                
                UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions(), animations: {
                    strongSelf.tableView.bounds = CGRect(x: 0, y: contentOffset.y, width: strongSelf.tableView.frame.size.width, height: strongSelf.tableView.frame.size.height)
                    strongSelf.tableView.contentInset = inset;
                    strongSelf.tableView.scrollIndicatorInsets = inset;
                }, completion: { (finished:Bool) in
                })
                print("show completed")
            }
        })
    }
    
    private func hideNotificationView() -> Void {
        if let notifView = self.notificationView {
            notifView.setVisible(false, animated: true, completion: {[weak self] in
                if let strongSelf = self {
                    strongSelf.tableView.contentInset.top = 0
                    SmartLoader.shared.loadOrders(orderDocumentType: .none,
                                                  date: Date(),
                                                  limit: 50) {[weak self] (orders_: [Order]) in
                                                    self?.reloadOrders(orders_: orders_)
                    }
                    print("hide completed")
                }
            })
        }
    }
}

//
// MARK: - View Controller DataSource and Delegate
//
extension DetailedOrderListTVC {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].orderDetails.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if sections[indexPath.section].collapsed {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CategoriesListTVCell", for: indexPath) as! CategoriesListTVCell
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailsHistoryTVCell", for: indexPath) as! OrderDetailsHistoryTVCell
            let orderDetail = sections[indexPath.section].orderDetails[indexPath.row]
            cell.orderDetail = orderDetail
            cell.numberLabel.text = "\(indexPath.row + 1). "
            cell.reloadData()
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return sections[indexPath.section].collapsed ? 0 : 60.0
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CustomHeaderView") as! CustomHeaderView
        
        header.setCollapsed(collapsed: sections[section].collapsed)
        let order = sections[section].order
        header.order = order
        header.tpSection = sections[section]
        header.section = section
        header.delegate = self
        
        header.reloadData()
        
        return header
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
}

//
// MARK: - Section Header Delegate
//
extension DetailedOrderListTVC: CollapsibleTableViewHeaderDelegate {
    
    func toggleSection(header: CustomHeaderView, section: Int) {
        let collapsed = !sections[section].collapsed
        
        // Toggle collapse
        sections[section].collapsed = collapsed
        header.setCollapsed(collapsed: collapsed)
        
        // Adjust the height of the rows inside the section
        //tableView.reloadData()
        tableView.beginUpdates()
        for i in 0 ..< sections[section].orderDetails.count {
            tableView.reloadRows(at: [IndexPath(row: i, section: section)], with: .automatic)
        }
        tableView.endUpdates()
    }
    
}
