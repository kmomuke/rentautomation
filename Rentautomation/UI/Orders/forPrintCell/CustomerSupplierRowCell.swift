//
//  CustomerSupplierRowCell.swift
//  Rentautomation
//
//  Created by kanybek on 4/25/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class CustomerSupplierRowCell: UITableViewCell {

    @IBOutlet weak var supplierLabel: UILabel!
    @IBOutlet weak var supplierContentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
