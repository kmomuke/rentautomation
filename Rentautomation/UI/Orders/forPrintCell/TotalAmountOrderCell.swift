//
//  TotalAmountOrderCell.swift
//  Rentautomation
//
//  Created by kanybek on 4/25/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class TotalAmountOrderCell: UITableViewCell {
    
    @IBOutlet weak var totalStringLabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
