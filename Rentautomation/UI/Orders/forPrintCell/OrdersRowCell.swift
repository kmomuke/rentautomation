//
//  OrdersRowCell.swift
//  Rentautomation
//
//  Created by kanybek on 4/25/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class OrdersRowCell: UITableViewCell {
    
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var totalAmountWithDiscountLabel: UILabel!
    
    var orderDetail: OrderDetail?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.accessoryType = .none
        self.editingAccessoryType = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func reloadData(row: Int) -> Void {
        if let orderDetail_ = self.orderDetail {
            self.numberLabel.text = "\(row)"
            var productName: String = ""
            var quantityPerUnit: String = ""
            if let product = Database.shared.productFor(productId: orderDetail_.productId) {
                productName = product.productName
                quantityPerUnit = product.quantityPerUnit
            }
            self.nameLabel.text = "\(productName)"
            self.priceLabel.text = Global.shared.priceFormatter.string(for: orderDetail_.price)
            
            var orderQuantity: String = ""
            if let text_ = Global.shared.amountFormatter.string(for: orderDetail_.orderQuantity) {
                orderQuantity = text_
            }
            self.quantityLabel.text = "\(orderQuantity) \(quantityPerUnit)"
            
            let totalPriceWithDiscount = orderDetail_.price * (1) * orderDetail_.orderQuantity
            self.totalAmountWithDiscountLabel.text =  Global.shared.priceFormatter.string(for: totalPriceWithDiscount)
            
        } else {
            self.resetLabels()
        }
    }
    
    override func prepareForReuse() {
        self.orderDetail = nil
        self.resetLabels()
    }
    
    func resetLabels() -> Void {
        self.nameLabel.text = ""
        self.priceLabel.text = ""
        self.quantityLabel.text = ""
        self.totalAmountWithDiscountLabel.text = ""
        self.numberLabel.text = ""
    }
}

