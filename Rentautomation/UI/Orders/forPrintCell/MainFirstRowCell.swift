//
//  MainFirstRowCell.swift
//  Rentautomation
//
//  Created by kanybek on 4/25/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class MainFirstRowCell: UITableViewCell {
    @IBOutlet weak var fullTitleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
