//
//  MoneyWordsRowCell.swift
//  Rentautomation
//
//  Created by kanybek on 4/25/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class MoneyWordsRowCell: UITableViewCell {
    
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var moneyStringLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
}
