//
//  TotalMoneyOnDateVC.swift
//  Rentautomation
//
//  Created by kanybek on 2/4/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import CSNotificationView

class TotalMoneyOnDateVC: FormViewController, ErrorPopoverRenderer {
    
    var notificationView: CSNotificationView?
    
    var initialItem: String?

    // ------
    var customerOrSupplierName: String?
    
    var productSaleToCustomer: Double = 0.0
    var productReceiveFromSupplier: Double = 0.0
    
    var moneyReceived: Double = 0.0
    var moneyGone: Double = 0.0
    
    var returnedFromCustomer: Double = 0.0
    var returnedToSupplier: Double = 0.0
    // ------
    
    convenience init() {
        self.init(initialItem: nil)
    }

    init(initialItem: String?) {
        self.initialItem = initialItem
        super.init(nibName: nil, bundle: nil)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func cancelVC(_ sender: AnyObject) -> Void {
        let empty = EmptyVC()
        let detailNavigationController = RNNavigationController(rootViewController:empty)
        self.splitViewController?.showDetailViewController(detailNavigationController, sender: self)
    }
    
    
    private enum InsertPosition: Int {
        case Below, Above
    }
    private var insertRowPosition: InsertPosition = .Below
    
    // MARK: Public
    override func viewDidLoad() {
        super.viewDidLoad()
        var shouldAddCloseButton = true
        if let initialItem_ = self.initialItem {
            if initialItem_ == "fromIphone" {
                shouldAddCloseButton = false
            }
        }
        
        if shouldAddCloseButton {
            let canceButton = UIBarButtonItem(
                title: "Закрыть",
                style: .plain,
                target: self,
                action: #selector(cancelVC(_:))
            )
            self.navigationItem.leftBarButtonItem = canceButton
        }
        
        let addButton = UIBarButtonItem(
            title: "Фильтры",
            style: .plain,
            target: self,
            action: #selector(filterPressed(_:))
        )
        self.navigationItem.rightBarButtonItem = addButton
        
        configure()
    }
    
    // MARK: filterPressed
    func filterPressed(_ sender: AnyObject) {
        
        self.hideNotificationView()
        
        let ordersFilterVC = OrderFiltersVC(initialItem: nil)
        ordersFilterVC.applyFiltersBlock = {[weak self] (startDate_: Date, endDate_: Date, customerId_: UInt64, supplierId_: UInt64, staffId: UInt64, documentType_: OrderDocumentType, filterOptions: FilterOptions) -> Void in
            
            self?.applyFiltersToOrder(startDate: startDate_,
                                      endDate: endDate_,
                                      customerId: customerId_,
                                      supplierId: supplierId_,
                                      staffId: staffId,
                                      documentType: documentType_)
        }
        self.presentSheet(viewController: ordersFilterVC)
    }
    
    private func applyFiltersToOrder(startDate: Date, endDate: Date, customerId: UInt64, supplierId: UInt64, staffId: UInt64, documentType: OrderDocumentType) -> Void {
        
        let documentString = Order.orderDocumentTypeStringName(orderDocType: documentType)
        let startDateString = "\(startDate.toString(.custom("EEEE, dd MMM HH:mm:ss")))"
        let endDateString = "\(endDate.toString(.custom("EEEE, dd MMM HH:mm:ss")))"
        
        var customerOrSupplierName: String = ""
        if customerId > 0 {
            if let customer = Database.shared.customerBy(customerId: customerId) {
                customerOrSupplierName = "\(customer.firstName) \(customer.secondName)"
            }
        } else if supplierId > 0 {
            if let supplier = Database.shared.supplierBy(supplierId: supplierId) {
                customerOrSupplierName = "\(supplier.companyName)"
            }
        }
        if staffId > 0 {
            if let employee = Database.shared.userBy(userId: staffId) {
                customerOrSupplierName = customerOrSupplierName + "|" + "сотр:" + "\(employee.firstName) \(employee.secondName)"
            }
        }
        
        self.customerOrSupplierName = customerOrSupplierName
        
        Database.shared.loadOrdersForFilters(documentType: documentType,
                                             startDate: startDate,
                                             endDate: endDate,
                                             customerId: customerId,
                                             supplierId: supplierId,
                                             userId: staffId,
                                             limit: 500,
                                             filterOptions: []) {[weak self] (orders_: [Order]) in
                                                
                                                let fullString = "Найд:\(orders_.count)| тип-док:\(documentString)| нач:\(startDateString)| кон:\(endDateString)| контр:\(customerOrSupplierName)"
                                                
                                                self?.showNotificationView(fullString)
                                                self?.calculateOrders(orders: orders_)
        }
    }
    
    private func calculateOrders(orders: [Order]) -> Void {
        
        var productSaleToCustomer: Double = 0.0
        var productReceiveFromSupplier: Double = 0.0
        
        var moneyReceived: Double = 0.0
        var moneyGone: Double = 0.0
        
        var returnedFromCustomer: Double = 0.0
        var returnedToSupplier: Double = 0.0
        
        for order in orders {
            
            if order.isEdited {
                continue
            }
            
            switch order.orderDocument {
                
            case .none:
                print(".none")
                
            case .saleToCustomer:
                print(".productOrderSaledToCustomer")
                if let payment = Database.shared.paymentBy(paymentId: order.paymentId) {
                    productSaleToCustomer = productSaleToCustomer + payment.totalPriceWithDiscount
                }
            case .saleToCustomerEdited:
                print(".productOrderSaleEditedToCustomer")
                
            case .receiveFromSupplier:
                print(".productOrderReceivedFromSupplier")
                if let payment = Database.shared.paymentBy(paymentId: order.paymentId) {
                    productReceiveFromSupplier = productReceiveFromSupplier + payment.totalPriceWithDiscount
                }
                
            case .receiveFromSupplierEdited:
                print(".productOrderReceiveEditedFromSupplier")
                
            case .returnFromCustomer:
                print(".productReturnedFromCustomer")
                if let payment = Database.shared.paymentBy(paymentId: order.paymentId) {
                    returnedFromCustomer = returnedFromCustomer + payment.totalPriceWithDiscount
                }
                
            case .returnFromCustomerEdited:
                print(".productReturneEditedFromCustomer")
                
            case .returnToSupplier:
                print(".productReturnedToSupplier")
                if let payment = Database.shared.paymentBy(paymentId: order.paymentId) {
                    returnedToSupplier = returnedToSupplier + payment.totalPriceWithDiscount
                }
                
            case .returnToSupplierEdited:
                print(".productReturneEditedToSupplier")
                
            case .moneyReceive:
                print(".moneyReceived")
                if let payment = Database.shared.paymentBy(paymentId: order.paymentId) {
                    moneyReceived = moneyReceived + payment.totalPriceWithDiscount
                }
                
            case .moneyReceiveEdited:
                print(".moneyReceiveEdited")
                
            case .moneyGone:
                print(".moneyGone")
                if let payment = Database.shared.paymentBy(paymentId: order.paymentId) {
                    moneyGone = moneyGone + payment.totalPriceWithDiscount
                }
                
            case .moneyGoneEdited:
                print(".moneyGoneEdited")
                
            default:
                print("default")
            }
        }
        
        self.productSaleToCustomer = productSaleToCustomer
        self.productReceiveFromSupplier = productReceiveFromSupplier
        
        self.moneyReceived = moneyReceived
        self.moneyGone = moneyGone
        
        self.returnedFromCustomer = returnedFromCustomer
        self.returnedToSupplier = returnedToSupplier
    }
    
    private func showNotificationView(_ message_: String) -> Void {
        
        self.notificationView = CSNotificationView(parentViewController: self,
                                                   tintColor: UIColor.midnightBlueColor(),
                                                   image: UIImage(named: "close-X")!,
                                                   message: message_)
        
        self.notificationView?.tapHandler = {[weak self] (Void) -> Void in
            self?.hideNotificationView()
        }
        
        self.notificationView?.setVisible(true, animated: true, completion: {[weak self] in
            self?.updateAllRows()
        })
    }
    
    private func hideNotificationView() -> Void {
        if let notifView = self.notificationView {
            notifView.setVisible(false, animated: true, completion: {[weak self] in
                self?.customerOrSupplierName = nil
                
                self?.productSaleToCustomer = 0.0
                self?.productReceiveFromSupplier = 0.0
                
                self?.moneyReceived = 0.0
                self?.moneyGone = 0.0
                
                self?.returnedFromCustomer = 0.0
                self?.returnedToSupplier = 0.0
                
                self?.updateAllRows()
            })
        }
    }
    
    private func updateAllRows() -> Void {
        for sections in self.former.sectionFormers {
            for row in sections.rowFormers {
                row.update()
            }
        }
    }
    
    private func configure() {
        title = "Деньги за промежуток времени"
        
        tableView.contentInset.top = 40
        tableView.contentInset.bottom = 40
        tableView.backgroundColor = UIColor.white
        self.view.backgroundColor = UIColor.white
        
        //tableView.contentInset.top = 10
        //tableView.contentInset.bottom = 30
        //tableView.contentOffset.y = -10
        
        let positions = ["     Below    ", "     Above     "]
        let insertRowPositionRow = SegmentedRowFormer<FormSegmentedCell>(instantiateType: .Class) {
            $0.tintColor = .formerSubColor()
            $0.titleLabel.text = "Segmented"
            
            }.configure {
                $0.rowHeight = 60
                $0.segmentTitles = positions
                $0.selectedIndex = insertRowPosition.rawValue
            }.onSegmentSelected { [weak self] index, _ in
                self?.insertRowPosition = InsertPosition(rawValue: index)!
        }
        
        // Create Headers
        let createHeader: (() -> ViewFormer) = {
            return CustomViewFormer<FormHeaderFooterView>()
                .configure {
                    $0.viewHeight = 30
            }
        }
        
        let createCustomButtonRow: ((String, String, (() -> Void)?, ((LabelRowFormer<FormLabelCell>) -> Void)?) -> RowFormer) = { [weak self] text, subText, onSelected, onUpdated in
            return LabelRowFormer<FormLabelCell>() {
                
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .none
                $0.subTextLabel.textColor = .formerSubColor()
                $0.subTextLabel.font = .boldSystemFont(ofSize: 14)
                
                }.configure {
                    $0.text = text
                    $0.rowHeight = 60
                    $0.subText = subText
                }.onSelected { [weak self] _ in
                    onSelected?()
                    self?.former.deselect(animated: true)
                }.onUpdate { [weak self] m in
                    onUpdated?(m)
            }
        }
        
        let customerSupplierRow = createCustomButtonRow("Контрагент", "", {
            print("customerSupplierRow")
        }) { (subLabel: LabelRowFormer<FormLabelCell>) in
            if let custOrSuppName = self.customerOrSupplierName {
                subLabel.subText = custOrSuppName
            } else {
                subLabel.subText = "Не выбран"
            }
        }
        
        let someText = Global.shared.priceFormatter.string(for: self.productSaleToCustomer)
        let customSaleRow = createCustomButtonRow("Продажи", someText!, {
            print("customSaleRow")
        }) { (subLabel: LabelRowFormer<FormLabelCell>) in
            subLabel.subText = Global.shared.priceFormatter.string(for: self.productSaleToCustomer)
        }
        
        let editSaleRow = createCustomButtonRow("Редактирование продажи", "в процессе", {
            print("editSaleRow")
        }) { (subLabel: LabelRowFormer<FormLabelCell>) in
            subLabel.subText = "в процессе"
        }
        
        let supplierIncomeRow = createCustomButtonRow("Закупки", "", {
            print("supplierIncomeRow")
        }) { (subLabel: LabelRowFormer<FormLabelCell>) in
            subLabel.subText = Global.shared.priceFormatter.string(for: self.productReceiveFromSupplier)
        }
        
        let editSupplierIncomeRow = createCustomButtonRow("Редактирование закупки", "в процессе", {
            print("editSupplierIncomeRow")
        }) { (subLabel: LabelRowFormer<FormLabelCell>) in
            subLabel.subText = "в процессе"
        }
        
        let incomeRow = createCustomButtonRow("Приход", "", {
            print("incomeRow")
        }) { (subLabel: LabelRowFormer<FormLabelCell>) in
            subLabel.subText = Global.shared.priceFormatter.string(for: self.moneyReceived)
        }
        
        let outgoneRow = createCustomButtonRow("Расход", "", {
            print("outgoneRow")
        }) { (subLabel: LabelRowFormer<FormLabelCell>) in
            subLabel.subText = Global.shared.priceFormatter.string(for: self.moneyGone)
        }
        
        let returnSaleRow = createCustomButtonRow("Возвраты от клиента", "", {
            print("returnSaleRow")
        }) { (subLabel: LabelRowFormer<FormLabelCell>) in
            subLabel.subText = Global.shared.priceFormatter.string(for: self.returnedFromCustomer)
        }
        
        let returnSupplierIncomeRow = createCustomButtonRow("Возвраты к поставщику", "", {
            print("returnSupplierIncomeRow")
        }) { (subLabel: LabelRowFormer<FormLabelCell>) in
            subLabel.subText = Global.shared.priceFormatter.string(for: self.returnedToSupplier)
        }
        
        let titleSection0 = SectionFormer(rowFormer:customerSupplierRow, customSaleRow, editSaleRow, supplierIncomeRow, editSupplierIncomeRow, incomeRow, outgoneRow, returnSaleRow, returnSupplierIncomeRow)
            .set(headerViewFormer: createHeader())
        
        let dateSection = SectionFormer(rowFormer: insertRowPositionRow)
            .set(headerViewFormer: createHeader())
        
        former.append(sectionFormer: titleSection0)
    }
    
    
    private func configureCell(cell: RowFormer) -> Void {
        
    }
}
