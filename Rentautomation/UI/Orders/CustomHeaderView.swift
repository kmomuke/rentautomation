//
//  CustomHeaderView.swift
//  Rentautomation
//
//  Created by kanybek on 2/13/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

protocol CollapsibleTableViewHeaderDelegate {
    func toggleSection(header: CustomHeaderView, section: Int)
}

class CustomHeaderView: UITableViewHeaderFooterView {
    
    var delegate: CollapsibleTableViewHeaderDelegate?
    var section: Int = 0
    
    var order: Order?
    var tpSection: TPSection?
    
    @IBOutlet weak var backgroundCustomView: UIView!
    @IBOutlet weak var numberCountLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var orderTypeLabel: UILabel!
    @IBOutlet weak var customerSupplierLabel: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var totalSumLabel: UILabel!
    
    @IBOutlet weak var arrowLabel: UILabel!
    
    class func instanceFromNib() -> UITableViewHeaderFooterView {
        return UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UITableViewHeaderFooterView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CustomHeaderView.tapHeader(gestureRecognizer:))))
        
        self.backgroundCustomView.backgroundColor = UIColor.clear
        self.numberCountLabel.backgroundColor = UIColor.clear
        self.dateLabel.backgroundColor = UIColor.clear
        self.orderTypeLabel.backgroundColor = UIColor.clear
        self.customerSupplierLabel.backgroundColor = UIColor.clear
        self.discountLabel.backgroundColor = UIColor.clear
        self.totalSumLabel.backgroundColor = UIColor.clear
        self.arrowLabel.backgroundColor = UIColor.clear
        self.totalAmountLabel.backgroundColor = UIColor.clear
        self.backgroundCustomView.backgroundColor = UIColor.silverColor()
    }
    
    func tapHeader(gestureRecognizer: UITapGestureRecognizer) {
        guard let cell = gestureRecognizer.view as? CustomHeaderView else {
            return
        }
        
        delegate?.toggleSection(header: self, section: cell.section)
    }
    
    func setCollapsed(collapsed: Bool) {
        //
        // Animate the arrow rotation (see Extensions.swf)
        //
        arrowLabel.rotate(toValue: collapsed ? 0.0 : CGFloat(M_PI_2))
    }
    
    func reloadData() -> Void {
        if let order_ = self.order, let tpSection_ = self.tpSection {
        
            self.numberCountLabel.text = "\(order_.orderId)"
            self.dateLabel.text = "\(order_.orderDate.toString(.custom("EEE, dd MMM HH:mm:ss")))"
            self.orderTypeLabel.text = Order.orderDocumentTypeStringName(orderDocType: order_.orderDocument)
            
            var orderDetailCount: String = ""
            if order_.orderId > 0 {
                let totalCount = Database.shared.countOrderDetailsBy(orderId: order_.orderId)
                if totalCount > 0 {
                    orderDetailCount = "\n\(totalCount)-товара,"
                }
            }
            
            var customerOrSupplierName: String = ""
            if order_.customerId > 0 {
                if let customer = Database.shared.customerBy(customerId: order_.customerId) {
                    customerOrSupplierName = "\(customer.firstName) \(customer.secondName)"
                }
            } else if order_.supplierId > 0 {
                if let supplier = Database.shared.supplierBy(supplierId: order_.supplierId) {
                    customerOrSupplierName = "\(supplier.companyName)"
                }
            } else {
                customerOrSupplierName = Order.moneyMovementTypeStringName(moneyType: order_.moneyMovementType)
            }
            
            if order_.isEdited {
                self.customerSupplierLabel.text = customerOrSupplierName + orderDetailCount + " документ редактирован"
                self.customerSupplierLabel.textColor = UIColor.red
                self.customerSupplierLabel.backgroundColor = UIColor.clear
            } else {
                self.customerSupplierLabel.text = customerOrSupplierName + orderDetailCount
                self.customerSupplierLabel.backgroundColor = UIColor.clear
                self.customerSupplierLabel.textColor = UIColor.black
            }
            
            var discount: Double = 0.0
            var totalSum: Double = 0.0
            if let payment = Database.shared.paymentBy(paymentId: order_.paymentId) {
                discount = payment.discount
                totalSum = payment.totalPriceWithDiscountF()
            }
            self.discountLabel.text = Global.shared.priceFormatter.string(for: discount)
            self.totalAmountLabel.text = Global.shared.amountFormatter.string(for: tpSection_.totalSaleAmount)
            
            self.totalSumLabel.text = Global.shared.priceFormatter.string(for: totalSum)
            self.orderTypeLabel.textColor = Order.orderDocumentTypeColor(orderDocType: order_.orderDocument)
            self.orderTypeLabel.backgroundColor = UIColor.clear
            
            if Database.shared.isModelSynchronized(primaryKey: order_.orderId) {
                self.numberCountLabel.textColor = UIColor.black
                self.numberCountLabel.backgroundColor = UIColor.clear
            } else {
                self.numberCountLabel.textColor = UIColor.white
                self.numberCountLabel.backgroundColor = UIColor.pomergranateColor()
            }
            
        } else {
            self.resetLabels()
        }
    }
    
    override func prepareForReuse() {
        self.order = nil
        self.resetLabels()
    }
    
    func resetLabels() -> Void {
        self.numberCountLabel.text = ""
        self.dateLabel.text = ""
        self.orderTypeLabel.text = ""
        self.customerSupplierLabel.text = ""
        self.discountLabel.text = ""
        self.totalSumLabel.text = ""
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}

extension UIColor {
    
    convenience init(hex:Int, alpha:CGFloat = 1.0) {
        self.init(
            red:   CGFloat((hex & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((hex & 0x00FF00) >> 8)  / 255.0,
            blue:  CGFloat((hex & 0x0000FF) >> 0)  / 255.0,
            alpha: alpha
        )
    }
    
}

extension UIView {
    
    func rotate(toValue: CGFloat, duration: CFTimeInterval = 0.2) {
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        
        animation.toValue = toValue
        animation.duration = duration
        animation.isRemovedOnCompletion = false
        animation.fillMode = kCAFillModeForwards
        
        self.layer.add(animation, forKey: nil)
    }
    
}


