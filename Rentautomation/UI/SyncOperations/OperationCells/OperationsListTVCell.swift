//
//  OperationsListTVCell.swift
//  Rentautomation
//
//  Created by kanybek on 1/22/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class OperationsListTVCell: UITableViewCell {

    var operation: Event?
    
    @IBOutlet weak var primaryKeyLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var isSyncedStatusLabel: UILabel!
    @IBOutlet weak var additionalLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func reloadData() -> Void {
        
        if let operation_ = self.operation {
            self.primaryKeyLabel.text = "\(operation_.syncOperationId)"
            self.dateLabel.text = operation_.syncOperationDate.toString(.custom("EEE, dd MMM HH:mm:ss"))
            self.typeLabel.text = Event.syncOperationTypeStringName(syncType: operation_.operationType)
            if operation_.isSynchronized {
                self.isSyncedStatusLabel.text = "сохранен"
                self.isSyncedStatusLabel.textColor = UIColor.emeraldColor()
            } else {
                self.isSyncedStatusLabel.text = "не сохранен"
                self.isSyncedStatusLabel.textColor = UIColor.pomergranateColor()
            }
            self.additionalLabel.text = "\(operation_)"
        } else {
            self.resetLabels()
        }
    }
    
    override func prepareForReuse() {
        self.operation = nil
        self.resetLabels()
    }
    
    func resetLabels() -> Void {
        self.primaryKeyLabel.text = ""
        self.dateLabel.text = ""
        self.typeLabel.text = ""
        self.isSyncedStatusLabel.text = ""
        self.additionalLabel.text = ""
    }
    
}
