//
//  SyncOperationsListVC.swift
//  Rentautomation
//
//  Created by kanybek on 1/18/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftSignalKit

class SyncOperationsListVC: BaseListVC, ErrorPopoverRenderer, UITableViewDelegate, UITableViewDataSource {

    var tableView: UITableView
    var operations: [Event] = []
    
    var initialString: String?
    
    var enableAboveHistoryRequests: Bool
    var enableBelowHistoryRequests: Bool
    
    var disposeSet: DisposableSet?
    
    convenience init() {
        self.init(initialString: nil)
    }
    
    init(initialString: String?) {
        self.initialString = initialString
        self.tableView = UITableView(frame: CGRect.zero, style: .plain)
        self.enableAboveHistoryRequests = true
        self.enableBelowHistoryRequests = true
        self.disposeSet = DisposableSet()
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.disposeSet?.dispose()
        self.disposeSet = nil
    }
    
    override func loadView() {
        super.loadView()
        self.title = "Синхронизация"
        self.addCanceButton()
        
        let nib = UINib(nibName: "OperationsListTVCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "OperationsListTVCell")
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.view.addSubview(self.tableView)
        
        
        let syncButton = UIBarButtonItem(
            title: "Сохранить",
            style: .plain,
            target: self,
            action: #selector(syncAllOperations(_:))
        )
        
        if Global.shared.isShowTestButton {
            let addButton = UIBarButtonItem(
                title: "Тестировать",
                style: .plain,
                target: self,
                action: #selector(testDynamicOperation(_:))
            )
            self.navigationItem.rightBarButtonItems = [addButton, syncButton]
        } else {
            self.navigationItem.rightBarButtonItems = [syncButton]
        }
        
        let allSyncFinishedSignal: Signal = PipesStore.shared.allSyncOperationsFinishedSignal()
        let allSyncFinishedDispose = allSyncFinishedSignal.start(next: {[weak self] (finished: Bool) in
            self?.reloadAllData()
        })
        self.disposeSet?.add(allSyncFinishedDispose)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.reloadAllData()
    }
    
    func reloadAllData() {
        Database.shared.loadAllSyncOperations(date: Date().dateByAddingMonths(5), limit: 50, completion: {[weak self] (operations_: [Event]) in
            self?.operations = operations_
            self?.tableView.reloadData()
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = self.view.bounds
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func syncAllOperations(_ sender: AnyObject) {
        Synchronizer.shared.synchronizeAllNeededOperations {[weak self] (response: Bool) in
            if response {
                Database.shared.loadAllSyncOperations(date: Date().dateByAddingMonths(5), limit: 50, completion: {[weak self] (operations_: [Event]) in
                    self?.operations = operations_
                    self?.tableView.reloadData()
                })
            }
        }
    }
    
    func testDynamicOperation(_ sender: AnyObject) {
        
        let alert: UIAlertController = UIAlertController(title: "Тестировать",
                                                         message: "",
            preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "CREATE STATIC MODELS", style: .default, handler: {
            (UIAlertAction) -> Void in
            
            let customers = Database.shared.loadAllCustomers(100)
            if customers.count < 5 {
                self.showLoading("Generating models for create")
                GenerateSamples.shared.generateStaticModelsForCreate(completion: { (response: Bool) in
                    self.dismissLoading()
                    Database.shared.loadAllSyncOperations(date: Date().dateByAddingMonths(5), limit: 50, completion: { (operations_: [Event]) in
                        self.operations = operations_
                        self.tableView.reloadData()
                    })
                })
            } else {
                let _ = self.showErrorAlert(text: "Allready generated")
            }
        }))
        alert.addAction(UIAlertAction(title: "UPDATE STATIC MODELS", style: .default, handler: {
            (UIAlertAction) -> Void in
            self.showLoading("Generating models for update")
            GenerateSamples.shared.generateStaticModelsForUpdate(completion: { (response: Bool) in
                self.dismissLoading()
                Database.shared.loadAllSyncOperations(date: Date().dateByAddingMonths(5), limit: 50, completion: { (operations_: [Event]) in
                    self.operations = operations_
                    self.tableView.reloadData()
                })
            })
        }))
        
        alert.addAction(UIAlertAction(title: "CREATE DYNAMIC MODELS", style: .default, handler: {
            (UIAlertAction) -> Void in
            
            self.showLoading("Generating dynamic model create")
            GenerateSamples.shared.generateDynamicModelsForAllOperation { (finished: Bool) in
                self.dismissLoading()
                if finished {
                    Database.shared.loadAllSyncOperations(date: Date().dateByAddingMonths(5), limit: 50, completion: { (operations_: [Event]) in
                        self.operations = operations_
                        self.tableView.reloadData()
                    })
                }
            }
        }))
        
        alert.addAction(UIAlertAction(title: "GET STATIC MODELS", style: .default, handler: {
            (UIAlertAction) -> Void in
            self.showLoading("getting data ...")
            Synchronizer.shared.getInitialStaticModels(completion: {[weak self] (boolValue: Bool) in
                DispatchQueue.main.async {
                    self?.dismissLoading()
                }
            })
        }))
        
        alert.addAction(UIAlertAction(title: "TEST/CSV AMAZON S3", style: .default, handler: {
            (UIAlertAction) -> Void in
            //CSVGenerator.shared.someMethod1(self)
            AmazonUploader.shared.testAmazonS3Upload()
        }))
        
        alert.addAction(UIAlertAction(title: "STOP ALL PROCCESS", style: .default, handler: {
            (UIAlertAction) -> Void in
            Synchronizer.shared.stopAllSyncProcess()
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    private func loadMoreMessagesAbove() -> Void {

    }
    
    private func loadMoreMessagesBelow() -> Void {
        
        if !self.enableBelowHistoryRequests {
            return
        }
        
        if let currentLastOperation = self.operations.last {
            self.showLoading("Загружаем")
            self.enableBelowHistoryRequests = false
            Database.shared.loadAllSyncOperations(date: currentLastOperation.syncOperationDate, limit: 50, completion: { (operations_: [Event]) in
                self.dismissLoading()
                
                if operations_.count > 0 {
                    
                    var indicicesToInsert :[IndexPath] = []
                    for (index, _) in operations_.enumerated() {
                        let indexPath = IndexPath(row: index + self.operations.count, section: 0)
                        indicicesToInsert.append(indexPath)
                    }
                    
                    self.operations.insert(contentsOf: operations_, at: self.operations.count)
                    
                    self.tableView.beginUpdates()
                    self.tableView.insertRows(at: indicicesToInsert, with: .top)
                    self.tableView.endUpdates()
                    
                    let delay = DispatchTime.now() + DispatchTimeInterval.seconds(1)
                    DispatchQueue.main.asyncAfter(deadline: delay, execute: {
                        self.enableBelowHistoryRequests = true
                    })
                    
                } else {
                    self.enableBelowHistoryRequests = false
                }
            })
        }
    }
    
    // MARK: TableView DataSource
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.tableView {
            let flt_epsilon: CGFloat = 0.1
            if scrollView.contentSize.height > flt_epsilon {
                
                if (enableBelowHistoryRequests && scrollView.contentOffset.y > scrollView.contentSize.height - 500 * 2.0) && (scrollView.contentSize.height > flt_epsilon) {
                    print("loadMoreMessagesBelow")
                    self.loadMoreMessagesBelow()
                }
                
                if (enableAboveHistoryRequests && scrollView.contentOffset.y < 60 * 2.0) {
                    print("loadMoreMessagesAbove")
                    self.loadMoreMessagesAbove()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return operations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OperationsListTVCell", for: indexPath) as! OperationsListTVCell
        cell.operation = operations[(indexPath as NSIndexPath).row]
        cell.reloadData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
