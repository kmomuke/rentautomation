//
//  InventoryVC.swift
//  Rentautomation
//
//  Created by kanybek on 12/16/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

class InventoryVC: UIViewController {

    var initialString: String?
    
    convenience init() {
        self.init(initialString: nil)
    }
    
    init(initialString: String?) {
        self.initialString = initialString
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        let canceButton = UIBarButtonItem(
            title: "Вернутся на Главную",
            style: .plain,
            target: self,
            action: #selector(dismiss(_:))
        )
        
        self.navigationItem.leftBarButtonItem = canceButton
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func dismiss(_ sender: AnyObject) -> Void {
        self.navigationController?.dismiss(animated: true, completion: {[weak self] in
            print("dismiss InventoryVC")
        })
    }
}
