//
//  TransactionsListTVCell.swift
//  Rentautomation
//
//  Created by kanybek on 12/21/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

class TransactionsListTVCell: UITableViewCell {

    var transaction: Transaction?
    
    @IBOutlet weak var transactionPrimaryKeyLabel: UILabel!
    @IBOutlet weak var transactionTypeLabel: UILabel!
    @IBOutlet weak var transactionDateLabel: UILabel!
    @IBOutlet weak var moneyAmountLabel: UILabel!
    @IBOutlet weak var customerOrSupplierLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        transactionTypeLabel.backgroundColor = UIColor.clear
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func reloadData() -> Void {
        if let transaction_ = self.transaction {
            
            if Global.shared.isDebug {
                self.transactionPrimaryKeyLabel.text = "\(transaction_.transactionId)"
                self.transactionPrimaryKeyLabel.backgroundColor = UIColor.clear
            } else {
                self.transactionPrimaryKeyLabel.isHidden = true
            }
            
            transactionTypeLabel.text = Transaction.transactionTypeStringName(transactionType: transaction_.transactionType)
            transactionTypeLabel.textColor = Transaction.transactionTypeColor(transactionType: transaction_.transactionType)
            transactionDateLabel.text = "\(transaction_.transactionDate.toString(.custom("EEE, dd MMM HH:mm:ss")))"
            
            moneyAmountLabel.text = Global.shared.priceFormatter.string(for: transaction_.moneyAmount)
            
            var customerOrSupplierName: String = ""
            if transaction_.customerId > 0 {
                if let customer = Database.shared.customerBy(customerId: transaction_.customerId) {
                    customerOrSupplierName = "\(customer.firstName) \(customer.secondName)"
                }
            } else if transaction_.supplierId > 0 {
                if let supplier =  Database.shared.supplierBy(supplierId: transaction_.supplierId) {
                    customerOrSupplierName = supplier.companyName
                }
            } else {
                customerOrSupplierName = ""
            }
            
            customerOrSupplierLabel.text = customerOrSupplierName
            
            if Database.shared.isModelSynchronized(primaryKey: transaction_.transactionId) {
                self.transactionDateLabel.textColor = UIColor.black
                self.transactionDateLabel.backgroundColor = UIColor.groupTableViewBackground
            } else {
                self.transactionDateLabel.textColor = UIColor.white
                self.transactionDateLabel.backgroundColor = UIColor.pomergranateColor()
            }
            
        } else {
            self.resetLabels()
        }
    }
    
    override func prepareForReuse() {
        self.transaction = nil
        self.resetLabels()
    }
    
    func resetLabels() -> Void {
        self.transactionTypeLabel.text = ""
        self.transactionDateLabel.text = ""
        self.moneyAmountLabel.text = ""
        self.customerOrSupplierLabel.text = ""
        self.transactionPrimaryKeyLabel.text = ""
    }
}
