//
//  TransactionsListVC.swift
//  Rentautomation
//
//  Created by kanybek on 12/16/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

class TransactionsListVC: BaseListVC, ErrorPopoverRenderer, UITableViewDelegate, UITableViewDataSource {

    var tableView: UITableView
    var transacations: [Transaction] = []
    
    var initialString: String?
    
    var enableAboveHistoryRequests: Bool
    var enableBelowHistoryRequests: Bool
    
    convenience init() {
        self.init(initialString: nil)
    }
    
    init(initialString: String?) {
        self.initialString = initialString
        self.tableView = UITableView(frame: CGRect.zero, style: .plain)
        self.enableAboveHistoryRequests = true
        self.enableBelowHistoryRequests = true
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func loadView() {
        super.loadView()
        self.addCanceButton()
        self.title = "Транзакции"
        let nib = UINib(nibName: "TransactionsListTVCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "TransactionsListTVCell")
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.view.addSubview(self.tableView)
        
        let addButton = UIBarButtonItem(
            title: "Фильтры",
            style: .plain,
            target: self,
            action: #selector(filterPressed(_:))
        )
        self.navigationItem.rightBarButtonItem = addButton
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SmartLoader.shared.loadAllTransactions(customerId: 0,
                                               supplierId: 0,
                                               date: Date(),
                                               limit: 50,
                                               completion: {[weak self] (transactions_: [Transaction]) in
                                                    self?.transacations = transactions_
                                                    self?.tableView.reloadData()
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = self.view.bounds
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: filterPressed
    func filterPressed(_ sender: AnyObject) {
        let transactionFilter = TransactionFiltersVC(initialItem: nil)
        self.presentSheet(viewController: transactionFilter)
    }
    
    // MARK: TableView DataSource
    private func insertTransactions(transactions_: [Transaction]) -> Void {
        self.dismissLoading()
        
        if transactions_.count > 0 {
            
            var indicicesToInsert :[IndexPath] = []
            for (index, _) in transactions_.enumerated() {
                let indexPath = IndexPath(row: index + self.transacations.count, section: 0)
                indicicesToInsert.append(indexPath)
            }
            
            self.transacations.insert(contentsOf: transactions_, at: self.transacations.count)
            
            self.tableView.beginUpdates()
            self.tableView.insertRows(at: indicicesToInsert, with: .top)
            self.tableView.endUpdates()
            
            let delay = DispatchTime.now() + DispatchTimeInterval.seconds(1)
            DispatchQueue.main.asyncAfter(deadline: delay, execute: {
                self.enableBelowHistoryRequests = true
            })
            
        } else {
            self.enableBelowHistoryRequests = false
        }
    }
    
    private func loadMoreMessagesAbove() -> Void {
        
    }
    
    private func loadMoreMessagesBelow() -> Void {
        
        if !self.enableBelowHistoryRequests {
            return
        }
        
        if let currentLastTransaction = self.transacations.last {
            self.showLoading("Загружаем")
            self.enableBelowHistoryRequests = false
            
            SmartLoader.shared.loadAllTransactions(customerId: 0,
                                                   supplierId: 0,
                                                   date: currentLastTransaction.transactionDate,
                                                   limit: 50,
                                                   completion: { (transactions_: [Transaction]) in
                                                    self.insertTransactions(transactions_: transactions_)
            })
        }
    }
    
    // MARK: TableView DataSource
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.tableView {
            let flt_epsilon: CGFloat = 0.1
            if scrollView.contentSize.height > flt_epsilon {
                
                if (enableBelowHistoryRequests && scrollView.contentOffset.y > scrollView.contentSize.height - 500 * 2.0) && (scrollView.contentSize.height > flt_epsilon) {
                    print("loadMoreMessagesBelow")
                    self.loadMoreMessagesBelow()
                }
                
                if (enableAboveHistoryRequests && scrollView.contentOffset.y < 60 * 2.0) {
                    print("loadMoreMessagesAbove")
                    self.loadMoreMessagesAbove()
                }
            }
        }
    }
    
    // MARK: TableView DataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.transacations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionsListTVCell", for: indexPath) as! TransactionsListTVCell
        let transaction = transacations[(indexPath as NSIndexPath).row]
        cell.transaction = transaction
        cell.reloadData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let transaction = transacations[(indexPath as NSIndexPath).row]
        if transaction.orderId > 0 {
            let orderDescVC = OrderDescriptionVC(initialOrderId: transaction.orderId)
            self.navigationController?.pushViewController(orderDescVC, animated: true)
        }
    }
}
