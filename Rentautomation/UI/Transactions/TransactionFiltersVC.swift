//
//  TransactionFiltersVC.swift
//  Rentautomation
//
//  Created by kanybek on 12/22/16.
//  Copyright © 2016 kanybek. All rights reserved.
//

import UIKit

class TransactionFiltersVC: UIViewController {

    var initialItem: String?

    convenience init() {
        self.init(initialItem: nil)
    }

    init(initialItem: String?) {
        self.initialItem = initialItem
        super.init(nibName: nil, bundle: nil)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        let canceButton = UIBarButtonItem(
            title: "Закрыть",
            style: .plain,
            target: self,
            action: #selector(cancelObject(_:))
        )
        
        self.navigationItem.leftBarButtonItem = canceButton
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func cancelObject(_ sender: AnyObject) -> Void {
        self.navigationController?.dismiss(animated: true, completion: {
            print("dismiss CreateCustomerVC")
        })
    }
}
