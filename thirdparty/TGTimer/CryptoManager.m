//
//  Crypto.m
//  mobilnik
//
//  Created by kanybek on 8/24/16.
//  Copyright © 2016 Mobilnik. All rights reserved.
//

#import "CryptoManager.h"
#import <CommonCrypto/CommonDigest.h>

NSString *TGEncodedText(NSString *string, int key)
{
    NSMutableString *result = [[NSMutableString alloc] init];
    
    for (int i = 0; i < (int)[string length]; i++)
    {
        unichar c = [string characterAtIndex:i];
        c += key;
        [result appendString:[NSString stringWithCharacters:&c length:1]];
    }
    
    return result;
}

NSString *TGStringMD5(NSString *string)
{
    const char *ptr = [string UTF8String];
    unsigned char md5Buffer[16];
    CC_MD5(ptr, (CC_LONG)[string lengthOfBytesUsingEncoding:NSUTF8StringEncoding], md5Buffer);
    NSString *output = [[NSString alloc] initWithFormat:@"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x", md5Buffer[0], md5Buffer[1], md5Buffer[2], md5Buffer[3], md5Buffer[4], md5Buffer[5], md5Buffer[6], md5Buffer[7], md5Buffer[8], md5Buffer[9], md5Buffer[10], md5Buffer[11], md5Buffer[12], md5Buffer[13], md5Buffer[14], md5Buffer[15]];
    
    return output;
}

@implementation CryptoManager

+ (NSString *)TGEncodedeTEXT:(NSString *)string key:(int)key
{
    return TGEncodedText(string, key);
}

+ (NSString *)TGStringDigestMD5:(NSString *)string
{
    return TGStringMD5(string);
}

+ (NSString *)getPINDigestKey
{
    static NSString *PINDigest;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      PINDigest = TGStringMD5(TGEncodedText(@"`EfTERWtdspmmSbohfUpWjtjcmf;bojnbufe;", -1));
                  });
    return PINDigest;
}

+ (NSString *)getTokenDigestKey
{
    static NSString *token;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      token = TGStringMD5(TGEncodedText(@"`tdspmmSbohfUpWjtjcmf;bojnbufe;", -1));
                  });
    return token;
}

@end
