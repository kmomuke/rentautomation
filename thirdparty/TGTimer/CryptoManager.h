//
//  Crypto.h
//  mobilnik
//
//  Created by kanybek on 8/24/16.
//  Copyright © 2016 Mobilnik. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CryptoManager : NSObject
+ (NSString *)TGEncodedeTEXT:(NSString *)string key:(int)key;
+ (NSString *)TGStringDigestMD5:(NSString *)string;
+ (NSString *)getTokenDigestKey;
+ (NSString *)getPINDigestKey;
@end
