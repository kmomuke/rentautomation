//
//  LoginVC.swift
//  Rentautomation
//
//  Created by kanybek on 3/19/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit
import grpcPod

class LoginVC: FormViewController, ErrorPopoverRenderer {
    
    var createStaffDispose: Disposable?
    
    var email: String
    var password: String
    
    var initialItem: String?
    
    convenience init() {
        self.init(initialItem: nil)
    }
    
    init(initialItem: String?) {
        self.initialItem = initialItem
        self.email = ""
        self.password = ""
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.createStaffDispose?.dispose()
        self.createStaffDispose = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        let signInButton = UIBarButtonItem(
            title: "Войти",
            style: .plain,
            target: self,
            action: #selector(createStaff(_:))
        )
        self.navigationItem.rightBarButtonItem = signInButton
        
        
        let demoButton = UIBarButtonItem(
            title: "Демо версия",
            style: .plain,
            target: self,
            action: #selector(demoStaff(_:))
        )
        self.navigationItem.leftBarButtonItem = demoButton
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: CREATE
    func demoStaff(_ sender: UIBarButtonItem) -> Void {
        let signal = RPCNetwork.shared.signInRPC(email: "test2",
                                                 password: "test2")
        
        let finalUserSignal = signal |> mapToSignal({ (signInReq: SignInRequest) -> Signal<UserRequest, Error> in
            KeychainManager.shared.removeToken()
            let _ = KeychainManager.shared.saveToken(signInReq.token)
            return RPCNetwork.shared.getUserRPC()
        })
        
        let finalSignal = finalUserSignal |> runOn(Queue.mainQueue()) |> deliverOn(Queue.mainQueue())
        
        self.showLoading("Signing in")
        self.createStaffDispose = finalSignal.start(next: { (userReq: UserRequest) in
            self.dismissLoading()
            let user = ProtoToModel.userFrom(userReq)
            user.isCurrentUser = true
            Database.shared.createUser([user])
            
            AppSession.shared._currentEmployee = user
            
            AppSettings.shared.setDefaultSettings()
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.enterMainApplication()
            
        }, error: { (error: Error) in
            self.dismissLoading()
            let _ = self.showErrorAlert(text: error.localizedDescription)
            print("SignIn finished with error")
        }, completed: {
            self.dismissLoading()
            print("finalSignal completed")
        })
    }
    
    func createStaff(_ sender: UIBarButtonItem) -> Void {
        
        self.createStaffDispose?.dispose()
        self.createStaffDispose = nil
        
        if email.characters.count < 2 {
            let _ = showErrorAlert(text: "Введите почту")
            return
        }
        
        if password.characters.count < 2 {
            let _ = showErrorAlert(text: "Введите пароль")
            return
        }
        
        
        let signal = RPCNetwork.shared.signInRPC(email: email,
                                                 password: password)
        
        let finalUserSignal = signal |> mapToSignal({ (signInReq: SignInRequest) -> Signal<UserRequest, Error> in
            KeychainManager.shared.removeToken()
            let _ = KeychainManager.shared.saveToken(signInReq.token)
            return RPCNetwork.shared.getUserRPC()
        })
        
        let finalSignal = finalUserSignal |> runOn(Queue.mainQueue()) |> deliverOn(Queue.mainQueue())
        
        self.showLoading("Signing in")
        self.createStaffDispose = finalSignal.start(next: { (userReq: UserRequest) in
            self.dismissLoading()
            let user = ProtoToModel.userFrom(userReq)
            user.isCurrentUser = true
            Database.shared.createUser([user])
            
            AppSession.shared._currentEmployee = user
            
            AppSettings.shared.setDefaultSettings()
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.enterMainApplication()
            
        }, error: { (error: Error) in
            self.dismissLoading()
            let _ = self.showErrorAlert(text: error.localizedDescription)
            print("SignIn finished with error")
        }, completed: {
            self.dismissLoading()
            print("finalSignal completed")
        })
        
        
        /*let signal = RPCNetwork.shared.signInRPC(email: email, password: password)
        let finalSignal = signal |> runOn(Queue.mainQueue()) |> deliverOn(Queue.mainQueue())
        
        self.showLoading("Signing in")
        self.createStaffDispose = finalSignal.start(next: { (creatingStaff: User) in
            self.dismissLoading()
            creatingStaff.isCurrentStaff = true
            Database.shared.createStaff([creatingStaff])
            AppSession.shared.currentEmployee = creatingStaff
            StaffMemoryInstance.sharedInstance.clearAllVariables()
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.enterMainApplication()
        }, error: { (error: Error) in
            self.dismissLoading()
            let _ = self.showErrorAlert(text: error.localizedDescription)
            print("SignIn finished with error")
        }, completed: {
            self.dismissLoading()
            print("finalSignal completed")
        })*/
    }
    
    // MARK: Private
    private lazy var formerInputAccessoryView: FormerInputAccessoryView = FormerInputAccessoryView(former: self.former)
    
    // MARK: Needed product section
    private lazy var neededProductSection: SectionFormer = { [weak self] in
        
        let emailRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            $0.titleLabel.text = "Почта"
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.returnKeyType = .next
            $0.textField.keyboardType = .emailAddress
            $0.textField.autocapitalizationType = .none
            
            }.configure { [weak self] in
                $0.placeholder = "Почта"
                $0.text = self?.email
                $0.rowHeight = 50
                
            }.onTextChanged { [weak self] in
                self?.email = $0
        }
        
        let passwordRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) { [weak self] in
            $0.titleLabel.text = "Пароль"
            $0.textField.inputAccessoryView = self?.formerInputAccessoryView
            $0.textField.isSecureTextEntry = true
            $0.textField.returnKeyType = .go
            $0.textField.autocapitalizationType = .none
            }.configure { [weak self] in
                $0.placeholder = "Пароль"
                $0.text = self?.password
                $0.rowHeight = 50
            }.onTextChanged { [weak self] in
                self?.password = $0
        }
        
        let section = SectionFormer(rowFormer:emailRow, passwordRow)
        section.set(headerViewFormer: self?.createHeaderFunc(s: ""))
        return section
    }()
    
    private func createHeaderFunc(s: String) -> ViewFormer {
        return LabelViewFormer<FormLabelHeaderView>()
            .configure {
                $0.viewHeight = 40
                $0.text = s
                $0.view.formTitleLabel().backgroundColor = UIColor.clear
                $0.view.contentView.backgroundColor = UIColor.clear
        }
    }
    
    private func configure() {
        
        //let text: UITextField = UITextField(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        //text.isSecureTextEntry = true
        
        tableView.contentInset.top = 40
        tableView.contentInset.bottom = 40
        tableView.backgroundColor = UIColor.white
        self.view.backgroundColor = UIColor.white
        former.append(sectionFormer:self.neededProductSection)
            .onCellSelected { [weak self] _ in
                self?.formerInputAccessoryView.update()
        }
    }
}
