//
//  SuppliersListVC.swift
//  Rentautomation
//
//  Created by kanybek on 3/19/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit

class SuppliersListVC: BaseVC, ErrorPopoverRenderer, UITableViewDelegate, UITableViewDataSource {
    
    private var searchController: UISearchController!
    private var searchResultsVC: SupplierSearchResultsTVC!
    
    var tableView: UITableView
    var suppliers: [Supplier] = []
    
    var supplierSelectBlock : ((Supplier) -> Void)?
    
    var createSupplierDispose: Disposable?
    var updateSupplierDispose: Disposable?
    
    var initialString: String?
    var isCommingFromProduct: Bool = false
    var isComingFromOrder: Bool = false
    
    convenience init() {
        self.init(initialString: nil, isCommingFromProduct: false, isComingFromOrder: false)
    }
    
    init(initialString: String?, isCommingFromProduct: Bool, isComingFromOrder: Bool) {
        self.initialString = initialString
        self.isCommingFromProduct = isCommingFromProduct
        self.isComingFromOrder = isComingFromOrder
        
        self.tableView = UITableView(frame: CGRect.zero, style: .plain)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.searchController?.view.removeFromSuperview()
        self.createSupplierDispose?.dispose()
        self.createSupplierDispose = nil
        self.updateSupplierDispose?.dispose()
        self.updateSupplierDispose = nil
    }
    
    override func loadView() {
        super.loadView()
        
        if (self.isComingFromOrder || self.isCommingFromProduct) {
        } else {
        }
        
        self.title = "Поставщики"
        let nib = UINib(nibName: "SuppliersListTVCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "SuppliersListTVCell")
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.view.addSubview(self.tableView)
                
        self.searchResultsVC = SupplierSearchResultsTVC()
        self.searchResultsVC.supplierSelectBlock = {[weak self]  (supplier: Supplier) -> Void in
            if let strongSelf = self {
                strongSelf.didSelectSupplier(supplier)
            }
        }
        
        self.searchController = UISearchController(searchResultsController: self.searchResultsVC)
        self.searchController.searchResultsUpdater = self
        self.searchController.searchBar.sizeToFit()
        self.tableView.tableHeaderView = searchController.searchBar
        
        self.searchController.delegate = self
        self.searchController.dimsBackgroundDuringPresentation = false // default is YES
        self.searchController.searchBar.delegate = self    // so we can monitor text changes + others
        
        self.searchController.searchBar.barStyle = .default
        self.searchController.searchBar.placeholder = "Search"
        self.searchController.searchBar.backgroundColor = UIColor.white
        
        /*
         Search is now just presenting a view controller. As such, normal view controller
         presentation semantics apply. Namely that presentation will walk up the view controller
         hierarchy until it finds the root view controller or one that defines a presentation context.
         */
        definesPresentationContext = true
        
        let supplierCreateSignal: Signal = PipesStore.shared.supplierCreatedSignal()
        self.createSupplierDispose = supplierCreateSignal.start(next: {[weak self] (supplier: Supplier) in
            self?.suppliers.insert(supplier, at: 0)
            self?.tableView.beginUpdates()
            self?.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
            self?.tableView.endUpdates()
        })
        
        let supplierUpdateSignal: Signal = PipesStore.shared.supplierUpdatedSignal()
        self.updateSupplierDispose = supplierUpdateSignal.start(next: {[weak self] (supplier: Supplier) in
            if let i = self?.suppliers.index(where: {$0.supplierId == supplier.supplierId}) {
                self?.suppliers[i] = supplier
                self?.tableView.reloadRows(at: [IndexPath(row: i, section: 0)], with: .fade)
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Database.shared.loadAllSuppliers(limit: Global.shared.limitForStaticModels) {[weak self] (suppliers_: [Supplier]) in
            self?.suppliers = suppliers_
            self?.tableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = self.view.bounds
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func createSupplier(_ sender: AnyObject) {
    }
    
    // MARK: TableView DataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return suppliers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SuppliersListTVCell", for: indexPath) as! SuppliersListTVCell
        let object = suppliers[(indexPath as NSIndexPath).row]
        cell.supplier = object
        cell.reloadData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            suppliers.remove(at: (indexPath as NSIndexPath).row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let supplier = suppliers[(indexPath as NSIndexPath).row]
        self.didSelectSupplier(supplier)
    }
    
    func didSelectSupplier(_ supplier: Supplier) {
        if Database.shared.isModelSynchronized(primaryKey: supplier.supplierId) {            
            
            
            if let initialString_ = self.initialString {
                if initialString_ == "comeFromSaleOrder" {
                    if let supplierSelectBlock_ = self.supplierSelectBlock {
                        supplierSelectBlock_(supplier)
                        let _ = self.navigationController?.popViewController(animated: true)
                    }
                    return
                }
            }
            
            
            if isCommingFromProduct {
                ProductMemoryInstance.sharedInstance.supplierId = supplier.supplierId
                OrderFilterMemory.shared.supplierId = supplier.supplierId
                let _ = self.navigationController?.popViewController(animated: true)
            } else if isComingFromOrder {
                OrderMemomry.shared.supplierId = supplier.supplierId
                OrderFilterMemory.shared.supplierId = supplier.supplierId
                let _ = self.navigationController?.popViewController(animated: true)
            } else {
                let supplierDescrVC = SupplierDescriptionVC(initialSupplier: supplier)
                self.navigationController?.pushViewController(supplierDescrVC, animated: true)
            }
        } else {
            let _ = self.showErrorAlert(text: "Не сохранен")
        }
    }
}

extension SuppliersListVC: UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating {
    
    //----------
    func presentSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    func willPresentSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    func didPresentSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    func willDismissSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    func didDismissSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    // MARK: UISearchResultsUpdating
    func updateSearchResults(for searchController: UISearchController) {
        print("updateSearchResultsForSearchController")
        if let searchText = searchController.searchBar.text {
            if searchText.characters.count > 0 {
                self.showLoading("Поиск")
                Database.shared.searchSupplierBy(name: searchText, completion: {[weak self] (foundSuppliers: [Supplier]) in
                    self?.dismissLoading()
                    let resultsController = searchController.searchResultsController as! SupplierSearchResultsTVC
                    resultsController.filteredSuppliers = foundSuppliers
                    resultsController.tableView.reloadData()
                })
            }
        }
    }
    
}
