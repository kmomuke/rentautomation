//
//  SupplierDescriptionVC.swift
//  Rentautomation
//
//  Created by kanybek on 3/24/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit

class SupplierDescriptionVC: BaseVC, ErrorPopoverRenderer {
    
    @IBOutlet weak var currentSupplierBalanceLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var supplierNameLabel: UILabel!
    @IBOutlet weak var supplierPhoneButton: UIButton!
    @IBOutlet weak var supplierAddressLabel: UILabel!
    
    var isFirstTime: Bool = true
    var initialSupplier: Supplier?
    var updateSupplierDispose: Disposable?
    var balanceUpdateSupplierDispose: Disposable?
    
    private lazy var moneyOperationsVC: CustomerMoneyOperationsVC = { [weak self] in
        let moneyOperationsVC_ = CustomerMoneyOperationsVC(initialCustomer: nil, initialSupplier: self?.initialSupplier)
        return moneyOperationsVC_
    }()
    
    convenience init() {
        self.init(initialSupplier: nil)
    }
    
    init(initialSupplier: Supplier?) {
        self.initialSupplier = initialSupplier
        super.init(nibName: "SupplierDescriptionVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.updateSupplierDispose?.dispose()
        self.updateSupplierDispose = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let currentSupplier = self.initialSupplier {
            if let account_ = Database.shared.accountBy(supplierId: currentSupplier.supplierId) {
                self.currentSupplierBalanceLabel.text = Global.shared.numberFormatter.string(for: account_.balance)
            }
        }
        
        let supplierUpdateSignal: Signal = PipesStore.shared.supplierUpdatedSignal()
        self.updateSupplierDispose = supplierUpdateSignal.start(next: {[weak self] (supplier: Supplier) in
            if let currentSupplier = self?.initialSupplier {
                if currentSupplier.supplierId == supplier.supplierId {
                    self?.initialSupplier = supplier
                    self?.updateUI()
                }
            }
        })
        
        let balanceUpdatedSignal: Signal = PipesStore.shared.balanceUpdatedSignal()
        self.balanceUpdateSupplierDispose = balanceUpdatedSignal.start(next: {[weak self] (supplierId: UInt64) in
            
            if let currentSupplier = self?.initialSupplier {
                if currentSupplier.supplierId == supplierId {
                    if let account_ = Database.shared.accountBy(supplierId: supplierId) {
                        self?.currentSupplierBalanceLabel.text = Global.shared.numberFormatter.string(for: account_.balance)
                        self?.moneyOperationsVC.reloadOperations()
                    }
                }
            }
        })
        
        self.updateUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (isFirstTime) {
            self.containerView.backgroundColor = UIColor.white
            self.add(asChildViewController: self.moneyOperationsVC, frame_: self.containerView.frame)
            isFirstTime = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func updateUI() -> Void {
        if let initalSupplier_ = self.initialSupplier {
            self.title = initalSupplier_.companyName
            self.supplierNameLabel.text = "\(initalSupplier_.companyName)"
            self.supplierPhoneButton.setTitle(initalSupplier_.phoneNumber, for: .normal)
            self.supplierAddressLabel.text = initalSupplier_.address
        }
    }
    
    func editSupplier(_ sender: AnyObject) -> Void {
    }
    
    @IBAction func phoneDidPressed(_ sender: Any) {
        if let initalSupplier_ = self.initialSupplier {
            let aString = initalSupplier_.phoneNumber
            let newString = aString.replacingOccurrences(of: "-", with: "")
            if let url = URL(string: "tel://\(newString)") {
                UIApplication.shared.openURL(url)
            }
        }
    }
    @IBAction func setSupplierBalanceDidPressed(_ sender: Any) {
    }
}
