//
//  StaffListVC.swift
//  Rentautomation
//
//  Created by kanybek on 3/19/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit

class StaffListVC: BaseVC, ErrorPopoverRenderer, UITableViewDelegate, UITableViewDataSource {
    
    private var searchController: UISearchController!
    private var searchResultsVC: StaffSearchResultsTVC!
    
    var staffSelectBlock : ((User) -> Void)?
    
    var tableView: UITableView
    
    lazy var staff: [User] = { [weak self] in
        let staff_ = Database.shared.loadAllUsers()
        return staff_
    }()
    
    var createStaffDispose: Disposable?
    var updateStaffDispose: Disposable?
    
    var initialString: String?
    var isCommingFromCustomer: Bool = false
    
    convenience init() {
        self.init(initialString: nil, isCommingFromCustomer: false)
    }
    
    init(initialString: String?, isCommingFromCustomer: Bool) {
        self.initialString = initialString
        self.isCommingFromCustomer = isCommingFromCustomer
        self.tableView = UITableView(frame: CGRect.zero, style: .plain)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.searchController?.view.removeFromSuperview()
        self.createStaffDispose?.dispose()
        self.createStaffDispose = nil
        self.updateStaffDispose?.dispose()
        self.updateStaffDispose = nil
    }
    
    override func loadView() {
        super.loadView()
        if !self.isCommingFromCustomer {
        }
        self.title = "Сотрудники"
        let nib = UINib(nibName: "StaffListTVCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "StaffListTVCell")
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.view.addSubview(self.tableView)
        
        self.searchResultsVC = StaffSearchResultsTVC()
        self.searchResultsVC.staffSelectBlock = {[weak self]  (employee: User) -> Void in
            if let strongSelf = self {
                strongSelf.didSelectStaff(employee)
            }
        }
        
        self.searchController = UISearchController(searchResultsController: self.searchResultsVC)
        self.searchController.searchResultsUpdater = self
        self.searchController.searchBar.sizeToFit()
        self.tableView.tableHeaderView = searchController.searchBar
        
        self.searchController.delegate = self
        self.searchController.dimsBackgroundDuringPresentation = false // default is YES
        self.searchController.searchBar.delegate = self    // so we can monitor text changes + others
        
        self.searchController.searchBar.barStyle = .default
        self.searchController.searchBar.placeholder = "Search"
        self.searchController.searchBar.backgroundColor = UIColor.white
        
        /*
         Search is now just presenting a view controller. As such, normal view controller
         presentation semantics apply. Namely that presentation will walk up the view controller
         hierarchy until it finds the root view controller or one that defines a presentation context.
         */
        definesPresentationContext = true
        
        let staffCreateSignal: Signal = PipesStore.shared.staffCreatedSignal()
        self.createStaffDispose = staffCreateSignal.start(next: {[weak self] (employee: User) in
            self?.staff.insert(employee, at: 0)
            self?.tableView.beginUpdates()
            self?.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
            self?.tableView.endUpdates()
        })
        
        let staffUpdateSignal: Signal = PipesStore.shared.staffUpdatedSignal()
        self.updateStaffDispose = staffUpdateSignal.start(next: {[weak self] (employee: User) in
            if let i = self?.staff.index(where: {$0.userId == employee.userId}) {
                self?.staff[i] = employee
                self?.tableView.reloadRows(at: [IndexPath(row: i, section: 0)], with: .fade)
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = self.view.bounds
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func createStaff(_ sender: AnyObject) {
    }
    
    func dismissCategory(_ sender: AnyObject) {
    }
    
    // MARK: TableView DataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return staff.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "StaffListTVCell", for: indexPath) as! StaffListTVCell
        let object = staff[(indexPath as NSIndexPath).row]
        cell.staff = object
        cell.reloadData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            staff.remove(at: (indexPath as NSIndexPath).row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let employee = staff[(indexPath as NSIndexPath).row]
        self.didSelectStaff(employee)
    }
    
    func didSelectStaff(_ employee: User) {
        if Database.shared.isModelSynchronized(primaryKey: employee.userId) {
            if self.isCommingFromCustomer {
                CustomerMemoryInstance.sharedInstance.staffId = employee.userId
                OrderFilterMemory.shared.staffId = employee.userId
                OrderMemomry.shared.staffId = employee.userId
                
                let _ = self.navigationController?.popViewController(animated: true)
                if let staffSelectBlock_ = self.staffSelectBlock {
                    staffSelectBlock_(employee)
                    self.dismiss(animated: true, completion: {
                        print("StaffListVC dismissed")
                    })
                }
            } else {
                //let detailViewController = StaffDescriptionVC(initialStaff: employee)
                //self.navigationController?.pushViewController(detailViewController, animated: true)
            }
        } else {
            let _ = self.showErrorAlert(text: "Не сохранен")
        }
    }
    
}

extension StaffListVC: UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating {
    
    //----------
    func presentSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    func willPresentSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    func didPresentSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    func willDismissSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    func didDismissSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    // MARK: UISearchResultsUpdating
    func updateSearchResults(for searchController: UISearchController) {
        print("updateSearchResultsForSearchController")
        if let searchText = searchController.searchBar.text {
            if searchText.characters.count > 0 {
                self.showLoading("Поиск")
                Database.shared.searchUserBy(name: searchText, completion: {[weak self] (foundStaff: [User]) in
                    self?.dismissLoading()
                    let resultsController = searchController.searchResultsController as! StaffSearchResultsTVC
                    resultsController.filteredStaff = foundStaff
                    resultsController.tableView.reloadData()
                })
            }
        }
    }
    
}
