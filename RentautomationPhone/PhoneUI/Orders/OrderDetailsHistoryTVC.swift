//
//  OrderDetailsHistoryTVC.swift
//  Rentautomation
//
//  Created by kanybek on 3/24/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class OrderDetailsHistoryTVC: UITableViewController {
    
    var initialOrder: Order?
    var selectedProducts: [OrderDetail] = []
    
    convenience init() {
        self.init(initialOrder: nil)
    }
    
    init(initialOrder: Order?) {
        self.initialOrder = initialOrder
        if let initialOrder_ = self.initialOrder {
            let orderDetails = Database.shared.orderDetailsBy(orderId: initialOrder_.orderId)
            self.selectedProducts = orderDetails
        }
        super.init(nibName: "OrderDetailsHistoryTVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "OrderDetailsHistoryTVCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "OrderDetailsHistoryTVCell")
        self.tableView.alwaysBounceVertical = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedProducts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailsHistoryTVCell", for: indexPath) as! OrderDetailsHistoryTVCell
        let orderDetail = selectedProducts[(indexPath as NSIndexPath).row]
        cell.orderDetail = orderDetail
        cell.reloadData()
        return cell
    }
}
