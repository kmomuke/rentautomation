//
//  OrderDescriptionVC.swift
//  Rentautomation
//
//  Created by kanybek on 3/24/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class OrderDescriptionVC: BaseVC, ErrorPopoverRenderer {
    
    var initialOrder: Order?
    var isFirstTime: Bool = true
    var selectTVC: OrderDetailsHistoryTVC?
    
    @IBOutlet weak var orderEdittedStatusLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var staffLabel: UILabel!
    @IBOutlet weak var customerSupplierLabel: UILabel!
    @IBOutlet weak var documentTypeLabel: UILabel!
    @IBOutlet weak var moneyMovementTypeLabel: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var totalSumLabel: UILabel!
    @IBOutlet weak var totalSumWithDiscountLabel: UILabel!
    @IBOutlet weak var orderDetailsView: UIView!
    
    convenience init() {
        self.init(initialOrderId: 0)
    }
    
    init(initialOrderId: UInt64) {
        self.initialOrder = Database.shared.orderBy(orderId: initialOrderId)
        super.init(nibName: "OrderDescriptionVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let generateButton = UIBarButtonItem(
            title: "CSV",
            style: .plain,
            target: self,
            action: #selector(generatePressed(_:))
        )
        self.navigationItem.rightBarButtonItem = generateButton
        
        self.updateUI()
        self.selectTVC = OrderDetailsHistoryTVC(initialOrder: self.initialOrder)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (isFirstTime) {
            self.add(asChildViewController: self.selectTVC!, frame_: self.orderDetailsView.frame)
            isFirstTime = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func updateUI() -> Void {
        if let order_ = self.initialOrder {
            
            self.title = order_.billingNo
            
            if order_.isEdited {
                self.orderEdittedStatusLabel.text = "Документ был откатан назад"
            } else {
                self.orderEdittedStatusLabel.isHidden = true
            }
            
            self.numberLabel.text = "\(order_.orderId)"
            self.dateLabel.text = "\(order_.orderDate.toString(.custom("EEE, dd MMM HH:mm")))"
            
            var staffName: String = ""
            if order_.userId > 0 {
                if let employee = Database.shared.userBy(userId: order_.userId) {
                    staffName = "\(employee.firstName) \(employee.secondName)"
                }
            }
            self.staffLabel.text = staffName
            
            var customerOrSupplierName: String = ""
            if order_.customerId > 0 {
                if let customer = Database.shared.customerBy(customerId: order_.customerId) {
                    customerOrSupplierName = "\(customer.firstName) \(customer.secondName)"
                }
            } else if order_.supplierId > 0 {
                if let supplier = Database.shared.supplierBy(supplierId: order_.supplierId) {
                    customerOrSupplierName = "\(supplier.companyName)"
                }
            }
            self.customerSupplierLabel.text = customerOrSupplierName
            
            self.documentTypeLabel.text = Order.orderDocumentTypeStringName(orderDocType: order_.orderDocument)
            self.documentTypeLabel.textColor = Order.orderDocumentTypeColor(orderDocType: order_.orderDocument)
            
            self.moneyMovementTypeLabel.text = Order.moneyMovementTypeStringName(moneyType: order_.moneyMovementType)
            
            var discount: Double = 0.0
            var totalSum: Double = 0.0
            var totalSumWithDiscount: Double = 0.0
            if let payment = Database.shared.paymentBy(paymentId: order_.paymentId) {
                discount = payment.discount
                totalSum = payment.totalOrderPrice
                totalSumWithDiscount = payment.totalPriceWithDiscountF()
            }
            
            self.discountLabel.text = "\(discount)%"
            self.totalSumLabel.text = Global.shared.numberFormatter.string(for: totalSum)
            self.totalSumWithDiscountLabel.text = Global.shared.numberFormatter.string(for: totalSumWithDiscount)
        }
    }
    
    // MARK: - Helper Methods
    func editOrderDetails(_ sender: AnyObject) -> Void {
    }
    
    @IBAction func getTextFromOrderDidPressed(_ sender: UIButton) {
        //let vc = TextFromOrderVC(initialItem: self.initialOrder?.comment)
        //let vc = DocumentCommentVC(initialOrder: self.initialOrder)
        //self.presentSheet(viewController: vc)
    }
    
    func generatePressed(_ sender: UIBarButtonItem) {
        
        if let order = self.initialOrder {
            let alert: UIAlertController = UIAlertController(title: "Сгенерировать CSV",
                                                             message: "Вы действительно хотите сгенерировать CSV?",
                                                             preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Сгенерировать", style: .default, handler: {[weak self]
                (UIAlertAction) -> Void in
                if let stringSelf = self {
                    
                    stringSelf.showLoading("Генерация CSV...")
                    CSVGenerator.shared.generateCSVFrom(orders: [order],
                                                        progress: {[weak self] (fraction: Float) in
                                                            if let strongSelf = self {
                                                                strongSelf.showProgress(progress: fraction, text: "Генерация CSV")
                                                            }
                                                            
                        }, completion: {[weak self] (url: URL) in
                            
                            if let strongSelf = self {
                                strongSelf.dismissLoading()
                                
                                let activityViewController = UIActivityViewController(activityItems: [url] , applicationActivities: nil)
                                if activityViewController.responds(to: #selector(getter: UIViewController.popoverPresentationController)) {
                                    activityViewController.popoverPresentationController?.barButtonItem = sender
                                }
                                
                                strongSelf.present(activityViewController, animated: true, completion: {
                                })
                            }
                    })
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
}
