//
//  OrderListTVCell.swift
//  Rentautomation
//
//  Created by kanybek on 3/24/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class OrderListTVCell: UITableViewCell {

    var order: Order?
    
    @IBOutlet weak var numberCountLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var orderTypeLabel: UILabel!
    @IBOutlet weak var customerSupplierLabel: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var totalSumLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func reloadData() -> Void {
        //-----
        
        if let order_ = self.order {
            
            //if Global.shared.isDebug {
            //    self.orderPrimaryKeyLabel.text = "\(order_.orderId)"
            //    self.orderPrimaryKeyLabel.backgroundColor = UIColor.clear
            //} else {
            //    self.orderPrimaryKeyLabel.isHidden = true
            //}
            
            self.numberCountLabel.text = "\(order_.orderId)"
            self.dateLabel.text = "\(order_.orderDate.toString(.custom("EEE, dd MMM HH:mm:ss")))"
            self.orderTypeLabel.text = Order.orderDocumentTypeStringName(orderDocType: order_.orderDocument)
            
            var orderDetailCount: String = ""
            if order_.orderId > 0 {
                let totalCount = Database.shared.countOrderDetailsBy(orderId: order_.orderId)
                if totalCount > 0 {
                    orderDetailCount = ":\(totalCount)-товара"
                }
            }
            
            var customerOrSupplierName: String = ""
            if order_.customerId > 0 {
                if let customer = Database.shared.customerBy(customerId: order_.customerId) {
                    customerOrSupplierName = "\(customer.firstName) \(customer.secondName)"
                }
            } else if order_.supplierId > 0 {
                if let supplier = Database.shared.supplierBy(supplierId: order_.supplierId) {
                    customerOrSupplierName = "\(supplier.companyName)"
                }
            } else {
                customerOrSupplierName = Order.moneyMovementTypeStringName(moneyType: order_.moneyMovementType)
            }
            
            customerOrSupplierName = customerOrSupplierName + orderDetailCount
            
            //if let employee_ = Database.shared.staffBy(staffId: order_.staffId) {
            //    customerOrSupplierName = customerOrSupplierName + "\n" + employee_.firstName + " " + employee_.secondName
            //}
            
            if order_.isEdited {
                self.customerSupplierLabel.text = customerOrSupplierName + " документ редактирован"
                self.customerSupplierLabel.textColor = UIColor.red
                self.customerSupplierLabel.backgroundColor = UIColor.clear
            } else {
                self.customerSupplierLabel.text = customerOrSupplierName
                self.customerSupplierLabel.backgroundColor = UIColor.clear
                self.customerSupplierLabel.textColor = UIColor.midnightBlueColor()
            }
            
            var discount: Double = 0.0
            var totalSum: Double = 0.0
            if let payment = Database.shared.paymentBy(paymentId: order_.paymentId) {
                discount = payment.discount
                totalSum = payment.totalPriceWithDiscount
            }
            self.discountLabel.text = "\(discount)%"
            self.totalSumLabel.text = Global.shared.numberFormatter.string(for: totalSum)
            self.orderTypeLabel.textColor = Order.orderDocumentTypeColor(orderDocType: order_.orderDocument)
            self.orderTypeLabel.backgroundColor = UIColor.white
            
            if Database.shared.isModelSynchronized(primaryKey: order_.orderId) {
                self.numberCountLabel.textColor = UIColor.lightGray
                self.numberCountLabel.backgroundColor = UIColor.clear
            } else {
                self.numberCountLabel.textColor = UIColor.white
                self.numberCountLabel.backgroundColor = UIColor.pomergranateColor()
            }
            
        } else {
            self.resetLabels()
        }
    }
    
    override func prepareForReuse() {
        self.order = nil
        self.resetLabels()
    }
    
    func resetLabels() -> Void {
        self.numberCountLabel.text = ""
        self.dateLabel.text = ""
        self.orderTypeLabel.text = ""
        self.customerSupplierLabel.text = ""
        self.discountLabel.text = ""
        self.totalSumLabel.text = ""
    }
}
