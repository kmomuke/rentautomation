//
//  OrderDetailsHistoryTVCell.swift
//  Rentautomation
//
//  Created by kanybek on 3/24/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class OrderDetailsHistoryTVCell: UITableViewCell {

    // -----
    
    @IBOutlet weak var orderDetailPrimaryKeyLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    //@IBOutlet weak var discountLabel: UILabel!
    //@IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var totalAmountWithDiscountLabel: UILabel!
    
    var orderDetail: OrderDetail?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.accessoryType = .none
        self.editingAccessoryType = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func reloadData() -> Void {
        if let orderDetail_ = self.orderDetail {
            if Global.shared.isDebug {
                self.orderDetailPrimaryKeyLabel.text = "\(orderDetail_.orderDetailId)"
                self.orderDetailPrimaryKeyLabel.backgroundColor = UIColor.clear
            } else {
                self.orderDetailPrimaryKeyLabel.isHidden = true
            }
            
            var productName: String = ""
            var quantityPerUnit: String = ""
            if let product = Database.shared.productFor(productId: orderDetail_.productId) {
                productName = product.productName
                quantityPerUnit = product.quantityPerUnit
            }
            self.nameLabel.text = "\(productName)"
            self.priceLabel.text = Global.shared.numberFormatter.string(for: orderDetail_.price)
            
            var orderQuantity: String = ""
            if let text_ = Global.shared.numberFormatterForAmount.string(for: orderDetail_.orderQuantity) {
                orderQuantity = text_
            }
            self.quantityLabel.text = "\(orderQuantity) \(quantityPerUnit)"
            
            //let totalPrice = orderDetail_.price * orderDetail_.orderQuantity
            //self.totalAmountLabel.text = Global.shared.numberFormatter.string(for: totalPrice)
            
            let totalPriceWithDiscount = orderDetail_.price * orderDetail_.orderQuantity
            self.totalAmountWithDiscountLabel.text =  Global.shared.numberFormatter.string(for: totalPriceWithDiscount)
            
            //self.discountLabel.text = "\(orderDetail_.discount)%"
            
            if Database.shared.isModelSynchronized(primaryKey: orderDetail_.orderDetailId) {
                self.nameLabel.textColor = UIColor.black
                self.nameLabel.backgroundColor = UIColor.clear
            } else {
                self.nameLabel.textColor = UIColor.white
                self.nameLabel.backgroundColor = UIColor.pomergranateColor()
            }
            
        } else {
            self.resetLabels()
        }
    }
    
    override func prepareForReuse() {
        self.orderDetail = nil
        self.resetLabels()
    }
    
    func resetLabels() -> Void {
        self.nameLabel.text = ""
        self.priceLabel.text = ""
        self.quantityLabel.text = ""
        //self.discountLabel.text = ""
        //self.totalAmountLabel.text = ""
        self.totalAmountWithDiscountLabel.text = ""
        self.orderDetailPrimaryKeyLabel.text = ""
    }
    
}
