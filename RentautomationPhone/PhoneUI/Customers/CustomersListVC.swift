//
//  CustomersListVC.swift
//  Rentautomation
//
//  Created by kanybek on 3/19/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit

class CustomersListVC: BaseVC, ErrorPopoverRenderer, UITableViewDelegate, UITableViewDataSource {
    
    private var searchController: UISearchController!
    private var searchResultsVC: CustomerSearchResultsTVC!
    
    var tableView: UITableView
    var customers: [Customer] = []
    
    var createCustomerDispose: Disposable?
    var updateCustomerDispose: Disposable?
    
    var initialString: String?
    var isComingFromOrder: Bool = false
    
    convenience init() {
        self.init(initialString: nil, isComingFromOrder: false)
    }
    
    init(initialString: String?, isComingFromOrder: Bool) {
        self.initialString = initialString
        self.isComingFromOrder = isComingFromOrder
        self.tableView = UITableView(frame: CGRect.zero, style: .plain)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.searchController?.view.removeFromSuperview()
        self.createCustomerDispose?.dispose()
        self.createCustomerDispose = nil
        self.updateCustomerDispose?.dispose()
        self.updateCustomerDispose = nil
    }
    
    override func loadView() {
        super.loadView()
        
        if !self.isComingFromOrder {
        }
        
        self.title = "Клиенты"
        let nib = UINib(nibName: "CustomersListTVCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "CustomersListTVCell")
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.view.addSubview(self.tableView)
        
        self.searchResultsVC = CustomerSearchResultsTVC()
        self.searchResultsVC.customerSelectBlock = {[weak self]  (customer: Customer) -> Void in
            if let strongSelf = self {
                strongSelf.didSelectCustomer(customer)
            }
        }
        
        self.searchController = UISearchController(searchResultsController: self.searchResultsVC)
        self.searchController.searchResultsUpdater = self
        self.searchController.searchBar.sizeToFit()
        self.tableView.tableHeaderView = searchController.searchBar
        
        self.searchController.delegate = self
        self.searchController.dimsBackgroundDuringPresentation = false // default is YES
        self.searchController.searchBar.delegate = self    // so we can monitor text changes + others
        
        self.searchController.searchBar.barStyle = .default
        self.searchController.searchBar.placeholder = "Search"
        self.searchController.searchBar.backgroundColor = UIColor.white
        
        /*
         Search is now just presenting a view controller. As such, normal view controller
         presentation semantics apply. Namely that presentation will walk up the view controller
         hierarchy until it finds the root view controller or one that defines a presentation context.
         */
        definesPresentationContext = true
        
        let customerCreateSignal: Signal = PipesStore.shared.customerCreatedSignal()
        self.createCustomerDispose = customerCreateSignal.start(next: {[weak self] (customer: Customer) in
            self?.customers.insert(customer, at: 0)
            self?.tableView.beginUpdates()
            self?.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
            self?.tableView.endUpdates()
        })
        
        let customerUpdateSignal: Signal = PipesStore.shared.customerUpdatedSignal()
        self.updateCustomerDispose = customerUpdateSignal.start(next: {[weak self] (customer: Customer) in
            if let i = self?.customers.index(where: {$0.customerId == customer.customerId}) {
                self?.customers[i] = customer
                self?.tableView.reloadRows(at: [IndexPath(row: i, section: 0)], with: .fade)
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Database.shared.loadAllCustomers(limit: Global.shared.limitForStaticModels, userId: 0) {[weak self] (customers_: [Customer]) in
            self?.customers = customers_
            self?.tableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //tableView.frame = self.view.bounds
        
        tableView.frame = CGRect(x: self.view.bounds.minX,
                                 y: self.view.bounds.minY,
                                 width: self.view.bounds.size.width,
                                 height: self.view.bounds.size.height)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func createCustomer(_ sender: AnyObject) {
    }
    
    // MARK: TableView DataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return customers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomersListTVCell", for: indexPath) as! CustomersListTVCell
        let object = customers[(indexPath as NSIndexPath).row]
        cell.customer = object
        cell.reloadData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            customers.remove(at: (indexPath as NSIndexPath).row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        let customer = customers[(indexPath as NSIndexPath).row]
        self.didSelectCustomer(customer)
    }
    
    func didSelectCustomer(_ customer: Customer) {
        
        if Database.shared.isModelSynchronized(primaryKey: customer.customerId) {
            
            if isComingFromOrder {
                OrderMemomry.shared.customerId = customer.customerId
                OrderFilterMemory.shared.customerId = customer.customerId
                let _ = self.navigationController?.popViewController(animated: true)
            } else {
                let detailViewController = CustomerDescriptionVC(initialCustomer: customer)
                self.navigationController?.pushViewController(detailViewController, animated: true)
            }
            
        } else {
            let _ = self.showErrorAlert(text: "не сохранен")
        }
    }
    
}

extension CustomersListVC:  UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating {
    //----------
    func presentSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    func willPresentSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    func didPresentSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    func willDismissSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    func didDismissSearchController(searchController: UISearchController) {
        //debugPrint("UISearchControllerDelegate invoked method: \(__FUNCTION__).")
    }
    
    // MARK: UISearchResultsUpdating
    func updateSearchResults(for searchController: UISearchController) {
        print("updateSearchResultsForSearchController")
        if let searchText = searchController.searchBar.text {
            if searchText.characters.count > 0 {
                
                self.showLoading("Поиск")
                Database.shared.searchCustomerBy(name: searchText, limit: 100, completion: { (foundCustomers: [Customer]) in
                    self.dismissLoading()
                    let resultsController = searchController.searchResultsController as! CustomerSearchResultsTVC
                    resultsController.filteredCustomers = foundCustomers
                    resultsController.tableView.reloadData()
                })
            }
        }
    }
}

