//
//  CustomerMoneyOperationsVC.swift
//  Rentautomation
//
//  Created by kanybek on 3/25/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit

class CustomerMoneyOperationsVC: UIViewController, ErrorPopoverRenderer, UITableViewDelegate, UITableViewDataSource {
    
    var initialCustomer: Customer?
    var initialSupplier: Supplier?
    var transacations: [Transaction] = []
    
    var disposeSet: DisposableSet?
    
    var enableAboveHistoryRequests: Bool
    var enableBelowHistoryRequests: Bool
    
    @IBOutlet weak var tableView: UITableView!
    
    convenience init() {
        self.init(initialCustomer: nil, initialSupplier: nil)
    }
    
    init(initialCustomer: Customer?, initialSupplier: Supplier?) {
        self.initialCustomer = initialCustomer
        self.initialSupplier = initialSupplier
        self.enableAboveHistoryRequests = true
        self.enableBelowHistoryRequests = true
        self.disposeSet = DisposableSet()
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.disposeSet?.dispose()
        self.disposeSet = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        let nib = UINib(nibName: "CustomerOperationsTVCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "CustomerOperationsTVCell")
        
        let allSyncFinishedSignal: Signal = PipesStore.shared.allSyncOperationsFinishedSignal()
        let allSyncFinishedDispose = allSyncFinishedSignal.start(next: {[weak self] (finished: Bool) in
            self?.reloadOperations()
        })
        self.disposeSet?.add(allSyncFinishedDispose)
        
        self.reloadOperations()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func reloadOperations() -> Void {
        if let customer_ = self.initialCustomer {
            SmartLoader.shared.loadAllTransactions(customerId: customer_.customerId,
                                                   supplierId: 0,
                                                   date: Date(),
                                                   limit: 20,
                                                   completion: {[weak self] (transactions_: [Transaction]) in
                                                    self?.transacations = transactions_
                                                    self?.tableView.reloadData()
            })
        } else if let supplier_ = self.initialSupplier {
            SmartLoader.shared.loadAllTransactions(customerId: 0,
                                                   supplierId: supplier_.supplierId,
                                                   date: Date(),
                                                   limit: 20,
                                                   completion: {[weak self] (transactions_: [Transaction]) in
                                                    self?.transacations = transactions_
                                                    self?.tableView.reloadData()
            })
        }
    }
    
    // MARK: TableView DataSource
    private func insertTransactions(transactions_: [Transaction]) -> Void {
        self.dismissLoading()
        
        if transactions_.count > 0 {
            
            var indicicesToInsert :[IndexPath] = []
            for (index, _) in transactions_.enumerated() {
                let indexPath = IndexPath(row: index + self.transacations.count, section: 0)
                indicicesToInsert.append(indexPath)
            }
            
            self.transacations.insert(contentsOf: transactions_, at: self.transacations.count)
            
            self.tableView.beginUpdates()
            self.tableView.insertRows(at: indicicesToInsert, with: .top)
            self.tableView.endUpdates()
            
            let delay = DispatchTime.now() + DispatchTimeInterval.seconds(1)
            DispatchQueue.main.asyncAfter(deadline: delay, execute: {
                self.enableBelowHistoryRequests = true
            })
            
        } else {
            self.enableBelowHistoryRequests = false
        }
    }
    
    private func loadMoreMessagesAbove() -> Void {
        
    }
    
    private func loadMoreMessagesBelow() -> Void {
        
        if !self.enableBelowHistoryRequests {
            return
        }
        
        if let currentLastTransaction = self.transacations.last {
            
            if currentLastTransaction.isLastTransaction {
                self.enableBelowHistoryRequests = false
            } else {
                self.showLoading("Загружаем")
                self.enableBelowHistoryRequests = false
                
                if let customer_ = self.initialCustomer {
                    SmartLoader.shared.loadAllTransactions(customerId: customer_.customerId,
                                                           supplierId: 0,
                                                           date: currentLastTransaction.transactionDate,
                                                           limit: 20,
                                                           completion: { (transactions_: [Transaction]) in
                                                            self.insertTransactions(transactions_: transactions_)
                    })
                } else if let supplier_ = self.initialSupplier {
                    SmartLoader.shared.loadAllTransactions(customerId: 0,
                                                           supplierId: supplier_.supplierId,
                                                           date: currentLastTransaction.transactionDate,
                                                           limit: 20,
                                                           completion: { (transactions_: [Transaction]) in
                                                            self.insertTransactions(transactions_: transactions_)
                    })
                }
            }
        }
    }
    
    // MARK: TableView DataSource
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.tableView {
            let flt_epsilon: CGFloat = 0.1
            if scrollView.contentSize.height > flt_epsilon {
                
                if (enableBelowHistoryRequests && scrollView.contentOffset.y > scrollView.contentSize.height - 500 * 2.0) && (scrollView.contentSize.height > flt_epsilon) {
                    print("loadMoreMessagesBelow")
                    self.loadMoreMessagesBelow()
                }
                
                if (enableAboveHistoryRequests && scrollView.contentOffset.y < 60 * 2.0) {
                    print("loadMoreMessagesAbove")
                    self.loadMoreMessagesAbove()
                }
            }
        }
    }
    
    // MARK: TableView DataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transacations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomerOperationsTVCell", for: indexPath) as! CustomerOperationsTVCell
        let transaction = transacations[(indexPath as NSIndexPath).row]
        cell.transaction = transaction
        cell.reloadData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let transaction = transacations[(indexPath as NSIndexPath).row]
        if transaction.orderId > 0 {
            let orderDescVC = OrderDescriptionVC(initialOrderId: transaction.orderId)
            self.navigationController?.pushViewController(orderDescVC, animated: true)
        } else {
            let transactionComment = TransactionCommentVC(intialTransaction: transaction)
            self.presentSheet(viewController: transactionComment)
        }
    }
}
