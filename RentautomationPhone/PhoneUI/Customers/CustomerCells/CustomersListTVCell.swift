//
//  CustomersListTVCell.swift
//  Rentautomation
//
//  Created by kanybek on 3/24/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class CustomersListTVCell: UITableViewCell {
    // ----
    var customer: Customer?
    
    @IBOutlet weak var customerPrimaryKeyLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var adressLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var accountBalanceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.cellImageView.layer.cornerRadius = 37.5
        self.cellImageView.layer.masksToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func reloadData() -> Void {
        self.cellImageView.image = Global.shared.customerPlaceholderImage
        if let customer_ = self.customer {
            
            if Global.shared.isDebug {
                self.customerPrimaryKeyLabel.text = "\(customer_.customerId)"
                self.customerPrimaryKeyLabel.backgroundColor = UIColor.clear
            } else {
                self.customerPrimaryKeyLabel.isHidden = true
            }
            
            self.nameLabel.text = "\(customer_.firstName)  \(customer_.secondName)"
            self.adressLabel.text = "\(customer_.address)"
            self.phoneLabel.text = "\(customer_.phoneNumber)"
            var balanceString = "0.0"
            if let account_ = Database.shared.accountBy(customerId: customer_.customerId) {
                if let blStr = Global.shared.numberFormatter.string(for: account_.balance) {
                    balanceString = blStr
                }
            }
            self.accountBalanceLabel.text = balanceString
            
            if Database.shared.isModelSynchronized(primaryKey: customer_.customerId) {
                self.selectionStyle = .default
                self.nameLabel.textColor = UIColor.black
            } else {
                self.selectionStyle = .none
                self.nameLabel.textColor = UIColor.pomergranateColor()
            }
            
        } else {
            self.resetLabels()
        }
    }
    
    override func prepareForReuse() {
        self.customer = nil
        self.resetLabels()
    }
    
    func resetLabels() -> Void {
        self.nameLabel.text = ""
        self.phoneLabel.text = ""
        self.adressLabel.text = ""
        self.cellImageView.image = nil
        self.accountBalanceLabel.text = ""
        self.customerPrimaryKeyLabel.text = ""
    }
}
