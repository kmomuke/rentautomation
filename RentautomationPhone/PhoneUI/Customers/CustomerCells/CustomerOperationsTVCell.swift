//
//  CustomerOperationsTVCell.swift
//  Rentautomation
//
//  Created by kanybek on 3/25/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
class CustomerOperationsTVCell: UITableViewCell {
    
    var transaction: Transaction?
    
    @IBOutlet weak var transactionPrimaryKeyLabel: UILabel!
    @IBOutlet weak var transactionTypeLabel: UILabel!
    @IBOutlet weak var transactionDateLabel: UILabel!
    @IBOutlet weak var moneyAmountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func reloadData() -> Void {
        
        if let transaction_ = self.transaction {
            
            if Global.shared.isDebug {
                self.transactionPrimaryKeyLabel.text = "\(transaction_.transactionId)"
                self.transactionPrimaryKeyLabel.backgroundColor = UIColor.clear
            } else {
                self.transactionPrimaryKeyLabel.isHidden = true
            }
            
            transactionTypeLabel.text = Transaction.transactionTypeStringName(transactionType: transaction_.transactionType)
            transactionTypeLabel.textColor = Transaction.transactionTypeColor(transactionType: transaction_.transactionType)
            transactionDateLabel.text = "\(transaction_.transactionDate.toString(.custom("EEE, dd MMM HH:mm:ss")))"
            
            moneyAmountLabel.text = Global.shared.numberFormatter.string(for: transaction_.moneyAmount)
            
            if transaction_.isLastTransaction {
                transactionTypeLabel.backgroundColor = UIColor.greenSeaColor()
            } else {
                transactionTypeLabel.backgroundColor = UIColor.white
            }
            
            if Database.shared.isModelSynchronized(primaryKey: transaction_.transactionId) {
                self.transactionDateLabel.textColor = UIColor.black
                self.transactionDateLabel.backgroundColor = UIColor.groupTableViewBackground
            } else {
                self.transactionDateLabel.textColor = UIColor.white
                self.transactionDateLabel.backgroundColor = UIColor.pomergranateColor()
            }
            
        } else {
            self.resetLabels()
        }
    }
    
    override func prepareForReuse() {
        self.transaction = nil
        self.resetLabels()
    }
    
    func resetLabels() -> Void {
        self.transactionTypeLabel.text = ""
        self.transactionDateLabel.text = ""
        self.moneyAmountLabel.text = ""
        self.transactionPrimaryKeyLabel.text = ""
    }
}
