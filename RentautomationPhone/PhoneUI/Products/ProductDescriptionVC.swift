//
//  ProductDescriptionVC.swift
//  Rentautomation
//
//  Created by kanybek on 3/24/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftSignalKit
import MZFormSheetPresentationController
import Neon
import IDMPhotoBrowser


class ProductDescriptionVC: BaseVC, ErrorPopoverRenderer {
    // ----->
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var barcodeLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var historyProductMovementContainerView: UIView!
    
    private lazy var productMovementsVC: ProdcutMovementsVC = { [weak self] in
        let productMovementsVC_ = ProdcutMovementsVC(initialProduct: self?.initialProduct)
        return productMovementsVC_
    }()
    
    var initialProduct: Product?
    var isFirstTime: Bool = true
    var updateProductDispose: Disposable?
    
    convenience init() {
        self.init(initialProduct: nil)
    }
    
    init(initialProduct: Product?) {
        self.initialProduct = initialProduct
        super.init(nibName: "ProductDescriptionVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.updateProductDispose?.dispose()
        self.updateProductDispose = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let productUpdateSignal: Signal = PipesStore.shared.productUpdatedSignal()
        self.updateProductDispose = productUpdateSignal.start(next: {[weak self] (product: Product) in
            
            if let currentProduct = self?.initialProduct {
                if currentProduct.productId == product.productId {
                    self?.initialProduct = product
                    self?.updateLabels()
                }
            }
        })
        
        self.updateLabels()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (isFirstTime) {
            self.add(asChildViewController: self.productMovementsVC, frame_: self.historyProductMovementContainerView.frame)
            isFirstTime = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews() {
        //productImageView.anchorInCorner(.topLeft, xPad: 5, yPad: 5, width: 150, height: 150)
        //historyProductMovementContainerView.align(.underMatchingLeft,
        //                                          relativeTo: productImageView,
        //                                          padding: 500,
        //                                          width: 200,
        //                                          height: 200)
    }
    
    func updateLabels() -> Void {
        
        if let product_ = self.initialProduct {
            self.title = product_.productName
            self.barcodeLabel.text = "штрихкод: \(product_.barcode)"
            self.quantityLabel.text = "На складе: \(product_.unitsInStock) \(product_.quantityPerUnit)"
            
            var salePrice = ""
            if let stringSalePrice = Global.shared.numberFormatter.string(for: product_.saleUnitPrice) {
                salePrice = "прод: \(stringSalePrice)"
            }
            
            var incomePrice = ""
            if let stringIncomePrice = Global.shared.numberFormatter.string(for: product_.incomeUnitPrice) {
                incomePrice = "закуп: \(stringIncomePrice)"
            }
            
            self.priceLabel.text = "\(salePrice), \(incomePrice)"
            self.nameLabel.text = "Наим.: \(product_.productName)"
            
            if let url_ = product_.productImagePath {
                let url = URL(string: url_)
                self.productImageView.kf.indicatorType = .activity
                self.productImageView.kf.setImage(with: url,
                                                  placeholder: Global.shared.productPlaceholderImage,
                                                  options: nil,
                                                  progressBlock: nil,
                                                  completionHandler: { (im: Image?, err: NSError?, typr: CacheType, url: URL?) in
                })
            } else {
                self.productImageView.image = Global.shared.productPlaceholderImage
            }
            
            
            if let productImagePath_ = product_.productImagePath {
                if let imageExsist = ImageCache.default.retrieveImageInDiskCache(forKey: productImagePath_) {
                    self.productImageView.image = imageExsist
                } else {
                    self.productImageView.image = Global.shared.productPlaceholderImage
                }
            } else {
                self.productImageView.image = Global.shared.productPlaceholderImage
            }
            
            self.productMovementsVC.reloadTableData()
        }
    }
    
    func editProduct(_ sender: AnyObject) -> Void {
    }
    
    @IBAction func imageDidPressed(_ sender: UIButton) {
        
        if let productImagePath_ = self.initialProduct?.productImagePath {
            ImageCache.default.retrieveImage(forKey: productImagePath_,
                                             options: nil,
                                             completionHandler: {[weak self] (image: UIImage?, type: CacheType) in
                                                if let imageExsist = image {
                                                    var photos: [IDMPhoto] = []
                                                    let photo = IDMPhoto(image: imageExsist)
                                                    photos.append(photo!)
                                                    //let browser = IDMPhotoBrowser.init(photos: photos)
                                                    let browser: IDMPhotoBrowser = IDMPhotoBrowser(photos: photos, animatedFrom: self?.productImageView)
                                                    browser.scaleImage = self?.productImageView.image
                                                    self?.present(browser, animated: true, completion: nil)
                                                }
            })
        }
        
        //let productImVC = ProductImageVC(initialProduct: self.initialProduct)
        //self.navigationController?.pushViewController(productImVC, animated: true)
    }
}
