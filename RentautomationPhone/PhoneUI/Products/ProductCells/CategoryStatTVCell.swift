//
//  CategoryStatTVCell.swift
//  Rentautomation
//
//  Created by kanybek on 5/14/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class CategoryStatTVCell: UITableViewCell {

    var category: CategoryStat?
    
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var amountInStockLabel: UILabel!
    @IBOutlet weak var totalPriceInStockLabel: UILabel!
    @IBOutlet weak var incomePriceInStockLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func reloadData() -> Void {
        if let categoryStat_ = self.category {
            self.categoryNameLabel.text = categoryStat_.categoryName
            self.amountInStockLabel.text = Global.shared.numberFormatterForAmount.string(for: categoryStat_.amountInStock)
            self.totalPriceInStockLabel.text = Global.shared.numberFormatter.string(for: categoryStat_.categorySaledPrice)
            self.incomePriceInStockLabel.text = Global.shared.numberFormatter.string(for: categoryStat_.categoryIncomePrice)
        } else {
            self.resetLabels()
        }
    }
    
    override func prepareForReuse() {
        self.category = nil
        self.resetLabels()
    }
    
    func resetLabels() -> Void {
        self.categoryNameLabel.text = ""
        self.amountInStockLabel.text = ""
        self.totalPriceInStockLabel.text = ""
    }
    
}
