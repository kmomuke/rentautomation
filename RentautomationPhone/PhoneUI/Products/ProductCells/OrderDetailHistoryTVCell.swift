//
//  OrderDetailHistoryTVCell.swift
//  Rentautomation
//
//  Created by kanybek on 3/25/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class OrderDetailHistoryTVCell: UITableViewCell {
    
    // ----
    var orderDetail: OrderDetail?
    
    @IBOutlet weak var orderDetailPrimaryKeyLabel: UILabel!
    @IBOutlet weak var numberOrderDetalLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var nameOrderDetalLabel: UILabel!
    @IBOutlet weak var priceOrderDetalLabel: UILabel!
    @IBOutlet weak var quantityOrderDetalLabel: UILabel!
    @IBOutlet weak var totalOrderDetalLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func reloadData() -> Void {
        
        if let orderDetail_ = self.orderDetail {
            
            if Global.shared.isDebug {
                self.orderDetailPrimaryKeyLabel.text = "\(orderDetail_.orderDetailId)"
                self.orderDetailPrimaryKeyLabel.backgroundColor = UIColor.clear
            } else {
                self.orderDetailPrimaryKeyLabel.isHidden = true
            }
            
            self.numberOrderDetalLabel.text = "\(orderDetail_.orderDetailId)"
            self.nameOrderDetalLabel.backgroundColor = UIColor.white
            
            self.dateLabel.text = "\(orderDetail_.orderDetailDate.toString(.custom("EEE, dd MMM HH:mm")))" //:ss
            
            var name: String = "Не определено"
            if let order = Database.shared.orderBy(orderId: orderDetail_.orderId) {
                name = Order.orderDocumentTypeStringName(orderDocType: order.orderDocument)
                //dateString = "\(order.orderDate.toString(.custom("EEE, dd MMM HH:mm")))"
                self.nameOrderDetalLabel.textColor = Order.orderDocumentTypeColor(orderDocType: order.orderDocument)
                
            } else {
                //dateString = orderDetail_.billingNo
                if orderDetail_.orderId == 0 {
                    name = "Владелец редактировал"
                } else {
                    name = "Не определено пока"
                }
                
                self.nameOrderDetalLabel.textColor = UIColor.red
            }
            
            var quantityPerUnit: String = ""
            if let product = Database.shared.productFor(productId: orderDetail_.productId) {
                quantityPerUnit = product.quantityPerUnit
            }
            
            self.nameOrderDetalLabel.text = "\(name)"
            self.priceOrderDetalLabel.text = Global.shared.numberFormatter.string(for: orderDetail_.price)
            
            let text_ = Global.shared.numberFormatterForAmount.string(for: orderDetail_.orderQuantity)!
            self.quantityOrderDetalLabel.text = "\(text_) \(quantityPerUnit)"
            
            let totalPrice = orderDetail_.price * orderDetail_.orderQuantity
            self.totalOrderDetalLabel.text = Global.shared.numberFormatter.string(for: totalPrice)
            
            if orderDetail_.isLast {
                self.nameOrderDetalLabel.backgroundColor = UIColor.greenSeaColor()
            } else {
                self.nameOrderDetalLabel.backgroundColor = UIColor.clear
            }
            
            if Database.shared.isModelSynchronized(primaryKey: orderDetail_.orderDetailId) {
                self.numberOrderDetalLabel.textColor = UIColor.black
                self.numberOrderDetalLabel.backgroundColor = UIColor.clear
            } else {
                self.numberOrderDetalLabel.textColor = UIColor.white
                self.numberOrderDetalLabel.backgroundColor = UIColor.pomergranateColor()
            }
            
        } else {
            self.resetLabels()
        }
    }
    
    override func prepareForReuse() {
        self.orderDetail = nil
        self.resetLabels()
    }
    
    func resetLabels() -> Void {
        self.numberOrderDetalLabel.text = ""
        self.nameOrderDetalLabel.text = ""
        self.priceOrderDetalLabel.text = ""
        self.quantityOrderDetalLabel.text = ""
        self.totalOrderDetalLabel.text = ""
        self.orderDetailPrimaryKeyLabel.text = ""
    }
}
