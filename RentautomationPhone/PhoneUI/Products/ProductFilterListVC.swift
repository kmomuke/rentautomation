//
//  ProductFilterListVC.swift
//  Rentautomation
//
//  Created by kanybek on 3/24/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class ProductFilterListVC: FormViewController {
    
    var initialString: String?
    
    convenience init() {
        self.init(initialString: nil)
    }
    
    init(initialString: String?) {
        self.initialString = initialString
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    deinit {
    }
    
    // MARK: Public
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Товары"
        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
    }
    
    // MARK: Private
    private func configure() {
        
        let backBarButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backBarButton
        
        // Create RowFormers
        let createMenu: ((String, (() -> Void)?) -> RowFormer) = { text, onSelected in
            return LabelRowFormer<FormLabelCell>() {
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                }.configure {
                    $0.text = text
                    $0.rowHeight = 70
                }.onSelected { _ in
                    onSelected?()
            }
        }
        
        let allProductsRow = createMenu("Все") { [weak self] in
            let productsVC = ProductsListVC(initialString: nil, initialCategoryId: 0)
            self?.navigationController?.pushViewController(productsVC, animated: true)
        }
        
        let categoriesRow = createMenu("По категориям") { [weak self] in
            let categoryListVC = CategoriesListVC(initialString: "", isCommingFromProduct: false)
            self?.navigationController?.pushViewController(categoryListVC, animated: true)
        }
        
        let discountsRow = createMenu("Общая сумма в складе") { [weak self] in
            let totalAmountInStockVC = TotalAmountInStockVC()
            self?.navigationController?.pushViewController(totalAmountInStockVC, animated: true)
        }
        
        // Create Headers and Footers
        let createHeader: ((String) -> ViewFormer) = { text in
            return LabelViewFormer<FormLabelHeaderView>()
                .configure {
                    $0.text = text
                    $0.viewHeight = 40
            }
        }
        
        let createFooter: ((String) -> ViewFormer) = { text in
            return LabelViewFormer<FormLabelFooterView>()
                .configure {
                    $0.text = text
                    $0.viewHeight = 100
            }
        }
        
        // Create SectionFormers
        let realExampleSection = SectionFormer(rowFormer: allProductsRow, categoriesRow, discountsRow)
            .set(headerViewFormer: createHeader("Редактирование"))
            .set(footerViewFormer: createFooter("TazaPos - Простое решение, для учета вашего склада"))
        
        former.append(sectionFormer: realExampleSection)
    }
}
