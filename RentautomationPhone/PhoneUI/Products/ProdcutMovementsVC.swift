//
//  ProdcutMovementsVC.swift
//  Rentautomation
//
//  Created by kanybek on 3/25/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class ProdcutMovementsVC: UIViewController, ErrorPopoverRenderer, UITableViewDelegate, UITableViewDataSource {
    
    var initialProduct: Product?
    var orderDetails: [OrderDetail] = []
    
    var enableAboveHistoryRequests: Bool
    var enableBelowHistoryRequests: Bool
    
    @IBOutlet weak var tableView: UITableView!
    
    convenience init() {
        self.init(initialProduct: nil)
    }
    
    init(initialProduct: Product?) {
        self.initialProduct = initialProduct
        self.enableAboveHistoryRequests = true
        self.enableBelowHistoryRequests = true
        super.init(nibName: "ProdcutMovementsVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        let nib = UINib(nibName: "OrderDetailHistoryTVCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "OrderDetailHistoryTVCell")
        self.reloadTableData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func reloadTableData() -> Void {
        if let product = self.initialProduct {
            SmartLoader.shared.loadAllOrderDetailsFor(productId: product.productId,
                                                      date: Date(),
                                                      limit: 50) { (orderDetails_: [OrderDetail]) in
                                                        
                                                        self.orderDetails = orderDetails_
                                                        self.tableView.reloadData()
            }
        }
    }
    
    // MARK: TableView DataSource
    private func loadMoreMessagesAbove() -> Void {
        
    }
    
    private func loadMoreMessagesBelow() -> Void {
        
        if !self.enableBelowHistoryRequests {
            return
        }
        
        if let currentLastOrderDetail = self.orderDetails.last, let product = self.initialProduct {
            
            if currentLastOrderDetail.isLast {
                self.enableBelowHistoryRequests = false
            } else {
                self.showLoading("Загружаем")
                self.enableBelowHistoryRequests = false
                SmartLoader.shared.loadAllOrderDetailsFor(productId: product.productId,
                                                          date: currentLastOrderDetail.orderDetailDate,
                                                          limit: 50) { (orderDetails_: [OrderDetail]) in
                                                            
                                                            self.dismissLoading()
                                                            
                                                            if orderDetails_.count > 0 {
                                                                
                                                                var indicicesToInsert :[IndexPath] = []
                                                                for (index, _) in orderDetails_.enumerated() {
                                                                    let indexPath = IndexPath(row: index + self.orderDetails.count, section: 0)
                                                                    indicicesToInsert.append(indexPath)
                                                                }
                                                                
                                                                self.orderDetails.insert(contentsOf: orderDetails_, at: self.orderDetails.count)
                                                                
                                                                self.tableView.beginUpdates()
                                                                self.tableView.insertRows(at: indicicesToInsert, with: .top)
                                                                self.tableView.endUpdates()
                                                                
                                                                let delay = DispatchTime.now() + DispatchTimeInterval.seconds(1)
                                                                DispatchQueue.main.asyncAfter(deadline: delay, execute: {
                                                                    self.enableBelowHistoryRequests = true
                                                                })
                                                                
                                                            } else {
                                                                self.enableBelowHistoryRequests = false
                                                            }
                }
            }
        }
    }
    
    // MARK: TableView DataSource
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.tableView {
            let flt_epsilon: CGFloat = 0.1
            if scrollView.contentSize.height > flt_epsilon {
                
                if (enableBelowHistoryRequests && scrollView.contentOffset.y > scrollView.contentSize.height - 500 * 2.0) && (scrollView.contentSize.height > flt_epsilon) {
                    print("loadMoreMessagesBelow")
                    self.loadMoreMessagesBelow()
                }
                
                if (enableAboveHistoryRequests && scrollView.contentOffset.y < 60 * 2.0) {
                    print("loadMoreMessagesAbove")
                    self.loadMoreMessagesAbove()
                }
            }
        }
    }
    
    // MARK: TableView DataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailHistoryTVCell", for: indexPath) as! OrderDetailHistoryTVCell
        let orderDetail = orderDetails[(indexPath as NSIndexPath).row]
        cell.orderDetail = orderDetail
        cell.reloadData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let orderDetail = orderDetails[(indexPath as NSIndexPath).row]
        if orderDetail.orderId > 0 {
            let orderDescVC = OrderDescriptionVC(initialOrderId: orderDetail.orderId)
            self.navigationController?.pushViewController(orderDescVC, animated: true)
        }
    }
}
