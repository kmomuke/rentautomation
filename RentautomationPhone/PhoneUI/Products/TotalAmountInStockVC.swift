//
//  TotalAmountInStockVC.swift
//  Rentautomation
//
//  Created by kanybek on 3/25/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class TotalAmountInStockVC: BaseVC, ErrorPopoverRenderer, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var salePriceTotal: UILabel!
    @IBOutlet weak var incomePriceTotalLabel: UILabel!
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var phoneTableView: UITableView!
    
    var initialCategories: [CategoryStat] = []
    var categories: [CategoryStat] = []
    
    var initialItem: String?
    
    convenience init() {
        self.init(initialItem: nil)
    }
    
    init(initialItem: String?) {
        self.initialItem = initialItem
        super.init(nibName: "TotalAmountInStockVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "CategoryStatTVCell", bundle: nil)
        self.phoneTableView.register(nib, forCellReuseIdentifier: "CategoryStatTVCell")
        
        self.searchBar.delegate = self
        self.phoneTableView.delegate = self
        self.phoneTableView.dataSource = self
        
        self.title = "Остатки на складе"
        
        var totalSaleSum = 0.0
        var totalIncomeSum = 0.0
        
        Database.shared.loadAllProductsBy(categoryId: 0,
                                          limit: Global.shared.limitForStaticModels) {[weak self] (products_: [Product]) in
                                            
                                            for product in products_ {
                                                totalSaleSum = totalSaleSum + product.saleUnitPrice * product.unitsInStock
                                                totalIncomeSum = totalIncomeSum + product.incomeUnitPrice * product.unitsInStock
                                            }
                                            
                                            var salePrice = ""
                                            if let stringSalePrice = Global.shared.numberFormatter.string(for: totalSaleSum) {
                                                salePrice = stringSalePrice
                                            }
                                            self?.salePriceTotal.text = "\(salePrice)"
                                            
                                            var incomePrice = ""
                                            if let stringIncomePrice = Global.shared.numberFormatter.string(for: totalIncomeSum) {
                                                incomePrice = stringIncomePrice
                                            }
                                            self?.incomePriceTotalLabel.text = "\(incomePrice)"
        }
        
        self.reloadAllCategories()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func reloadAllCategories() {
        self.showLoading("Генерация ...")
        StatGenerator.shared.calculateCategoryStats {[weak self] (categoryStats: [CategoryStat]) in
            self?.dismissLoading()
            self?.initialCategories = categoryStats
            self?.categories = categoryStats
            self?.phoneTableView.reloadData()
        }
    }
    
    // MARK: TableView DataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryStatTVCell", for: indexPath) as! CategoryStatTVCell
        cell.category = categories[(indexPath as NSIndexPath).row]
        cell.reloadData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}


extension TotalAmountInStockVC: UISearchBarDelegate {
    
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("searchBarTextDidBeginEditing")
    }
    
    public func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print("searchBarTextDidEndEditing")
    }
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)")
        
        if searchText.characters.count > 0 {
            let filteredArray = self.initialCategories.filter({ (cat: CategoryStat) -> Bool in
                if cat.categoryName.range(of:searchText) != nil{
                    return true
                }
                return false
            })
            self.categories = filteredArray
            self.phoneTableView.reloadData()
        } else {
            self.categories = self.initialCategories
            self.phoneTableView.reloadData()
        }
    }
    
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("searchBarSearchButtonClicked ==> \(searchBar.text!)")
        searchBar.resignFirstResponder()
    }
    
    public func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        print("searchBarBookmarkButtonClicked")
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("searchBarCancelButtonClicked")
        self.categories = self.initialCategories
        self.phoneTableView.reloadData()
    }
    
    public func searchBarResultsListButtonClicked(_ searchBar: UISearchBar) {
        print("searchBarResultsListButtonClicked")
    }
    
    public func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        print("searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope")
    }
}
