//
//  MostSaledProductsListVC.swift
//  Rentautomation
//
//  Created by kanybek on 5/10/17.
//  Copyright © 2017 kanybek. All rights reserved.
//


import UIKit

class MostSaledProductsListVC: BaseVC, ErrorPopoverRenderer, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var totalSaledLabel: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dateLabel: UILabel!
    
    var isFirstTime: Bool = true
    var mostSaledProducts: [MostProduct] = []
    var initalOrderDocType: OrderDocumentType
    
    convenience init() {
        self.init(initalOrderDocType: .none)
    }
    
    init(initalOrderDocType: OrderDocumentType) {
        self.initalOrderDocType = initalOrderDocType
        super.init(nibName: "MostSaledProductsListVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        OrderFilterMemory.shared.clearAllVariables()
        
        let endDate = Date()
        let startDate = endDate.dateBySubtractingWeeks(1)
        OrderFilterMemory.shared.startDate = startDate
        OrderFilterMemory.shared.endDate = endDate
        
        self.title = Order.orderDocumentTypeStringName(orderDocType: self.initalOrderDocType)
        
        let font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightMedium)
        self.segmentedControl.setTitleTextAttributes([NSFontAttributeName: font],
                                                     for: .normal)
        self.dateLabel.textColor = UIColor.midnightBlueColor()
        let nib = UINib(nibName: "ProductStatisticTVCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "ProductStatisticTVCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (isFirstTime) {
            self.segmentedControl.selectedSegmentIndex = 0
            self.indexChanged(self.segmentedControl)
            isFirstTime = false
        }
    }
    
    func reloadAllData(startDate_: Date, endDate_: Date) {
        self.showLoading("Получение данных...")
        self.dateLabel.text = "\(startDate_.toString(.custom("EEE, dd MMMM HH:mm")))   -   \(endDate_.toString(.custom("EEE, dd MMMM HH:mm")))"
        
        StatGenerator.shared.calculateMostSaledProducts(startDate: startDate_, endDate: endDate_, orderDocType: self.initalOrderDocType, userId: 0) {[weak self] (p: [MostProduct]) in
            if let strongSelf = self {
                strongSelf.dismissLoading()
                strongSelf.mostSaledProducts = p
                strongSelf.tableView.reloadData()
                if let lastP_ = p.last {
                    strongSelf.totalSaledLabel.text = Global.shared.numberFormatterForAmount.string(for: lastP_.totalSaledAmount)
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func customDateDidPressed() {
        
        let statVC = StatisticFilterVC(initialItem: "")
        statVC.applyFiltersBlock = {[weak self] (startDate_: Date, endDate_: Date) -> Void in
            if let strongSelf = self {
                strongSelf.reloadAllData(startDate_: startDate_, endDate_: endDate_)
            }
        }
        statVC.cancelBlock = {[weak self] (finished: Bool) -> Void in
            if let strongSelf = self {
                strongSelf.segmentedControl.selectedSegmentIndex = 0
                strongSelf.indexChanged(strongSelf.segmentedControl)
            }
        }
        self.presentSheet(viewController: statVC)
    }
    
    @IBAction func indexChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex
        {
        case 0:
            print("First Segment Selected")
            let endDate = Date()
            let startDate = endDate.dateBySubtractingWeeks(1).dateAtStartOfDay()
            self.reloadAllData(startDate_: startDate, endDate_: endDate)
        case 1:
            print("Second Segment Selected")
            let endDate = Date()
            let startDate = endDate.dateBySubtractingMonths(1).dateAtStartOfDay()
            self.reloadAllData(startDate_: startDate, endDate_: endDate)
        case 2:
            print("Third Segment Selected")
            self.customDateDidPressed()
        default:
            print("default")
            break
        }
    }
    
    // MARK: TableView DataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mostSaledProducts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductStatisticTVCell", for: indexPath) as! ProductStatisticTVCell
        let mostPr: MostProduct = mostSaledProducts[(indexPath as NSIndexPath).row]
        cell.mostProduct = mostPr
        cell.reloadData(row: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let _ = mostSaledProducts[(indexPath as NSIndexPath).row]
    }
}
