//
//  DetailStatisticsVC.swift
//  Rentautomation
//
//  Created by kanybek on 5/16/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class DetailStatisticsVC: UIViewController, ErrorPopoverRenderer, UITableViewDelegate, UITableViewDataSource {

    var selectedSegmentIndex: Int = 0
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var clientButton: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var customerOrders: [CustomerOrder] = []
    var initalString: String?
    
    convenience init() {
        self.init(initalString: "kokekoke")
    }
    
    init(initalString: String) {
        self.initalString = initalString
        OrderMemomry.shared.clearAllVariables()
        super.init(nibName: "DetailStatisticsVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let title_ = self.initalString {
            self.title = title_
        }
        self.clientButton.addTarget(self, action: #selector(customerDidPressed(_:)), for: .touchUpInside)
        
        let nib = UINib(nibName: "CustomerStatisticTVCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "CustomerStatisticTVCell")
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    func cancelVC(_ sender: AnyObject) -> Void {
        OrderMemomry.shared.clearAllVariables()
        let empty = EmptyVC()
        let detailNavigationController = RNNavigationController(rootViewController:empty)
        self.splitViewController?.showDetailViewController(detailNavigationController, sender: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if OrderMemomry.shared.customerId > 0 {
            let customerId = OrderMemomry.shared.customerId
            if let customer__ = Database.shared.customerBy(customerId:customerId) {
                self.clientButton.setTitle("\(customer__.firstName) \(customer__.secondName)", for: .normal)
            }
        }
        self.reloadAllData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func indexChanged(_ sender: UISegmentedControl) {
        self.selectedSegmentIndex = sender.selectedSegmentIndex
        self.reloadAllData()
    }
    
    @IBAction func customerDidPressed(_ sender: Any) {
        let customerListVC = CustomersListVC(initialString: nil, isComingFromOrder: true)
        self.navigationController?.pushViewController(customerListVC, animated: true)
    }
    
    private func updateLabel(_ startDate: Date, endDate: Date) -> Void {
        OrderFilterMemory.shared.startDate = startDate
        OrderFilterMemory.shared.endDate = endDate
        
        self.dateLabel.text = "\(startDate.toString(.custom("EEE, dd MMMM HH:mm"))) - \(endDate.toString(.custom("EEE, dd MMMM HH:mm")))"
    }
    
    func reloadAllData() -> Void {
        switch self.selectedSegmentIndex
        {
        case 0:
            print("First Segment Selected")
            let startDate = Date().dateAtStartOfDay()
            let endDate = Date()
            self.updateLabel(startDate, endDate: endDate)
            let customerId = OrderMemomry.shared.customerId
            self.getOrders(startDate, endDate, customerId: customerId)
        case 1:
            print("Second Segment Selected")
            let startDate = Date().dateAtStartOfWeek()
            let endDate = Date()
            self.updateLabel(startDate, endDate: endDate)
            let customerId = OrderMemomry.shared.customerId
            self.getOrders(startDate, endDate, customerId: customerId)
        case 2:
            print("Third Segment Selected")
            let startDate = Date().dateAtTheStartOfMonth().dateAtStartOfDay()
            let endDate = Date()
            self.updateLabel(startDate, endDate: endDate)
            let customerId = OrderMemomry.shared.customerId
            self.getOrders(startDate, endDate, customerId: customerId)
        case 3:
            self.customDateDidPressed()
        default:
            print("default")
            break
        }
    }
    
    func customDateDidPressed() {
        
        let statVC = StatisticFilterVC(initialItem: "")
        statVC.applyFiltersBlock = {[weak self] (startDate_: Date, endDate_: Date) -> Void in
            if let strongSelf = self {
                let startDate = startDate_
                let endDate = endDate_
                strongSelf.updateLabel(startDate, endDate: endDate)
                let customerId = OrderFilterMemory.shared.customerId
                strongSelf.getOrders(startDate, endDate, customerId: customerId)
            }
        }
        statVC.cancelBlock = {[weak self] (finished: Bool) -> Void in
            self?.segmentedControl.selectedSegmentIndex = 0
            self?.selectedSegmentIndex = 0
            self?.reloadAllData()
        }
        self.presentSheet(viewController: statVC)
    }
    
    
    func getOrders(_ startDate: Date, _ endDate: Date, customerId: UInt64) {
        self.showLoading("Подсчет данных ...")
        Database.shared.loadOrdersForFilters(documentType: .none,
                                             startDate: startDate,
                                             endDate: endDate,
                                             customerId: customerId,
                                             supplierId: 0,
                                             userId: 0,
                                             limit: 5000,
                                             filterOptions: []) {[weak self] (orders_: [Order]) in
                                                print(orders_)
                                                StatGenerator.shared.calculateCustomerOrders(orders: orders_, completion: {[weak self] (k:
                                                    [CustomerOrder]) in
                                                    self?.dismissLoading()
                                                    self?.customerOrders = k
                                                    self?.tableView.reloadData()
                                                })
        }
    }
    
    
    // MARK: TableView DataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.customerOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomerStatisticTVCell", for: indexPath) as! CustomerStatisticTVCell
        cell.customerOrder = self.customerOrders[(indexPath as NSIndexPath).row]
        cell.reloadData(indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
