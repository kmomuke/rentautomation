//
//  CustomerStatisticTVCell.swift
//  Rentautomation
//
//  Created by kanybek on 5/16/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class CustomerStatisticTVCell: UITableViewCell {

    
    var customerOrder: CustomerOrder?
    
    @IBOutlet weak var totalReceivedLabel: UILabel!
    @IBOutlet weak var productsSaledLabel: UILabel!
    @IBOutlet weak var customerNameLabel: UILabel!
    @IBOutlet weak var ordersLabel: UILabel!
    @IBOutlet weak var debtPriceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.totalReceivedLabel.textColor = UIColor.greenSeaColor()
        self.debtPriceLabel.textColor = UIColor.wisteriaColor()
        self.productsSaledLabel.textColor = UIColor.belizeHoleColor()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func reloadData(_ indexPath: IndexPath) -> Void {
        if let customerOrder_ = self.customerOrder {
            
            var customerOrSupplierName: String = ""
            if customerOrder_.customerId > 0 {
                if let customer = Database.shared.customerBy(customerId: customerOrder_.customerId) {
                    customerOrSupplierName = "\((indexPath.row + 1)). \(customer.firstName) \(customer.secondName)"
                }
            }
            self.customerNameLabel.text = customerOrSupplierName
            self.totalReceivedLabel.text = Global.shared.numberFormatter.string(for: customerOrder_.totalIncomeMoney)
            self.productsSaledLabel.text = Global.shared.numberFormatter.string(for: customerOrder_.productsSaledMoney)
            self.ordersLabel.text = Global.shared.numberFormatterForAmount.string(for: customerOrder_.orders.count)
            self.debtPriceLabel.text = Global.shared.numberFormatter.string(for: (customerOrder_.productsSaledMoney - customerOrder_.saleIncomeMoney))
        } else {
            self.resetLabels()
        }
    }
    
    override func prepareForReuse() {        
        self.customerOrder = nil
        self.resetLabels()
    }
    
    func resetLabels() -> Void {
        self.customerNameLabel.text = ""
        self.totalReceivedLabel.text = ""
        self.productsSaledLabel.text = ""
        self.ordersLabel.text = ""
    }
}
