//
//  StatisticTypesListVC.swift
//  Rentautomation
//
//  Created by kanybek on 5/10/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class StatisticTypesListVC: FormViewController {
    
    var initialString: String?
    
    convenience init() {
        self.init(initialString: nil)
    }
    
    init(initialString: String?) {
        self.initialString = initialString
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Отчеты о продажах"
        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
    }
    
    // MARK: Private
    private func configure() {
        
        let backBarButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backBarButton
        
        // Create RowFormers
        let createMenu: ((String, (() -> Void)?) -> RowFormer) = { text, onSelected in
            return LabelRowFormer<FormLabelCell>() {
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                }.configure {
                    $0.text = text
                    $0.rowHeight = 70
                }.onSelected { _ in
                    onSelected?()
            }
        }
        
        let allDocumentTypesRow = createMenu("Отчеты о продаже прибыли и по долгам") { [weak self] in
            let statisticsVC = MainStatisticsVC(initalString: "Отчеты о продаже и прибыли")
            self?.navigationController?.pushViewController(statisticsVC, animated: true)
        }
        
        let debtTypesRow = createMenu("Детальные отчеты по клиентам") { [weak self] in
            let statisticsVC = DetailStatisticsVC(initalString: "Отчеты по клиентам")
            self?.navigationController?.pushViewController(statisticsVC, animated: true)
        }
        
        let mostSaledRow = createMenu("Самые продоваемые товары") { [weak self] in
            let mostSaledVC = MostSaledProductsListVC(initalOrderDocType: .saleToCustomer)
            self?.navigationController?.pushViewController(mostSaledVC, animated: true)
        }
        
        let supplierIncomeRow = createMenu("Закупаемые товары") { [weak self] in
            let mostSaledVC = MostSaledProductsListVC(initalOrderDocType: .receiveFromSupplier)
            self?.navigationController?.pushViewController(mostSaledVC, animated: true)
        }
        
        let totalDebtsVC = createMenu("Общая сумма долгов клиентов") { [weak self] in
            let mostSaledVC = TotalCustomerPhoneDebtsVC(initalString: "")
            self?.navigationController?.pushViewController(mostSaledVC, animated: true)
        }
        
        let graphRow = createMenu("График продаж за \(Global.shared.statisticShowDay) дней") { [weak self] in
                let graphsVC = SaleGraphsCV(initalString: "")
                self?.navigationController?.pushViewController(graphsVC, animated: true)
        }
        
        // Create Headers and Footers
        let createHeader: ((String) -> ViewFormer) = { text in
            return LabelViewFormer<FormLabelHeaderView>()
                .configure {
                    $0.text = text
                    $0.viewHeight = 40
            }
        }
        
        let realExampleSection1 = SectionFormer(rowFormer: allDocumentTypesRow, debtTypesRow, graphRow, mostSaledRow, supplierIncomeRow, totalDebtsVC)
            .set(headerViewFormer: createHeader(" "))
        
        former.append(sectionFormer: realExampleSection1)
    }
}
