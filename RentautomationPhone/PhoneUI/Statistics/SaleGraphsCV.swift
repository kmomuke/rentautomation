//
//  SaleGraphsCV.swift
//  Rentautomation
//
//  Created by kanybek on 5/10/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import Charts

class SaleGraphsCV: BaseVC, ErrorPopoverRenderer {
    
    @IBOutlet weak var chartView: LineChartView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var isFirstTime: Bool = true
    var initalString: String?
    var calcNumbers: [(Double, UInt32, Double, Double)]
    
    convenience init() {
        self.init(initalString: "")
    }
    
    init(initalString: String) {
        self.initalString = initalString
        self.calcNumbers = []
        super.init(nibName: "SaleGraphsCV", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let font = UIFont.systemFont(ofSize: 13, weight: UIFontWeightMedium)
        self.segmentedControl.setTitleTextAttributes([NSFontAttributeName: font],
                                                     for: .normal)
        
        self.title = "График продаж за \(Global.shared.statisticShowDay) дней"
        self.chartView.delegate = self
        self.chartView.chartDescription?.enabled = false
        self.chartView.dragEnabled = true
        self.chartView.setScaleEnabled(true)
        self.chartView.pinchZoomEnabled = true
        self.chartView.drawGridBackgroundEnabled = false
        self.chartView.highlightPerDragEnabled = true
        self.chartView.backgroundColor = UIColor.white
        self.chartView.legend.enabled = false
        
        let xAxis: XAxis = self.chartView.xAxis
        xAxis.labelPosition = .top
        xAxis.labelFont = UIFont.systemFont(ofSize: 11, weight: UIFontWeightRegular)
        xAxis.labelTextColor = UIColor.blue
        xAxis.drawAxisLineEnabled = true
        xAxis.drawGridLinesEnabled = true
        xAxis.centerAxisLabelsEnabled = false
        xAxis.granularity = 1.0;
        xAxis.valueFormatter = DateValueFormatter()
        
        let leftAxis: YAxis = self.chartView.leftAxis
        leftAxis.labelPosition = .insideChart
        leftAxis.labelFont = UIFont.systemFont(ofSize: 11, weight: UIFontWeightRegular)
        leftAxis.labelTextColor = UIColor.blue
        leftAxis.drawGridLinesEnabled = true
        leftAxis.granularityEnabled = true
        leftAxis.axisMinimum = -40000.0
        leftAxis.axisMaximum = 40000.0
        leftAxis.yOffset = -9.0;
        
        let marker = BalloonMarker(color: UIColor.alizarinColor(),
                                   font: UIFont.systemFont(ofSize: 12, weight: UIFontWeightMedium),
                                   textColor: UIColor.black,
                                   insets: UIEdgeInsets(top: 8.0, left: 8.0, bottom: 20.0, right: 8.0))
        
        marker.chartView = self.chartView
        marker.minimumSize = CGSize(width: 80, height: 40)
        self.chartView.marker = marker
        
        self.chartView.rightAxis.enabled = false
        self.chartView.legend.form = .line
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (isFirstTime) {
            self.showLoading("Получение данных...")
            StatGenerator.shared.calculateOrdersForCustomDates(completion: {[weak self] (calcNumbers: [(Double, UInt32, Double, Double)]) in
                if let strongSelf = self {
                    strongSelf.dismissLoading()
                    strongSelf.calcNumbers = calcNumbers
                    strongSelf.productSaleToCustomerCount(calcNumbers: strongSelf.calcNumbers)
                    //strongSelf.setMoneyProfitCount(calcNumbers: strongSelf.calcNumbers)
                }
            })
            isFirstTime = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setMoneyProfitCount(calcNumbers: [(Double, UInt32, Double, Double)]) -> Void {
        
        //var productSaleToCustomer: Double = 0.0
        //var totalSaleOrders: UInt32 = 0
        //var moneyProffit: Double = 0.0
        
        let leftAxis: YAxis = self.chartView.leftAxis
        leftAxis.axisMinimum = -20000.0
        leftAxis.axisMaximum = 20000.0
        leftAxis.yOffset = -9.0;
        
        var moneyProfitEntries: [ChartDataEntry] = []
        for (index, calcNumber) in calcNumbers.enumerated() {
            let entry = ChartDataEntry(x: Double(index), y: calcNumber.2)
            moneyProfitEntries.append(entry)
        }
        
        let set1 = LineChartDataSet(values: moneyProfitEntries, label: "Прибыль")
        set1.axisDependency = .left
        set1.valueTextColor = UIColor.midnightBlueColor()
        set1.lineWidth = 2
        set1.circleRadius = 4
        set1.drawCirclesEnabled = true
        set1.drawValuesEnabled = true
        set1.drawFilledEnabled = true
        set1.fillAlpha = 0.3
        set1.fillColor = UIColor.greenSeaColor()
        set1.highlightColor = UIColor.pomergranateColor()
        set1.drawCircleHoleEnabled = false
        
        let data = LineChartData(dataSet: set1)
        data.setValueTextColor(UIColor.alizarinColor())
        data.setValueFont(UIFont.systemFont(ofSize: 11, weight: UIFontWeightMedium))
        self.chartView.data = data;
    }
    
    func productSaleToCustomerCount(calcNumbers: [(Double, UInt32, Double, Double)]) -> Void {
        
        //var productSaleToCustomer: Double = 0.0
        //var totalSaleOrders: UInt32 = 0
        //var moneyProffit: Double = 0.0
        
        let leftAxis: YAxis = self.chartView.leftAxis
        leftAxis.axisMinimum = 0.0
        leftAxis.axisMaximum = 40000.0
        leftAxis.yOffset = -9.0;
        
        var saleProductEntries: [ChartDataEntry] = []
        for (index, calcNumber) in calcNumbers.enumerated() {
            let saleEntry = ChartDataEntry(x: Double(index), y: calcNumber.0)
            saleProductEntries.append(saleEntry)
        }
        
        let set1 = LineChartDataSet(values: saleProductEntries, label: "Продажи")
        set1.axisDependency = .left
        set1.valueTextColor = UIColor.midnightBlueColor()
        set1.lineWidth = 2
        set1.circleRadius = 4
        set1.drawCirclesEnabled = true
        set1.drawValuesEnabled = true
        set1.drawFilledEnabled = true
        set1.fillAlpha = 0.2
        set1.fillColor = UIColor.emeraldColor()
        set1.highlightColor = UIColor.pomergranateColor()
        set1.drawCircleHoleEnabled = false
        
        let data = LineChartData(dataSet: set1)
        data.setValueTextColor(UIColor.alizarinColor())
        data.setValueFont(UIFont.systemFont(ofSize: 11, weight: UIFontWeightMedium))
        self.chartView.data = data;
    }
    
    func totalSaleOrdersCount(calcNumbers: [(Double, UInt32, Double, Double)]) -> Void {
        
        //var productSaleToCustomer: Double = 0.0
        //var totalSaleOrders: UInt32 = 0
        //var moneyProffit: Double = 0.0
        let leftAxis: YAxis = self.chartView.leftAxis
        leftAxis.axisMinimum = 0.0
        leftAxis.axisMaximum = 100.0
        leftAxis.yOffset = -9.0;
        
        var moneyProfitEntries: [ChartDataEntry] = []
        for (index, calcNumber) in calcNumbers.enumerated() {
            let entry = ChartDataEntry(x: Double(index), y: Double(calcNumber.1))
            moneyProfitEntries.append(entry)
        }
        
        let set1 = LineChartDataSet(values: moneyProfitEntries, label: "Кол посетители")
        set1.axisDependency = .left
        set1.valueTextColor = UIColor.midnightBlueColor()
        set1.lineWidth = 2
        set1.circleRadius = 4
        set1.drawCirclesEnabled = true
        set1.drawValuesEnabled = true
        set1.drawFilledEnabled = true
        //set1.fillAlpha = 0.5
        //set1.fillColor = UIColor.asbestosColor()
        set1.highlightColor = UIColor.pomergranateColor()
        set1.drawCircleHoleEnabled = false
        
        let data = LineChartData(dataSet: set1)
        data.setValueTextColor(UIColor.alizarinColor())
        data.setValueFont(UIFont.systemFont(ofSize: 11, weight: UIFontWeightMedium))
        self.chartView.data = data;
    }
    
    func incomeMoney(calcNumbers: [(Double, UInt32, Double, Double)]) -> Void {
        
        let leftAxis: YAxis = self.chartView.leftAxis
        leftAxis.axisMinimum = 0.0
        leftAxis.axisMaximum = 40000.0
        leftAxis.yOffset = -9.0;
        
        var incomeMoneyEntries: [ChartDataEntry] = []
        for (index, calcNumber) in calcNumbers.enumerated() {
            
            let incomeEntry = ChartDataEntry(x: Double(index), y: calcNumber.3)
            incomeMoneyEntries.append(incomeEntry)
        }
        
        let set2 = LineChartDataSet(values: incomeMoneyEntries, label: "Приход")
        set2.axisDependency = .left
        set2.valueTextColor = UIColor.midnightBlueColor()
        set2.lineWidth = 2
        set2.circleRadius = 4
        set2.drawCirclesEnabled = true
        set2.drawValuesEnabled = true
        set2.drawFilledEnabled = true
        set2.fillAlpha = 0.3
        set2.fillColor = UIColor.belizeHoleColor()
        set2.highlightColor = UIColor.pomergranateColor()
        set2.drawCircleHoleEnabled = false
        
        let data = LineChartData(dataSet: set2)
        data.setValueTextColor(UIColor.alizarinColor())
        data.setValueFont(UIFont.systemFont(ofSize: 11, weight: UIFontWeightMedium))
        self.chartView.data = data;
    }
    
    
    
    @IBAction func indexChanged(_ sender: UISegmentedControl) {
        
        self.chartView.clear()
        //float xScale = totalPoints / initialPointsDisplayed;
        //[chart zoom:xScale scaleY:1.0 x:0.0 y:0.0];
        //[self.chartView moveViewToX:totalPoints - initialPointsDisplayed];
        
        
        //self.chartView.zoom(scaleX: 1.0, scaleY: 1.0, x: 0.0, y: 0.0)
        //self.chartView.moveViewToX(0)
        
        //        self.chartView.zoomAndCenterViewAnimated(scaleX: 1.0,
        //                                                 scaleY: 1.0,
        //                                                 xValue: 0,
        //                                                 yValue: 0,
        //                                                 axis: .right,
        //                                                 duration: 40)
        switch sender.selectedSegmentIndex
        {
        case 0:
            print("Sales")
            self.productSaleToCustomerCount(calcNumbers: self.calcNumbers)
        case 1:
            print("Profit")
            self.setMoneyProfitCount(calcNumbers: self.calcNumbers)
        case 2:
            print("People")
            self.totalSaleOrdersCount(calcNumbers: self.calcNumbers)
        case 3:
            print("Income")
            self.incomeMoney(calcNumbers: self.calcNumbers)
        default:
            print("default")
            break
        }
    }
}

extension SaleGraphsCV: ChartViewDelegate {
    
}
