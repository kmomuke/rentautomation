//
//  MainScreenVC.swift
//  Rentautomation
//
//  Created by kanybek on 3/19/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit
import DrawerController

class MainScreenVC: FormViewController, ErrorPopoverRenderer {
    
    var initialString: String?
    var nonSyncedAmount: Int32
    var isFirstTime: Bool
    var syncFinishedDispose: Disposable?
    var rpcStatusDispose: Disposable?
    
    lazy var titleView: TitleView2 = {
        let view = TitleView2()
        return view
    }()
    
    override var title: String? {
        set {
            titleView.titleLabel.text = newValue
        }
        get {
            return titleView.titleLabel.text
        }
    }
    
    convenience init() {
        self.init(initialString: nil)
    }
    
    init(initialString: String?) {
        self.initialString = initialString
        self.nonSyncedAmount = 0
        self.isFirstTime = true
        super.init(nibName: nil, bundle: nil)
        let (image1, image2) = Global.shared.contactsIconImage()
        tabBarItem = UITabBarItem(title: "Главная", image: image1, selectedImage: image2)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.syncFinishedDispose?.dispose()
        self.syncFinishedDispose = nil
        
        self.rpcStatusDispose?.dispose()
        self.rpcStatusDispose = nil
    }
    
    // MARK: Public
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.titleView = self.titleView
        
        self.title = "Главная"
        if ReachabilityManager.sharedInstance.isConnectedToInternet() {
            self.titleView.detailLabel.text = "Идет соединение ..."
        } else {
            self.titleView.detailLabel.text = "Отсоединен от сервера"
        }
        
        let syncFinishedSignal: Signal = PipesStore.shared.syncFinishedSignal()
        self.syncFinishedDispose = syncFinishedSignal.start(next: {[weak self] (finished: Bool) in
            if finished {
                self?.updateSyncCell()
            }
        })
        
        let rpcSignalSignal: Signal = PipesStore.shared.rpcConnectionStatusSignal()
        self.syncFinishedDispose = rpcSignalSignal.start(next: {[weak self] (connectionStatus: RpcConnectionType) in
            switch connectionStatus {
            case .connected:
                self?.titleView.detailLabel.text = "Соединен"
                print("connected")
            case .connecting:
                self?.titleView.detailLabel.text = "Идет соединение ..."
                print("connecting")
            case .disconnected:
                self?.titleView.detailLabel.text = "Отсоединен от сервера"
                print("disconnected")
            default:
                print("default")
            }
        })
        
        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.former.deselect(animated: true)
        self.updateSyncCell()
    }
    
    private func updateSyncCell() -> Void {
        Database.shared.countUnSyncedOperations { (totalCount: Int32) in
            self.nonSyncedAmount = totalCount
            let row = self.former.firstSectionFormer?.rowFormers.first
            row?.update()
        }
    }
    
    private func startDocument(_ title: String, viewController: UIViewController) -> Void {
    }
    
    // MARK: Private
    private func configure() {
        
        // Create RowFormers
        let createMenu: ((String, (() -> Void)?) -> RowFormer) = {[weak self] text, onSelected in
            return LabelRowFormer<FormLabelCell>() {
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                }.configure {
                    $0.text = text
                    $0.rowHeight = 50
                }.onSelected { _ in
                    self?.former.deselect(animated: true)
                    onSelected?()
            }
        }
        
        let createDynamicMenu: ((String, (() -> Void)?) -> RowFormer) = {[weak self] text, onSelected in
            return LabelRowFormer<FormLabelCell>() {
                
                $0.titleLabel.textColor = UIColor.emeraldColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                }.configure {
                    $0.text = text
                    $0.rowHeight = 50
                }.onSelected { _ in
                    self?.former.deselect(animated: true)
                    onSelected?()
                }.onUpdate {
                    if let totalCount = self?.nonSyncedAmount {
                        if totalCount > 0 {
                            $0.text = "Не сохранен \(totalCount)"
                            $0.cellUpdate({ (cell: FormLabelCell) in
                                cell.formTextLabel()?.textColor = UIColor.pomergranateColor()
                            })
                        } else {
                            $0.text = "Сохранить"
                            $0.cellUpdate({ (cell: FormLabelCell) in
                                cell.formTextLabel()?.textColor = UIColor.emeraldColor()
                            })
                        }
                    }
            }
        }
        
        let syncRow = createDynamicMenu("Сохранить") {[weak self] (Void) in
            SynchronizeManager.sharedInstance.synchronizeAllNeededOperations {(response: Bool) in
                if response {
                }
            }
        }
        
        let getStaticModelsRow = createMenu("Обновить все данные из сервера") {[weak self] in
            self?.former.deselect(animated: true)
            let alert: UIAlertController = UIAlertController(title: "Обновить все",
                                                             message: "Вы действительно хотите обновить все данные из сервера?",
                                                             preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
            
            alert.addAction(UIAlertAction(title: "Обновить", style: .default, handler: {
                (UIAlertAction) -> Void in
                
                Database.shared.countUnSyncedOperations {[weak self] (totalCount: Int32) in
                    if let strongSelf = self {
                        strongSelf.showLoading("Получение статических данных ...")
                        SynchronizeManager.sharedInstance.getInitialStaticModels(completion: {[weak self] (boolValue: Bool) in
                            DispatchQueue.main.async {
                                self?.dismissLoading()
                            }
                        })
                    }
                }
            }))
            
            self?.present(alert, animated: true, completion: nil)
        }
        
        let activityRow = createMenu("История продаж, закупок, прихода и расхода") {[weak self] (Void) in
            let ordersVC = OrderTypesListVC(initialString: "fromIphone")
            self?.navigationController?.pushViewController(ordersVC, animated: true)
        }
        
        let statisticsRow = createMenu("Отчеты о продажах") {[weak self] (Void) in
            let statisticsVC = StatisticTypesListVC(initialString: "")
            self?.navigationController?.pushViewController(statisticsVC, animated: true)
        }
        
        let inventoryRow = createMenu("Инвентаризация / Черновик") {[weak self] (Void) in
            
            var drawerController: DrawerController
            
            let leftSideDrawerViewController = FilterProductsTVC()
            let centerViewController = MainDocumentVC(initialOrderDocument: .stockTaking)
            let rightSideDrawerViewController = ProductsBucketVC()
            
            let navigationController = UINavigationController(rootViewController: centerViewController)
            let rightSideNavController = UINavigationController(rootViewController: rightSideDrawerViewController)
            let leftSideNavController = UINavigationController(rootViewController: leftSideDrawerViewController)
            
            drawerController = DrawerController(centerViewController: navigationController, leftDrawerViewController: leftSideNavController, rightDrawerViewController: rightSideNavController)
            drawerController.showsShadows = false
            
            drawerController.maximumLeftDrawerWidth = 300.0
            drawerController.maximumRightDrawerWidth = 320.0
            drawerController.openDrawerGestureModeMask = .all
            drawerController.closeDrawerGestureModeMask = .all
            
            drawerController.drawerVisualStateBlock = { (drawerController, drawerSide, percentVisible) in
                let block = ExampleDrawerVisualStateManager.sharedManager.drawerVisualStateBlock(for: drawerSide)
                block?(drawerController, drawerSide, percentVisible)
            }
            
            self?.present(drawerController, animated: true, completion: {
            })            
        }
        
        let requestForCustomerRow = createMenu("Сделать заказ для клиента") {[weak self] (Void) in
            
            var drawerController: DrawerController
            
            let leftSideDrawerViewController = FilterProductsTVC()
            let centerViewController = MainDocumentVC(initialOrderDocument: .preOrderToCustomer)
            let rightSideDrawerViewController = ProductsBucketVC()
            
            let navigationController = UINavigationController(rootViewController: centerViewController)
            let rightSideNavController = UINavigationController(rootViewController: rightSideDrawerViewController)
            let leftSideNavController = UINavigationController(rootViewController: leftSideDrawerViewController)
            
            drawerController = DrawerController(centerViewController: navigationController, leftDrawerViewController: leftSideNavController, rightDrawerViewController: rightSideNavController)
            drawerController.showsShadows = false
            
            drawerController.maximumLeftDrawerWidth = 300.0
            drawerController.maximumRightDrawerWidth = 320.0
            drawerController.openDrawerGestureModeMask = .all
            drawerController.closeDrawerGestureModeMask = .all
            
            drawerController.drawerVisualStateBlock = { (drawerController, drawerSide, percentVisible) in
                let block = ExampleDrawerVisualStateManager.sharedManager.drawerVisualStateBlock(for: drawerSide)
                block?(drawerController, drawerSide, percentVisible)
            }
            
            self?.present(drawerController, animated: true, completion: {
            })
            
            
//            var drawerController: DrawerController
//
//            let leftSideDrawerViewController = ExampleLeftSideDrawerViewController()
//            let centerViewController = ExampleCenterTableViewController()
//            let rightSideDrawerViewController = ExampleRightSideDrawerViewController()
//            
//            let navigationController = UINavigationController(rootViewController: centerViewController)
//            navigationController.restorationIdentifier = "ExampleCenterNavigationControllerRestorationKey"
//            
//            let rightSideNavController = UINavigationController(rootViewController: rightSideDrawerViewController)
//            rightSideNavController.restorationIdentifier = "ExampleRightNavigationControllerRestorationKey"
//            
//            let leftSideNavController = UINavigationController(rootViewController: leftSideDrawerViewController)
//            leftSideNavController.restorationIdentifier = "ExampleLeftNavigationControllerRestorationKey"
//            
//            drawerController = DrawerController(centerViewController: navigationController, leftDrawerViewController: leftSideNavController, rightDrawerViewController: rightSideNavController)
//            drawerController.showsShadows = false
//            
//            drawerController.restorationIdentifier = "Drawer"
//            drawerController.maximumRightDrawerWidth = 200.0
//            drawerController.openDrawerGestureModeMask = .all
//            drawerController.closeDrawerGestureModeMask = .all
//            
//            drawerController.drawerVisualStateBlock = { (drawerController, drawerSide, percentVisible) in
//                let block = ExampleDrawerVisualStateManager.sharedManager.drawerVisualStateBlock(for: drawerSide)
//                block?(drawerController, drawerSide, percentVisible)
//            }
//            
//            self?.present(drawerController, animated: true, completion: {
//                
//            })
        }
        
        let productRow = createMenu("Товары (Склад)") {[weak self] (Void) in
            let productFiltersVC = ProductFilterListVC(initialString: nil)
            self?.navigationController?.pushViewController(productFiltersVC, animated: true)
        }
        
        let customersRow = createMenu("Клиенты") { [weak self] in
            let customerList = CustomersListVC(initialString: nil, isComingFromOrder: false)
            self?.navigationController?.pushViewController(customerList, animated: true)
        }
        
        let suppliersRow = createMenu("Поставщики") { [weak self] in
            let suppliersList = SuppliersListVC(initialString: nil, isCommingFromProduct: false, isComingFromOrder: false)
            self?.navigationController?.pushViewController(suppliersList, animated: true)
        }
        
        let staffRow = createMenu("Сотрудники") { [weak self] in
            let staffList = StaffListVC(initialString: nil, isCommingFromCustomer: false)
            self?.navigationController?.pushViewController(staffList, animated: true)
        }
        
        let additionalSettingsRow = createMenu("Настройки") { [weak self] in
            let settingsVC = SettingsVC(initialString: nil)
            self?.navigationController?.pushViewController(settingsVC, animated: true)
        }
        
        let logOutRow = createMenu("Выйти из аккаунта \nи удалить всю информацию") { [weak self] in
            
            let alert: UIAlertController = UIAlertController(title: "Выйти из аккаунта",
                                                             message: "Вы действительно хотите выйти из аккаунта \nи удалить всю информацию?",
                                                             preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
            
            alert.addAction(UIAlertAction(title: "Выйти", style: .default, handler: {
                (UIAlertAction) -> Void in
                
                Database.shared.countUnSyncedOperations {[weak self] (totalCount: Int32) in
                    if let strongSelf = self {
                        strongSelf.nonSyncedAmount = totalCount
                        
                        if totalCount > 0 {
                            let _ = strongSelf.showErrorAlert(text: "Нужно сохранить")
                        } else {
                            AppSession.shared.endSession()
                            let passcodeRepository = UserDefaultsPasscodeRepository()
                            passcodeRepository.delete()
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.enterLoginApplication()
                        }
                    }
                }
            }))
            
            self?.present(alert, animated: true, completion: nil)
        }
        
        // Create Headers and Footers
        let createHeader: ((String, UIColor) -> ViewFormer) = { text, color in
            return LabelViewFormer<FormLabelHeaderView>()
                .configure {
                    $0.text = text
                    $0.textColor = color
                    $0.viewHeight = 50
            }
        }
        
        let createFooter: ((String) -> ViewFormer) = { text in
            return LabelViewFormer<FormLabelFooterView>()
                .configure {
                    $0.text = text
                    $0.viewHeight = 55
            }
        }
        
        let syncSection = SectionFormer(rowFormer: syncRow, getStaticModelsRow)
        //.set(headerViewFormer: createHeader("", UIColor.clear))
        
        //let realExampleSection = SectionFormer(rowFormer: createSaleRow, createProductIncomeRow)
        //    .set(headerViewFormer: createHeader("Продажи", UIColor.greenSeaColor()))
        
        //let moneySection = SectionFormer(rowFormer: moneyReceiveRow, moneySentRow)
        //    .set(headerViewFormer: createHeader("Деньги", UIColor.nephritisColor()))
        
        let activitySection = SectionFormer(rowFormer: activityRow, statisticsRow)
            .set(headerViewFormer: createHeader("История", UIColor.belizeHoleColor()))
        //.set(footerViewFormer: createFooter("=== Дополнительно, редкое: ==="))
        
        
        //let resendSection = SectionFormer(rowFormer: receiveFromCustomerRow, resendSupplierRow)
        //    .set(headerViewFormer: createHeader("Возврат", UIColor.alizarinColor()))
        
        let defaultSection = SectionFormer(rowFormer: inventoryRow, requestForCustomerRow)
            .set(headerViewFormer: createHeader("Инвентаризация / Черновик", UIColor.wisteriaColor()))
            //.set(footerViewFormer: createFooter(""))
        
        //for (index, row) in syncSection.rowFormers.enumerated() {
        //    row.enabled = false
        //}
        
//        for (index, row) in realExampleSection.rowFormers.enumerated() {
//            row.enabled = false
//        }
//        for (index, row) in moneySection.rowFormers.enumerated() {
//            row.enabled = false
//        }
//        for (index, row) in resendSection.rowFormers.enumerated() {
//            row.enabled = false
//        }
//        for (index, row) in defaultSection.rowFormers.enumerated() {
//            row.enabled = false
//        }

        let settingsSection = SectionFormer(rowFormer: productRow, customersRow, suppliersRow, staffRow, additionalSettingsRow)
            .set(headerViewFormer: createHeader("Данные:", UIColor.carrotColor()))
            //.set(footerViewFormer: createFooter("Простое решение, для учета вашего склада"))
        
        let dangerSection = SectionFormer(rowFormer: logOutRow)
            .set(headerViewFormer: createHeader("", UIColor.pomergranateColor()))
        
        former.append(sectionFormer: syncSection, activitySection, defaultSection, settingsSection, dangerSection)
        
        //former.append(sectionFormer: syncSection, realExampleSection, moneySection, activitySection, resendSection, defaultSection, settingsSection, dangerSection)
    }
}

class TitleView2: UIView {
    
    var titleLabel: UILabel!
    var detailLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        titleLabel = UILabel()
        titleLabel.font = UIFont.systemFont(ofSize: 16, weight: UIFontWeightSemibold)
        titleLabel.textColor = UIColor.black
        titleLabel.textAlignment = .center
        addSubview(titleLabel)
        
        
        detailLabel = UILabel()
        detailLabel.font = UIFont.systemFont(ofSize: 12)
        detailLabel.textColor = UIColor.gray
        detailLabel.textAlignment = .center
        addSubview(detailLabel)
        
        detailLabel.text = "last seen yesterday at 5:56 PM"
        
        verticalLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func verticalLayout() {
        frame = CGRect(x: 0, y: 0, width: 200, height: 44)
        
        titleLabel.frame = CGRect(x: 0, y: 4, width: frame.width, height: 21)
        detailLabel.frame = CGRect(x: 0, y: titleLabel.frame.maxY + 2, width: frame.width, height: 15)
    }
    
    func horizontalLayout() {
        frame = CGRect(x: 0, y: 0, width: 300, height: 40)
        
        let titleSize = titleLabel.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: 21))
        let detailSize = detailLabel.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: 15))
        
        let contentWidth = titleSize.width + 6 + detailSize.width
        
        var currentX = frame.width / 2 - contentWidth / 2
        
        titleLabel.frame = CGRect(x: currentX, y: frame.height / 2 - titleSize.height / 2, width: titleSize.width, height: titleSize.height)
        
        currentX += titleSize.width + 6
        
        detailLabel.frame = CGRect(x: currentX, y: frame.height / 2 - detailSize.height / 2, width: detailSize.width, height: detailSize.height)
    }
}
