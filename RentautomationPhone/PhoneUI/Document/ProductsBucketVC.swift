//
//  ProductsBucketVC.swift
//  Rentautomation
//
//  Created by kanybek on 4/2/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit

class ProductsBucketVC: UIViewController, ErrorPopoverRenderer {
    
    @IBOutlet weak var ordersView: UIView!
    @IBOutlet weak var customerButton: UIButton!
    @IBOutlet weak var makeOrderButton: UIButton!
    @IBOutlet weak var totalSumLabel: UILabel!
    
    var totalPrice: Double = 0.0
    var totalDiscount: UInt32 = 0
    
    var totalSelectedOrederDetailsDispose: Disposable?
    var isFirstTime: Bool = true
    var initialString: String?
    
    lazy var selectedProductsTVC: SelectedProductsTVC = {
        let selectedProductListVC = SelectedProductsTVC()
        return selectedProductListVC
    }()
    
    convenience init() {
        self.init(initialString: nil)
    }
    
    init(initialString: String?) {
        self.initialString = initialString
        super.init(nibName: "ProductsBucketVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Корзина"
        //self.navigationController?.navigationBar.barTintColor = UIColor(red: 161 / 255, green: 164 / 255, blue: 166 / 255, alpha: 1.0)
        //self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(red: 55 / 255, green: 70 / 255, blue: 77 / 255, alpha: 1.0)]
        
        let buton2 = UIButton(type: .custom)
        buton2.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
        buton2.setImage(UIImage(named: "arrowleft32"), for: .normal)
        buton2.addTarget(self, action: #selector(self.leftPressed(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: buton2)
        
        let buton = UIButton(type: .custom)
        buton.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
        buton.setImage(UIImage(named: "cancel-32"), for: .normal)
        buton.addTarget(self, action: #selector(self.cancelPressed(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: buton)
        
        let totalSelectedOrederDetailsSignal: Signal = PipesStore.shared.totalSelectedOrderDetailsSignal()
        self.totalSelectedOrederDetailsDispose = totalSelectedOrederDetailsSignal.start(next: {[weak self] (orderDetails: [OrderDetail]) in
            
            if let strongSelf = self {
                
                var totalPrice_: Double = 0.0
                for orderDetail in orderDetails {
                    
                    let totalPriceWithDiscount = orderDetail.price * orderDetail.orderQuantity
                    totalPrice_ = totalPrice_ + totalPriceWithDiscount
                }
                
                strongSelf.totalPrice = totalPrice_
                print("total price is \(strongSelf.totalPrice)")
                strongSelf.totalSumLabel.text = Global.shared.numberFormatter.string(for: strongSelf.totalPrice)
                
                
                //let totalPriceWithDiscount = strongSelf.totalPrice * (1 - Double(strongSelf.totalDiscount) / 100)
                //strongSelf.totalSumLabelWithDiscount.text = Global.shared.numberFormatter.string(for: totalPriceWithDiscount)
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateUIElemts()
        if (isFirstTime) {
            self.add(asChildViewController: self.selectedProductsTVC, frame_: self.ordersView.frame)
            isFirstTime = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func cancelPressed(_ button: UIButton){
        self.dismiss(animated: true) {}
    }
    
    func leftPressed(_ button: UIButton){
        self.evo_drawerController?.closeDrawer(animated: true, completion: {[weak self] (f: Bool) in
            self?.navigationController?.popViewController(animated: false)
        })
    }
    
    @IBAction func customerDidPress(_ sender: Any) {
        let customerListVC = CustomersListVC(initialString: nil, isComingFromOrder: true)
        self.navigationController?.pushViewController(customerListVC, animated: true)
    }
    
    func updateUIElemts() -> Void {
        if OrderMemomry.shared.customerId > 0 {
            let customerId = OrderMemomry.shared.customerId
            if let customer__ = Database.shared.customerBy(customerId:customerId) {
                self.customerButton.setTitle("\(customer__.firstName) \(customer__.secondName)", for: .normal)
            }
        }
    }
    
    func updateLabel(_ sum: String) -> Void {
        totalSumLabel.text = sum
    }

    @IBAction func saveOrderDidPress(_ sender: Any) {
        
        if let uiNV_ = self.evo_drawerController?.centerViewController as? UINavigationController {
            if let controller_ = uiNV_.topViewController as? MainDocumentVC {
         
                var isSavedToDatabase: Bool = false
                
                //(1) orderDetails_ = self.orderDetails  //[OrderDetail]?
                //(2) let employee_ = AppSession.shared.currentEmployee //Staff
                //(3) OrderMemomry.shared.customerId // UInt64
                //(4) OrderMemomry.shared.supplierId // UInt64
                //(5) self.orderDocument // OrderDocumentType
                //(6) self.totalPrice // Double
                //(7) discount // Double
                
                switch controller_.initialOrderDocument {
                case .saleToCustomer, .returnFromCustomer, .saleToCustomerEdited, .preOrderToCustomer:
                    if OrderMemomry.shared.customerId < 1 {
                        let _ = self.showErrorAlert(text: "Выберите клиента!")
                        return
                    }
                case .receiveFromSupplier, .returnToSupplier:
                    if OrderMemomry.shared.supplierId < 1 {
                        let _ = self.showErrorAlert(text: "Выберите поставщика!")
                        return
                    }
                default:
                    print("")
                }
                
                if self.selectedProductsTVC.selectedProducts.count == 0 {
                    let _ = self.showErrorAlert(text: "Выберите товар(продукт)")
                    return
                }
                
                if let employee_ = AppSession.shared.currentEmployee {
                    
                    let orderDetails_ = self.selectedProductsTVC.selectedProducts
                    isSavedToDatabase = true
                    
                    let msgString = "\"" + Order.orderDocumentTypeStringName(orderDocType: controller_.initialOrderDocument) + "\""
                    let alert: UIAlertController = UIAlertController(title: "Сохранить документ",
                                                                     message: "Вы действительно хотите сохранить документ \(msgString)?",
                        preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
                    alert.addAction(UIAlertAction(title: "Сохранить", style: .default, handler: {
                        (UIAlertAction) -> Void in
                        self.performSaveOrder(senderVC: self,
                                              employee: employee_,
                                              orderDetails: orderDetails_,
                                              orderDocument: controller_.initialOrderDocument,
                                              totalPrice: self.totalPrice,
                                              totalDiscount: self.totalDiscount)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                
                if !isSavedToDatabase {
                    let _ = self.showErrorAlert(text: "Не все поля!")
                }
            }
        }
    }
    
    private func saveOrderDidPressed(_ sender: Any) {
        
        var isSavedToDatabase: Bool = false
        
        //(1) orderDetails_ = self.orderDetails  //[OrderDetail]?
        //(2) let employee_ = AppSession.shared.currentEmployee //Staff
        //(3) OrderMemomry.shared.customerId // UInt64
        //(4) OrderMemomry.shared.supplierId // UInt64
        //(5) self.orderDocument // OrderDocumentType
        //(6) self.totalPrice // Double
        //(7) discount // Double
        
//        switch orderDocument {
//        case .productOrderSaledToCustomer, .productReturnedFromCustomer, .productOrderSaleEditedToCustomer, .customerMadePreOrder:
//            if OrderMemomry.shared.customerId < 1 {
//                let _ = self.showErrorAlert(text: "Выберите клиента!")
//                return
//            }
//        case .productOrderReceivedFromSupplier, .productReturnedToSupplier:
//            if OrderMemomry.shared.supplierId < 1 {
//                let _ = self.showErrorAlert(text: "Выберите поставщика!")
//                return
//            }
//        default:
//            print("")
//        }
//        
//        if let orderDetails_ = self.orderDetails {
//            if orderDetails_.count == 0 {
//                let _ = self.showErrorAlert(text: "Выберите товар(продукт)")
//                return
//            }
//        }
//        
//        if let orderDetails_ = self.orderDetails, let employee_ = self.currentEmployee {
//            
//            isSavedToDatabase = true
//            
//            let msgString = "\"" + Order.orderDocumentTypeStringName(orderDocType: self.orderDocument) + "\""
//            let alert: UIAlertController = UIAlertController(title: "Сохранить документ",
//                                                             message: "Вы действительно хотите сохранить документ \(msgString)?",
//                preferredStyle: .alert)
//            
//            alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
//            
//            alert.addAction(UIAlertAction(title: "Сохранить", style: .default, handler: {
//                (UIAlertAction) -> Void in
//                self.documentCompanion.performSaveOrder(senderVC: self,
//                                                        employee: employee_,
//                                                        orderDetails: orderDetails_,
//                                                        orderDocument: self.orderDocument,
//                                                        totalPrice: self.totalPrice,
//                                                        totalDiscount: self.totalDiscount)
//            }))
//            self.present(alert, animated: true, completion: nil)
//        }
//        
//        if !isSavedToDatabase {
//            let _ = self.showErrorAlert(text: "Не все поля!")
//        }
    }
    
    
    func performSaveOrder<T: ProductsBucketVC>(senderVC: T,
                          employee: User,
                          orderDetails: [OrderDetail],
                          orderDocument: OrderDocumentType,
                          totalPrice: Double,
                          totalDiscount: UInt32) where T: ErrorPopoverRenderer {
        
        switch orderDocument {
            
        case .preOrderToCustomer:
            print("Заказ для клиента")
            if OrderMemomry.shared.customerId > 0 {
                
                let customerId = OrderMemomry.shared.customerId
                let _ = OrderManager.shared.ownerMadePreOrderForCustomer(orderDetails: orderDetails,
                                                                         staff: employee,
                                                                         customerId: customerId,
                                                                         orderDocType: orderDocument,
                                                                         totalPrice: totalPrice,
                                                                         discount: Discount())
                OrderMemomry.shared.clearAllVariables()
                senderVC.navigationController?.dismiss(animated: true, completion: {[weak self] in
                    senderVC.showSuccessAlert(text: "Документ успешно сохранен!")
                    print("dismiss MainDocumentVC")
                })
            }
            
        case .stockTaking:
            print("Инвентаризация / Черновик")
            let customerId = OrderMemomry.shared.customerId
            let _ = OrderManager.shared.ownerMadePreOrderForCustomer(orderDetails: orderDetails,
                                                                     staff: employee,
                                                                     customerId: customerId,
                                                                     orderDocType: orderDocument,
                                                                     totalPrice: totalPrice,
                                                                     discount: Discount())
            OrderMemomry.shared.clearAllVariables()
            senderVC.navigationController?.dismiss(animated: true, completion: {[weak self] in
                senderVC.showSuccessAlert(text: "Документ успешно сохранен!")
                print("dismiss MainDocumentVC")
            })
        default:
            print("default:")
        }
    }
    
}
