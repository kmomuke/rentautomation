//
//  MainDocumentVC.swift
//  Rentautomation
//
//  Created by kanybek on 4/2/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit
import Kingfisher
import CSNotificationView
import MZFormSheetPresentationController
import BBBadgeBarButtonItem

class MainDocumentVC: UIViewController, ErrorPopoverRenderer, UITableViewDelegate, UITableViewDataSource {
    
    var initialOrder: Order?
    var notificationView: CSNotificationView?
    
    var products: [Product] = []
    var badgeDict: [UInt64: Int] = [:]
    var barButton: BBBadgeBarButtonItem?
    
    var initialOrderDocument: OrderDocumentType
    
    var priceString: String = ""
    var shouldUseSalePrice: Bool = false
    
    var productRemovedDispose: Disposable?
    var totalSelectedOrederDetailsDispose: Disposable?
    
    var initialString: String?
    @IBOutlet weak var searchBarGlobal: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    convenience init() {
        self.init(initialOrderDocument: .none)
    }
    
    init(initialOrderDocument: OrderDocumentType) {
        self.initialOrderDocument = initialOrderDocument
        OrderMemomry.shared.clearAllVariables()
        OrderMemomry.shared.orderDocument = initialOrderDocument
        super.init(nibName: "MainDocumentVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.productRemovedDispose?.dispose()
        self.productRemovedDispose = nil
        self.totalSelectedOrederDetailsDispose?.dispose()
        self.totalSelectedOrederDetailsDispose = nil
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchBarGlobal.delegate = self
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        //self.evo_drawerController?.closeDrawerGestureModeMask
        
        self.title = "Товары"
        
        switch self.initialOrderDocument {
            
        case .saleToCustomer:
            self.title = "Продажа"
        case .receiveFromSupplier:
            self.title = "Закупка"
        case .returnFromCustomer:
            self.title = "Возврат продукта от клиента"
        case .returnToSupplier:
            self.title = "Возврат продукта к потавщику"
        case .saleToCustomerEdited:
            self.title = "Редактирование Продажи"
        case .receiveFromSupplierEdited:
            self.title = "Редактирование Закупки"
        case .preOrderToCustomer, .preOrderToCustomerEdited:
            self.title = "Заказ для клиента"
        case .stockTaking:
            self.title = "Инвентаризация / Черновик"
        default:
            print("default:")
        }
        
        let nib = UINib(nibName: "ProductsListTVCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "ProductsListTVCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let buton = UIButton(type: .custom)
        buton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        buton.setImage(UIImage(named: "shopping_50"), for: .normal)
        buton.addTarget(self, action: #selector(self.bucketPressed(_:)), for: .touchUpInside)
        
        self.barButton = BBBadgeBarButtonItem(customUIButton: buton)
        self.barButton?.shouldAnimateBadge = true
        self.navigationItem.rightBarButtonItem = self.barButton
        self.setupLeftMenuButton()
        
        self.takeIntialDataFromDatabase()
        
        let productRemovedSignal: Signal = PipesStore.shared.productRemovedFromSaleListSignal()
        self.productRemovedDispose = productRemovedSignal.start(next: {[weak self] (orderDetail: OrderDetail) in
            if let indexFound = self?.products.index(where: {$0.productId == orderDetail.productId}) {
                self?.badgeDict[orderDetail.productId] = 0
                let indexPath_ = IndexPath(row: indexFound, section: 0)
                self?.tableView.reloadRows(at: [indexPath_], with: .none)
            } else {
                self?.badgeDict[orderDetail.productId] = 0
            }
        })
        
        let totalSelectedOrederDetailsSignal: Signal = PipesStore.shared.totalSelectedOrderDetailsSignal()
        self.totalSelectedOrederDetailsDispose = totalSelectedOrederDetailsSignal.start(next: {[weak self] (orderDetails: [OrderDetail]) in
            
            if let strongSelf = self {
                
                //strongSelf.orderDetails = orderDetails
                
                var totalPrice_: Double = 0.0
                for orderDetail in orderDetails {
                    
                    let totalPriceWithDiscount = orderDetail.price * orderDetail.orderQuantity
                    totalPrice_ = totalPrice_ + totalPriceWithDiscount
                }
                
                strongSelf.barButton?.badgeValue = "\(orderDetails.count)"
                
                //strongSelf.totalPrice = totalPrice_
                //print("total price is \(strongSelf.totalPrice)")
                //strongSelf.totalSumLabel.text = Global.shared.numberFormatter.string(for: strongSelf.totalPrice)
                
                
                let totalPriceWithDiscount = totalPrice_ * (1 - Double(0) / 100)
                let totalSumText = Global.shared.numberFormatter.string(for: totalPriceWithDiscount)
                
                if let mainDocumentNVC = self?.evo_drawerController?.rightDrawerViewController as? UINavigationController {
                    if let rootVC = mainDocumentNVC.topViewController as? ProductsBucketVC {
                        if let totalSumText_ = totalSumText {
                            rootVC.updateLabel(totalSumText_)
                        }
                    }
                }
            }
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.evo_drawerController?.bouncePreview(for: .right, distance: 20.0, completion: { (value: Bool) in
        })
    }

    func keyboardWillShow(_ notification: NSNotification){
    }
    
    func keyboardWillHide(_ notification: NSNotification){
    }
    
    func setupLeftMenuButton() {
        let buton = UIButton(type: .custom)
        buton.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
        buton.setImage(UIImage(named: "filter50"), for: .normal)
        buton.addTarget(self, action: #selector(self.filterPressed(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: buton)
    }
    
    func filterPressed(_ button: UIButton){
        self.evo_drawerController?.openDrawerSide(.left, animated: true, completion: { (value: Bool) in
        })
    }
    
    func bucketPressed(_ button: UIButton){
        self.evo_drawerController?.openDrawerSide(.right, animated: true, completion: { (value: Bool) in
        })
    }
    
    private func takeIntialDataFromDatabase() -> Void {
        Database.shared.loadAllProductsBy(categoryId: 0,
                                          limit: Global.shared.limitForStaticModels) {[weak self] (products_: [Product]) in
                                            
                                            self?.products = products_
                                            
                                            for prod in products_ {
                                                let key = prod.productId
                                                let value = 0
                                                self?.badgeDict[key] = value
                                            }
                                            
                                            if let order_ = self?.initialOrder {
                                                let orderDetails = Database.shared.orderDetailsBy(orderId: order_.orderId)
                                                for orderDetail in orderDetails {
                                                    self?.badgeDict[orderDetail.productId] = 1
                                                }
                                            }
                                            
                                            self?.tableView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func reloadTableWithCategory(_ category: Category) -> Void {
        self.showNotificationView(category.categoryName)
        Database.shared.loadAllProductsBy(categoryId: category.categoryId,
                                          limit: Global.shared.limitForStaticModels,
                                          completion: {[weak self] (products_: [Product]) in
                                            self?.products = products_
                                            self?.tableView.reloadData()
        })
    }
    
    func reloadTableWithSupplier(_ supplier: Supplier) -> Void {
        self.showNotificationView(supplier.companyName)
        Database.shared.loadAllProductsBy(supplierId: supplier.supplierId,
                                          limit: Global.shared.limitForStaticModels,
                                          completion: {[weak self] (products_: [Product]) in
                                            self?.products = products_
                                            self?.tableView.reloadData()
        })
    }
    
    private func reloadAllDataFromDatabase() -> Void {
        Database.shared.loadAllProductsBy(categoryId: 0,
                                          limit: Global.shared.limitForStaticModels) {[weak self] (products_: [Product]) in
                                            self?.products = products_
                                            self?.tableView.reloadData()
        }
    }
    
    public func reloadTableData(_ products_: [Product]) -> Void {
        self.products = products_
        self.tableView.reloadData()
    }
    
    public func showNotificationView(_ message_: String) -> Void {
        self.hideNotificationView {[weak self] (Void) in
            if let strongSelf = self {
                
                strongSelf.notificationView = CSNotificationView(parentViewController: strongSelf,
                                                           tintColor: UIColor.midnightBlueColor(),
                                                           image: UIImage(named: "close-X")!,
                                                           message: message_)

                strongSelf.notificationView?.tapHandler = {[weak self] (Void) -> Void in
                    self?.hideNotificationView({[weak self] (Void) in
                        self?.reloadAllDataFromDatabase()
                    })
                }
                
                strongSelf.notificationView?.setVisible(true, animated: true, completion: {[weak self] in
                    if let strongSelf = self {
                        print("show completed")
                    }
                })
            }
        }
    }
    
    public func hideNotificationView(_ completion: @escaping (Void) -> ()) -> Void {
        if let notifView = self.notificationView {
            notifView.setVisible(false, animated: false, completion: {[weak self] in
                if let strongSelf = self {
                    print("hide completed")
                    completion()
                }
            })
        } else {
            completion()
        }
    }
    
    func showScaner() -> Void {
        let scanerVC = ScanQRCodeVC(initialItem: "cameFromIphone")
        scanerVC.scanerScannedBlock = {[weak self] (barcode: String) -> Void in
            print("barcode == \(barcode)")
            
            if let strongSelf = self {
                if let product_ = Database.shared.productFor(barcode: barcode) {
                    
                    if let _ = strongSelf.badgeDict[product_.productId] {
                        strongSelf.badgeDict[product_.productId] = 1
                    }
                    
                    if let indexFound = strongSelf.products.index(where: {$0.productId == product_.productId}) {
                        strongSelf.tableView.reloadRows(at: [IndexPath(row: indexFound, section: 0)], with: .fade)
                    }
                    
                    PipesStore.shared.productSelectedPipe.putNext((product_, strongSelf.shouldUseSalePrice))
                } else {
                    let _ = strongSelf.showErrorAlert(text: "Не найдено!")
                }
            }
        }
        
        let navController = RNNavigationController(rootViewController: scanerVC)
        let formSheetController = MZFormSheetPresentationViewController(contentViewController: navController)
        formSheetController.allowDismissByPanningPresentedView = true
        formSheetController.contentViewControllerTransitionStyle = .slideAndBounceFromBottom
        formSheetController.presentationController?.contentViewSize = CGSize(width: 300, height: 300)
        self.present(formSheetController, animated: true, completion:{_ in
        })
    }
    
    //
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 81.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductsListTVCell", for: indexPath) as! ProductsListTVCell
        let product = products[(indexPath as NSIndexPath).row]
        //cell.product = product
        //cell.reloadData()
        
        if Global.shared.isDebug {
            cell.productPrimaryKeyLabel.text = "\(product.productId)"
            cell.productPrimaryKeyLabel.backgroundColor = UIColor.clear
        } else {
            cell.productPrimaryKeyLabel.isHidden = true
        }
        
        cell.incomePriceLabel.text = ""
        
        var price: Double = 0.0
        if self.shouldUseSalePrice {
            price = product.saleUnitPrice
        } else {
            price = product.incomeUnitPrice
        }
        
        if let stringPrice = Global.shared.numberFormatter.string(for: price) {
            cell.salePriceLabel.text = "\(self.priceString) \(stringPrice)"
        }
        
        cell.nameLabel.text = product.productName
        
        
        var unitsInStock = ""
        if let text_ = Global.shared.numberFormatterForAmount.string(for: product.unitsInStock) {
            unitsInStock = text_
        }
        cell.productKeyLabel.text = "ост: \(unitsInStock) \(product.quantityPerUnit)"
        
        if let url_ = product.productImagePath {
            
            if url_.characters.count > 4 {
                let url = URL(string: url_)
                if url_.lowercased().range(of:"https") != nil {
                    let processor = ResizingImageProcessor(referenceSize: CGSize(width: 100, height: 100), mode: .none)
                    cell.cellImageView.kf.indicatorType = .activity
                    cell.cellImageView.kf.setImage(with: url,
                                                   placeholder: Global.shared.productPlaceholderImage,
                                                   options: [.processor(processor), .transition(.fade(0.2))],
                                                   progressBlock: nil,
                                                   completionHandler: { (im: Image?, err: NSError?, typr: CacheType, url: URL?) in
                    })
                } else {
                    cell.cellImageView.kf.setImage(with: url,
                                                   placeholder: Global.shared.productPlaceholderImage,
                                                   options: nil,
                                                   progressBlock: nil,
                                                   completionHandler: { (im: Image?, err: NSError?, typr: CacheType, url: URL?) in
                    })
                }
            } else {
                cell.cellImageView.image = Global.shared.productPlaceholderImage
            }
        } else {
            cell.cellImageView.image = Global.shared.productPlaceholderImage
        }
        
        //App installation failed
        //A valid provision profile for this executable was not found
        if let badgeValue = self.badgeDict[product.productId] {
            if badgeValue > 0 {
                cell.badgeString = "\(badgeValue)"
            } else {
                cell.badgeString = ""
            }
        } else {
            cell.badgeString = ""
        }
        
        if Database.shared.isModelSynchronized(primaryKey: product.productId) {
            cell.selectionStyle = .default
            cell.nameLabel.textColor = UIColor.black
        } else {
            cell.selectionStyle = .none
            cell.nameLabel.textColor = UIColor.pomergranateColor()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let product = products[(indexPath as NSIndexPath).row]
        self.didSelectProduct(product_: product, indexPath_: indexPath)
    }
    
    private func didSelectProduct(product_: Product, indexPath_: IndexPath) {
        if Database.shared.isModelSynchronized(primaryKey: product_.productId) {
            
            if let _ = self.badgeDict[product_.productId] {
                self.badgeDict[product_.productId] = 1
            }
            
            tableView.reloadRows(at: [indexPath_], with: .none)
            
            PipesStore.shared.productSelectedPipe.putNext((product_, self.shouldUseSalePrice))
        } else {
            let _ = self.showErrorAlert(text: "не сохранен")
        }
    }
}

extension MainDocumentVC: UISearchBarDelegate {
    
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("searchBarTextDidBeginEditing")
    }
    
    public func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print("searchBarTextDidEndEditing")
    }
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)")
        if searchText.characters.count > 0 {
            self.showLoading("Поиск")
            Database.shared.search(productName: searchText, completion: {[weak self] (products_: [Product]) in
                self?.dismissLoading()
                self?.products = products_
                self?.tableView.reloadData()
            })
        } else {
            Database.shared.loadAllProductsBy(categoryId: 0,
                                              limit: Global.shared.limitForStaticModels,
                                              completion: {[weak self] (products_: [Product]) in
                                                self?.products = products_
                                                self?.tableView.reloadData()
            })
        }
    }
    
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("searchBarSearchButtonClicked ==> \(searchBar.text!)")
        searchBar.resignFirstResponder()
    }
    
    public func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        print("searchBarBookmarkButtonClicked")
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("searchBarCancelButtonClicked")
        Database.shared.loadAllProductsBy(categoryId: 0,
                                          limit: Global.shared.limitForStaticModels,
                                          completion: {[weak self] (products_: [Product]) in
                                            self?.products = products_
                                            self?.tableView.reloadData()
        })
    }
    
    public func searchBarResultsListButtonClicked(_ searchBar: UISearchBar) {
        print("searchBarResultsListButtonClicked")
    }
    
    public func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        print("searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope")
    }
}
