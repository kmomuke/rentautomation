//
//  SelectedProductsTVC.swift
//  Rentautomation
//
//  Created by kanybek on 4/3/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit

class SelectedProductsTVC: UITableViewController {
    
    var initialOrder: Order?
    var productSelectedDispose: Disposable?
    var selectedProducts: [OrderDetail] = []
    
    convenience init() {
        self.init(initialOrder: nil)
    }
    
    init(initialOrder: Order?) {
        self.initialOrder = initialOrder
        super.init(nibName: "SelectedProductsTVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.productSelectedDispose?.dispose()
        self.productSelectedDispose = nil
    }
    
    internal func focus(gesture: UITapGestureRecognizer) {
        UIApplication.shared.keyWindow?.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "SelectedProductTVCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "SelectedProductTVCell")
        
        if let initialOrder_ = self.initialOrder {
            let orderDetails = Database.shared.orderDetailsBy(orderId: initialOrder_.orderId)
            self.selectedProducts = orderDetails
            self.tableView.reloadData()
        }
        
        let productSelectedSignal: Signal = PipesStore.shared.productSelectedSignal()
        self.productSelectedDispose = productSelectedSignal.start(next: {[weak self] (product: Product, isSalePrice: Bool) in
            
            if let indexFound = self?.selectedProducts.index(where: {$0.productId == product.productId}) {
                
                if let orderDetail = self?.selectedProducts[indexFound] {
                    orderDetail.orderQuantity = orderDetail.orderQuantity + Double(1)
                    self?.selectedProducts[indexFound] = orderDetail
                    self?.tableView.reloadRows(at: [IndexPath(row: indexFound, section: 0)], with: .fade)
                }
                
            } else {
                
                let orderDetail = OrderDetail()
                orderDetail.productId = product.productId
                orderDetail.orderQuantity = Double(1)
                orderDetail.orderDetailDate = Date()
                
                if isSalePrice {
                    orderDetail.price = product.saleUnitPrice
                } else {
                    orderDetail.price = product.incomeUnitPrice
                }
                
                 
                self?.selectedProducts.insert(orderDetail, at: 0)
                self?.tableView.beginUpdates()
                self?.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .top)
                self?.tableView.endUpdates()
                
                self?.reloadTableAfterSecond() // ???
            }
            
            PipesStore.shared.totalSelectedOrderDetailsPipe.putNext((self?.selectedProducts)!)
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func reloadTableAfterSecond() -> Void {
        //let delay = DispatchTime.now() + DispatchTimeInterval.milliseconds(500)
        //DispatchQueue.main.asyncAfter(deadline: delay, execute: {
        //    self.tableView.reloadData()
        //})
    }
    
    // MARK: - Table view data source
    //override func scrollViewDidScroll(_ scrollView: UIScrollView) {
    //    self.tableView.endEditing(true)
    //}
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedProducts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectedProductTVCell", for: indexPath) as! SelectedProductTVCell
        let orderDetail = selectedProducts[(indexPath as NSIndexPath).row]
        cell.orderDetail = orderDetail
        cell.countNumberLabel.text = "\(indexPath.row + 1)"
        cell.deleteBlock = {[weak self] (Void) -> Void in
            
            if let indexFound = self?.selectedProducts.index(where: {$0.productId == orderDetail.productId}) {
                PipesStore.shared.productRemovedFromSaleListPipe.putNext(orderDetail)
                
                self?.selectedProducts.remove(at: indexFound)
                let indexPath_ = IndexPath(row: indexFound, section: 0)
                self?.tableView.deleteRows(at: [indexPath_], with: .none)
                PipesStore.shared.totalSelectedOrderDetailsPipe.putNext((self?.selectedProducts)!)
                
                self?.reloadTableAfterSecond()
            }
        }
        
        cell.quantityEditinEndBlock = {[weak self] (Void) -> Void in
            PipesStore.shared.totalSelectedOrderDetailsPipe.putNext((self?.selectedProducts)!)
        }
        
        // -------- AAAAAAAA ---------
        cell.dicsountBlock = {[weak self]  (button: UIButton, orderDetail: OrderDetail?) -> Void in
        }
        // -------- FINISH ---------
        
        // -------- AAAAAAAA ---------
        cell.priceButtonBlock = {[weak self]  (button: UIButton, orderDetail_: OrderDetail?) -> Void in
            
        }
        // -------- FINISH ---------
        
        cell.reloadData()
        return cell
    }
    
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        
    }
    
}
