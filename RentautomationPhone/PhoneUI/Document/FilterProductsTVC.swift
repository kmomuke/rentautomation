//
//  FilterProductsTVC.swift
//  Rentautomation
//
//  Created by kanybek on 4/3/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class FilterProductsTVC: UITableViewController {
    
    let localTitles: [String] = ["Сканер", "Категория", "Поставщик"]
    var initialString: String?
    
    convenience init() {
        self.init(initialString: nil)
    }
    
    init(initialString: String?) {
        self.initialString = initialString
        super.init(nibName: "FilterProductsTVC", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Фильтры"
        self.setupLeftMenuButton()
        let nib = UINib(nibName: "CategoriesListTVCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "CategoriesListTVCell")
        
        //self.tableView.backgroundColor = UIColor(red: 110 / 255, green: 113 / 255, blue: 115 / 255, alpha: 1.0)
        //self.navigationController?.navigationBar.barTintColor = UIColor(red: 161 / 255, green: 164 / 255, blue: 166 / 255, alpha: 1.0)
        //self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(red: 55 / 255, green: 70 / 255, blue: 77 / 255, alpha: 1.0)]
        //self.view.backgroundColor = UIColor.clear
    }
    
    func setupLeftMenuButton() {
        
        let buton = UIButton(type: .custom)
        buton.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
        buton.setImage(UIImage(named: "cancel-32"), for: .normal)
        buton.addTarget(self, action: #selector(self.cancelPressed(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: buton)
        
        let buton2 = UIButton(type: .custom)
        buton2.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
        buton2.setImage(UIImage(named: "arrowright32"), for: .normal)
        buton2.addTarget(self, action: #selector(self.rightPressed(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: buton2)
    }
    
    func cancelPressed(_ button: UIButton){
        self.dismiss(animated: true) {}
    }
    
    func rightPressed(_ button: UIButton){
        self.evo_drawerController?.closeDrawer(animated: true, completion: {[weak self] (f: Bool) in
            self?.navigationController?.popViewController(animated: false)
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.localTitles.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoriesListTVCell", for: indexPath) as! CategoriesListTVCell
        let object = self.localTitles[(indexPath as NSIndexPath).row]
        cell.categoryNameLabel.text = "\(object)"
        cell.categoryPrimaryKeyLabel.isHidden = true
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 0 {
            //self.evo_drawerController?.centerHiddenInteractionMode
            self.evo_drawerController?.closeDrawer(animated: true, completion: { (finished: Bool) in
                if let mainDocumentNVC = self.evo_drawerController?.centerViewController as? UINavigationController {
                    if let rootVC = mainDocumentNVC.topViewController as? MainDocumentVC {
                        rootVC.showScaner()
                    }
                }
            })
        } else if indexPath.row == 1 {
            let categoriesListVC = CategoriesListVC(initialString: "comeFromSaleOrder", isCommingFromProduct: true)
            categoriesListVC.categorySelectBlock = {[weak self]  (category: Category) -> Void in
                print("category.name = \(category.categoryName)")
                
                self?.evo_drawerController?.closeDrawer(animated: true, completion: { (finished: Bool) in
                    if let mainDocumentNVC = self?.evo_drawerController?.centerViewController as? UINavigationController {
                        if let rootVC = mainDocumentNVC.topViewController as? MainDocumentVC {
                            rootVC.reloadTableWithCategory(category)
                        }
                    }
                })
            }
            self.navigationController?.pushViewController(categoriesListVC, animated: true)
        } else if indexPath.row == 2 {
            
            let suppliersListVC = SuppliersListVC(initialString: "comeFromSaleOrder", isCommingFromProduct: true, isComingFromOrder: false)
            suppliersListVC.supplierSelectBlock = {[weak self]  (supplier: Supplier) -> Void in
                print("supplier.companyName = \(supplier.companyName)")
                
                self?.evo_drawerController?.closeDrawer(animated: true, completion: { (finished: Bool) in
                    if let mainDocumentNVC = self?.evo_drawerController?.centerViewController as? UINavigationController {
                        if let rootVC = mainDocumentNVC.topViewController as? MainDocumentVC {
                            rootVC.reloadTableWithSupplier(supplier)
                        }
                    }
                })
            }
            
            self.navigationController?.pushViewController(suppliersListVC, animated: true)
        } else if indexPath.row == 3 {
        }
    }
}
