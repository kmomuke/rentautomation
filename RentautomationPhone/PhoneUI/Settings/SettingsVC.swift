//
//  SettingsVC.swift
//  Rentautomation
//
//  Created by kanybek on 3/19/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class SettingsVC: FormViewController, ErrorPopoverRenderer {
    
    var initialString: String?
    private let repository: UserDefaultsPasscodeRepository
    private let configuration: PasscodeLockConfigurationType
    
    convenience init() {
        self.init(initialString: nil)
    }
    
    init(initialString: String?) {
        self.initialString = initialString
        self.repository = UserDefaultsPasscodeRepository()
        self.configuration = PasscodeLockConfiguration(repository: repository)
        super.init(nibName: nil, bundle: nil)
        let (image1, image2) = Global.shared.settingsIconImage()
        tabBarItem = UITabBarItem(title: "Настройки", image: image1, selectedImage: image2)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
    }
    
    // MARK: Public
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Настройки"
        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
    }
    
    // MARK: Private
    
    private func pushSelectorRowSelected(options: [String]) -> (RowFormer) -> Void {
        return { [weak self] rowFormer in
            if let rowFormer = rowFormer as? LabelRowFormer<FormLabelCell> {
                let controller = TextSelectorViewContoller()
                controller.texts = options
                controller.selectedText = rowFormer.subText
                controller.onSelected = {
                    rowFormer.subText = $0
                    rowFormer.update()
                    
                    if $0 == "$" {
                        print("$")
                        AppSettings.shared.removeDeviceCurrency()
                        AppSettings.shared.saveDeviceCurrency("$")
                        Global.shared.numberFormatter.currencySymbol = "$"
                    } else if $0 == "сом" {
                        print("сом")
                        AppSettings.shared.removeDeviceCurrency()
                        AppSettings.shared.saveDeviceCurrency("сом")
                        Global.shared.numberFormatter.currencySymbol = "сом"
                    } else if $0 == "руб" {
                        print("руб")
                        AppSettings.shared.removeDeviceCurrency()
                        AppSettings.shared.saveDeviceCurrency("руб")
                        Global.shared.numberFormatter.currencySymbol = "руб"
                    }
                    
                }
                self?.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    private func pushSelectorRowSelected2(options: [String]) -> (RowFormer) -> Void {
        return { [weak self] rowFormer in
            if let rowFormer = rowFormer as? LabelRowFormer<FormLabelCell> {
                let controller = TextSelectorViewContoller()
                controller.texts = options
                controller.selectedText = rowFormer.subText
                controller.onSelected = {
                    rowFormer.subText = $0
                    rowFormer.update()
                    
                    if $0 == "0" {
                        AppSettings.shared.savemaximumFractionDigits(0)
                        BKCurrencyTextField.setIdGen(0)
                        Global.shared.numberFormatter.maximumFractionDigits = 0
                        Global.shared.numberFormatterForAmount.maximumFractionDigits = 0
                    } else if $0 == "1" {
                        AppSettings.shared.savemaximumFractionDigits(1)
                        BKCurrencyTextField.setIdGen(1)
                        Global.shared.numberFormatter.maximumFractionDigits = 1
                        Global.shared.numberFormatterForAmount.maximumFractionDigits = 1
                    } else if $0 == "2" {
                        AppSettings.shared.savemaximumFractionDigits(2)
                        BKCurrencyTextField.setIdGen(2)
                        Global.shared.numberFormatter.maximumFractionDigits = 2
                        Global.shared.numberFormatterForAmount.maximumFractionDigits = 2
                    }
                }
                self?.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    private func configure() {
        
        //let logo = UIImage(named: "TGUserInfo.png")!
        //navigationItem.titleView = UIImageView(image: logo)
        
        let backBarButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backBarButton
        
        // Create RowFormers
        let createMenu: ((String, (() -> Void)?) -> RowFormer) = { text, onSelected in
            return LabelRowFormer<FormLabelCell>() {
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                }.configure {
                    $0.text = text
                    $0.rowHeight = 50
                }.onSelected { _ in
                    onSelected?()
            }
        }
        
        let switchDateStyleRow = SwitchRowFormer<FormSwitchCell>() {
            $0.titleLabel.text = "Установить пароль"
            $0.titleLabel.textColor = .formerColor()
            $0.titleLabel.font = .boldSystemFont(ofSize: 16)
            $0.switchButton.onTintColor = .formerSubColor()
            }.configure {
                //$0.switched = false
                let hasPasscode = configuration.repository.hasPasscode
                $0.switched = hasPasscode
                $0.rowHeight = 50
                
            }.onSwitchChanged {[weak self] switched in
                
                if let strongSelf = self {
                    let passcodeVC: PasscodeLockViewController
                    if switched {
                        passcodeVC = PasscodeLockViewController(state: .set, configuration: strongSelf.configuration)
                    } else {
                        passcodeVC = PasscodeLockViewController(state: .remove, configuration: strongSelf.configuration)
                    }
                    
                    passcodeVC.dismissCompletionCallback = {(Void) in
                        print("dismissCompletionCallback")
                        //let hasPasscode = self?.configuration.repository.hasPasscode
                        //switchDateStyleRow.switched = hasPasscode
                    }
                    
                    passcodeVC.successCallback = { (_ lock: PasscodeLockType) in
                        if lock.repository.hasPasscode {
                            print("hasPasscode true")
                        } else {
                            print("hasPasscode false")
                        }
                        print("successCallback")
                    }
                    
                    strongSelf.present(passcodeVC, animated: true, completion: nil)
                }
        }
        let changePasswordRow = createMenu("Поменять пароль") { [weak self] in
            let repo = UserDefaultsPasscodeRepository()
            let config = PasscodeLockConfiguration(repository: repo)
            let passcodeLock = PasscodeLockViewController(state: .change, configuration: config)
            self?.present(passcodeLock, animated: true, completion: nil)
        }
        
        let createSelectorRow = { (
            text: String,
            subText: String,
            onSelected: ((RowFormer) -> Void)?
            ) -> RowFormer in
            return LabelRowFormer<FormLabelCell>() {
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = .boldSystemFont(ofSize: 16)
                $0.subTextLabel.textColor = .formerSubColor()
                $0.subTextLabel.font = .boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                }.configure { form in
                    _ = onSelected.map { form.onSelected($0) }
                    form.text = text
                    form.subText = subText
                    form.rowHeight = 50
            }
        }
        
        let options = ["$", "сом", "руб"]
        let currentCurrency = AppSettings.shared.currentCurrency()
        let currencyRow = createSelectorRow("Валюта", currentCurrency, pushSelectorRowSelected(options: options))
        
        
        let options2 = ["0", "1", "2"]
        let currentCurrency2 = AppSettings.shared.getMaximumFractionDigits()
        let currencyRow2 = createSelectorRow("Точка", "\(currentCurrency2)", pushSelectorRowSelected2(options: options2))
        
        let ipAddress = AppSettings.shared.currentIpAddress()
        let last7 = String(ipAddress.characters.suffix(7))
        
        let customCellRow = createMenu("Neon examples \(last7)") { [weak self] in
            if let strongSelf = self {
            }
        }
        
        let stopAllProcessRow = createMenu("Остановить синхронизацию") {[weak self] in
            self?.former.deselect(animated: true)
            SynchronizeManager.sharedInstance.stopAllSyncProcess()
        }
        
        let getStaticModelsRow = createMenu("Обновить все данные из сервера") {[weak self] in
            self?.former.deselect(animated: true)
            let alert: UIAlertController = UIAlertController(title: "Обновить все",
                                                             message: "Вы действительно хотите обновить все данные из сервера?",
                                                             preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
            
            alert.addAction(UIAlertAction(title: "Обновить", style: .default, handler: {
                (UIAlertAction) -> Void in
                
                Database.shared.countUnSyncedOperations {[weak self] (totalCount: Int32) in
                    if let strongSelf = self {
                        strongSelf.showLoading("Получение статических данных ...")
                        SynchronizeManager.sharedInstance.getInitialStaticModels(completion: {[weak self] (boolValue: Bool) in
                            DispatchQueue.main.async {
                                self?.dismissLoading()
                            }
                        })
                    }
                }
            }))
            
            self?.present(alert, animated: true, completion: nil)
        }
        
        let checkDataRow = createMenu("Свертка всех данных из базы") {[weak self] in
            self?.former.deselect(animated: true)
            if let strongSelf = self {
                let compareDataVC = CompareDataVC()
                strongSelf.navigationController?.pushViewController(compareDataVC, animated: true)
            }
        }
        
        let releaseVersionRow = LabelRowFormer<FormLabelCell>()
            .configure {
                $0.rowHeight = 50
                $0.text = "Версия"
                if let releaseVersion = Bundle.main.releaseVersionNumber {
                    $0.subText = releaseVersion
                }
            }.onSelected { [weak self] _ in
                self?.former.deselect(animated: true)
            }.onUpdate {
                $0.text = "Версия"
                //$0.subText = "10 000"
        }
        
        let buildVersionNumberRow = LabelRowFormer<FormLabelCell>()
            .configure {
                $0.rowHeight = 50
                $0.text = "Сборка"
                if let buildVersion = Bundle.main.buildVersionNumber {
                    $0.subText = buildVersion
                }
            }.onSelected { [weak self] _ in
                self?.former.deselect(animated: true)
            }.onUpdate {
                $0.text = "Сборка"
                //$0.subText = "10 000"
        }
        
        // Create Headers and Footers
        let createHeader: ((String) -> ViewFormer) = { text in
            return LabelViewFormer<FormLabelHeaderView>()
                .configure {
                    $0.text = text
                    $0.viewHeight = 40
            }
        }
        
        let createFooter: ((String) -> ViewFormer) = { text in
            return LabelViewFormer<FormLabelFooterView>()
                .configure {
                    $0.text = text
                    $0.viewHeight = 100
            }
        }
        
        let developerModeRow = SwitchRowFormer<FormSwitchCell>() {
            $0.titleLabel.text = "Developer mode"
            $0.titleLabel.textColor = .formerColor()
            $0.titleLabel.font = .boldSystemFont(ofSize: 16)
            $0.switchButton.onTintColor = .formerSubColor()
            }.configure {
                $0.switched = AppSettings.shared.getDeveloperMode()
                $0.rowHeight = 50
            }.onSwitchChanged { switched in
                AppSettings.shared.saveDeveloperMode(switched)
        }
        
        // Create SectionFormers
        let checkDataSection = SectionFormer(rowFormer: getStaticModelsRow, checkDataRow)
            .set(headerViewFormer: createHeader(""))
        //.set(footerViewFormer: createFooter("Default UI"))
        
        let realExampleSection = SectionFormer(rowFormer: developerModeRow, switchDateStyleRow, changePasswordRow)
            .set(headerViewFormer: createHeader("Редактирование"))
            //.set(footerViewFormer: createFooter("Редактирование"))
        
        let useCaseSection = SectionFormer(rowFormer:releaseVersionRow, buildVersionNumberRow, currencyRow, currencyRow2, customCellRow)
            .set(headerViewFormer: createHeader(""))
            //.set(footerViewFormer: createFooter("Use Case"))
        
        let defaultSection = SectionFormer(rowFormer: stopAllProcessRow)
            .set(headerViewFormer: createHeader("Опасно"))
            //.set(footerViewFormer: createFooter("Default UI"))
        former.append(sectionFormer: checkDataSection, realExampleSection, useCaseSection, defaultSection)
    }
    
}
