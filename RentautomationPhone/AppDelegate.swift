//
//  AppDelegate.swift
//  RentautomationPhone
//
//  Created by kanybek on 2/4/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import SVProgressHUD
import Kingfisher
import Whisper
import AFDateHelper

let kScanditBarcodeScannerAppKey = "DVTqsEFFEeSDMNlzEgHjjkxXeU3NHtBMlm1tV0TMd3o";

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var passcodeWindow: UIWindow?
    var currentPasscodePresenter: PasscodeLockPresenter?
    var rootNavController: UINavigationController?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow(frame:UIScreen.main.bounds)
        self.setUpHud()
        self.setUpKingfisherCache()
        SBSLicense.setAppKey(kScanditBarcodeScannerAppKey)
        
        var rootController: UIViewController
        if let _ = AppSession.shared.currentEmployee {
            rootController = MainScreenVC(initialString: nil)
        } else {
            rootController = LoginVC(initialItem: nil)
        }
        self.rootNavController = UINavigationController(rootViewController:rootController)
        self.window!.rootViewController = self.rootNavController
        self.window?.makeKeyAndVisible()
        return true
    }
    
    private func createPasscodeLockPresenter() -> PasscodeLockPresenter {
        
        let configuration = PasscodeLockConfiguration()
        let passcodeLockVC = PasscodeLockViewController(state: .enter, configuration: configuration)
        
        passcodeLockVC.dismissCompletionCallback = { [weak self] in
            print("dismissCompletionCallback")
            self?.currentPasscodePresenter?.dismiss()
            self?.currentPasscodePresenter = nil
            self?.passcodeWindow = nil
        }
        
        //passcodeLockVC.logOutCallback = { [weak self] in
        //    print("logOutCallback")
        //    self?.gotoAuthorization()
        //}
        
        passcodeLockVC.successCallback = { (_ lock: PasscodeLockType) in
            print("successCallback")
        }
        
        self.passcodeWindow = UIWindow(frame:UIScreen.main.bounds)
        let presenter = PasscodeLockPresenter(mainWindow: self.passcodeWindow, configuration: configuration, viewController: passcodeLockVC)
        
        return presenter
    }
    
    func enterMainApplication() -> Void {
        ReachabilityManager.sharedInstance.startMonitoringNetworkReachability()
        RPCNetwork.shared.openStreamingConnection()
        let rootController = MainScreenVC(initialString: nil)
        self.rootNavController = UINavigationController(rootViewController: rootController)
        self.window!.rootViewController = self.rootNavController
    }
    
    func enterLoginApplication() -> Void {
        let rootController = LoginVC(initialItem: nil)
        self.rootNavController = UINavigationController(rootViewController: rootController)
        self.window!.rootViewController = self.rootNavController
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        if let _ = AppSession.shared.currentEmployee {
            ReachabilityManager.sharedInstance.stopMonitoringNetworkReachability()
            RPCNetwork.shared.stopStreamingConnection()
        }
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        if let _ = AppSession.shared.currentEmployee {
            ReachabilityManager.sharedInstance.stopMonitoringNetworkReachability()
            RPCNetwork.shared.stopStreamingConnection()
        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        if let _ = AppSession.shared.currentEmployee {
            ReachabilityManager.sharedInstance.startMonitoringNetworkReachability()
            RPCNetwork.shared.openStreamingConnection()
        }
        
        UIApplication.shared.keyWindow?.endEditing(true)
        let passcodePresenter = self.createPasscodeLockPresenter()
        self.currentPasscodePresenter = passcodePresenter
        passcodePresenter.present()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
    }
    
    private func setUpKingfisherCache() -> Void {
        let maxDigits = AppSettings.shared.maximumFractionDigits
        BKCurrencyTextField.setIdGen(Int32(maxDigits))
        ImageCache.default.maxCachePeriodInSecond = TimeInterval(60 * 60 * 24 * 365 * 2) //Cache exists for 2 year
    }
    
    private func setUpHud() -> Void {
        Whisper.Config.modifyInset = false
        SVProgressHUD.setMinimumDismissTimeInterval(1.6)
        SVProgressHUD.setMaximumDismissTimeInterval(1.6)
        SVProgressHUD.setDefaultStyle(.dark)
        SVProgressHUD.setDefaultMaskType(.none)
        SVProgressHUD.setDefaultAnimationType(.native)
        //SVProgressHUD.setDefaultMaskType(.clear)
    }
}

