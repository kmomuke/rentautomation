//
//  MainScreenVC.swift
//  Rentautomation
//
//  Created by kanybek on 8/2/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
import SwiftSignalKit

class MainScreenVC: FormViewController, ErrorPopoverRenderer, KKPasscodeViewControllerDelegate {
    
    var initialString: String?
    var nonSyncedAmount: Int32
    var isFirstTime: Bool
    var syncFinishedDispose: Disposable?
    var rpcStatusDispose: Disposable?
    
    lazy var titleView: TitleView2 = {
        let view = TitleView2()
        return view
    }()
    
    override var title: String? {
        set {
            titleView.titleLabel.text = newValue
        }
        get {
            return titleView.titleLabel.text
        }
    }
    
    convenience init() {
        self.init(initialString: nil)
    }
    
    init(initialString: String?) {
        self.initialString = initialString
        self.nonSyncedAmount = 0
        self.isFirstTime = true
        super.init(nibName: nil, bundle: nil)
        let (image1, image2) = Global.shared.contactsIconImage()
        tabBarItem = UITabBarItem(title: "Главная", image: image1, selectedImage: image2)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.syncFinishedDispose?.dispose()
        self.syncFinishedDispose = nil
        
        self.rpcStatusDispose?.dispose()
        self.rpcStatusDispose = nil
    }
    
    // MARK: Public
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.titleView = self.titleView
        
        self.title = "Главная"
        if ReachabilityManager.shared.isConnectedToInternet() {
            self.titleView.detailLabel.text = "Идет соединение ..."
        } else {
            self.titleView.detailLabel.text = "Отсоединен от сервера"
        }
        
        let syncFinishedSignal: Signal = PipesStore.shared.syncFinishedSignal()
        self.syncFinishedDispose = syncFinishedSignal.start(next: {[weak self] (finished: Bool) in
            if finished {
                self?.updateSyncCell()
            }
        })
        
        let rpcSignalSignal: Signal = PipesStore.shared.rpcConnectionStatusSignal()
        self.syncFinishedDispose = rpcSignalSignal.start(next: {[weak self] (connectionStatus: RpcConnectionType) in
            switch connectionStatus {
            case .connected:
                self?.titleView.detailLabel.text = "Соединен"
                print("connected")
            case .connecting:
                self?.titleView.detailLabel.text = "Идет соединение ..."
                print("connecting")
            case .disconnected:
                self?.titleView.detailLabel.text = "Отсоединен от сервера"
                print("disconnected")
            default:
                print("default")
            }
        })
        
        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.former.deselect(animated: true)
        self.updateSyncCell()
    }
    
    private func updateSyncCell() -> Void {
        Database.shared.countUnSyncedOperations { (totalCount: Int32) in
            
            if (AppSettings.shared.getAutoSyncronizeMode() && totalCount > 3) {
                Synchronizer.shared.synchronizeAllNeededOperations {(response: Bool) in
                    if response {}
                }
            }
            
            self.nonSyncedAmount = totalCount
            let row = self.former.firstSectionFormer?.rowFormers.first
            row?.update()
        }
    }
    
    private func startDocument(_ title: String, viewController: UIViewController) -> Void {
        
        let navController = self.splitViewController?.viewControllers.last
        let controller = navController?.childViewControllers.last
        
        if controller is CreateProductVC {
            return
        }
        
        viewController.title = title
        let documentNC = RNNavigationController(rootViewController:viewController)
        self.splitViewController?.showDetailViewController(documentNC, sender: self)
    }
    
    // MARK: Private
    private func configure() {
        
        tableView.contentInset.top = 20
        tableView.contentInset.bottom = 0
        
        // Create RowFormers
        let createMenu: ((String, (() -> Void)?) -> RowFormer) = {[weak self] text, onSelected in
            return LabelRowFormer<FormLabelCell>() {
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                }.configure {
                    $0.text = text
                    $0.rowHeight = 70
                }.onSelected { _ in
                    self?.former.deselect(animated: true)
                    onSelected?()
            }
        }
        
        let createDynamicMenu: ((String, (() -> Void)?) -> RowFormer) = {[weak self] text, onSelected in
            return LabelRowFormer<FormLabelCell>() {
                
                $0.titleLabel.textColor = UIColor.emeraldColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                }.configure {
                    $0.text = text
                    $0.rowHeight = 70
                }.onSelected { _ in
                    self?.former.deselect(animated: true)
                    onSelected?()
                }.onUpdate {
                    if let totalCount = self?.nonSyncedAmount {
                        if totalCount > 0 {
                            $0.text = "Не сохранено \(totalCount)"
                            $0.cellUpdate({ (cell: FormLabelCell) in
                                cell.formTextLabel()?.textColor = UIColor.pomergranateColor()
                            })
                        } else {
                            $0.text = "Сохранить"
                            $0.cellUpdate({ (cell: FormLabelCell) in
                                cell.formTextLabel()?.textColor = UIColor.emeraldColor()
                            })
                        }
                    }
            }
        }
        
        let syncRow = createDynamicMenu("Сохранить") {[weak self] (Void) in
            if Global.shared.isShowTestButton {
                let syncListVC = SyncOperationsListVC()
                let syncListNC = RNNavigationController(rootViewController:syncListVC)
                self?.splitViewController?.showDetailViewController(syncListNC, sender: self)
            } else {
                Synchronizer.shared.synchronizeAllNeededOperations {(response: Bool) in
                    if response {
                    }
                }
            }
        }
        
        let createSaleRow = createMenu("Продажи") {[weak self] (Void) in
            
            let documentVC = MainDocumentVC(orderDocument: .saleToCustomer, initalOrder: nil)
            documentVC.title = "Продажи"
            let navController = RNNavigationController(rootViewController: documentVC)
            navController.modalPresentationStyle = .fullScreen
            self?.present(navController, animated: true, completion:{_ in
            })
        }
        let incomeSaleRow = createMenu("Закупка") {[weak self] (Void) in
            
            let documentVC = MainDocumentVC(orderDocument: .receiveFromSupplier, initalOrder: nil)
            documentVC.title = "Закупка"
            let navController = RNNavigationController(rootViewController: documentVC)
            navController.modalPresentationStyle = .fullScreen
            self?.present(navController, animated: true, completion:{_ in
            })
        }
        
        
        
        let activityRow = createMenu("История продаж, прихода и расхода") {[weak self] (Void) in
            let orderListVC = OrderTypesListVC(initialString: nil)
            self?.navigationController?.pushViewController(orderListVC, animated: true)
        }
        
        let statisticsRow = createMenu("Отчеты о продажах") {[weak self] (Void) in
            if let strongSelf = self {
                if KKPasscodeLock.shared().isPasscodeRequired() {
                    let vc = KKPasscodeViewController(nibName: nil, bundle: nil)
                    vc.passcodeMode = "statistics"
                    vc.mode = UInt(KKPasscodeModeEnter)
                    vc.delegate = strongSelf
                    strongSelf.presentSheet(viewController: vc)
                } else {
                    let statListVC = StatisticTypesListVC(initialString: "")
                    strongSelf.navigationController?.pushViewController(statListVC, animated: true)
                }
            }
        }
        
        let productRow = createMenu("Товары (Склад)") {[weak self] (Void) in
            if let strongSelf = self {
                if KKPasscodeLock.shared().isPasscodeRequired() && AppSettings.shared.getPassordOnProduct() {
                    let vc = KKPasscodeViewController(nibName: nil, bundle: nil)
                    vc.passcodeMode = "products"
                    vc.mode = UInt(KKPasscodeModeEnter)
                    vc.delegate = strongSelf
                    strongSelf.presentSheet(viewController: vc)
                } else {
                    strongSelf.navigationController?.pushViewController(ProductFilterListVC(), animated: true)
                }
            }
        }
        
        let additionalSettingsRow = createMenu("Настройки") { [weak self] in
            let navController = self?.splitViewController?.viewControllers.last
            let controller = navController?.childViewControllers.last
            
            if controller is SettingsListVC {
                return
            }
            
            let settingsVC = SettingsListVC(initialString: "")
            let settingsNC = RNNavigationController(rootViewController:settingsVC)
            self?.splitViewController?.showDetailViewController(settingsNC, sender: self)
        }
        
        let logOutRow = createMenu("Выйти из аккаунта \nи удалить всю информацию") { [weak self] in
            
            
            
            let alert: UIAlertController = UIAlertController(title: "Выйти из аккаунта",
                                                             message: "Вы действительно хотите выйти из аккаунта \nи удалить всю информацию?",
                                                             preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
            
            alert.addAction(UIAlertAction(title: "Выйти", style: .default, handler: {
                (UIAlertAction) -> Void in
                
                Database.shared.countUnSyncedOperations {[weak self] (totalCount: Int32) in
                    if let strongSelf = self {
                        strongSelf.nonSyncedAmount = totalCount
                        
                        if totalCount > 0 {
                            let _ = strongSelf.showErrorAlert(text: "Нужно сохранить")
                        } else {
                            AppSession.shared.endSession()
                            let passcodeRepository = UserDefaultsPasscodeRepository()
                            passcodeRepository.delete()
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.enterLoginApplication()
                        }
                    }
                }
            }))
            
            self?.present(alert, animated: true, completion: nil)
        }
        
        // Create Headers and Footers
        let createHeader: ((String, UIColor) -> ViewFormer) = { text, color in
            return LabelViewFormer<FormLabelHeaderView>()
                .configure {
                    $0.text = text
                    $0.textColor = color
                    $0.viewHeight = 40
            }
        }
        
        let createFooter: ((String) -> ViewFormer) = { text in
            return LabelViewFormer<FormLabelFooterView>()
                .configure {
                    $0.text = text
                    $0.viewHeight = 55
            }
        }
        
        // Create SectionFormers
        let syncSection = SectionFormer(rowFormer: syncRow)
        //.set(headerViewFormer: createHeader("", UIColor.clear))
        
        let realExampleSection = SectionFormer(rowFormer: createSaleRow, incomeSaleRow, productRow)
            .set(headerViewFormer: createHeader("Продажи", UIColor.greenSeaColor()))
        
        let activitySection = SectionFormer(rowFormer: activityRow, statisticsRow)
            .set(headerViewFormer: createHeader("История", UIColor.belizeHoleColor()))
        //.set(footerViewFormer: createFooter("=== Дополнительно, редкое: ==="))
        
        let settingsSection = SectionFormer(rowFormer: additionalSettingsRow)
            .set(headerViewFormer: createHeader("Данные:", UIColor.carrotColor()))
            //.set(footerViewFormer: createFooter("Простое решение, для учета вашего склада"))
        
        let dangerSection = SectionFormer(rowFormer: logOutRow)
            .set(headerViewFormer: createHeader("", UIColor.pomergranateColor()))
        
        former.append(sectionFormer: syncSection, realExampleSection, activitySection, settingsSection, dangerSection)
    }
    
    //MARK: KKPasscodeViewControllerDelegate methods
    func didPasscodeEnteredCorrectly(_ viewController: KKPasscodeViewController!) {
        if viewController.passcodeMode == "statistics" {
            let statListVC = StatisticTypesListVC(initialString: "")
            self.navigationController?.pushViewController(statListVC, animated: true)
        } else if viewController.passcodeMode == "products" {
            self.navigationController?.pushViewController(ProductFilterListVC(), animated: true)
        }
    }
    
    func didPasscodeEnteredIncorrectly(_ viewController: KKPasscodeViewController!) {
        print("didPasscodeEnteredIncorrectly")
    }
}

class TitleView2: UIView {
    
    var titleLabel: UILabel!
    var detailLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        titleLabel = UILabel()
        titleLabel.font = UIFont.systemFont(ofSize: 16, weight: UIFontWeightSemibold)
        titleLabel.textColor = UIColor.black
        titleLabel.textAlignment = .center
        addSubview(titleLabel)
        
        detailLabel = UILabel()
        detailLabel.font = UIFont.systemFont(ofSize: 12)
        detailLabel.textColor = UIColor.gray
        detailLabel.textAlignment = .center
        addSubview(detailLabel)
        
        detailLabel.text = "last seen yesterday at 5:56 PM"
        
        // Comment here for future.
        //detailLabel.isHidden = true
        
        verticalLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func verticalLayout() {
        frame = CGRect(x: 0, y: 0, width: 200, height: 44)
        
        titleLabel.frame = CGRect(x: 0, y: 4, width: frame.width, height: 21)
        detailLabel.frame = CGRect(x: 0, y: titleLabel.frame.maxY + 2, width: frame.width, height: 15)
    }
    
    func horizontalLayout() {
        frame = CGRect(x: 0, y: 0, width: 300, height: 40)
        
        let titleSize = titleLabel.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: 21))
        let detailSize = detailLabel.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: 15))
        
        let contentWidth = titleSize.width + 6 + detailSize.width
        
        var currentX = frame.width / 2 - contentWidth / 2
        
        titleLabel.frame = CGRect(x: currentX, y: frame.height / 2 - titleSize.height / 2, width: titleSize.width, height: titleSize.height)
        
        currentX += titleSize.width + 6
        
        detailLabel.frame = CGRect(x: currentX, y: frame.height / 2 - detailSize.height / 2, width: detailSize.width, height: detailSize.height)
    }
    
}

