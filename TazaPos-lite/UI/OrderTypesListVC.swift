//
//  OrderTypesListVC.swift
//  Rentautomation
//
//  Created by kanybek on 8/2/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit

class OrderTypesListVC: FormViewController {
    
    var initialString: String?
    
    convenience init() {
        self.init(initialString: nil)
    }
    
    init(initialString: String?) {
        self.initialString = initialString
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    deinit {
    }
    
    // MARK: Public
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "История"
        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
    }
    
    // MARK: Private
    private func configure() {
        
        let backBarButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backBarButton
        
        // Create RowFormers
        let createMenu: ((String, (() -> Void)?) -> RowFormer) = { text, onSelected in
            return LabelRowFormer<FormLabelCell>() {
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                }.configure {
                    $0.text = text
                    $0.rowHeight = 70
                }.onSelected { _ in
                    onSelected?()
            }
        }
        
        let allDocumentTypesRow = createMenu("Все операции по кассе") { [weak self] in
            
            let orderListVC = OrdersListVC(initialOrderDocumentType: .none);
            let productListNC = RNNavigationController(rootViewController:orderListVC)
            self?.splitViewController?.showDetailViewController(productListNC, sender: self)
        }
        
        let saleDocumentTypesTRow = createMenu("Продажи") { [weak self] in
            let orderListVC = OrdersListVC(initialOrderDocumentType: .saleToCustomer);
            let productListNC = RNNavigationController(rootViewController:orderListVC)
            self?.splitViewController?.showDetailViewController(productListNC, sender: self)
        }
        
        let customerPreOrderTypesRow = createMenu("Закупки") { [weak self] in
            let orderListVC = OrdersListVC(initialOrderDocumentType: .receiveFromSupplier);
            let productListNC = RNNavigationController(rootViewController:orderListVC)
            self?.splitViewController?.showDetailViewController(productListNC, sender: self)
        }
        
        /*
         let incomeMoneyDocumentTypesRow = createMenu("Приход денег") { [weak self] in
         let orderListVC = OrdersListVC(initialOrderDocumentType: .moneyReceived);
         let productListNC = RNNavigationController(rootViewController:orderListVC)
         self?.splitViewController?.showDetailViewController(productListNC, sender: self)
         }
         
         let receiveDocumentTypesRow = createMenu("История закупок") { [weak self] in
         
         let orderListVC = OrdersListVC(initialOrderDocumentType: .productOrderReceivedFromSupplier);
         let productListNC = RNNavigationController(rootViewController:orderListVC)
         self?.splitViewController?.showDetailViewController(productListNC, sender: self)
         }
         
         let incomeMoneyDocumentTypesRow = createMenu("История прихода") { [weak self] in
         
         let orderListVC = OrdersListVC(initialOrderDocumentType: .moneyReceived);
         let productListNC = RNNavigationController(rootViewController:orderListVC)
         self?.splitViewController?.showDetailViewController(productListNC, sender: self)
         }
         
         let outgoneMoneyDocumentTypesRow = createMenu("История расхода") { [weak self] in
         
         let orderListVC = OrdersListVC(initialOrderDocumentType: .moneyGone);
         let productListNC = RNNavigationController(rootViewController:orderListVC)
         self?.splitViewController?.showDetailViewController(productListNC, sender: self)
         }
         
         let returnFromCustomerRow = createMenu("История возврата от клиента") { [weak self] in
         
         let orderListVC = OrdersListVC(initialOrderDocumentType: .productReturnedFromCustomer);
         let productListNC = RNNavigationController(rootViewController:orderListVC)
         self?.splitViewController?.showDetailViewController(productListNC, sender: self)
         }
         
         let returnToSupplierRow = createMenu("История возврата к поставщику") { [weak self] in
         
         let orderListVC = OrdersListVC(initialOrderDocumentType: .productReturnedToSupplier);
         let productListNC = RNNavigationController(rootViewController:orderListVC)
         self?.splitViewController?.showDetailViewController(productListNC, sender: self)
         }
         
        let moneyIncomeAutgoneRow = createMenu("Приход/Расход денег за промежуток времени") { [weak self] in
            let totalMoneyVC = TotalMoneyOnDateVC(initialItem: "koke")
            let totalMoneyNC = RNNavigationController(rootViewController:totalMoneyVC)
            self?.splitViewController?.showDetailViewController(totalMoneyNC, sender: self)
        }
        
        let detailRow = createMenu("Подробная свертка документов") { [weak self] in
            let detailVC = DetailedOrderListTVC(initialItem: nil)
            let detailNC = RNNavigationController(rootViewController:detailVC)
            self?.splitViewController?.showDetailViewController(detailNC, sender: self)
        }
        
        let allMoneyTransactionsRow = createMenu("Платежи с клиентами и поставщикам") { [weak self] in
            let transactionsListVC = TransactionsListVC(initialString: nil)
            let transactionsListNC = RNNavigationController(rootViewController:transactionsListVC)
            self?.splitViewController?.showDetailViewController(transactionsListNC, sender: self)
        }*/
        
        // Create Headers and Footers
        let createHeader: ((String) -> ViewFormer) = { text in
            return LabelViewFormer<FormLabelHeaderView>()
                .configure {
                    $0.text = text
                    $0.viewHeight = 40
            }
        }
        
        let createFooter: ((String) -> ViewFormer) = { text in
            return LabelViewFormer<FormLabelFooterView>()
                .configure {
                    $0.text = text
                    $0.viewHeight = 100
            }
        }
        
        // Create SectionFormers
        //let realExampleSection = SectionFormer(rowFormer: allDocumentTypesRow, saleDocumentTypesTRow, receiveDocumentTypesRow, incomeMoneyDocumentTypesRow, outgoneMoneyDocumentTypesRow, returnFromCustomerRow, returnToSupplierRow, moneyIncomeAutgoneRow, allMoneyTransactionsRow)
        let realExampleSection1 = SectionFormer(rowFormer: allDocumentTypesRow, saleDocumentTypesTRow, customerPreOrderTypesRow)
            .set(headerViewFormer: createHeader("История"))
        
        //let realExampleSection2 = SectionFormer(rowFormer: moneyIncomeAutgoneRow, detailRow, allMoneyTransactionsRow)
        //    .set(headerViewFormer: createHeader("Дополнительно"))
        //    .set(footerViewFormer: createFooter("История денег, продаж, закупок, и расход"))
        
        former.append(sectionFormer: realExampleSection1)
    }
}
