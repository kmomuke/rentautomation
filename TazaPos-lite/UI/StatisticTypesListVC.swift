//
//  StatisticTypesListVC.swift
//  Rentautomation
//
//  Created by kanybek on 8/3/17.
//  Copyright © 2017 kanybek. All rights reserved.
//

import UIKit
class StatisticTypesListVC: FormViewController {
    
    var initialString: String?
    
    convenience init() {
        self.init(initialString: nil)
    }
    
    init(initialString: String?) {
        self.initialString = initialString
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Отчеты о продажах"
        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        former.deselect(animated: true)
    }
    
    // MARK: Private
    private func configure() {
        
        let backBarButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backBarButton
        
        // Create RowFormers
        let createMenu: ((String, (() -> Void)?) -> RowFormer) = { text, onSelected in
            return LabelRowFormer<FormLabelCell>() {
                $0.titleLabel.textColor = .formerColor()
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
                $0.accessoryType = .disclosureIndicator
                }.configure {
                    $0.text = text
                    $0.rowHeight = 70
                }.onSelected { _ in
                    onSelected?()
            }
        }
        
        let debtStatisticsRow = createMenu("Отчеты о продаже, прибыли и по долгам клиентов") { [weak self] in
            let navController = self?.splitViewController?.viewControllers.last
            let controller = navController?.childViewControllers.last
            
            if controller is DebtStatisticsVC {
                return
            }
            
            let statisticsVC = DebtStatisticsVC(initalString: "")
            let statisticsNC = RNNavigationController(rootViewController:statisticsVC)
            self?.splitViewController?.showDetailViewController(statisticsNC, sender: self)
        }
        
        let customerPreOrderTypesRow = createMenu("Самые продоваемые товары") { [weak self] in
            let statisticsVC = MostSaledProductsVC(initialOrderDocumentType: .saleToCustomer)
            let statisticsNC = RNNavigationController(rootViewController:statisticsVC)
            self?.splitViewController?.showDetailViewController(statisticsNC, sender: self)
        }
        
        
        // Create Headers and Footers
        let createHeader: ((String) -> ViewFormer) = { text in
            return LabelViewFormer<FormLabelHeaderView>()
                .configure {
                    $0.text = text
                    $0.viewHeight = 40
            }
        }
        
        let realExampleSection1 = SectionFormer(rowFormer: debtStatisticsRow, customerPreOrderTypesRow)
            .set(headerViewFormer: createHeader(" "))
        former.append(sectionFormer: realExampleSection1)
    }
}
